package au.awh.file.utilitiesprintandemail.confirmprint;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 8
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ConfirmPrint (WSSYSWSPVK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ConfirmPrintResult implements Serializable
{
	private static final long serialVersionUID = 7456061468678113845L;

	private ReturnCodeEnum _sysReturnCode;
	private ExitProgramOptionEnum exitProgramOption = null; // ExitProgramOptionEnum 29901
	private String copies = ""; // String 9040
	private FormsEnum forms = null; // FormsEnum 9043
	private HoldClFormatyesnoEnum holdClFormatyesno = null; // HoldClFormatyesnoEnum 9078
	private SaveClFormatyesnoEnum saveClFormatyesno = null; // SaveClFormatyesnoEnum 9079
	private String outputQueue = ""; // String 34671
	private String outputQueueLibrary = ""; // String 36415
	private SendViaPrintedReportEnum sendViaEmail = null; // SendViaPrintedReportEnum 42734

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
	
	public String getCopies() {
		return copies;
	}
	
	public void setCopies(String copies) {
		this.copies = copies;
	}
	
	public FormsEnum getForms() {
		return forms;
	}
	
	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}
	
	public void setForms(String forms) {
		setForms(FormsEnum.valueOf(forms));
	}
	
	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(String holdClFormatyesno) {
		setHoldClFormatyesno(HoldClFormatyesnoEnum.valueOf(holdClFormatyesno));
	}
	
	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(String saveClFormatyesno) {
		setSaveClFormatyesno(SaveClFormatyesnoEnum.valueOf(saveClFormatyesno));
	}
	
	public String getOutputQueue() {
		return outputQueue;
	}
	
	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}
	
	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}
	
	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}
	
	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}
	
	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}
	
	public void setSendViaEmail(String sendViaEmail) {
		setSendViaEmail(SendViaPrintedReportEnum.valueOf(sendViaEmail));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
