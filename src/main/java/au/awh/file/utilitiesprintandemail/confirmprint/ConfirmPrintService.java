package au.awh.file.utilitiesprintandemail.confirmprint;

// Function Stub for PMTRCD 1611115
// UtilitiesPrintAndEmail.confirmPrint

// Parameters:
// O Field 29901 exitProgramOption ExitProgramOptionEnum "Exit Program Option"
// B Field 9040 copies String(2) "Copies"
// B Field 9043 forms FormsEnum "Forms"
// B Field 9078 holdClFormatyesno HoldClFormatyesnoEnum "Hold (CL Format *YES/*NO)"
// B Field 9079 saveClFormatyesno SaveClFormatyesnoEnum "Save (CL Format *YES/*NO)"
// B Field 34671 outputQueue String(10) "Output Queue"
// B Field 36415 outputQueueLibrary String(10) Ref 31632 storeLocationOutqlib "Output Queue Library"
// B Field 42734 sendViaEmail SendViaPrintedReportEnum Ref 42733 sendViaPrintedReport "Send via Email"
// I Field 33688 displayConfirmPrompt YesOrNoEnum Ref 12106 yesOrNo "Display Confirm Prompt"
// I Field 42200 report ReportEnum "Report"

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;


import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;

/**
 * PMTRCD Service stub for 'Confirm Print', file 'Utilities Print & Email' (WSG4RCP)
 *
 * @author X2EGenerator  FunctionStub.kt
 */
@Service
public class ConfirmPrintService extends AbstractService<ConfirmPrintService, ConfirmPrintDTO>
{
	private final Step execute = define("execute", ConfirmPrintParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	return null;
	});

	@Autowired
	public ConfirmPrintService() {
		super(ConfirmPrintService.class, ConfirmPrintDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	private StepResult executeService(ConfirmPrintDTO confirmPrintDTO, ConfirmPrintParams confirmPrintParams) throws ServiceException {

		// TODO: Code generation is not currently supported for this function
		return NO_ACTION;
	}
}
