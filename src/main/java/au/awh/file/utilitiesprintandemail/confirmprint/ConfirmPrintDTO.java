package au.awh.file.utilitiesprintandemail.confirmprint;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.ValidationOkEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ConfirmPrintDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ExitProgramOptionEnum exitProgramOption;
	private FormsEnum forms;
	private HoldClFormatyesnoEnum holdClFormatyesno;
	private ReportEnum report;
	private SaveClFormatyesnoEnum saveClFormatyesno;
	private SendViaPrintedReportEnum sendViaEmail;
	private String copies;
	private String outputQueue;
	private String outputQueueLibrary;
	private YesOrNoEnum displayConfirmPrompt;

	private ValidationOkEnum lclValidationOk;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getCopies() {
		return copies;
	}

	public YesOrNoEnum getDisplayConfirmPrompt() {
		return displayConfirmPrompt;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public ReportEnum getReport() {
		return report;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}

	public ValidationOkEnum getLclValidationOk() {
		return lclValidationOk;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public void setDisplayConfirmPrompt(YesOrNoEnum displayConfirmPrompt) {
		this.displayConfirmPrompt = displayConfirmPrompt;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public void setReport(ReportEnum report) {
		this.report = report;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}

	public void setLclValidationOk(ValidationOkEnum lclValidationOk) {
		this.lclValidationOk = lclValidationOk;
	}

}
