package au.awh.file.utilitiesprintandemail.confirmeprint;

// ExecuteInternalFunction.kt File=UtilitiesPrintAndEmail (G4) Function=confirmePrint (1611121) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesprintandemail.confirmprint.ConfirmPrintService;
// imports for DTO
import au.awh.file.utilitiesprintandemail.confirmprint.ConfirmPrintDTO;
// imports for Enums
import au.awh.model.CommitmentControlLevelEnum;
import au.awh.model.ConsignmentContainerStsEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.utilitiesprintandemail.confirmprint.ConfirmPrintParams;
// imports for Results
import au.awh.file.utilitiesprintandemail.confirmprint.ConfirmPrintResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ConfirmE Print' (file 'Utilities Print & Email' (WSG4RCP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ConfirmePrintService extends AbstractService<ConfirmePrintService, ConfirmePrintDTO>
{
	private final Step execute = define("execute", ConfirmePrintParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private ConfirmPrintService confirmPrintService;

	private final Step serviceConfirmPrint = define("serviceConfirmPrint",ConfirmPrintResult.class, this::processServiceConfirmPrint);
	
	@Autowired
	public ConfirmePrintService() {
		super(ConfirmePrintService.class, ConfirmePrintDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ConfirmePrintParams params) throws ServiceException {
		ConfirmePrintDTO dto = new ConfirmePrintDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ConfirmePrintDTO dto, ConfirmePrintParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ConfirmPrintResult confirmPrintResult = null;
		ConfirmPrintParams confirmPrintParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Confirm Print - Utilities Print & Email  *
		confirmPrintParams = new ConfirmPrintParams();
		confirmPrintResult = new ConfirmPrintResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, confirmPrintParams);
		confirmPrintParams.setOutputQueue(dto.getOutputQueue());
		confirmPrintParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
		confirmPrintParams.setCopies(dto.getCopies());
		confirmPrintParams.setForms(dto.getForms());
		confirmPrintParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
		confirmPrintParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
		confirmPrintParams.setReport(dto.getReport());
		confirmPrintParams.setDisplayConfirmPrompt(dto.getDisplayConfirmPrompt());
		confirmPrintParams.setSendViaEmail(dto.getSendViaEmail());
		// TODO SPLIT SCREEN EXCINTFUN UtilitiesPrintAndEmail.confirmePrint (2) -> PMTRCD UtilitiesPrintAndEmail.confirmPrint
		stepResult = StepResult.callService(ConfirmPrintService.class, confirmPrintParams).thenCall(serviceConfirmPrint);
		dto.setExitProgramOption(confirmPrintResult.getExitProgramOption());
		dto.setOutputQueue(confirmPrintResult.getOutputQueue());
		dto.setOutputQueueLibrary(confirmPrintResult.getOutputQueueLibrary());
		dto.setCopies(confirmPrintResult.getCopies());
		dto.setForms(confirmPrintResult.getForms());
		dto.setHoldClFormatyesno(confirmPrintResult.getHoldClFormatyesno());
		dto.setSaveClFormatyesno(confirmPrintResult.getSaveClFormatyesno());
		dto.setSendViaEmail(confirmPrintResult.getSendViaEmail());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1611121 UtilitiesPrintAndEmail.confirmePrint 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		ConfirmePrintResult result = new ConfirmePrintResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


    /**
     * ConfirmPrintService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceConfirmPrint(ConfirmePrintDTO dto, ConfirmPrintResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, dto);
        }

        //TODO: call the continuation of the program
        ConfirmePrintParams confirmePrintParams = new ConfirmePrintParams();
        BeanUtils.copyProperties(dto, confirmePrintParams);
        stepResult = StepResult.callService(ConfirmePrintService.class, confirmePrintParams);

        return stepResult;
    }

}
