package au.awh.file.utilitiesprintandemail.confirmeprint;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 10
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReportEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ConfirmePrint ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ConfirmePrintParams implements Serializable
{
	private static final long serialVersionUID = -8203557487926685707L;

    private ErrorProcessingEnum errorProcessing = null;
    private String copies = "";
    private FormsEnum forms = null;
    private HoldClFormatyesnoEnum holdClFormatyesno = null;
    private SaveClFormatyesnoEnum saveClFormatyesno = null;
    private String outputQueue = "";
    private String outputQueueLibrary = "";
    private SendViaPrintedReportEnum sendViaEmail = null;
    private YesOrNoEnum displayConfirmPrompt = null;
    private ReportEnum report = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public String getCopies() {
		return copies;
	}
	
	public void setCopies(String copies) {
		this.copies = copies;
	}
	
	public FormsEnum getForms() {
		return forms;
	}
	
	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}
	
	public void setForms(String forms) {
		setForms(FormsEnum.valueOf(forms));
	}
	
	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(String holdClFormatyesno) {
		setHoldClFormatyesno(HoldClFormatyesnoEnum.valueOf(holdClFormatyesno));
	}
	
	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(String saveClFormatyesno) {
		setSaveClFormatyesno(SaveClFormatyesnoEnum.valueOf(saveClFormatyesno));
	}
	
	public String getOutputQueue() {
		return outputQueue;
	}
	
	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}
	
	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}
	
	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}
	
	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}
	
	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}
	
	public void setSendViaEmail(String sendViaEmail) {
		setSendViaEmail(SendViaPrintedReportEnum.valueOf(sendViaEmail));
	}
	
	public YesOrNoEnum getDisplayConfirmPrompt() {
		return displayConfirmPrompt;
	}
	
	public void setDisplayConfirmPrompt(YesOrNoEnum displayConfirmPrompt) {
		this.displayConfirmPrompt = displayConfirmPrompt;
	}
	
	public void setDisplayConfirmPrompt(String displayConfirmPrompt) {
		setDisplayConfirmPrompt(YesOrNoEnum.valueOf(displayConfirmPrompt));
	}
	
	public ReportEnum getReport() {
		return report;
	}
	
	public void setReport(ReportEnum report) {
		this.report = report;
	}
	
	public void setReport(String report) {
		setReport(ReportEnum.valueOf(report));
	}
}
