package au.awh.file.utilitiesprintandemail.getcurrentuseremail;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetCurrentUserEmail ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCurrentUserEmailResult implements Serializable
{
	private static final long serialVersionUID = 6174315453930489560L;

	private ReturnCodeEnum _sysReturnCode;
	private String emailAddress60 = ""; // String 42777

	public String getEmailAddress60() {
		return emailAddress60;
	}
	
	public void setEmailAddress60(String emailAddress60) {
		this.emailAddress60 = emailAddress60;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
