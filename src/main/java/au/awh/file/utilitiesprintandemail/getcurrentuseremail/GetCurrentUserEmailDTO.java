package au.awh.file.utilitiesprintandemail.getcurrentuseremail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetCurrentUserEmailDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String emailAddress60;

	private String lclCharacter;
	private String lclUserProfileNameText;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getEmailAddress60() {
		return emailAddress60;
	}

	public String getLclCharacter() {
		return lclCharacter;
	}

	public String getLclUserProfileNameText() {
		return lclUserProfileNameText;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setEmailAddress60(String emailAddress60) {
		this.emailAddress60 = emailAddress60;
	}

	public void setLclCharacter(String lclCharacter) {
		this.lclCharacter = lclCharacter;
	}

	public void setLclUserProfileNameText(String lclUserProfileNameText) {
		this.lclUserProfileNameText = lclUserProfileNameText;
	}

}
