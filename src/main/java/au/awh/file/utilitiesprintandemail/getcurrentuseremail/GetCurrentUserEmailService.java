package au.awh.file.utilitiesprintandemail.getcurrentuseremail;

// ExecuteInternalFunction.kt File=UtilitiesPrintAndEmail (G4) Function=getCurrentUserEmail (2028027) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsService;
import au.awh.file.utilitiesstrings.rmvspacesinstring.RmvSpacesInStringService;
// imports for DTO
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsDTO;
import au.awh.file.utilitiesstrings.rmvspacesinstring.RmvSpacesInStringDTO;
// imports for Enums
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsParams;
import au.awh.file.utilitiesstrings.rmvspacesinstring.RmvSpacesInStringParams;
// imports for Results
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsResult;
import au.awh.file.utilitiesstrings.rmvspacesinstring.RmvSpacesInStringResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'Get current User Email' (file 'Utilities Print & Email' (WSG4RCP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GetCurrentUserEmailService extends AbstractService<GetCurrentUserEmailService, GetCurrentUserEmailDTO>
{
	private final Step execute = define("execute", GetCurrentUserEmailParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private RmvSpacesInStringService rmvSpacesInStringService;

	@Autowired
	private RtvusrprfDetailsService rtvusrprfDetailsService;

	//private final Step serviceRtvusrprfDetails = define("serviceRtvusrprfDetails",RtvusrprfDetailsResult.class, this::processServiceRtvusrprfDetails);
	//private final Step serviceRmvSpacesInString = define("serviceRmvSpacesInString",RmvSpacesInStringResult.class, this::processServiceRmvSpacesInString);
	
	@Autowired
	public GetCurrentUserEmailService() {
		super(GetCurrentUserEmailService.class, GetCurrentUserEmailDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GetCurrentUserEmailParams params) throws ServiceException {
		GetCurrentUserEmailDTO dto = new GetCurrentUserEmailDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GetCurrentUserEmailDTO dto, GetCurrentUserEmailParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		RtvusrprfDetailsResult rtvusrprfDetailsResult = null;
		RmvSpacesInStringResult rmvSpacesInStringResult = null;
		RtvusrprfDetailsParams rtvusrprfDetailsParams = null;
		RmvSpacesInStringParams rmvSpacesInStringParams = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT RTVUSRPRF Details - Utilities OS/400 API  *
		rtvusrprfDetailsParams = new RtvusrprfDetailsParams();
		rtvusrprfDetailsResult = new RtvusrprfDetailsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, rtvusrprfDetailsParams);
		rtvusrprfDetailsParams.setReturnCode(dto.get_SysReturnCode());
		rtvusrprfDetailsParams.setUserId(job.getUser());
		stepResult = rtvusrprfDetailsService.execute(rtvusrprfDetailsParams);
		rtvusrprfDetailsResult = (RtvusrprfDetailsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(rtvusrprfDetailsResult.get_SysReturnCode());
		dto.set_SysReturnCode(rtvusrprfDetailsResult.getReturnCode());
		dto.setLclUserProfileNameText(rtvusrprfDetailsResult.getUserProfileNameText());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000041 ACT LCL.Character = JOB.*USER
		dto.setLclCharacter(job.getUser());
		// DEBUG genFunctionCall END
		if (dto.getLclCharacter().equals("A")) {
			// LCL.Character is A
			// DEBUG genFunctionCall BEGIN 1000012 ACT Rmv spaces in String - Utilities Strings  *
			rmvSpacesInStringParams = new RmvSpacesInStringParams();
			rmvSpacesInStringResult = new RmvSpacesInStringResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, rmvSpacesInStringParams);
			rmvSpacesInStringParams.setString280(dto.getLclUserProfileNameText());
			rmvSpacesInStringParams.setStringLength("40");
			stepResult = rmvSpacesInStringService.execute(rmvSpacesInStringParams);
			rmvSpacesInStringResult = (RmvSpacesInStringResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(rmvSpacesInStringResult.get_SysReturnCode());
			dto.setLclUserProfileNameText(rmvSpacesInStringResult.getString280());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000016 ACT PAR.Email Address 60 = CONCAT(LCL.User Profile Name Text,CON.@awh.com.au,CND.*None)
			dto.setEmailAddress60(dto.getLclUserProfileNameText().trim().concat("@awh.com.au"));
			// DEBUG genFunctionCall END
		}

		GetCurrentUserEmailResult result = new GetCurrentUserEmailResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * RtvusrprfDetailsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvusrprfDetails(GetCurrentUserEmailDTO dto, RtvusrprfDetailsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GetCurrentUserEmailParams getCurrentUserEmailParams = new GetCurrentUserEmailParams();
//        BeanUtils.copyProperties(dto, getCurrentUserEmailParams);
//        stepResult = StepResult.callService(GetCurrentUserEmailService.class, getCurrentUserEmailParams);
//
//        return stepResult;
//    }
////
//    /**
//     * RmvSpacesInStringService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRmvSpacesInString(GetCurrentUserEmailDTO dto, RmvSpacesInStringParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GetCurrentUserEmailParams getCurrentUserEmailParams = new GetCurrentUserEmailParams();
//        BeanUtils.copyProperties(dto, getCurrentUserEmailParams);
//        stepResult = StepResult.callService(GetCurrentUserEmailService.class, getCurrentUserEmailParams);
//
//        return stepResult;
//    }
//
}
