package au.awh.file.utilitiesprintandemail.setwsprintparameters;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ReportEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: SetWsPrintParameters (WSUTLWSXFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class SetWsPrintParametersParams implements Serializable
{
	private static final long serialVersionUID = -5135192220102865043L;

    private String brokerIdKey = "";
    private StateCodeKeyEnum stateCodeKey = null;
    private String centreCodeKey = "";
    private ReportEnum report = null;

	public String getBrokerIdKey() {
		return brokerIdKey;
	}
	
	public void setBrokerIdKey(String brokerIdKey) {
		this.brokerIdKey = brokerIdKey;
	}
	
	public StateCodeKeyEnum getStateCodeKey() {
		return stateCodeKey;
	}
	
	public void setStateCodeKey(StateCodeKeyEnum stateCodeKey) {
		this.stateCodeKey = stateCodeKey;
	}
	
	public void setStateCodeKey(String stateCodeKey) {
		setStateCodeKey(StateCodeKeyEnum.fromCode(stateCodeKey));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public ReportEnum getReport() {
		return report;
	}
	
	public void setReport(ReportEnum report) {
		this.report = report;
	}
	
	public void setReport(String report) {
		setReport(ReportEnum.fromCode(report));
	}
}
