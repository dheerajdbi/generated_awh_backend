package au.awh.file.utilitiesprintandemail.setwsprintparameters;

// Function Stub for EXCEXTFUN 1600491
// UtilitiesPrintAndEmail.setWsPrintParameters

// Parameters:
// I Field 1118 brokerIdKey String(1) "Broker Id             KEY"
// I Field 20673 stateCodeKey StateCodeKeyEnum "State Code            KEY"
// I Field 1140 centreCodeKey String(2) "Centre Code           KEY"
// I Field 42200 report ReportEnum "Report"
// O Field 34671 outputQueue String(10) "Output Queue"
// O Field 36415 outputQueueLibrary String(10) Ref 31632 storeLocationOutqlib "Output Queue Library"
// O Field 9040 copies String(2) "Copies"
// O Field 9043 forms FormsEnum "Forms"
// O Field 9078 holdClFormatyesno HoldClFormatyesnoEnum "Hold (CL Format *YES/*NO)"
// O Field 9079 saveClFormatyesno SaveClFormatyesnoEnum "Save (CL Format *YES/*NO)"
// O Field 43094 overflowLineNumberText String(3) "Overflow Line Number Text"

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;


/**
 * EXCEXTFUN Service for 'Set WS print parameters' (file 'Utilities Print & Email'
 *
 * @author X2EGenerator  FunctionStub.kt
 */
@Service
public class SetWsPrintParametersService {

	public StepResult execute(SetWsPrintParametersParams setWsPrintParametersParams) {
		// TODO: Code generation is not currently supported for this function
		return NO_ACTION;
	}
}
