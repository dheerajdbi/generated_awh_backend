package au.awh.file.utilitiesprintandemail.setwsprintparameters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.StateCodeKeyEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class SetWsPrintParametersDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private FormsEnum forms;
	private HoldClFormatyesnoEnum holdClFormatyesno;
	private ReportEnum report;
	private SaveClFormatyesnoEnum saveClFormatyesno;
	private StateCodeKeyEnum stateCodeKey;
	private String brokerIdKey;
	private String centreCodeKey;
	private String copies;
	private String outputQueue;
	private String outputQueueLibrary;
	private String overflowLineNumberText;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getBrokerIdKey() {
		return brokerIdKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getCopies() {
		return copies;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public String getOverflowLineNumberText() {
		return overflowLineNumberText;
	}

	public ReportEnum getReport() {
		return report;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public StateCodeKeyEnum getStateCodeKey() {
		return stateCodeKey;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setBrokerIdKey(String brokerIdKey) {
		this.brokerIdKey = brokerIdKey;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public void setOverflowLineNumberText(String overflowLineNumberText) {
		this.overflowLineNumberText = overflowLineNumberText;
	}

	public void setReport(ReportEnum report) {
		this.report = report;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public void setStateCodeKey(StateCodeKeyEnum stateCodeKey) {
		this.stateCodeKey = stateCodeKey;
	}

}
