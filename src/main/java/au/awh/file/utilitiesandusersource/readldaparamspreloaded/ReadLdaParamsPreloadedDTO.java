package au.awh.file.utilitiesandusersource.readldaparamspreloaded;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.GlobalContext;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ReadLdaParamsPreloadedDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Autowired
	private GlobalContext globalCtx;

	private ReturnCodeEnum _sysReturnCode;



	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getWfLdaAttnMenuSwitch() {
		return globalCtx.getString("ldaAttnMenuSwitch");
	}

	public String getWfLdaClientAccountId() {
		return globalCtx.getString("ldaClientAccountId");
	}

	public String getWfLdaClipCode() {
		return globalCtx.getString("ldaClipCode");
	}

	public String getWfLdaCostCentre() {
		return globalCtx.getString("ldaCostCentre");
	}

	public String getWfLdaCurrentBrokerId() {
		return globalCtx.getString("ldaCurrentBrokerId");
	}

	public String getWfLdaCurrentCentreCode() {
		return globalCtx.getString("ldaCurrentCentreCode");
	}

	public String getWfLdaCurrentStateCode() {
		return globalCtx.getString("ldaCurrentStateCode");
	}

	public String getWfLdaDefaultBrokerId() {
		return globalCtx.getString("ldaDefaultBrokerId");
	}

	public String getWfLdaDefaultCentreCode() {
		return globalCtx.getString("ldaDefaultCentreCode");
	}

	public String getWfLdaDefaultStateCode() {
		return globalCtx.getString("ldaDefaultStateCode");
	}

	public String getWfLdaDftSaleNbrId() {
		return globalCtx.getString("ldaDftSaleNbrId");
	}

	public String getWfLdaDftSaleNbrSlgCtr() {
		return globalCtx.getString("ldaDftSaleNbrSlgCtr");
	}

	public String getWfLdaDftSaleNbrStgCtr() {
		return globalCtx.getString("ldaDftSaleNbrStgCtr");
	}

	public String getWfLdaDftSaleSeasonCent() {
		return globalCtx.getString("ldaDftSaleSeasonCent");
	}

	public String getWfLdaDftSaleSeasonYear() {
		return globalCtx.getString("ldaDftSaleSeasonYear");
	}

	public String getWfLdaEldersStateCode() {
		return globalCtx.getString("ldaEldersStateCode");
	}

	public String getWfLdaEldersStateNumCode() {
		return globalCtx.getString("ldaEldersStateNumCode");
	}

	public String getWfLdaEnvironment() {
		return globalCtx.getString("ldaEnvironment");
	}

	public String getWfLdaFolioNumber() {
		return globalCtx.getString("ldaFolioNumber");
	}

	public String getWfLdaHomeCostCentre() {
		return globalCtx.getString("ldaHomeCostCentre");
	}

	public String getWfLdaLotNumber() {
		return globalCtx.getString("ldaLotNumber");
	}

	public String getWfLdaNetworkSystemName() {
		return globalCtx.getString("ldaNetworkSystemName");
	}

	public String getWfLdaOutputCostCentre() {
		return globalCtx.getString("ldaOutputCostCentre");
	}

	public String getWfLdaPrinter() {
		return globalCtx.getString("ldaPrinter");
	}

	public String getWfLdaProcSaleNbrId() {
		return globalCtx.getString("ldaProcSaleNbrId");
	}

	public String getWfLdaProcSaleNbrSlgCtr() {
		return globalCtx.getString("ldaProcSaleNbrSlgCtr");
	}

	public String getWfLdaProcSaleNbrStgCtr() {
		return globalCtx.getString("ldaProcSaleNbrStgCtr");
	}

	public String getWfLdaProcSaleSeasonCent() {
		return globalCtx.getString("ldaProcSaleSeasonCent");
	}

	public String getWfLdaProcSaleSeasonYear() {
		return globalCtx.getString("ldaProcSaleSeasonYear");
	}

	public String getWfLdaSecurityProfile() {
		return globalCtx.getString("ldaSecurityProfile");
	}

	public String getWfLdaStateCostCentre() {
		return globalCtx.getString("ldaStateCostCentre");
	}

	public String getWfLdaStateHighSpeedPrtr() {
		return globalCtx.getString("ldaStateHighSpeedPrtr");
	}

	public String getWfLdaTerminal() {
		return globalCtx.getString("ldaTerminal");
	}

	public String getWfLdaUser() {
		return globalCtx.getString("ldaUser");
	}

	public String getWfLdaWoolNumber() {
		return globalCtx.getString("ldaWoolNumber");
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setWfLdaAttnMenuSwitch(String wfLdaAttnMenuSwitch) {
		globalCtx.setString("ldaAttnMenuSwitch", wfLdaAttnMenuSwitch);
	}

	public void setWfLdaClientAccountId(String wfLdaClientAccountId) {
		globalCtx.setString("ldaClientAccountId", wfLdaClientAccountId);
	}

	public void setWfLdaClipCode(String wfLdaClipCode) {
		globalCtx.setString("ldaClipCode", wfLdaClipCode);
	}

	public void setWfLdaCostCentre(String wfLdaCostCentre) {
		globalCtx.setString("ldaCostCentre", wfLdaCostCentre);
	}

	public void setWfLdaCurrentBrokerId(String wfLdaCurrentBrokerId) {
		globalCtx.setString("ldaCurrentBrokerId", wfLdaCurrentBrokerId);
	}

	public void setWfLdaCurrentCentreCode(String wfLdaCurrentCentreCode) {
		globalCtx.setString("ldaCurrentCentreCode", wfLdaCurrentCentreCode);
	}

	public void setWfLdaCurrentStateCode(String wfLdaCurrentStateCode) {
		globalCtx.setString("ldaCurrentStateCode", wfLdaCurrentStateCode);
	}

	public void setWfLdaDefaultBrokerId(String wfLdaDefaultBrokerId) {
		globalCtx.setString("ldaDefaultBrokerId", wfLdaDefaultBrokerId);
	}

	public void setWfLdaDefaultCentreCode(String wfLdaDefaultCentreCode) {
		globalCtx.setString("ldaDefaultCentreCode", wfLdaDefaultCentreCode);
	}

	public void setWfLdaDefaultStateCode(String wfLdaDefaultStateCode) {
		globalCtx.setString("ldaDefaultStateCode", wfLdaDefaultStateCode);
	}

	public void setWfLdaDftSaleNbrId(String wfLdaDftSaleNbrId) {
		globalCtx.setString("ldaDftSaleNbrId", wfLdaDftSaleNbrId);
	}

	public void setWfLdaDftSaleNbrSlgCtr(String wfLdaDftSaleNbrSlgCtr) {
		globalCtx.setString("ldaDftSaleNbrSlgCtr", wfLdaDftSaleNbrSlgCtr);
	}

	public void setWfLdaDftSaleNbrStgCtr(String wfLdaDftSaleNbrStgCtr) {
		globalCtx.setString("ldaDftSaleNbrStgCtr", wfLdaDftSaleNbrStgCtr);
	}

	public void setWfLdaDftSaleSeasonCent(String wfLdaDftSaleSeasonCent) {
		globalCtx.setString("ldaDftSaleSeasonCent", wfLdaDftSaleSeasonCent);
	}

	public void setWfLdaDftSaleSeasonYear(String wfLdaDftSaleSeasonYear) {
		globalCtx.setString("ldaDftSaleSeasonYear", wfLdaDftSaleSeasonYear);
	}

	public void setWfLdaEldersStateCode(String wfLdaEldersStateCode) {
		globalCtx.setString("ldaEldersStateCode", wfLdaEldersStateCode);
	}

	public void setWfLdaEldersStateNumCode(String wfLdaEldersStateNumCode) {
		globalCtx.setString("ldaEldersStateNumCode", wfLdaEldersStateNumCode);
	}

	public void setWfLdaEnvironment(String wfLdaEnvironment) {
		globalCtx.setString("ldaEnvironment", wfLdaEnvironment);
	}

	public void setWfLdaFolioNumber(String wfLdaFolioNumber) {
		globalCtx.setString("ldaFolioNumber", wfLdaFolioNumber);
	}

	public void setWfLdaHomeCostCentre(String wfLdaHomeCostCentre) {
		globalCtx.setString("ldaHomeCostCentre", wfLdaHomeCostCentre);
	}

	public void setWfLdaLotNumber(String wfLdaLotNumber) {
		globalCtx.setString("ldaLotNumber", wfLdaLotNumber);
	}

	public void setWfLdaNetworkSystemName(String wfLdaNetworkSystemName) {
		globalCtx.setString("ldaNetworkSystemName", wfLdaNetworkSystemName);
	}

	public void setWfLdaOutputCostCentre(String wfLdaOutputCostCentre) {
		globalCtx.setString("ldaOutputCostCentre", wfLdaOutputCostCentre);
	}

	public void setWfLdaPrinter(String wfLdaPrinter) {
		globalCtx.setString("ldaPrinter", wfLdaPrinter);
	}

	public void setWfLdaProcSaleNbrId(String wfLdaProcSaleNbrId) {
		globalCtx.setString("ldaProcSaleNbrId", wfLdaProcSaleNbrId);
	}

	public void setWfLdaProcSaleNbrSlgCtr(String wfLdaProcSaleNbrSlgCtr) {
		globalCtx.setString("ldaProcSaleNbrSlgCtr", wfLdaProcSaleNbrSlgCtr);
	}

	public void setWfLdaProcSaleNbrStgCtr(String wfLdaProcSaleNbrStgCtr) {
		globalCtx.setString("ldaProcSaleNbrStgCtr", wfLdaProcSaleNbrStgCtr);
	}

	public void setWfLdaProcSaleSeasonCent(String wfLdaProcSaleSeasonCent) {
		globalCtx.setString("ldaProcSaleSeasonCent", wfLdaProcSaleSeasonCent);
	}

	public void setWfLdaProcSaleSeasonYear(String wfLdaProcSaleSeasonYear) {
		globalCtx.setString("ldaProcSaleSeasonYear", wfLdaProcSaleSeasonYear);
	}

	public void setWfLdaSecurityProfile(String wfLdaSecurityProfile) {
		globalCtx.setString("ldaSecurityProfile", wfLdaSecurityProfile);
	}

	public void setWfLdaStateCostCentre(String wfLdaStateCostCentre) {
		globalCtx.setString("ldaStateCostCentre", wfLdaStateCostCentre);
	}

	public void setWfLdaStateHighSpeedPrtr(String wfLdaStateHighSpeedPrtr) {
		globalCtx.setString("ldaStateHighSpeedPrtr", wfLdaStateHighSpeedPrtr);
	}

	public void setWfLdaTerminal(String wfLdaTerminal) {
		globalCtx.setString("ldaTerminal", wfLdaTerminal);
	}

	public void setWfLdaUser(String wfLdaUser) {
		globalCtx.setString("ldaUser", wfLdaUser);
	}

	public void setWfLdaWoolNumber(String wfLdaWoolNumber) {
		globalCtx.setString("ldaWoolNumber", wfLdaWoolNumber);
	}

}
