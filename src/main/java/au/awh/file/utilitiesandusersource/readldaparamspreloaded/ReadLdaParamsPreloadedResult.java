package au.awh.file.utilitiesandusersource.readldaparamspreloaded;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ReadLdaParamsPreloaded ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ReadLdaParamsPreloadedResult implements Serializable
{
	private static final long serialVersionUID = 6178596058901528630L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
