package au.awh.file.utilitiesandusersource.readldaparamspreloaded;

// ExecuteInternalFunction.kt File=UtilitiesAndUserSource () Function=readLdaParamsPreloaded (1104826) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesandusersource.readldausersource.ReadLdaUserSourceService;
// imports for DTO
import au.awh.file.utilitiesandusersource.readldausersource.ReadLdaUserSourceDTO;
// imports for Enums
import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// imports for Parameters
import au.awh.file.utilitiesandusersource.readldausersource.ReadLdaUserSourceParams;
// imports for Results
import au.awh.file.utilitiesandusersource.readldausersource.ReadLdaUserSourceResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'Read LDA-Params PreLoaded' (file 'Utilities & User Source' ()
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ReadLdaParamsPreloadedService extends AbstractService<ReadLdaParamsPreloadedService, ReadLdaParamsPreloadedDTO>
{
	private final Step execute = define("execute", ReadLdaParamsPreloadedParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private ReadLdaUserSourceService readLdaUserSourceService;

	//private final Step serviceReadLdaUserSource = define("serviceReadLdaUserSource",ReadLdaUserSourceResult.class, this::processServiceReadLdaUserSource);
	
	@Autowired
	public ReadLdaParamsPreloadedService() {
		super(ReadLdaParamsPreloadedService.class, ReadLdaParamsPreloadedDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ReadLdaParamsPreloadedParams params) throws ServiceException {
		ReadLdaParamsPreloadedDTO dto = new ReadLdaParamsPreloadedDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ReadLdaParamsPreloadedDTO dto, ReadLdaParamsPreloadedParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ReadLdaUserSourceParams readLdaUserSourceParams = null;
		ReadLdaUserSourceResult readLdaUserSourceResult = null;
		// DEBUG genFunctionCall BEGIN 1000039 ACT Read LDA User Source - Utilities & User Source  *
		readLdaUserSourceParams = new ReadLdaUserSourceParams();
		readLdaUserSourceResult = new ReadLdaUserSourceResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, readLdaUserSourceParams);
		stepResult = readLdaUserSourceService.execute(readLdaUserSourceParams);
		readLdaUserSourceResult = (ReadLdaUserSourceResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(readLdaUserSourceResult.get_SysReturnCode());
		dto.setWfLdaTerminal(readLdaUserSourceResult.getLdaTerminal());
		dto.setWfLdaUser(readLdaUserSourceResult.getLdaUser());
		dto.setWfLdaCostCentre(readLdaUserSourceResult.getLdaCostCentre());
		dto.setWfLdaSecurityProfile(readLdaUserSourceResult.getLdaSecurityProfile());
		dto.setWfLdaNetworkSystemName(readLdaUserSourceResult.getLdaNetworkSystemName());
		dto.setWfLdaStateCostCentre(readLdaUserSourceResult.getLdaStateCostCentre());
		dto.setWfLdaPrinter(readLdaUserSourceResult.getLdaPrinter());
		dto.setWfLdaStateHighSpeedPrtr(readLdaUserSourceResult.getLdaStateHighSpeedPrtr());
		dto.setWfLdaEnvironment(readLdaUserSourceResult.getLdaEnvironment().getCode());
		dto.setWfLdaEldersStateCode(readLdaUserSourceResult.getLdaEldersStateCode());
		dto.setWfLdaEldersStateNumCode(readLdaUserSourceResult.getLdaEldersStateNumCode());
		dto.setWfLdaOutputCostCentre(readLdaUserSourceResult.getLdaOutputCostCentre());
		dto.setWfLdaHomeCostCentre(readLdaUserSourceResult.getLdaHomeCostCentre());
		dto.setWfLdaAttnMenuSwitch(readLdaUserSourceResult.getLdaAttnMenuSwitch());
		dto.setWfLdaCurrentBrokerId(readLdaUserSourceResult.getLdaCurrentBrokerId());
		dto.setWfLdaCurrentStateCode(readLdaUserSourceResult.getLdaCurrentStateCode().getCode());
		dto.setWfLdaCurrentCentreCode(readLdaUserSourceResult.getLdaCurrentCentreCode());
		dto.setWfLdaDefaultBrokerId(readLdaUserSourceResult.getLdaDefaultBrokerId());
		dto.setWfLdaDefaultStateCode(readLdaUserSourceResult.getLdaDefaultStateCode().getCode());
		dto.setWfLdaDefaultCentreCode(readLdaUserSourceResult.getLdaDefaultCentreCode());
		dto.setWfLdaDftSaleNbrSlgCtr(readLdaUserSourceResult.getLdaDftSaleNbrSlgCtr());
		dto.setWfLdaDftSaleNbrId(readLdaUserSourceResult.getLdaDftSaleNbrId());
		dto.setWfLdaDftSaleNbrStgCtr(readLdaUserSourceResult.getLdaDftSaleNbrStgCtr());
		dto.setWfLdaProcSaleNbrSlgCtr(readLdaUserSourceResult.getLdaProcSaleNbrSlgCtr());
		dto.setWfLdaProcSaleNbrId(readLdaUserSourceResult.getLdaProcSaleNbrId());
		dto.setWfLdaProcSaleNbrStgCtr(readLdaUserSourceResult.getLdaProcSaleNbrStgCtr());
		dto.setWfLdaClientAccountId(readLdaUserSourceResult.getLdaClientAccountId());
		dto.setWfLdaWoolNumber(readLdaUserSourceResult.getLdaWoolNumber());
		dto.setWfLdaClipCode(readLdaUserSourceResult.getLdaClipCode());
		dto.setWfLdaLotNumber(readLdaUserSourceResult.getLdaLotNumber());
		dto.setWfLdaFolioNumber(readLdaUserSourceResult.getLdaFolioNumber());
		dto.setWfLdaDftSaleSeasonCent(readLdaUserSourceResult.getLdaDftSaleSeasonCent());
		dto.setWfLdaDftSaleSeasonYear(readLdaUserSourceResult.getLdaDftSaleSeasonYear());
		dto.setWfLdaProcSaleSeasonCent(readLdaUserSourceResult.getLdaProcSaleSeasonCent());
		dto.setWfLdaProcSaleSeasonYear(readLdaUserSourceResult.getLdaProcSaleSeasonYear());
		// DEBUG genFunctionCall END

		ReadLdaParamsPreloadedResult result = new ReadLdaParamsPreloadedResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ReadLdaUserSourceService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceReadLdaUserSource(ReadLdaParamsPreloadedDTO dto, ReadLdaUserSourceParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ReadLdaParamsPreloadedParams readLdaParamsPreloadedParams = new ReadLdaParamsPreloadedParams();
//        BeanUtils.copyProperties(dto, readLdaParamsPreloadedParams);
//        stepResult = StepResult.callService(ReadLdaParamsPreloadedService.class, readLdaParamsPreloadedParams);
//
//        return stepResult;
//    }
//
}
