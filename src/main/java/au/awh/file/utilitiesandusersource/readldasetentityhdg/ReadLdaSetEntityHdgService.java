package au.awh.file.utilitiesandusersource.readldasetentityhdg;

// ExecuteInternalFunction.kt File=UtilitiesAndUserSource () Function=readLdaSetEntityHdg (1109741) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesandusersource.readldaparamspreloaded.ReadLdaParamsPreloadedService;
// imports for DTO
import au.awh.file.utilitiesandusersource.readldaparamspreloaded.ReadLdaParamsPreloadedDTO;
// imports for Enums
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// imports for Parameters
import au.awh.file.utilitiesandusersource.readldaparamspreloaded.ReadLdaParamsPreloadedParams;
// imports for Results
import au.awh.file.utilitiesandusersource.readldaparamspreloaded.ReadLdaParamsPreloadedResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'Read LDA/Set Entity Hdg' (file 'Utilities & User Source' ()
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ReadLdaSetEntityHdgService extends AbstractService<ReadLdaSetEntityHdgService, ReadLdaSetEntityHdgDTO>
{
	private final Step execute = define("execute", ReadLdaSetEntityHdgParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private ReadLdaParamsPreloadedService readLdaParamsPreloadedService;

	//private final Step serviceReadLdaParamsPreloaded = define("serviceReadLdaParamsPreloaded",ReadLdaParamsPreloadedResult.class, this::processServiceReadLdaParamsPreloaded);
	
	@Autowired
	public ReadLdaSetEntityHdgService() {
		super(ReadLdaSetEntityHdgService.class, ReadLdaSetEntityHdgDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ReadLdaSetEntityHdgParams params) throws ServiceException {
		ReadLdaSetEntityHdgDTO dto = new ReadLdaSetEntityHdgDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ReadLdaSetEntityHdgDTO dto, ReadLdaSetEntityHdgParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ReadLdaParamsPreloadedParams readLdaParamsPreloadedParams = null;
		ReadLdaParamsPreloadedResult readLdaParamsPreloadedResult = null;
		// DEBUG genFunctionCall BEGIN 1000014 ACT Read LDA-Params PreLoaded - Utilities & User Source  *
		readLdaParamsPreloadedParams = new ReadLdaParamsPreloadedParams();
		readLdaParamsPreloadedResult = new ReadLdaParamsPreloadedResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, readLdaParamsPreloadedParams);
		stepResult = readLdaParamsPreloadedService.execute(readLdaParamsPreloadedParams);
		readLdaParamsPreloadedResult = (ReadLdaParamsPreloadedResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(readLdaParamsPreloadedResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		// 
		// DEBUG genFunctionCall BEGIN 1000006 ACT WRK.Broker Id = WRK.LDA Current Broker Id
		dto.setWfBrokerId(dto.getWfLdaCurrentBrokerId());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000010 ACT WRK.State Code = WRK.LDA Current State Code
		dto.setWfStateCode(dto.getWfLdaCurrentStateCode());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000002 ACT WRK.Centre Code = WRK.LDA Current Centre Code
		dto.setWfCentreCode(dto.getWfLdaCurrentCentreCode());
		// DEBUG genFunctionCall END
		// 
		// DEBUG genFunctionCall BEGIN 1000016 ACT WRK.Sale Company Id = WRK.LDA Current Broker Id
		dto.setWfSaleCompanyId(dto.getWfLdaCurrentBrokerId());
		// DEBUG genFunctionCall END

		ReadLdaSetEntityHdgResult result = new ReadLdaSetEntityHdgResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ReadLdaParamsPreloadedService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceReadLdaParamsPreloaded(ReadLdaSetEntityHdgDTO dto, ReadLdaParamsPreloadedParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ReadLdaSetEntityHdgParams readLdaSetEntityHdgParams = new ReadLdaSetEntityHdgParams();
//        BeanUtils.copyProperties(dto, readLdaSetEntityHdgParams);
//        stepResult = StepResult.callService(ReadLdaSetEntityHdgService.class, readLdaSetEntityHdgParams);
//
//        return stepResult;
//    }
//
}
