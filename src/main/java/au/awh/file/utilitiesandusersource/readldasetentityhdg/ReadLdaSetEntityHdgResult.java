package au.awh.file.utilitiesandusersource.readldasetentityhdg;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ReadLdaSetEntityHdg ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ReadLdaSetEntityHdgResult implements Serializable
{
	private static final long serialVersionUID = 6799073232234367580L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
