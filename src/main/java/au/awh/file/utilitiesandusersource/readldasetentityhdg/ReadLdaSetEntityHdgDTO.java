package au.awh.file.utilitiesandusersource.readldasetentityhdg;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.GlobalContext;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ReadLdaSetEntityHdgDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Autowired
	private GlobalContext globalCtx;

	private ReturnCodeEnum _sysReturnCode;



	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getWfBrokerId() {
		return globalCtx.getString("brokerId");
	}

	public String getWfCentreCode() {
		return globalCtx.getString("centreCode");
	}

	public String getWfLdaCurrentBrokerId() {
		return globalCtx.getString("ldaCurrentBrokerId");
	}

	public String getWfLdaCurrentCentreCode() {
		return globalCtx.getString("ldaCurrentCentreCode");
	}

	public String getWfLdaCurrentStateCode() {
		return globalCtx.getString("ldaCurrentStateCode");
	}

	public String getWfSaleCompanyId() {
		return globalCtx.getString("saleCompanyId");
	}

	public String getWfStateCode() {
		return globalCtx.getString("stateCode");
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setWfBrokerId(String wfBrokerId) {
		globalCtx.setString("brokerId", wfBrokerId);
	}

	public void setWfCentreCode(String wfCentreCode) {
		globalCtx.setString("centreCode", wfCentreCode);
	}

	public void setWfLdaCurrentBrokerId(String wfLdaCurrentBrokerId) {
		globalCtx.setString("ldaCurrentBrokerId", wfLdaCurrentBrokerId);
	}

	public void setWfLdaCurrentCentreCode(String wfLdaCurrentCentreCode) {
		globalCtx.setString("ldaCurrentCentreCode", wfLdaCurrentCentreCode);
	}

	public void setWfLdaCurrentStateCode(String wfLdaCurrentStateCode) {
		globalCtx.setString("ldaCurrentStateCode", wfLdaCurrentStateCode);
	}

	public void setWfSaleCompanyId(String wfSaleCompanyId) {
		globalCtx.setString("saleCompanyId", wfSaleCompanyId);
	}

	public void setWfStateCode(String wfStateCode) {
		globalCtx.setString("stateCode", wfStateCode);
	}

}
