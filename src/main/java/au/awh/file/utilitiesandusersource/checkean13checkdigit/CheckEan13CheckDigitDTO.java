package au.awh.file.utilitiesandusersource.checkean13checkdigit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CheckEan13CheckDigitDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String actualCheckDigit;
	private String checkDigit;
	private long ean13;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getActualCheckDigit() {
		return actualCheckDigit;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public long getEan13() {
		return ean13;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setActualCheckDigit(String actualCheckDigit) {
		this.actualCheckDigit = actualCheckDigit;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public void setEan13(long ean13) {
		this.ean13 = ean13;
	}

}
