package au.awh.file.utilitiesandusersource.checkean13checkdigit;

// Function Stub for EXCUSRSRC 1432779
// UtilitiesAndUserSource.checkEan13CheckDigit

// Parameters:
// I Field 36544 ean13 long(13) "EAN 13"
// O Field 14195 checkDigit String(1) "Check Digit"
// O Field 36562 actualCheckDigit String(1) Ref 14195 checkDigit "Actual Check Digit"

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;


/**
 * EXCUSRSRC Service for 'Check Ean 13 Check Digit' (file 'Utilities & User Source'
 *
 * @author X2EGenerator  FunctionStub.kt
 */
@Service
public class CheckEan13CheckDigitService {

	public StepResult execute(CheckEan13CheckDigitParams checkEan13CheckDigitParams) {
		// TODO: Code generation is not currently supported for this function
		return NO_ACTION;
	}
}
