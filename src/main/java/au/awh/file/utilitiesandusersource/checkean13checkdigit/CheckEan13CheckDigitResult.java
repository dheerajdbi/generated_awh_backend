package au.awh.file.utilitiesandusersource.checkean13checkdigit;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CheckEan13CheckDigit (WSSYSBQUFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CheckEan13CheckDigitResult implements Serializable
{
	private static final long serialVersionUID = -5889535060164066458L;

	private ReturnCodeEnum _sysReturnCode;
	private String checkDigit = ""; // String 14195
	private String actualCheckDigit = ""; // String 36562

	public String getCheckDigit() {
		return checkDigit;
	}
	
	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}
	
	public String getActualCheckDigit() {
		return actualCheckDigit;
	}
	
	public void setActualCheckDigit(String actualCheckDigit) {
		this.actualCheckDigit = actualCheckDigit;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
