package au.awh.file.utilitiesandusersource.checkean13checkdigit;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CheckEan13CheckDigit (WSSYSBQUFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CheckEan13CheckDigitParams implements Serializable
{
	private static final long serialVersionUID = -5292584579445798276L;

    private long ean13 = 0L;

	public long getEan13() {
		return ean13;
	}
	
	public void setEan13(long ean13) {
		this.ean13 = ean13;
	}
	
	public void setEan13(String ean13) {
		setEan13(Long.parseLong(ean13));
	}
}
