package au.awh.file.utilitiesandusersource.readldausersource;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 35
import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ReadLdaUserSource (WSREADLDA).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ReadLdaUserSourceResult implements Serializable
{
	private static final long serialVersionUID = 2880480610257555349L;

	private ReturnCodeEnum _sysReturnCode;
	private String ldaTerminal = ""; // String 4335
	private String ldaUser = ""; // String 4345
	private String ldaCostCentre = ""; // String 4346
	private String ldaSecurityProfile = ""; // String 4336
	private String ldaNetworkSystemName = ""; // String 4337
	private String ldaStateCostCentre = ""; // String 4347
	private String ldaPrinter = ""; // String 4338
	private String ldaStateHighSpeedPrtr = ""; // String 4339
	private LdaEnvironmentEnum ldaEnvironment = null; // LdaEnvironmentEnum 4340
	private String ldaEldersStateCode = ""; // String 4341
	private String ldaEldersStateNumCode = ""; // String 4342
	private String ldaOutputCostCentre = ""; // String 4348
	private String ldaHomeCostCentre = ""; // String 4349
	private String ldaAttnMenuSwitch = ""; // String 4343
	private String ldaCurrentBrokerId = ""; // String 4389
	private StateCodeKeyEnum ldaCurrentStateCode = null; // StateCodeKeyEnum 4390
	private String ldaCurrentCentreCode = ""; // String 4391
	private String ldaDefaultBrokerId = ""; // String 4366
	private StateCodeKeyEnum ldaDefaultStateCode = null; // StateCodeKeyEnum 4367
	private String ldaDefaultCentreCode = ""; // String 4368
	private String ldaDftSaleNbrSlgCtr = ""; // String 5896
	private String ldaDftSaleNbrId = ""; // String 5897
	private String ldaDftSaleNbrStgCtr = ""; // String 5898
	private String ldaProcSaleNbrSlgCtr = ""; // String 5899
	private String ldaProcSaleNbrId = ""; // String 5900
	private String ldaProcSaleNbrStgCtr = ""; // String 5901
	private String ldaClientAccountId = ""; // String 5908
	private String ldaWoolNumber = ""; // String 5902
	private String ldaClipCode = ""; // String 5903
	private String ldaLotNumber = ""; // String 5904
	private String ldaFolioNumber = ""; // String 5905
	private String ldaDftSaleSeasonCent = ""; // String 5909
	private String ldaDftSaleSeasonYear = ""; // String 5906
	private String ldaProcSaleSeasonCent = ""; // String 5910
	private String ldaProcSaleSeasonYear = ""; // String 5907

	public String getLdaTerminal() {
		return ldaTerminal;
	}
	
	public void setLdaTerminal(String ldaTerminal) {
		this.ldaTerminal = ldaTerminal;
	}
	
	public String getLdaUser() {
		return ldaUser;
	}
	
	public void setLdaUser(String ldaUser) {
		this.ldaUser = ldaUser;
	}
	
	public String getLdaCostCentre() {
		return ldaCostCentre;
	}
	
	public void setLdaCostCentre(String ldaCostCentre) {
		this.ldaCostCentre = ldaCostCentre;
	}
	
	public String getLdaSecurityProfile() {
		return ldaSecurityProfile;
	}
	
	public void setLdaSecurityProfile(String ldaSecurityProfile) {
		this.ldaSecurityProfile = ldaSecurityProfile;
	}
	
	public String getLdaNetworkSystemName() {
		return ldaNetworkSystemName;
	}
	
	public void setLdaNetworkSystemName(String ldaNetworkSystemName) {
		this.ldaNetworkSystemName = ldaNetworkSystemName;
	}
	
	public String getLdaStateCostCentre() {
		return ldaStateCostCentre;
	}
	
	public void setLdaStateCostCentre(String ldaStateCostCentre) {
		this.ldaStateCostCentre = ldaStateCostCentre;
	}
	
	public String getLdaPrinter() {
		return ldaPrinter;
	}
	
	public void setLdaPrinter(String ldaPrinter) {
		this.ldaPrinter = ldaPrinter;
	}
	
	public String getLdaStateHighSpeedPrtr() {
		return ldaStateHighSpeedPrtr;
	}
	
	public void setLdaStateHighSpeedPrtr(String ldaStateHighSpeedPrtr) {
		this.ldaStateHighSpeedPrtr = ldaStateHighSpeedPrtr;
	}
	
	public LdaEnvironmentEnum getLdaEnvironment() {
		return ldaEnvironment;
	}
	
	public void setLdaEnvironment(LdaEnvironmentEnum ldaEnvironment) {
		this.ldaEnvironment = ldaEnvironment;
	}
	
	public void setLdaEnvironment(String ldaEnvironment) {
		setLdaEnvironment(LdaEnvironmentEnum.valueOf(ldaEnvironment));
	}
	
	public String getLdaEldersStateCode() {
		return ldaEldersStateCode;
	}
	
	public void setLdaEldersStateCode(String ldaEldersStateCode) {
		this.ldaEldersStateCode = ldaEldersStateCode;
	}
	
	public String getLdaEldersStateNumCode() {
		return ldaEldersStateNumCode;
	}
	
	public void setLdaEldersStateNumCode(String ldaEldersStateNumCode) {
		this.ldaEldersStateNumCode = ldaEldersStateNumCode;
	}
	
	public String getLdaOutputCostCentre() {
		return ldaOutputCostCentre;
	}
	
	public void setLdaOutputCostCentre(String ldaOutputCostCentre) {
		this.ldaOutputCostCentre = ldaOutputCostCentre;
	}
	
	public String getLdaHomeCostCentre() {
		return ldaHomeCostCentre;
	}
	
	public void setLdaHomeCostCentre(String ldaHomeCostCentre) {
		this.ldaHomeCostCentre = ldaHomeCostCentre;
	}
	
	public String getLdaAttnMenuSwitch() {
		return ldaAttnMenuSwitch;
	}
	
	public void setLdaAttnMenuSwitch(String ldaAttnMenuSwitch) {
		this.ldaAttnMenuSwitch = ldaAttnMenuSwitch;
	}
	
	public String getLdaCurrentBrokerId() {
		return ldaCurrentBrokerId;
	}
	
	public void setLdaCurrentBrokerId(String ldaCurrentBrokerId) {
		this.ldaCurrentBrokerId = ldaCurrentBrokerId;
	}
	
	public StateCodeKeyEnum getLdaCurrentStateCode() {
		return ldaCurrentStateCode;
	}
	
	public void setLdaCurrentStateCode(StateCodeKeyEnum ldaCurrentStateCode) {
		this.ldaCurrentStateCode = ldaCurrentStateCode;
	}
	
	public void setLdaCurrentStateCode(String ldaCurrentStateCode) {
		setLdaCurrentStateCode(StateCodeKeyEnum.valueOf(ldaCurrentStateCode));
	}
	
	public String getLdaCurrentCentreCode() {
		return ldaCurrentCentreCode;
	}
	
	public void setLdaCurrentCentreCode(String ldaCurrentCentreCode) {
		this.ldaCurrentCentreCode = ldaCurrentCentreCode;
	}
	
	public String getLdaDefaultBrokerId() {
		return ldaDefaultBrokerId;
	}
	
	public void setLdaDefaultBrokerId(String ldaDefaultBrokerId) {
		this.ldaDefaultBrokerId = ldaDefaultBrokerId;
	}
	
	public StateCodeKeyEnum getLdaDefaultStateCode() {
		return ldaDefaultStateCode;
	}
	
	public void setLdaDefaultStateCode(StateCodeKeyEnum ldaDefaultStateCode) {
		this.ldaDefaultStateCode = ldaDefaultStateCode;
	}
	
	public void setLdaDefaultStateCode(String ldaDefaultStateCode) {
		setLdaDefaultStateCode(StateCodeKeyEnum.valueOf(ldaDefaultStateCode));
	}
	
	public String getLdaDefaultCentreCode() {
		return ldaDefaultCentreCode;
	}
	
	public void setLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		this.ldaDefaultCentreCode = ldaDefaultCentreCode;
	}
	
	public String getLdaDftSaleNbrSlgCtr() {
		return ldaDftSaleNbrSlgCtr;
	}
	
	public void setLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		this.ldaDftSaleNbrSlgCtr = ldaDftSaleNbrSlgCtr;
	}
	
	public String getLdaDftSaleNbrId() {
		return ldaDftSaleNbrId;
	}
	
	public void setLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		this.ldaDftSaleNbrId = ldaDftSaleNbrId;
	}
	
	public String getLdaDftSaleNbrStgCtr() {
		return ldaDftSaleNbrStgCtr;
	}
	
	public void setLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		this.ldaDftSaleNbrStgCtr = ldaDftSaleNbrStgCtr;
	}
	
	public String getLdaProcSaleNbrSlgCtr() {
		return ldaProcSaleNbrSlgCtr;
	}
	
	public void setLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		this.ldaProcSaleNbrSlgCtr = ldaProcSaleNbrSlgCtr;
	}
	
	public String getLdaProcSaleNbrId() {
		return ldaProcSaleNbrId;
	}
	
	public void setLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		this.ldaProcSaleNbrId = ldaProcSaleNbrId;
	}
	
	public String getLdaProcSaleNbrStgCtr() {
		return ldaProcSaleNbrStgCtr;
	}
	
	public void setLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		this.ldaProcSaleNbrStgCtr = ldaProcSaleNbrStgCtr;
	}
	
	public String getLdaClientAccountId() {
		return ldaClientAccountId;
	}
	
	public void setLdaClientAccountId(String ldaClientAccountId) {
		this.ldaClientAccountId = ldaClientAccountId;
	}
	
	public String getLdaWoolNumber() {
		return ldaWoolNumber;
	}
	
	public void setLdaWoolNumber(String ldaWoolNumber) {
		this.ldaWoolNumber = ldaWoolNumber;
	}
	
	public String getLdaClipCode() {
		return ldaClipCode;
	}
	
	public void setLdaClipCode(String ldaClipCode) {
		this.ldaClipCode = ldaClipCode;
	}
	
	public String getLdaLotNumber() {
		return ldaLotNumber;
	}
	
	public void setLdaLotNumber(String ldaLotNumber) {
		this.ldaLotNumber = ldaLotNumber;
	}
	
	public String getLdaFolioNumber() {
		return ldaFolioNumber;
	}
	
	public void setLdaFolioNumber(String ldaFolioNumber) {
		this.ldaFolioNumber = ldaFolioNumber;
	}
	
	public String getLdaDftSaleSeasonCent() {
		return ldaDftSaleSeasonCent;
	}
	
	public void setLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		this.ldaDftSaleSeasonCent = ldaDftSaleSeasonCent;
	}
	
	public String getLdaDftSaleSeasonYear() {
		return ldaDftSaleSeasonYear;
	}
	
	public void setLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		this.ldaDftSaleSeasonYear = ldaDftSaleSeasonYear;
	}
	
	public String getLdaProcSaleSeasonCent() {
		return ldaProcSaleSeasonCent;
	}
	
	public void setLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		this.ldaProcSaleSeasonCent = ldaProcSaleSeasonCent;
	}
	
	public String getLdaProcSaleSeasonYear() {
		return ldaProcSaleSeasonYear;
	}
	
	public void setLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		this.ldaProcSaleSeasonYear = ldaProcSaleSeasonYear;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
