package au.awh.file.utilitiesandusersource.readldausersource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeKeyEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ReadLdaUserSourceDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private LdaEnvironmentEnum ldaEnvironment;
	private StateCodeKeyEnum ldaCurrentStateCode;
	private StateCodeKeyEnum ldaDefaultStateCode;
	private String ldaAttnMenuSwitch;
	private String ldaClientAccountId;
	private String ldaClipCode;
	private String ldaCostCentre;
	private String ldaCurrentBrokerId;
	private String ldaCurrentCentreCode;
	private String ldaDefaultBrokerId;
	private String ldaDefaultCentreCode;
	private String ldaDftSaleNbrId;
	private String ldaDftSaleNbrSlgCtr;
	private String ldaDftSaleNbrStgCtr;
	private String ldaDftSaleSeasonCent;
	private String ldaDftSaleSeasonYear;
	private String ldaEldersStateCode;
	private String ldaEldersStateNumCode;
	private String ldaFolioNumber;
	private String ldaHomeCostCentre;
	private String ldaLotNumber;
	private String ldaNetworkSystemName;
	private String ldaOutputCostCentre;
	private String ldaPrinter;
	private String ldaProcSaleNbrId;
	private String ldaProcSaleNbrSlgCtr;
	private String ldaProcSaleNbrStgCtr;
	private String ldaProcSaleSeasonCent;
	private String ldaProcSaleSeasonYear;
	private String ldaSecurityProfile;
	private String ldaStateCostCentre;
	private String ldaStateHighSpeedPrtr;
	private String ldaTerminal;
	private String ldaUser;
	private String ldaWoolNumber;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getLdaAttnMenuSwitch() {
		return ldaAttnMenuSwitch;
	}

	public String getLdaClientAccountId() {
		return ldaClientAccountId;
	}

	public String getLdaClipCode() {
		return ldaClipCode;
	}

	public String getLdaCostCentre() {
		return ldaCostCentre;
	}

	public String getLdaCurrentBrokerId() {
		return ldaCurrentBrokerId;
	}

	public String getLdaCurrentCentreCode() {
		return ldaCurrentCentreCode;
	}

	public StateCodeKeyEnum getLdaCurrentStateCode() {
		return ldaCurrentStateCode;
	}

	public String getLdaDefaultBrokerId() {
		return ldaDefaultBrokerId;
	}

	public String getLdaDefaultCentreCode() {
		return ldaDefaultCentreCode;
	}

	public StateCodeKeyEnum getLdaDefaultStateCode() {
		return ldaDefaultStateCode;
	}

	public String getLdaDftSaleNbrId() {
		return ldaDftSaleNbrId;
	}

	public String getLdaDftSaleNbrSlgCtr() {
		return ldaDftSaleNbrSlgCtr;
	}

	public String getLdaDftSaleNbrStgCtr() {
		return ldaDftSaleNbrStgCtr;
	}

	public String getLdaDftSaleSeasonCent() {
		return ldaDftSaleSeasonCent;
	}

	public String getLdaDftSaleSeasonYear() {
		return ldaDftSaleSeasonYear;
	}

	public String getLdaEldersStateCode() {
		return ldaEldersStateCode;
	}

	public String getLdaEldersStateNumCode() {
		return ldaEldersStateNumCode;
	}

	public LdaEnvironmentEnum getLdaEnvironment() {
		return ldaEnvironment;
	}

	public String getLdaFolioNumber() {
		return ldaFolioNumber;
	}

	public String getLdaHomeCostCentre() {
		return ldaHomeCostCentre;
	}

	public String getLdaLotNumber() {
		return ldaLotNumber;
	}

	public String getLdaNetworkSystemName() {
		return ldaNetworkSystemName;
	}

	public String getLdaOutputCostCentre() {
		return ldaOutputCostCentre;
	}

	public String getLdaPrinter() {
		return ldaPrinter;
	}

	public String getLdaProcSaleNbrId() {
		return ldaProcSaleNbrId;
	}

	public String getLdaProcSaleNbrSlgCtr() {
		return ldaProcSaleNbrSlgCtr;
	}

	public String getLdaProcSaleNbrStgCtr() {
		return ldaProcSaleNbrStgCtr;
	}

	public String getLdaProcSaleSeasonCent() {
		return ldaProcSaleSeasonCent;
	}

	public String getLdaProcSaleSeasonYear() {
		return ldaProcSaleSeasonYear;
	}

	public String getLdaSecurityProfile() {
		return ldaSecurityProfile;
	}

	public String getLdaStateCostCentre() {
		return ldaStateCostCentre;
	}

	public String getLdaStateHighSpeedPrtr() {
		return ldaStateHighSpeedPrtr;
	}

	public String getLdaTerminal() {
		return ldaTerminal;
	}

	public String getLdaUser() {
		return ldaUser;
	}

	public String getLdaWoolNumber() {
		return ldaWoolNumber;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setLdaAttnMenuSwitch(String ldaAttnMenuSwitch) {
		this.ldaAttnMenuSwitch = ldaAttnMenuSwitch;
	}

	public void setLdaClientAccountId(String ldaClientAccountId) {
		this.ldaClientAccountId = ldaClientAccountId;
	}

	public void setLdaClipCode(String ldaClipCode) {
		this.ldaClipCode = ldaClipCode;
	}

	public void setLdaCostCentre(String ldaCostCentre) {
		this.ldaCostCentre = ldaCostCentre;
	}

	public void setLdaCurrentBrokerId(String ldaCurrentBrokerId) {
		this.ldaCurrentBrokerId = ldaCurrentBrokerId;
	}

	public void setLdaCurrentCentreCode(String ldaCurrentCentreCode) {
		this.ldaCurrentCentreCode = ldaCurrentCentreCode;
	}

	public void setLdaCurrentStateCode(StateCodeKeyEnum ldaCurrentStateCode) {
		this.ldaCurrentStateCode = ldaCurrentStateCode;
	}

	public void setLdaDefaultBrokerId(String ldaDefaultBrokerId) {
		this.ldaDefaultBrokerId = ldaDefaultBrokerId;
	}

	public void setLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		this.ldaDefaultCentreCode = ldaDefaultCentreCode;
	}

	public void setLdaDefaultStateCode(StateCodeKeyEnum ldaDefaultStateCode) {
		this.ldaDefaultStateCode = ldaDefaultStateCode;
	}

	public void setLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		this.ldaDftSaleNbrId = ldaDftSaleNbrId;
	}

	public void setLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		this.ldaDftSaleNbrSlgCtr = ldaDftSaleNbrSlgCtr;
	}

	public void setLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		this.ldaDftSaleNbrStgCtr = ldaDftSaleNbrStgCtr;
	}

	public void setLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		this.ldaDftSaleSeasonCent = ldaDftSaleSeasonCent;
	}

	public void setLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		this.ldaDftSaleSeasonYear = ldaDftSaleSeasonYear;
	}

	public void setLdaEldersStateCode(String ldaEldersStateCode) {
		this.ldaEldersStateCode = ldaEldersStateCode;
	}

	public void setLdaEldersStateNumCode(String ldaEldersStateNumCode) {
		this.ldaEldersStateNumCode = ldaEldersStateNumCode;
	}

	public void setLdaEnvironment(LdaEnvironmentEnum ldaEnvironment) {
		this.ldaEnvironment = ldaEnvironment;
	}

	public void setLdaFolioNumber(String ldaFolioNumber) {
		this.ldaFolioNumber = ldaFolioNumber;
	}

	public void setLdaHomeCostCentre(String ldaHomeCostCentre) {
		this.ldaHomeCostCentre = ldaHomeCostCentre;
	}

	public void setLdaLotNumber(String ldaLotNumber) {
		this.ldaLotNumber = ldaLotNumber;
	}

	public void setLdaNetworkSystemName(String ldaNetworkSystemName) {
		this.ldaNetworkSystemName = ldaNetworkSystemName;
	}

	public void setLdaOutputCostCentre(String ldaOutputCostCentre) {
		this.ldaOutputCostCentre = ldaOutputCostCentre;
	}

	public void setLdaPrinter(String ldaPrinter) {
		this.ldaPrinter = ldaPrinter;
	}

	public void setLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		this.ldaProcSaleNbrId = ldaProcSaleNbrId;
	}

	public void setLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		this.ldaProcSaleNbrSlgCtr = ldaProcSaleNbrSlgCtr;
	}

	public void setLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		this.ldaProcSaleNbrStgCtr = ldaProcSaleNbrStgCtr;
	}

	public void setLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		this.ldaProcSaleSeasonCent = ldaProcSaleSeasonCent;
	}

	public void setLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		this.ldaProcSaleSeasonYear = ldaProcSaleSeasonYear;
	}

	public void setLdaSecurityProfile(String ldaSecurityProfile) {
		this.ldaSecurityProfile = ldaSecurityProfile;
	}

	public void setLdaStateCostCentre(String ldaStateCostCentre) {
		this.ldaStateCostCentre = ldaStateCostCentre;
	}

	public void setLdaStateHighSpeedPrtr(String ldaStateHighSpeedPrtr) {
		this.ldaStateHighSpeedPrtr = ldaStateHighSpeedPrtr;
	}

	public void setLdaTerminal(String ldaTerminal) {
		this.ldaTerminal = ldaTerminal;
	}

	public void setLdaUser(String ldaUser) {
		this.ldaUser = ldaUser;
	}

	public void setLdaWoolNumber(String ldaWoolNumber) {
		this.ldaWoolNumber = ldaWoolNumber;
	}

}
