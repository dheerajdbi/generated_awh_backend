package au.awh.file.utilitiesandusersource.processeean13;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ProcesseEan13 ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ProcesseEan13Params implements Serializable
{
	private static final long serialVersionUID = 5611925606041866217L;

    private long ean13 = 0L;
    private ErrorProcessingEnum errorProcessing = null;

	public long getEan13() {
		return ean13;
	}
	
	public void setEan13(long ean13) {
		this.ean13 = ean13;
	}
	
	public void setEan13(String ean13) {
		setEan13(Long.parseLong(ean13));
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
}
