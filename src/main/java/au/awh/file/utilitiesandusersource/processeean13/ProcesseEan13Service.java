package au.awh.file.utilitiesandusersource.processeean13;

// ExecuteInternalFunction.kt File=UtilitiesAndUserSource () Function=processeEan13 (1432570) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesandusersource.checkean13checkdigit.CheckEan13CheckDigitService;
// imports for DTO
import au.awh.file.utilitiesandusersource.checkean13checkdigit.CheckEan13CheckDigitDTO;
// imports for Enums
import au.awh.model.Ean13TypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.utilitiesandusersource.checkean13checkdigit.CheckEan13CheckDigitParams;
// imports for Results
import au.awh.file.utilitiesandusersource.checkean13checkdigit.CheckEan13CheckDigitResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ProcessE EAN 13' (file 'Utilities & User Source' ()
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ProcesseEan13Service extends AbstractService<ProcesseEan13Service, ProcesseEan13DTO>
{
	private final Step execute = define("execute", ProcesseEan13Params.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private CheckEan13CheckDigitService checkEan13CheckDigitService;

	//private final Step serviceCheckEan13CheckDigit = define("serviceCheckEan13CheckDigit",CheckEan13CheckDigitResult.class, this::processServiceCheckEan13CheckDigit);
	
	@Autowired
	public ProcesseEan13Service() {
		super(ProcesseEan13Service.class, ProcesseEan13DTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ProcesseEan13Params params) throws ServiceException {
		ProcesseEan13DTO dto = new ProcesseEan13DTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ProcesseEan13DTO dto, ProcesseEan13Params params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		CheckEan13CheckDigitParams checkEan13CheckDigitParams = null;
		CheckEan13CheckDigitResult checkEan13CheckDigitResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT LCL.EAN 13 Alpha = CVTVAR(PAR.EAN 13)
		dto.setLclEan13Alpha(String.format("%013d", dto.getEan13()));
		// DEBUG genFunctionCall END
		if (dto.getEan13() >= 1 && dto.getEan13() <= 999999) {
			// PAR.EAN 13 is Folio
			// DEBUG genFunctionCall BEGIN 1000015 ACT PAR.Folio Number          KEY = SUBSTRING(LCL.EAN 13 Alpha,CON.8,CON.6)
			dto.setFolioNumberKey(StringUtils.substring(dto.getLclEan13Alpha(), (int) 7, 13));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000033 ACT PAR.EAN 13 Type = CND.Folio Number
			dto.setEan13Type(Ean13TypeEnum._FOLIO_NUMBER);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000041 ACT PAR.EAN 13 Surrogate = CND.Not entered
			dto.setEan13Surrogate(0);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000025 ACT PAR.EAN 13 Sample Sequence = CND.Not Entered
			dto.setEan13SampleSequence(0);
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000075 ACT LCL.EAN 13 Sample Seq Alpha = SUBSTRING(LCL.EAN 13 Alpha,CON.1,CON.2)
			dto.setLclEan13SampleSeqAlpha(StringUtils.substring(dto.getLclEan13Alpha(), (int) 0, 2));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000081 ACT LCL.EAN 13 Sample Sequence = CVTVAR(LCL.EAN 13 Sample Seq Alpha)
			dto.setLclEan13SampleSequence(Integer.valueOf(dto.getLclEan13SampleSeqAlpha()));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000051 ACT LCL.EAN 13 Type = SUBSTRING(LCL.EAN 13 Alpha,CON.3,CON.1)
			dto.setLclEan13Type(Ean13TypeEnum.valueOf("_" + StringUtils.substring(dto.getLclEan13Alpha(), (int) 2, 3)));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000089 ACT LCL.EAN 13 Surrogate Alpha = SUBSTRING(LCL.EAN 13 Alpha,CON.4,CON.9)
			dto.setLclEan13SurrogateAlpha(StringUtils.substring(dto.getLclEan13Alpha(), (int) 3, 12));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000065 ACT LCL.EAN 13 Surrogate = CVTVAR(LCL.EAN 13 Surrogate Alpha)
			dto.setLclEan13Surrogate(Integer.valueOf(dto.getLclEan13SurrogateAlpha()));
			// DEBUG genFunctionCall END
			if (ErrorProcessingEnum.isYes(dto.getErrorProcessing())) {
				// PAR.Error Processing is Yes
				// DEBUG genFunctionCall BEGIN 1000260 ACT Check Ean 13 Check Digit - Utilities & User Source  *
				checkEan13CheckDigitParams = new CheckEan13CheckDigitParams();
				checkEan13CheckDigitResult = new CheckEan13CheckDigitResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, checkEan13CheckDigitParams);
				checkEan13CheckDigitParams.setEan13(dto.getEan13());
				stepResult = checkEan13CheckDigitService.execute(checkEan13CheckDigitParams);
				checkEan13CheckDigitResult = (CheckEan13CheckDigitResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(checkEan13CheckDigitResult.get_SysReturnCode());
				dto.setLclCheckDigit(checkEan13CheckDigitResult.getCheckDigit());
				dto.setLclActualCheckDigit(checkEan13CheckDigitResult.getActualCheckDigit());
				// DEBUG genFunctionCall END
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				!dto.getLclCheckDigit().equals(dto.getLclActualCheckDigit())
				// DEBUG ComparatorJavaGenerator END
				) {
					// LCL.Check Digit NE LCL.Actual Check Digit
					// DEBUG genFunctionCall BEGIN 1000269 ACT Send error message - 'EAN Mod 10 Failed'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1432862) EXCINTFUN 1432570 UtilitiesAndUserSource.processeEan13 
					// DEBUG genFunctionCall END
					if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Quit
						// DEBUG genFunctionCall BEGIN 1000275 ACT <-- *QUIT
						// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
						// DEBUG genFunctionCall END
					} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Exit
						// DEBUG genFunctionCall BEGIN 1000279 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
					}
				}
				if (dto.getLclEan13Surrogate() == 0) {
					// LCL.EAN 13 Surrogate is Not entered
					if (ErrorProcessingEnum.isSendMessage(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Send message
						// DEBUG genFunctionCall BEGIN 1000212 ACT Send error message - 'EAN Surrogate Inv'
						// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1432619) EXCINTFUN 1432570 UtilitiesAndUserSource.processeEan13 
						// DEBUG genFunctionCall END
					}
					if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Quit
						// DEBUG genFunctionCall BEGIN 1000220 ACT <-- *QUIT
						// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
						// DEBUG genFunctionCall END
					} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Exit
						// DEBUG genFunctionCall BEGIN 1000224 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
					}
				}
				if (Ean13TypeEnum.isGrabSample(dto.getLclEan13Type())) {
					// LCL.EAN 13 Type is Grab Sample
					if (dto.getLclEan13SampleSequence() == 0) {
						// LCL.EAN 13 Sample Sequence is Not Entered
						if (ErrorProcessingEnum.isSendMessage(dto.getErrorProcessing())) {
							// PAR.Error Processing is *Send message
							// DEBUG genFunctionCall BEGIN 1000380 ACT Send error message - 'EAN Sample Seq Portion Rq'
							// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1432616) EXCINTFUN 1432570 UtilitiesAndUserSource.processeEan13 
							// DEBUG genFunctionCall END
							if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
								// PAR.Error Processing is *Quit
								// DEBUG genFunctionCall BEGIN 1000387 ACT <-- *QUIT
								// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
								// DEBUG genFunctionCall END
							}
						}
					}
					// 
				} else if (Ean13TypeEnum.isLocation(dto.getLclEan13Type())) {
					// LCL.EAN 13 Type is Location
					// 
				} else if (dto.getLclEan13Type() == Ean13TypeEnum._DUMP_SYSTEM) {
					// LCL.EAN 13 Type is Dump System
					if (dto.getLclEan13SampleSequence() >= 00 && dto.getLclEan13SampleSequence() <= 20) {
						// LCL.EAN 13 Sample Sequence is Valid for Dump
					} else {
						// *OTHERWISE
						if (ErrorProcessingEnum.isSendMessage(dto.getErrorProcessing())) {
							// PAR.Error Processing is *Send message
							// DEBUG genFunctionCall BEGIN 1000416 ACT Send error message - 'EAN Seq Portion Inv Dump'
							// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1490352) EXCINTFUN 1432570 UtilitiesAndUserSource.processeEan13 
							// DEBUG genFunctionCall END
							if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
								// PAR.Error Processing is *Quit
								// DEBUG genFunctionCall BEGIN 1000423 ACT <-- *QUIT
								// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
								// DEBUG genFunctionCall END
							}
						}
					}
					// 
				} else if (dto.getLclEan13Type() == Ean13TypeEnum._EQUIPMENT) {
					// LCL.EAN 13 Type is Equipment
					// 
				} else if (dto.getLclEan13Type() == Ean13TypeEnum._DELIVERY_LOAD) {
					// LCL.EAN 13 Type is Delivery Load
					// 
				} else {
					// *OTHERWISE
					if (ErrorProcessingEnum.isSendMessage(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Send message
						// DEBUG genFunctionCall BEGIN 1000191 ACT Send error message - 'EAN Type Invalid'
						// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1432618) EXCINTFUN 1432570 UtilitiesAndUserSource.processeEan13 
						// DEBUG genFunctionCall END
					}
					if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Quit
						// DEBUG genFunctionCall BEGIN 1000198 ACT <-- *QUIT
						// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
						// DEBUG genFunctionCall END
					} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
						// PAR.Error Processing is *Exit
						// DEBUG genFunctionCall BEGIN 1000202 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
					}
				}
			}
			// DEBUG genFunctionCall BEGIN 1000238 ACT PAR.EAN 13 Type = LCL.EAN 13 Type
			dto.setEan13Type(dto.getLclEan13Type());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000242 ACT PAR.EAN 13 Surrogate = LCL.EAN 13 Surrogate
			dto.setEan13Surrogate(dto.getLclEan13Surrogate());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000227 ACT PAR.Folio Number          KEY = CND.Not entered
			dto.setFolioNumberKey("");
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000252 ACT PAR.EAN 13 Sample Sequence = LCL.EAN 13 Sample Sequence
			dto.setEan13SampleSequence(dto.getLclEan13SampleSequence());
			// DEBUG genFunctionCall END
		}

		ProcesseEan13Result result = new ProcesseEan13Result();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * CheckEan13CheckDigitService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCheckEan13CheckDigit(ProcesseEan13DTO dto, CheckEan13CheckDigitParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ProcesseEan13Params processeEan13Params = new ProcesseEan13Params();
//        BeanUtils.copyProperties(dto, processeEan13Params);
//        stepResult = StepResult.callService(ProcesseEan13Service.class, processeEan13Params);
//
//        return stepResult;
//    }
//
}
