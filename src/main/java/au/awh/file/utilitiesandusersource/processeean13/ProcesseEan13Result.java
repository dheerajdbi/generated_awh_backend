package au.awh.file.utilitiesandusersource.processeean13;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.Ean13TypeEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ProcesseEan13 ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ProcesseEan13Result implements Serializable
{
	private static final long serialVersionUID = 5116934549675985872L;

	private ReturnCodeEnum _sysReturnCode;
	private String folioNumberKey = ""; // String 1403
	private Ean13TypeEnum ean13Type = null; // Ean13TypeEnum 36557
	private long ean13Surrogate = 0L; // long 36558
	private long ean13SampleSequence = 0L; // long 36559

	public String getFolioNumberKey() {
		return folioNumberKey;
	}
	
	public void setFolioNumberKey(String folioNumberKey) {
		this.folioNumberKey = folioNumberKey;
	}
	
	public Ean13TypeEnum getEan13Type() {
		return ean13Type;
	}
	
	public void setEan13Type(Ean13TypeEnum ean13Type) {
		this.ean13Type = ean13Type;
	}
	
	public void setEan13Type(String ean13Type) {
		setEan13Type(Ean13TypeEnum.valueOf(ean13Type));
	}
	
	public long getEan13Surrogate() {
		return ean13Surrogate;
	}
	
	public void setEan13Surrogate(long ean13Surrogate) {
		this.ean13Surrogate = ean13Surrogate;
	}
	
	public void setEan13Surrogate(String ean13Surrogate) {
		setEan13Surrogate(Long.parseLong(ean13Surrogate));
	}
	
	public long getEan13SampleSequence() {
		return ean13SampleSequence;
	}
	
	public void setEan13SampleSequence(long ean13SampleSequence) {
		this.ean13SampleSequence = ean13SampleSequence;
	}
	
	public void setEan13SampleSequence(String ean13SampleSequence) {
		setEan13SampleSequence(Long.parseLong(ean13SampleSequence));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
