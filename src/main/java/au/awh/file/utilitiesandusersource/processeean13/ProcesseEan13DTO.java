package au.awh.file.utilitiesandusersource.processeean13;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.Ean13TypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ProcesseEan13DTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private Ean13TypeEnum ean13Type;
	private ErrorProcessingEnum errorProcessing;
	private String folioNumberKey;
	private long ean13;
	private long ean13SampleSequence;
	private long ean13Surrogate;

	private String lclActualCheckDigit;
	private String lclCheckDigit;
	private String lclEan13Alpha;
	private String lclEan13SampleSeqAlpha;
	private long lclEan13SampleSequence;
	private long lclEan13Surrogate;
	private String lclEan13SurrogateAlpha;
	private Ean13TypeEnum lclEan13Type;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public long getEan13() {
		return ean13;
	}

	public long getEan13SampleSequence() {
		return ean13SampleSequence;
	}

	public long getEan13Surrogate() {
		return ean13Surrogate;
	}

	public Ean13TypeEnum getEan13Type() {
		return ean13Type;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public String getFolioNumberKey() {
		return folioNumberKey;
	}

	public String getLclActualCheckDigit() {
		return lclActualCheckDigit;
	}

	public String getLclCheckDigit() {
		return lclCheckDigit;
	}

	public String getLclEan13Alpha() {
		return lclEan13Alpha;
	}

	public String getLclEan13SampleSeqAlpha() {
		return lclEan13SampleSeqAlpha;
	}

	public long getLclEan13SampleSequence() {
		return lclEan13SampleSequence;
	}

	public long getLclEan13Surrogate() {
		return lclEan13Surrogate;
	}

	public String getLclEan13SurrogateAlpha() {
		return lclEan13SurrogateAlpha;
	}

	public Ean13TypeEnum getLclEan13Type() {
		return lclEan13Type;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setEan13(long ean13) {
		this.ean13 = ean13;
	}

	public void setEan13SampleSequence(long ean13SampleSequence) {
		this.ean13SampleSequence = ean13SampleSequence;
	}

	public void setEan13Surrogate(long ean13Surrogate) {
		this.ean13Surrogate = ean13Surrogate;
	}

	public void setEan13Type(Ean13TypeEnum ean13Type) {
		this.ean13Type = ean13Type;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setFolioNumberKey(String folioNumberKey) {
		this.folioNumberKey = folioNumberKey;
	}

	public void setLclActualCheckDigit(String lclActualCheckDigit) {
		this.lclActualCheckDigit = lclActualCheckDigit;
	}

	public void setLclCheckDigit(String lclCheckDigit) {
		this.lclCheckDigit = lclCheckDigit;
	}

	public void setLclEan13Alpha(String lclEan13Alpha) {
		this.lclEan13Alpha = lclEan13Alpha;
	}

	public void setLclEan13SampleSeqAlpha(String lclEan13SampleSeqAlpha) {
		this.lclEan13SampleSeqAlpha = lclEan13SampleSeqAlpha;
	}

	public void setLclEan13SampleSequence(long lclEan13SampleSequence) {
		this.lclEan13SampleSequence = lclEan13SampleSequence;
	}

	public void setLclEan13Surrogate(long lclEan13Surrogate) {
		this.lclEan13Surrogate = lclEan13Surrogate;
	}

	public void setLclEan13SurrogateAlpha(String lclEan13SurrogateAlpha) {
		this.lclEan13SurrogateAlpha = lclEan13SurrogateAlpha;
	}

	public void setLclEan13Type(Ean13TypeEnum lclEan13Type) {
		this.lclEan13Type = lclEan13Type;
	}

}
