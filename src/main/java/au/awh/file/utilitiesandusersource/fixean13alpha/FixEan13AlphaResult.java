package au.awh.file.utilitiesandusersource.fixean13alpha;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: FixEan13Alpha ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class FixEan13AlphaResult implements Serializable
{
	private static final long serialVersionUID = 8441515867304513231L;

	private ReturnCodeEnum _sysReturnCode;
	private String ean13Alpha = ""; // String 36564

	public String getEan13Alpha() {
		return ean13Alpha;
	}
	
	public void setEan13Alpha(String ean13Alpha) {
		this.ean13Alpha = ean13Alpha;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
