package au.awh.file.utilitiesandusersource.fixean13alpha;

// ExecuteInternalFunction.kt File=UtilitiesAndUserSource () Function=fixEan13Alpha (1513616) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthService;
// imports for DTO
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthDTO;
// imports for Enums
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthParams;
// imports for Results
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'Fix Ean 13 Alpha' (file 'Utilities & User Source' ()
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class FixEan13AlphaService extends AbstractService<FixEan13AlphaService, FixEan13AlphaDTO>
{
	private final Step execute = define("execute", FixEan13AlphaParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GetStringLengthService getStringLengthService;

	//private final Step serviceGetStringLength = define("serviceGetStringLength",GetStringLengthResult.class, this::processServiceGetStringLength);
	
	@Autowired
	public FixEan13AlphaService() {
		super(FixEan13AlphaService.class, FixEan13AlphaDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(FixEan13AlphaParams params) throws ServiceException {
		FixEan13AlphaDTO dto = new FixEan13AlphaDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(FixEan13AlphaDTO dto, FixEan13AlphaParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetStringLengthParams getStringLengthParams = null;
		GetStringLengthResult getStringLengthResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT Get String Length - Utilities & User Source  *
		getStringLengthParams = new GetStringLengthParams();
		getStringLengthResult = new GetStringLengthResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getStringLengthParams);
		getStringLengthParams.setQclscanString(dto.getEan13Alpha());
		getStringLengthParams.setQclscanStringLength("13");
		getStringLengthParams.setIncludeLeadingBlanks("N");
		stepResult = getStringLengthService.execute(getStringLengthParams);
		getStringLengthResult = (GetStringLengthResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getStringLengthResult.get_SysReturnCode());
		dto.setLclQclscanResult(getStringLengthResult.getQclscanResult());
		// DEBUG genFunctionCall END
		if (dto.getLclQclscanResult() == 12) {
			// LCL.QCLSCAN Result is 12
			// DEBUG genFunctionCall BEGIN 1000011 ACT PAR.EAN 13 Alpha = CONCAT(CON.0,PAR.EAN 13 Alpha,CON.*ZERO)
			dto.setEan13Alpha("0".trim().concat(dto.getEan13Alpha()));
			// DEBUG genFunctionCall END
		}

		FixEan13AlphaResult result = new FixEan13AlphaResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetStringLengthService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetStringLength(FixEan13AlphaDTO dto, GetStringLengthParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        FixEan13AlphaParams fixEan13AlphaParams = new FixEan13AlphaParams();
//        BeanUtils.copyProperties(dto, fixEan13AlphaParams);
//        stepResult = StepResult.callService(FixEan13AlphaService.class, fixEan13AlphaParams);
//
//        return stepResult;
//    }
//
}
