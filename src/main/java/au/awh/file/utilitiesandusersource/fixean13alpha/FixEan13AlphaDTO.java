package au.awh.file.utilitiesandusersource.fixean13alpha;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class FixEan13AlphaDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String ean13Alpha;

	private long lclQclscanResult;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getEan13Alpha() {
		return ean13Alpha;
	}

	public long getLclQclscanResult() {
		return lclQclscanResult;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setEan13Alpha(String ean13Alpha) {
		this.ean13Alpha = ean13Alpha;
	}

	public void setLclQclscanResult(long lclQclscanResult) {
		this.lclQclscanResult = lclQclscanResult;
	}

}
