package au.awh.file.utilitiesandusersource.fixean13alpha;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: FixEan13Alpha ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class FixEan13AlphaParams implements Serializable
{
	private static final long serialVersionUID = -8743505535742819023L;

    private String ean13Alpha = "";

	public String getEan13Alpha() {
		return ean13Alpha;
	}
	
	public void setEan13Alpha(String ean13Alpha) {
		this.ean13Alpha = ean13Alpha;
	}
}
