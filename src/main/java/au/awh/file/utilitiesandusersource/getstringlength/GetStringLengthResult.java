package au.awh.file.utilitiesandusersource.getstringlength;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetStringLength ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetStringLengthResult implements Serializable
{
	private static final long serialVersionUID = -8883905270393714640L;

	private ReturnCodeEnum _sysReturnCode;
	private long qclscanResult = 0L; // long 32105

	public long getQclscanResult() {
		return qclscanResult;
	}
	
	public void setQclscanResult(long qclscanResult) {
		this.qclscanResult = qclscanResult;
	}
	
	public void setQclscanResult(String qclscanResult) {
		setQclscanResult(Long.parseLong(qclscanResult));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
