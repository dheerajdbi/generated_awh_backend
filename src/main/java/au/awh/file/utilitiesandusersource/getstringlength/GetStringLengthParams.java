package au.awh.file.utilitiesandusersource.getstringlength;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetStringLength ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetStringLengthParams implements Serializable
{
	private static final long serialVersionUID = -8662695171426136362L;

    private String qclscanString = "";
    private long qclscanStringLength = 0L;
    private YesOrNoEnum includeLeadingBlanks = null;

	public String getQclscanString() {
		return qclscanString;
	}
	
	public void setQclscanString(String qclscanString) {
		this.qclscanString = qclscanString;
	}
	
	public long getQclscanStringLength() {
		return qclscanStringLength;
	}
	
	public void setQclscanStringLength(long qclscanStringLength) {
		this.qclscanStringLength = qclscanStringLength;
	}
	
	public void setQclscanStringLength(String qclscanStringLength) {
		setQclscanStringLength(Long.parseLong(qclscanStringLength));
	}
	
	public YesOrNoEnum getIncludeLeadingBlanks() {
		return includeLeadingBlanks;
	}
	
	public void setIncludeLeadingBlanks(YesOrNoEnum includeLeadingBlanks) {
		this.includeLeadingBlanks = includeLeadingBlanks;
	}
	
	public void setIncludeLeadingBlanks(String includeLeadingBlanks) {
		setIncludeLeadingBlanks(YesOrNoEnum.valueOf(includeLeadingBlanks));
	}
}
