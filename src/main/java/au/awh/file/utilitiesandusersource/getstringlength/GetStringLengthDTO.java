package au.awh.file.utilitiesandusersource.getstringlength;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetStringLengthDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String character;
	private String qclscanString;
	private YesOrNoEnum includeLeadingBlanks;
	private long counter1;
	private long counter2;
	private long qclscanResult;
	private long qclscanStringLength;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getCharacter() {
		return character;
	}

	public long getCounter1() {
		return counter1;
	}

	public long getCounter2() {
		return counter2;
	}

	public YesOrNoEnum getIncludeLeadingBlanks() {
		return includeLeadingBlanks;
	}

	public long getQclscanResult() {
		return qclscanResult;
	}

	public String getQclscanString() {
		return qclscanString;
	}

	public long getQclscanStringLength() {
		return qclscanStringLength;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	public void setCounter1(long counter1) {
		this.counter1 = counter1;
	}

	public void setCounter2(long counter2) {
		this.counter2 = counter2;
	}

	public void setIncludeLeadingBlanks(YesOrNoEnum includeLeadingBlanks) {
		this.includeLeadingBlanks = includeLeadingBlanks;
	}

	public void setQclscanResult(long qclscanResult) {
		this.qclscanResult = qclscanResult;
	}

	public void setQclscanString(String qclscanString) {
		this.qclscanString = qclscanString;
	}

	public void setQclscanStringLength(long qclscanStringLength) {
		this.qclscanStringLength = qclscanStringLength;
	}

}
