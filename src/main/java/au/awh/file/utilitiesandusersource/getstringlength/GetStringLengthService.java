package au.awh.file.utilitiesandusersource.getstringlength;

// ExecuteInternalFunction.kt File=UtilitiesAndUserSource () Function=getStringLength (1357999) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

/**
 * EXCINTFNC Service controller for 'Get String Length' (file 'Utilities & User Source' ()
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GetStringLengthService extends AbstractService<GetStringLengthService, GetStringLengthDTO>
{
	private final Step execute = define("execute", GetStringLengthParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public GetStringLengthService() {
		super(GetStringLengthService.class, GetStringLengthDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GetStringLengthParams params) throws ServiceException {
		GetStringLengthDTO dto = new GetStringLengthDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GetStringLengthDTO dto, GetStringLengthParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		
		if ((dto.getQclscanString() != null && dto.getQclscanString().isEmpty()) || (dto.getQclscanStringLength() <= 0)) {
			// DEBUG genFunctionCall BEGIN 1000074 ACT PAR.Counter 1 = CON.*ZERO
			dto.setCounter1(0);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000065 ACT PAR.Counter 2 = CON.*ZERO
			dto.setCounter2(0);
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000034 ACT PAR.Counter 1 = PAR.QCLSCAN String Length
			dto.setCounter1(dto.getQclscanStringLength());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000015 ACT PAR.Character = CND.Not Entered
			dto.setCharacter("");
			// DEBUG genFunctionCall END
			while ((dto.getCounter1() > 0) && (dto.getCharacter() != null && dto.getCharacter().isEmpty())) {
				// DEBUG genFunctionCall BEGIN 1000045 ACT PAR.Character = SUBSTRING(PAR.QCLSCAN String,PAR.Counter 1,CON.1)
				dto.setCharacter(StringUtils.substring(dto.getQclscanString(), (int)(int) dto.getCounter1() - 1, 1));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000051 ACT PAR.Counter 1 = PAR.Counter 1 - CON.1
				dto.setCounter1(dto.getCounter1() - 1);
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000088 ACT PAR.Counter 1 = PAR.Counter 1 + CON.1
			dto.setCounter1(dto.getCounter1() + 1);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000002 ACT PAR.Counter 2 = CON.*ZERO
			dto.setCounter2(0);
			// DEBUG genFunctionCall END
			if (dto.getIncludeLeadingBlanks() == YesOrNoEnum._NO) {
				// PAR.Include Leading Blanks is No
				// DEBUG genFunctionCall BEGIN 1000096 ACT PAR.Character = CND.Not Entered
				dto.setCharacter("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000110 ACT PAR.Counter 2 = CON.1
				dto.setCounter2(1);
				// DEBUG genFunctionCall END
				while ((
				// DEBUG ComparatorJavaGenerator BEGIN
				dto.getCounter2() <= dto.getQclscanStringLength()
				// DEBUG ComparatorJavaGenerator END
				) && (dto.getCharacter() != null && dto.getCharacter().isEmpty())) {
					// DEBUG genFunctionCall BEGIN 1000028 ACT PAR.Character = SUBSTRING(PAR.QCLSCAN String,PAR.Counter 2,CON.1)
					dto.setCharacter(StringUtils.substring(dto.getQclscanString(), (int)(int) dto.getCounter2() - 1, 1));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000019 ACT PAR.Counter 2 = PAR.Counter 2 + CON.1
					dto.setCounter2(dto.getCounter2() + 1);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000078 ACT PAR.Counter 2 = PAR.Counter 2 - CON.2
				dto.setCounter2(dto.getCounter2() - 2);
				// DEBUG genFunctionCall END
			}
		}
		// DEBUG genFunctionCall BEGIN 1000024 ACT PAR.QCLSCAN Result = PAR.Counter 1 - PAR.Counter 2
		dto.setQclscanResult(dto.getCounter1() - dto.getCounter2());
		// DEBUG genFunctionCall END

		GetStringLengthResult result = new GetStringLengthResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
