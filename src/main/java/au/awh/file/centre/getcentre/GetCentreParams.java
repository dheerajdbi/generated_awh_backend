package au.awh.file.centre.getcentre;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetCentre ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentreParams implements Serializable
{
	private static final long serialVersionUID = -3778376821229731489L;

    private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
