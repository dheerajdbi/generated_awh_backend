package au.awh.file.centre.getecentre;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.CentreTypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GeteCentreDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private CentreTypeEnum centreType;
	private ErrorProcessingEnum errorProcessing;
	private GetRecordOptionEnum getRecordOption;
	private StateCodeEnum stateCode;
	private String centreCodeKey;
	private String centreName;
	private String centreRegion;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getCentreName() {
		return centreName;
	}

	public String getCentreRegion() {
		return centreRegion;
	}

	public CentreTypeEnum getCentreType() {
		return centreType;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}

	public StateCodeEnum getStateCode() {
		return stateCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public void setCentreRegion(String centreRegion) {
		this.centreRegion = centreRegion;
	}

	public void setCentreType(CentreTypeEnum centreType) {
		this.centreType = centreType;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}

	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}

}
