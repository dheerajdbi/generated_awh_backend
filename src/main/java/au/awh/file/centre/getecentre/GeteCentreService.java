package au.awh.file.centre.getecentre;

// ExecuteInternalFunction.kt File=Centre (CR) Function=geteCentre (1416805) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.centre.Centre;
import au.awh.file.centre.CentreId;
import au.awh.file.centre.CentreRepository;
// imports for Services
import au.awh.file.centre.getcentre.GetCentreService;
// imports for DTO
import au.awh.file.centre.getcentre.GetCentreDTO;
// imports for Enums
import au.awh.model.CentreTypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// imports for Parameters
import au.awh.file.centre.getcentre.GetCentreParams;
// imports for Results
import au.awh.file.centre.getcentre.GetCentreResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Centre' (file 'Centre' (WSCENTREP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteCentreService extends AbstractService<GeteCentreService, GeteCentreDTO>
{
	private final Step execute = define("execute", GeteCentreParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private CentreRepository centreRepository;

	@Autowired
	private GetCentreService getCentreService;

	//private final Step serviceGetCentre = define("serviceGetCentre",GetCentreResult.class, this::processServiceGetCentre);
	
	@Autowired
	public GeteCentreService() {
		super(GeteCentreService.class, GeteCentreDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteCentreParams params) throws ServiceException {
		GeteCentreDTO dto = new GeteCentreDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteCentreDTO dto, GeteCentreParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetCentreParams getCentreParams = null;
		GetCentreResult getCentreResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Centre - Centre  *
		getCentreParams = new GetCentreParams();
		getCentreResult = new GetCentreResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getCentreParams);
		getCentreParams.setCentreCodeKey(dto.getCentreCodeKey());
		stepResult = getCentreService.execute(getCentreParams);
		getCentreResult = (GetCentreResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getCentreResult.get_SysReturnCode());
		dto.setCentreName(getCentreResult.getCentreName());
		dto.setCentreType(getCentreResult.getCentreType());
		dto.setStateCode(getCentreResult.getStateCode());
		dto.setCentreRegion(getCentreResult.getCentreRegion());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000210 ACT Send error message - 'Centre                 NF'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1100313) EXCINTFUN 1416805 Centre.geteCentre 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1416805 Centre.geteCentre 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteCentreResult result = new GeteCentreResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetCentreService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentre(GeteCentreDTO dto, GetCentreParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteCentreParams geteCentreParams = new GeteCentreParams();
//        BeanUtils.copyProperties(dto, geteCentreParams);
//        stepResult = StepResult.callService(GeteCentreService.class, geteCentreParams);
//
//        return stepResult;
//    }
//
}
