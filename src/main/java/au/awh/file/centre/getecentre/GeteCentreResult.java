package au.awh.file.centre.getecentre;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.CentreTypeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteCentre ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteCentreResult implements Serializable
{
	private static final long serialVersionUID = -7177717562401928891L;

	private ReturnCodeEnum _sysReturnCode;
	private String centreName = ""; // String 1142
	private CentreTypeEnum centreType = null; // CentreTypeEnum 1149
	private StateCodeEnum stateCode = null; // StateCodeEnum 1570
	private String centreRegion = ""; // String 1144

	public String getCentreName() {
		return centreName;
	}
	
	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}
	
	public CentreTypeEnum getCentreType() {
		return centreType;
	}
	
	public void setCentreType(CentreTypeEnum centreType) {
		this.centreType = centreType;
	}
	
	public void setCentreType(String centreType) {
		setCentreType(CentreTypeEnum.valueOf(centreType));
	}
	
	public StateCodeEnum getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}
	
	public void setStateCode(String stateCode) {
		setStateCode(StateCodeEnum.valueOf(stateCode));
	}
	
	public String getCentreRegion() {
		return centreRegion;
	}
	
	public void setCentreRegion(String centreRegion) {
		this.centreRegion = centreRegion;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
