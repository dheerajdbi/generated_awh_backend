package au.awh.file.centre;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Centre (WSCENTREP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface CentreRepository extends CentreRepositoryCustom, JpaRepository<Centre, CentreId> {

	List<Centre> findAllBy();
}
