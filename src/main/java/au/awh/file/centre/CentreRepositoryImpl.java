package au.awh.file.centre;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.centre.Centre;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Centre (WSCENTREP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class CentreRepositoryImpl implements CentreRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public RestResponsePage<Wsut5502SelectCentreGDO> wsut5502SelectCentre(
		String centreCodeKey
, String centreName, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			whereClause += " (centre.id.centreCodeKey >= :centreCodeKey OR centre.id.centreCodeKey like CONCAT('%', :centreCodeKey, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreName)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " (centre.centreName >= :centreName OR centre.centreName like CONCAT('%', :centreName, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreGDO(centre.id.centreCodeKey, centre.centreName, centre.centreType, centre.stateCode, centre.centreRegion) from Centre centre";
		String countQueryString = "SELECT COUNT(centre.id.centreCodeKey) FROM Centre centre";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			Centre centre = new Centre();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(centre.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"centre.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"centre." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (StringUtils.isNotEmpty(centreName)) {
			countQuery.setParameter("centreName", centreName);
			query.setParameter("centreName", centreName);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<Wsut5502SelectCentreGDO> content = query.getResultList();
		RestResponsePage<Wsut5502SelectCentreGDO> pageGdo = new RestResponsePage<Wsut5502SelectCentreGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.centre.CentreService#getCentreDetails(String)
	 */
	@Override
	public Centre getCentreDetails(
		String centreCodeKey
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			whereClause += " centre.id.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		String sqlString = "SELECT centre FROM Centre centre";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Centre dto = (Centre)query.getSingleResult();

		return dto;
	}

	/**
	 * @see au.awh.file.centre.CentreService#getCentre(String)
	 */
	@Override
	public Centre getCentre(
		String centreCodeKey
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			whereClause += " centre.id.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		String sqlString = "SELECT centre FROM Centre centre";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Centre dto = (Centre)query.getSingleResult();

		return dto;
	}

}
