package au.awh.file.centre.wsut5502selectcentre;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.CentreTypeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Gdo for file 'Centre' (WSCENTREP) and function 'WSUT5502 Select Centre' (WSUT5502).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class Wsut5502SelectCentreGDO implements Serializable {
	private static final long serialVersionUID = -6545290530181264125L;

	private String _sysSelected = "";
	private String centreCodeKey = "";
	private String centreName = "";
	private CentreTypeEnum centreType = null;
	private StateCodeEnum stateCode = null;
	private String centreRegion = "";

	public Wsut5502SelectCentreGDO() {

	}

	public Wsut5502SelectCentreGDO(String centreCodeKey, String centreName, CentreTypeEnum centreType, StateCodeEnum stateCode, String centreRegion) {
		this.centreCodeKey = centreCodeKey;
		this.centreName = centreName;
		this.centreType = centreType;
		this.stateCode = stateCode;
		this.centreRegion = centreRegion;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setCentreType(CentreTypeEnum centreType) {
		this.centreType = centreType;
	}

	public CentreTypeEnum getCentreType() {
		return centreType;
	}

	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}

	public StateCodeEnum getStateCode() {
		return stateCode;
	}

	public void setCentreRegion(String centreRegion) {
		this.centreRegion = centreRegion;
	}

	public String getCentreRegion() {
		return centreRegion;
	}

}
