package au.awh.file.centre.wsut5502selectcentre;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: Wsut5502SelectCentre (WSUT5502).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class Wsut5502SelectCentreParams implements Serializable
{
    private static final long serialVersionUID = -5394074574334506876L;

	private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}

