package au.awh.file.centre.wsut5502selectcentre;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import au.awh.model.GlobalContext;
import au.awh.file.centre.Centre;
// generateStatusFieldImportStatements BEGIN 24
import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Centre' (WSCENTREP) and function 'WSUT5502 Select Centre' (WSUT5502).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Configurable
public class Wsut5502SelectCentreState extends Wsut5502SelectCentreDTO {
    private static final long serialVersionUID = -8588664489562452323L;

    @Autowired
    private GlobalContext globalCtx;


    // System fields
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public Wsut5502SelectCentreState() {
    }

    public Wsut5502SelectCentreState(Centre centre) {
        setDtoFields(centre);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setWfBrokerId(String brokerId) {
        globalCtx.setString("brokerId", brokerId);
    }

    public String getWfBrokerId() {
        return globalCtx.getString("brokerId");
    }

    public void setWfCentreCode(String centreCode) {
        globalCtx.setString("centreCode", centreCode);
    }

    public String getWfCentreCode() {
        return globalCtx.getString("centreCode");
    }

    public void setWfLdaAttnMenuSwitch(String ldaAttnMenuSwitch) {
        globalCtx.setString("ldaAttnMenuSwitch", ldaAttnMenuSwitch);
    }

    public String getWfLdaAttnMenuSwitch() {
        return globalCtx.getString("ldaAttnMenuSwitch");
    }

    public void setWfLdaCostCentre(String ldaCostCentre) {
        globalCtx.setString("ldaCostCentre", ldaCostCentre);
    }

    public String getWfLdaCostCentre() {
        return globalCtx.getString("ldaCostCentre");
    }

    public void setWfLdaCurrentBrokerId(String ldaCurrentBrokerId) {
        globalCtx.setString("ldaCurrentBrokerId", ldaCurrentBrokerId);
    }

    public String getWfLdaCurrentBrokerId() {
        return globalCtx.getString("ldaCurrentBrokerId");
    }

    public void setWfLdaCurrentCentreCode(String ldaCurrentCentreCode) {
        globalCtx.setString("ldaCurrentCentreCode", ldaCurrentCentreCode);
    }

    public String getWfLdaCurrentCentreCode() {
        return globalCtx.getString("ldaCurrentCentreCode");
    }

    public void setWfLdaCurrentStateCode(String ldaCurrentStateCode) {
        globalCtx.setString("ldaCurrentStateCode", ldaCurrentStateCode);
    }

    public String getWfLdaCurrentStateCode() {
        return globalCtx.getString("ldaCurrentStateCode");
    }

    public void setWfLdaDefaultBrokerId(String ldaDefaultBrokerId) {
        globalCtx.setString("ldaDefaultBrokerId", ldaDefaultBrokerId);
    }

    public String getWfLdaDefaultBrokerId() {
        return globalCtx.getString("ldaDefaultBrokerId");
    }

    public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
        globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
    }

    public String getWfLdaDefaultCentreCode() {
        return globalCtx.getString("ldaDefaultCentreCode");
    }

    public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
        globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
    }

    public String getWfLdaDefaultStateCode() {
        return globalCtx.getString("ldaDefaultStateCode");
    }

    public void setWfLdaEldersStateCode(String ldaEldersStateCode) {
        globalCtx.setString("ldaEldersStateCode", ldaEldersStateCode);
    }

    public String getWfLdaEldersStateCode() {
        return globalCtx.getString("ldaEldersStateCode");
    }

    public void setWfLdaEldersStateNumCode(String ldaEldersStateNumCode) {
        globalCtx.setString("ldaEldersStateNumCode", ldaEldersStateNumCode);
    }

    public String getWfLdaEldersStateNumCode() {
        return globalCtx.getString("ldaEldersStateNumCode");
    }

    public void setWfLdaEnvironment(String ldaEnvironment) {
        globalCtx.setString("ldaEnvironment", ldaEnvironment);
    }

    public String getWfLdaEnvironment() {
        return globalCtx.getString("ldaEnvironment");
    }

    public void setWfLdaHomeCostCentre(String ldaHomeCostCentre) {
        globalCtx.setString("ldaHomeCostCentre", ldaHomeCostCentre);
    }

    public String getWfLdaHomeCostCentre() {
        return globalCtx.getString("ldaHomeCostCentre");
    }

    public void setWfLdaNetworkSystemName(String ldaNetworkSystemName) {
        globalCtx.setString("ldaNetworkSystemName", ldaNetworkSystemName);
    }

    public String getWfLdaNetworkSystemName() {
        return globalCtx.getString("ldaNetworkSystemName");
    }

    public void setWfLdaOutputCostCentre(String ldaOutputCostCentre) {
        globalCtx.setString("ldaOutputCostCentre", ldaOutputCostCentre);
    }

    public String getWfLdaOutputCostCentre() {
        return globalCtx.getString("ldaOutputCostCentre");
    }

    public void setWfLdaPrinter(String ldaPrinter) {
        globalCtx.setString("ldaPrinter", ldaPrinter);
    }

    public String getWfLdaPrinter() {
        return globalCtx.getString("ldaPrinter");
    }

    public void setWfLdaSecurityProfile(String ldaSecurityProfile) {
        globalCtx.setString("ldaSecurityProfile", ldaSecurityProfile);
    }

    public String getWfLdaSecurityProfile() {
        return globalCtx.getString("ldaSecurityProfile");
    }

    public void setWfLdaStateCostCentre(String ldaStateCostCentre) {
        globalCtx.setString("ldaStateCostCentre", ldaStateCostCentre);
    }

    public String getWfLdaStateCostCentre() {
        return globalCtx.getString("ldaStateCostCentre");
    }

    public void setWfLdaStateHighSpeedPrtr(String ldaStateHighSpeedPrtr) {
        globalCtx.setString("ldaStateHighSpeedPrtr", ldaStateHighSpeedPrtr);
    }

    public String getWfLdaStateHighSpeedPrtr() {
        return globalCtx.getString("ldaStateHighSpeedPrtr");
    }

    public void setWfLdaTerminal(String ldaTerminal) {
        globalCtx.setString("ldaTerminal", ldaTerminal);
    }

    public String getWfLdaTerminal() {
        return globalCtx.getString("ldaTerminal");
    }

    public void setWfLdaUser(String ldaUser) {
        globalCtx.setString("ldaUser", ldaUser);
    }

    public String getWfLdaUser() {
        return globalCtx.getString("ldaUser");
    }

    public void setWfStateCode(String stateCode) {
        globalCtx.setString("stateCode", stateCode);
    }

    public String getWfStateCode() {
        return globalCtx.getString("stateCode");
    }

    public void set_SysReturnCode(ReturnCodeEnum returnCode) {
        _sysReturnCode = returnCode;
    }

    public ReturnCodeEnum get_SysReturnCode() {
        return _sysReturnCode;
    }

    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
    return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
