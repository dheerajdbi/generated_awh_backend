package au.awh.file.centre.wsut5502selectcentre;
    
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;
import au.awh.support.JobContext;

import java.io.IOException;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;

import au.awh.file.centre.CentreRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.utilitiesandusersource.readldasetentityhdg.ReadLdaSetEntityHdgService;
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileService;
// asServiceDtoImportStatement
import au.awh.file.utilitiesandusersource.readldasetentityhdg.ReadLdaSetEntityHdgDTO;
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileDTO;
// asServiceParamsImportStatement
import au.awh.file.utilitiesandusersource.readldasetentityhdg.ReadLdaSetEntityHdgParams;
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileParams;
// asServiceResultImportStatement
import au.awh.file.utilitiesandusersource.readldasetentityhdg.ReadLdaSetEntityHdgResult;
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.CentreTypeEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * SELRCD Service controller for 'WSUT5502 Select Centre' (WSUT5502) of file 'Centre' (WSCENTREP)
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Service
public class Wsut5502SelectCentreService extends AbstractService<Wsut5502SelectCentreService, Wsut5502SelectCentreState> {
    
	@Autowired
	private JobContext job;

	@Autowired
	private CentreRepository centreRepository;
        
    @Autowired
    private LoadScreenHdgSubfileService loadScreenHdgSubfileService;
    
    @Autowired
    private ReadLdaSetEntityHdgService readLdaSetEntityHdgService;
    

    
    public static final String SCREEN_SELECT = "wsut5502SelectCentre";

	private final Step execute = define("execute", Wsut5502SelectCentreParams.class, this::executeService);
	private final Step response = define("response", Wsut5502SelectCentreDTO.class, this::processResponse);
	//private final Step serviceReadLdaSetEntityHdg = define("serviceReadLdaSetEntityHdg",ReadLdaSetEntityHdgResult.class, this::processServiceReadLdaSetEntityHdg);
	//private final Step serviceLoadScreenHdgSubfile = define("serviceLoadScreenHdgSubfile",LoadScreenHdgSubfileResult.class, this::processServiceLoadScreenHdgSubfile);
	
    

    @Autowired
    public Wsut5502SelectCentreService()
    {
        super(Wsut5502SelectCentreService.class, Wsut5502SelectCentreState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * Wsut5502SelectCentre service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(Wsut5502SelectCentreState state, Wsut5502SelectCentreParams params)
    {
        StepResult stepResult = NO_ACTION;

        if (params != null) {
            BeanUtils.copyProperties(params, state);
        }
        usrInitializeProgram(state);
        stepResult =  mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(Wsut5502SelectCentreState state)
    {
        StepResult stepResult = NO_ACTION;

        dbfReadFirstDataRecord(state);
        if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
            loadNextSubfilePage(state);
        }
        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(Wsut5502SelectCentreState state)
    {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            Wsut5502SelectCentreDTO dto = new Wsut5502SelectCentreDTO();
            BeanUtils.copyProperties(state, dto);
            stepResult = callScreen(SCREEN_SELECT, dto).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_SELECT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(Wsut5502SelectCentreState state, Wsut5502SelectCentreDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey())) {
            //TODO: processResetRequest(state);//synon built-in function
        }
        else if (isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextDataRecord(state);
            loadNextSubfilePage(state);
        }
        else {
            usrProcessSubfileControl(state);
            //TODO:readFirstChangedSubfileRecord(state);//synon built-in function
//			while (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
//              for (Wsut5502SelectCentreGDO gdo : ((Page<Wsut5502SelectCentreGDO>) state.getPageGdo()).getContent())
//		        {
                    if (state.getCentreCodeKey() != null && !state.getCentreCodeKey().equals("")) {
                        Wsut5502SelectCentreGDO gdo = null;
                        for(Wsut5502SelectCentreGDO obj: state.getPageGdo().getContent()) {
                            if(obj.getCentreCodeKey().equals(state.getCentreCodeKey())) {
                                gdo = obj;
                                break;
                            }
                        }
                        usrProcessSelectedLine(state, gdo);
                        Wsut5502SelectCentreResult params = new Wsut5502SelectCentreResult();
                        BeanUtils.copyProperties(state, params);
                        stepResult = StepResult.returnFromService(params);
                        return stepResult;
                    }
//                    usrProcessChangedSubfileRecord(state, gdo);
//                    usrScreenFunctionFields(state, gdo);
//                    //TODO:updateSubfileRecord(state, gdo);//synon built-in function
//                    //TODO:readNextChangedSubfileRecord(state);//synon built-in function
//                }
            if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
            {
                state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
            }
            usrProcessCommandKeys(state);
        }
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     */
    private void loadNextSubfilePage(Wsut5502SelectCentreState state)
    {
        for (Wsut5502SelectCentreGDO gdo : ((Page<Wsut5502SelectCentreGDO>) state.getPageGdo()).getContent())
        {
            state.setRecordSelect(RecordSelectedEnum._STA_YES);
            //TODO:moveDbfRecordFieldsToSubfileRecord(state);//synon built-in function
            usrScreenFunctionFields(state, gdo);
            usrLoadSubfileRecordFromDbfRecord(state, gdo);
            if(state.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(state);//synon built-in function
            }
        }
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(Wsut5502SelectCentreState state)
    {
        StepResult stepResult = NO_ACTION;

        usrExitProgramProcessing(state);

        Wsut5502SelectCentreResult params = new Wsut5502SelectCentreResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param state - Service state class.
     */
    private void dbfReadFirstDataRecord(Wsut5502SelectCentreState state)
    {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     */
    private void dbfReadNextDataRecord(Wsut5502SelectCentreState state)
    {
        state.setPage(state.getPage() + 1);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the actual page
     * @param state - Service state class.
     */
    private void dbfReadDataRecord(Wsut5502SelectCentreState state)
    {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try
        {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet())
            {
                if (entry.getValue() == null)
                {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe)
        {
        }

        if (CollectionUtils.isEmpty(sortOrders))
        {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else
        {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
    }

		RestResponsePage<Wsut5502SelectCentreGDO> pageGdo = centreRepository.wsut5502SelectCentre(null,null, pageable);
		state.setPageGdo(pageGdo);
	}
    
    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(Wsut5502SelectCentreState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            LoadScreenHdgSubfileParams loadScreenHdgSubfileParams = null;
			LoadScreenHdgSubfileResult loadScreenHdgSubfileResult = null;
			ReadLdaSetEntityHdgParams readLdaSetEntityHdgParams = null;
			ReadLdaSetEntityHdgResult readLdaSetEntityHdgResult = null;
			// DEBUG genFunctionCall BEGIN 1000002 ACT Read LDA/Set Entity Hdg - Utilities & User Source  *
			readLdaSetEntityHdgParams = new ReadLdaSetEntityHdgParams();
			readLdaSetEntityHdgResult = new ReadLdaSetEntityHdgResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, readLdaSetEntityHdgParams);
			stepResult = readLdaSetEntityHdgService.execute(readLdaSetEntityHdgParams);
			readLdaSetEntityHdgResult = (ReadLdaSetEntityHdgResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(readLdaSetEntityHdgResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000024 ACT Load Screen Hdg - Subfile - Utilities & User Source  *
			loadScreenHdgSubfileParams = new LoadScreenHdgSubfileParams();
			loadScreenHdgSubfileResult = new LoadScreenHdgSubfileResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, loadScreenHdgSubfileParams);
			loadScreenHdgSubfileParams.setApplicationHeaderName("");
			loadScreenHdgSubfileParams.setBrokerId(dto.getWfBrokerId());
			loadScreenHdgSubfileParams.setStateCode(StateCodeEnum.fromCode(dto.getWfStateCode()));
			loadScreenHdgSubfileParams.setCentreCode(dto.getWfCentreCode());
			stepResult = loadScreenHdgSubfileService.execute(loadScreenHdgSubfileParams);
			loadScreenHdgSubfileResult = (LoadScreenHdgSubfileResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(loadScreenHdgSubfileResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
    private StepResult usrProcessSubfileControl(Wsut5502SelectCentreState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
    private StepResult usrProcessSelectedLine(Wsut5502SelectCentreState dto, Wsut5502SelectCentreGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 107 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
    private StepResult usrProcessChangedSubfileRecord(Wsut5502SelectCentreState dto, Wsut5502SelectCentreGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
    private StepResult usrScreenFunctionFields(Wsut5502SelectCentreState dto, Wsut5502SelectCentreGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 165 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
    private StepResult usrProcessCommandKeys(Wsut5502SelectCentreState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel(dto.get_SysCmdKey())) {
				// CTL.*CMD key is Cancel
				// DEBUG genFunctionCall BEGIN 1000033 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrLoadSubfileRecordFromDbfRecord(Wsut5502SelectCentreState dto, Wsut5502SelectCentreGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 41 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(Wsut5502SelectCentreState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * ReadLdaSetEntityHdgService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceReadLdaSetEntityHdg(Wsut5502SelectCentreState state, ReadLdaSetEntityHdgParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        Wsut5502SelectCentreParams wsut5502SelectCentreParams = new Wsut5502SelectCentreParams();
//        BeanUtils.copyProperties(state, wsut5502SelectCentreParams);
//        stepResult = StepResult.callService(Wsut5502SelectCentreService.class, wsut5502SelectCentreParams);
//
//        return stepResult;
//    }
////
//    /**
//     * LoadScreenHdgSubfileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceLoadScreenHdgSubfile(Wsut5502SelectCentreState state, LoadScreenHdgSubfileParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        Wsut5502SelectCentreParams wsut5502SelectCentreParams = new Wsut5502SelectCentreParams();
//        BeanUtils.copyProperties(state, wsut5502SelectCentreParams);
//        stepResult = StepResult.callService(Wsut5502SelectCentreService.class, wsut5502SelectCentreParams);
//
//        return stepResult;
//    }
//


}
