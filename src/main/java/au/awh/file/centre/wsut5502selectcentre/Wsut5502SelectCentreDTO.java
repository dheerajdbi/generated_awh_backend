package au.awh.file.centre.wsut5502selectcentre;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.CentreTypeEnum;
import au.awh.model.LdaEnvironmentEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.StateCodeKeyEnum;

import au.awh.file.centre.Centre;
import au.awh.service.data.SelectRecordDTO;

/**
 * Dto for file 'Centre' (WSCENTREP) and function 'WSUT5502 Select Centre' (WSUT5502).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Configurable
public class Wsut5502SelectCentreDTO extends SelectRecordDTO<Wsut5502SelectCentreGDO> {
	private static final long serialVersionUID = 7061877841289888416L;

	private String centreCodeKey = "";
	private String centreName = "";
	private CentreTypeEnum centreType = null;
	private StateCodeEnum stateCode = StateCodeEnum._NOT_ENTERED;
	private String centreRegion = "";

	private ReturnCodeEnum returnCode = ReturnCodeEnum._STA_NORMAL;

	public Wsut5502SelectCentreDTO() {
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setCentreType(CentreTypeEnum centreType) {
		this.centreType = centreType;
	}

	public CentreTypeEnum getCentreType() {
		return centreType;
	}

	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}

	public StateCodeEnum getStateCode() {
		return stateCode;
	}

	public void setCentreRegion(String centreRegion) {
		this.centreRegion = centreRegion;
	}

	public String getCentreRegion() {
		return centreRegion;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param centre Centre Entity bean
     */
    public void setDtoFields(Centre centre) {
        BeanUtils.copyProperties(centre, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param centre Centre Entity bean
     */
    public void setEntityFields(Centre centre) {
        BeanUtils.copyProperties(this, centre);
    }
}
