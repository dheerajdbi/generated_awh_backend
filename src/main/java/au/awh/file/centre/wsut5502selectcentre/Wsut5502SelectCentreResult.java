package au.awh.file.centre.wsut5502selectcentre;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: Wsut5502SelectCentre (WSUT5502).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class Wsut5502SelectCentreResult implements Serializable
{
    private static final long serialVersionUID = 4824591883407412828L;

	private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}

