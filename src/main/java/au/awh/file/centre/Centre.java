package au.awh.file.centre;

import au.awh.model.CentreTypeEnum;
import au.awh.model.StateCodeEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSCENTREP")
public class Centre implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private CentreId id = new CentreId();

	@Column(name = "CRCTRNME")
	private String centreName = "";
	
	@Column(name = "CRCTRRGN")
	private String centreRegion = "";
	
	@Column(name = "CRCTRTYP")
	private CentreTypeEnum centreType = null;
	
	@Column(name = "CRSTTCDE")
	private StateCodeEnum stateCode = StateCodeEnum._NOT_ENTERED;

	public CentreId getId() {
		return id;
	}

	public String getCentreCodeKey() {
		return id.getCentreCodeKey();
	}

	public String getCentreName() {
		return centreName;
	}
	
	public CentreTypeEnum getCentreType() {
		return centreType;
	}
	
	public String getCentreRegion() {
		return centreRegion;
	}
	
	public StateCodeEnum getStateCode() {
		return stateCode;
	}

	

	

	public void setCentreCodeKey(String centreCodeKey) {
		this.id.setCentreCodeKey(centreCodeKey);
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}
	
	public void setCentreType(CentreTypeEnum centreType) {
		this.centreType = centreType;
	}
	
	public void setCentreRegion(String centreRegion) {
		this.centreRegion = centreRegion;
	}
	
	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
