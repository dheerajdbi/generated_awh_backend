package au.awh.file.centre;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreGDO;

/**
 * Custom Spring Data JPA repository interface for model: Centre (WSCENTREP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface CentreRepositoryCustom {

	/**
	 * 
	 * @param centreCodeKey Centre Code Key
	 * @param centreName Centre Name
	 * @param pageable a Pageable object used for pagination
	 * @return Page of Wsut5502SelectCentreGDO
	 */
	RestResponsePage<Wsut5502SelectCentreGDO> wsut5502SelectCentre(String centreCodeKey
	, String centreName, Pageable pageable);

	/**
	 * 
	 * @param centreCodeKey Centre Code Key
	 * @return Centre
	 */
	Centre getCentreDetails(String centreCodeKey
	);

	/**
	 * 
	 * @param centreCodeKey Centre Code Key
	 * @return Centre
	 */
	Centre getCentre(String centreCodeKey
	);
}
