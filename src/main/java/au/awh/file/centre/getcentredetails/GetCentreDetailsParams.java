package au.awh.file.centre.getcentredetails;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetCentreDetails ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentreDetailsParams implements Serializable
{
	private static final long serialVersionUID = 2971884105571257884L;

    private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
