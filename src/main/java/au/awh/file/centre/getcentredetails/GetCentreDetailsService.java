package au.awh.file.centre.getcentredetails;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.centre.Centre;
import au.awh.file.centre.CentreId;
import au.awh.file.centre.CentreRepository;
import au.awh.model.CentreTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Centre Details' (file 'Centre' (WSCENTREP)
 *
 * @author X2EGenerator
 */
@Service
public class GetCentreDetailsService extends AbstractService<GetCentreDetailsService, GetCentreDetailsDTO>
{
    private final Step execute = define("execute", GetCentreDetailsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private CentreRepository centreRepository;

	

    @Autowired
    public GetCentreDetailsService()
    {
        super(GetCentreDetailsService.class, GetCentreDetailsDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetCentreDetailsParams params) throws ServiceException {
        GetCentreDetailsDTO dto = new GetCentreDetailsDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetCentreDetailsDTO dto, GetCentreDetailsParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<Centre> centreList = centreRepository.findAllBy();
		if (!centreList.isEmpty()) {
			for (Centre centre : centreList) {
				processDataRecord(dto, centre);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetCentreDetailsResult result = new GetCentreDetailsResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetCentreDetailsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetCentreDetailsDTO dto, Centre centre) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = DB1 By name
		dto.setCentreName(centre.getCentreName());
		dto.setCentreType(centre.getCentreType());
		dto.setStateCode(centre.getStateCode());
		dto.setCentreRegion(centre.getCentreRegion());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetCentreDetailsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = CON By name
		dto.setCentreName("");
		dto.setCentreType(null);
		dto.setStateCode(StateCodeEnum._NOT_ENTERED);
		dto.setCentreRegion("");
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetCentreDetailsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
