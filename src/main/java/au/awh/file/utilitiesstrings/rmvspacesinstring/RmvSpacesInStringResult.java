package au.awh.file.utilitiesstrings.rmvspacesinstring;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: RmvSpacesInString ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RmvSpacesInStringResult implements Serializable
{
	private static final long serialVersionUID = 9018059251407470995L;

	private ReturnCodeEnum _sysReturnCode;
	private String string280 = ""; // String 36563

	public String getString280() {
		return string280;
	}
	
	public void setString280(String string280) {
		this.string280 = string280;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
