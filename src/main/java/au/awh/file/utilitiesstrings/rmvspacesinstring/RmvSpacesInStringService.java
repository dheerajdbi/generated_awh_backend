package au.awh.file.utilitiesstrings.rmvspacesinstring;

// ExecuteInternalFunction.kt File=UtilitiesStrings (G6) Function=rmvSpacesInString (1924483) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.ReturnCodeEnum;

/**
 * EXCINTFNC Service controller for 'Rmv spaces in String' (file 'Utilities Strings' (WSG6RCP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class RmvSpacesInStringService extends AbstractService<RmvSpacesInStringService, RmvSpacesInStringDTO>
{
	private final Step execute = define("execute", RmvSpacesInStringParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public RmvSpacesInStringService() {
		super(RmvSpacesInStringService.class, RmvSpacesInStringDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(RmvSpacesInStringParams params) throws ServiceException {
		RmvSpacesInStringDTO dto = new RmvSpacesInStringDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(RmvSpacesInStringDTO dto, RmvSpacesInStringParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000143 ACT LCL.String 280 = CND.Not entered
		dto.setLclString280("");
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000105 ACT LCL.Count = CND.equal to  1
		dto.setLclCount(1);
		// DEBUG genFunctionCall END
		while (
		// DEBUG ComparatorJavaGenerator BEGIN
		dto.getLclCount() <= dto.getStringLength()
		// DEBUG ComparatorJavaGenerator END
		) {
			// DEBUG genFunctionCall BEGIN 1000010 ACT LCL.Character = SUBSTRING(PAR.String 280,LCL.Count,CON.1)
			dto.setLclCharacter(StringUtils.substring(dto.getString280(), (int)(int) dto.getLclCount() - 1, 1));
			// DEBUG genFunctionCall END
			if (!dto.getLclCharacter().equals("")) {
				// LCL.Character is Entered
				// DEBUG genFunctionCall BEGIN 1000113 ACT LCL.String 280 = CONCAT(LCL.String 280,LCL.Character,CND.*None)
				dto.setLclString280(dto.getLclString280().trim().concat(dto.getLclCharacter()));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000005 ACT LCL.Count = LCL.Count + CON.1
			dto.setLclCount(dto.getLclCount() + 1);
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1000123 ACT PAR.String 280 = LCL.String 280
		dto.setString280(dto.getLclString280());
		// DEBUG genFunctionCall END

		RmvSpacesInStringResult result = new RmvSpacesInStringResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
