package au.awh.file.utilitiesstrings.rmvspacesinstring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RmvSpacesInStringDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String string280;
	private long stringLength;

	private String lclCharacter;
	private long lclCount;
	private String lclString280;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getString280() {
		return string280;
	}

	public long getStringLength() {
		return stringLength;
	}

	public String getLclCharacter() {
		return lclCharacter;
	}

	public long getLclCount() {
		return lclCount;
	}

	public String getLclString280() {
		return lclString280;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setString280(String string280) {
		this.string280 = string280;
	}

	public void setStringLength(long stringLength) {
		this.stringLength = stringLength;
	}

	public void setLclCharacter(String lclCharacter) {
		this.lclCharacter = lclCharacter;
	}

	public void setLclCount(long lclCount) {
		this.lclCount = lclCount;
	}

	public void setLclString280(String lclString280) {
		this.lclString280 = lclString280;
	}

}
