package au.awh.file.utilitiesstrings.rmvspacesinstring;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: RmvSpacesInString ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RmvSpacesInStringParams implements Serializable
{
	private static final long serialVersionUID = 437298271425154342L;

    private String string280 = "";
    private long stringLength = 0L;

	public String getString280() {
		return string280;
	}
	
	public void setString280(String string280) {
		this.string280 = string280;
	}
	
	public long getStringLength() {
		return stringLength;
	}
	
	public void setStringLength(long stringLength) {
		this.stringLength = stringLength;
	}
	
	public void setStringLength(String stringLength) {
		setStringLength(Long.parseLong(stringLength));
	}
}
