package au.awh.file.utilitiesos400api.rtvusrprfdetails;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: RtvusrprfDetails (WRTVUSRPRF).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RtvusrprfDetailsResult implements Serializable
{
	private static final long serialVersionUID = -8687867913718054647L;

	private ReturnCodeEnum _sysReturnCode;
	private String userProfileNameText = ""; // String 51868
	private ReturnCodeEnum returnCode = null; // ReturnCodeEnum 149
	private String outputQueue = ""; // String 34671
	private String jobDescription = ""; // String 35719
	private String jobDescriptionLibrary = ""; // String 56413
	private String userInitialProgram = ""; // String 56426
	private String userInitialProgramLib = ""; // String 56427

	public String getUserProfileNameText() {
		return userProfileNameText;
	}
	
	public void setUserProfileNameText(String userProfileNameText) {
		this.userProfileNameText = userProfileNameText;
	}
	
	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}
	
	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
	
	public void setReturnCode(String returnCode) {
		setReturnCode(ReturnCodeEnum.valueOf(returnCode));
	}
	
	public String getOutputQueue() {
		return outputQueue;
	}
	
	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}
	
	public String getJobDescription() {
		return jobDescription;
	}
	
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	
	public String getJobDescriptionLibrary() {
		return jobDescriptionLibrary;
	}
	
	public void setJobDescriptionLibrary(String jobDescriptionLibrary) {
		this.jobDescriptionLibrary = jobDescriptionLibrary;
	}
	
	public String getUserInitialProgram() {
		return userInitialProgram;
	}
	
	public void setUserInitialProgram(String userInitialProgram) {
		this.userInitialProgram = userInitialProgram;
	}
	
	public String getUserInitialProgramLib() {
		return userInitialProgramLib;
	}
	
	public void setUserInitialProgramLib(String userInitialProgramLib) {
		this.userInitialProgramLib = userInitialProgramLib;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
