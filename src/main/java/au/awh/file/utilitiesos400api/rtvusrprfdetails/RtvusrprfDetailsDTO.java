package au.awh.file.utilitiesos400api.rtvusrprfdetails;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvusrprfDetailsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ReturnCodeEnum returnCode;
	private String jobDescription;
	private String jobDescriptionLibrary;
	private String outputQueue;
	private String userId;
	private String userInitialProgram;
	private String userInitialProgramLib;
	private String userProfileNameText;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public String getJobDescriptionLibrary() {
		return jobDescriptionLibrary;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserInitialProgram() {
		return userInitialProgram;
	}

	public String getUserInitialProgramLib() {
		return userInitialProgramLib;
	}

	public String getUserProfileNameText() {
		return userProfileNameText;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public void setJobDescriptionLibrary(String jobDescriptionLibrary) {
		this.jobDescriptionLibrary = jobDescriptionLibrary;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserInitialProgram(String userInitialProgram) {
		this.userInitialProgram = userInitialProgram;
	}

	public void setUserInitialProgramLib(String userInitialProgramLib) {
		this.userInitialProgramLib = userInitialProgramLib;
	}

	public void setUserProfileNameText(String userProfileNameText) {
		this.userProfileNameText = userProfileNameText;
	}

}
