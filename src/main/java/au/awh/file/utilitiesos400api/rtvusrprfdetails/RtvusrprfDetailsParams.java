package au.awh.file.utilitiesos400api.rtvusrprfdetails;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: RtvusrprfDetails (WRTVUSRPRF).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RtvusrprfDetailsParams implements Serializable
{
	private static final long serialVersionUID = 926688394023028689L;

    private String userId = "";
    private ReturnCodeEnum returnCode = null;

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}
	
	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}
	
	public void setReturnCode(String returnCode) {
		setReturnCode(ReturnCodeEnum.valueOf(returnCode));
	}
}
