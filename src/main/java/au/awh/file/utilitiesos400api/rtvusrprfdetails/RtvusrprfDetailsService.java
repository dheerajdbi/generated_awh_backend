package au.awh.file.utilitiesos400api.rtvusrprfdetails;

// ExecuteUserProgramFunction.kt File=UtilitiesOs400Api (G8) Function=rtvusrprfDetails (1744971) WRTVUSRPRF

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.ReturnCodeEnum;

/**
 * EXCUSRPGM Service controller for 'RTVUSRPRF Details' (file 'Utilities OS/400 API' (WSG8RCP)
 *
 * @author X2EGenerator  ExecuteUserProgramFunction.kt
 */
@Service
public class RtvusrprfDetailsService extends AbstractService<RtvusrprfDetailsService, RtvusrprfDetailsDTO>
{
	private final Step execute = define("execute", RtvusrprfDetailsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public RtvusrprfDetailsService() {
		super(RtvusrprfDetailsService.class, RtvusrprfDetailsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(RtvusrprfDetailsParams params) throws ServiceException {
		RtvusrprfDetailsDTO dto = new RtvusrprfDetailsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(RtvusrprfDetailsDTO dto, RtvusrprfDetailsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Empty:2)
		 */
		

		RtvusrprfDetailsResult result = new RtvusrprfDetailsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
