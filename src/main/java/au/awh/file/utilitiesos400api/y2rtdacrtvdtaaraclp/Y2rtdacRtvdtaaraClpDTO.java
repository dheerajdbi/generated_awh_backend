package au.awh.file.utilitiesos400api.y2rtdacrtvdtaaraclp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class Y2rtdacRtvdtaaraClpDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String dtaaraName;
	private String dtaaraReturnValue;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getDtaaraName() {
		return dtaaraName;
	}

	public String getDtaaraReturnValue() {
		return dtaaraReturnValue;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setDtaaraName(String dtaaraName) {
		this.dtaaraName = dtaaraName;
	}

	public void setDtaaraReturnValue(String dtaaraReturnValue) {
		this.dtaaraReturnValue = dtaaraReturnValue;
	}

}
