package au.awh.file.utilitiesos400api.y2rtdacrtvdtaaraclp;

// ExecuteUserProgramFunction.kt File=UtilitiesOs400Api (G8) Function=y2rtdacRtvdtaaraClp (1349872) Y2RTDAC

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.ReturnCodeEnum;

/**
 * EXCUSRPGM Service controller for 'Y2RTDAC RTVDTAARA     CLP' (file 'Utilities OS/400 API' (WSG8RCP)
 *
 * @author X2EGenerator  ExecuteUserProgramFunction.kt
 */
@Service
public class Y2rtdacRtvdtaaraClpService extends AbstractService<Y2rtdacRtvdtaaraClpService, Y2rtdacRtvdtaaraClpDTO>
{
	private final Step execute = define("execute", Y2rtdacRtvdtaaraClpParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public Y2rtdacRtvdtaaraClpService() {
		super(Y2rtdacRtvdtaaraClpService.class, Y2rtdacRtvdtaaraClpDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(Y2rtdacRtvdtaaraClpParams params) throws ServiceException {
		Y2rtdacRtvdtaaraClpDTO dto = new Y2rtdacRtvdtaaraClpDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(Y2rtdacRtvdtaaraClpDTO dto, Y2rtdacRtvdtaaraClpParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Empty:2)
		 */
		

		Y2rtdacRtvdtaaraClpResult result = new Y2rtdacRtvdtaaraClpResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
