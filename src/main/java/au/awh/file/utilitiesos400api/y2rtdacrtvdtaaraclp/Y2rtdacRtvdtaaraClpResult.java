package au.awh.file.utilitiesos400api.y2rtdacrtvdtaaraclp;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: Y2rtdacRtvdtaaraClp (Y2RTDAC).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class Y2rtdacRtvdtaaraClpResult implements Serializable
{
	private static final long serialVersionUID = 967503877491475019L;

	private ReturnCodeEnum _sysReturnCode;
	private String dtaaraReturnValue = ""; // String 31573

	public String getDtaaraReturnValue() {
		return dtaaraReturnValue;
	}
	
	public void setDtaaraReturnValue(String dtaaraReturnValue) {
		this.dtaaraReturnValue = dtaaraReturnValue;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
