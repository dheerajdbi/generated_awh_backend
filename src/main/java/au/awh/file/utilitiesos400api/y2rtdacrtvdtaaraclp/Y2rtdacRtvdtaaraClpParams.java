package au.awh.file.utilitiesos400api.y2rtdacrtvdtaaraclp;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: Y2rtdacRtvdtaaraClp (Y2RTDAC).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class Y2rtdacRtvdtaaraClpParams implements Serializable
{
	private static final long serialVersionUID = -3986716449155037191L;

    private String dtaaraName = "";

	public String getDtaaraName() {
		return dtaaraName;
	}
	
	public void setDtaaraName(String dtaaraName) {
		this.dtaaraName = dtaaraName;
	}
}
