package au.awh.file.awhregion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: AWH Region (WSREG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface AwhRegionRepository extends AwhRegionRepositoryCustom, JpaRepository<AwhRegion, AwhRegionId> {

	List<AwhRegion> findAllByIdAwhRegionCode(String awhRegionCode);
}
