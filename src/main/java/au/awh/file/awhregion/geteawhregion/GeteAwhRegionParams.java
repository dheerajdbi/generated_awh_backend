package au.awh.file.awhregion.geteawhregion;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteAwhRegionParams implements Serializable
{
	private static final long serialVersionUID = 4262858735722578486L;

    private String awhRegionCode = "";
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.fromCode(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.fromCode(getRecordOption));
	}
}
