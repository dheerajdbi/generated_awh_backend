package au.awh.file.awhregion.geteawhregion;

// ExecuteInternalFunction.kt File=AwhRegion (AR) Function=geteAwhRegion (1878542) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.AwhRegionId;
import au.awh.file.awhregion.AwhRegionRepository;
// imports for Services
import au.awh.file.awhregion.getawhregion.GetAwhRegionService;
// imports for DTO
import au.awh.file.awhregion.getawhregion.GetAwhRegionDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.awhregion.getawhregion.GetAwhRegionParams;
// imports for Results
import au.awh.file.awhregion.getawhregion.GetAwhRegionResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE AWH Region' (file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteAwhRegionService extends AbstractService<GeteAwhRegionService, GeteAwhRegionDTO>
{
	private final Step execute = define("execute", GeteAwhRegionParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionRepository awhRegionRepository;

	@Autowired
	private GetAwhRegionService getAwhRegionService;

	//private final Step serviceGetAwhRegion = define("serviceGetAwhRegion",GetAwhRegionResult.class, this::processServiceGetAwhRegion);
	
	@Autowired
	public GeteAwhRegionService() {
		super(GeteAwhRegionService.class, GeteAwhRegionDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteAwhRegionParams params) throws ServiceException {
		GeteAwhRegionDTO dto = new GeteAwhRegionDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteAwhRegionDTO dto, GeteAwhRegionParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetAwhRegionResult getAwhRegionResult = null;
		GetAwhRegionParams getAwhRegionParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get AWH Region - AWH Region  *
		getAwhRegionParams = new GetAwhRegionParams();
		getAwhRegionResult = new GetAwhRegionResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getAwhRegionParams);
		getAwhRegionParams.setAwhRegionCode(dto.getAwhRegionCode());
		stepResult = getAwhRegionService.execute(getAwhRegionParams);
		getAwhRegionResult = (GetAwhRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getAwhRegionResult.get_SysReturnCode());
		dto.setAwhRegionName(getAwhRegionResult.getAwhRegionName());
		dto.setAwhRegionOrg(getAwhRegionResult.getAwhRegionOrg());
		dto.setAwhRegionContact(getAwhRegionResult.getAwhRegionContact());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// * NOTE: Insert <file> NF message here.
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'AWH Region             NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878043) EXCINTFUN 1878542 AwhRegion.geteAwhRegion 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878542 AwhRegion.geteAwhRegion 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteAwhRegionResult result = new GeteAwhRegionResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetAwhRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetAwhRegion(GeteAwhRegionDTO dto, GetAwhRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteAwhRegionParams geteAwhRegionParams = new GeteAwhRegionParams();
//        BeanUtils.copyProperties(dto, geteAwhRegionParams);
//        stepResult = StepResult.callService(GeteAwhRegionService.class, geteAwhRegionParams);
//
//        return stepResult;
//    }
//
}
