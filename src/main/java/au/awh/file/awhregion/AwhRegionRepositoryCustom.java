package au.awh.file.awhregion;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.awhregion.selectawhregion.SelectAwhRegionGDO;
import au.awh.file.awhregion.editawhregion.EditAwhRegionGDO;

/**
 * Custom Spring Data JPA repository interface for model: AWH Region (WSREG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface AwhRegionRepositoryCustom {

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 */
	void deleteAwhRegion(String awhRegionCode);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectAwhRegionGDO
	 */
	RestResponsePage<SelectAwhRegionGDO> selectAwhRegion(String awhRegionCode
	, Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EditAwhRegionGDO
	 */
	RestResponsePage<EditAwhRegionGDO> editAwhRegion(
	Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @return AwhRegion
	 */
	AwhRegion getAwhRegion(String awhRegionCode
	);
}
