package au.awh.file.awhregion.createawhregion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreateAwhRegionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String awhRegionCode;
	private String awhRegionName;
	private String awhRegionOrg;
	private long awhRegionContact;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public long getAwhRegionContact() {
		return awhRegionContact;
	}

	public String getAwhRegionName() {
		return awhRegionName;
	}

	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setAwhRegionContact(long awhRegionContact) {
		this.awhRegionContact = awhRegionContact;
	}

	public void setAwhRegionName(String awhRegionName) {
		this.awhRegionName = awhRegionName;
	}

	public void setAwhRegionOrg(String awhRegionOrg) {
		this.awhRegionOrg = awhRegionOrg;
	}

}
