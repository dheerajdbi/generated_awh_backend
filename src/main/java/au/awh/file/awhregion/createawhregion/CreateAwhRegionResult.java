package au.awh.file.awhregion.createawhregion;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CreateAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreateAwhRegionResult implements Serializable
{
	private static final long serialVersionUID = 4049015003507672239L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
