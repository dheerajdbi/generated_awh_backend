package au.awh.file.awhregion.createawhregion;

// CreateObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.AwhRegionId;
import au.awh.file.awhregion.AwhRegionRepository;
import au.awh.model.ReturnCodeEnum;

/**
 * CRTOBJ Service controller for 'Create AWH Region' (file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator  CreateObjectFunction.kt
 */
@Service
public class CreateAwhRegionService  extends AbstractService<CreateAwhRegionService, CreateAwhRegionDTO>
{
	private final Step execute = define("execute", CreateAwhRegionParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionRepository awhRegionRepository;

	
	@Autowired
	public CreateAwhRegionService() {
		super(CreateAwhRegionService.class, CreateAwhRegionDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CreateAwhRegionParams params) throws ServiceException {
		CreateAwhRegionDTO dto = new CreateAwhRegionDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CreateAwhRegionDTO dto, CreateAwhRegionParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		AwhRegion awhRegion = new AwhRegion();
		awhRegion.setAwhRegionCode(dto.getAwhRegionCode());
		awhRegion.setAwhRegionName(dto.getAwhRegionName());
		awhRegion.setAwhRegionOrg(dto.getAwhRegionOrg());
		awhRegion.setAwhRegionContact(dto.getAwhRegionContact());

		processingBeforeDataUpdate(dto, awhRegion);

		AwhRegion awhRegion2 = awhRegionRepository.findById(awhRegion.getId()).orElse(null);
		if (awhRegion2 != null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
			processingIfDataRecordAlreadyExists(dto, awhRegion2);
		}
		else {
			try {
				awhRegionRepository.saveAndFlush(awhRegion);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto, awhRegion);
			} catch (Exception e) {
				System.out.println("Create error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
				processingIfDataUpdateError(dto, awhRegion);
			}
		}

		CreateAwhRegionResult result = new CreateAwhRegionResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(CreateAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordAlreadyExists(CreateAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(CreateAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataUpdateError(CreateAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		
		return stepResult;
	}


}
