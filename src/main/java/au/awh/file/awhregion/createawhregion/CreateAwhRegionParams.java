package au.awh.file.awhregion.createawhregion;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CreateAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreateAwhRegionParams implements Serializable
{
	private static final long serialVersionUID = 1900378261941207770L;

    private String awhRegionCode = "";
    private String awhRegionName = "";
    private String awhRegionOrg = "";
    private long awhRegionContact = 0L;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getAwhRegionName() {
		return awhRegionName;
	}
	
	public void setAwhRegionName(String awhRegionName) {
		this.awhRegionName = awhRegionName;
	}
	
	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}
	
	public void setAwhRegionOrg(String awhRegionOrg) {
		this.awhRegionOrg = awhRegionOrg;
	}
	
	public long getAwhRegionContact() {
		return awhRegionContact;
	}
	
	public void setAwhRegionContact(long awhRegionContact) {
		this.awhRegionContact = awhRegionContact;
	}
	
	public void setAwhRegionContact(String awhRegionContact) {
		setAwhRegionContact(Long.parseLong(awhRegionContact));
	}
}
