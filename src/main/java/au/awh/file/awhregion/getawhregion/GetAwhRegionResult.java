package au.awh.file.awhregion.getawhregion;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetAwhRegionResult implements Serializable
{
	private static final long serialVersionUID = 2751295656895398224L;

	private ReturnCodeEnum _sysReturnCode;
	private String awhRegionName = ""; // String 56453
	private String awhRegionOrg = ""; // String 56461
	private long awhRegionContact = 0L; // long 56462

	public String getAwhRegionName() {
		return awhRegionName;
	}
	
	public void setAwhRegionName(String awhRegionName) {
		this.awhRegionName = awhRegionName;
	}
	
	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}
	
	public void setAwhRegionOrg(String awhRegionOrg) {
		this.awhRegionOrg = awhRegionOrg;
	}
	
	public long getAwhRegionContact() {
		return awhRegionContact;
	}
	
	public void setAwhRegionContact(long awhRegionContact) {
		this.awhRegionContact = awhRegionContact;
	}
	
	public void setAwhRegionContact(String awhRegionContact) {
		setAwhRegionContact(Long.parseLong(awhRegionContact));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
