package au.awh.file.awhregion.getawhregion;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetAwhRegionParams implements Serializable
{
	private static final long serialVersionUID = -855719222711598182L;

    private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}
