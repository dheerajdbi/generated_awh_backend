package au.awh.file.awhregion;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSREG")
public class AwhRegion implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private AwhRegionId id = new AwhRegionId();

	@Column(name = "ARREGCNT")
	private long awhRegionContact = 0L;
	
	@Column(name = "ARAWHREGN")
	private String awhRegionName = "";
	
	@Column(name = "ARREGORG")
	private String awhRegionOrg = "";

	public AwhRegionId getId() {
		return id;
	}

	public String getAwhRegionCode() {
		return id.getAwhRegionCode();
	}

	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}
	
	public String getAwhRegionName() {
		return awhRegionName;
	}
	
	public long getAwhRegionContact() {
		return awhRegionContact;
	}

	

	

	public void setAwhRegionCode(String awhRegionCode) {
		this.id.setAwhRegionCode(awhRegionCode);
	}

	public void setAwhRegionOrg(String awhRegionOrg) {
		this.awhRegionOrg = awhRegionOrg;
	}
	
	public void setAwhRegionName(String awhRegionName) {
		this.awhRegionName = awhRegionName;
	}
	
	public void setAwhRegionContact(long awhRegionContact) {
		this.awhRegionContact = awhRegionContact;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
