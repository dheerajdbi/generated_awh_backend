package au.awh.file.awhregion;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionGDO;
import au.awh.file.awhregion.editawhregion.EditAwhRegionGDO;

/**
 * Custom Spring Data JPA repository implementation for model: AWH Region (WSREG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class AwhRegionRepositoryImpl implements AwhRegionRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.awhregion.AwhRegionService#deleteAwhRegion(AwhRegion)
	 */
	@Override
	public void deleteAwhRegion(
		String awhRegionCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " awhRegion.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM AwhRegion awhRegion";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<SelectAwhRegionGDO> selectAwhRegion(
		String awhRegionCode
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " awhRegion.id.awhRegionCode like CONCAT('%', :awhRegionCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.awhregion.selectawhregion.SelectAwhRegionGDO(awhRegion.id.awhRegionCode, awhRegion.awhRegionName, awhRegion.awhRegionOrg, awhRegion.awhRegionContact) from AwhRegion awhRegion";
		String countQueryString = "SELECT COUNT(awhRegion.id.awhRegionCode) FROM AwhRegion awhRegion";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			AwhRegion awhRegion = new AwhRegion();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(awhRegion.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"awhRegion.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"awhRegion." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"awhRegion.id.awhRegionCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectAwhRegionGDO> content = query.getResultList();
		RestResponsePage<SelectAwhRegionGDO> pageGdo = new RestResponsePage<SelectAwhRegionGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<EditAwhRegionGDO> editAwhRegion(
		
Pageable pageable) {
		String sqlString = "SELECT new au.awh.file.awhregion.editawhregion.EditAwhRegionGDO(awhRegion.id.awhRegionCode, awhRegion.awhRegionName, awhRegion.awhRegionOrg, awhRegion.awhRegionContact) from AwhRegion awhRegion";
		String countQueryString = "SELECT COUNT(awhRegion.id.awhRegionCode) FROM AwhRegion awhRegion";

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			AwhRegion awhRegion = new AwhRegion();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(awhRegion.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"awhRegion.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"awhRegion." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"awhRegion.id.awhRegionCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EditAwhRegionGDO> content = query.getResultList();
		RestResponsePage<EditAwhRegionGDO> pageGdo = new RestResponsePage<EditAwhRegionGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.awhregion.AwhRegionService#getAwhRegion(String)
	 */
	@Override
	public AwhRegion getAwhRegion(
		String awhRegionCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " awhRegion.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		String sqlString = "SELECT awhRegion FROM AwhRegion awhRegion";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		AwhRegion dto = (AwhRegion)query.getSingleResult();

		return dto;
	}

}
