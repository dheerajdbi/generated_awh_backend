package au.awh.file.awhregion.editawhregion;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerMapping;


import org.springframework.stereotype.Service;

import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;


import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.AwhRegionId;
import au.awh.file.awhregion.AwhRegionRepository;


import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionService;
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionParams;
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionResult;

import au.awh.file.awhregion.createawhregion.CreateAwhRegionService;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionParams;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionResult;

import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionService;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionParams;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionResult;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionService;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionService;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionService;
import au.awh.file.organisationcontact.geteorganisationcontact.GeteOrganisationContactService;
// asServiceDtoImportStatement
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionDTO;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionDTO;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionDTO;
import au.awh.file.organisationcontact.geteorganisationcontact.GeteOrganisationContactDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionParams;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionParams;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionParams;
import au.awh.file.organisationcontact.geteorganisationcontact.GeteOrganisationContactParams;
// asServiceResultImportStatement
import au.awh.file.awhregion.changeawhregion.ChangeAwhRegionResult;
import au.awh.file.awhregion.createawhregion.CreateAwhRegionResult;
import au.awh.file.awhregion.deleteawhregion.DeleteAwhRegionResult;
import au.awh.file.organisationcontact.geteorganisationcontact.GeteOrganisationContactResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END



import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * EDTFIL controller for 'Edit AWH Region' (WSPLTWIEFK) of file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
@Service
public class EditAwhRegionService extends AbstractService<EditAwhRegionService, EditAwhRegionState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private AwhRegionRepository awhRegionRepository;	
	@Autowired
	private ChangeAwhRegionService changeAwhRegionService;
	
	@Autowired
	private CreateAwhRegionService createAwhRegionService;
	
	@Autowired
	private DeleteAwhRegionService deleteAwhRegionService;
	
	@Autowired
	private GeteOrganisationContactService geteOrganisationContactService;
	

    @Autowired
    private EditAwhRegionService editAwhRegionService;

    @Autowired
    private MessageSource messageSource;

//    @Autowired
//    private EditAwhRegionValidator editAwhRegionValidator;


    
	public static final String SCREEN_CTL = "editAwhRegion";
    public static final String SCREEN_RCD = "EditAwhRegion.rcd";
    public static final String SCREEN_CFM = "EditAwhRegion.confirm";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", EditAwhRegionParams.class, this::executeService);
    private final Step response = define("ctlScreen", EditAwhRegionState.class, this::processResponse);
	private final Step confirmScreenResponse = define("cfmscreen", EditAwhRegionDTO.class, this::processConfirmScreenResponse);

	//private final Step serviceCreateAwhRegion = define("serviceCreateAwhRegion",CreateAwhRegionResult.class, this::processServiceCreateAwhRegion);
	//private final Step serviceDeleteAwhRegion = define("serviceDeleteAwhRegion",DeleteAwhRegionResult.class, this::processServiceDeleteAwhRegion);
	//private final Step serviceChangeAwhRegion = define("serviceChangeAwhRegion",ChangeAwhRegionResult.class, this::processServiceChangeAwhRegion);
	//private final Step serviceGeteOrganisationContact = define("serviceGeteOrganisationContact",GeteOrganisationContactResult.class, this::processServiceGeteOrganisationContact);
	

    
    @Autowired
    public EditAwhRegionService() {
        super(EditAwhRegionService.class, EditAwhRegionState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * EDTFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EditAwhRegionState state, EditAwhRegionParams params) {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, state);
        usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN  initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = usrInitializeSubfileHeader(state);

        dbfReadFirstDataRecord(state);
        if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
            state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
        } else {
            state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        stepResult = loadNextSubfilePage(state);

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            for (EditAwhRegionGDO gdo : state.getPageGdo().getContent()) {
                stepResult = usrInitializeSubfileRecordExistingRecord(state, gdo);
                validateSubfileRecord(state, gdo);
                stepResult = dbfUpdateDataRecord(state, gdo);
            }
        } else {
            //whsetCon()
            EditAwhRegionGDO gdo = new EditAwhRegionGDO();
            stepResult = usrInitializeSubfileRecordNewRecord(state, gdo);
            validateSubfileRecord(state, gdo);
            stepResult = dbfUpdateDataRecord(state, gdo);
        }

        return stepResult;
    }

    /**
     * SCREEN  validate subfile record.
     *
     * @param gdo - subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileRecord(EditAwhRegionState state, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            EditAwhRegionDTO dto = new EditAwhRegionDTO();
            BeanUtils.copyProperties(state, dto);
            stepResult = callScreen(SCREEN_CTL, dto).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_KEY returned response processing method.
     *
     * @param state      - Service state class.
     * @param fromScreen - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(EditAwhRegionState state, EditAwhRegionDTO fromScreen) {
        StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(fromScreen, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = processHelpRequest(state); //synon built-in function
        } else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
        }

    private StepResult processHelpRequest(EditAwhRegionState state) {
        return NO_ACTION;
    }

    /**
     * SCREEN process screen.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = validateSubfileControl(state);
        if (state.get_SysErrorFound()) {
            return stepResult;
        }

        for (EditAwhRegionGDO gdo : state.getPageGdo().getContent()) {
            if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                stepResult = checkFields(gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = usrValidateSubfileRecordFields(state, gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = checkRelations(gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = usrSubfileRecordFunctionFields(state, gdo);
                stepResult = usrValidateSubfileRecordRelations(state, gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = dbfUpdateDataRecord(state, gdo);
            }
        }

        EditAwhRegionDTO dto = new EditAwhRegionDTO();
        BeanUtils.copyProperties(state, dto);
        //bypass confirm screen for now
        state.setConfirm(true);
        EditAwhRegionDTO model = new EditAwhRegionDTO();
        BeanUtils.copyProperties(state,model);
        stepResult = processConfirmScreenResponse(state, model);
        //stepResult = callScreen(SCREEN_CFM, dto).thenCall(confirmScreenResponse);

        return stepResult;
    }

    /**
     * SCREEN  check the fields of GDO.
     *
     * @param gdo -  Service subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult checkFields(EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  check the relations of GDO.
     *
     * @param gdo -  Service subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult checkRelations(EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  validate subfile control.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileControl(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        validateSubfileControlField(state);
        if (state.get_SysErrorFound()) {
            return stepResult;
        }
        usrSubfileControlFunctionFields(state);
        stepResult = usrValidateSubfileControl(state);

        return stepResult;
    }

    /**
     * SCREEN  validate subfile control field.
     *
     * @param state -  Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileControlField(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN_CONFIRM returned response processing method.
     *
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processConfirmScreenResponse(EditAwhRegionState state, EditAwhRegionDTO model) {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(model,state);

        if (!state.isConfirm()) {
            return stepResult;
        }
        else {
            state.setConfirm(false);
        }

        for (EditAwhRegionGDO gdo : state.getPageGdo().getContent()) {
            if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                if ("DELETE".equals(gdo.get_SysSelected())) {
                    stepResult = dbfDeleteDataRecord(state, gdo);
                } else {
                    if ("CHANGE".equals(gdo.get_SysSelected())) {
                        stepResult = dbfUpdateDataRecord(state, gdo);
                    } else {
                        //if *Program mode is *ADD
                        stepResult = dbfCreateDataRecord(state, gdo);
                    }
                }
            }
            stepResult = usrExtraProcessingAfterDBFUpdate(state);
        }

        if (state.get_SysErrorFound()) {
            return stepResult;
        }
        if (state.get_SysReloadSubfile() == ReloadSubfileEnum._STA_YES) {
            // request subfile reload if necessary
        }
        if (isChangeMode(state.get_SysCmdKey())) {
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            } else {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
        } else {
            for (EditAwhRegionGDO gdo : state.getPageGdo().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = stepResult = usrProcessCommandKeys(state, gdo);
                }
            }
            if (stepResult != StepResult.NO_ACTION) {
                return stepResult;
            }
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EditAwhRegionState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        EditAwhRegionResult params = new EditAwhRegionResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(EditAwhRegionState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
    private void dbfReadNextPageRecord(EditAwhRegionState state) {
        state.setPage(state.getPage() + 1);
        dbfReadDataRecord(state);
    }

    /**
     * Read record data.
     * @param state - Service state class.
     */
    private void dbfReadDataRecord(EditAwhRegionState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<EditAwhRegionGDO> pageGdo = awhRegionRepository.editAwhRegion(pageable);
        state.setPageGdo(pageGdo);
    }


    /**
     * Create record data.
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfCreateDataRecord(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CRTOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Create Object (Generated:1387)
			 */
			CreateAwhRegionResult createAwhRegionResult = null;
			CreateAwhRegionParams createAwhRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1388 ACT Create AWH Region - AWH Region  *
			createAwhRegionParams = new CreateAwhRegionParams();
			createAwhRegionResult = new CreateAwhRegionResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), createAwhRegionParams);
			stepResult = createAwhRegionService.execute(createAwhRegionParams);
			createAwhRegionResult = (CreateAwhRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(createAwhRegionResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }


    /**
     * Delete record data
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfDeleteDataRecord(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val functions = x2EFile.functions.filter { it.functionType == "DLTOBJ" }

//        val dbfOBJ: X2EFunction = if (functions.isNotEmpty()) getFunctionForType(x2EFile, "DLTOBJ") else function
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Delete Object (Generated:1389)
			 */
			DeleteAwhRegionResult deleteAwhRegionResult = null;
			DeleteAwhRegionParams deleteAwhRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1390 ACT Delete AWH Region - AWH Region  *
			deleteAwhRegionParams = new DeleteAwhRegionParams();
			deleteAwhRegionResult = new DeleteAwhRegionResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), deleteAwhRegionParams);
			stepResult = deleteAwhRegionService.execute(deleteAwhRegionParams);
			deleteAwhRegionResult = (DeleteAwhRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(deleteAwhRegionResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }


    /**
     * Update record data
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfUpdateDataRecord(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CHGOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
			AwhRegionId awhRegionId = new AwhRegionId(gdo.getAwhRegionCode());
			if (awhRegionRepository.existsById(awhRegionId)) {
			
        		/**
				 * USER: Change Object (Generated:1391)
				 */
				ChangeAwhRegionParams changeAwhRegionParams = null;
				ChangeAwhRegionResult changeAwhRegionResult = null;
				// DEBUG genFunctionCall BEGIN 1392 ACT Change AWH Region - AWH Region  *
				changeAwhRegionParams = new ChangeAwhRegionParams();
				changeAwhRegionResult = new ChangeAwhRegionResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), changeAwhRegionParams);
				stepResult = changeAwhRegionService.execute(changeAwhRegionParams);
				changeAwhRegionResult = (ChangeAwhRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(changeAwhRegionResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
		
            }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
	}


    
	/**
	 * USER: Initialize Program (Generated:15)
	 */
    private StepResult usrInitializeProgram(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 15 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Header (Generated:1488)
	 */
    private StepResult usrInitializeSubfileHeader(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1488 -
        
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record (New Record) (Generated:1027)
	 */
    private StepResult usrInitializeSubfileRecordNewRecord(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1027 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
	 */
    private StepResult usrInitializeSubfileRecordExistingRecord(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1023 -

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:1463)
	 */
    private StepResult usrSubfileControlFunctionFields(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1463 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Control (Generated:37)
	 */
    private StepResult usrValidateSubfileControl(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 37 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Record Fields (Generated:1496)
	 */
    private StepResult usrValidateSubfileRecordFields(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1496 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:1449)
	 */
    private StepResult usrSubfileRecordFunctionFields(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		GeteOrganisationContactParams geteOrganisationContactParams = null;
				GeteOrganisationContactResult geteOrganisationContactResult = null;
				if (gdo.getAwhRegionContact() != 0) {
					// RCD.AWH Region Contact is Entered
					// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Organisation Contact - Organisation Contact  *
					geteOrganisationContactParams = new GeteOrganisationContactParams();
					geteOrganisationContactResult = new GeteOrganisationContactResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteOrganisationContactParams);
					geteOrganisationContactParams.setOrganisation(gdo.getAwhRegionOrg());
					geteOrganisationContactParams.setOrgContactSequence(gdo.getAwhRegionContact());
					geteOrganisationContactParams.setErrorProcessing("2");
					geteOrganisationContactParams.setGetRecordOption("");
					stepResult = geteOrganisationContactService.execute(geteOrganisationContactParams);
					geteOrganisationContactResult = (GeteOrganisationContactResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(geteOrganisationContactResult.get_SysReturnCode());
					gdo.setContactName(geteOrganisationContactResult.getOrgContactName());
					// DEBUG genFunctionCall END
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Record Relations (Generated:1171)
	 */
    private StepResult usrValidateSubfileRecordRelations(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1171 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Extra Processing After DBF Update (Generated:1442)
	 */
    private StepResult usrExtraProcessingAfterDBFUpdate(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1442 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1420)
	 */
    private StepResult usrProcessCommandKeys(EditAwhRegionState dto, EditAwhRegionGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1420 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1262)
	 */
    private StepResult usrExitProgramProcessing(EditAwhRegionState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        		
				// Unprocessed SUB 1262 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * CreateAwhRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateAwhRegion(EditAwhRegionState state, CreateAwhRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EditAwhRegionParams editAwhRegionParams = new EditAwhRegionParams();
//        BeanUtils.copyProperties(state, editAwhRegionParams);
//        stepResult = StepResult.callService(EditAwhRegionService.class, editAwhRegionParams);
//
//        return stepResult;
//    }
////
//    /**
//     * DeleteAwhRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteAwhRegion(EditAwhRegionState state, DeleteAwhRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EditAwhRegionParams editAwhRegionParams = new EditAwhRegionParams();
//        BeanUtils.copyProperties(state, editAwhRegionParams);
//        stepResult = StepResult.callService(EditAwhRegionService.class, editAwhRegionParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChangeAwhRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeAwhRegion(EditAwhRegionState state, ChangeAwhRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EditAwhRegionParams editAwhRegionParams = new EditAwhRegionParams();
//        BeanUtils.copyProperties(state, editAwhRegionParams);
//        stepResult = StepResult.callService(EditAwhRegionService.class, editAwhRegionParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteOrganisationContactService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteOrganisationContact(EditAwhRegionState state, GeteOrganisationContactParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EditAwhRegionParams editAwhRegionParams = new EditAwhRegionParams();
//        BeanUtils.copyProperties(state, editAwhRegionParams);
//        stepResult = StepResult.callService(EditAwhRegionService.class, editAwhRegionParams);
//
//        return stepResult;
//    }
//


}
