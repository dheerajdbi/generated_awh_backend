package au.awh.file.awhregion.editawhregion;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.awhregion.AwhRegion;
// generateStatusFieldImportStatements BEGIN 5
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;

/**
 * Gdo for file 'AWH Region' (WSREG) and function 'Edit AWH Region' (WSPLTWIEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */
public class EditAwhRegionGDO implements Serializable {
	private static final long serialVersionUID = 2576283827442894449L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String awhRegionCode = "";
	private String awhRegionName = "";
	private String awhRegionOrg = "";
	private long awhRegionContact = 0L;
	private String contactName = "";

	public EditAwhRegionGDO() {

	}

   	public EditAwhRegionGDO(String awhRegionCode, String awhRegionName, String awhRegionOrg, long awhRegionContact) {
		this.awhRegionCode = awhRegionCode;
		this.awhRegionName = awhRegionName;
		this.awhRegionOrg = awhRegionOrg;
		this.awhRegionContact = awhRegionContact;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setAwhRegionCode(String awhRegionCode) {
    	this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionName(String awhRegionName) {
    	this.awhRegionName = awhRegionName;
    }

	public String getAwhRegionName() {
		return awhRegionName;
	}

	public void setAwhRegionOrg(String awhRegionOrg) {
    	this.awhRegionOrg = awhRegionOrg;
    }

	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}

	public void setAwhRegionContact(long awhRegionContact) {
    	this.awhRegionContact = awhRegionContact;
    }

	public long getAwhRegionContact() {
		return awhRegionContact;
	}

	public void setContactName(String contactName) {
    	this.contactName = contactName;
    }

	public String getContactName() {
		return contactName;
	}


    /**
     * Copies the fields of the Entity bean into the GDO bean.
     *
     * @param awhRegion AwhRegion Entity bean
     */
    public void setDtoFields(AwhRegion awhRegion) {
  BeanUtils.copyProperties(awhRegion, this);
    }

    /**
     * Copies the fields of the GDO bean into the Entity bean.
     *
     * @param awhRegion AwhRegion Entity bean
     */
    public void setEntityFields(AwhRegion awhRegion) {
      BeanUtils.copyProperties(this, awhRegion);
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}