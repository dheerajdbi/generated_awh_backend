package au.awh.file.awhregion.editawhregion;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.freschelegacy.utils.RestResponsePage;

import au.awh.file.awhregion.AwhRegion;
import au.awh.service.data.BaseDTO;

import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Dto for file 'AWH Region' (WSREG) and function 'Edit AWH Region' (WSPLTWIEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EditAwhRegionDTO extends BaseDTO {
	private static final long serialVersionUID = -8327439375282584398L;

    private RestResponsePage<EditAwhRegionGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;

	private String awhRegionCode = "";


	private EditAwhRegionGDO gdo;

    public EditAwhRegionDTO() {

    }
    public void setPageGdo(RestResponsePage<EditAwhRegionGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<EditAwhRegionGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

    public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setGdo(EditAwhRegionGDO gdo) {
		this.gdo = gdo;
	}

	public EditAwhRegionGDO getGdo() {
		return gdo;
	}

}