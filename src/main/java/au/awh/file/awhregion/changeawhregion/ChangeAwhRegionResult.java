package au.awh.file.awhregion.changeawhregion;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChangeAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeAwhRegionResult implements Serializable
{
	private static final long serialVersionUID = 5749379963256520994L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
