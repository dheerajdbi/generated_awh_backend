package au.awh.file.awhregion.changeawhregion;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.AwhRegionId;
import au.awh.file.awhregion.AwhRegionRepository;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Change AWH Region' (file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChangeAwhRegionService extends AbstractService<ChangeAwhRegionService, ChangeAwhRegionDTO>
{
	private final Step execute = define("execute", ChangeAwhRegionParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionRepository awhRegionRepository;

	
	@Autowired
	public ChangeAwhRegionService() {
		super(ChangeAwhRegionService.class, ChangeAwhRegionDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChangeAwhRegionParams params) throws ServiceException {
		ChangeAwhRegionDTO dto = new ChangeAwhRegionDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChangeAwhRegionDTO dto, ChangeAwhRegionParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		AwhRegionId awhRegionId = new AwhRegionId();
		awhRegionId.setAwhRegionCode(dto.getAwhRegionCode());
		AwhRegion awhRegion = awhRegionRepository.findById(awhRegionId).orElse(null);
		if (awhRegion == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, awhRegion);
			awhRegion.setAwhRegionCode(dto.getAwhRegionCode());
			awhRegion.setAwhRegionName(dto.getAwhRegionName());
			awhRegion.setAwhRegionOrg(dto.getAwhRegionOrg());
			awhRegion.setAwhRegionContact(dto.getAwhRegionContact());

			processingBeforeDataUpdate(dto, awhRegion);

			try {
				awhRegionRepository.saveAndFlush(awhRegion);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChangeAwhRegionResult result = new ChangeAwhRegionResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChangeAwhRegionDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChangeAwhRegionDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChangeAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Empty:48)
		 */
		
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChangeAwhRegionDTO dto, AwhRegion awhRegion) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:10)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChangeAwhRegionDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Empty:23)
		 */
		
		return stepResult;
	}


}
