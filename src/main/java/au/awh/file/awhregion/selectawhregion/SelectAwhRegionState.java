package au.awh.file.awhregion.selectawhregion;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.awhregion.AwhRegion;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'AWH Region' (WSREG) and function 'Select AWH Region' (WSPLTWHSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectAwhRegionState extends SelectAwhRegionDTO {
    private static final long serialVersionUID = -3539354819242472379L;


    // System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public SelectAwhRegionState() {
    }

    public SelectAwhRegionState(AwhRegion awhRegion) {
        setDtoFields(awhRegion);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
    return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
