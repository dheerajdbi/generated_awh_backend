package au.awh.file.awhregion.selectawhregion;
    
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;
import au.awh.support.JobContext;

import java.io.IOException;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;

import au.awh.file.awhregion.AwhRegionRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
// asServiceDtoImportStatement
// asServiceParamsImportStatement
// asServiceResultImportStatement

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 4
// generateStatusFieldImportStatements END

/**
 * SELRCD Service controller for 'Select AWH Region' (WSPLTWHSRK) of file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Service
public class SelectAwhRegionService extends AbstractService<SelectAwhRegionService, SelectAwhRegionState> {
    
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionRepository awhRegionRepository;
        

    
    public static final String SCREEN_SELECT = "selectAwhRegion";

	private final Step execute = define("execute", SelectAwhRegionParams.class, this::executeService);
	private final Step response = define("response", SelectAwhRegionDTO.class, this::processResponse);
	
    

    @Autowired
    public SelectAwhRegionService()
    {
        super(SelectAwhRegionService.class, SelectAwhRegionState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * SelectAwhRegion service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(SelectAwhRegionState state, SelectAwhRegionParams params)
    {
        StepResult stepResult = NO_ACTION;

        if (params != null) {
            BeanUtils.copyProperties(params, state);
        }
        usrInitializeProgram(state);
        stepResult =  mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(SelectAwhRegionState state)
    {
        StepResult stepResult = NO_ACTION;

        dbfReadFirstDataRecord(state);
        if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
            loadNextSubfilePage(state);
        }
        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(SelectAwhRegionState state)
    {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            SelectAwhRegionDTO dto = new SelectAwhRegionDTO();
            BeanUtils.copyProperties(state, dto);
            stepResult = callScreen(SCREEN_SELECT, dto).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_SELECT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(SelectAwhRegionState state, SelectAwhRegionDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey())) {
            //TODO: processResetRequest(state);//synon built-in function
        }
        else if (isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextDataRecord(state);
            loadNextSubfilePage(state);
        }
        else {
            usrProcessSubfileControl(state);
            //TODO:readFirstChangedSubfileRecord(state);//synon built-in function
//			while (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
//              for (SelectAwhRegionGDO gdo : ((Page<SelectAwhRegionGDO>) state.getPageGdo()).getContent())
//		        {
                    if (state.getAwhRegionCode() != null && !state.getAwhRegionCode().equals("")) {
                        SelectAwhRegionGDO gdo = null;
                        for(SelectAwhRegionGDO obj: state.getPageGdo().getContent()) {
                            if(obj.getAwhRegionCode().equals(state.getAwhRegionCode())) {
                                gdo = obj;
                                break;
                            }
                        }
                        usrProcessSelectedLine(state, gdo);
                        SelectAwhRegionResult params = new SelectAwhRegionResult();
                        BeanUtils.copyProperties(state, params);
                        stepResult = StepResult.returnFromService(params);
                        return stepResult;
                    }
//                    usrProcessChangedSubfileRecord(state, gdo);
//                    usrScreenFunctionFields(state, gdo);
//                    //TODO:updateSubfileRecord(state, gdo);//synon built-in function
//                    //TODO:readNextChangedSubfileRecord(state);//synon built-in function
//                }
            if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
            {
                state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
            }
            usrProcessCommandKeys(state);
        }
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     */
    private void loadNextSubfilePage(SelectAwhRegionState state)
    {
        for (SelectAwhRegionGDO gdo : ((Page<SelectAwhRegionGDO>) state.getPageGdo()).getContent())
        {
            state.setRecordSelect(RecordSelectedEnum._STA_YES);
            //TODO:moveDbfRecordFieldsToSubfileRecord(state);//synon built-in function
            usrScreenFunctionFields(state, gdo);
            usrLoadSubfileRecordFromDbfRecord(state, gdo);
            if(state.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(state);//synon built-in function
            }
        }
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(SelectAwhRegionState state)
    {
        StepResult stepResult = NO_ACTION;

        usrExitProgramProcessing(state);

        SelectAwhRegionResult params = new SelectAwhRegionResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param state - Service state class.
     */
    private void dbfReadFirstDataRecord(SelectAwhRegionState state)
    {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     */
    private void dbfReadNextDataRecord(SelectAwhRegionState state)
    {
        state.setPage(state.getPage() + 1);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the actual page
     * @param state - Service state class.
     */
    private void dbfReadDataRecord(SelectAwhRegionState state)
    {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try
        {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet())
            {
                if (entry.getValue() == null)
                {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe)
        {
        }

        if (CollectionUtils.isEmpty(sortOrders))
        {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else
        {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
    }

		RestResponsePage<SelectAwhRegionGDO> pageGdo = awhRegionRepository.selectAwhRegion(null, pageable);
		state.setPageGdo(pageGdo);
	}
    
    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(SelectAwhRegionState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 20 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
    private StepResult usrProcessSubfileControl(SelectAwhRegionState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
    private StepResult usrProcessSelectedLine(SelectAwhRegionState dto, SelectAwhRegionGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 107 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
    private StepResult usrProcessChangedSubfileRecord(SelectAwhRegionState dto, SelectAwhRegionGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
    private StepResult usrScreenFunctionFields(SelectAwhRegionState dto, SelectAwhRegionGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 165 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
    private StepResult usrProcessCommandKeys(SelectAwhRegionState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 143 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrLoadSubfileRecordFromDbfRecord(SelectAwhRegionState dto, SelectAwhRegionGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 41 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(SelectAwhRegionState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */



}
