package au.awh.file.awhregion.selectawhregion;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 4
// generateStatusFieldImportStatements END

/**
 * Gdo for file 'AWH Region' (WSREG) and function 'Select AWH Region' (WSPLTWHSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectAwhRegionGDO implements Serializable {
	private static final long serialVersionUID = -2628002437817153384L;

	private String _sysSelected = "";
	private String awhRegionCode = "";
	private String awhRegionName = "";
	private String awhRegionOrg = "";
	private long awhRegionContact = 0L;

	public SelectAwhRegionGDO() {

	}

	public SelectAwhRegionGDO(String awhRegionCode, String awhRegionName, String awhRegionOrg, long awhRegionContact) {
		this.awhRegionCode = awhRegionCode;
		this.awhRegionName = awhRegionName;
		this.awhRegionOrg = awhRegionOrg;
		this.awhRegionContact = awhRegionContact;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionName(String awhRegionName) {
		this.awhRegionName = awhRegionName;
	}

	public String getAwhRegionName() {
		return awhRegionName;
	}

	public void setAwhRegionOrg(String awhRegionOrg) {
		this.awhRegionOrg = awhRegionOrg;
	}

	public String getAwhRegionOrg() {
		return awhRegionOrg;
	}

	public void setAwhRegionContact(long awhRegionContact) {
		this.awhRegionContact = awhRegionContact;
	}

	public long getAwhRegionContact() {
		return awhRegionContact;
	}

}
