package au.awh.file.awhregion.selectawhregion;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: SelectAwhRegion (WSPLTWHSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectAwhRegionParams implements Serializable
{
    private static final long serialVersionUID = 101090727831881842L;

	private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}

