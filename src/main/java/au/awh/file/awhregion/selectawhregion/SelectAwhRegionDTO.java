package au.awh.file.awhregion.selectawhregion;

import org.springframework.beans.BeanUtils;


import au.awh.file.awhregion.AwhRegion;
import au.awh.service.data.SelectRecordDTO;

/**
 * Dto for file 'AWH Region' (WSREG) and function 'Select AWH Region' (WSPLTWHSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectAwhRegionDTO extends SelectRecordDTO<SelectAwhRegionGDO> {
	private static final long serialVersionUID = 837394848650053441L;

	private String awhRegionCode = "";

	public SelectAwhRegionDTO() {
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param awhRegion AwhRegion Entity bean
     */
    public void setDtoFields(AwhRegion awhRegion) {
        BeanUtils.copyProperties(awhRegion, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param awhRegion AwhRegion Entity bean
     */
    public void setEntityFields(AwhRegion awhRegion) {
        BeanUtils.copyProperties(this, awhRegion);
    }
}
