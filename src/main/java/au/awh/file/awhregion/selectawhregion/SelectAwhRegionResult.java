package au.awh.file.awhregion.selectawhregion;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: SelectAwhRegion (WSPLTWHSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectAwhRegionResult implements Serializable
{
    private static final long serialVersionUID = 8498335443454411402L;

	private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}

