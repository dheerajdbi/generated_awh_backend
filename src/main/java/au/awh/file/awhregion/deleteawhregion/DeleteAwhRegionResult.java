package au.awh.file.awhregion.deleteawhregion;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: DeleteAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DeleteAwhRegionResult implements Serializable
{
	private static final long serialVersionUID = 8166117576166296887L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
