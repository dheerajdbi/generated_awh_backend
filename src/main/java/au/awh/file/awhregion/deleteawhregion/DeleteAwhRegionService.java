package au.awh.file.awhregion.deleteawhregion;

// DeleteObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.awhregion.AwhRegion;
import au.awh.file.awhregion.AwhRegionId;
import au.awh.file.awhregion.AwhRegionRepository;
import au.awh.model.ReturnCodeEnum;

/**
 * DELOBJ Service controller for 'Delete AWH Region' (file 'AWH Region' (WSREG)
 *
 * @author X2EGenerator  DeleteObjectFunction.kt
 */
@Service
public class DeleteAwhRegionService  extends AbstractService<DeleteAwhRegionService, DeleteAwhRegionDTO>
{
	private final Step execute = define("execute", DeleteAwhRegionParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionRepository awhRegionRepository;

	
	@Autowired
	public DeleteAwhRegionService() {
		super(DeleteAwhRegionService.class, DeleteAwhRegionDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(DeleteAwhRegionParams params) throws ServiceException {
		DeleteAwhRegionDTO dto = new DeleteAwhRegionDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(DeleteAwhRegionDTO dto, DeleteAwhRegionParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);

		AwhRegionId awhRegionId = new AwhRegionId();
		awhRegionId.setAwhRegionCode(dto.getAwhRegionCode());
		try {
			awhRegionRepository.deleteById(awhRegionId);
			awhRegionRepository.flush();
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			System.out.println("Delete error: " + Arrays.toString(e.getStackTrace()));
			dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
		}

		DeleteAwhRegionResult result = new DeleteAwhRegionResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(DeleteAwhRegionDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(DeleteAwhRegionDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
		return stepResult;
	}


}
