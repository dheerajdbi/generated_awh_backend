package au.awh.file.awhregion.deleteawhregion;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: DeleteAwhRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DeleteAwhRegionParams implements Serializable
{
	private static final long serialVersionUID = -1840273257431546273L;

    private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}
