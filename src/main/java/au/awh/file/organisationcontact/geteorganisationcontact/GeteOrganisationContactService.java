package au.awh.file.organisationcontact.geteorganisationcontact;

// ExecuteInternalFunction.kt File=OrganisationContact (KK) Function=geteOrganisationContact (1500466) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.organisationcontact.OrganisationContact;
import au.awh.file.organisationcontact.OrganisationContactId;
import au.awh.file.organisationcontact.OrganisationContactRepository;
// imports for Services
import au.awh.file.organisationcontact.getorganisationcontact.GetOrganisationContactService;
// imports for DTO
import au.awh.file.organisationcontact.getorganisationcontact.GetOrganisationContactDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.organisationcontact.getorganisationcontact.GetOrganisationContactParams;
// imports for Results
import au.awh.file.organisationcontact.getorganisationcontact.GetOrganisationContactResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Organisation Contact' (file 'Organisation Contact' (WDORGCTC)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteOrganisationContactService extends AbstractService<GeteOrganisationContactService, GeteOrganisationContactDTO>
{
	private final Step execute = define("execute", GeteOrganisationContactParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private OrganisationContactRepository organisationContactRepository;

	@Autowired
	private GetOrganisationContactService getOrganisationContactService;

	//private final Step serviceGetOrganisationContact = define("serviceGetOrganisationContact",GetOrganisationContactResult.class, this::processServiceGetOrganisationContact);
	
	@Autowired
	public GeteOrganisationContactService() {
		super(GeteOrganisationContactService.class, GeteOrganisationContactDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteOrganisationContactParams params) throws ServiceException {
		GeteOrganisationContactDTO dto = new GeteOrganisationContactDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteOrganisationContactDTO dto, GeteOrganisationContactParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetOrganisationContactParams getOrganisationContactParams = null;
		GetOrganisationContactResult getOrganisationContactResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Organisation Contact - Organisation Contact  *
		getOrganisationContactParams = new GetOrganisationContactParams();
		getOrganisationContactResult = new GetOrganisationContactResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getOrganisationContactParams);
		getOrganisationContactParams.setOrganisation(dto.getOrganisation());
		getOrganisationContactParams.setOrgContactSequence(dto.getOrgContactSequence());
		stepResult = getOrganisationContactService.execute(getOrganisationContactParams);
		getOrganisationContactResult = (GetOrganisationContactResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getOrganisationContactResult.get_SysReturnCode());
		dto.setOrgContactName(getOrganisationContactResult.getOrgContactName());
		dto.setOrgContactDftDocCnn(getOrganisationContactResult.getOrgContactDftDocCnn());
		dto.setOrgContactPhone(getOrganisationContactResult.getOrgContactPhone());
		dto.setOrgContactMobile(getOrganisationContactResult.getOrgContactMobile());
		dto.setOrgContactFax(getOrganisationContactResult.getOrgContactFax());
		dto.setOrgContactEmail(getOrganisationContactResult.getOrgContactEmail());
		dto.setOrgContactJobTitle(getOrganisationContactResult.getOrgContactJobTitle());
		dto.setOrgContactSalutation(getOrganisationContactResult.getOrgContactSalutation());
		dto.setOrgContactDepartment(getOrganisationContactResult.getOrgContactDepartment());
		dto.setOrgContactDataSource(getOrganisationContactResult.getOrgContactDataSource());
		dto.setOrgContactStatus(getOrganisationContactResult.getOrgContactStatus());
		dto.setOrgContactModem1Type(getOrganisationContactResult.getOrgContactModem1Type());
		dto.setOrgContactModem1(getOrganisationContactResult.getOrgContactModem1());
		dto.setOrgContactModem2Type(getOrganisationContactResult.getOrgContactModem2Type());
		dto.setOrgContactModem2(getOrganisationContactResult.getOrgContactModem2());
		dto.setOrgContactIsDefault(getOrganisationContactResult.getOrgContactIsDefault());
		dto.setOrgContactAddressLin1(getOrganisationContactResult.getOrgContactAddressLin1());
		dto.setOrgContactAddressLin2(getOrganisationContactResult.getOrgContactAddressLin2());
		dto.setOrgContactAddressLin3(getOrganisationContactResult.getOrgContactAddressLin3());
		dto.setOrgContactAddressLin4(getOrganisationContactResult.getOrgContactAddressLin4());
		dto.setOrgContactAddressLin5(getOrganisationContactResult.getOrgContactAddressLin5());
		dto.setOrgContactAddressLin6(getOrganisationContactResult.getOrgContactAddressLin6());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Organisation Contact   NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1464116) EXCINTFUN 1500466 OrganisationContact.geteOrganisationContact 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1500466 OrganisationContact.geteOrganisationContact 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteOrganisationContactResult result = new GeteOrganisationContactResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetOrganisationContactService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetOrganisationContact(GeteOrganisationContactDTO dto, GetOrganisationContactParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteOrganisationContactParams geteOrganisationContactParams = new GeteOrganisationContactParams();
//        BeanUtils.copyProperties(dto, geteOrganisationContactParams);
//        stepResult = StepResult.callService(GeteOrganisationContactService.class, geteOrganisationContactParams);
//
//        return stepResult;
//    }
//
}
