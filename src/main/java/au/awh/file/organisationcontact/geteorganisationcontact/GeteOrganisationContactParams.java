package au.awh.file.organisationcontact.geteorganisationcontact;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteOrganisationContact ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteOrganisationContactParams implements Serializable
{
	private static final long serialVersionUID = -7407868505047415948L;

    private String organisation = "";
    private long orgContactSequence = 0L;
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	public long getOrgContactSequence() {
		return orgContactSequence;
	}
	
	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}
	
	public void setOrgContactSequence(String orgContactSequence) {
		setOrgContactSequence(Long.parseLong(orgContactSequence));
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.valueOf(getRecordOption));
	}
}
