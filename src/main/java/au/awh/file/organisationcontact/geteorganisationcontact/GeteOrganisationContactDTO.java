package au.awh.file.organisationcontact.geteorganisationcontact;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GeteOrganisationContactDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ErrorProcessingEnum errorProcessing;
	private GetRecordOptionEnum getRecordOption;
	private OrgContactDataSourceEnum orgContactDataSource;
	private OrgContactStatusEnum orgContactStatus;
	private String orgContactAddressLin1;
	private String orgContactAddressLin2;
	private String orgContactAddressLin3;
	private String orgContactAddressLin4;
	private String orgContactAddressLin5;
	private String orgContactAddressLin6;
	private String orgContactDepartment;
	private String orgContactDftDocCnn;
	private String orgContactEmail;
	private String orgContactFax;
	private String orgContactJobTitle;
	private String orgContactMobile;
	private String orgContactModem1;
	private String orgContactModem1Type;
	private String orgContactModem2;
	private String orgContactModem2Type;
	private String orgContactName;
	private String orgContactPhone;
	private String orgContactSalutation;
	private String organisation;
	private YesOrNoEnum orgContactIsDefault;
	private long orgContactSequence;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}

	public String getOrgContactAddressLin1() {
		return orgContactAddressLin1;
	}

	public String getOrgContactAddressLin2() {
		return orgContactAddressLin2;
	}

	public String getOrgContactAddressLin3() {
		return orgContactAddressLin3;
	}

	public String getOrgContactAddressLin4() {
		return orgContactAddressLin4;
	}

	public String getOrgContactAddressLin5() {
		return orgContactAddressLin5;
	}

	public String getOrgContactAddressLin6() {
		return orgContactAddressLin6;
	}

	public OrgContactDataSourceEnum getOrgContactDataSource() {
		return orgContactDataSource;
	}

	public String getOrgContactDepartment() {
		return orgContactDepartment;
	}

	public String getOrgContactDftDocCnn() {
		return orgContactDftDocCnn;
	}

	public String getOrgContactEmail() {
		return orgContactEmail;
	}

	public String getOrgContactFax() {
		return orgContactFax;
	}

	public YesOrNoEnum getOrgContactIsDefault() {
		return orgContactIsDefault;
	}

	public String getOrgContactJobTitle() {
		return orgContactJobTitle;
	}

	public String getOrgContactMobile() {
		return orgContactMobile;
	}

	public String getOrgContactModem1() {
		return orgContactModem1;
	}

	public String getOrgContactModem1Type() {
		return orgContactModem1Type;
	}

	public String getOrgContactModem2() {
		return orgContactModem2;
	}

	public String getOrgContactModem2Type() {
		return orgContactModem2Type;
	}

	public String getOrgContactName() {
		return orgContactName;
	}

	public String getOrgContactPhone() {
		return orgContactPhone;
	}

	public String getOrgContactSalutation() {
		return orgContactSalutation;
	}

	public long getOrgContactSequence() {
		return orgContactSequence;
	}

	public OrgContactStatusEnum getOrgContactStatus() {
		return orgContactStatus;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}

	public void setOrgContactAddressLin1(String orgContactAddressLin1) {
		this.orgContactAddressLin1 = orgContactAddressLin1;
	}

	public void setOrgContactAddressLin2(String orgContactAddressLin2) {
		this.orgContactAddressLin2 = orgContactAddressLin2;
	}

	public void setOrgContactAddressLin3(String orgContactAddressLin3) {
		this.orgContactAddressLin3 = orgContactAddressLin3;
	}

	public void setOrgContactAddressLin4(String orgContactAddressLin4) {
		this.orgContactAddressLin4 = orgContactAddressLin4;
	}

	public void setOrgContactAddressLin5(String orgContactAddressLin5) {
		this.orgContactAddressLin5 = orgContactAddressLin5;
	}

	public void setOrgContactAddressLin6(String orgContactAddressLin6) {
		this.orgContactAddressLin6 = orgContactAddressLin6;
	}

	public void setOrgContactDataSource(OrgContactDataSourceEnum orgContactDataSource) {
		this.orgContactDataSource = orgContactDataSource;
	}

	public void setOrgContactDepartment(String orgContactDepartment) {
		this.orgContactDepartment = orgContactDepartment;
	}

	public void setOrgContactDftDocCnn(String orgContactDftDocCnn) {
		this.orgContactDftDocCnn = orgContactDftDocCnn;
	}

	public void setOrgContactEmail(String orgContactEmail) {
		this.orgContactEmail = orgContactEmail;
	}

	public void setOrgContactFax(String orgContactFax) {
		this.orgContactFax = orgContactFax;
	}

	public void setOrgContactIsDefault(YesOrNoEnum orgContactIsDefault) {
		this.orgContactIsDefault = orgContactIsDefault;
	}

	public void setOrgContactJobTitle(String orgContactJobTitle) {
		this.orgContactJobTitle = orgContactJobTitle;
	}

	public void setOrgContactMobile(String orgContactMobile) {
		this.orgContactMobile = orgContactMobile;
	}

	public void setOrgContactModem1(String orgContactModem1) {
		this.orgContactModem1 = orgContactModem1;
	}

	public void setOrgContactModem1Type(String orgContactModem1Type) {
		this.orgContactModem1Type = orgContactModem1Type;
	}

	public void setOrgContactModem2(String orgContactModem2) {
		this.orgContactModem2 = orgContactModem2;
	}

	public void setOrgContactModem2Type(String orgContactModem2Type) {
		this.orgContactModem2Type = orgContactModem2Type;
	}

	public void setOrgContactName(String orgContactName) {
		this.orgContactName = orgContactName;
	}

	public void setOrgContactPhone(String orgContactPhone) {
		this.orgContactPhone = orgContactPhone;
	}

	public void setOrgContactSalutation(String orgContactSalutation) {
		this.orgContactSalutation = orgContactSalutation;
	}

	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}

	public void setOrgContactStatus(OrgContactStatusEnum orgContactStatus) {
		this.orgContactStatus = orgContactStatus;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

}
