package au.awh.file.organisationcontact.geteorganisationcontact;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 22
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteOrganisationContact ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteOrganisationContactResult implements Serializable
{
	private static final long serialVersionUID = 3562895387406844685L;

	private ReturnCodeEnum _sysReturnCode;
	private String orgContactName = ""; // String 39119
	private String orgContactPhone = ""; // String 39120
	private String orgContactMobile = ""; // String 39121
	private String orgContactFax = ""; // String 39122
	private String orgContactEmail = ""; // String 39123
	private String orgContactJobTitle = ""; // String 39124
	private String orgContactSalutation = ""; // String 39125
	private String orgContactDepartment = ""; // String 39126
	private OrgContactDataSourceEnum orgContactDataSource = null; // OrgContactDataSourceEnum 39127
	private String orgContactModem1Type = ""; // String 39128
	private String orgContactModem1 = ""; // String 39129
	private OrgContactStatusEnum orgContactStatus = null; // OrgContactStatusEnum 39130
	private String orgContactModem2Type = ""; // String 39161
	private YesOrNoEnum orgContactIsDefault = null; // YesOrNoEnum 39162
	private String orgContactModem2 = ""; // String 39163
	private String orgContactAddressLin1 = ""; // String 39596
	private String orgContactAddressLin2 = ""; // String 39597
	private String orgContactAddressLin3 = ""; // String 39598
	private String orgContactAddressLin4 = ""; // String 39599
	private String orgContactAddressLin5 = ""; // String 39600
	private String orgContactAddressLin6 = ""; // String 39601
	private String orgContactDftDocCnn = ""; // String 39614

	public String getOrgContactName() {
		return orgContactName;
	}
	
	public void setOrgContactName(String orgContactName) {
		this.orgContactName = orgContactName;
	}
	
	public String getOrgContactPhone() {
		return orgContactPhone;
	}
	
	public void setOrgContactPhone(String orgContactPhone) {
		this.orgContactPhone = orgContactPhone;
	}
	
	public String getOrgContactMobile() {
		return orgContactMobile;
	}
	
	public void setOrgContactMobile(String orgContactMobile) {
		this.orgContactMobile = orgContactMobile;
	}
	
	public String getOrgContactFax() {
		return orgContactFax;
	}
	
	public void setOrgContactFax(String orgContactFax) {
		this.orgContactFax = orgContactFax;
	}
	
	public String getOrgContactEmail() {
		return orgContactEmail;
	}
	
	public void setOrgContactEmail(String orgContactEmail) {
		this.orgContactEmail = orgContactEmail;
	}
	
	public String getOrgContactJobTitle() {
		return orgContactJobTitle;
	}
	
	public void setOrgContactJobTitle(String orgContactJobTitle) {
		this.orgContactJobTitle = orgContactJobTitle;
	}
	
	public String getOrgContactSalutation() {
		return orgContactSalutation;
	}
	
	public void setOrgContactSalutation(String orgContactSalutation) {
		this.orgContactSalutation = orgContactSalutation;
	}
	
	public String getOrgContactDepartment() {
		return orgContactDepartment;
	}
	
	public void setOrgContactDepartment(String orgContactDepartment) {
		this.orgContactDepartment = orgContactDepartment;
	}
	
	public OrgContactDataSourceEnum getOrgContactDataSource() {
		return orgContactDataSource;
	}
	
	public void setOrgContactDataSource(OrgContactDataSourceEnum orgContactDataSource) {
		this.orgContactDataSource = orgContactDataSource;
	}
	
	public void setOrgContactDataSource(String orgContactDataSource) {
		setOrgContactDataSource(OrgContactDataSourceEnum.valueOf(orgContactDataSource));
	}
	
	public String getOrgContactModem1Type() {
		return orgContactModem1Type;
	}
	
	public void setOrgContactModem1Type(String orgContactModem1Type) {
		this.orgContactModem1Type = orgContactModem1Type;
	}
	
	public String getOrgContactModem1() {
		return orgContactModem1;
	}
	
	public void setOrgContactModem1(String orgContactModem1) {
		this.orgContactModem1 = orgContactModem1;
	}
	
	public OrgContactStatusEnum getOrgContactStatus() {
		return orgContactStatus;
	}
	
	public void setOrgContactStatus(OrgContactStatusEnum orgContactStatus) {
		this.orgContactStatus = orgContactStatus;
	}
	
	public void setOrgContactStatus(String orgContactStatus) {
		setOrgContactStatus(OrgContactStatusEnum.valueOf(orgContactStatus));
	}
	
	public String getOrgContactModem2Type() {
		return orgContactModem2Type;
	}
	
	public void setOrgContactModem2Type(String orgContactModem2Type) {
		this.orgContactModem2Type = orgContactModem2Type;
	}
	
	public YesOrNoEnum getOrgContactIsDefault() {
		return orgContactIsDefault;
	}
	
	public void setOrgContactIsDefault(YesOrNoEnum orgContactIsDefault) {
		this.orgContactIsDefault = orgContactIsDefault;
	}
	
	public void setOrgContactIsDefault(String orgContactIsDefault) {
		setOrgContactIsDefault(YesOrNoEnum.valueOf(orgContactIsDefault));
	}
	
	public String getOrgContactModem2() {
		return orgContactModem2;
	}
	
	public void setOrgContactModem2(String orgContactModem2) {
		this.orgContactModem2 = orgContactModem2;
	}
	
	public String getOrgContactAddressLin1() {
		return orgContactAddressLin1;
	}
	
	public void setOrgContactAddressLin1(String orgContactAddressLin1) {
		this.orgContactAddressLin1 = orgContactAddressLin1;
	}
	
	public String getOrgContactAddressLin2() {
		return orgContactAddressLin2;
	}
	
	public void setOrgContactAddressLin2(String orgContactAddressLin2) {
		this.orgContactAddressLin2 = orgContactAddressLin2;
	}
	
	public String getOrgContactAddressLin3() {
		return orgContactAddressLin3;
	}
	
	public void setOrgContactAddressLin3(String orgContactAddressLin3) {
		this.orgContactAddressLin3 = orgContactAddressLin3;
	}
	
	public String getOrgContactAddressLin4() {
		return orgContactAddressLin4;
	}
	
	public void setOrgContactAddressLin4(String orgContactAddressLin4) {
		this.orgContactAddressLin4 = orgContactAddressLin4;
	}
	
	public String getOrgContactAddressLin5() {
		return orgContactAddressLin5;
	}
	
	public void setOrgContactAddressLin5(String orgContactAddressLin5) {
		this.orgContactAddressLin5 = orgContactAddressLin5;
	}
	
	public String getOrgContactAddressLin6() {
		return orgContactAddressLin6;
	}
	
	public void setOrgContactAddressLin6(String orgContactAddressLin6) {
		this.orgContactAddressLin6 = orgContactAddressLin6;
	}
	
	public String getOrgContactDftDocCnn() {
		return orgContactDftDocCnn;
	}
	
	public void setOrgContactDftDocCnn(String orgContactDftDocCnn) {
		this.orgContactDftDocCnn = orgContactDftDocCnn;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
