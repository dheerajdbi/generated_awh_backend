package au.awh.file.organisationcontact;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.organisationcontact.OrganisationContact;
import au.awh.file.organisationcontact.selectorganisationconta.SelectOrganisationContaGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Organisation Contact (WDORGCTC).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class OrganisationContactRepositoryImpl implements OrganisationContactRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public RestResponsePage<SelectOrganisationContaGDO> selectOrganisationConta(
		String organisation, long orgContactSequence
, String orgContactName, String orgContactJobTitle, OrgContactStatusEnum orgContactStatus, YesOrNoEnum orgContactIsDefault, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(organisation)) {
			whereClause += " (organisationContact.id.organisation >= :organisation OR organisationContact.id.organisation like CONCAT('%', :organisation, '%'))";
			isParamSet = true;
		}

		if (orgContactSequence > 0) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " organisationContact.id.orgContactSequence = :orgContactSequence";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(orgContactName)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " (organisationContact.orgContactName >= :orgContactName OR organisationContact.orgContactName like CONCAT('%', :orgContactName, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(orgContactJobTitle)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " organisationContact.orgContactJobTitle like CONCAT('%', :orgContactJobTitle, '%')";
			isParamSet = true;
		}

		if (orgContactStatus != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " organisationContact.orgContactStatus = :orgContactStatus";
			isParamSet = true;
		}

		if (orgContactIsDefault != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " organisationContact.orgContactIsDefault = :orgContactIsDefault";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.organisationcontact.selectorganisationconta.SelectOrganisationContaGDO(organisationContact.id.organisation, organisationContact.id.orgContactSequence, organisationContact.orgContactName, organisationContact.orgContactDftDocCnn, organisationContact.orgContactPhone, organisationContact.orgContactMobile, organisationContact.orgContactFax, organisationContact.orgContactEmail, organisationContact.orgContactJobTitle, organisationContact.orgContactSalutation, organisationContact.orgContactDepartment, organisationContact.orgContactDataSource, organisationContact.orgContactStatus, organisationContact.orgContactModem1Type, organisationContact.orgContactModem1, organisationContact.orgContactModem2Type, organisationContact.orgContactModem2, organisationContact.orgContactIsDefault, organisationContact.orgContactAddressLin1, organisationContact.orgContactAddressLin2, organisationContact.orgContactAddressLin3, organisationContact.orgContactAddressLin4, organisationContact.orgContactAddressLin5, organisationContact.orgContactAddressLin6) from OrganisationContact organisationContact";
		String countQueryString = "SELECT COUNT(organisationContact.id.organisation) FROM OrganisationContact organisationContact";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			OrganisationContact organisationContact = new OrganisationContact();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(organisationContact.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"organisationContact.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"organisationContact." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"organisationContact.id.organisation"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"organisationContact.id.orgContactSequence"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(organisation)) {
			countQuery.setParameter("organisation", organisation);
			query.setParameter("organisation", organisation);
		}

		if (orgContactSequence > 0) {
			countQuery.setParameter("orgContactSequence", orgContactSequence);
			query.setParameter("orgContactSequence", orgContactSequence);
		}

		if (StringUtils.isNotEmpty(orgContactName)) {
			countQuery.setParameter("orgContactName", orgContactName);
			query.setParameter("orgContactName", orgContactName);
		}

		if (StringUtils.isNotEmpty(orgContactJobTitle)) {
			countQuery.setParameter("orgContactJobTitle", orgContactJobTitle);
			query.setParameter("orgContactJobTitle", orgContactJobTitle);
		}

		if (orgContactStatus != null) {
			countQuery.setParameter("orgContactStatus", orgContactStatus);
			query.setParameter("orgContactStatus", orgContactStatus);
		}

		if (orgContactIsDefault != null) {
			countQuery.setParameter("orgContactIsDefault", orgContactIsDefault);
			query.setParameter("orgContactIsDefault", orgContactIsDefault);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectOrganisationContaGDO> content = query.getResultList();
		RestResponsePage<SelectOrganisationContaGDO> pageGdo = new RestResponsePage<SelectOrganisationContaGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.organisationcontact.OrganisationContactService#getOrganisationContact(String, long)
	 */
	@Override
	public OrganisationContact getOrganisationContact(
		String organisation, long orgContactSequence
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(organisation)) {
			whereClause += " organisationContact.id.organisation = :organisation";
			isParamSet = true;
		}

		if (orgContactSequence > 0) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " organisationContact.id.orgContactSequence = :orgContactSequence";
			isParamSet = true;
		}

		String sqlString = "SELECT organisationContact FROM OrganisationContact organisationContact";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(organisation)) {
			query.setParameter("organisation", organisation);
		}

		if (orgContactSequence > 0) {
			query.setParameter("orgContactSequence", orgContactSequence);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		OrganisationContact dto = (OrganisationContact)query.getSingleResult();

		return dto;
	}

}
