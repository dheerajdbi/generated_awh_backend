package au.awh.file.organisationcontact;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

import au.awh.file.organisationcontact.selectorganisationconta.SelectOrganisationContaGDO;

/**
 * Custom Spring Data JPA repository interface for model: Organisation Contact (WDORGCTC).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface OrganisationContactRepositoryCustom {

	/**
	 * 
	 * @param organisation Organisation
	 * @param orgContactSequence Org Contact Sequence
	 * @param orgContactName Org Contact Name
	 * @param orgContactJobTitle Org Contact Job Title
	 * @param orgContactStatus Org Contact Status
	 * @param orgContactIsDefault Org Contact is Default
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectOrganisationContaGDO
	 */
	RestResponsePage<SelectOrganisationContaGDO> selectOrganisationConta(String organisation, long orgContactSequence
	, String orgContactName, String orgContactJobTitle, OrgContactStatusEnum orgContactStatus, YesOrNoEnum orgContactIsDefault, Pageable pageable);

	/**
	 * 
	 * @param organisation Organisation
	 * @param orgContactSequence Org Contact Sequence
	 * @return OrganisationContact
	 */
	OrganisationContact getOrganisationContact(String organisation, long orgContactSequence
	);
}
