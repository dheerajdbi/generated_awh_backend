package au.awh.file.organisationcontact.getorganisationcontact;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetOrganisationContact ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetOrganisationContactParams implements Serializable
{
	private static final long serialVersionUID = -5013832274864550487L;

    private String organisation = "";
    private long orgContactSequence = 0L;

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	public long getOrgContactSequence() {
		return orgContactSequence;
	}
	
	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}
	
	public void setOrgContactSequence(String orgContactSequence) {
		setOrgContactSequence(Long.parseLong(orgContactSequence));
	}
}
