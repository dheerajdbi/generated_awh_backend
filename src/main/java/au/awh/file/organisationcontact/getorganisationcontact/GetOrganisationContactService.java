package au.awh.file.organisationcontact.getorganisationcontact;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.organisationcontact.OrganisationContact;
import au.awh.file.organisationcontact.OrganisationContactId;
import au.awh.file.organisationcontact.OrganisationContactRepository;
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Organisation Contact' (file 'Organisation Contact' (WDORGCTC)
 *
 * @author X2EGenerator
 */
@Service
public class GetOrganisationContactService extends AbstractService<GetOrganisationContactService, GetOrganisationContactDTO>
{
    private final Step execute = define("execute", GetOrganisationContactParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private OrganisationContactRepository organisationContactRepository;

	

    @Autowired
    public GetOrganisationContactService()
    {
        super(GetOrganisationContactService.class, GetOrganisationContactDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetOrganisationContactParams params) throws ServiceException {
        GetOrganisationContactDTO dto = new GetOrganisationContactDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetOrganisationContactDTO dto, GetOrganisationContactParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<OrganisationContact> organisationContactList = organisationContactRepository.findAllByIdOrganisationAndIdOrgContactSequence(dto.getOrganisation(), dto.getOrgContactSequence());
		if (!organisationContactList.isEmpty()) {
			for (OrganisationContact organisationContact : organisationContactList) {
				processDataRecord(dto, organisationContact);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetOrganisationContactResult result = new GetOrganisationContactResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetOrganisationContactDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetOrganisationContactDTO dto, OrganisationContact organisationContact) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetOrganisationContactDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000020 ACT PAR = CON By name
		dto.setOrgContactName("");
		dto.setOrgContactDftDocCnn("");
		dto.setOrgContactPhone("");
		dto.setOrgContactMobile("");
		dto.setOrgContactFax("");
		dto.setOrgContactEmail("");
		dto.setOrgContactJobTitle("");
		dto.setOrgContactSalutation("");
		dto.setOrgContactDepartment("");
		dto.setOrgContactDataSource(null);
		dto.setOrgContactStatus(null);
		dto.setOrgContactModem1Type("");
		dto.setOrgContactModem1("");
		dto.setOrgContactModem2Type("");
		dto.setOrgContactModem2("");
		dto.setOrgContactIsDefault(YesOrNoEnum._NOT_ENTERED);
		dto.setOrgContactAddressLin1("");
		dto.setOrgContactAddressLin2("");
		dto.setOrgContactAddressLin3("");
		dto.setOrgContactAddressLin4("");
		dto.setOrgContactAddressLin5("");
		dto.setOrgContactAddressLin6("");
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000014 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetOrganisationContactDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
