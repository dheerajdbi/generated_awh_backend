package au.awh.file.organisationcontact.selectorganisationconta;

import org.springframework.beans.BeanUtils;

import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;

import au.awh.file.organisationcontact.OrganisationContact;
import au.awh.service.data.SelectRecordDTO;

/**
 * Dto for file 'Organisation Contact' (WDORGCTC) and function 'Select Organisation Conta' (WDSYSW9SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationContaDTO extends SelectRecordDTO<SelectOrganisationContaGDO> {
	private static final long serialVersionUID = 4990662522222562231L;

	private String organisation = "";
	private long orgContactSequence = 0L;
	private String orgContactName = "";
	private String orgContactJobTitle = "";
	private OrgContactStatusEnum orgContactStatus = null;
	private YesOrNoEnum orgContactIsDefault = YesOrNoEnum._NOT_ENTERED;

	public SelectOrganisationContaDTO() {
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}

	public long getOrgContactSequence() {
		return orgContactSequence;
	}

	public void setOrgContactName(String orgContactName) {
		this.orgContactName = orgContactName;
	}

	public String getOrgContactName() {
		return orgContactName;
	}

	public void setOrgContactJobTitle(String orgContactJobTitle) {
		this.orgContactJobTitle = orgContactJobTitle;
	}

	public String getOrgContactJobTitle() {
		return orgContactJobTitle;
	}

	public void setOrgContactStatus(OrgContactStatusEnum orgContactStatus) {
		this.orgContactStatus = orgContactStatus;
	}

	public OrgContactStatusEnum getOrgContactStatus() {
		return orgContactStatus;
	}

	public void setOrgContactIsDefault(YesOrNoEnum orgContactIsDefault) {
		this.orgContactIsDefault = orgContactIsDefault;
	}

	public YesOrNoEnum getOrgContactIsDefault() {
		return orgContactIsDefault;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param organisationContact OrganisationContact Entity bean
     */
    public void setDtoFields(OrganisationContact organisationContact) {
        BeanUtils.copyProperties(organisationContact, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param organisationContact OrganisationContact Entity bean
     */
    public void setEntityFields(OrganisationContact organisationContact) {
        BeanUtils.copyProperties(this, organisationContact);
    }
}
