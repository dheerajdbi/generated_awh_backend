package au.awh.file.organisationcontact.selectorganisationconta;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 24
import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Gdo for file 'Organisation Contact' (WDORGCTC) and function 'Select Organisation Conta' (WDSYSW9SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationContaGDO implements Serializable {
	private static final long serialVersionUID = 5649360700052819747L;

	private String _sysSelected = "";
	private String organisation = "";
	private long orgContactSequence = 0L;
	private String orgContactName = "";
	private String orgContactDftDocCnn = "";
	private String orgContactPhone = "";
	private String orgContactMobile = "";
	private String orgContactFax = "";
	private String orgContactEmail = "";
	private String orgContactJobTitle = "";
	private String orgContactSalutation = "";
	private String orgContactDepartment = "";
	private OrgContactDataSourceEnum orgContactDataSource = null;
	private OrgContactStatusEnum orgContactStatus = null;
	private String orgContactModem1Type = "";
	private String orgContactModem1 = "";
	private String orgContactModem2Type = "";
	private String orgContactModem2 = "";
	private YesOrNoEnum orgContactIsDefault = null;
	private String orgContactAddressLin1 = "";
	private String orgContactAddressLin2 = "";
	private String orgContactAddressLin3 = "";
	private String orgContactAddressLin4 = "";
	private String orgContactAddressLin5 = "";
	private String orgContactAddressLin6 = "";

	public SelectOrganisationContaGDO() {

	}

	public SelectOrganisationContaGDO(String organisation, long orgContactSequence, String orgContactName, String orgContactDftDocCnn, String orgContactPhone, String orgContactMobile, String orgContactFax, String orgContactEmail, String orgContactJobTitle, String orgContactSalutation, String orgContactDepartment, OrgContactDataSourceEnum orgContactDataSource, OrgContactStatusEnum orgContactStatus, String orgContactModem1Type, String orgContactModem1, String orgContactModem2Type, String orgContactModem2, YesOrNoEnum orgContactIsDefault, String orgContactAddressLin1, String orgContactAddressLin2, String orgContactAddressLin3, String orgContactAddressLin4, String orgContactAddressLin5, String orgContactAddressLin6) {
		this.organisation = organisation;
		this.orgContactSequence = orgContactSequence;
		this.orgContactName = orgContactName;
		this.orgContactDftDocCnn = orgContactDftDocCnn;
		this.orgContactPhone = orgContactPhone;
		this.orgContactMobile = orgContactMobile;
		this.orgContactFax = orgContactFax;
		this.orgContactEmail = orgContactEmail;
		this.orgContactJobTitle = orgContactJobTitle;
		this.orgContactSalutation = orgContactSalutation;
		this.orgContactDepartment = orgContactDepartment;
		this.orgContactDataSource = orgContactDataSource;
		this.orgContactStatus = orgContactStatus;
		this.orgContactModem1Type = orgContactModem1Type;
		this.orgContactModem1 = orgContactModem1;
		this.orgContactModem2Type = orgContactModem2Type;
		this.orgContactModem2 = orgContactModem2;
		this.orgContactIsDefault = orgContactIsDefault;
		this.orgContactAddressLin1 = orgContactAddressLin1;
		this.orgContactAddressLin2 = orgContactAddressLin2;
		this.orgContactAddressLin3 = orgContactAddressLin3;
		this.orgContactAddressLin4 = orgContactAddressLin4;
		this.orgContactAddressLin5 = orgContactAddressLin5;
		this.orgContactAddressLin6 = orgContactAddressLin6;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}

	public long getOrgContactSequence() {
		return orgContactSequence;
	}

	public void setOrgContactName(String orgContactName) {
		this.orgContactName = orgContactName;
	}

	public String getOrgContactName() {
		return orgContactName;
	}

	public void setOrgContactDftDocCnn(String orgContactDftDocCnn) {
		this.orgContactDftDocCnn = orgContactDftDocCnn;
	}

	public String getOrgContactDftDocCnn() {
		return orgContactDftDocCnn;
	}

	public void setOrgContactPhone(String orgContactPhone) {
		this.orgContactPhone = orgContactPhone;
	}

	public String getOrgContactPhone() {
		return orgContactPhone;
	}

	public void setOrgContactMobile(String orgContactMobile) {
		this.orgContactMobile = orgContactMobile;
	}

	public String getOrgContactMobile() {
		return orgContactMobile;
	}

	public void setOrgContactFax(String orgContactFax) {
		this.orgContactFax = orgContactFax;
	}

	public String getOrgContactFax() {
		return orgContactFax;
	}

	public void setOrgContactEmail(String orgContactEmail) {
		this.orgContactEmail = orgContactEmail;
	}

	public String getOrgContactEmail() {
		return orgContactEmail;
	}

	public void setOrgContactJobTitle(String orgContactJobTitle) {
		this.orgContactJobTitle = orgContactJobTitle;
	}

	public String getOrgContactJobTitle() {
		return orgContactJobTitle;
	}

	public void setOrgContactSalutation(String orgContactSalutation) {
		this.orgContactSalutation = orgContactSalutation;
	}

	public String getOrgContactSalutation() {
		return orgContactSalutation;
	}

	public void setOrgContactDepartment(String orgContactDepartment) {
		this.orgContactDepartment = orgContactDepartment;
	}

	public String getOrgContactDepartment() {
		return orgContactDepartment;
	}

	public void setOrgContactDataSource(OrgContactDataSourceEnum orgContactDataSource) {
		this.orgContactDataSource = orgContactDataSource;
	}

	public OrgContactDataSourceEnum getOrgContactDataSource() {
		return orgContactDataSource;
	}

	public void setOrgContactStatus(OrgContactStatusEnum orgContactStatus) {
		this.orgContactStatus = orgContactStatus;
	}

	public OrgContactStatusEnum getOrgContactStatus() {
		return orgContactStatus;
	}

	public void setOrgContactModem1Type(String orgContactModem1Type) {
		this.orgContactModem1Type = orgContactModem1Type;
	}

	public String getOrgContactModem1Type() {
		return orgContactModem1Type;
	}

	public void setOrgContactModem1(String orgContactModem1) {
		this.orgContactModem1 = orgContactModem1;
	}

	public String getOrgContactModem1() {
		return orgContactModem1;
	}

	public void setOrgContactModem2Type(String orgContactModem2Type) {
		this.orgContactModem2Type = orgContactModem2Type;
	}

	public String getOrgContactModem2Type() {
		return orgContactModem2Type;
	}

	public void setOrgContactModem2(String orgContactModem2) {
		this.orgContactModem2 = orgContactModem2;
	}

	public String getOrgContactModem2() {
		return orgContactModem2;
	}

	public void setOrgContactIsDefault(YesOrNoEnum orgContactIsDefault) {
		this.orgContactIsDefault = orgContactIsDefault;
	}

	public YesOrNoEnum getOrgContactIsDefault() {
		return orgContactIsDefault;
	}

	public void setOrgContactAddressLin1(String orgContactAddressLin1) {
		this.orgContactAddressLin1 = orgContactAddressLin1;
	}

	public String getOrgContactAddressLin1() {
		return orgContactAddressLin1;
	}

	public void setOrgContactAddressLin2(String orgContactAddressLin2) {
		this.orgContactAddressLin2 = orgContactAddressLin2;
	}

	public String getOrgContactAddressLin2() {
		return orgContactAddressLin2;
	}

	public void setOrgContactAddressLin3(String orgContactAddressLin3) {
		this.orgContactAddressLin3 = orgContactAddressLin3;
	}

	public String getOrgContactAddressLin3() {
		return orgContactAddressLin3;
	}

	public void setOrgContactAddressLin4(String orgContactAddressLin4) {
		this.orgContactAddressLin4 = orgContactAddressLin4;
	}

	public String getOrgContactAddressLin4() {
		return orgContactAddressLin4;
	}

	public void setOrgContactAddressLin5(String orgContactAddressLin5) {
		this.orgContactAddressLin5 = orgContactAddressLin5;
	}

	public String getOrgContactAddressLin5() {
		return orgContactAddressLin5;
	}

	public void setOrgContactAddressLin6(String orgContactAddressLin6) {
		this.orgContactAddressLin6 = orgContactAddressLin6;
	}

	public String getOrgContactAddressLin6() {
		return orgContactAddressLin6;
	}

}
