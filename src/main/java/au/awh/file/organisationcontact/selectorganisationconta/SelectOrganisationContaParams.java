package au.awh.file.organisationcontact.selectorganisationconta;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Params for resource: SelectOrganisationConta (WDSYSW9SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationContaParams implements Serializable
{
    private static final long serialVersionUID = 3504532962219477883L;

	private String organisation = "";
	private long orgContactSequence = 0L;

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	public long getOrgContactSequence() {
		return orgContactSequence;
	}
	
	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}
	
	public void setOrgContactSequence(String orgContactSequence) {
		setOrgContactSequence(Long.parseLong(orgContactSequence));
	}
}

