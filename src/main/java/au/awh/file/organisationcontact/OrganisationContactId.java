package au.awh.file.organisationcontact;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrganisationContactId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "KKOCTCSEQ")
	private long orgContactSequence = 0L;
	
	@Column(name = "KKORG")
	private String organisation = "";

	public OrganisationContactId() {
	
	}

	public OrganisationContactId(String organisation, long orgContactSequence) { 	this.orgContactSequence = orgContactSequence;
		this.organisation = organisation;
	}

	public long getOrgContactSequence() {
		return orgContactSequence;
	}
	
	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrgContactSequence(long orgContactSequence) {
		this.orgContactSequence = orgContactSequence;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
