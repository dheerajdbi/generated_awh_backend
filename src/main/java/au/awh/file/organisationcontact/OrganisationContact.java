package au.awh.file.organisationcontact;

import au.awh.model.OrgContactDataSourceEnum;
import au.awh.model.OrgContactStatusEnum;
import au.awh.model.YesOrNoEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WDORGCTC")
public class OrganisationContact implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private OrganisationContactId id = new OrganisationContactId();

	@Column(name = "KKOCTCAD1")
	private String orgContactAddressLin1 = "";
	
	@Column(name = "KKOCTCAD2")
	private String orgContactAddressLin2 = "";
	
	@Column(name = "KKOCTCAD3")
	private String orgContactAddressLin3 = "";
	
	@Column(name = "KKOCTCAD4")
	private String orgContactAddressLin4 = "";
	
	@Column(name = "KKOCTCAD5")
	private String orgContactAddressLin5 = "";
	
	@Column(name = "KKOCTCAD6")
	private String orgContactAddressLin6 = "";
	
	@Column(name = "KKOCTCDTA")
	private OrgContactDataSourceEnum orgContactDataSource = null;
	
	@Column(name = "KKOCTCDPT")
	private String orgContactDepartment = "";
	
	@Column(name = "KKOCTCDDC")
	private String orgContactDftDocCnn = "";
	
	@Column(name = "KKOCTCEML")
	private String orgContactEmail = "";
	
	@Column(name = "KKOCTCFAX")
	private String orgContactFax = "";
	
	@Column(name = "KKOCTCDFT")
	private YesOrNoEnum orgContactIsDefault = YesOrNoEnum._NOT_ENTERED;
	
	@Column(name = "KKOCTCJTL")
	private String orgContactJobTitle = "";
	
	@Column(name = "KKOCTCMOB")
	private String orgContactMobile = "";
	
	@Column(name = "KKOCTCMD1")
	private String orgContactModem1 = "";
	
	@Column(name = "KKOCTCMTP")
	private String orgContactModem1Type = "";
	
	@Column(name = "KKOCTCMD2")
	private String orgContactModem2 = "";
	
	@Column(name = "KKOCTCMT2")
	private String orgContactModem2Type = "";
	
	@Column(name = "KKOCTCNAM")
	private String orgContactName = "";
	
	@Column(name = "KKOCTCPHN")
	private String orgContactPhone = "";
	
	@Column(name = "KKOCTCSAL")
	private String orgContactSalutation = "";
	
	@Column(name = "KKOCTCSTS")
	private OrgContactStatusEnum orgContactStatus = null;

	public OrganisationContactId getId() {
		return id;
	}

	public String getOrganisation() {
		return id.getOrganisation();
	}
	
	public long getOrgContactSequence() {
		return id.getOrgContactSequence();
	}

	public String getOrgContactName() {
		return orgContactName;
	}
	
	public String getOrgContactDftDocCnn() {
		return orgContactDftDocCnn;
	}
	
	public String getOrgContactPhone() {
		return orgContactPhone;
	}
	
	public String getOrgContactMobile() {
		return orgContactMobile;
	}
	
	public String getOrgContactFax() {
		return orgContactFax;
	}
	
	public String getOrgContactEmail() {
		return orgContactEmail;
	}
	
	public String getOrgContactJobTitle() {
		return orgContactJobTitle;
	}
	
	public String getOrgContactSalutation() {
		return orgContactSalutation;
	}
	
	public String getOrgContactDepartment() {
		return orgContactDepartment;
	}
	
	public OrgContactDataSourceEnum getOrgContactDataSource() {
		return orgContactDataSource;
	}
	
	public OrgContactStatusEnum getOrgContactStatus() {
		return orgContactStatus;
	}
	
	public String getOrgContactModem1Type() {
		return orgContactModem1Type;
	}
	
	public String getOrgContactModem1() {
		return orgContactModem1;
	}
	
	public String getOrgContactModem2Type() {
		return orgContactModem2Type;
	}
	
	public String getOrgContactModem2() {
		return orgContactModem2;
	}
	
	public YesOrNoEnum getOrgContactIsDefault() {
		return orgContactIsDefault;
	}
	
	public String getOrgContactAddressLin1() {
		return orgContactAddressLin1;
	}
	
	public String getOrgContactAddressLin2() {
		return orgContactAddressLin2;
	}
	
	public String getOrgContactAddressLin3() {
		return orgContactAddressLin3;
	}
	
	public String getOrgContactAddressLin4() {
		return orgContactAddressLin4;
	}
	
	public String getOrgContactAddressLin5() {
		return orgContactAddressLin5;
	}
	
	public String getOrgContactAddressLin6() {
		return orgContactAddressLin6;
	}

	

	

	public void setOrganisation(String organisation) {
		this.id.setOrganisation(organisation);
	}
	
	public void setOrgContactSequence(long orgContactSequence) {
		this.id.setOrgContactSequence(orgContactSequence);
	}

	public void setOrgContactName(String orgContactName) {
		this.orgContactName = orgContactName;
	}
	
	public void setOrgContactDftDocCnn(String orgContactDftDocCnn) {
		this.orgContactDftDocCnn = orgContactDftDocCnn;
	}
	
	public void setOrgContactPhone(String orgContactPhone) {
		this.orgContactPhone = orgContactPhone;
	}
	
	public void setOrgContactMobile(String orgContactMobile) {
		this.orgContactMobile = orgContactMobile;
	}
	
	public void setOrgContactFax(String orgContactFax) {
		this.orgContactFax = orgContactFax;
	}
	
	public void setOrgContactEmail(String orgContactEmail) {
		this.orgContactEmail = orgContactEmail;
	}
	
	public void setOrgContactJobTitle(String orgContactJobTitle) {
		this.orgContactJobTitle = orgContactJobTitle;
	}
	
	public void setOrgContactSalutation(String orgContactSalutation) {
		this.orgContactSalutation = orgContactSalutation;
	}
	
	public void setOrgContactDepartment(String orgContactDepartment) {
		this.orgContactDepartment = orgContactDepartment;
	}
	
	public void setOrgContactDataSource(OrgContactDataSourceEnum orgContactDataSource) {
		this.orgContactDataSource = orgContactDataSource;
	}
	
	public void setOrgContactStatus(OrgContactStatusEnum orgContactStatus) {
		this.orgContactStatus = orgContactStatus;
	}
	
	public void setOrgContactModem1Type(String orgContactModem1Type) {
		this.orgContactModem1Type = orgContactModem1Type;
	}
	
	public void setOrgContactModem1(String orgContactModem1) {
		this.orgContactModem1 = orgContactModem1;
	}
	
	public void setOrgContactModem2Type(String orgContactModem2Type) {
		this.orgContactModem2Type = orgContactModem2Type;
	}
	
	public void setOrgContactModem2(String orgContactModem2) {
		this.orgContactModem2 = orgContactModem2;
	}
	
	public void setOrgContactIsDefault(YesOrNoEnum orgContactIsDefault) {
		this.orgContactIsDefault = orgContactIsDefault;
	}
	
	public void setOrgContactAddressLin1(String orgContactAddressLin1) {
		this.orgContactAddressLin1 = orgContactAddressLin1;
	}
	
	public void setOrgContactAddressLin2(String orgContactAddressLin2) {
		this.orgContactAddressLin2 = orgContactAddressLin2;
	}
	
	public void setOrgContactAddressLin3(String orgContactAddressLin3) {
		this.orgContactAddressLin3 = orgContactAddressLin3;
	}
	
	public void setOrgContactAddressLin4(String orgContactAddressLin4) {
		this.orgContactAddressLin4 = orgContactAddressLin4;
	}
	
	public void setOrgContactAddressLin5(String orgContactAddressLin5) {
		this.orgContactAddressLin5 = orgContactAddressLin5;
	}
	
	public void setOrgContactAddressLin6(String orgContactAddressLin6) {
		this.orgContactAddressLin6 = orgContactAddressLin6;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
