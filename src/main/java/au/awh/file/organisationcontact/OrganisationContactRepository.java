package au.awh.file.organisationcontact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Organisation Contact (WDORGCTC).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface OrganisationContactRepository extends OrganisationContactRepositoryCustom, JpaRepository<OrganisationContact, OrganisationContactId> {

	List<OrganisationContact> findAllByIdOrganisation(String organisation);

	List<OrganisationContact> findAllByIdOrganisationAndIdOrgContactSequence(String organisation, long orgContactSequence);
}
