package au.awh.file.equipmentreadingcontrol.chgestatus;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgeStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeStatusParams implements Serializable
{
	private static final long serialVersionUID = -2102173097297362338L;

    private ErrorProcessingEnum errorProcessing = null;
    private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;
    private EquipReadingCtrlStsEnum equipReadingCtrlSts = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	
	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(String equipReadingCtrlSts) {
		setEquipReadingCtrlSts(EquipReadingCtrlStsEnum.valueOf(equipReadingCtrlSts));
	}
}
