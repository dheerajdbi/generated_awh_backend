package au.awh.file.equipmentreadingcontrol.chgestatus;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeStatusResult implements Serializable
{
	private static final long serialVersionUID = 6280244214688664139L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
