package au.awh.file.equipmentreadingcontrol;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Repository;

import com.freschelegacy.utils.RestResponsePage;

import au.awh.file.equipmentreadingcontrol.enterbatchreadingshh.EnterBatchReadingsHhDTO;
import au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk.WrkEquipmentReadingsWspltwqdfkGDO;
import au.awh.model.EquipReadingCtrlStsEnum;

/**
 * Custom Spring Data JPA repository implementation for model: Equipment Reading Control (WSERCTL).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class EquipmentReadingControlRepositoryImpl implements EquipmentReadingControlRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> wrkEquipmentReadingsWspltwqdfk(
		
		String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate, boolean unCompletedOnly, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;
		
		if (awhRegionCode != null) {
			whereClause += " equipmentReadingControl.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadingControl.centreCodeKey like CONCAT('%', :centreCodeKey, '%')";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadingControl.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		if (unCompletedOnly) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadingControl.equipReadingCtrlSts <> :equipReadingCtrlSts";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk.WrkEquipmentReadingsWspltwqdfkGDO(equipmentReadingControl.awhRegionCode, equipmentReadingControl.centreCodeKey, equipmentReadingControl.readingRequiredDate, equipmentReadingControl.id.equipReadingBatchNo, equipmentReadingControl.equipmentLeasor, equipmentReadingControl.equipReadingCtrlSts) from EquipmentReadingControl equipmentReadingControl";
		String countQueryString = "SELECT COUNT(equipmentReadingControl.id.equipReadingBatchNo) FROM EquipmentReadingControl equipmentReadingControl";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			EquipmentReadingControl equipmentReadingControl = new EquipmentReadingControl();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(equipmentReadingControl.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"equipmentReadingControl.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"equipmentReadingControl." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentReadingControl.awhRegionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentReadingControl.centreCodeKey"));
			sortOrders.add(new Order(Sort.Direction.DESC,
				"equipmentReadingControl.readingRequiredDate"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (awhRegionCode != null) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		if (unCompletedOnly) {
			countQuery.setParameter("equipReadingCtrlSts", EquipReadingCtrlStsEnum._COMPLETE);
			query.setParameter("equipReadingCtrlSts", EquipReadingCtrlStsEnum._COMPLETE);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<WrkEquipmentReadingsWspltwqdfkGDO> content = query.getResultList();

		return new RestResponsePage<>(content, pageable, count);
		
	}

	/**
	 * @see au.awh.file.equipmentreadingcontrol.EquipmentReadingControlRepositoryCustom#getEquipmentReadingCon(long)
	 */
	@Override
	public EquipmentReadingControl getEquipmentReadingCon(
		long equipReadingBatchNo
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (equipReadingBatchNo > 0) {
			whereClause += " equipmentReadingControl.id.equipReadingBatchNo = :equipReadingBatchNo";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadingControl FROM EquipmentReadingControl equipmentReadingControl";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (equipReadingBatchNo > 0) {
			query.setParameter("equipReadingBatchNo", equipReadingBatchNo);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		EquipmentReadingControl dto = (EquipmentReadingControl)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<EnterBatchReadingsHhDTO> enterBatchReadingsHh(
		
Pageable pageable) {
		String sqlString = "SELECT new au.awh.file.equipmentreadingcontrol.enterbatchreadingshh.EnterBatchReadingsHhDTO(equipmentReadingControl.id.equipReadingBatchNo, equipmentReadingControl.awhRegionCode, equipmentReadingControl.centreCodeKey, equipmentReadingControl.readingRequiredDate, equipmentReadingControl.equipReadingCtrlSts, equipmentReadingControl.equipmentLeasor) from EquipmentReadingControl equipmentReadingControl";
		String countQueryString = "SELECT COUNT(equipmentReadingControl.id.equipReadingBatchNo) FROM EquipmentReadingControl equipmentReadingControl";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EnterBatchReadingsHhDTO> content = query.getResultList();

		return new RestResponsePage<>(content, pageable, count);
	}

}
