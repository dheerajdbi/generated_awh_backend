package au.awh.file.equipmentreadingcontrol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 5
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Equipment Reading Control (WSERCTL).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentReadingControlRepository extends EquipmentReadingControlRepositoryCustom, JpaRepository<EquipmentReadingControl, EquipmentReadingControlId> {

	List<EquipmentReadingControl> findAllBy();

	List<EquipmentReadingControl> findAllByAwhRegionCodeAndCentreCodeKeyAndReadingRequiredDate(String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate);
}
