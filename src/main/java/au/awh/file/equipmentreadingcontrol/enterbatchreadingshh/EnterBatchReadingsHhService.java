package au.awh.file.equipmentreadingcontrol.enterbatchreadingshh;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlService;
import au.awh.file.equipmentreadings.addembatchreadinghh.AddemBatchReadingHhService;
import au.awh.file.utilitiesandusersource.fixean13alpha.FixEan13AlphaService;
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthService;
import au.awh.file.utilitiesandusersource.processeean13.ProcesseEan13Service;
import au.awh.file.utilitiesdisplay.loadscreenhdghh.LoadScreenHdgHHService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlDTO;
import au.awh.file.equipmentreadings.addembatchreadinghh.AddemBatchReadingHhDTO;
import au.awh.file.utilitiesandusersource.fixean13alpha.FixEan13AlphaDTO;
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthDTO;
import au.awh.file.utilitiesandusersource.processeean13.ProcesseEan13DTO;
import au.awh.file.utilitiesdisplay.loadscreenhdghh.LoadScreenHdgHHDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlParams;
import au.awh.file.equipmentreadings.addembatchreadinghh.AddemBatchReadingHhParams;
import au.awh.file.utilitiesandusersource.fixean13alpha.FixEan13AlphaParams;
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthParams;
import au.awh.file.utilitiesandusersource.processeean13.ProcesseEan13Params;
import au.awh.file.utilitiesdisplay.loadscreenhdghh.LoadScreenHdgHHParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlResult;
import au.awh.file.equipmentreadings.addembatchreadinghh.AddemBatchReadingHhResult;
import au.awh.file.utilitiesandusersource.fixean13alpha.FixEan13AlphaResult;
import au.awh.file.utilitiesandusersource.getstringlength.GetStringLengthResult;
import au.awh.file.utilitiesandusersource.processeean13.ProcesseEan13Result;
import au.awh.file.utilitiesdisplay.loadscreenhdghh.LoadScreenHdgHHResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 13
import au.awh.model.CmdKeyEnum;
import au.awh.model.Ean13TypeEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Enter Batch Readings HH' (WSPLTXAPVK) of file 'Equipment Reading Control' (WSERCTL)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service
public class EnterBatchReadingsHhService extends AbstractService<EnterBatchReadingsHhService, EnterBatchReadingsHhState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private EquipmentReadingControlRepository equipmentReadingControlRepository;

    
    public static final String SCREEN_PROMPT = "enterBatchReadingsHh";
    public static final String SCREEN_CONFIRM_PROMPT = "EnterBatchReadingsHh.key2";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",EnterBatchReadingsHhParams.class, this::executeService);
    private final Step response = define("response",EnterBatchReadingsHhDTO.class, this::processResponse);
//  private final Step displayConfirmPrompt = define("displayConfirmPrompt",EnterBatchReadingsHhDTO.class, this::processDisplayConfirmPrompt);

	//private final Step serviceGetStringLength = define("serviceGetStringLength",GetStringLengthResult.class, this::processServiceGetStringLength);
	//private final Step serviceFixEan13Alpha = define("serviceFixEan13Alpha",FixEan13AlphaResult.class, this::processServiceFixEan13Alpha);
	//private final Step serviceProcesseEan13 = define("serviceProcesseEan13",ProcesseEan13Result.class, this::processServiceProcesseEan13);
	//private final Step serviceGeteEquipReadingCtrl = define("serviceGeteEquipReadingCtrl",GeteEquipReadingCtrlResult.class, this::processServiceGeteEquipReadingCtrl);
	//private final Step serviceAddemBatchReadingHh = define("serviceAddemBatchReadingHh",AddemBatchReadingHhResult.class, this::processServiceAddemBatchReadingHh);
	//private final Step serviceLoadScreenHdgHH = define("serviceLoadScreenHdgHH",LoadScreenHdgHHResult.class, this::processServiceLoadScreenHdgHH);
	

	
    	
@Autowired
private AddemBatchReadingHhService addemBatchReadingHhService;
	
@Autowired
private FixEan13AlphaService fixEan13AlphaService;
	
@Autowired
private GetStringLengthService getStringLengthService;
	
@Autowired
private GeteEquipReadingCtrlService geteEquipReadingCtrlService;
	
@Autowired
private LoadScreenHdgHHService loadScreenHdgHHService;
	
@Autowired
private ProcesseEan13Service processeEan13Service;
	
    @Autowired
    public EnterBatchReadingsHhService()
    {
        super(EnterBatchReadingsHhService.class, EnterBatchReadingsHhState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * EnterBatchReadingsHh service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EnterBatchReadingsHhState state, EnterBatchReadingsHhParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * EnterBatchReadingsHh service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(EnterBatchReadingsHhState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(EnterBatchReadingsHhState state)
    {
        StepResult stepResult = NO_ACTION;

        EnterBatchReadingsHhDTO dto = new EnterBatchReadingsHhDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(EnterBatchReadingsHhState state, EnterBatchReadingsHhDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
//    private StepResult processDisplayConfirmPrompt(EnterBatchReadingsHhState state, EnterBatchReadingsHhDTO dto) {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        stepResult = conductScreenConversation(state);
//
//        return stepResult;
//    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void validateScreenInput(EnterBatchReadingsHhState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(EnterBatchReadingsHhState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(EnterBatchReadingsHhState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EnterBatchReadingsHhState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        EnterBatchReadingsHhResult params = new EnterBatchReadingsHhResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            LoadScreenHdgHHParams loadScreenHdgHHParams = null;
			LoadScreenHdgHHResult loadScreenHdgHHResult = null;
			// DEBUG genFunctionCall BEGIN 1000974 ACT Load Screen Hdg H/H - Utilities Display  *
			loadScreenHdgHHParams = new LoadScreenHdgHHParams();
			loadScreenHdgHHResult = new LoadScreenHdgHHResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, loadScreenHdgHHParams);
			loadScreenHdgHHParams.setCentreName("READING ENTRY");
			stepResult = loadScreenHdgHHService.execute(loadScreenHdgHHParams);
			loadScreenHdgHHResult = (LoadScreenHdgHHResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(loadScreenHdgHHResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1058 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000330 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            FixEan13AlphaParams fixEan13AlphaParams = null;
			GeteEquipReadingCtrlParams geteEquipReadingCtrlParams = null;
			ProcesseEan13Params processeEan13Params = null;
			GetStringLengthParams getStringLengthParams = null;
			FixEan13AlphaResult fixEan13AlphaResult = null;
			ProcesseEan13Result processeEan13Result = null;
			GetStringLengthResult getStringLengthResult = null;
			GeteEquipReadingCtrlResult geteEquipReadingCtrlResult = null;
			if (dto.getEan13Alpha() != null && dto.getEan13Alpha().isEmpty()) {
				// DTL.EAN 13 Alpha is Not Entered
				// DEBUG genFunctionCall BEGIN 1000558 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000562 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000567 ACT LCL.Character = DTL.EAN 13 Alpha
				dto.setLclCharacter(dto.getEan13Alpha());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000571 ACT Get String Length - Utilities & User Source  *
				getStringLengthParams = new GetStringLengthParams();
				getStringLengthResult = new GetStringLengthResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, getStringLengthParams);
				getStringLengthParams.setQclscanString(dto.getEan13Alpha());
				getStringLengthParams.setQclscanStringLength("13");
				getStringLengthParams.setIncludeLeadingBlanks("Y");
				stepResult = getStringLengthService.execute(getStringLengthParams);
				getStringLengthResult = (GetStringLengthResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(getStringLengthResult.get_SysReturnCode());
				dto.setLclQclscanResult(getStringLengthResult.getQclscanResult());
				// DEBUG genFunctionCall END
				if ((dto.getLclQclscanResult() >= 12 && dto.getLclQclscanResult() <= 13) && (dto.getLclCharacter().trim().compareTo("0") >= 0 && dto.getLclCharacter().trim().compareTo("9") <= 0 )) {
					// See if EAN
					// DEBUG genFunctionCall BEGIN 1000584 ACT Fix Ean 13 Alpha - Utilities & User Source  *
					fixEan13AlphaParams = new FixEan13AlphaParams();
					fixEan13AlphaResult = new FixEan13AlphaResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, fixEan13AlphaParams);
					fixEan13AlphaParams.setEan13Alpha(dto.getEan13Alpha());
					stepResult = fixEan13AlphaService.execute(fixEan13AlphaParams);
					fixEan13AlphaResult = (FixEan13AlphaResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(fixEan13AlphaResult.get_SysReturnCode());
					dto.setEan13Alpha(fixEan13AlphaResult.getEan13Alpha());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000593 ACT LCL.EAN 13 = CVTVAR(DTL.EAN 13 Alpha)
					dto.setLclEan13(Integer.valueOf(dto.getEan13Alpha()));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000597 ACT ProcessE EAN 13 - Utilities & User Source  *
					processeEan13Params = new ProcesseEan13Params();
					processeEan13Result = new ProcesseEan13Result();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, processeEan13Params);
					processeEan13Params.setEan13(dto.getLclEan13());
					processeEan13Params.setErrorProcessing("2");
					stepResult = processeEan13Service.execute(processeEan13Params);
					processeEan13Result = (ProcesseEan13Result)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(processeEan13Result.get_SysReturnCode());
					dto.setLclEan13Type(processeEan13Result.getEan13Type());
					dto.setLclEan13Surrogate(processeEan13Result.getEan13Surrogate());
					dto.setLclEan13SampleSequence(processeEan13Result.getEan13SampleSequence());
					dto.setLclFolioNumberKey(processeEan13Result.getFolioNumberKey());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000605 ACT LCL.EAN 13 Barcode = CVTVAR(LCL.EAN 13)
					dto.setLclEan13Barcode(String.format("%013d", dto.getLclEan13()));
					// DEBUG genFunctionCall END
					if (dto.getLclEan13Type() == Ean13TypeEnum._EQUIPMENT) {
						// LCL.EAN 13 Type is Equipment
						// DEBUG genFunctionCall BEGIN 1000616 ACT DTL.Equip Reading Batch No. = LCL.EAN 13 Surrogate
						dto.setEquipReadingBatchNo(dto.getLclEan13Surrogate());
						// DEBUG genFunctionCall END
						// 
					} else {
						// *OTHERWISE
						// DEBUG genFunctionCall BEGIN 1000818 ACT Send error message - 'EAN Type Invalid'
						//dto.addMessage("ean13", "ean.type.invalid", messageSource);
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1000823 ACT <-- *QUIT
						// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
						// DEBUG genFunctionCall END
					}
				} else {
					// *OTHERWISE
					// Otherwise assume it is a Batch Number
					// DEBUG genFunctionCall BEGIN 1000945 ACT LCL.EAN 13 = CVTVAR(DTL.EAN 13 Alpha)
					dto.setLclEan13(Integer.valueOf(dto.getEan13Alpha()));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000896 ACT DTL.Equip Reading Batch No. = CVTVAR(LCL.EAN 13)
					dto.setEquipReadingBatchNo(dto.getLclEan13());
					// DEBUG genFunctionCall END
				}
			}
			// DEBUG genFunctionCall BEGIN 1000953 ACT GetE Equip Reading Ctrl - Equipment Reading Control  *
			geteEquipReadingCtrlParams = new GeteEquipReadingCtrlParams();
			geteEquipReadingCtrlResult = new GeteEquipReadingCtrlResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteEquipReadingCtrlParams);
			//geteEquipReadingCtrlParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			geteEquipReadingCtrlParams.setErrorProcessing("2");
			geteEquipReadingCtrlParams.setGetRecordOption("");
			stepResult = geteEquipReadingCtrlService.execute(geteEquipReadingCtrlParams);
			geteEquipReadingCtrlResult = (GeteEquipReadingCtrlResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteEquipReadingCtrlResult.get_SysReturnCode());
			dto.setAwhRegionCode(geteEquipReadingCtrlResult.getAwhRegionCode());
			dto.setCentreCodeKey(geteEquipReadingCtrlResult.getCentreCodeKey());
			dto.setReadingRequiredDate(geteEquipReadingCtrlResult.getReadingRequiredDate());
			dto.setEquipReadingCtrlSts(geteEquipReadingCtrlResult.getEquipReadingCtrlSts());
			dto.setEquipmentLeasor(geteEquipReadingCtrlResult.getEquipmentLeasor());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000337 ACT Centre Name Drv          *FIELD                                             DTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000337
			// DEBUG X2EActionDiagramElement 1000337 ACT     Centre Name Drv          *FIELD                                             DTLC
			// DEBUG X2EActionDiagramElement 1000338 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000339 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000963 ACT DTL.*Condition name = Condition name of DTL.Equip Reading Ctrl Sts
			dto.set_SysConditionName(dto.getEquipReadingCtrlSts().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            AddemBatchReadingHhParams addemBatchReadingHhParams = null;
			AddemBatchReadingHhResult addemBatchReadingHhResult = null;
			// DEBUG genFunctionCall BEGIN 1000969 ACT AddE:M Batch Reading HH - Equipment Readings  *
			addemBatchReadingHhParams = new AddemBatchReadingHhParams();
			addemBatchReadingHhResult = new AddemBatchReadingHhResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, addemBatchReadingHhParams);
			addemBatchReadingHhParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			addemBatchReadingHhParams.setErrorProcessing("2");
			// TODO SPLIT FUNCTION PMTRCD EquipmentReadingControl.enterBatchReadingsHh (41) -> EXCINTFUN EquipmentReadings.addemBatchReadingHh
			stepResult = addemBatchReadingHhService.execute(addemBatchReadingHhParams);
			addemBatchReadingHhResult = (AddemBatchReadingHhResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(addemBatchReadingHhResult.get_SysReturnCode());
			dto.setLclExitProgramOption(addemBatchReadingHhResult.getExitProgramOption());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(EnterBatchReadingsHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000247 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GetStringLengthService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetStringLength(EnterBatchReadingsHhState state, GetStringLengthParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * FixEan13AlphaService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceFixEan13Alpha(EnterBatchReadingsHhState state, FixEan13AlphaParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ProcesseEan13Service returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceProcesseEan13(EnterBatchReadingsHhState state, ProcesseEan13Params serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteEquipReadingCtrlService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipReadingCtrl(EnterBatchReadingsHhState state, GeteEquipReadingCtrlParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * AddemBatchReadingHhService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAddemBatchReadingHh(EnterBatchReadingsHhState state, AddemBatchReadingHhParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * LoadScreenHdgHHService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceLoadScreenHdgHH(EnterBatchReadingsHhState state, LoadScreenHdgHHParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EnterBatchReadingsHhParams enterBatchReadingsHhParams = new EnterBatchReadingsHhParams();
//        BeanUtils.copyProperties(state, enterBatchReadingsHhParams);
//        stepResult = StepResult.callService(EnterBatchReadingsHhService.class, enterBatchReadingsHhParams);
//
//        return stepResult;
//    }
//


}
