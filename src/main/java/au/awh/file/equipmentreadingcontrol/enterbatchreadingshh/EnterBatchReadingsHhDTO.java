package au.awh.file.equipmentreadingcontrol.enterbatchreadingshh;

import java.time.LocalDate;


import org.springframework.beans.BeanUtils;

import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
// generateStatusFieldImportStatements BEGIN 18
import au.awh.model.Ean13TypeEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Reading Control' (WSERCTL) and function 'Enter Batch Readings HH' (WSPLTXAPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class EnterBatchReadingsHhDTO extends BaseDTO {
	private static final long serialVersionUID = -6249555613131584983L;

// DEBUG fields
	private String ean13Alpha = ""; // 36564
	private String centreNameDrv = ""; // 34917
	private long equipReadingBatchNo = 0L; // 56572
	private String awhRegionCode = ""; // 56452
	private String centreCodeKey = ""; // 1140
	private LocalDate readingRequiredDate = null; // 56555
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = null; // 56566
	private String equipmentLeasor = ""; // 56525
// DEBUG detail fields
	private String _SysconditionName = ""; // 272
// DEBUG param
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; //0
// DEBUG local fields
	private String lclFolioNumberKey = ""; // 1403
	private String lclCharacter = ""; // 29524
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL; // 29901
	private long lclQclscanResult = 0L; // 32105
	private String lclEan13Barcode = ""; // 35308
	private long lclEan13 = 0L; // 36544
	private Ean13TypeEnum lclEan13Type = Ean13TypeEnum._FOLIO_NUMBER; // 36557
	private long lclEan13Surrogate = 0L; // 36558
	private long lclEan13SampleSequence = 0L; // 36559
// DEBUG Constructor

	public EnterBatchReadingsHhDTO() {

	}

	public EnterBatchReadingsHhDTO(EquipmentReadingControl equipmentReadingControl) {
		BeanUtils.copyProperties(equipmentReadingControl, this);
	}

	public EnterBatchReadingsHhDTO(long equipReadingBatchNo, String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate, EquipReadingCtrlStsEnum equipReadingCtrlSts, String equipmentLeasor) {
		this.equipReadingBatchNo = equipReadingBatchNo;
		this.awhRegionCode = awhRegionCode;
		this.centreCodeKey = centreCodeKey;
		this.readingRequiredDate = readingRequiredDate;
		this.equipReadingCtrlSts = equipReadingCtrlSts;
		this.equipmentLeasor = equipmentLeasor;
	}

	public void setEan13Alpha(String ean13Alpha) {
		this.ean13Alpha = ean13Alpha;
	}

	public String getEan13Alpha() {
		return ean13Alpha;
	}

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}

	public long getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}

	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void set_SysConditionName(String conditionName) {
		this._SysconditionName = conditionName;
	}

	public String get_SysConditionName() {
		return _SysconditionName;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public void setLclFolioNumberKey(String folioNumberKey) {
		this.lclFolioNumberKey = folioNumberKey;
	}

	public String getLclFolioNumberKey() {
		return lclFolioNumberKey;
	}

	public void setLclCharacter(String character) {
		this.lclCharacter = character;
	}

	public String getLclCharacter() {
		return lclCharacter;
	}

	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.lclExitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getLclExitProgramOption() {
		return lclExitProgramOption;
	}

	public void setLclQclscanResult(long qclscanResult) {
		this.lclQclscanResult = qclscanResult;
	}

	public long getLclQclscanResult() {
		return lclQclscanResult;
	}

	public void setLclEan13Barcode(String ean13Barcode) {
		this.lclEan13Barcode = ean13Barcode;
	}

	public String getLclEan13Barcode() {
		return lclEan13Barcode;
	}

	public void setLclEan13(long ean13) {
		this.lclEan13 = ean13;
	}

	public long getLclEan13() {
		return lclEan13;
	}

	public void setLclEan13Type(Ean13TypeEnum ean13Type) {
		this.lclEan13Type = ean13Type;
	}

	public Ean13TypeEnum getLclEan13Type() {
		return lclEan13Type;
	}

	public void setLclEan13Surrogate(long ean13Surrogate) {
		this.lclEan13Surrogate = ean13Surrogate;
	}

	public long getLclEan13Surrogate() {
		return lclEan13Surrogate;
	}

	public void setLclEan13SampleSequence(long ean13SampleSequence) {
		this.lclEan13SampleSequence = ean13SampleSequence;
	}

	public long getLclEan13SampleSequence() {
		return lclEan13SampleSequence;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentReadingControl EquipmentReadingControl Entity bean
     */
    public void setDtoFields(EquipmentReadingControl equipmentReadingControl) {
        BeanUtils.copyProperties(equipmentReadingControl, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentReadingControl EquipmentReadingControl Entity bean
     */
    public void setEntityFields(EquipmentReadingControl equipmentReadingControl) {
        BeanUtils.copyProperties(this, equipmentReadingControl);
    }
}
