package au.awh.file.equipmentreadingcontrol.enterbatchreadingshh;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.Ean13TypeEnum;
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Reading Control' (WSERCTL) and function 'Enter Batch Readings HH' (WSPLTXAPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class EnterBatchReadingsHhState extends EnterBatchReadingsHhDTO {
    private static final long serialVersionUID = -7478499952654990222L;

    // Local fields
    private String lclCharacter = "";
    private long lclEan13 = 0L;
    private String lclEan13Barcode = "";
    private long lclEan13SampleSequence = 0L;
    private long lclEan13Surrogate = 0L;
    private Ean13TypeEnum lclEan13Type = Ean13TypeEnum._FOLIO_NUMBER;
    private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL;
    private String lclFolioNumberKey = "";
    private long lclQclscanResult = 0L;

    // System fields
    private boolean _sysErrorFound = false;

    public EnterBatchReadingsHhState() {
    }

    public EnterBatchReadingsHhState(EquipmentReadingControl equipmentReadingControl) {
        setDtoFields(equipmentReadingControl);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclCharacter(String character) {
        this.lclCharacter = character;
    }

    public String getLclCharacter() {
        return lclCharacter;
    }
    public void setLclEan13(long ean13) {
        this.lclEan13 = ean13;
    }

    public long getLclEan13() {
        return lclEan13;
    }
    public void setLclEan13Barcode(String ean13Barcode) {
        this.lclEan13Barcode = ean13Barcode;
    }

    public String getLclEan13Barcode() {
        return lclEan13Barcode;
    }
    public void setLclEan13SampleSequence(long ean13SampleSequence) {
        this.lclEan13SampleSequence = ean13SampleSequence;
    }

    public long getLclEan13SampleSequence() {
        return lclEan13SampleSequence;
    }
    public void setLclEan13Surrogate(long ean13Surrogate) {
        this.lclEan13Surrogate = ean13Surrogate;
    }

    public long getLclEan13Surrogate() {
        return lclEan13Surrogate;
    }
    public void setLclEan13Type(Ean13TypeEnum ean13Type) {
        this.lclEan13Type = ean13Type;
    }

    public Ean13TypeEnum getLclEan13Type() {
        return lclEan13Type;
    }
    public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
        return lclExitProgramOption;
    }
    public void setLclFolioNumberKey(String folioNumberKey) {
        this.lclFolioNumberKey = folioNumberKey;
    }

    public String getLclFolioNumberKey() {
        return lclFolioNumberKey;
    }
    public void setLclQclscanResult(long qclscanResult) {
        this.lclQclscanResult = qclscanResult;
    }

    public long getLclQclscanResult() {
        return lclQclscanResult;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
