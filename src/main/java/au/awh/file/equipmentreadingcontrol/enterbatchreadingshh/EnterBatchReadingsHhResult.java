package au.awh.file.equipmentreadingcontrol.enterbatchreadingshh;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: EnterBatchReadingsHh (WSPLTXAPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class EnterBatchReadingsHhResult implements Serializable
{
	private static final long serialVersionUID = 2922372835996287330L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
