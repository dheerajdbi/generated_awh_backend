package au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.freschelegacy.utils.RestResponsePage;

import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
// generateStatusFieldImportStatements BEGIN 19
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Reading Control' (WSERCTL) and function 'Wrk Equipment Readings' (WSPLTWQDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class WrkEquipmentReadingsWspltwqdfkDTO extends BaseDTO {
	private static final long serialVersionUID = -8921030031912775867L;

    private RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String outputQueue = "";
	private String outputQueueLibrary = "";
	private String copies = "";
	private FormsEnum forms = null;
	private HoldClFormatyesnoEnum holdClFormatyesno = null;
	private SaveClFormatyesnoEnum saveClFormatyesno = null;
	private String printJobName = "";
	private String spoolFileName = "";
	private String overflowLineNumberText = "";
	private String awhRegionCode = "";
	private String awhRegionNameDrv = "";
	private String text15Chars = "";
	private String centreCodeKey = "";
	private String centreNameDrv = "";
	private LocalDate readingRequiredDate = null;
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = EquipReadingCtrlStsEnum._COMPLETE;
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL;


	private WrkEquipmentReadingsWspltwqdfkGDO gdo;

    public WrkEquipmentReadingsWspltwqdfkDTO() {

    }

	public WrkEquipmentReadingsWspltwqdfkDTO(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

    public void setPageGdo(RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
    }

    public String getOutputQueue() {
    	return outputQueue;
    }

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
    }

    public String getOutputQueueLibrary() {
    	return outputQueueLibrary;
    }

	public void setCopies(String copies) {
		this.copies = copies;
    }

    public String getCopies() {
    	return copies;
    }

	public void setForms(FormsEnum forms) {
		this.forms = forms;
    }

    public FormsEnum getForms() {
    	return forms;
    }

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
    }

    public HoldClFormatyesnoEnum getHoldClFormatyesno() {
    	return holdClFormatyesno;
    }

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
    }

    public SaveClFormatyesnoEnum getSaveClFormatyesno() {
    	return saveClFormatyesno;
    }

	public void setPrintJobName(String printJobName) {
		this.printJobName = printJobName;
    }

    public String getPrintJobName() {
    	return printJobName;
    }

	public void setSpoolFileName(String spoolFileName) {
		this.spoolFileName = spoolFileName;
    }

    public String getSpoolFileName() {
    	return spoolFileName;
    }

	public void setOverflowLineNumberText(String overflowLineNumberText) {
		this.overflowLineNumberText = overflowLineNumberText;
    }

    public String getOverflowLineNumberText() {
    	return overflowLineNumberText;
    }

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
    }

    public String getAwhRegionNameDrv() {
    	return awhRegionNameDrv;
    }

	public void setText15Chars(String text15Chars) {
		this.text15Chars = text15Chars;
    }

    public String getText15Chars() {
    	return text15Chars;
    }

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
    }

    public String getCentreCodeKey() {
    	return centreCodeKey;
    }

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
    }

    public String getCentreNameDrv() {
    	return centreNameDrv;
    }

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
    }

    public LocalDate getReadingRequiredDate() {
    	return readingRequiredDate;
    }

	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
    }

    public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
    	return equipReadingCtrlSts;
    }

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
    	return exitProgramOption;
    }
	public void setGdo(WrkEquipmentReadingsWspltwqdfkGDO gdo) {
		this.gdo = gdo;
	}

	public WrkEquipmentReadingsWspltwqdfkGDO getGdo() {
		return gdo;
	}

}