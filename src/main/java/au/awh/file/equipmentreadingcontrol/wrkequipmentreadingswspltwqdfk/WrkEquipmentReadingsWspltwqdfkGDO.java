package au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import au.awh.config.LocalDateDeserializer;
import au.awh.config.LocalDateSerializer;

// generateStatusFieldImportStatements BEGIN 8
import au.awh.model.EquipReadingCtrlStsEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;


/**
 * Gdo for file 'Equipment Reading Control' (WSERCTL) and function 'Wrk Equipment Readings' (WSPLTWQDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class WrkEquipmentReadingsWspltwqdfkGDO implements Serializable {
	private static final long serialVersionUID = 4972218310812911615L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String awhRegionCode = "";
	private String centreCodeKey = "";
	private String centreNameDrv = "";
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate readingRequiredDate = null;
	private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;
	private String equipmentLeasor = "";
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = null;
	private String conditionName = "";

	public WrkEquipmentReadingsWspltwqdfkGDO() {

	}

   	public WrkEquipmentReadingsWspltwqdfkGDO(String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate, BigDecimal equipReadingBatchNo, String equipmentLeasor, EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.awhRegionCode = awhRegionCode;
		this.centreCodeKey = centreCodeKey;
		this.readingRequiredDate = readingRequiredDate;
		this.equipReadingBatchNo = equipReadingBatchNo;
		this.equipmentLeasor = equipmentLeasor;
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setAwhRegionCode(String awhRegionCode) {
    	this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
    	this.centreCodeKey = centreCodeKey;
    }

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreNameDrv(String centreNameDrv) {
    	this.centreNameDrv = centreNameDrv;
    }

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
    	this.readingRequiredDate = readingRequiredDate;
    }

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
    	this.equipReadingBatchNo = equipReadingBatchNo;
    }

	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
    	this.equipmentLeasor = equipmentLeasor;
    }

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
    	this.equipReadingCtrlSts = equipReadingCtrlSts;
    }

	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}

	public void setConditionName(String conditionName) {
    	this.conditionName = conditionName;
    }

	public String getConditionName() {
		return conditionName;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}