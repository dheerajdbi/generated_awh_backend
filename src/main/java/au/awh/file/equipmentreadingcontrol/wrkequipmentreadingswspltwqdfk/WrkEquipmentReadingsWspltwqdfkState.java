package au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;

// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SendViaPrintedReportEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Equipment Reading Control' (WSERCTL) and function 'Wrk Equipment Readings' (WSPLTWQDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
 public class WrkEquipmentReadingsWspltwqdfkState extends WrkEquipmentReadingsWspltwqdfkDTO {
	private static final long serialVersionUID = 7194118921450113423L;

	// Local fields
	private FirstPassEnum lclFirstPass = null;
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL;
	private SendViaPrintedReportEnum lclSendViaEmail = SendViaPrintedReportEnum._USE_HIGHER_LEVEL;

	// System fields
	private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
	private RecordSelectedEnum _sysRecordSelected = null;
	private ReReadSubfileRecordEnum _sysReReadSubfileRecord = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public WrkEquipmentReadingsWspltwqdfkState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
    	this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
    	return lclExitProgramOption;
    }
	public void setLclFirstPass(FirstPassEnum firstPass) {
    	this.lclFirstPass = firstPass;
    }

    public FirstPassEnum getLclFirstPass() {
    	return lclFirstPass;
    }
	public void setLclSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
    	this.lclSendViaEmail = sendViaEmail;
    }

    public SendViaPrintedReportEnum getLclSendViaEmail() {
    	return lclSendViaEmail;
    }

	public void set_SysReReadSubfileRecord(ReReadSubfileRecordEnum reReadSubfileRecord) {
    	_sysReReadSubfileRecord = reReadSubfileRecord;
    }

    public ReReadSubfileRecordEnum get_SysReReadSubfileRecord() {
    	return _sysReReadSubfileRecord;
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
    	_sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
    	return _sysRecordSelected;
    }

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
    	_sysReturnCode = returnCode;
    }

    public ReturnCodeEnum get_SysReturnCode() {
    	return _sysReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}