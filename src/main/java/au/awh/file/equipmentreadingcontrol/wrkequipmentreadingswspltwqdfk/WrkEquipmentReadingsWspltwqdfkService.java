package au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlRepository;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingService;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkService;
import au.awh.file.utilitiesprintandemail.confirmeprint.ConfirmePrintService;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersService;
// asServiceDtoImportStatement
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingDTO;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkDTO;
import au.awh.file.utilitiesprintandemail.confirmeprint.ConfirmePrintDTO;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersDTO;
// asServiceParamsImportStatement
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingParams;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkParams;
import au.awh.file.utilitiesprintandemail.confirmeprint.ConfirmePrintParams;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersParams;
// asServiceResultImportStatement
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingResult;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkResult;
import au.awh.file.utilitiesprintandemail.confirmeprint.ConfirmePrintResult;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkService;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkDTO;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkParams;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkResult;


// generateImportsForEnum
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 22
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.StateCodeKeyEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * DSPFIL Service controller for 'Wrk Equipment Readings' (WSPLTWQDFK) of file 'Equipment Reading Control' (WSERCTL)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class WrkEquipmentReadingsWspltwqdfkService extends AbstractService<WrkEquipmentReadingsWspltwqdfkService, WrkEquipmentReadingsWspltwqdfkState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private EquipmentReadingControlRepository equipmentReadingControlRepository;
    
    @Autowired
    private BldprteEquipmentReadingService bldprteEquipmentReadingService;
    
    @Autowired
    private ConfirmePrintService confirmePrintService;
    
    @Autowired
    private SetWsPrintParametersService setWsPrintParametersService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "wrkEquipmentReadingsWspltwqdfk";
    public static final String SCREEN_RCD = "WrkEquipmentReadingsWspltwqdfk.rcd";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", WrkEquipmentReadingsWspltwqdfkParams.class, this::executeService);
    private final Step response = define("response", WrkEquipmentReadingsWspltwqdfkDTO.class, this::processResponse);

	private final Step serviceEquipmentReadingsWspltwmdfk = define("serviceEquipmentReadingsWspltwmdfk",EquipmentReadingsWspltwmdfkResult.class, this::processServiceEquipmentReadingsWspltwmdfk);
	//private final Step serviceSetWsPrintParameters = define("serviceSetWsPrintParameters",SetWsPrintParametersResult.class, this::processServiceSetWsPrintParameters);
	//private final Step serviceBldprteEquipmentReading = define("serviceBldprteEquipmentReading",BldprteEquipmentReadingResult.class, this::processServiceBldprteEquipmentReading);
	//private final Step serviceConfirmePrint = define("serviceConfirmePrint",ConfirmePrintResult.class, this::processServiceConfirmePrint);
	

    
    @Autowired
    public WrkEquipmentReadingsWspltwqdfkService() {
        super(WrkEquipmentReadingsWspltwqdfkService.class, WrkEquipmentReadingsWspltwqdfkState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(WrkEquipmentReadingsWspltwqdfkState state, WrkEquipmentReadingsWspltwqdfkParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_CTL initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(WrkEquipmentReadingsWspltwqdfkState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Load pageGdo
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(WrkEquipmentReadingsWspltwqdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrInitializeSubfileControl(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
		    stepResult = loadNextSubfilePage(state);
		}

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(WrkEquipmentReadingsWspltwqdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	List<WrkEquipmentReadingsWspltwqdfkGDO> list = state.getPageGdo().getContent();
        for (WrkEquipmentReadingsWspltwqdfkGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            stepResult = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//            TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//            TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(WrkEquipmentReadingsWspltwqdfkState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            WrkEquipmentReadingsWspltwqdfkDTO model = new WrkEquipmentReadingsWspltwqdfkDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(WrkEquipmentReadingsWspltwqdfkState state, WrkEquipmentReadingsWspltwqdfkDTO model) {
    	StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
           // return processScreen(state);
        } else if (isHelp(state.get_SysCmdKey())) {
//        TODO:processHelpRequest(state);//synon built-in function
        }
        /*else if (state.get_SysCmdKey().equals(CmdKeyEnum._CF11)) {
        	state
        	
        	stepResult = processScreen(state);
        }*/
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(WrkEquipmentReadingsWspltwqdfkState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrProcessSubfilePreConfirm(state);
        if ( stepResult != NO_ACTION) {
            return stepResult;
        }

        if (state.get_SysErrorFound()) {
            return closedown(state);
        } else {
            /*if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
                return closedown(state);
            } else {*/
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	stepResult = usrProcessCommandKeys(state);
//		        }

                stepResult = usrProcessSubfileControlPostConfirm(state);
                for (WrkEquipmentReadingsWspltwqdfkGDO gdo : state.getPageGdo().getContent()) {
                    if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                    	stepResult = usrSubfileRecordFunctionFields(state, gdo);
                    	stepResult = usrProcessSubfileRecordPostConfirm(state, gdo);
                        
//	                    TODO:writeSubfileRecord(state);   // synon built-in function
		            }
		        }
		        stepResult = usrFinalProcessingPostConfirm(state);
                stepResult = usrProcessCommandKeys(state);
            }
        return mainLoop(state);
        }

      //  stepResult = conductScreenConversation(state);

        
   

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult usrProcessSubfilePreConfirm(WrkEquipmentReadingsWspltwqdfkState state) {
    	StepResult stepResult = NO_ACTION;

	stepResult = usrSubfileControlFunctionFields(state);
        stepResult = usrProcessSubfileControlPreConfirm(state);
    	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
            for (WrkEquipmentReadingsWspltwqdfkGDO gdo : state.getPageGdo().getContent()) {
                
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = usrSubfileRecordFunctionFields(state, gdo);
                    stepResult = usrProcessSubfileRecordPreConfirm(state, gdo);
                    if (stepResult != NO_ACTION) {
                        return stepResult;
                    }
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
                
            }
        }

	stepResult = usrFinalProcessingPreConfirm(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(WrkEquipmentReadingsWspltwqdfkState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);
        //may be should not be there...
        WrkEquipmentReadingsWspltwqdfkResult params = new WrkEquipmentReadingsWspltwqdfkResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(WrkEquipmentReadingsWspltwqdfkState state) {
       // state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
	private void dbfReadNextPageRecord(WrkEquipmentReadingsWspltwqdfkState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(WrkEquipmentReadingsWspltwqdfkState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }
        Boolean uncompletedOnly = state.getText15Chars().equals("Uncompleted");
        RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> pageGdo = equipmentReadingControlRepository.wrkEquipmentReadingsWspltwqdfk(state.getAwhRegionCode(), state.getCentreCodeKey(), state.getReadingRequiredDate(),uncompletedOnly, pageable);
        state.setPageGdo(pageGdo);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000865 ACT InlFnc Wrk Equip Readings - Equipment Reading Control  *
			/*
			// TODO: Wrapped functions are not supported yet (message surrogate: 2026911)
			inlfncWrkEquipReadingsParams = new InlfncWrkEquipReadingsParams();
			inlfncWrkEquipReadingsResult = new InlfncWrkEquipReadingsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), inlfncWrkEquipReadingsParams);
			inlfncWrkEquipReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
			inlfncWrkEquipReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
			stepResult = inlfncWrkEquipReadingsService.execute(inlfncWrkEquipReadingsParams);
			inlfncWrkEquipReadingsResult = (InlfncWrkEquipReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(inlfncWrkEquipReadingsResult.get_SysReturnCode());
			dto.setCentreCodeKey(inlfncWrkEquipReadingsResult.getCentreCodeKey());
			dto.setAwhRegionCode(inlfncWrkEquipReadingsResult.getAwhRegionCode());
			*/
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000558 ACT LCL.First Pass = CND.First Pass
			dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
			// DEBUG genFunctionCall END
			if (dto.getText15Chars() != null && dto.getText15Chars().isEmpty()) {
				// CTL.Text (15 chars) is Not Entered
				// DEBUG genFunctionCall BEGIN 1000897 ACT CTL.Text (15 chars) = CND.Uncompleted
				dto.setText15Chars("Uncompleted");
				// DEBUG genFunctionCall END
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(WrkEquipmentReadingsWspltwqdfkState dto, WrkEquipmentReadingsWspltwqdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if ((dto.getLclFirstPass() == FirstPassEnum._FIRST_PASS) && !(isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000569 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000573 ACT LCL.First Pass = CND.Not First Pass
				dto.setLclFirstPass(FirstPassEnum._NOT_FIRST_PASS);
				// DEBUG genFunctionCall END
				// 
			} else if ((dto.getLclFirstPass() == FirstPassEnum._NOT_FIRST_PASS) && (isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000584 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000588 ACT LCL.First Pass = CND.First Pass
				dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
				// DEBUG genFunctionCall END
			}
			if ((dto.getText15Chars().equals("UncompletedL")) && (gdo.getEquipReadingCtrlSts() == EquipReadingCtrlStsEnum._COMPLETE)) {
				// DEBUG genFunctionCall BEGIN 1000949 ACT PGM.*Record selected = CND.*NO
				dto.set_SysRecordSelected(RecordSelectedEnum._STA_NO);
				// DEBUG genFunctionCall END
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000870 ACT CLCSFC Wrk Equip Readings - Equipment Reading Control  *
			/*
			// TODO: Wrapped functions are not supported yet (message surrogate: 2026905)
			clcsfcWrkEquipReadingsParams = new ClcsfcWrkEquipReadingsParams();
			clcsfcWrkEquipReadingsResult = new ClcsfcWrkEquipReadingsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), clcsfcWrkEquipReadingsParams);
			clcsfcWrkEquipReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
			clcsfcWrkEquipReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
			clcsfcWrkEquipReadingsParams.setEquipReadingCtrlSts(dto.getEquipReadingCtrlSts());
			stepResult = clcsfcWrkEquipReadingsService.execute(clcsfcWrkEquipReadingsParams);
			clcsfcWrkEquipReadingsResult = (ClcsfcWrkEquipReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(clcsfcWrkEquipReadingsResult.get_SysReturnCode());
			dto.setAwhRegionNameDrv(clcsfcWrkEquipReadingsResult.getAwhRegionNameDrv());
			dto.setCentreNameDrv(clcsfcWrkEquipReadingsResult.getCentreNameDrv());
			*/
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (isCancel_1(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000910 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000914 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysCmdKey() == CmdKeyEnum._CF11) {
				// CTL.*CMD key is CF11
				if (dto.getText15Chars().equals("UncompletedL")) {
					// CTL.Text (15 chars) is Uncompleted
					// DEBUG genFunctionCall BEGIN 1000920 ACT CTL.Text (15 chars) = CND.All Batches
					dto.setText15Chars("All Batches");
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000928 ACT CTL.Text (15 chars) = CND.Uncompleted
					dto.setText15Chars("Uncompleted");
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000936 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
			// * Condition for normal processing of subfile records.
			// DEBUG genFunctionCall BEGIN 1000340 ACT LCL.Exit Program Option = CND.Normal
			dto.setLclExitProgramOption(ExitProgramOptionEnum._NORMAL);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(WrkEquipmentReadingsWspltwqdfkState dto, WrkEquipmentReadingsWspltwqdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000861 ACT Centre Name Drv          *FIELD                                             RCDC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             RCDC 1000861
			// DEBUG X2EActionDiagramElement 1000861 ACT     Centre Name Drv          *FIELD                                             RCDC
			// DEBUG X2EActionDiagramElement 1000862 PAR RCD 
			// DEBUG X2EActionDiagramElement 1000863 PAR RCD 
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(WrkEquipmentReadingsWspltwqdfkState dto, WrkEquipmentReadingsWspltwqdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	ConfirmePrintParams confirmePrintParams = null;
			BldprteEquipmentReadingResult bldprteEquipmentReadingResult = null;
			SetWsPrintParametersParams setWsPrintParametersParams = null;
			BldprteEquipmentReadingParams bldprteEquipmentReadingParams = null;
			EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = null;
			EquipmentReadingsWspltwmdfkResult equipmentReadingsWspltwmdfkResult = null;
			ConfirmePrintResult confirmePrintResult = null;
			SetWsPrintParametersResult setWsPrintParametersResult = null;
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._CANCEL_REQUESTED) {
				// LCL.Exit Program Option is Cancel requested
				// * Reprocess this record next time user presses Enter.
				// DEBUG genFunctionCall BEGIN 1000348 ACT PGM.*Re-read Subfile Record = CND.*YES
				dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum._STA_YES);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				if (!(gdo.get_SysSelected().equals("Not entered"))) {
					// NOT RCD.*SFLSEL is Not entered
					// DEBUG genFunctionCall BEGIN 1000360 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
					// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("Select#1") || gdo.get_SysSelected().equals("*Select#2")) {
					// RCD.*SFLSEL is *Select request
					// DEBUG genFunctionCall BEGIN 1000767 ACT Equipment Readings - Plant Equipment  *
					equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
					equipmentReadingsWspltwmdfkResult = new EquipmentReadingsWspltwmdfkResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), equipmentReadingsWspltwmdfkParams);
					equipmentReadingsWspltwmdfkParams.setEquipReadingBatchNo(gdo.getEquipReadingBatchNo());
					// TODO SPLIT SCREEN DSPFIL EquipmentReadingControl.wrkEquipmentReadingsWspltwqdfk (101) -> DSPFIL PlantEquipment.equipmentReadingsWspltwmdfk
					stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams).thenCall(serviceEquipmentReadingsWspltwmdfk);
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._EXIT_REQUESTED) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// DEBUG genFunctionCall BEGIN 1000781 ACT PAR.Exit Program Option = CND.Exit requested
						dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1000785 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000792 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1000796 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("P")) {
					// RCD.*SFLSEL is P
					// DEBUG genFunctionCall BEGIN 1000854 ACT LCL.Send via Email = CND.No
					dto.setLclSendViaEmail(SendViaPrintedReportEnum._NO);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000804 ACT Set WS print parameters - Utilities Print & Email  *
					setWsPrintParametersParams = new SetWsPrintParametersParams();
					setWsPrintParametersResult = new SetWsPrintParametersResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), setWsPrintParametersParams);
					setWsPrintParametersParams.setBrokerIdKey("");
					setWsPrintParametersParams.setStateCodeKey("");
					setWsPrintParametersParams.setCentreCodeKey(gdo.getCentreCodeKey());
					setWsPrintParametersParams.setReport("EQPREAD");
					stepResult = setWsPrintParametersService.execute(setWsPrintParametersParams);
					setWsPrintParametersResult = (SetWsPrintParametersResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(setWsPrintParametersResult.get_SysReturnCode());
					dto.setOutputQueue(setWsPrintParametersResult.getOutputQueue());
					dto.setOutputQueueLibrary(setWsPrintParametersResult.getOutputQueueLibrary());
					dto.setCopies(setWsPrintParametersResult.getCopies());
					dto.setForms(setWsPrintParametersResult.getForms());
					dto.setHoldClFormatyesno(setWsPrintParametersResult.getHoldClFormatyesno());
					dto.setSaveClFormatyesno(setWsPrintParametersResult.getSaveClFormatyesno());
					dto.setOverflowLineNumberText(setWsPrintParametersResult.getOverflowLineNumberText());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000841 ACT ConfirmE Print - Utilities Print & Email  *
					confirmePrintParams = new ConfirmePrintParams();
					confirmePrintResult = new ConfirmePrintResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), confirmePrintParams);
					confirmePrintParams.setReport("EQPREAD");
					confirmePrintParams.setOutputQueue(dto.getOutputQueue());
					confirmePrintParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
					confirmePrintParams.setCopies(dto.getCopies());
					confirmePrintParams.setForms(dto.getForms());
					confirmePrintParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
					confirmePrintParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
					confirmePrintParams.setSendViaEmail(dto.getLclSendViaEmail());
					confirmePrintParams.setDisplayConfirmPrompt("N");
					confirmePrintParams.setErrorProcessing("2");
					// TODO SPLIT FUNCTION DSPFIL EquipmentReadingControl.wrkEquipmentReadingsWspltwqdfk (101) -> EXCINTFUN UtilitiesPrintAndEmail.confirmePrint
					stepResult = confirmePrintService.execute(confirmePrintParams);
					confirmePrintResult = (ConfirmePrintResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(confirmePrintResult.get_SysReturnCode());
					dto.setOutputQueue(confirmePrintResult.getOutputQueue());
					dto.setOutputQueueLibrary(confirmePrintResult.getOutputQueueLibrary());
					dto.setCopies(confirmePrintResult.getCopies());
					dto.setForms(confirmePrintResult.getForms());
					dto.setHoldClFormatyesno(confirmePrintResult.getHoldClFormatyesno());
					dto.setSaveClFormatyesno(confirmePrintResult.getSaveClFormatyesno());
					dto.setLclSendViaEmail(confirmePrintResult.getSendViaEmail());
					dto.setLclExitProgramOption(confirmePrintResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
						// PGM.*Return code is *Normal
						// DEBUG genFunctionCall BEGIN 1000731 ACT BldPrtE Equipment Reading - Plant Equipment  *
						bldprteEquipmentReadingParams = new BldprteEquipmentReadingParams();
						bldprteEquipmentReadingResult = new BldprteEquipmentReadingResult();
						// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
						BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), bldprteEquipmentReadingParams);
						bldprteEquipmentReadingParams.setErrorProcessing("2");
						bldprteEquipmentReadingParams.setReadingRequiredDate(gdo.getReadingRequiredDate());
						bldprteEquipmentReadingParams.setAwhRegionCode(gdo.getAwhRegionCode());
						bldprteEquipmentReadingParams.setCentreCodeKey(gdo.getCentreCodeKey());
						bldprteEquipmentReadingParams.setOutputQueue(dto.getOutputQueue());
						bldprteEquipmentReadingParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
						bldprteEquipmentReadingParams.setCopies(dto.getCopies());
						bldprteEquipmentReadingParams.setForms(dto.getForms());
						bldprteEquipmentReadingParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
						bldprteEquipmentReadingParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
						bldprteEquipmentReadingParams.setOverflowLineNumberText(dto.getOverflowLineNumberText());
						bldprteEquipmentReadingParams.setCheckForHolidays("N");
						stepResult = bldprteEquipmentReadingService.execute(bldprteEquipmentReadingParams);
						bldprteEquipmentReadingResult = (BldprteEquipmentReadingResult)((ReturnFromService)stepResult.getAction()).getResults();
						dto.set_SysReturnCode(bldprteEquipmentReadingResult.get_SysReturnCode());
						dto.setOutputQueue(bldprteEquipmentReadingResult.getOutputQueue());
						dto.setOutputQueueLibrary(bldprteEquipmentReadingResult.getOutputQueueLibrary());
						dto.setCopies(bldprteEquipmentReadingResult.getCopies());
						dto.setForms(bldprteEquipmentReadingResult.getForms());
						dto.setHoldClFormatyesno(bldprteEquipmentReadingResult.getHoldClFormatyesno());
						dto.setSaveClFormatyesno(bldprteEquipmentReadingResult.getSaveClFormatyesno());
						dto.setOverflowLineNumberText(bldprteEquipmentReadingResult.getOverflowLineNumberText());
						// DEBUG genFunctionCall END
					}
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._EXIT_REQUESTED) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// DEBUG genFunctionCall BEGIN 1000744 ACT PAR.Exit Program Option = CND.Exit requested
						dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1000748 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000755 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
						// DEBUG genFunctionCall END
					}
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(WrkEquipmentReadingsWspltwqdfkState dto, WrkEquipmentReadingsWspltwqdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(WrkEquipmentReadingsWspltwqdfkState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        	
			if (isExit(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000251 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * EquipmentReadingsWspltwmdfkService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEquipmentReadingsWspltwmdfk(WrkEquipmentReadingsWspltwqdfkState state, EquipmentReadingsWspltwmdfkResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        WrkEquipmentReadingsWspltwqdfkParams wrkEquipmentReadingsWspltwqdfkParams = new WrkEquipmentReadingsWspltwqdfkParams();
        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwqdfkParams);
        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwqdfkService.class, wrkEquipmentReadingsWspltwqdfkParams);

        return stepResult;
    }
//
//    /**
//     * SetWsPrintParametersService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceSetWsPrintParameters(WrkEquipmentReadingsWspltwqdfkState state, SetWsPrintParametersParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwqdfkParams wrkEquipmentReadingsWspltwqdfkParams = new WrkEquipmentReadingsWspltwqdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwqdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwqdfkService.class, wrkEquipmentReadingsWspltwqdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * BldprteEquipmentReadingService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceBldprteEquipmentReading(WrkEquipmentReadingsWspltwqdfkState state, BldprteEquipmentReadingParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwqdfkParams wrkEquipmentReadingsWspltwqdfkParams = new WrkEquipmentReadingsWspltwqdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwqdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwqdfkService.class, wrkEquipmentReadingsWspltwqdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ConfirmePrintService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceConfirmePrint(WrkEquipmentReadingsWspltwqdfkState state, ConfirmePrintParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwqdfkParams wrkEquipmentReadingsWspltwqdfkParams = new WrkEquipmentReadingsWspltwqdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwqdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwqdfkService.class, wrkEquipmentReadingsWspltwqdfkParams);
//
//        return stepResult;
//    }
//


}
