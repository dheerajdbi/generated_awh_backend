package au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk;

import java.io.Serializable;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END


/**
 * Result(s) for resource: WrkEquipmentReadingsWspltwqdfk (WSPLTWQDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class WrkEquipmentReadingsWspltwqdfkResult implements Serializable
{
	private static final long serialVersionUID = 5431357811583175842L;

	private ExitProgramOptionEnum exitProgramOption = null;


	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}

}