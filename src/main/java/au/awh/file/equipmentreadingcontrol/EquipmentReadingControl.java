package au.awh.file.equipmentreadingcontrol;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.utils.DateConverters;


@Entity
@Table(name="WSERCTL")
public class EquipmentReadingControl implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private EquipmentReadingControlId id = new EquipmentReadingControlId();

	@Column(name = "ECAWHREG")
	private String awhRegionCode = "";
	
	@Column(name = "ECCTRCDEK")
	private String centreCodeKey = "";
	
	@Column(name = "ECPJST")
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = EquipReadingCtrlStsEnum.fromCode("");
	
	@Column(name = "ECEQLEASOR")
	private String equipmentLeasor = "";
	
	@Column(name = "ECREADDREQ")
	@Convert(converter = DateConverters.class)
	private LocalDate readingRequiredDate = LocalDate.of(1801, 1, 1);

	public EquipmentReadingControlId getId() {
		return id;
	}

	public BigDecimal getEquipReadingBatchNo() {
		return id.getEquipReadingBatchNo();
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	

	

	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.id.setEquipReadingBatchNo(new BigDecimal(equipReadingBatchNo));
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
