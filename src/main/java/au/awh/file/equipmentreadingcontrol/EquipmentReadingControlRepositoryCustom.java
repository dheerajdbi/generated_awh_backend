package au.awh.file.equipmentreadingcontrol;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.EquipReadingCtrlStsEnum;
// generateStatusFieldImportStatements END

import au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk.WrkEquipmentReadingsWspltwqdfkGDO;
import au.awh.file.equipmentreadingcontrol.enterbatchreadingshh.EnterBatchReadingsHhDTO;

/**
 * Custom Spring Data JPA repository interface for model: Equipment Reading Control (WSERCTL).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentReadingControlRepositoryCustom {

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param centreCodeKey Centre Code Key
	 * @param readingRequiredDate Reading Required Date
	 * @param equipReadingCtrlSts Equip Reading Ctrl Sts
	 * @param pageable a Pageable object used for pagination
	 * @return Page of WrkEquipmentReadingsWspltwqdfkGDO
	 */
	RestResponsePage<WrkEquipmentReadingsWspltwqdfkGDO> wrkEquipmentReadingsWspltwqdfk(
	String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate, boolean unCompletedOnly, Pageable pageable);

	/**
	 * 
	 * @param equipReadingBatchNo Equip Reading Batch No.
	 * @return EquipmentReadingControl
	 */
	EquipmentReadingControl getEquipmentReadingCon(long equipReadingBatchNo
	);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EnterBatchReadingsHhDTO
	 */
	RestResponsePage<EnterBatchReadingsHhDTO> enterBatchReadingsHh(
	Pageable pageable);
}
