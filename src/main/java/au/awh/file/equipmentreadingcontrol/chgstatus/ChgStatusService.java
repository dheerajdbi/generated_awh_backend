package au.awh.file.equipmentreadingcontrol.chgstatus;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlId;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlRepository;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Chg Status' (file 'Equipment Reading Control' (WSERCTL)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChgStatusService extends AbstractService<ChgStatusService, ChgStatusDTO>
{
	private final Step execute = define("execute", ChgStatusParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingControlRepository equipmentReadingControlRepository;

	
	@Autowired
	public ChgStatusService() {
		super(ChgStatusService.class, ChgStatusDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgStatusParams params) throws ServiceException {
		ChgStatusDTO dto = new ChgStatusDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgStatusDTO dto, ChgStatusParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		EquipmentReadingControlId equipmentReadingControlId = new EquipmentReadingControlId();
		equipmentReadingControlId.setEquipReadingBatchNo(new BigDecimal(dto.getEquipReadingBatchNo()));
		EquipmentReadingControl equipmentReadingControl = equipmentReadingControlRepository.findById(equipmentReadingControlId).orElse(null);
		if (equipmentReadingControl == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, equipmentReadingControl);
			equipmentReadingControl.setCentreCodeKey(dto.getCentreCodeKey());
			equipmentReadingControl.setAwhRegionCode(dto.getAwhRegionCode());
			equipmentReadingControl.setReadingRequiredDate(dto.getReadingRequiredDate());
			equipmentReadingControl.setEquipReadingCtrlSts(dto.getEquipReadingCtrlSts());
			equipmentReadingControl.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			equipmentReadingControl.setEquipmentLeasor(dto.getEquipmentLeasor());

			processingBeforeDataUpdate(dto, equipmentReadingControl);

			try {
				equipmentReadingControlRepository.saveAndFlush(equipmentReadingControl);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChgStatusResult result = new ChgStatusResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChgStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChgStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChgStatusDTO dto, EquipmentReadingControl equipmentReadingControl) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Empty:48)
		 */
		
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChgStatusDTO dto, EquipmentReadingControl equipmentReadingControl) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:10)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChgStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Empty:23)
		 */
		
		return stepResult;
	}


}
