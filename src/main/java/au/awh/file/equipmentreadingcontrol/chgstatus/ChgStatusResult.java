package au.awh.file.equipmentreadingcontrol.chgstatus;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgStatusResult implements Serializable
{
	private static final long serialVersionUID = -6313113894998292299L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
