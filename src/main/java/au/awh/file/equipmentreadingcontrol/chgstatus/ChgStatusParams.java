package au.awh.file.equipmentreadingcontrol.chgstatus;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.EquipReadingCtrlStsEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgStatusParams implements Serializable
{
	private static final long serialVersionUID = 4119127237874175489L;

    private EquipReadingCtrlStsEnum equipReadingCtrlSts = null;
    private long equipReadingBatchNo = 0L;

	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(String equipReadingCtrlSts) {
		setEquipReadingCtrlSts(EquipReadingCtrlStsEnum.valueOf(equipReadingCtrlSts));
	}
	
	public long getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(String equipReadingBatchNo) {
		setEquipReadingBatchNo(Long.parseLong(equipReadingBatchNo));
	}
}
