package au.awh.file.equipmentreadingcontrol;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EquipmentReadingControlId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "ECERBAT")
	private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;

	public EquipmentReadingControlId() {
	
	}

	public EquipmentReadingControlId(BigDecimal equipReadingBatchNo) { 	this.equipReadingBatchNo = equipReadingBatchNo;
	}

	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
