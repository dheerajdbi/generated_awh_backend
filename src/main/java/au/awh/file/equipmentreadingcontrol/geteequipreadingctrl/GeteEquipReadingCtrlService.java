package au.awh.file.equipmentreadingcontrol.geteequipreadingctrl;

// ExecuteInternalFunction.kt File=EquipmentReadingControl (EC) Function=geteEquipReadingCtrl (1879324) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControl;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlId;
import au.awh.file.equipmentreadingcontrol.EquipmentReadingControlRepository;
// imports for Services
import au.awh.file.equipmentreadingcontrol.getequipmentreadingcon.GetEquipmentReadingConService;
// imports for DTO
import au.awh.file.equipmentreadingcontrol.getequipmentreadingcon.GetEquipmentReadingConDTO;
// imports for Enums
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadingcontrol.getequipmentreadingcon.GetEquipmentReadingConParams;
// imports for Results
import au.awh.file.equipmentreadingcontrol.getequipmentreadingcon.GetEquipmentReadingConResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Equip Reading Ctrl' (file 'Equipment Reading Control' (WSERCTL)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteEquipReadingCtrlService extends AbstractService<GeteEquipReadingCtrlService, GeteEquipReadingCtrlDTO>
{
	private final Step execute = define("execute", GeteEquipReadingCtrlParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingControlRepository equipmentReadingControlRepository;

	@Autowired
	private GetEquipmentReadingConService getEquipmentReadingConService;

	//private final Step serviceGetEquipmentReadingCon = define("serviceGetEquipmentReadingCon",GetEquipmentReadingConResult.class, this::processServiceGetEquipmentReadingCon);
	
	@Autowired
	public GeteEquipReadingCtrlService() {
		super(GeteEquipReadingCtrlService.class, GeteEquipReadingCtrlDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteEquipReadingCtrlParams params) throws ServiceException {
		GeteEquipReadingCtrlDTO dto = new GeteEquipReadingCtrlDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteEquipReadingCtrlDTO dto, GeteEquipReadingCtrlParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetEquipmentReadingConParams getEquipmentReadingConParams = null;
		GetEquipmentReadingConResult getEquipmentReadingConResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Equipment Reading Con - Equipment Reading Control  *
		getEquipmentReadingConParams = new GetEquipmentReadingConParams();
		getEquipmentReadingConResult = new GetEquipmentReadingConResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getEquipmentReadingConParams);
		stepResult = getEquipmentReadingConService.execute(getEquipmentReadingConParams);
		getEquipmentReadingConResult = (GetEquipmentReadingConResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getEquipmentReadingConResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// * NOTE: Insert <file> NF message here.
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Equipment Reading Cont NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878906) EXCINTFUN 1879324 EquipmentReadingControl.geteEquipReadingCtrl 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1879324 EquipmentReadingControl.geteEquipReadingCtrl 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteEquipReadingCtrlResult result = new GeteEquipReadingCtrlResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetEquipmentReadingConService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetEquipmentReadingCon(GeteEquipReadingCtrlDTO dto, GetEquipmentReadingConParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteEquipReadingCtrlParams geteEquipReadingCtrlParams = new GeteEquipReadingCtrlParams();
//        BeanUtils.copyProperties(dto, geteEquipReadingCtrlParams);
//        stepResult = StepResult.callService(GeteEquipReadingCtrlService.class, geteEquipReadingCtrlParams);
//
//        return stepResult;
//    }
//
}
