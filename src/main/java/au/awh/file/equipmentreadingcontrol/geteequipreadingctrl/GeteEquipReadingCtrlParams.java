package au.awh.file.equipmentreadingcontrol.geteequipreadingctrl;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteEquipReadingCtrl ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteEquipReadingCtrlParams implements Serializable
{
	private static final long serialVersionUID = 1434820530916574593L;

    private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;

	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.valueOf(getRecordOption));
	}
}
