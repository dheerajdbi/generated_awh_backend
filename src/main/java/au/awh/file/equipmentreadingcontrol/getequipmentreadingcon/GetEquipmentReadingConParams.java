package au.awh.file.equipmentreadingcontrol.getequipmentreadingcon;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetEquipmentReadingCon ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetEquipmentReadingConParams implements Serializable
{
	private static final long serialVersionUID = 9209681752863097564L;

    private long equipReadingBatchNo = 0L;

	public long getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(String equipReadingBatchNo) {
		setEquipReadingBatchNo(Long.parseLong(equipReadingBatchNo));
	}
}
