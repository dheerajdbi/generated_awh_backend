package au.awh.file.equipmentreadingcontrol.getequipmentreadingcon;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.EquipReadingCtrlStsEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetEquipmentReadingCon ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetEquipmentReadingConResult implements Serializable
{
	private static final long serialVersionUID = -7114218594398419076L;

	private ReturnCodeEnum _sysReturnCode;
	private String awhRegionCode = ""; // String 56452
	private String centreCodeKey = ""; // String 1140
	private LocalDate readingRequiredDate = null; // LocalDate 56555
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = null; // EquipReadingCtrlStsEnum 56566
	private String equipmentLeasor = ""; // String 56525

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(String equipReadingCtrlSts) {
		setEquipReadingCtrlSts(EquipReadingCtrlStsEnum.valueOf(equipReadingCtrlSts));
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
