package au.awh.file.equipmentreadings.addmequipmentreadings;

import java.time.LocalDate;
import java.math.BigDecimal;

import java.time.LocalDateTime;


import org.springframework.beans.BeanUtils;

import au.awh.file.equipmentreadings.EquipmentReadings;
// generateStatusFieldImportStatements BEGIN 46
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CentreTypeEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Readings' (WSEQREAD) and function 'Add:M Equipment Readings' (WSPLTWVPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmEquipmentReadingsDTO extends BaseDTO {
	private static final long serialVersionUID = 1566652208953575892L;

// DEBUG fields
	private long equipmentReadingSgt = 0L; // 56531
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private String plantEquipmentCode = ""; // 56471
	private String equipmentDescription = ""; // 56472
	private String equipmentTypeCode = ""; // 56496
	private String equipmentTypeDescDrv = ""; // 56549
	private String conditionName2 = ""; // 31710
	private String centreCodeKey = ""; // 1140
	private String centreNameDrv = ""; // 34917
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // 56533
	private String conditionName = ""; // 29897
	private LocalDate readingRequiredDate = null; // 56555
	private long differenceFromPrevious = 0L; // 60183
	private long idleDaysFromPrevious = 0L; // 60184
	private long unavailableDaysFromPrv = 0L; // 60185
	private LocalDateTime readingTimestamp = null; // 56485
	private ReadingTypeEnum readingType = null; // 56488
	private long readingValue = 0L; // 56486
	private EquipReadingUnitEnum equipReadingUnit = null; // 56498
	private String readingComment = ""; // 56487
	private String userId = ""; // 4344
	private String equipmentLeasor = ""; // 56525
	private long transferToFromSgt = 0L; // 56539
	private String transferToCentre = ""; // 56579
	private String centreName = ""; // 1142
	private AwhBuisnessSegmentEnum transferToBuisSeg = null; // 56580
	private String conditionName1 = ""; // 32088
// DEBUG detail fields
	private String _SysconditionName = ""; // 272
// DEBUG param
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; //0
// DEBUG local fields
	private CentreTypeEnum lclCentreType = null; // 1149
	private String lclEquipmentBarcode = ""; // 56473
	private String lclEquipmentComment = ""; // 56474
	private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED; // 56488
	private long lclEquipmentReadingSgt = 0L; // 56531
	private LocalDate lclReadingRequiredDate = null; // 56555
	private YesOrNoEnum lclTransferFunction = YesOrNoEnum._NOT_ENTERED; // 56581
	private String lclTransferAwhRegion = ""; // 56582
	private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum._NOT_ENTERED; // 56583
	private String lclEquipmentSerialNumber = ""; // 60161
	private LocalDate lclLeaseComencmentDate = null; // 60165
	private LocalDate lclLeaseExpireDate = null; // 60166
	private BigDecimal lclLeaseMonthlyRateS = BigDecimal.ZERO; // 60169
	private long lclLeaseMaxTotalUnits = 0L; // 60170
	private ReadingFrequencyEnum lclReadingFrequency = ReadingFrequencyEnum._DAILY; // 60171
	private String lclVisualEquipmentCode = ""; // 60172
// DEBUG program fields
	private ReturnCodeEnum returnCode = ReturnCodeEnum._STA_NORMAL; // 149
	private DeferConfirmEnum deferConfirm = null; // 237
	private String cursorField = ""; // 473
// DEBUG Constructor

	public AddmEquipmentReadingsDTO() {

	}

	public AddmEquipmentReadingsDTO(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(equipmentReadings, this);
	}

	public AddmEquipmentReadingsDTO(long equipmentReadingSgt, String awhRegionCode, String plantEquipmentCode, String equipmentDescription, String equipmentTypeCode, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, LocalDate readingRequiredDate, long differenceFromPrevious, long idleDaysFromPrevious, long unavailableDaysFromPrv, LocalDateTime readingTimestamp, ReadingTypeEnum readingType, long readingValue, EquipReadingUnitEnum equipReadingUnit, String readingComment, String userId, String equipmentLeasor, long transferToFromSgt, String centreName) {
		this.equipmentReadingSgt = equipmentReadingSgt;
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentDescription = equipmentDescription;
		this.equipmentTypeCode = equipmentTypeCode;
		this.centreCodeKey = centreCodeKey;
		this.awhBuisnessSegment = awhBuisnessSegment;
		this.readingRequiredDate = readingRequiredDate;
		this.differenceFromPrevious = differenceFromPrevious;
		this.idleDaysFromPrevious = idleDaysFromPrevious;
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
		this.readingTimestamp = readingTimestamp;
		this.readingType = readingType;
		this.readingValue = readingValue;
		this.equipReadingUnit = equipReadingUnit;
		this.readingComment = readingComment;
		this.userId = userId;
		this.equipmentLeasor = equipmentLeasor;
		this.transferToFromSgt = transferToFromSgt;
		this.centreName = centreName;
	}

	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
	}

	public String getAwhRegionNameDrv() {
		return awhRegionNameDrv;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
		this.equipmentTypeDescDrv = equipmentTypeDescDrv;
	}

	public String getEquipmentTypeDescDrv() {
		return equipmentTypeDescDrv;
	}

	public void setConditionName2(String conditionName2) {
		this.conditionName2 = conditionName2;
	}

	public String getConditionName2() {
		return conditionName2;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public String getConditionName() {
		return conditionName;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setDifferenceFromPrevious(long differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}

	public long getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public void setIdleDaysFromPrevious(long idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}

	public long getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public void setUnavailableDaysFromPrv(long unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

	public long getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}

	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}

	public long getReadingValue() {
		return readingValue;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}

	public String getReadingComment() {
		return readingComment;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}

	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}

	public void setTransferToCentre(String transferToCentre) {
		this.transferToCentre = transferToCentre;
	}

	public String getTransferToCentre() {
		return transferToCentre;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
		this.transferToBuisSeg = transferToBuisSeg;
	}

	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}

	public void setConditionName1(String conditionName1) {
		this.conditionName1 = conditionName1;
	}

	public String getConditionName1() {
		return conditionName1;
	}

	public void set_SysConditionName(String conditionName) {
		this._SysconditionName = conditionName;
	}

	public String get_SysConditionName() {
		return _SysconditionName;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public void setLclCentreType(CentreTypeEnum centreType) {
		this.lclCentreType = centreType;
	}

	public CentreTypeEnum getLclCentreType() {
		return lclCentreType;
	}

	public void setLclEquipmentBarcode(String equipmentBarcode) {
		this.lclEquipmentBarcode = equipmentBarcode;
	}

	public String getLclEquipmentBarcode() {
		return lclEquipmentBarcode;
	}

	public void setLclEquipmentComment(String equipmentComment) {
		this.lclEquipmentComment = equipmentComment;
	}

	public String getLclEquipmentComment() {
		return lclEquipmentComment;
	}

	public void setLclReadingType(ReadingTypeEnum readingType) {
		this.lclReadingType = readingType;
	}

	public ReadingTypeEnum getLclReadingType() {
		return lclReadingType;
	}

	public void setLclEquipmentReadingSgt(long equipmentReadingSgt) {
		this.lclEquipmentReadingSgt = equipmentReadingSgt;
	}

	public long getLclEquipmentReadingSgt() {
		return lclEquipmentReadingSgt;
	}

	public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
		this.lclReadingRequiredDate = readingRequiredDate;
	}

	public LocalDate getLclReadingRequiredDate() {
		return lclReadingRequiredDate;
	}

	public void setLclTransferFunction(YesOrNoEnum transferFunction) {
		this.lclTransferFunction = transferFunction;
	}

	public YesOrNoEnum getLclTransferFunction() {
		return lclTransferFunction;
	}

	public void setLclTransferAwhRegion(String transferAwhRegion) {
		this.lclTransferAwhRegion = transferAwhRegion;
	}

	public String getLclTransferAwhRegion() {
		return lclTransferAwhRegion;
	}

	public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.lclEquipmentStatus = equipmentStatus;
	}

	public EquipmentStatusEnum getLclEquipmentStatus() {
		return lclEquipmentStatus;
	}

	public void setLclEquipmentSerialNumber(String equipmentSerialNumber) {
		this.lclEquipmentSerialNumber = equipmentSerialNumber;
	}

	public String getLclEquipmentSerialNumber() {
		return lclEquipmentSerialNumber;
	}

	public void setLclLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.lclLeaseComencmentDate = leaseComencmentDate;
	}

	public LocalDate getLclLeaseComencmentDate() {
		return lclLeaseComencmentDate;
	}

	public void setLclLeaseExpireDate(LocalDate leaseExpireDate) {
		this.lclLeaseExpireDate = leaseExpireDate;
	}

	public LocalDate getLclLeaseExpireDate() {
		return lclLeaseExpireDate;
	}

	public void setLclLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.lclLeaseMonthlyRateS = leaseMonthlyRateS;
	}

	public BigDecimal getLclLeaseMonthlyRateS() {
		return lclLeaseMonthlyRateS;
	}

	public void setLclLeaseMaxTotalUnits(long leaseMaxTotalUnits) {
		this.lclLeaseMaxTotalUnits = leaseMaxTotalUnits;
	}

	public long getLclLeaseMaxTotalUnits() {
		return lclLeaseMaxTotalUnits;
	}

	public void setLclReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.lclReadingFrequency = readingFrequency;
	}

	public ReadingFrequencyEnum getLclReadingFrequency() {
		return lclReadingFrequency;
	}

	public void setLclVisualEquipmentCode(String visualEquipmentCode) {
		this.lclVisualEquipmentCode = visualEquipmentCode;
	}

	public String getLclVisualEquipmentCode() {
		return lclVisualEquipmentCode;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}
	public void setDeferConfirm(DeferConfirmEnum deferConfirm) {
		this.deferConfirm = deferConfirm;
	}

	public DeferConfirmEnum getDeferConfirm() {
		return deferConfirm;
	}
	public void setCursorField(String cursorField) {
		this.cursorField = cursorField;
	}

	public String getCursorField() {
		return cursorField;
	}
    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setDtoFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(equipmentReadings, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setEntityFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(this, equipmentReadings);
    }
}
