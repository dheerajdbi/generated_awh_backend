package au.awh.file.equipmentreadings.addmequipmentreadings;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 18
import au.awh.model.CentreTypeEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Add:M Equipment Readings' (WSPLTWVPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmEquipmentReadingsState extends AddmEquipmentReadingsDTO {
    private static final long serialVersionUID = -2791521545604413995L;

    // Local fields
    private CentreTypeEnum lclCentreType = null;
    private String lclEquipmentBarcode = "";
    private String lclEquipmentComment = "";
    private long lclEquipmentReadingSgt = 0L;
    private String lclEquipmentSerialNumber = "";
    private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum._NOT_ENTERED;
    private LocalDate lclLeaseComencmentDate = null;
    private LocalDate lclLeaseExpireDate = null;
    private long lclLeaseMaxTotalUnits = 0L;
    private BigDecimal lclLeaseMonthlyRateS = BigDecimal.ZERO;
    private ReadingFrequencyEnum lclReadingFrequency = ReadingFrequencyEnum._DAILY;
    private LocalDate lclReadingRequiredDate = null;
    private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED;
    private String lclTransferAwhRegion = "";
    private YesOrNoEnum lclTransferFunction = YesOrNoEnum._NOT_ENTERED;
    private String lclVisualEquipmentCode = "";

    // System fields
    private DeferConfirmEnum _sysDeferConfirm = null;
    private String _sysCursorField = "";
    private boolean _sysErrorFound = false;

    public AddmEquipmentReadingsState() {
    }

    public AddmEquipmentReadingsState(EquipmentReadings equipmentReadings) {
        setDtoFields(equipmentReadings);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclCentreType(CentreTypeEnum centreType) {
        this.lclCentreType = centreType;
    }

    public CentreTypeEnum getLclCentreType() {
        return lclCentreType;
    }
    public void setLclEquipmentBarcode(String equipmentBarcode) {
        this.lclEquipmentBarcode = equipmentBarcode;
    }

    public String getLclEquipmentBarcode() {
        return lclEquipmentBarcode;
    }
    public void setLclEquipmentComment(String equipmentComment) {
        this.lclEquipmentComment = equipmentComment;
    }

    public String getLclEquipmentComment() {
        return lclEquipmentComment;
    }
    public void setLclEquipmentReadingSgt(long equipmentReadingSgt) {
        this.lclEquipmentReadingSgt = equipmentReadingSgt;
    }

    public long getLclEquipmentReadingSgt() {
        return lclEquipmentReadingSgt;
    }
    public void setLclEquipmentSerialNumber(String equipmentSerialNumber) {
        this.lclEquipmentSerialNumber = equipmentSerialNumber;
    }

    public String getLclEquipmentSerialNumber() {
        return lclEquipmentSerialNumber;
    }
    public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
        this.lclEquipmentStatus = equipmentStatus;
    }

    public EquipmentStatusEnum getLclEquipmentStatus() {
        return lclEquipmentStatus;
    }
    public void setLclLeaseComencmentDate(LocalDate leaseComencmentDate) {
        this.lclLeaseComencmentDate = leaseComencmentDate;
    }

    public LocalDate getLclLeaseComencmentDate() {
        return lclLeaseComencmentDate;
    }
    public void setLclLeaseExpireDate(LocalDate leaseExpireDate) {
        this.lclLeaseExpireDate = leaseExpireDate;
    }

    public LocalDate getLclLeaseExpireDate() {
        return lclLeaseExpireDate;
    }
    public void setLclLeaseMaxTotalUnits(long leaseMaxTotalUnits) {
        this.lclLeaseMaxTotalUnits = leaseMaxTotalUnits;
    }

    public long getLclLeaseMaxTotalUnits() {
        return lclLeaseMaxTotalUnits;
    }
    public void setLclLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
        this.lclLeaseMonthlyRateS = leaseMonthlyRateS;
    }

    public BigDecimal getLclLeaseMonthlyRateS() {
        return lclLeaseMonthlyRateS;
    }
    public void setLclReadingFrequency(ReadingFrequencyEnum readingFrequency) {
        this.lclReadingFrequency = readingFrequency;
    }

    public ReadingFrequencyEnum getLclReadingFrequency() {
        return lclReadingFrequency;
    }
    public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
        this.lclReadingRequiredDate = readingRequiredDate;
    }

    public LocalDate getLclReadingRequiredDate() {
        return lclReadingRequiredDate;
    }
    public void setLclReadingType(ReadingTypeEnum readingType) {
        this.lclReadingType = readingType;
    }

    public ReadingTypeEnum getLclReadingType() {
        return lclReadingType;
    }
    public void setLclTransferAwhRegion(String transferAwhRegion) {
        this.lclTransferAwhRegion = transferAwhRegion;
    }

    public String getLclTransferAwhRegion() {
        return lclTransferAwhRegion;
    }
    public void setLclTransferFunction(YesOrNoEnum transferFunction) {
        this.lclTransferFunction = transferFunction;
    }

    public YesOrNoEnum getLclTransferFunction() {
        return lclTransferFunction;
    }
    public void setLclVisualEquipmentCode(String visualEquipmentCode) {
        this.lclVisualEquipmentCode = visualEquipmentCode;
    }

    public String getLclVisualEquipmentCode() {
        return lclVisualEquipmentCode;
    }

    public void set_SysCursorField(String cursorField) {
        _sysCursorField = cursorField;
    }

    public String get_SysCursorField() {
        return _sysCursorField;
    }

    public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
        _sysDeferConfirm = deferConfirm;
    }

    public DeferConfirmEnum get_SysDeferConfirm() {
        return _sysDeferConfirm;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
