package au.awh.file.equipmentreadings.addmequipmentreadings;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.equipmentreadings.EquipmentReadingsRepository;


import au.awh.utils.BuildInFunctionUtils;
import com.freschelegacy.utils.DateTimeUtils;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionService;
import au.awh.file.centre.getecentre.GeteCentreService;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreService;
import au.awh.file.equipmentreadings.chgetransfersgt.ChgeTransferSgtService;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsService;
import au.awh.file.equipmentreadings.transfesubsqentreadings.TransfeSubsqentReadingsService;
import au.awh.file.plantequipment.chgcrteplantequipment.ChgcrtePlantEquipmentService;
import au.awh.file.plantequipment.chgeequipmentstatus.ChgeEquipmentStatusService;
import au.awh.file.plantequipment.chgefortransfer.ChgeForTransferService;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentService;
// asServiceDtoImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionDTO;
import au.awh.file.centre.getecentre.GeteCentreDTO;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreDTO;
import au.awh.file.equipmentreadings.chgetransfersgt.ChgeTransferSgtDTO;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.transfesubsqentreadings.TransfeSubsqentReadingsDTO;
import au.awh.file.plantequipment.chgcrteplantequipment.ChgcrtePlantEquipmentDTO;
import au.awh.file.plantequipment.chgeequipmentstatus.ChgeEquipmentStatusDTO;
import au.awh.file.plantequipment.chgefortransfer.ChgeForTransferDTO;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionParams;
import au.awh.file.centre.getecentre.GeteCentreParams;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreParams;
import au.awh.file.equipmentreadings.chgetransfersgt.ChgeTransferSgtParams;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsParams;
import au.awh.file.equipmentreadings.transfesubsqentreadings.TransfeSubsqentReadingsParams;
import au.awh.file.plantequipment.chgcrteplantequipment.ChgcrtePlantEquipmentParams;
import au.awh.file.plantequipment.chgeequipmentstatus.ChgeEquipmentStatusParams;
import au.awh.file.plantequipment.chgefortransfer.ChgeForTransferParams;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionResult;
import au.awh.file.centre.getecentre.GeteCentreResult;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreResult;
import au.awh.file.equipmentreadings.chgetransfersgt.ChgeTransferSgtResult;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsResult;
import au.awh.file.equipmentreadings.transfesubsqentreadings.TransfeSubsqentReadingsResult;
import au.awh.file.plantequipment.chgcrteplantequipment.ChgcrtePlantEquipmentResult;
import au.awh.file.plantequipment.chgeequipmentstatus.ChgeEquipmentStatusResult;
import au.awh.file.plantequipment.chgefortransfer.ChgeForTransferResult;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreService;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreDTO;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreParams;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreResult;


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 34
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CentreTypeEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Add:M Equipment Readings' (WSPLTWVPVK) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service
public class AddmEquipmentReadingsService extends AbstractService<AddmEquipmentReadingsService, AddmEquipmentReadingsState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

    
    public static final String SCREEN_PROMPT = "addmEquipmentReadings";
    public static final String SCREEN_CONFIRM_PROMPT = "AddmEquipmentReadings.key2";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",AddmEquipmentReadingsParams.class, this::executeService);
    private final Step response = define("response",AddmEquipmentReadingsDTO.class, this::processResponse);
//  private final Step displayConfirmPrompt = define("displayConfirmPrompt",AddmEquipmentReadingsDTO.class, this::processDisplayConfirmPrompt);

	private final Step serviceWsut5502SelectCentre = define("serviceWsut5502SelectCentre",Wsut5502SelectCentreResult.class, this::processServiceWsut5502SelectCentre);
	//private final Step serviceGetePlantEquipment = define("serviceGetePlantEquipment",GetePlantEquipmentResult.class, this::processServiceGetePlantEquipment);
	//private final Step serviceGeteCentre = define("serviceGeteCentre",GeteCentreResult.class, this::processServiceGeteCentre);
	//private final Step serviceCrteEquipmentReadings = define("serviceCrteEquipmentReadings",CrteEquipmentReadingsResult.class, this::processServiceCrteEquipmentReadings);
	//private final Step serviceGeteCentresRegion = define("serviceGeteCentresRegion",GeteCentresRegionResult.class, this::processServiceGeteCentresRegion);
	//private final Step serviceChgeTransferSgt = define("serviceChgeTransferSgt",ChgeTransferSgtResult.class, this::processServiceChgeTransferSgt);
	//private final Step serviceChgeEquipmentStatus = define("serviceChgeEquipmentStatus",ChgeEquipmentStatusResult.class, this::processServiceChgeEquipmentStatus);
	//private final Step serviceChgcrtePlantEquipment = define("serviceChgcrtePlantEquipment",ChgcrtePlantEquipmentResult.class, this::processServiceChgcrtePlantEquipment);
	//private final Step serviceChgeForTransfer = define("serviceChgeForTransfer",ChgeForTransferResult.class, this::processServiceChgeForTransfer);
	//private final Step serviceTransfeSubsqentReadings = define("serviceTransfeSubsqentReadings",TransfeSubsqentReadingsResult.class, this::processServiceTransfeSubsqentReadings);
	

	
    	
@Autowired
private ChgcrtePlantEquipmentService chgcrtePlantEquipmentService;
	
@Autowired
private ChgeEquipmentStatusService chgeEquipmentStatusService;
	
@Autowired
private ChgeForTransferService chgeForTransferService;
	
@Autowired
private ChgeTransferSgtService chgeTransferSgtService;
	
@Autowired
private CrteEquipmentReadingsService crteEquipmentReadingsService;
	
@Autowired
private GeteCentreService geteCentreService;
	
@Autowired
private GeteCentresRegionService geteCentresRegionService;
	
@Autowired
private GetePlantEquipmentService getePlantEquipmentService;
	
@Autowired
private TransfeSubsqentReadingsService transfeSubsqentReadingsService;
	
    @Autowired
    public AddmEquipmentReadingsService()
    {
        super(AddmEquipmentReadingsService.class, AddmEquipmentReadingsState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * AddmEquipmentReadings service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(AddmEquipmentReadingsState state, AddmEquipmentReadingsParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * AddmEquipmentReadings service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(AddmEquipmentReadingsState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(AddmEquipmentReadingsState state)
    {
        StepResult stepResult = NO_ACTION;

        AddmEquipmentReadingsDTO dto = new AddmEquipmentReadingsDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(AddmEquipmentReadingsState state, AddmEquipmentReadingsDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
//    private StepResult processDisplayConfirmPrompt(AddmEquipmentReadingsState state, AddmEquipmentReadingsDTO dto) {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        stepResult = conductScreenConversation(state);
//
//        return stepResult;
//    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void validateScreenInput(AddmEquipmentReadingsState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(AddmEquipmentReadingsState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(AddmEquipmentReadingsState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(AddmEquipmentReadingsState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        AddmEquipmentReadingsResult params = new AddmEquipmentReadingsResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// * Need to remove processing for select existing record.
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            GetePlantEquipmentResult getePlantEquipmentResult = null;
			GetePlantEquipmentParams getePlantEquipmentParams = null;
			// DEBUG genFunctionCall BEGIN 1000368 ACT GetE Plant Equipment - Plant Equipment  *
			getePlantEquipmentParams = new GetePlantEquipmentParams();
			getePlantEquipmentResult = new GetePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getePlantEquipmentParams);
			getePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
			getePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			getePlantEquipmentParams.setErrorProcessing("2");
			getePlantEquipmentParams.setGetRecordOption("");
			stepResult = getePlantEquipmentService.execute(getePlantEquipmentParams);
			getePlantEquipmentResult = (GetePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getePlantEquipmentResult.get_SysReturnCode());
			dto.setAwhBuisnessSegment(getePlantEquipmentResult.getAwhBuisnessSegment());
			dto.setCentreCodeKey(getePlantEquipmentResult.getCentreCodeKey());
			dto.setEquipmentDescription(getePlantEquipmentResult.getEquipmentDescription());
			dto.setLclEquipmentBarcode(getePlantEquipmentResult.getEquipmentBarcode());
			dto.setLclEquipmentComment(getePlantEquipmentResult.getEquipmentComment());
			dto.setEquipmentTypeCode(getePlantEquipmentResult.getEquipmentTypeCode());
			dto.setEquipReadingUnit(getePlantEquipmentResult.getEquipReadingUnit());
			dto.setEquipmentLeasor(getePlantEquipmentResult.getEquipmentLeasor());
			dto.setLclEquipmentStatus(getePlantEquipmentResult.getEquipmentStatus());
			dto.setLclEquipmentSerialNumber(getePlantEquipmentResult.getEquipmentSerialNumber());
			dto.setLclLeaseComencmentDate(getePlantEquipmentResult.getLeaseComencmentDate());
			dto.setLclLeaseExpireDate(getePlantEquipmentResult.getLeaseExpireDate());
			dto.setLclLeaseMonthlyRateS(getePlantEquipmentResult.getLeaseMonthlyRateS());
			dto.setLclLeaseMaxTotalUnits(getePlantEquipmentResult.getLeaseMaxTotalUnits());
			dto.setLclReadingFrequency(getePlantEquipmentResult.getReadingFrequency());
			dto.setLclVisualEquipmentCode(getePlantEquipmentResult.getVisualEquipmentCode());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000776 ACT DTL.Condition Name = Condition name of DTL.AWH Buisness Segment
			dto.setConditionName(dto.getAwhBuisnessSegment().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000382 ACT DTL.Condition Name 2 = Condition name of LCL.Equipment Status
			dto.setConditionName2(dto.getLclEquipmentStatus().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			if (dto.getReadingRequiredDate().equals(LocalDate.parse("0001-01-01", DateTimeFormatter.BASIC_ISO_DATE))) {
				// DTL.Reading Required Date is Not Entered
				// DEBUG genFunctionCall BEGIN 1000523 ACT DTL.Reading Required Date = JOB.*Job date
				dto.setReadingRequiredDate(LocalDate.now());
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000356 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000356
			// DEBUG X2EActionDiagramElement 1000356 ACT     AWH region Name Drv      *FIELD                                             DTLA
			// DEBUG X2EActionDiagramElement 1000357 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000358 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000360 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
			// TODO: NEED TO SUPPORT *FIELD Equipment Type Desc Drv  *FIELD                                             DTLE 1000360
			// DEBUG X2EActionDiagramElement 1000360 ACT     Equipment Type Desc Drv  *FIELD                                             DTLE
			// DEBUG X2EActionDiagramElement 1000361 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000362 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000364 ACT Centre Name Drv          *FIELD                                             DTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000364
			// DEBUG X2EActionDiagramElement 1000364 ACT     Centre Name Drv          *FIELD                                             DTLC
			// DEBUG X2EActionDiagramElement 1000365 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000366 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000631 ACT LCL.Transfer Function = CND.No
			dto.setLclTransferFunction(YesOrNoEnum._NO);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000330 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            Wsut5502SelectCentreResult wsut5502SelectCentreResult = null;
			GeteCentreParams geteCentreParams = null;
			GeteCentreResult geteCentreResult = null;
			Wsut5502SelectCentreParams wsut5502SelectCentreParams = null;
			if (ReadingTypeEnum.isAllValues(dto.getReadingType())) {
				// DTL.Reading Type is *ALL values
				// DEBUG genFunctionCall BEGIN 1000767 ACT DTL.*Condition name = Condition name of DTL.Reading Type
				dto.set_SysConditionName(dto.getReadingType().getDescription());
				// Retrieve condition
				// DEBUG genFunctionCall END
			}
			if (dto.getReadingType() == ReadingTypeEnum._NOT_ENTERED) {
				// DTL.Reading Type is Not entered
				// DEBUG genFunctionCall BEGIN 1000661 ACT *Set Cursor: DTL.Reading Type  (*Override=*NO)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
			} else if (dto.getReadingValue() == 0) {
				// DTL.Reading Value is Not entered
				// DEBUG genFunctionCall BEGIN 1000653 ACT *Set Cursor: DTL.Reading Value  (*Override=*NO)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
			}
			if ((dto.getReadingType() == ReadingTypeEnum._NOT_ENTERED) || (
			// DEBUG ComparatorJavaGenerator BEGIN
			dto.getReadingType() != dto.getLclReadingType()
			// DEBUG ComparatorJavaGenerator END
			)) {
				// DEBUG genFunctionCall BEGIN 1000400 ACT PGM.*Defer confirm = CND.Defer confirm
				dto.set_SysDeferConfirm(DeferConfirmEnum._DEFER_CONFIRM);
				// DEBUG genFunctionCall END
				if (dto.getReadingType() == ReadingTypeEnum._TRANSFER_OUT) {
					// DTL.Reading Type is Transfer Out
					// DEBUG genFunctionCall BEGIN 1000419 ACT DTL.Transfer to Centre = DTL.Centre Code           KEY
					dto.setTransferToCentre(dto.getCentreCodeKey());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000413 ACT DTL.Transfer to Buis Seg = DTL.AWH Buisness Segment
					dto.setTransferToBuisSeg(dto.getAwhBuisnessSegment());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000617 ACT LCL.Transfer Function = CND.Yes
					dto.setLclTransferFunction(YesOrNoEnum._YES);
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000625 ACT LCL.Transfer Function = CND.No
					dto.setLclTransferFunction(YesOrNoEnum._NO);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000406 ACT LCL.Reading Type = DTL.Reading Type
				dto.setLclReadingType(dto.getReadingType());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000782 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
			if ((dto.getReadingValue() == 0) && (
			// DEBUG ComparatorJavaGenerator BEGIN
			dto.getReadingType() == dto.getLclReadingType()
			// DEBUG ComparatorJavaGenerator END
			)) {
				// DEBUG genFunctionCall BEGIN 1000638 ACT Send error message - 'Value Required Num'
				//dto.addMessage("readingValue", "value.required.num", messageSource);
				// DEBUG genFunctionCall END
			}
			if (dto.getReadingType() == ReadingTypeEnum._TRANSFER_OUT) {
				// DTL.Reading Type is Transfer Out
				if (isPrompt(dto.get_SysCmdKey())) {
					// DTL.*CMD key is *Prompt
					if (true) {
						// PGM.*Cursor field <IS> DTL.Transfer to Centre
						// DEBUG genFunctionCall BEGIN 1000456 ACT WSUT5502 Select Centre - Centre  *
						wsut5502SelectCentreParams = new Wsut5502SelectCentreParams();
						wsut5502SelectCentreResult = new Wsut5502SelectCentreResult();
						// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
						BeanUtils.copyProperties(dto, wsut5502SelectCentreParams);
						wsut5502SelectCentreParams.setCentreCodeKey(dto.getTransferToCentre());
						// TODO SPLIT SCREEN PMTRCD EquipmentReadings.addmEquipmentReadings (1065) -> SELRCD Centre.wsut5502SelectCentre
						stepResult = StepResult.callService(Wsut5502SelectCentreService.class, wsut5502SelectCentreParams).thenCall(serviceWsut5502SelectCentre);
						dto.setTransferToCentre(wsut5502SelectCentreResult.getCentreCodeKey());
						// DEBUG genFunctionCall END
					}
				}
				// DEBUG genFunctionCall BEGIN 1000464 ACT GetE Centre - Centre  *
				geteCentreParams = new GeteCentreParams();
				geteCentreResult = new GeteCentreResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteCentreParams);
				geteCentreParams.setCentreCodeKey(dto.getTransferToCentre());
				geteCentreParams.setErrorProcessing("2");
				geteCentreParams.setGetRecordOption("");
				stepResult = geteCentreService.execute(geteCentreParams);
				geteCentreResult = (GeteCentreResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteCentreResult.get_SysReturnCode());
				dto.setCentreName(geteCentreResult.getCentreName());
				dto.setLclCentreType(geteCentreResult.getCentreType());
				// DEBUG genFunctionCall END
				if (AwhBuisnessSegmentEnum.isAllValues(dto.getTransferToBuisSeg())) {
					// DTL.Transfer to Buis Seg is *ALL values
					// DEBUG genFunctionCall BEGIN 1000843 ACT DTL.Condition Name 1 = Condition name of DTL.Transfer to Buis Seg
					dto.setConditionName1(dto.getTransferToBuisSeg().getDescription());
					// Retrieve condition
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000481 ACT Send error message - 'AWH Buis seg Invalid'
					//dto.addMessage("awhBuisnessSegment", "awh.buis.seg.invalid", messageSource);
					// DEBUG genFunctionCall END
				}
				if ((
				// DEBUG ComparatorJavaGenerator BEGIN
				dto.getTransferToCentre().equals(dto.getCentreCodeKey())
				// DEBUG ComparatorJavaGenerator END
				) && (
				// DEBUG ComparatorJavaGenerator BEGIN
				dto.getTransferToBuisSeg() == dto.getAwhBuisnessSegment()
				// DEBUG ComparatorJavaGenerator END
				)) {
					// DEBUG genFunctionCall BEGIN 1000443 ACT Send error message - 'Either Cntr or BSeg Chg'
					//dto.addMessage("centreCodeKey", "either.cntr.or.bseg.chg", messageSource);
					// DEBUG genFunctionCall END
				}
			}
			if (dto.get_SysDeferConfirm() == DeferConfirmEnum._PROCEED_TO_CONFIRM) {
				// PGM.*Defer confirm is Proceed to confirm
				if ((dto.getReadingType() == ReadingTypeEnum._MAKE_UNAVAILABLE) && !(dto.getLclEquipmentStatus() == EquipmentStatusEnum._ACTIVE)) {
					// DEBUG genFunctionCall BEGIN 1000812 ACT Send error message - 'Move Unavailable Invalid'
					//dto.addMessage("conditionName", "move.unavailable.invalid", messageSource);
					// DEBUG genFunctionCall END
				} else if ((dto.getReadingType() == ReadingTypeEnum._MAKE_AVAILABLE) && !(dto.getLclEquipmentStatus() == EquipmentStatusEnum._UNAVAILABLE)) {
					// DEBUG genFunctionCall BEGIN 1000800 ACT Send error message - 'Move Unavailable Invalid'
					//dto.addMessage("conditionName", "move.unavailable.invalid", messageSource);
					// DEBUG genFunctionCall END
				}
				if ((dto.getReadingType() == ReadingTypeEnum._MAKE_UNAVAILABLE) && (dto.getReadingComment() != null && dto.getReadingComment().isEmpty())) {
					// DEBUG genFunctionCall BEGIN 1000790 ACT Send error message - 'Value Required'
					//dto.addMessage("readingComment", "value.required", messageSource);
					// DEBUG genFunctionCall END
				}
			}
			// DEBUG genFunctionCall BEGIN 1000833 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            ChgeTransferSgtResult chgeTransferSgtResult = null;
			CrteEquipmentReadingsParams crteEquipmentReadingsParams = null;
			ChgeEquipmentStatusParams chgeEquipmentStatusParams = null;
			ChgeForTransferParams chgeForTransferParams = null;
			TransfeSubsqentReadingsResult transfeSubsqentReadingsResult = null;
			ChgeTransferSgtParams chgeTransferSgtParams = null;
			ChgeEquipmentStatusResult chgeEquipmentStatusResult = null;
			ChgeForTransferResult chgeForTransferResult = null;
			GeteCentresRegionParams geteCentresRegionParams = null;
			ChgcrtePlantEquipmentResult chgcrtePlantEquipmentResult = null;
			ChgcrtePlantEquipmentParams chgcrtePlantEquipmentParams = null;
			CrteEquipmentReadingsResult crteEquipmentReadingsResult = null;
			GeteCentresRegionResult geteCentresRegionResult = null;
			TransfeSubsqentReadingsParams transfeSubsqentReadingsParams = null;
			// DEBUG genFunctionCall BEGIN 1000565 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000837 ACT LCL.Equipment Reading Sgt = CND.Not Entered
			dto.setLclEquipmentReadingSgt(0);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000484 ACT CrtE Equipment Readings - Equipment Readings  *
			crteEquipmentReadingsParams = new CrteEquipmentReadingsParams();
			crteEquipmentReadingsResult = new CrteEquipmentReadingsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, crteEquipmentReadingsParams);
			//crteEquipmentReadingsParams.setEquipmentReadingSgt(dto.getLclEquipmentReadingSgt());
			crteEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
			crteEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			crteEquipmentReadingsParams.setReadingType(dto.getReadingType());
			//crteEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
			crteEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
			crteEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
			crteEquipmentReadingsParams.setTransferToFromSgt(BigDecimal.ZERO);
			crteEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
			crteEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			crteEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			crteEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			crteEquipmentReadingsParams.setErrorProcessing("2");
			stepResult = crteEquipmentReadingsService.execute(crteEquipmentReadingsParams);
			crteEquipmentReadingsResult = (CrteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(crteEquipmentReadingsResult.get_SysReturnCode());
			//dto.setLclEquipmentReadingSgt(crteEquipmentReadingsResult.getEquipmentReadingSgt());
			// DEBUG genFunctionCall END
			if (dto.getReadingType() == ReadingTypeEnum._TRANSFER_OUT) {
				// DTL.Reading Type is Transfer Out
				// DEBUG genFunctionCall BEGIN 1000517 ACT GetE Centres Region - AWH Region/Centre  *
				geteCentresRegionParams = new GeteCentresRegionParams();
				geteCentresRegionResult = new GeteCentresRegionResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteCentresRegionParams);
				geteCentresRegionParams.setCentreCodeKey(dto.getTransferToCentre());
				geteCentresRegionParams.setErrorProcessing("2");
				geteCentresRegionParams.setGetRecordOption("");
				stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
				geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteCentresRegionResult.get_SysReturnCode());
				dto.setLclTransferAwhRegion(geteCentresRegionResult.getAwhRegionCode());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000502 ACT CrtE Equipment Readings - Equipment Readings  *
				crteEquipmentReadingsParams = new CrteEquipmentReadingsParams();
				crteEquipmentReadingsResult = new CrteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, crteEquipmentReadingsParams);
				//crteEquipmentReadingsParams.setEquipmentReadingSgt(dto.getTransferToFromSgt());
				crteEquipmentReadingsParams.setAwhRegionCode(dto.getLclTransferAwhRegion());
				crteEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				crteEquipmentReadingsParams.setReadingType("TI");
				//crteEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
				crteEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
				crteEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
				//crteEquipmentReadingsParams.setTransferToFromSgt(dto.getEquipmentReadingSgt());
				crteEquipmentReadingsParams.setCentreCodeKey(dto.getTransferToCentre());
				crteEquipmentReadingsParams.setAwhBuisnessSegment(dto.getTransferToBuisSeg());
				crteEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
				crteEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				crteEquipmentReadingsParams.setErrorProcessing("2");
				stepResult = crteEquipmentReadingsService.execute(crteEquipmentReadingsParams);
				crteEquipmentReadingsResult = (CrteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(crteEquipmentReadingsResult.get_SysReturnCode());
				//dto.setTransferToFromSgt(crteEquipmentReadingsResult.getEquipmentReadingSgt());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000529 ACT ChgE Transfer Sgt - Equipment Readings  *
				chgeTransferSgtParams = new ChgeTransferSgtParams();
				chgeTransferSgtResult = new ChgeTransferSgtResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, chgeTransferSgtParams);
				chgeTransferSgtParams.setEquipmentReadingSgt(dto.getLclEquipmentReadingSgt());
				chgeTransferSgtParams.setTransferToFromSgt(dto.getTransferToFromSgt());
				chgeTransferSgtParams.setErrorProcessing("2");
				stepResult = chgeTransferSgtService.execute(chgeTransferSgtParams);
				chgeTransferSgtResult = (ChgeTransferSgtResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgeTransferSgtResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				!dto.getAwhRegionCode().equals(dto.getLclTransferAwhRegion())
				// DEBUG ComparatorJavaGenerator END
				) {
					// DTL.AWH Region Code NE LCL.Transfer AWH region
					// DEBUG genFunctionCall BEGIN 1000577 ACT ChgE Equipment Status - Plant Equipment  *
					chgeEquipmentStatusParams = new ChgeEquipmentStatusParams();
					chgeEquipmentStatusResult = new ChgeEquipmentStatusResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, chgeEquipmentStatusParams);
					chgeEquipmentStatusParams.setAwhRegionCode(dto.getAwhRegionCode());
					chgeEquipmentStatusParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
					chgeEquipmentStatusParams.setEquipmentStatus("TR");
					chgeEquipmentStatusParams.setErrorProcessing("2");
					stepResult = chgeEquipmentStatusService.execute(chgeEquipmentStatusParams);
					chgeEquipmentStatusResult = (ChgeEquipmentStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(chgeEquipmentStatusResult.get_SysReturnCode());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000583 ACT ChgCrtE Plant Equipment - Plant Equipment  *
					chgcrtePlantEquipmentParams = new ChgcrtePlantEquipmentParams();
					chgcrtePlantEquipmentResult = new ChgcrtePlantEquipmentResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, chgcrtePlantEquipmentParams);
					chgcrtePlantEquipmentParams.setErrorProcessing("2");
					chgcrtePlantEquipmentParams.setAwhRegionCode(dto.getLclTransferAwhRegion());
					chgcrtePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
					chgcrtePlantEquipmentParams.setAwhBuisnessSegment(dto.getTransferToBuisSeg());
					chgcrtePlantEquipmentParams.setCentreCodeKey(dto.getTransferToCentre());
					chgcrtePlantEquipmentParams.setEquipmentDescription(dto.getEquipmentDescription());
					chgcrtePlantEquipmentParams.setEquipmentBarcode(dto.getLclEquipmentBarcode());
					chgcrtePlantEquipmentParams.setEquipmentComment(dto.getLclEquipmentComment());
					chgcrtePlantEquipmentParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
					chgcrtePlantEquipmentParams.setEquipReadingUnit(dto.getEquipReadingUnit());
					chgcrtePlantEquipmentParams.setEquipmentLeasor(dto.getEquipmentLeasor());
					chgcrtePlantEquipmentParams.setEquipmentStatus("AT");
					chgcrtePlantEquipmentParams.setEquipmentSerialNumber(dto.getLclEquipmentSerialNumber());
					chgcrtePlantEquipmentParams.setLeaseComencmentDate("0001-01-01");
					chgcrtePlantEquipmentParams.setLeaseExpireDate("0001-01-01");
					chgcrtePlantEquipmentParams.setLeaseMonthlyRateS("0");
					chgcrtePlantEquipmentParams.setLeaseMaxTotalUnits("0");
					chgcrtePlantEquipmentParams.setReadingFrequency("");
					chgcrtePlantEquipmentParams.setVisualEquipmentCode("");
					stepResult = chgcrtePlantEquipmentService.execute(chgcrtePlantEquipmentParams);
					chgcrtePlantEquipmentResult = (ChgcrtePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(chgcrtePlantEquipmentResult.get_SysReturnCode());
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					if (dto.getReadingType() == ReadingTypeEnum._RETIREMENT) {
						// DTL.Reading Type is Retirement
						// DEBUG genFunctionCall BEGIN 1000672 ACT LCL.Equipment Status = CND.Retired
						dto.setLclEquipmentStatus(EquipmentStatusEnum._RETIRED);
						// DEBUG genFunctionCall END
					} else {
						// *OTHERWISE
						// DEBUG genFunctionCall BEGIN 1000680 ACT LCL.Equipment Status = CND.Active
						dto.setLclEquipmentStatus(EquipmentStatusEnum._ACTIVE);
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1000598 ACT ChgE For Transfer - Plant Equipment  *
					chgeForTransferParams = new ChgeForTransferParams();
					chgeForTransferResult = new ChgeForTransferResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, chgeForTransferParams);
					chgeForTransferParams.setAwhRegionCode(dto.getAwhRegionCode());
					chgeForTransferParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
					chgeForTransferParams.setEquipmentStatus(dto.getLclEquipmentStatus());
					chgeForTransferParams.setErrorProcessing("2");
					chgeForTransferParams.setAwhBuisnessSegment(dto.getTransferToBuisSeg());
					chgeForTransferParams.setCentreCodeKey(dto.getTransferToCentre());
					stepResult = chgeForTransferService.execute(chgeForTransferParams);
					chgeForTransferResult = (ChgeForTransferResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(chgeForTransferResult.get_SysReturnCode());
					// DEBUG genFunctionCall END
				}
				// * Transfer any subsequent readings to new region and centre
				// DEBUG genFunctionCall BEGIN 1000728 ACT LCL.Reading Required Date = DTL.Reading Required Date + CON.1 *DAYS
				dto.setLclReadingRequiredDate(BuildInFunctionUtils.getDateIncrement(
				dto.getReadingRequiredDate(), 1, "DY", "1111111", null, Boolean.FALSE, Boolean.FALSE));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000720 ACT TransfE Subsqent Readings - Equipment Readings  *
				transfeSubsqentReadingsParams = new TransfeSubsqentReadingsParams();
				transfeSubsqentReadingsResult = new TransfeSubsqentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, transfeSubsqentReadingsParams);
				transfeSubsqentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
				transfeSubsqentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				transfeSubsqentReadingsParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
				transfeSubsqentReadingsParams.setTransferToBuisSeg(dto.getTransferToBuisSeg());
				transfeSubsqentReadingsParams.setTransferToCentre(dto.getTransferToCentre());
				transfeSubsqentReadingsParams.setErrorProcessing("2");
				stepResult = transfeSubsqentReadingsService.execute(transfeSubsqentReadingsParams);
				transfeSubsqentReadingsResult = (TransfeSubsqentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(transfeSubsqentReadingsResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
			}
			if (dto.getReadingType() == ReadingTypeEnum._RETIREMENT) {
				// DTL.Reading Type is Retirement
				// DEBUG genFunctionCall BEGIN 1000851 ACT ChgE Equipment Status - Plant Equipment  *
				chgeEquipmentStatusParams = new ChgeEquipmentStatusParams();
				chgeEquipmentStatusResult = new ChgeEquipmentStatusResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, chgeEquipmentStatusParams);
				chgeEquipmentStatusParams.setAwhRegionCode(dto.getAwhRegionCode());
				chgeEquipmentStatusParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				chgeEquipmentStatusParams.setEquipmentStatus("RT");
				chgeEquipmentStatusParams.setErrorProcessing("2");
				stepResult = chgeEquipmentStatusService.execute(chgeEquipmentStatusParams);
				chgeEquipmentStatusResult = (ChgeEquipmentStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgeEquipmentStatusResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
			} else if (dto.getReadingType() == ReadingTypeEnum._MAKE_AVAILABLE) {
				// DTL.Reading Type is Make Available
				// DEBUG genFunctionCall BEGIN 1000827 ACT ChgE Equipment Status - Plant Equipment  *
				chgeEquipmentStatusParams = new ChgeEquipmentStatusParams();
				chgeEquipmentStatusResult = new ChgeEquipmentStatusResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, chgeEquipmentStatusParams);
				chgeEquipmentStatusParams.setAwhRegionCode(dto.getAwhRegionCode());
				chgeEquipmentStatusParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				chgeEquipmentStatusParams.setEquipmentStatus("AT");
				chgeEquipmentStatusParams.setErrorProcessing("2");
				stepResult = chgeEquipmentStatusService.execute(chgeEquipmentStatusParams);
				chgeEquipmentStatusResult = (ChgeEquipmentStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgeEquipmentStatusResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
			} else if (dto.getReadingType() == ReadingTypeEnum._MAKE_UNAVAILABLE) {
				// DTL.Reading Type is Make UnAvailable
				// DEBUG genFunctionCall BEGIN 1000819 ACT ChgE Equipment Status - Plant Equipment  *
				chgeEquipmentStatusParams = new ChgeEquipmentStatusParams();
				chgeEquipmentStatusResult = new ChgeEquipmentStatusResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, chgeEquipmentStatusParams);
				chgeEquipmentStatusParams.setAwhRegionCode(dto.getAwhRegionCode());
				chgeEquipmentStatusParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				chgeEquipmentStatusParams.setEquipmentStatus("UA");
				chgeEquipmentStatusParams.setErrorProcessing("2");
				stepResult = chgeEquipmentStatusService.execute(chgeEquipmentStatusParams);
				chgeEquipmentStatusResult = (ChgeEquipmentStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgeEquipmentStatusResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000695 ACT PAR.Equipment Reading Sgt = DTL.Equipment Reading Sgt
			dto.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000706 ACT PAR.Reading Value = DTL.Reading Value
			dto.setReadingValue(dto.getReadingValue());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000689 ACT PAR.Reading Comment = DTL.Reading Comment
			dto.setReadingComment(dto.getReadingComment());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000553 ACT Commit .
				// TODO: Unsupported Internal Function Type '*COMMIT' (message surrogate = 1001566)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000555 ACT Send completion message - 'Updates accepted'
				//dto.addMessage("", "updates.accepted", messageSource);
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// * Master functions perform implicit Rollback. No need for it here.
				// DEBUG genFunctionCall BEGIN 1000561 ACT Send completion message - 'Updates cancelled'
				//dto.addMessage("", "updates.cancelled", messageSource);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(AddmEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000247 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */

    /**
     * Wsut5502SelectCentreService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceWsut5502SelectCentre(AddmEquipmentReadingsState state, Wsut5502SelectCentreResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, state);
        }

        //TODO: call the continuation of the program
        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);

        return stepResult;
    }
//
//    /**
//     * GetePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetePlantEquipment(AddmEquipmentReadingsState state, GetePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteCentreService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentre(AddmEquipmentReadingsState state, GeteCentreParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * CrteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCrteEquipmentReadings(AddmEquipmentReadingsState state, CrteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentresRegion(AddmEquipmentReadingsState state, GeteCentresRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeTransferSgtService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeTransferSgt(AddmEquipmentReadingsState state, ChgeTransferSgtParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeEquipmentStatusService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeEquipmentStatus(AddmEquipmentReadingsState state, ChgeEquipmentStatusParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgcrtePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgcrtePlantEquipment(AddmEquipmentReadingsState state, ChgcrtePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeForTransferService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeForTransfer(AddmEquipmentReadingsState state, ChgeForTransferParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * TransfeSubsqentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceTransfeSubsqentReadings(AddmEquipmentReadingsState state, TransfeSubsqentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmEquipmentReadingsParams addmEquipmentReadingsParams = new AddmEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, addmEquipmentReadingsParams);
//        stepResult = StepResult.callService(AddmEquipmentReadingsService.class, addmEquipmentReadingsParams);
//
//        return stepResult;
//    }
//


}
