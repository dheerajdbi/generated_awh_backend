package au.awh.file.equipmentreadings.addmequipmentreadings;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: AddmEquipmentReadings (WSPLTWVPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = 8770408284720285762L;

	private ExitProgramOptionEnum exitProgramOption = null;
	private long equipmentReadingSgt = 0L;
	private long readingValue = 0L;
	private String readingComment = "";

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
	
	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public long getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}
	
	public void setReadingValue(String readingValue) {
		setReadingValue(Long.parseLong(readingValue));
	}
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
}
