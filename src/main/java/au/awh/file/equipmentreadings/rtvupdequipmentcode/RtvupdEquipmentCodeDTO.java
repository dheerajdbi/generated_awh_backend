package au.awh.file.equipmentreadings.rtvupdequipmentcode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvupdEquipmentCodeDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String awhRegionCode;
	private String newPlantEquipmentCode;
	private String plantEquipmentCode;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

}
