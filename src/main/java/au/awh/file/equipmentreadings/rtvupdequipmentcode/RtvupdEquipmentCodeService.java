package au.awh.file.equipmentreadings.rtvupdequipmentcode;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.chgeequipmentcode.ChgeEquipmentCodeService;
// imports for DTO
import au.awh.file.equipmentreadings.chgeequipmentcode.ChgeEquipmentCodeDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.chgeequipmentcode.ChgeEquipmentCodeParams;
// imports for Results
import au.awh.file.equipmentreadings.chgeequipmentcode.ChgeEquipmentCodeResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'RtvUpd Equipment Code' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator
 */
@Service
public class RtvupdEquipmentCodeService extends AbstractService<RtvupdEquipmentCodeService, RtvupdEquipmentCodeDTO>
{
    private final Step execute = define("execute", RtvupdEquipmentCodeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private ChgeEquipmentCodeService chgeEquipmentCodeService;

	//private final Step serviceChgeEquipmentCode = define("serviceChgeEquipmentCode",ChgeEquipmentCodeResult.class, this::processServiceChgeEquipmentCode);
	

    @Autowired
    public RtvupdEquipmentCodeService()
    {
        super(RtvupdEquipmentCodeService.class, RtvupdEquipmentCodeDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(RtvupdEquipmentCodeParams params) throws ServiceException {
        RtvupdEquipmentCodeDTO dto = new RtvupdEquipmentCodeDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(RtvupdEquipmentCodeDTO dto, RtvupdEquipmentCodeParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<EquipmentReadings> equipmentReadingsList = equipmentReadingsRepository.findAllByAwhRegionCodeAndPlantEquipmentCode(dto.getAwhRegionCode(), dto.getPlantEquipmentCode());
		if (!equipmentReadingsList.isEmpty()) {
			for (EquipmentReadings equipmentReadings : equipmentReadingsList) {
				processDataRecord(dto, equipmentReadings);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        RtvupdEquipmentCodeResult result = new RtvupdEquipmentCodeResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(RtvupdEquipmentCodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(RtvupdEquipmentCodeDTO dto, EquipmentReadings equipmentReadings) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		ChgeEquipmentCodeParams chgeEquipmentCodeParams = null;
		ChgeEquipmentCodeResult chgeEquipmentCodeResult = null;
		// DEBUG genFunctionCall BEGIN 1000006 ACT ChgE Equipment Code - Equipment Readings  * On error, quit
		chgeEquipmentCodeParams = new ChgeEquipmentCodeParams();
		chgeEquipmentCodeResult = new ChgeEquipmentCodeResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, chgeEquipmentCodeParams);
		chgeEquipmentCodeParams.setErrorProcessing("5");
		chgeEquipmentCodeParams.setEquipmentReadingSgt(equipmentReadings.getEquipmentReadingSgt());
		chgeEquipmentCodeParams.setPlantEquipmentCode(dto.getNewPlantEquipmentCode());
		stepResult = chgeEquipmentCodeService.execute(chgeEquipmentCodeParams);
		chgeEquipmentCodeResult = (ChgeEquipmentCodeResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(chgeEquipmentCodeResult.get_SysReturnCode());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(RtvupdEquipmentCodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(RtvupdEquipmentCodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * ChgeEquipmentCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeEquipmentCode(RtvupdEquipmentCodeDTO dto, ChgeEquipmentCodeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        RtvupdEquipmentCodeParams rtvupdEquipmentCodeParams = new RtvupdEquipmentCodeParams();
//        BeanUtils.copyProperties(dto, rtvupdEquipmentCodeParams);
//        stepResult = StepResult.callService(RtvupdEquipmentCodeService.class, rtvupdEquipmentCodeParams);
//
//        return stepResult;
//    }
//
}
