package au.awh.file.equipmentreadings.rtvupdequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: RtvupdEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RtvupdEquipmentCodeParams implements Serializable
{
	private static final long serialVersionUID = -3509203409097961589L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private String newPlantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}
	
	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}
}
