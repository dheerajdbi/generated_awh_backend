package au.awh.file.equipmentreadings.changereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChangeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = -5344768625997755998L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
