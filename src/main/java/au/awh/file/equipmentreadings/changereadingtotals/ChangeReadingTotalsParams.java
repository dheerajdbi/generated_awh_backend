package au.awh.file.equipmentreadings.changereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChangeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeReadingTotalsParams implements Serializable
{
	private static final long serialVersionUID = 2096988852782383370L;

    private long equipmentReadingSgt = 0L;
    private long differenceFromPrevious = 0L;
    private long idleDaysFromPrevious = 0L;
    private long unavailableDaysFromPrv = 0L;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public long getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}
	
	public void setDifferenceFromPrevious(long differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}
	
	public void setDifferenceFromPrevious(String differenceFromPrevious) {
		setDifferenceFromPrevious(Long.parseLong(differenceFromPrevious));
	}
	
	public long getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(long idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(String idleDaysFromPrevious) {
		setIdleDaysFromPrevious(Long.parseLong(idleDaysFromPrevious));
	}
	
	public long getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}
	
	public void setUnavailableDaysFromPrv(long unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}
	
	public void setUnavailableDaysFromPrv(String unavailableDaysFromPrv) {
		setUnavailableDaysFromPrv(Long.parseLong(unavailableDaysFromPrv));
	}
}
