package au.awh.file.equipmentreadings.chgequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgEquipmentCodeParams implements Serializable
{
	private static final long serialVersionUID = 1967346281804481191L;

    private String plantEquipmentCode = "";
    private long equipmentReadingSgt = 0L;

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
}
