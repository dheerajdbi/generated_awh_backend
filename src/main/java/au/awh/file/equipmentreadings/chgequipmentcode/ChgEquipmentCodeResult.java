package au.awh.file.equipmentreadings.chgequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgEquipmentCodeResult implements Serializable
{
	private static final long serialVersionUID = -596559912195960890L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
