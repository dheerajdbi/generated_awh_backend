package au.awh.file.equipmentreadings.tranfsunsqentreadings;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.chgesegmentandcentre.ChgeSegmentAndCentreService;
// imports for DTO
import au.awh.file.equipmentreadings.chgesegmentandcentre.ChgeSegmentAndCentreDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.chgesegmentandcentre.ChgeSegmentAndCentreParams;
// imports for Results
import au.awh.file.equipmentreadings.chgesegmentandcentre.ChgeSegmentAndCentreResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Tranf Sunsqent Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator
 */
@Service
public class TranfSunsqentReadingsService extends AbstractService<TranfSunsqentReadingsService, TranfSunsqentReadingsDTO>
{
    private final Step execute = define("execute", TranfSunsqentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private ChgeSegmentAndCentreService chgeSegmentAndCentreService;

	//private final Step serviceChgeSegmentAndCentre = define("serviceChgeSegmentAndCentre",ChgeSegmentAndCentreResult.class, this::processServiceChgeSegmentAndCentre);
	

    @Autowired
    public TranfSunsqentReadingsService()
    {
        super(TranfSunsqentReadingsService.class, TranfSunsqentReadingsDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(TranfSunsqentReadingsParams params) throws ServiceException {
        TranfSunsqentReadingsDTO dto = new TranfSunsqentReadingsDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(TranfSunsqentReadingsDTO dto, TranfSunsqentReadingsParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<EquipmentReadings> equipmentReadingsList = equipmentReadingsRepository.findAllByAwhRegionCodeAndPlantEquipmentCodeAndReadingRequiredDateGreaterThan(dto.getAwhRegionCode(), dto.getPlantEquipmentCode(), dto.getReadingRequiredDate());
		if (!equipmentReadingsList.isEmpty()) {
			for (EquipmentReadings equipmentReadings : equipmentReadingsList) {
				processDataRecord(dto, equipmentReadings);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        TranfSunsqentReadingsResult result = new TranfSunsqentReadingsResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(TranfSunsqentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(TranfSunsqentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		ChgeSegmentAndCentreResult chgeSegmentAndCentreResult = null;
		ChgeSegmentAndCentreParams chgeSegmentAndCentreParams = null;
		if (equipmentReadings.getReadingType() == ReadingTypeEnum._TRANSFER_OUT) {
			// DB1.Reading Type is Transfer Out
			// DEBUG genFunctionCall BEGIN 1000029 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1000006 ACT ChgE Segment and Centre - Equipment Readings  * On error, quit
		chgeSegmentAndCentreParams = new ChgeSegmentAndCentreParams();
		chgeSegmentAndCentreResult = new ChgeSegmentAndCentreResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, chgeSegmentAndCentreParams);
		chgeSegmentAndCentreParams.setErrorProcessing("5");
		chgeSegmentAndCentreParams.setEquipmentReadingSgt(equipmentReadings.getEquipmentReadingSgt());
		chgeSegmentAndCentreParams.setCentreCodeKey(dto.getTransferToBuisSeg().getCode());
		chgeSegmentAndCentreParams.setAwhBuisnessSegment(AwhBuisnessSegmentEnum.fromCode(dto.getTransferToCentre()));
		stepResult = chgeSegmentAndCentreService.execute(chgeSegmentAndCentreParams);
		chgeSegmentAndCentreResult = (ChgeSegmentAndCentreResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(chgeSegmentAndCentreResult.get_SysReturnCode());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(TranfSunsqentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(TranfSunsqentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * ChgeSegmentAndCentreService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeSegmentAndCentre(TranfSunsqentReadingsDTO dto, ChgeSegmentAndCentreParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        TranfSunsqentReadingsParams tranfSunsqentReadingsParams = new TranfSunsqentReadingsParams();
//        BeanUtils.copyProperties(dto, tranfSunsqentReadingsParams);
//        stepResult = StepResult.callService(TranfSunsqentReadingsService.class, tranfSunsqentReadingsParams);
//
//        return stepResult;
//    }
//
}
