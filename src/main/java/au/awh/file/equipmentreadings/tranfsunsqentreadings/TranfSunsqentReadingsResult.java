package au.awh.file.equipmentreadings.tranfsunsqentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: TranfSunsqentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class TranfSunsqentReadingsResult implements Serializable
{
	private static final long serialVersionUID = -5187026847712131548L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
