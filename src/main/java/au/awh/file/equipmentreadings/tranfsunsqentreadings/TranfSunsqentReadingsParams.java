package au.awh.file.equipmentreadings.tranfsunsqentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: TranfSunsqentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class TranfSunsqentReadingsParams implements Serializable
{
	private static final long serialVersionUID = -8030741799200272487L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private LocalDate readingRequiredDate = null;
    private AwhBuisnessSegmentEnum transferToBuisSeg = null;
    private String transferToCentre = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}
	
	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
		this.transferToBuisSeg = transferToBuisSeg;
	}
	
	public void setTransferToBuisSeg(String transferToBuisSeg) {
		setTransferToBuisSeg(AwhBuisnessSegmentEnum.valueOf(transferToBuisSeg));
	}
	
	public String getTransferToCentre() {
		return transferToCentre;
	}
	
	public void setTransferToCentre(String transferToCentre) {
		this.transferToCentre = transferToCentre;
	}
}
