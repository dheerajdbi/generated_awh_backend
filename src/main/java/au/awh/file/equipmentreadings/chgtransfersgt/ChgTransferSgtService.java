package au.awh.file.equipmentreadings.chgtransfersgt;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Chg Transfer Sgt' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChgTransferSgtService extends AbstractService<ChgTransferSgtService, ChgTransferSgtDTO>
{
	private final Step execute = define("execute", ChgTransferSgtParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	
	@Autowired
	public ChgTransferSgtService() {
		super(ChgTransferSgtService.class, ChgTransferSgtDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgTransferSgtParams params) throws ServiceException {
		ChgTransferSgtDTO dto = new ChgTransferSgtDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgTransferSgtDTO dto, ChgTransferSgtParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId();
		equipmentReadingsId.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		EquipmentReadings equipmentReadings = equipmentReadingsRepository.findById(equipmentReadingsId).orElse(null);
		if (equipmentReadings == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, equipmentReadings);
			equipmentReadings.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
			equipmentReadings.setAwhRegionCode(dto.getAwhRegionCode());
			equipmentReadings.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			equipmentReadings.setReadingTimestamp(dto.getReadingTimestamp());
			equipmentReadings.setReadingType(dto.getReadingType());
			equipmentReadings.setReadingValue(dto.getReadingValue());
			equipmentReadings.setReadingComment(dto.getReadingComment());
			equipmentReadings.setUserId(dto.getUserId());
			equipmentReadings.setEquipmentLeasor(dto.getEquipmentLeasor());
			equipmentReadings.setTransferToFromSgt(dto.getTransferToFromSgt());
			equipmentReadings.setCentreCodeKey(dto.getCentreCodeKey());
			equipmentReadings.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			equipmentReadings.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			equipmentReadings.setReadingRequiredDate(dto.getReadingRequiredDate());
			equipmentReadings.setDifferenceFromPrevious(dto.getDifferenceFromPrevious());
			equipmentReadings.setIdleDaysFromPrevious(dto.getIdleDaysFromPrevious());
			equipmentReadings.setUnavailableDaysFromPrv(dto.getUnavailableDaysFromPrv());

			processingBeforeDataUpdate(dto, equipmentReadings);

			try {
				equipmentReadingsRepository.saveAndFlush(equipmentReadings);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChgTransferSgtResult result = new ChgTransferSgtResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChgTransferSgtDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		
		// Unprocessed SUB 8 -
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChgTransferSgtDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		
		// Unprocessed SUB 43 -
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChgTransferSgtDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChgTransferSgtDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		
		// Unprocessed SUB 10 -
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChgTransferSgtDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		
		// Unprocessed SUB 23 -
		return stepResult;
	}


}
