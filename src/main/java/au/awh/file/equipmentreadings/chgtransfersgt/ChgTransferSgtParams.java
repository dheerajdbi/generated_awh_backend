package au.awh.file.equipmentreadings.chgtransfersgt;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgTransferSgt ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgTransferSgtParams implements Serializable
{
	private static final long serialVersionUID = -3734416087947194678L;

    private long equipmentReadingSgt = 0L;
    private long transferToFromSgt = 0L;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	public void setTransferToFromSgt(String transferToFromSgt) {
		setTransferToFromSgt(Long.parseLong(transferToFromSgt));
	}
}
