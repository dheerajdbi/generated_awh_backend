package au.awh.file.equipmentreadings.dspeequipmentreading;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=dspeEquipmentReading (1879806) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingService;
// imports for DTO
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingParams;
// imports for Results
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'DspE Equipment Reading' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class DspeEquipmentReadingService extends AbstractService<DspeEquipmentReadingService, DspeEquipmentReadingDTO>
{
	private final Step execute = define("execute", DspeEquipmentReadingParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private DspEquipmentReadingService dspEquipmentReadingService;

	private final Step serviceDspEquipmentReading = define("serviceDspEquipmentReading",DspEquipmentReadingResult.class, this::processServiceDspEquipmentReading);
	
	@Autowired
	public DspeEquipmentReadingService() {
		super(DspeEquipmentReadingService.class, DspeEquipmentReadingDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(DspeEquipmentReadingParams params) throws ServiceException {
		DspeEquipmentReadingDTO dto = new DspeEquipmentReadingDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(DspeEquipmentReadingDTO dto, DspeEquipmentReadingParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		DspEquipmentReadingParams dspEquipmentReadingParams = null;
		DspEquipmentReadingResult dspEquipmentReadingResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Dsp Equipment Reading - Equipment Readings  *
		dspEquipmentReadingParams = new DspEquipmentReadingParams();
		dspEquipmentReadingResult = new DspEquipmentReadingResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, dspEquipmentReadingParams);
		dspEquipmentReadingParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		// TODO SPLIT SCREEN EXCINTFUN EquipmentReadings.dspeEquipmentReading (2) -> DSPRCD EquipmentReadings.dspEquipmentReading
		stepResult = StepResult.callService(DspEquipmentReadingService.class, dspEquipmentReadingParams).thenCall(serviceDspEquipmentReading);
		dto.setExitProgramOption(dspEquipmentReadingResult.getExitProgramOption());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1879806 EquipmentReadings.dspeEquipmentReading 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		DspeEquipmentReadingResult result = new DspeEquipmentReadingResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


    /**
     * DspEquipmentReadingService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceDspEquipmentReading(DspeEquipmentReadingDTO dto, DspEquipmentReadingResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, dto);
        }

        //TODO: call the continuation of the program
        DspeEquipmentReadingParams dspeEquipmentReadingParams = new DspeEquipmentReadingParams();
        BeanUtils.copyProperties(dto, dspeEquipmentReadingParams);
        stepResult = StepResult.callService(DspeEquipmentReadingService.class, dspeEquipmentReadingParams);

        return stepResult;
    }

}
