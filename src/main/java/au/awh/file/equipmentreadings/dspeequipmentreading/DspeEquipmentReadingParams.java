package au.awh.file.equipmentreadings.dspeequipmentreading;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: DspeEquipmentReading ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DspeEquipmentReadingParams implements Serializable
{
	private static final long serialVersionUID = 8419648407326705295L;

    private ErrorProcessingEnum errorProcessing = null;
    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
}
