package au.awh.file.equipmentreadings.rtvupdeequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: RtvupdeEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RtvupdeEquipmentCodeParams implements Serializable
{
	private static final long serialVersionUID = -4432367322115191614L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;
    private String newPlantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.valueOf(getRecordOption));
	}
	
	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}
	
	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}
}
