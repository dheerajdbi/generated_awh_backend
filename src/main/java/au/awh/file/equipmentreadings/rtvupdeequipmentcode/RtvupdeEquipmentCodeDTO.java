package au.awh.file.equipmentreadings.rtvupdeequipmentcode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class RtvupdeEquipmentCodeDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ErrorProcessingEnum errorProcessing;
	private GetRecordOptionEnum getRecordOption;
	private String awhRegionCode;
	private String newPlantEquipmentCode;
	private String plantEquipmentCode;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}

	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}

	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

}
