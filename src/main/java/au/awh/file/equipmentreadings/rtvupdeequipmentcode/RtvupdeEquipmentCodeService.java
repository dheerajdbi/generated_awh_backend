package au.awh.file.equipmentreadings.rtvupdeequipmentcode;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=rtvupdeEquipmentCode (2046612) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.rtvupdequipmentcode.RtvupdEquipmentCodeService;
// imports for DTO
import au.awh.file.equipmentreadings.rtvupdequipmentcode.RtvupdEquipmentCodeDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.rtvupdequipmentcode.RtvupdEquipmentCodeParams;
// imports for Results
import au.awh.file.equipmentreadings.rtvupdequipmentcode.RtvupdEquipmentCodeResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'RtvUpdE Equipment Code' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class RtvupdeEquipmentCodeService extends AbstractService<RtvupdeEquipmentCodeService, RtvupdeEquipmentCodeDTO>
{
	private final Step execute = define("execute", RtvupdeEquipmentCodeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private RtvupdEquipmentCodeService rtvupdEquipmentCodeService;

	//private final Step serviceRtvupdEquipmentCode = define("serviceRtvupdEquipmentCode",RtvupdEquipmentCodeResult.class, this::processServiceRtvupdEquipmentCode);
	
	@Autowired
	public RtvupdeEquipmentCodeService() {
		super(RtvupdeEquipmentCodeService.class, RtvupdeEquipmentCodeDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(RtvupdeEquipmentCodeParams params) throws ServiceException {
		RtvupdeEquipmentCodeDTO dto = new RtvupdeEquipmentCodeDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(RtvupdeEquipmentCodeDTO dto, RtvupdeEquipmentCodeParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		RtvupdEquipmentCodeParams rtvupdEquipmentCodeParams = null;
		RtvupdEquipmentCodeResult rtvupdEquipmentCodeResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT RtvUpd Equipment Code - Equipment Readings  *
		rtvupdEquipmentCodeParams = new RtvupdEquipmentCodeParams();
		rtvupdEquipmentCodeResult = new RtvupdEquipmentCodeResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, rtvupdEquipmentCodeParams);
		rtvupdEquipmentCodeParams.setNewPlantEquipmentCode(dto.getNewPlantEquipmentCode());
		rtvupdEquipmentCodeParams.setAwhRegionCode(dto.getAwhRegionCode());
		rtvupdEquipmentCodeParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		stepResult = rtvupdEquipmentCodeService.execute(rtvupdEquipmentCodeParams);
		rtvupdEquipmentCodeResult = (RtvupdEquipmentCodeResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(rtvupdEquipmentCodeResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// * NOTE: Insert <file> NF message here.
				// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Equipment Readings     NF'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878067) EXCINTFUN 2046612 EquipmentReadings.rtvupdeEquipmentCode 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 2046612 EquipmentReadings.rtvupdeEquipmentCode 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		RtvupdeEquipmentCodeResult result = new RtvupdeEquipmentCodeResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * RtvupdEquipmentCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvupdEquipmentCode(RtvupdeEquipmentCodeDTO dto, RtvupdEquipmentCodeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        RtvupdeEquipmentCodeParams rtvupdeEquipmentCodeParams = new RtvupdeEquipmentCodeParams();
//        BeanUtils.copyProperties(dto, rtvupdeEquipmentCodeParams);
//        stepResult = StepResult.callService(RtvupdeEquipmentCodeService.class, rtvupdeEquipmentCodeParams);
//
//        return stepResult;
//    }
//
}
