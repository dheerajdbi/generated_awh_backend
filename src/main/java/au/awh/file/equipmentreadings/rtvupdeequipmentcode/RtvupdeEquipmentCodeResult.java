package au.awh.file.equipmentreadings.rtvupdeequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: RtvupdeEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class RtvupdeEquipmentCodeResult implements Serializable
{
	private static final long serialVersionUID = 5174202646712503270L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
