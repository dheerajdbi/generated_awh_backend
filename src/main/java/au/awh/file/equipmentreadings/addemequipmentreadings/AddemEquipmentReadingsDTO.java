package au.awh.file.equipmentreadings.addemequipmentreadings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class AddemEquipmentReadingsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ErrorProcessingEnum errorProcessing;
	private ExitProgramOptionEnum exitProgramOption;
	private LocalDate readingRequiredDate;
	private String awhRegionCode;
	private String plantEquipmentCode;
	private String readingComment;
	private long equipmentReadingSgt;
	private long readingValue;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public String getReadingComment() {
		return readingComment;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public long getReadingValue() {
		return readingValue;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}

}
