package au.awh.file.equipmentreadings.chgeequipmentcode;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeEquipmentCode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeEquipmentCodeResult implements Serializable
{
	private static final long serialVersionUID = 8045341025209239992L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
