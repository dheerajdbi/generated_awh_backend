package au.awh.file.equipmentreadings;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Equipment Readings (WSEQREAD).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentReadingsRepository extends EquipmentReadingsRepositoryCustom, JpaRepository<EquipmentReadings, EquipmentReadingsId> {

	List<EquipmentReadings> findAllBy();

	List<EquipmentReadings> findAllByAwhRegionCodeAndCentreCodeKeyAndReadingTypeAndPlantEquipmentCodeAndReadingRequiredDate(String awhRegionCode, String centreCodeKey, ReadingTypeEnum readingType, String plantEquipmentCode, LocalDate readingRequiredDate);

	List<EquipmentReadings> findAllByAwhRegionCodeAndPlantEquipmentCode(String awhRegionCode, String plantEquipmentCode);

	List<EquipmentReadings> findAllByAwhRegionCodeAndPlantEquipmentCodeAndReadingRequiredDateGreaterThan(String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate);
}
