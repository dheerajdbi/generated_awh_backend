package au.awh.file.equipmentreadings.transfesubsqentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: TransfeSubsqentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class TransfeSubsqentReadingsParams implements Serializable
{
	private static final long serialVersionUID = -9121681749988653396L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private LocalDate readingRequiredDate = null;
    private ErrorProcessingEnum errorProcessing = null;
    private AwhBuisnessSegmentEnum transferToBuisSeg = null;
    private String transferToCentre = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}
	
	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
		this.transferToBuisSeg = transferToBuisSeg;
	}
	
	public void setTransferToBuisSeg(String transferToBuisSeg) {
		setTransferToBuisSeg(AwhBuisnessSegmentEnum.valueOf(transferToBuisSeg));
	}
	
	public String getTransferToCentre() {
		return transferToCentre;
	}
	
	public void setTransferToCentre(String transferToCentre) {
		this.transferToCentre = transferToCentre;
	}
}
