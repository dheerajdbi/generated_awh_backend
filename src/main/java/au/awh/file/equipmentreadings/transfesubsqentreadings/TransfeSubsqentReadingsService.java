package au.awh.file.equipmentreadings.transfesubsqentreadings;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=transfeSubsqentReadings (2027011) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.tranfsunsqentreadings.TranfSunsqentReadingsService;
// imports for DTO
import au.awh.file.equipmentreadings.tranfsunsqentreadings.TranfSunsqentReadingsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.tranfsunsqentreadings.TranfSunsqentReadingsParams;
// imports for Results
import au.awh.file.equipmentreadings.tranfsunsqentreadings.TranfSunsqentReadingsResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'TransfE Subsqent Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class TransfeSubsqentReadingsService extends AbstractService<TransfeSubsqentReadingsService, TransfeSubsqentReadingsDTO>
{
	private final Step execute = define("execute", TransfeSubsqentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private TranfSunsqentReadingsService tranfSunsqentReadingsService;

	//private final Step serviceTranfSunsqentReadings = define("serviceTranfSunsqentReadings",TranfSunsqentReadingsResult.class, this::processServiceTranfSunsqentReadings);
	
	@Autowired
	public TransfeSubsqentReadingsService() {
		super(TransfeSubsqentReadingsService.class, TransfeSubsqentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(TransfeSubsqentReadingsParams params) throws ServiceException {
		TransfeSubsqentReadingsDTO dto = new TransfeSubsqentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(TransfeSubsqentReadingsDTO dto, TransfeSubsqentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		TranfSunsqentReadingsParams tranfSunsqentReadingsParams = null;
		TranfSunsqentReadingsResult tranfSunsqentReadingsResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Tranf Sunsqent Readings - Equipment Readings  *
		tranfSunsqentReadingsParams = new TranfSunsqentReadingsParams();
		tranfSunsqentReadingsResult = new TranfSunsqentReadingsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, tranfSunsqentReadingsParams);
		tranfSunsqentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
		tranfSunsqentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		tranfSunsqentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
		tranfSunsqentReadingsParams.setTransferToBuisSeg(dto.getTransferToBuisSeg());
		tranfSunsqentReadingsParams.setTransferToCentre(dto.getTransferToCentre());
		stepResult = tranfSunsqentReadingsService.execute(tranfSunsqentReadingsParams);
		tranfSunsqentReadingsResult = (TranfSunsqentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(tranfSunsqentReadingsResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 2027011 EquipmentReadings.transfeSubsqentReadings 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		TransfeSubsqentReadingsResult result = new TransfeSubsqentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * TranfSunsqentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceTranfSunsqentReadings(TransfeSubsqentReadingsDTO dto, TranfSunsqentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        TransfeSubsqentReadingsParams transfeSubsqentReadingsParams = new TransfeSubsqentReadingsParams();
//        BeanUtils.copyProperties(dto, transfeSubsqentReadingsParams);
//        stepResult = StepResult.callService(TransfeSubsqentReadingsService.class, transfeSubsqentReadingsParams);
//
//        return stepResult;
//    }
//
}
