package au.awh.file.equipmentreadings.transfesubsqentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: TransfeSubsqentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class TransfeSubsqentReadingsResult implements Serializable
{
	private static final long serialVersionUID = 578553019467646999L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
