package au.awh.file.equipmentreadings.transfesubsqentreadings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class TransfeSubsqentReadingsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private AwhBuisnessSegmentEnum transferToBuisSeg;
	private ErrorProcessingEnum errorProcessing;
	private LocalDate readingRequiredDate;
	private String awhRegionCode;
	private String plantEquipmentCode;
	private String transferToCentre;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}

	public String getTransferToCentre() {
		return transferToCentre;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
		this.transferToBuisSeg = transferToBuisSeg;
	}

	public void setTransferToCentre(String transferToCentre) {
		this.transferToCentre = transferToCentre;
	}

}
