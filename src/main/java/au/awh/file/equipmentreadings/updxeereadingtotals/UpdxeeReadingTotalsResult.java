package au.awh.file.equipmentreadings.updxeereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: UpdxeeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdxeeReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = -1653786140726836060L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
