package au.awh.file.equipmentreadings.updreadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: UpdReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdReadingTotalsParams implements Serializable
{
	private static final long serialVersionUID = 8055250481528982441L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private String centreCodeKey = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
