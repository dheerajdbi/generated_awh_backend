package au.awh.file.equipmentreadings.updreadingtotals;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class UpdReadingTotalsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private String awhRegionCode;
	private String centreCodeKey;
	private String plantEquipmentCode;

	private String lclDateListName;
	private BigDecimal lclDifferenceFromPrevious;
	private BigDecimal lclIdleDaysFromPrevious;
	private LocalDate lclPreviousReadingDate;
	private ReadingTypeEnum lclPreviousReadingType;
	private BigDecimal lclPreviousReadingValue;
	private BigDecimal lclUnavailableDaysFromPrv;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public String getLclDateListName() {
		return lclDateListName;
	}

	public BigDecimal getLclDifferenceFromPrevious() {
		return lclDifferenceFromPrevious;
	}

	public BigDecimal getLclIdleDaysFromPrevious() {
		return lclIdleDaysFromPrevious;
	}

	public LocalDate getLclPreviousReadingDate() {
		return lclPreviousReadingDate;
	}

	public ReadingTypeEnum getLclPreviousReadingType() {
		return lclPreviousReadingType;
	}

	public BigDecimal getLclPreviousReadingValue() {
		return lclPreviousReadingValue;
	}

	public BigDecimal getLclUnavailableDaysFromPrv() {
		return lclUnavailableDaysFromPrv;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setLclDateListName(String lclDateListName) {
		this.lclDateListName = lclDateListName;
	}

	public void setLclDifferenceFromPrevious(BigDecimal lclDifferenceFromPrevious) {
		this.lclDifferenceFromPrevious = lclDifferenceFromPrevious;
	}

	public void setLclIdleDaysFromPrevious(BigDecimal lclIdleDaysFromPrevious) {
		this.lclIdleDaysFromPrevious = lclIdleDaysFromPrevious;
	}

	public void setLclPreviousReadingDate(LocalDate lclPreviousReadingDate) {
		this.lclPreviousReadingDate = lclPreviousReadingDate;
	}

	public void setLclPreviousReadingType(ReadingTypeEnum lclPreviousReadingType) {
		this.lclPreviousReadingType = lclPreviousReadingType;
	}

	public void setLclPreviousReadingValue(BigDecimal lclPreviousReadingValue) {
		this.lclPreviousReadingValue = lclPreviousReadingValue;
	}

	public void setLclUnavailableDaysFromPrv(BigDecimal lclUnavailableDaysFromPrv) {
		this.lclUnavailableDaysFromPrv = lclUnavailableDaysFromPrv;
	}

}
