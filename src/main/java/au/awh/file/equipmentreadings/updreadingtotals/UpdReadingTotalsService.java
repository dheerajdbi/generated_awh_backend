package au.awh.file.equipmentreadings.updreadingtotals;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListService;
import au.awh.file.equipmentreadings.chgereadingtotals.ChgeReadingTotalsService;
// imports for DTO
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListDTO;
import au.awh.file.equipmentreadings.chgereadingtotals.ChgeReadingTotalsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
// imports for Parameters
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListParams;
import au.awh.file.equipmentreadings.chgereadingtotals.ChgeReadingTotalsParams;
// imports for Results
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListResult;
import au.awh.file.equipmentreadings.chgereadingtotals.ChgeReadingTotalsResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Upd Reading Totals' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator
 */
@Service
public class UpdReadingTotalsService extends AbstractService<UpdReadingTotalsService, UpdReadingTotalsDTO>
{
    private final Step execute = define("execute", UpdReadingTotalsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private ChgeReadingTotalsService chgeReadingTotalsService;

	@Autowired
	private GetCentreHolidayListService getCentreHolidayListService;

	//private final Step serviceChgeReadingTotals = define("serviceChgeReadingTotals",ChgeReadingTotalsResult.class, this::processServiceChgeReadingTotals);
	//private final Step serviceGetCentreHolidayList = define("serviceGetCentreHolidayList",GetCentreHolidayListResult.class, this::processServiceGetCentreHolidayList);
	

    @Autowired
    public UpdReadingTotalsService()
    {
        super(UpdReadingTotalsService.class, UpdReadingTotalsDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(UpdReadingTotalsParams params) throws ServiceException {
        UpdReadingTotalsDTO dto = new UpdReadingTotalsDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(UpdReadingTotalsDTO dto, UpdReadingTotalsParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<EquipmentReadings> equipmentReadingsList = equipmentReadingsRepository.findAllByAwhRegionCodeAndPlantEquipmentCode(dto.getAwhRegionCode(), dto.getPlantEquipmentCode());
		if (!equipmentReadingsList.isEmpty()) {
			for (EquipmentReadings equipmentReadings : equipmentReadingsList) {
				processDataRecord(dto, equipmentReadings);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        UpdReadingTotalsResult result = new UpdReadingTotalsResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(UpdReadingTotalsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		GetCentreHolidayListResult getCentreHolidayListResult = null;
		GetCentreHolidayListParams getCentreHolidayListParams = null;
		// DEBUG genFunctionCall BEGIN 1000090 ACT Get Centre Holiday List - *Date List Header  *
		getCentreHolidayListParams = new GetCentreHolidayListParams();
		getCentreHolidayListResult = new GetCentreHolidayListResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getCentreHolidayListParams);
		getCentreHolidayListParams.setCentreCodeKey(dto.getCentreCodeKey());
		stepResult = getCentreHolidayListService.execute(getCentreHolidayListParams);
		getCentreHolidayListResult = (GetCentreHolidayListResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getCentreHolidayListResult.get_SysReturnCode());
		dto.setLclDateListName(getCentreHolidayListResult.getDateListName());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000232 ACT LCL.Previous Reading Type = CND.Not entered
		dto.setLclPreviousReadingType(ReadingTypeEnum._NOT_ENTERED);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000236 ACT LCL.Previous Reading Value = CND.Not entered
		dto.setLclPreviousReadingValue(BigDecimal.ZERO);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000240 ACT LCL.Previous reading Date = CND.Not Entered
		dto.setLclPreviousReadingDate(DateTimeUtils.toLocalDate(0001-01-01));
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processDataRecord(UpdReadingTotalsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		ChgeReadingTotalsResult chgeReadingTotalsResult = null;
		ChgeReadingTotalsParams chgeReadingTotalsParams = null;
		if (!dto.getLclPreviousReadingDate().equals(DateTimeUtils.toLocalDate(0001-01-01))) {
			// LCL.Previous reading Date is Entered
			// DEBUG genFunctionCall BEGIN 1000102 ACT LCL.Unavailable Days from Prv = CND.Not Entered
			dto.setLclUnavailableDaysFromPrv(BigDecimal.ZERO);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000110 ACT LCL.Idle Days from Previous = CND.Not Entered
			dto.setLclIdleDaysFromPrevious(BigDecimal.ZERO);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000075 ACT LCL.Diffrence from Previous = CND.Not entered
			dto.setLclDifferenceFromPrevious(BigDecimal.ZERO);
			// DEBUG genFunctionCall END
			if (equipmentReadings.getReadingType() == ReadingTypeEnum._METER_RESET) {
				// DB1.Reading Type is Meter Reset
			}if (dto.getLclPreviousReadingType() == ReadingTypeEnum._MAKE_UNAVAILABLE) {
				// LCL.Previous Reading Type is Make UnAvailable
				// DEBUG genFunctionCall BEGIN 1000057 ACT LCL.Unavailable Days from Prv = DB1.Reading Required Date - LCL.Previous reading Date *DAYS
				// TODO: The '*DURATION' built-in Function is not supported yet
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000118 ACT LCL.Diffrence from Previous = DB1.Reading Value - LCL.Previous Reading Value
				dto.setLclDifferenceFromPrevious(equipmentReadings.getReadingValue().subtract(dto.getLclPreviousReadingValue()));
				// DEBUG genFunctionCall END
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				equipmentReadings.getReadingValue() == dto.getLclPreviousReadingValue()
				// DEBUG ComparatorJavaGenerator END
				) {
					// DB1.Reading Value EQ LCL.Previous Reading Value
					// DEBUG genFunctionCall BEGIN 1000134 ACT LCL.Idle Days from Previous = DB1.Reading Required Date - LCL.Previous reading Date *DAYS
					// TODO: The '*DURATION' built-in Function is not supported yet
					// DEBUG genFunctionCall END
				}
			}
			// DEBUG genFunctionCall BEGIN 1000006 ACT ChgE Reading Totals - Equipment Readings  * On error, quit
			chgeReadingTotalsParams = new ChgeReadingTotalsParams();
			chgeReadingTotalsResult = new ChgeReadingTotalsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, chgeReadingTotalsParams);
			chgeReadingTotalsParams.setErrorProcessing("2");
			chgeReadingTotalsParams.setEquipmentReadingSgt(equipmentReadings.getEquipmentReadingSgt());
			chgeReadingTotalsParams.setDifferenceFromPrevious(dto.getLclDifferenceFromPrevious());
			chgeReadingTotalsParams.setIdleDaysFromPrevious(dto.getLclIdleDaysFromPrevious());
			chgeReadingTotalsParams.setUnavailableDaysFromPrv(dto.getLclUnavailableDaysFromPrv());
			stepResult = chgeReadingTotalsService.execute(chgeReadingTotalsParams);
			chgeReadingTotalsResult = (ChgeReadingTotalsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(chgeReadingTotalsResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1000184 ACT LCL.Previous Reading Type = DB1.Reading Type
		dto.setLclPreviousReadingType(equipmentReadings.getReadingType());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000192 ACT LCL.Previous Reading Value = DB1.Reading Value
		dto.setLclPreviousReadingValue(equipmentReadings.getReadingValue());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000180 ACT LCL.Previous reading Date = DB1.Reading Required Date
		dto.setLclPreviousReadingDate(equipmentReadings.getReadingRequiredDate());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(UpdReadingTotalsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(UpdReadingTotalsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * ChgeReadingTotalsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeReadingTotals(UpdReadingTotalsDTO dto, ChgeReadingTotalsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        UpdReadingTotalsParams updReadingTotalsParams = new UpdReadingTotalsParams();
//        BeanUtils.copyProperties(dto, updReadingTotalsParams);
//        stepResult = StepResult.callService(UpdReadingTotalsService.class, updReadingTotalsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetCentreHolidayListService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentreHolidayList(UpdReadingTotalsDTO dto, GetCentreHolidayListParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        UpdReadingTotalsParams updReadingTotalsParams = new UpdReadingTotalsParams();
//        BeanUtils.copyProperties(dto, updReadingTotalsParams);
//        stepResult = StepResult.callService(UpdReadingTotalsService.class, updReadingTotalsParams);
//
//        return stepResult;
//    }
//
}
