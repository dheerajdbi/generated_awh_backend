package au.awh.file.equipmentreadings.updreadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: UpdReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = -6896494841093076194L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
