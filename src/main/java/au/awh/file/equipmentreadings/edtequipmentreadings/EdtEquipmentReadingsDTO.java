package au.awh.file.equipmentreadings.edtequipmentreadings;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import au.awh.file.equipmentreadings.EquipmentReadings;

import au.awh.service.data.BaseDTO;
// generateStatusFieldImportStatements BEGIN 27
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * DTO for file 'Equipment Readings' (WSEQREAD) and function 'Edt Equipment Readings' (WSPLTWWE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtEquipmentReadingsDTO extends BaseDTO {
    private static final long serialVersionUID = -8010706224501472391L;

    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
    private String sflselPromptText = "";
    private String awhRegionCode = "";
    private String awhRegionNameDrv = "";
    private String centreCodeKey = "";
    private String centreNameDrv = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String conditionName = "";
    private String plantEquipmentCode = "";
    private String equipmentDescriptionDrv = "";
    private String equipmentTypeCode = "";
    private String equipmentTypeDescDrv = "";
    private String equipmentLeasor = "";
    private String organisationNameDrv = "";
    private LocalDateTime readingTimestamp = null;
    private ReadingTypeEnum readingType = ReadingTypeEnum._NOT_ENTERED;
    private BigDecimal readingValue = BigDecimal.ZERO;
    private EquipReadingUnitEnum equipReadingUnit = null;
    private LocalDate readingRequiredDate = null;
    private BigDecimal differenceFromPrevious = BigDecimal.ZERO;
    private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO;
    private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO;
    private String readingComment = "";
    private String userId = "";
    private BigDecimal transferToFromSgt = BigDecimal.ZERO;
    private ProgramModeEnum programMode = null;
    private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL;
	private String _SysconditionName = "";

    public EdtEquipmentReadingsDTO() {
    }

    public EdtEquipmentReadingsDTO(EquipmentReadings equipmentReadings) {
        setDtoFields(equipmentReadings);
    }

    public BigDecimal getEquipmentReadingSgt() {
        return equipmentReadingSgt;
    }

    public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
        this.equipmentReadingSgt = equipmentReadingSgt;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getAwhRegionCode() {
        return awhRegionCode;
    }

    public void setAwhRegionCode(String awhRegionCode) {
        this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionNameDrv() {
        return awhRegionNameDrv;
    }

    public void setAwhRegionNameDrv(String awhRegionNameDrv) {
        this.awhRegionNameDrv = awhRegionNameDrv;
    }

    public String getCentreCodeKey() {
        return centreCodeKey;
    }

    public void setCentreCodeKey(String centreCodeKey) {
        this.centreCodeKey = centreCodeKey;
    }

    public String getCentreNameDrv() {
        return centreNameDrv;
    }

    public void setCentreNameDrv(String centreNameDrv) {
        this.centreNameDrv = centreNameDrv;
    }

    public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
        return awhBuisnessSegment;
    }

    public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
        this.awhBuisnessSegment = awhBuisnessSegment;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getPlantEquipmentCode() {
        return plantEquipmentCode;
    }

    public void setPlantEquipmentCode(String plantEquipmentCode) {
        this.plantEquipmentCode = plantEquipmentCode;
    }

    public String getEquipmentDescriptionDrv() {
        return equipmentDescriptionDrv;
    }

    public void setEquipmentDescriptionDrv(String equipmentDescriptionDrv) {
        this.equipmentDescriptionDrv = equipmentDescriptionDrv;
    }

    public String getEquipmentTypeCode() {
        return equipmentTypeCode;
    }

    public void setEquipmentTypeCode(String equipmentTypeCode) {
        this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeDescDrv() {
        return equipmentTypeDescDrv;
    }

    public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
        this.equipmentTypeDescDrv = equipmentTypeDescDrv;
    }

    public String getEquipmentLeasor() {
        return equipmentLeasor;
    }

    public void setEquipmentLeasor(String equipmentLeasor) {
        this.equipmentLeasor = equipmentLeasor;
    }

    public String getOrganisationNameDrv() {
        return organisationNameDrv;
    }

    public void setOrganisationNameDrv(String organisationNameDrv) {
        this.organisationNameDrv = organisationNameDrv;
    }

    public LocalDateTime getReadingTimestamp() {
        return readingTimestamp;
    }

    public void setReadingTimestamp(LocalDateTime readingTimestamp) {
        this.readingTimestamp = readingTimestamp;
    }

    public ReadingTypeEnum getReadingType() {
        return readingType;
    }

    public void setReadingType(ReadingTypeEnum readingType) {
        this.readingType = readingType;
    }

    public BigDecimal getReadingValue() {
        return readingValue;
    }

    public void setReadingValue(BigDecimal readingValue) {
        this.readingValue = readingValue;
    }

    public EquipReadingUnitEnum getEquipReadingUnit() {
        return equipReadingUnit;
    }

    public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
        this.equipReadingUnit = equipReadingUnit;
    }

    public LocalDate getReadingRequiredDate() {
        return readingRequiredDate;
    }

    public void setReadingRequiredDate(LocalDate readingRequiredDate) {
        this.readingRequiredDate = readingRequiredDate;
    }

    public BigDecimal getDifferenceFromPrevious() {
        return differenceFromPrevious;
    }

    public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
        this.differenceFromPrevious = differenceFromPrevious;
    }

    public BigDecimal getIdleDaysFromPrevious() {
        return idleDaysFromPrevious;
    }

    public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
        this.idleDaysFromPrevious = idleDaysFromPrevious;
    }

    public BigDecimal getUnavailableDaysFromPrv() {
        return unavailableDaysFromPrv;
    }

    public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
        this.unavailableDaysFromPrv = unavailableDaysFromPrv;
    }

    public String getReadingComment() {
        return readingComment;
    }

    public void setReadingComment(String readingComment) {
        this.readingComment = readingComment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public BigDecimal getTransferToFromSgt() {
        return transferToFromSgt;
    }

    public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
        this.transferToFromSgt = transferToFromSgt;
    }

    public ProgramModeEnum getProgramMode() {
        return programMode;
    }

    public void setProgramMode(ProgramModeEnum programMode) {
        this.programMode = programMode;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
        return exitProgramOption;
    }

    public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.exitProgramOption = exitProgramOption;
    }

	public void set_SysConditionName(String conditionName) {
    	this._SysconditionName = conditionName;
    }

	public String get_SysConditionName() {
		return _SysconditionName;
	}    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setDtoFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(equipmentReadings, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setEntityFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(this, equipmentReadings);
    }
}
