package au.awh.file.equipmentreadings.edtequipmentreadings;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.support.JobContext;

import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadings.changeequipmentreadings.ChangeEquipmentReadingsService;
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsService;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadings.changeequipmentreadings.ChangeEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsDTO;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadings.changeequipmentreadings.ChangeEquipmentReadingsParams;
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsParams;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadings.changeequipmentreadings.ChangeEquipmentReadingsResult;
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsResult;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END



import au.awh.service.ServiceException;

import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 42
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * EDTRCD Service controller for 'Edt Equipment Readings' (WSPLTWWE1K) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Service
public class EdtEquipmentReadingsService extends AbstractService<EdtEquipmentReadingsService, EdtEquipmentReadingsState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private EquipmentReadingsRepository equipmentReadingsRepository;
    
    @Autowired
    private ChangeEquipmentReadingsService changeEquipmentReadingsService;
    
    @Autowired
    private CreateEquipmentReadingsService createEquipmentReadingsService;
    
    @Autowired
    private GetePlantEquipmentService getePlantEquipmentService;
    
    @Autowired
    private MessageSource messageSource;
        
    public static final String SCREEN_KEY = "edtEquipmentReadingsEntryPanel";
    public static final String SCREEN_DTL = "edtEquipmentReadingsPanel";
    public static final String SCREEN_CFM = "EdtEquipmentReadings.confirm";
    
    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", EdtEquipmentReadingsParams.class, this::executeService);
    private final Step keyScreenResponse = define("keyScreen", EdtEquipmentReadingsDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", EdtEquipmentReadingsDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", EdtEquipmentReadingsDTO.class, this::processConfirmScreenResponse);
    
    
    //private final Step serviceCreateEquipmentReadings = define("serviceCreateEquipmentReadings",CreateEquipmentReadingsResult.class, this::processServiceCreateEquipmentReadings);
    //private final Step serviceChangeEquipmentReadings = define("serviceChangeEquipmentReadings",ChangeEquipmentReadingsResult.class, this::processServiceChangeEquipmentReadings);
    //private final Step serviceGetePlantEquipment = define("serviceGetePlantEquipment",GetePlantEquipmentResult.class, this::processServiceGetePlantEquipment);
    
        
    @Autowired
    public EdtEquipmentReadingsService()
    {
        super(EdtEquipmentReadingsService.class, EdtEquipmentReadingsState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EdtEquipmentReadingsState state, EdtEquipmentReadingsParams params)
    {
        StepResult stepResult = NO_ACTION;
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        usrInitializeProgram(state);
    
        stepResult = conductKeyScreenConversation(state);
    
        return stepResult;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductKeyScreenConversation(EdtEquipmentReadingsState state) 
    {
        StepResult stepResult = NO_ACTION;
    
        if(shouldBypassKeyScreen(state)){
            stepResult = conductDetailScreenConversation(state);
        } else {
            stepResult = displayKeyScreenConversation(state);
        }
    
        return stepResult;
    }
    
    /**
     * Determine if SCREEN_KEY should be bypassed
     * @param state - Service state class.
     * @return bypass - boolean
     */
    private boolean shouldBypassKeyScreen(EdtEquipmentReadingsState state)
    {
        boolean bypass = false;
    
        //bypass if all key screens are set
        if(state.getEquipmentReadingSgt() != BigDecimal.ZERO){
            bypass = true;
        }
    
        return bypass;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult displayKeyScreenConversation(EdtEquipmentReadingsState state)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            EdtEquipmentReadingsDTO model = new EdtEquipmentReadingsDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processKeyScreenResponse(EdtEquipmentReadingsState state, EdtEquipmentReadingsDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        model.clearMessages();
    
        BeanUtils.copyProperties(model, state);
    
        if (isHelp(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        }
        else if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
    
        }
        else if(isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            stepResult = displayKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = displayKeyScreenConversation(state);
                return stepResult;
            }
            usrValidateKeyScreen(state);
            dbfReadDataRecord(state);
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                usrInitializeExistingScreen(state);
            }
            else {
                usrInitializeNewScreen(state);
            }
    
            stepResult = conductDetailScreenConversation(state);
        }
    
        return stepResult;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     */
    private void checkKeyFields(EdtEquipmentReadingsState state)
    {
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductDetailScreenConversation(EdtEquipmentReadingsState state)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            EdtEquipmentReadingsDTO model = new EdtEquipmentReadingsDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processDetailScreenResponse(EdtEquipmentReadingsState state, EdtEquipmentReadingsDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        model.clearMessages();
    
        BeanUtils.copyProperties(model, state);
    
        if (isHelp(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        }
        else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        }
        else if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
    
        }
        else if (isKeyScreen(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            EquipmentReadings equipmentReadings = new EquipmentReadings();
            BeanUtils.copyProperties(equipmentReadings, state);
            stepResult = displayKeyScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = conductDetailScreenConversation(state);
                return stepResult;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = conductDetailScreenConversation(state);
                return stepResult;
            }
            usrDetailScreenFunctionFields(state);
            //TODO: make confirm screen
            stepResult = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processConfirmScreenResponse(EdtEquipmentReadingsState state, EdtEquipmentReadingsDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        stepResult = displayKeyScreenConversation(state);
    
        return stepResult;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     */
    private void checkFields(EdtEquipmentReadingsState state)
    {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     */
    private void checkRelations(EdtEquipmentReadingsState state)
    {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EdtEquipmentReadingsState state)
    {
        StepResult stepResult = NO_ACTION;
    
        stepResult = usrExitCommandProcessing(state);
    
        EdtEquipmentReadingsResult params = new EdtEquipmentReadingsResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);
    
        return stepResult;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;
    
        EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId(dto.getEquipmentReadingSgt());
        EquipmentReadings equipmentReadings = equipmentReadingsRepository.findById(equipmentReadingsId).orElse(null);
    
        if (equipmentReadings == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(equipmentReadings, dto);
        }
        return stepResult;
    }
    
    private StepResult dbfCreateDataRecord(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreateEquipmentReadingsResult createEquipmentReadingsResult = null;
    		CreateEquipmentReadingsParams createEquipmentReadingsParams = null;
    		// DEBUG genFunctionCall BEGIN 426 ACT Create Equipment Readings - Equipment Readings  *
    		createEquipmentReadingsParams = new CreateEquipmentReadingsParams();
    		createEquipmentReadingsResult = new CreateEquipmentReadingsResult();
    		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
    		BeanUtils.copyProperties(dto, createEquipmentReadingsParams);
    		createEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
    		createEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
    		createEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
    		createEquipmentReadingsParams.setReadingTimestamp(dto.getReadingTimestamp());
    		createEquipmentReadingsParams.setReadingType(dto.getReadingType());
    		createEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
    		createEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
    		createEquipmentReadingsParams.setUserId(dto.getUserId());
    		createEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
    		createEquipmentReadingsParams.setTransferToFromSgt(dto.getTransferToFromSgt());
    		createEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
    		createEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
    		createEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
    		createEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
    		stepResult = createEquipmentReadingsService.execute(createEquipmentReadingsParams);
    		createEquipmentReadingsResult = (CreateEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
    		dto.set_SysReturnCode(createEquipmentReadingsResult.get_SysReturnCode());
    		dto.setEquipmentReadingSgt(createEquipmentReadingsResult.getEquipmentReadingSgt());
    		// DEBUG genFunctionCall END
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }
    
    private StepResult dbfDeleteDataRecord(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;
        // TODO : Miss DLTOBJ function?? (EDTRCDJavaController)
    
        return stepResult;
    }
    
    private StepResult dbfUpdateDataRecord(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;
        try {
    		EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId(dto.getEquipmentReadingSgt());
    		if (equipmentReadingsRepository.existsById(equipmentReadingsId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangeEquipmentReadingsResult changeEquipmentReadingsResult = null;
    			ChangeEquipmentReadingsParams changeEquipmentReadingsParams = null;
    			// DEBUG genFunctionCall BEGIN 428 ACT Change Equipment Readings - Equipment Readings  *
    			changeEquipmentReadingsParams = new ChangeEquipmentReadingsParams();
    			changeEquipmentReadingsResult = new ChangeEquipmentReadingsResult();
    			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
    			BeanUtils.copyProperties(dto, changeEquipmentReadingsParams);
    			changeEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
    			changeEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
    			changeEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
    			changeEquipmentReadingsParams.setReadingTimestamp(dto.getReadingTimestamp());
    			changeEquipmentReadingsParams.setReadingType(dto.getReadingType());
    			changeEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
    			changeEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
    			changeEquipmentReadingsParams.setUserId(dto.getUserId());
    			changeEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
    			changeEquipmentReadingsParams.setTransferToFromSgt(dto.getTransferToFromSgt());
    			changeEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
    			changeEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
    			changeEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
    			changeEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
    			stepResult = changeEquipmentReadingsService.execute(changeEquipmentReadingsParams);
    			changeEquipmentReadingsResult = (ChangeEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
    			dto.set_SysReturnCode(changeEquipmentReadingsResult.get_SysReturnCode());
    			// DEBUG genFunctionCall END
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrInitializeProgram(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Program (Generated:10)
			 */
			
			if (dto.getProgramMode() == ProgramModeEnum._STA_ADD) {
				// PAR.*Program mode is *ADD
				// DEBUG genFunctionCall BEGIN 1000005 ACT PGM.*Program mode = CND.*ADD
				dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000012 ACT PGM.*Program mode = CND.*CHANGE
				dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000079 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrValidateKeyScreen(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Key Screen (Generated:418)
			 */
			
			// * Don't send any error messages on load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000091 ACT LCL.Error Processing = CND.Ignore
			dto.setLclErrorProcessing(ErrorProcessingEnum._IGNORE);
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrInitializeNewScreen(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Detail Screen (New Record) (Generated:239)
			 */
			
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrInitializeExistingScreen(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * Initialize Detail Screen (Existing Record) (Generated:242)
			 */
			GetePlantEquipmentResult getePlantEquipmentResult = null;
			GetePlantEquipmentParams getePlantEquipmentParams = null;
			// DEBUG genFunctionCall BEGIN 1000130 ACT DTL.*Condition name = Condition name of DTL.AWH Buisness Segment
			dto.set_SysConditionName(dto.getAwhBuisnessSegment().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000134 ACT DTL.Condition Name = Condition name of DTL.Reading Type
			dto.setConditionName(dto.getReadingType().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000138 ACT GetE Plant Equipment - Plant Equipment  *
			getePlantEquipmentParams = new GetePlantEquipmentParams();
			getePlantEquipmentResult = new GetePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getePlantEquipmentParams);
			getePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
			getePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			getePlantEquipmentParams.setErrorProcessing("2");
			getePlantEquipmentParams.setGetRecordOption(dto.getLclGetRecordOption());
			stepResult = getePlantEquipmentService.execute(getePlantEquipmentParams);
			getePlantEquipmentResult = (GetePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getePlantEquipmentResult.get_SysReturnCode());
			dto.setEquipReadingUnit(getePlantEquipmentResult.getEquipReadingUnit());
			dto.setLclEquipmentStatus(getePlantEquipmentResult.getEquipmentStatus());
			dto.setLclEquipmentSerialNumber(getePlantEquipmentResult.getEquipmentSerialNumber());
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrValidateDetailScreenFields(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			
			// * Send any error messages on validate of detail screen.
			// DEBUG genFunctionCall BEGIN 1000097 ACT LCL.Error Processing = CND.Send message and ignore
			dto.setLclErrorProcessing(ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE);
			// DEBUG genFunctionCall END
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000019 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000023 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrDetailScreenFunctionFields(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * CALC: Detail Screen Function Fields (Generated:469)
			 */
			
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000109 ACT Equipment Description Drv*FIELD                                             DTLE
			// TODO: NEED TO SUPPORT *FIELD Equipment Description Drv*FIELD                                             DTLE 1000109
			// DEBUG X2EActionDiagramElement 1000109 ACT     Equipment Description Drv*FIELD                                             DTLE
			// DEBUG X2EActionDiagramElement 1000110 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000111 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000112 PAR DTL 
			// ERROR UNEXPECTED NUMBER OF PARAMETERS
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000114 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000114
			// DEBUG X2EActionDiagramElement 1000114 ACT     AWH region Name Drv      *FIELD                                             DTLA
			// DEBUG X2EActionDiagramElement 1000115 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000116 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000118 ACT Centre Name Drv          *FIELD                                             DTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000118
			// DEBUG X2EActionDiagramElement 1000118 ACT     Centre Name Drv          *FIELD                                             DTLC
			// DEBUG X2EActionDiagramElement 1000119 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000120 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000122 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
			// TODO: NEED TO SUPPORT *FIELD Equipment Type Desc Drv  *FIELD                                             DTLE 1000122
			// DEBUG X2EActionDiagramElement 1000122 ACT     Equipment Type Desc Drv  *FIELD                                             DTLE
			// DEBUG X2EActionDiagramElement 1000123 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000124 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000126 ACT Organisation Name Drv    *FIELD                                             DTLO
			// TODO: NEED TO SUPPORT *FIELD Organisation Name Drv    *FIELD                                             DTLO 1000126
			// DEBUG X2EActionDiagramElement 1000126 ACT     Organisation Name Drv    *FIELD                                             DTLO
			// DEBUG X2EActionDiagramElement 1000127 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000128 PAR DTL 
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrExitCommandProcessing(EdtEquipmentReadingsState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			
			if (isExit(dto.get_SysCmdKey())) {
				// KEY.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000067 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * CreateEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateEquipmentReadings(EdtEquipmentReadingsState state, CreateEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtEquipmentReadingsParams edtEquipmentReadingsParams = new EdtEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, edtEquipmentReadingsParams);
//        stepResult = StepResult.callService(EdtEquipmentReadingsService.class, edtEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChangeEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeEquipmentReadings(EdtEquipmentReadingsState state, ChangeEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtEquipmentReadingsParams edtEquipmentReadingsParams = new EdtEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, edtEquipmentReadingsParams);
//        stepResult = StepResult.callService(EdtEquipmentReadingsService.class, edtEquipmentReadingsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetePlantEquipment(EdtEquipmentReadingsState state, GetePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtEquipmentReadingsParams edtEquipmentReadingsParams = new EdtEquipmentReadingsParams();
//        BeanUtils.copyProperties(state, edtEquipmentReadingsParams);
//        stepResult = StepResult.callService(EdtEquipmentReadingsService.class, edtEquipmentReadingsParams);
//
//        return stepResult;
//    }
//


}
