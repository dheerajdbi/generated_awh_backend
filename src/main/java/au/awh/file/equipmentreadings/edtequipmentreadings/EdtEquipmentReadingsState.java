package au.awh.file.equipmentreadings.edtequipmentreadings;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.model.ReloadSubfileEnum;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Edt Equipment Readings' (WSPLTWWE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtEquipmentReadingsState extends EdtEquipmentReadingsDTO {
    private static final long serialVersionUID = 958004535389736265L;
    // Local fields
    private String lclEquipmentSerialNumber = "";
    private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum._NOT_ENTERED;
    private ErrorProcessingEnum lclErrorProcessing = null;
    private GetRecordOptionEnum lclGetRecordOption = GetRecordOptionEnum._NORMAL;

    // System fields
    private ProgramModeEnum _sysProgramMode = null;
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

    public EdtEquipmentReadingsState() {
    }

    public EdtEquipmentReadingsState(EquipmentReadings equipmentReadings) {
        setDtoFields(equipmentReadings);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclEquipmentSerialNumber(String equipmentSerialNumber) {
        this.lclEquipmentSerialNumber = equipmentSerialNumber;
    }

    public String getLclEquipmentSerialNumber() {
        return lclEquipmentSerialNumber;
    }

    public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
        this.lclEquipmentStatus = equipmentStatus;
    }

    public EquipmentStatusEnum getLclEquipmentStatus() {
        return lclEquipmentStatus;
    }

    public void setLclErrorProcessing(ErrorProcessingEnum errorProcessing) {
        this.lclErrorProcessing = errorProcessing;
    }

    public ErrorProcessingEnum getLclErrorProcessing() {
        return lclErrorProcessing;
    }

    public void setLclGetRecordOption(GetRecordOptionEnum getRecordOption) {
        this.lclGetRecordOption = getRecordOption;
    }

    public GetRecordOptionEnum getLclGetRecordOption() {
        return lclGetRecordOption;
    }

    public void set_SysProgramMode(ProgramModeEnum programMode) {
        _sysProgramMode = programMode;
    }

    public ProgramModeEnum get_SysProgramMode() {
        return _sysProgramMode;
    }

    public void set_SysReturnCode(ReturnCodeEnum returnCode) {
        _sysReturnCode = returnCode;
    }

    public ReturnCodeEnum get_SysReturnCode() {
        return _sysReturnCode;
    }

    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfileEnum() {
        return _sysReloadSubfile;
    }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
