package au.awh.file.equipmentreadings.edtequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: EdtEquipmentReadings (WSPLTWWE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = 658441464774739233L;

    private ExitProgramOptionEnum exitProgramOption = null;

    public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
