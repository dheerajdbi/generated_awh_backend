package au.awh.file.equipmentreadings.edtequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EdtEquipmentReadings (WSPLTWWE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = -7068748167598224641L;

    private long equipmentReadingSgt = 0L;
    private ProgramModeEnum programMode = null;

    public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public ProgramModeEnum getProgramMode() {
		return programMode;
	}
	
	public void setProgramMode(ProgramModeEnum programMode) {
		this.programMode = programMode;
	}
	
	public void setProgramMode(String programMode) {
		setProgramMode(ProgramModeEnum.valueOf(programMode));
	}
}
