package au.awh.file.equipmentreadings.changeequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChangeEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = 7932185706178169473L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
