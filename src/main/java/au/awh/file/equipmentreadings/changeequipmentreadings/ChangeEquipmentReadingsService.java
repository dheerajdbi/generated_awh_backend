package au.awh.file.equipmentreadings.changeequipmentreadings;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Change Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChangeEquipmentReadingsService extends AbstractService<ChangeEquipmentReadingsService, ChangeEquipmentReadingsDTO>
{
	private final Step execute = define("execute", ChangeEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	
	@Autowired
	public ChangeEquipmentReadingsService() {
		super(ChangeEquipmentReadingsService.class, ChangeEquipmentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChangeEquipmentReadingsParams params) throws ServiceException {
		ChangeEquipmentReadingsDTO dto = new ChangeEquipmentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChangeEquipmentReadingsDTO dto, ChangeEquipmentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId();
		equipmentReadingsId.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		EquipmentReadings equipmentReadings = equipmentReadingsRepository.findById(equipmentReadingsId).orElse(null);
		if (equipmentReadings == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, equipmentReadings);
			equipmentReadings.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
			equipmentReadings.setAwhRegionCode(dto.getAwhRegionCode());
			equipmentReadings.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			equipmentReadings.setReadingTimestamp(dto.getReadingTimestamp());
			equipmentReadings.setReadingType(dto.getReadingType());
			equipmentReadings.setReadingValue(dto.getReadingValue());
			equipmentReadings.setReadingComment(dto.getReadingComment());
			equipmentReadings.setUserId(dto.getUserId());
			equipmentReadings.setEquipmentLeasor(dto.getEquipmentLeasor());
			equipmentReadings.setTransferToFromSgt(dto.getTransferToFromSgt());
			equipmentReadings.setCentreCodeKey(dto.getCentreCodeKey());
			equipmentReadings.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			equipmentReadings.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			equipmentReadings.setReadingRequiredDate(dto.getReadingRequiredDate());
			equipmentReadings.setDifferenceFromPrevious(dto.getDifferenceFromPrevious());
			equipmentReadings.setIdleDaysFromPrevious(dto.getIdleDaysFromPrevious());
			equipmentReadings.setUnavailableDaysFromPrv(dto.getUnavailableDaysFromPrv());

			processingBeforeDataUpdate(dto, equipmentReadings);

			try {
				equipmentReadingsRepository.saveAndFlush(equipmentReadings);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChangeEquipmentReadingsResult result = new ChangeEquipmentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChangeEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChangeEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChangeEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Empty:48)
		 */
		
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChangeEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:10)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChangeEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Empty:23)
		 */
		
		return stepResult;
	}


}
