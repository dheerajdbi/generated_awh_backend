package au.awh.file.equipmentreadings.getbyreversedate;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get By Reverse Date' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator
 */
@Service
public class GetByReverseDateService extends AbstractService<GetByReverseDateService, GetByReverseDateDTO>
{
    private final Step execute = define("execute", GetByReverseDateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	

    @Autowired
    public GetByReverseDateService()
    {
        super(GetByReverseDateService.class, GetByReverseDateDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetByReverseDateParams params) throws ServiceException {
        GetByReverseDateDTO dto = new GetByReverseDateDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetByReverseDateDTO dto, GetByReverseDateParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<EquipmentReadings> equipmentReadingsList = equipmentReadingsRepository.findAllByAwhRegionCodeAndPlantEquipmentCodeAndReadingRequiredDateGreaterThan(dto.getAwhRegionCode(), dto.getPlantEquipmentCode(), dto.getReadingRequiredDate());
		if (!equipmentReadingsList.isEmpty()) {
			for (EquipmentReadings equipmentReadings : equipmentReadingsList) {
				processDataRecord(dto, equipmentReadings);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetByReverseDateResult result = new GetByReverseDateResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetByReverseDateDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000054 ACT LCL.Reading Type = PAR.Reading Type
		dto.setLclReadingType(dto.getReadingType());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
		dto.setReadingValue(0L);
		dto.setReadingComment("");
		dto.setUserId("");
		dto.setEquipmentLeasor("");
		dto.setTransferToFromSgt(0L);
		dto.setCentreCodeKey("");
		dto.setAwhBuisnessSegment(null);
		dto.setEquipmentTypeCode("");
		dto.setReadingRequiredDate(LocalDate.of(1801, 1, 1));
		dto.setEquipmentReadingSgt(0L);
		dto.setReadingType(ReadingTypeEnum._NOT_ENTERED);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000064 ACT PAR.Reading Type = LCL.Reading Type
		dto.setReadingType(dto.getLclReadingType());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processDataRecord(GetByReverseDateDTO dto, EquipmentReadings equipmentReadings) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		if (((dto.getReadingType() == ReadingTypeEnum._STANDARD_READING) && (ReadingTypeEnum.isPreviousReading(equipmentReadings.getReadingType()))) || (!(dto.getReadingType() == ReadingTypeEnum._STANDARD_READING) && (
		// DEBUG ComparatorJavaGenerator BEGIN
		dto.getReadingType() == equipmentReadings.getReadingType()
		// DEBUG ComparatorJavaGenerator END
		)) || (dto.getReadingType() == ReadingTypeEnum._ANY)) {
			// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
			// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
			// DEBUG genFunctionCall END
			// * Assume this is a FIFO partial key access.
			// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetByReverseDateDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000068 ACT PAR = CON By name
		dto.setReadingValue(0L);
		dto.setReadingComment("");
		dto.setUserId("");
		dto.setEquipmentLeasor("");
		dto.setTransferToFromSgt(0L);
		dto.setCentreCodeKey("");
		dto.setAwhBuisnessSegment(null);
		dto.setEquipmentTypeCode("");
		dto.setReadingRequiredDate(LocalDate.of(1801, 1, 1));
		dto.setEquipmentReadingSgt(0L);
		dto.setReadingType(ReadingTypeEnum._NOT_ENTERED);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetByReverseDateDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
