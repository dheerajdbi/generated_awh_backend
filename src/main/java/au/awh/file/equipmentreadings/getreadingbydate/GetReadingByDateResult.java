package au.awh.file.equipmentreadings.getreadingbydate;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDateTime;


import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 9
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetReadingByDate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetReadingByDateResult implements Serializable
{
	private static final long serialVersionUID = -975507670178838816L;

	private ReturnCodeEnum _sysReturnCode;
	private long readingValue = 0L; // long 56486
	private String readingComment = ""; // String 56487
	private String userId = ""; // String 4344
	private String equipmentLeasor = ""; // String 56525
	private long transferToFromSgt = 0L; // long 56539
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // AwhBuisnessSegmentEnum 56533
	private String equipmentTypeCode = ""; // String 56496
	private long equipmentReadingSgt = 0L; // long 56531
	private LocalDateTime readingTimestamp = null; // LocalDateTime 56485

	public long getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}
	
	public void setReadingValue(String readingValue) {
		setReadingValue(Long.parseLong(readingValue));
	}
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	public void setTransferToFromSgt(String transferToFromSgt) {
		setTransferToFromSgt(Long.parseLong(transferToFromSgt));
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}
	
	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
