package au.awh.file.equipmentreadings.edteequipmentreadings;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=edteEquipmentReadings (1879474) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsService;
// imports for DTO
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsParams;
// imports for Results
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'EdtE Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class EdteEquipmentReadingsService extends AbstractService<EdteEquipmentReadingsService, EdteEquipmentReadingsDTO>
{
	private final Step execute = define("execute", EdteEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private EdtEquipmentReadingsService edtEquipmentReadingsService;

	private final Step serviceEdtEquipmentReadings = define("serviceEdtEquipmentReadings",EdtEquipmentReadingsResult.class, this::processServiceEdtEquipmentReadings);
	
	@Autowired
	public EdteEquipmentReadingsService() {
		super(EdteEquipmentReadingsService.class, EdteEquipmentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(EdteEquipmentReadingsParams params) throws ServiceException {
		EdteEquipmentReadingsDTO dto = new EdteEquipmentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(EdteEquipmentReadingsDTO dto, EdteEquipmentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		EdtEquipmentReadingsParams edtEquipmentReadingsParams = null;
		EdtEquipmentReadingsResult edtEquipmentReadingsResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Edt Equipment Readings - Equipment Readings  *
		edtEquipmentReadingsParams = new EdtEquipmentReadingsParams();
		edtEquipmentReadingsResult = new EdtEquipmentReadingsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, edtEquipmentReadingsParams);
		edtEquipmentReadingsParams.setProgramMode(dto.getProgramMode());
		// TODO SPLIT SCREEN EXCINTFUN EquipmentReadings.edteEquipmentReadings (2) -> EDTRCD EquipmentReadings.edtEquipmentReadings
		stepResult = StepResult.callService(EdtEquipmentReadingsService.class, edtEquipmentReadingsParams).thenCall(serviceEdtEquipmentReadings);
		dto.setExitProgramOption(edtEquipmentReadingsResult.getExitProgramOption());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1879474 EquipmentReadings.edteEquipmentReadings 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		EdteEquipmentReadingsResult result = new EdteEquipmentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


    /**
     * EdtEquipmentReadingsService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEdtEquipmentReadings(EdteEquipmentReadingsDTO dto, EdtEquipmentReadingsResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, dto);
        }

        //TODO: call the continuation of the program
        EdteEquipmentReadingsParams edteEquipmentReadingsParams = new EdteEquipmentReadingsParams();
        BeanUtils.copyProperties(dto, edteEquipmentReadingsParams);
        stepResult = StepResult.callService(EdteEquipmentReadingsService.class, edteEquipmentReadingsParams);

        return stepResult;
    }

}
