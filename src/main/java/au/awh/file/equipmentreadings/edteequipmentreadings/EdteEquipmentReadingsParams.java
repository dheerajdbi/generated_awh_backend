package au.awh.file.equipmentreadings.edteequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EdteEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EdteEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = 1704620063313775366L;

    private ErrorProcessingEnum errorProcessing = null;
    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
    private ProgramModeEnum programMode = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public ProgramModeEnum getProgramMode() {
		return programMode;
	}
	
	public void setProgramMode(ProgramModeEnum programMode) {
		this.programMode = programMode;
	}
	
	public void setProgramMode(String programMode) {
		setProgramMode(ProgramModeEnum.valueOf(programMode));
	}
}
