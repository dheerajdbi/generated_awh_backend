package au.awh.file.equipmentreadings.addmbatchreadinghh;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.equipmentreadings.EquipmentReadingsRepository;


import au.awh.utils.BuildInFunctionUtils;
import com.freschelegacy.utils.DateTimeUtils;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusService;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlService;
import au.awh.file.equipmentreadings.chgeupdatereading.ChgeUpdateReadingService;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsService;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateService;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateService;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsService;
import au.awh.file.plantequipment.getebyregionbarcode.GeteByRegionBarcodeService;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusDTO;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlDTO;
import au.awh.file.equipmentreadings.chgeupdatereading.ChgeUpdateReadingDTO;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateDTO;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateDTO;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsDTO;
import au.awh.file.plantequipment.getebyregionbarcode.GeteByRegionBarcodeDTO;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusParams;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlParams;
import au.awh.file.equipmentreadings.chgeupdatereading.ChgeUpdateReadingParams;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsParams;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateParams;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateParams;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsParams;
import au.awh.file.plantequipment.getebyregionbarcode.GeteByRegionBarcodeParams;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusResult;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlResult;
import au.awh.file.equipmentreadings.chgeupdatereading.ChgeUpdateReadingResult;
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsResult;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateResult;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateResult;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsResult;
import au.awh.file.plantequipment.getebyregionbarcode.GeteByRegionBarcodeResult;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 34
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.DisplayFlagUsrEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Add:M Batch Reading HH' (WSPLTXCPVK) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service
public class AddmBatchReadingHhService extends AbstractService<AddmBatchReadingHhService, AddmBatchReadingHhState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

    
    public static final String SCREEN_PROMPT = "addmBatchReadingHh";
    public static final String SCREEN_CONFIRM_PROMPT = "AddmBatchReadingHh.key2";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",AddmBatchReadingHhParams.class, this::executeService);
    private final Step response = define("response",AddmBatchReadingHhDTO.class, this::processResponse);
//  private final Step displayConfirmPrompt = define("displayConfirmPrompt",AddmBatchReadingHhDTO.class, this::processDisplayConfirmPrompt);

	//private final Step serviceGeteEquipReadingCtrl = define("serviceGeteEquipReadingCtrl",GeteEquipReadingCtrlResult.class, this::processServiceGeteEquipReadingCtrl);
	//private final Step serviceCrteEquipmentReadings = define("serviceCrteEquipmentReadings",CrteEquipmentReadingsResult.class, this::processServiceCrteEquipmentReadings);
	//private final Step serviceGeteByRegionBarcode = define("serviceGeteByRegionBarcode",GeteByRegionBarcodeResult.class, this::processServiceGeteByRegionBarcode);
	//private final Step serviceGetePlantEquipment = define("serviceGetePlantEquipment",GetePlantEquipmentResult.class, this::processServiceGetePlantEquipment);
	//private final Step serviceGeteReadingByDate = define("serviceGeteReadingByDate",GeteReadingByDateResult.class, this::processServiceGeteReadingByDate);
	//private final Step serviceGeteByReverseDate = define("serviceGeteByReverseDate",GeteByReverseDateResult.class, this::processServiceGeteByReverseDate);
	//private final Step serviceChgeUpdateReading = define("serviceChgeUpdateReading",ChgeUpdateReadingResult.class, this::processServiceChgeUpdateReading);
	//private final Step serviceChkReadingsBatchSts = define("serviceChkReadingsBatchSts",ChkReadingsBatchStsResult.class, this::processServiceChkReadingsBatchSts);
	//private final Step serviceChgeStatus = define("serviceChgeStatus",ChgeStatusResult.class, this::processServiceChgeStatus);
	

	
    	
@Autowired
private ChgeStatusService chgeStatusService;
	
@Autowired
private ChgeUpdateReadingService chgeUpdateReadingService;
	
@Autowired
private ChkReadingsBatchStsService chkReadingsBatchStsService;
	
@Autowired
private CrteEquipmentReadingsService crteEquipmentReadingsService;
	
@Autowired
private GeteByRegionBarcodeService geteByRegionBarcodeService;
	
@Autowired
private GeteByReverseDateService geteByReverseDateService;
	
@Autowired
private GeteEquipReadingCtrlService geteEquipReadingCtrlService;
	
@Autowired
private GetePlantEquipmentService getePlantEquipmentService;
	
@Autowired
private GeteReadingByDateService geteReadingByDateService;
	
    @Autowired
    public AddmBatchReadingHhService()
    {
        super(AddmBatchReadingHhService.class, AddmBatchReadingHhState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * AddmBatchReadingHh service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(AddmBatchReadingHhState state, AddmBatchReadingHhParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * AddmBatchReadingHh service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(AddmBatchReadingHhState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(AddmBatchReadingHhState state)
    {
        StepResult stepResult = NO_ACTION;

        AddmBatchReadingHhDTO dto = new AddmBatchReadingHhDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(AddmBatchReadingHhState state, AddmBatchReadingHhDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
//    private StepResult processDisplayConfirmPrompt(AddmBatchReadingHhState state, AddmBatchReadingHhDTO dto) {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        stepResult = conductScreenConversation(state);
//
//        return stepResult;
//    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void validateScreenInput(AddmBatchReadingHhState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(AddmBatchReadingHhState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(AddmBatchReadingHhState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(AddmBatchReadingHhState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        AddmBatchReadingHhResult params = new AddmBatchReadingHhResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// * Need to remove processing for select existing record.
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1052 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            GeteEquipReadingCtrlParams geteEquipReadingCtrlParams = null;
			GeteEquipReadingCtrlResult geteEquipReadingCtrlResult = null;
			// DEBUG genFunctionCall BEGIN 1000368 ACT GetE Equip Reading Ctrl - Equipment Reading Control  *
			geteEquipReadingCtrlParams = new GeteEquipReadingCtrlParams();
			geteEquipReadingCtrlResult = new GeteEquipReadingCtrlResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteEquipReadingCtrlParams);
			geteEquipReadingCtrlParams.setErrorProcessing("2");
			geteEquipReadingCtrlParams.setGetRecordOption("");
			geteEquipReadingCtrlParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			stepResult = geteEquipReadingCtrlService.execute(geteEquipReadingCtrlParams);
			geteEquipReadingCtrlResult = (GeteEquipReadingCtrlResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteEquipReadingCtrlResult.get_SysReturnCode());
			dto.setCentreCodeKey(geteEquipReadingCtrlResult.getCentreCodeKey());
			dto.setEquipmentLeasor(geteEquipReadingCtrlResult.getEquipmentLeasor());
			dto.setAwhRegionCode(geteEquipReadingCtrlResult.getAwhRegionCode());
			dto.setReadingRequiredDate(geteEquipReadingCtrlResult.getReadingRequiredDate());
			dto.setLclEquipReadingCtrlSts(geteEquipReadingCtrlResult.getEquipReadingCtrlSts());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000808 ACT LCL.Reading Required Date = DTL.Reading Required Date + CON.-1 *DAYS
			dto.setLclReadingRequiredDate(BuildInFunctionUtils.getDateIncrement(
			dto.getReadingRequiredDate(), -1, "DY", "1111111", null, Boolean.FALSE, Boolean.FALSE));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000631 ACT LCL.Transfer Function = CND.No
			dto.setLclTransferFunction(YesOrNoEnum._NO);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000645 ACT DTL.Display Flag 1        USR = CND.NO
			dto.setDisplayFlag1Usr(DisplayFlagUsrEnum._NO);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000651 ACT DTL.HH Entity 1 = CON.Scan Equip Barcode
			dto.setHhEntity1("Scan Equip Barcode");
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000330 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            GeteByReverseDateParams geteByReverseDateParams = null;
			GeteByReverseDateResult geteByReverseDateResult = null;
			ChkReadingsBatchStsResult chkReadingsBatchStsResult = null;
			ChgeStatusParams chgeStatusParams = null;
			GeteReadingByDateParams geteReadingByDateParams = null;
			GeteByRegionBarcodeResult geteByRegionBarcodeResult = null;
			ChgeStatusResult chgeStatusResult = null;
			GeteByRegionBarcodeParams geteByRegionBarcodeParams = null;
			GetePlantEquipmentResult getePlantEquipmentResult = null;
			ChkReadingsBatchStsParams chkReadingsBatchStsParams = null;
			GeteReadingByDateResult geteReadingByDateResult = null;
			GetePlantEquipmentParams getePlantEquipmentParams = null;
			if (dto.getDisplayFlag1Usr() == DisplayFlagUsrEnum._NO) {
				// DTL.Display Flag 1        USR is NO
				if (dto.getEquipmentBarcode() != null && dto.getEquipmentBarcode().isEmpty()) {
					// DTL.Equipment Barcode is Not entered
					// DEBUG genFunctionCall BEGIN 1000964 ACT Chk Readings Batch Sts - Plant Equipment  *
					chkReadingsBatchStsParams = new ChkReadingsBatchStsParams();
					chkReadingsBatchStsResult = new ChkReadingsBatchStsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, chkReadingsBatchStsParams);
					chkReadingsBatchStsParams.setAwhRegionCode(dto.getAwhRegionCode());
					chkReadingsBatchStsParams.setCentreCodeKey(dto.getCentreCodeKey());
					chkReadingsBatchStsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
					stepResult = chkReadingsBatchStsService.execute(chkReadingsBatchStsParams);
					chkReadingsBatchStsResult = (ChkReadingsBatchStsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(chkReadingsBatchStsResult.get_SysReturnCode());
					dto.setLclEquipReadingCtrlSts(chkReadingsBatchStsResult.getEquipReadingCtrlSts());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000974 ACT ChgE Status - Equipment Reading Control  *
					chgeStatusParams = new ChgeStatusParams();
					chgeStatusResult = new ChgeStatusResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, chgeStatusParams);
					chgeStatusParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
					chgeStatusParams.setEquipReadingCtrlSts(dto.getLclEquipReadingCtrlSts());
					chgeStatusParams.setErrorProcessing("2");
					stepResult = chgeStatusService.execute(chgeStatusParams);
					chgeStatusResult = (ChgeStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(chgeStatusResult.get_SysReturnCode());
					// DEBUG genFunctionCall END
					if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
						// PGM.*Return code is *Normal
						// DEBUG genFunctionCall BEGIN 1001033 ACT Commit .
						// TODO: Unsupported Internal Function Type '*COMMIT' (message surrogate = 1001566)
						// DEBUG genFunctionCall END
					} else {
						// *OTHERWISE
						// * Master functions perform implicit Rollback. No need for it here.
						// DEBUG genFunctionCall BEGIN 1001079 ACT Send completion message - 'Updates cancelled'
						//dto.addMessage("", "updates.cancelled", messageSource);
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1000665 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// * Try to get equipmet using bacode othewise assume its equipment code
					// DEBUG genFunctionCall BEGIN 1000686 ACT GetE By Region Barcode - Plant Equipment  *
					geteByRegionBarcodeParams = new GeteByRegionBarcodeParams();
					geteByRegionBarcodeResult = new GeteByRegionBarcodeResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, geteByRegionBarcodeParams);
					geteByRegionBarcodeParams.setAwhRegionCode(dto.getAwhRegionCode());
					geteByRegionBarcodeParams.setErrorProcessing("2");
					geteByRegionBarcodeParams.setGetRecordOption("I");
					geteByRegionBarcodeParams.setEquipmentBarcode(dto.getEquipmentBarcode());
					stepResult = geteByRegionBarcodeService.execute(geteByRegionBarcodeParams);
					geteByRegionBarcodeResult = (GeteByRegionBarcodeResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(geteByRegionBarcodeResult.get_SysReturnCode());
					dto.setAwhBuisnessSegment(geteByRegionBarcodeResult.getAwhBuisnessSegment());
					dto.setLclCentreCodeKey(geteByRegionBarcodeResult.getCentreCodeKey());
					dto.setEquipmentDescription(geteByRegionBarcodeResult.getEquipmentDescription());
					dto.setEquipmentTypeCode(geteByRegionBarcodeResult.getEquipmentTypeCode());
					dto.setEquipReadingUnit(geteByRegionBarcodeResult.getEquipReadingUnit());
					dto.setLclEquipmentLeasor(geteByRegionBarcodeResult.getEquipmentLeasor());
					dto.setLclEquipmentStatus(geteByRegionBarcodeResult.getEquipmentStatus());
					dto.setPlantEquipmentCode(geteByRegionBarcodeResult.getPlantEquipmentCode());
					dto.setLclEquipmentSerialNumber(geteByRegionBarcodeResult.getEquipmentSerialNumber());
					// DEBUG genFunctionCall END
					if (dto.getPlantEquipmentCode() != null && dto.getPlantEquipmentCode().isEmpty()) {
						// DTL.Plant Equipment Code is Not Entered
						// DEBUG genFunctionCall BEGIN 1000706 ACT GetE Plant Equipment - Plant Equipment  *
						getePlantEquipmentParams = new GetePlantEquipmentParams();
						getePlantEquipmentResult = new GetePlantEquipmentResult();
						// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
						BeanUtils.copyProperties(dto, getePlantEquipmentParams);
						getePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
						getePlantEquipmentParams.setPlantEquipmentCode(dto.getEquipmentBarcode());
						getePlantEquipmentParams.setErrorProcessing("2");
						getePlantEquipmentParams.setGetRecordOption("");
						stepResult = getePlantEquipmentService.execute(getePlantEquipmentParams);
						getePlantEquipmentResult = (GetePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
						dto.set_SysReturnCode(getePlantEquipmentResult.get_SysReturnCode());
						dto.setAwhBuisnessSegment(getePlantEquipmentResult.getAwhBuisnessSegment());
						dto.setLclCentreCodeKey(getePlantEquipmentResult.getCentreCodeKey());
						dto.setEquipmentDescription(getePlantEquipmentResult.getEquipmentDescription());
						dto.setEquipmentTypeCode(getePlantEquipmentResult.getEquipmentTypeCode());
						dto.setEquipReadingUnit(getePlantEquipmentResult.getEquipReadingUnit());
						dto.setLclEquipmentLeasor(getePlantEquipmentResult.getEquipmentLeasor());
						dto.setLclEquipmentStatus(getePlantEquipmentResult.getEquipmentStatus());
						dto.setLclEquipmentSerialNumber(getePlantEquipmentResult.getEquipmentSerialNumber());
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1000722 ACT DTL.Plant Equipment Code = DTL.Equipment Barcode
						dto.setPlantEquipmentCode(dto.getEquipmentBarcode());
						// DEBUG genFunctionCall END
					}
				}
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				!dto.getCentreCodeKey().equals(dto.getLclCentreCodeKey())
				// DEBUG ComparatorJavaGenerator END
				) {
					// DTL.Centre Code           KEY NE LCL.Centre Code           KEY
					// DEBUG genFunctionCall BEGIN 1000732 ACT Send error message - 'Equip for wrng Ctr'
					//dto.addMessage("centreCodeKey", "equip.for.wrng.ctr", messageSource);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000744 ACT DTL.Display Flag 1        USR = CND.YES
				dto.setDisplayFlag1Usr(DisplayFlagUsrEnum._YES);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000750 ACT GetE Reading by Date - Equipment Readings  *
				geteReadingByDateParams = new GeteReadingByDateParams();
				geteReadingByDateResult = new GeteReadingByDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteReadingByDateParams);
				geteReadingByDateParams.setAwhRegionCode(dto.getAwhRegionCode());
				geteReadingByDateParams.setCentreCodeKey(dto.getCentreCodeKey());
				geteReadingByDateParams.setReadingType("ST");
				geteReadingByDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				geteReadingByDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				geteReadingByDateParams.setErrorProcessing("2");
				geteReadingByDateParams.setGetRecordOption("I");
				stepResult = geteReadingByDateService.execute(geteReadingByDateParams);
				geteReadingByDateResult = (GeteReadingByDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteReadingByDateResult.get_SysReturnCode());
				dto.setEquipmentReadingSgt(geteReadingByDateResult.getEquipmentReadingSgt());
				dto.setReadingTimestamp(geteReadingByDateResult.getReadingTimestamp());
				dto.setReadingValue(geteReadingByDateResult.getReadingValue());
				dto.setHhEntity1(geteReadingByDateResult.getReadingComment());
				dto.setUnavailableDaysFromPrv(geteReadingByDateResult.getUnavailableDaysFromPrv());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1001137 ACT LCL.Reading Type = CND.Standard Reading
				dto.setLclReadingType(ReadingTypeEnum._STANDARD_READING);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000790 ACT GetE By reverse date - Equipment Readings  *
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(dto.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				geteByReverseDateParams.setErrorProcessing("2");
				geteByReverseDateParams.setGetRecordOption("I");
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				dto.setLclEquipmentReadingSgt(geteByReverseDateResult.getEquipmentReadingSgt());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				dto.setPreviousReadingValue(geteByReverseDateResult.getReadingValue());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000924 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
				// TODO: NEED TO SUPPORT *FIELD Equipment Type Desc Drv  *FIELD                                             DTLE 1000924
				// DEBUG X2EActionDiagramElement 1000924 ACT     Equipment Type Desc Drv  *FIELD                                             DTLE
				// DEBUG X2EActionDiagramElement 1000925 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000926 PAR DTL 
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000928 ACT DTL.Condition Name = Condition name of DTL.AWH Buisness Segment
				dto.setConditionName(dto.getAwhBuisnessSegment().getDescription());
				// Retrieve condition
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000934 ACT *Set Cursor: DTL.Reading Value  (*Override=*NO)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000853 ACT PGM.*Defer confirm = CND.Defer confirm
				dto.set_SysDeferConfirm(DeferConfirmEnum._DEFER_CONFIRM);
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				if (dto.getReadingValue() == BigDecimal.ZERO) {
					// DTL.Reading Value is Not entered
					// DEBUG genFunctionCall BEGIN 1000831 ACT DTL.Display Flag 1        USR = CND.NO
					dto.setDisplayFlag1Usr(DisplayFlagUsrEnum._NO);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000917 ACT PGM.*Defer confirm = CND.Defer confirm
					dto.set_SysDeferConfirm(DeferConfirmEnum._DEFER_CONFIRM);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000837 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					if ((dto.getPreviousReadingValue() != BigDecimal.ZERO) && (
					// DEBUG ComparatorJavaGenerator BEGIN
					dto.getReadingValue().intValue() < dto.getPreviousReadingValue().intValue()
					// DEBUG ComparatorJavaGenerator END
					)) {
						// DEBUG genFunctionCall BEGIN 1000849 ACT Send error message - 'Reading must be more prev'
						//dto.addMessage("readingValue", "reading.must.be.more.prev", messageSource);
						// DEBUG genFunctionCall END
					} else if ((dto.getEquipReadingUnit() == EquipReadingUnitEnum._HOURS) && (dto.getPreviousReadingValue() != BigDecimal.ZERO)) {
						// DEBUG genFunctionCall BEGIN 1001116 ACT LCL.Reading Value = DTL.Reading Value - DTL.Previous Reading Value
						dto.setLclReadingValue(dto.getReadingValue().subtract(dto.getPreviousReadingValue()));
						// DEBUG genFunctionCall END
						if (dto.getLclReadingValue().intValue() > 250) {
							// LCL.Reading Value is Usage > 250 hrs
							// DEBUG genFunctionCall BEGIN 1001124 ACT Send information message - 'Usage is > 250 hrs'
							//dto.addMessage("readingValue", "usage.is.>.250.hrs", messageSource);
							// DEBUG genFunctionCall END
						}
					}
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            CrteEquipmentReadingsResult crteEquipmentReadingsResult = null;
			CrteEquipmentReadingsParams crteEquipmentReadingsParams = null;
			ChgeUpdateReadingResult chgeUpdateReadingResult = null;
			ChgeUpdateReadingParams chgeUpdateReadingParams = null;
			// DEBUG genFunctionCall BEGIN 1000565 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
			if (dto.getEquipmentReadingSgt() != BigDecimal.ZERO) {
				// DTL.Equipment Reading Sgt is Entered
				// DEBUG genFunctionCall BEGIN 1000771 ACT ChgE Update reading - Equipment Readings  *
				chgeUpdateReadingParams = new ChgeUpdateReadingParams();
				chgeUpdateReadingResult = new ChgeUpdateReadingResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, chgeUpdateReadingParams);
				chgeUpdateReadingParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
				chgeUpdateReadingParams.setAwhRegionCode(dto.getAwhRegionCode());
				chgeUpdateReadingParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				chgeUpdateReadingParams.setReadingTimestamp(job.getSystemTimestamp());
				chgeUpdateReadingParams.setReadingType("ST");
				chgeUpdateReadingParams.setReadingValue(dto.getReadingValue());
				chgeUpdateReadingParams.setReadingComment(dto.getHhEntity1());
				chgeUpdateReadingParams.setUserId(job.getUser());
				chgeUpdateReadingParams.setEquipmentLeasor(dto.getEquipmentLeasor());
				chgeUpdateReadingParams.setTransferToFromSgt(dto.getTransferToFromSgt());
				chgeUpdateReadingParams.setCentreCodeKey(dto.getCentreCodeKey());
				chgeUpdateReadingParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
				chgeUpdateReadingParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
				chgeUpdateReadingParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				chgeUpdateReadingParams.setErrorProcessing("2");
				stepResult = chgeUpdateReadingService.execute(chgeUpdateReadingParams);
				chgeUpdateReadingResult = (ChgeUpdateReadingResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgeUpdateReadingResult.get_SysReturnCode());
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000484 ACT CrtE Equipment Readings - Equipment Readings  *
				crteEquipmentReadingsParams = new CrteEquipmentReadingsParams();
				crteEquipmentReadingsResult = new CrteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, crteEquipmentReadingsParams);
				crteEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
				crteEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
				crteEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				crteEquipmentReadingsParams.setReadingType("ST");
				crteEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
				crteEquipmentReadingsParams.setReadingComment(dto.getHhEntity1());
				crteEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
				crteEquipmentReadingsParams.setTransferToFromSgt(BigDecimal.ZERO);
				crteEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
				crteEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
				crteEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
				crteEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				crteEquipmentReadingsParams.setErrorProcessing("2");
				stepResult = crteEquipmentReadingsService.execute(crteEquipmentReadingsParams);
				crteEquipmentReadingsResult = (CrteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(crteEquipmentReadingsResult.get_SysReturnCode());
				dto.setEquipmentReadingSgt(crteEquipmentReadingsResult.getEquipmentReadingSgt());
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000553 ACT Commit .
				// TODO: Unsupported Internal Function Type '*COMMIT' (message surrogate = 1001566)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000897 ACT DTL.Equipment Reading Sgt = CND.Not Entered
				dto.setEquipmentReadingSgt(BigDecimal.ZERO);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000859 ACT DTL.HH Entity 1 = Rtvmsg('Equipment Updated')
				dto.setHhEntity1(messageSource.getMessage("equipment.updated", new Object[] { "", dto.getPlantEquipmentCode(), dto.getReadingValue() }, "Default", null));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000864 ACT DTL.Equipment Barcode = CND.Not entered
				dto.setEquipmentBarcode("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000879 ACT DTL.Reading Value = CND.Not entered
				dto.setReadingValue(BigDecimal.ZERO);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000870 ACT DTL.Previous Reading Value = CND.Not entered
				dto.setPreviousReadingValue(BigDecimal.ZERO);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000909 ACT DTL.Equipment Description = CND.Not entered
				dto.setEquipmentDescription("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000940 ACT DTL.Plant Equipment Code = CND.Not Entered
				dto.setPlantEquipmentCode("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000949 ACT DTL.Equipment Type Desc Drv = CND.Not Entered
				dto.setEquipmentTypeDescDrv("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000903 ACT DTL.Condition Name = CND.Not entered
				dto.setConditionName("");
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000891 ACT DTL.Display Flag 1        USR = CND.NO
				dto.setDisplayFlag1Usr(DisplayFlagUsrEnum._NO);
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// * Master functions perform implicit Rollback. No need for it here.
				// DEBUG genFunctionCall BEGIN 1000561 ACT Send completion message - 'Updates cancelled'
				//dto.addMessage("", "updates.cancelled", messageSource);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(AddmBatchReadingHhState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000247 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GeteEquipReadingCtrlService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipReadingCtrl(AddmBatchReadingHhState state, GeteEquipReadingCtrlParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * CrteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCrteEquipmentReadings(AddmBatchReadingHhState state, CrteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteByRegionBarcodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteByRegionBarcode(AddmBatchReadingHhState state, GeteByRegionBarcodeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetePlantEquipment(AddmBatchReadingHhState state, GetePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteReadingByDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteReadingByDate(AddmBatchReadingHhState state, GeteReadingByDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteByReverseDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteByReverseDate(AddmBatchReadingHhState state, GeteByReverseDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeUpdateReadingService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeUpdateReading(AddmBatchReadingHhState state, ChgeUpdateReadingParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChkReadingsBatchStsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChkReadingsBatchSts(AddmBatchReadingHhState state, ChkReadingsBatchStsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeStatusService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeStatus(AddmBatchReadingHhState state, ChgeStatusParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddmBatchReadingHhParams addmBatchReadingHhParams = new AddmBatchReadingHhParams();
//        BeanUtils.copyProperties(state, addmBatchReadingHhParams);
//        stepResult = StepResult.callService(AddmBatchReadingHhService.class, addmBatchReadingHhParams);
//
//        return stepResult;
//    }
//


}
