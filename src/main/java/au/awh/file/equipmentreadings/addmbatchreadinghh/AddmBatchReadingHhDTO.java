package au.awh.file.equipmentreadings.addmbatchreadinghh;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;


import org.springframework.beans.BeanUtils;

import au.awh.file.equipmentreadings.EquipmentReadings;
// generateStatusFieldImportStatements BEGIN 35
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.DisplayFlagUsrEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Readings' (WSEQREAD) and function 'Add:M Batch Reading HH' (WSPLTXCPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmBatchReadingHhDTO extends BaseDTO {
	private static final long serialVersionUID = 5686615618961099939L;

// DEBUG fields
	private BigDecimal equipReadingBatchNo = BigDecimal.ZERO; // 56572
	private LocalDate readingRequiredDate = null; // 56555
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO; // 60183
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO; // 60184
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO; // 60185
	private String equipmentBarcode = ""; // 56473
	private DisplayFlagUsrEnum displayFlag1Usr = null; // 9487
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO; // 56531
	private String awhRegionCode = ""; // 56452
	private String plantEquipmentCode = ""; // 56471
	private String equipmentDescription = ""; // 56472
	private String equipmentTypeDescDrv = ""; // 56549
	private String equipmentTypeCode = ""; // 56496
	private String centreCodeKey = ""; // 1140
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // 56533
	private String conditionName = ""; // 29897
	private String hhEntity1 = ""; // 36645
	private LocalDateTime readingTimestamp = null; // 56485
	private ReadingTypeEnum readingType = null; // 56488
	private BigDecimal readingValue = BigDecimal.ZERO; // 56486
	private EquipReadingUnitEnum equipReadingUnit = null; // 56498
	private BigDecimal previousReadingValue = BigDecimal.ZERO; // 56560
	private String readingComment = ""; // 56487
	private String userId = ""; // 4344
	private String equipmentLeasor = ""; // 56525
	private BigDecimal transferToFromSgt = BigDecimal.ZERO; // 56539
	private String transferToCentre = ""; // 56579
	private AwhBuisnessSegmentEnum transferToBuisSeg = null; // 56580
// DEBUG detail fields
// DEBUG param
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; //0
// DEBUG local fields
	private String lclCentreCodeKey = ""; // 1140
	private BigDecimal lclReadingValue = BigDecimal.ZERO; // 56486
	private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED; // 56488
	private String lclEquipmentLeasor = ""; // 56525
	private BigDecimal lclEquipmentReadingSgt = BigDecimal.ZERO; // 56531
	private LocalDate lclReadingRequiredDate = null; // 56555
	private EquipReadingCtrlStsEnum lclEquipReadingCtrlSts = null; // 56566
	private YesOrNoEnum lclTransferFunction = YesOrNoEnum._NOT_ENTERED; // 56581
	private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum._NOT_ENTERED; // 56583
	private String lclEquipmentSerialNumber = ""; // 60161
// DEBUG program fields
	private ReturnCodeEnum returnCode = ReturnCodeEnum._STA_NORMAL; // 149
	private DeferConfirmEnum deferConfirm = null; // 237
// DEBUG Constructor

	public AddmBatchReadingHhDTO() {

	}

	public AddmBatchReadingHhDTO(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(equipmentReadings, this);
	}

	public AddmBatchReadingHhDTO(BigDecimal equipReadingBatchNo, LocalDate readingRequiredDate, BigDecimal differenceFromPrevious, BigDecimal idleDaysFromPrevious, BigDecimal unavailableDaysFromPrv, String equipmentBarcode, BigDecimal equipmentReadingSgt, String awhRegionCode, String plantEquipmentCode, String equipmentDescription, String equipmentTypeCode, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, LocalDateTime readingTimestamp, ReadingTypeEnum readingType, BigDecimal readingValue, EquipReadingUnitEnum equipReadingUnit, String readingComment, String userId, String equipmentLeasor, BigDecimal transferToFromSgt) {
		this.equipReadingBatchNo = equipReadingBatchNo;
		this.readingRequiredDate = readingRequiredDate;
		this.differenceFromPrevious = differenceFromPrevious;
		this.idleDaysFromPrevious = idleDaysFromPrevious;
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
		this.equipmentBarcode = equipmentBarcode;
		this.equipmentReadingSgt = equipmentReadingSgt;
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentDescription = equipmentDescription;
		this.equipmentTypeCode = equipmentTypeCode;
		this.centreCodeKey = centreCodeKey;
		this.awhBuisnessSegment = awhBuisnessSegment;
		this.readingTimestamp = readingTimestamp;
		this.readingType = readingType;
		this.readingValue = readingValue;
		this.equipReadingUnit = equipReadingUnit;
		this.readingComment = readingComment;
		this.userId = userId;
		this.equipmentLeasor = equipmentLeasor;
		this.transferToFromSgt = transferToFromSgt;
	}

	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}

	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}

	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}

	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}

	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}

	public void setDisplayFlag1Usr(DisplayFlagUsrEnum displayFlag1Usr) {
		this.displayFlag1Usr = displayFlag1Usr;
	}

	public DisplayFlagUsrEnum getDisplayFlag1Usr() {
		return displayFlag1Usr;
	}

	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
		this.equipmentTypeDescDrv = equipmentTypeDescDrv;
	}

	public String getEquipmentTypeDescDrv() {
		return equipmentTypeDescDrv;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public String getConditionName() {
		return conditionName;
	}

	public void setHhEntity1(String hhEntity1) {
		this.hhEntity1 = hhEntity1;
	}

	public String getHhEntity1() {
		return hhEntity1;
	}

	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}

	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}

	public BigDecimal getReadingValue() {
		return readingValue;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public void setPreviousReadingValue(BigDecimal previousReadingValue) {
		this.previousReadingValue = previousReadingValue;
	}

	public BigDecimal getPreviousReadingValue() {
		return previousReadingValue;
	}

	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}

	public String getReadingComment() {
		return readingComment;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}

	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}

	public void setTransferToCentre(String transferToCentre) {
		this.transferToCentre = transferToCentre;
	}

	public String getTransferToCentre() {
		return transferToCentre;
	}

	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
		this.transferToBuisSeg = transferToBuisSeg;
	}

	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public void setLclCentreCodeKey(String centreCodeKey) {
		this.lclCentreCodeKey = centreCodeKey;
	}

	public String getLclCentreCodeKey() {
		return lclCentreCodeKey;
	}

	public void setLclReadingValue(BigDecimal readingValue) {
		this.lclReadingValue = readingValue;
	}

	public BigDecimal getLclReadingValue() {
		return lclReadingValue;
	}

	public void setLclReadingType(ReadingTypeEnum readingType) {
		this.lclReadingType = readingType;
	}

	public ReadingTypeEnum getLclReadingType() {
		return lclReadingType;
	}

	public void setLclEquipmentLeasor(String equipmentLeasor) {
		this.lclEquipmentLeasor = equipmentLeasor;
	}

	public String getLclEquipmentLeasor() {
		return lclEquipmentLeasor;
	}

	public void setLclEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.lclEquipmentReadingSgt = equipmentReadingSgt;
	}

	public BigDecimal getLclEquipmentReadingSgt() {
		return lclEquipmentReadingSgt;
	}

	public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
		this.lclReadingRequiredDate = readingRequiredDate;
	}

	public LocalDate getLclReadingRequiredDate() {
		return lclReadingRequiredDate;
	}

	public void setLclEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.lclEquipReadingCtrlSts = equipReadingCtrlSts;
	}

	public EquipReadingCtrlStsEnum getLclEquipReadingCtrlSts() {
		return lclEquipReadingCtrlSts;
	}

	public void setLclTransferFunction(YesOrNoEnum transferFunction) {
		this.lclTransferFunction = transferFunction;
	}

	public YesOrNoEnum getLclTransferFunction() {
		return lclTransferFunction;
	}

	public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.lclEquipmentStatus = equipmentStatus;
	}

	public EquipmentStatusEnum getLclEquipmentStatus() {
		return lclEquipmentStatus;
	}

	public void setLclEquipmentSerialNumber(String equipmentSerialNumber) {
		this.lclEquipmentSerialNumber = equipmentSerialNumber;
	}

	public String getLclEquipmentSerialNumber() {
		return lclEquipmentSerialNumber;
	}

	public void setReturnCode(ReturnCodeEnum returnCode) {
		this.returnCode = returnCode;
	}

	public ReturnCodeEnum getReturnCode() {
		return returnCode;
	}
	public void setDeferConfirm(DeferConfirmEnum deferConfirm) {
		this.deferConfirm = deferConfirm;
	}

	public DeferConfirmEnum getDeferConfirm() {
		return deferConfirm;
	}
    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setDtoFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(equipmentReadings, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setEntityFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(this, equipmentReadings);
    }
}
