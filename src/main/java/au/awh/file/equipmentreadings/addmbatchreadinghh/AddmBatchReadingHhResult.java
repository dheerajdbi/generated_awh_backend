package au.awh.file.equipmentreadings.addmbatchreadinghh;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: AddmBatchReadingHh (WSPLTXCPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmBatchReadingHhResult implements Serializable
{
	private static final long serialVersionUID = -454992521490445298L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
