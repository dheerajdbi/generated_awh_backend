package au.awh.file.equipmentreadings.addmbatchreadinghh;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: AddmBatchReadingHh (WSPLTXCPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmBatchReadingHhParams implements Serializable
{
	private static final long serialVersionUID = 8200347623032045901L;

	private long equipReadingBatchNo = 0L;

	public long getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(String equipReadingBatchNo) {
		setEquipReadingBatchNo(Long.parseLong(equipReadingBatchNo));
	}
}
