package au.awh.file.equipmentreadings.addmbatchreadinghh;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 11
import au.awh.model.DeferConfirmEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Add:M Batch Reading HH' (WSPLTXCPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddmBatchReadingHhState extends AddmBatchReadingHhDTO {
    private static final long serialVersionUID = -8654432304159963556L;

    // Local fields
    private String lclCentreCodeKey = "";
    private EquipReadingCtrlStsEnum lclEquipReadingCtrlSts = null;
    private String lclEquipmentLeasor = "";
    private BigDecimal lclEquipmentReadingSgt = BigDecimal.ZERO;
    private String lclEquipmentSerialNumber = "";
    private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum._NOT_ENTERED;
    private LocalDate lclReadingRequiredDate = null;
    private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED;
    private BigDecimal lclReadingValue = BigDecimal.ZERO;
    private YesOrNoEnum lclTransferFunction = YesOrNoEnum._NOT_ENTERED;

    // System fields
    private DeferConfirmEnum _sysDeferConfirm = null;
    private boolean _sysErrorFound = false;

    public AddmBatchReadingHhState() {
    }

    public AddmBatchReadingHhState(EquipmentReadings equipmentReadings) {
        setDtoFields(equipmentReadings);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclCentreCodeKey(String centreCodeKey) {
        this.lclCentreCodeKey = centreCodeKey;
    }

    public String getLclCentreCodeKey() {
        return lclCentreCodeKey;
    }
    public void setLclEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
        this.lclEquipReadingCtrlSts = equipReadingCtrlSts;
    }

    public EquipReadingCtrlStsEnum getLclEquipReadingCtrlSts() {
        return lclEquipReadingCtrlSts;
    }
    public void setLclEquipmentLeasor(String equipmentLeasor) {
        this.lclEquipmentLeasor = equipmentLeasor;
    }

    public String getLclEquipmentLeasor() {
        return lclEquipmentLeasor;
    }
    public void setLclEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
        this.lclEquipmentReadingSgt = equipmentReadingSgt;
    }

    public BigDecimal getLclEquipmentReadingSgt() {
        return lclEquipmentReadingSgt;
    }
    public void setLclEquipmentSerialNumber(String equipmentSerialNumber) {
        this.lclEquipmentSerialNumber = equipmentSerialNumber;
    }

    public String getLclEquipmentSerialNumber() {
        return lclEquipmentSerialNumber;
    }
    public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
        this.lclEquipmentStatus = equipmentStatus;
    }

    public EquipmentStatusEnum getLclEquipmentStatus() {
        return lclEquipmentStatus;
    }
    public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
        this.lclReadingRequiredDate = readingRequiredDate;
    }

    public LocalDate getLclReadingRequiredDate() {
        return lclReadingRequiredDate;
    }
    public void setLclReadingType(ReadingTypeEnum readingType) {
        this.lclReadingType = readingType;
    }

    public ReadingTypeEnum getLclReadingType() {
        return lclReadingType;
    }
    public void setLclReadingValue(BigDecimal readingValue) {
        this.lclReadingValue = readingValue;
    }

    public BigDecimal getLclReadingValue() {
        return lclReadingValue;
    }
    public void setLclTransferFunction(YesOrNoEnum transferFunction) {
        this.lclTransferFunction = transferFunction;
    }

    public YesOrNoEnum getLclTransferFunction() {
        return lclTransferFunction;
    }

    public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
        _sysDeferConfirm = deferConfirm;
    }

    public DeferConfirmEnum get_SysDeferConfirm() {
        return _sysDeferConfirm;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
