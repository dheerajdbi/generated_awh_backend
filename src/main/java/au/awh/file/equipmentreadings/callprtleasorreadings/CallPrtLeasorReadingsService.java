package au.awh.file.equipmentreadings.callprtleasorreadings;

// Function Stub for EXCUSRPGM 1880117
// EquipmentReadings.callPrtLeasorReadings

// Parameters:
// I Field 56452 awhRegionCode String(1) "AWH Region Code"
// I Field 1140 centreCodeKey String(2) "Centre Code           KEY"
// I Field 56555 readingRequiredDate LocalDate "Reading Required Date"
// I Field 56525 equipmentLeasor String(5) Ref 37588 organisation "Equipment Leasor"
// I Field 34671 outputQueue String(10) "Output Queue"
// I Field 36415 outputQueueLibrary String(10) Ref 31632 storeLocationOutqlib "Output Queue Library"
// I Field 9040 copies String(2) "Copies"
// I Field 9043 forms FormsEnum "Forms"
// I Field 9078 holdClFormatyesno HoldClFormatyesnoEnum "Hold (CL Format *YES/*NO)"
// I Field 9079 saveClFormatyesno SaveClFormatyesnoEnum "Save (CL Format *YES/*NO)"
// I Field 42734 sendViaEmail SendViaPrintedReportEnum Ref 42733 sendViaPrintedReport "Send via Email"
// I Field 41091 dateToIso LocalDate Ref 37852 programDate "Date To ISO"

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;


/**
 * EXCUSRPGM Service for 'Call Prt Leasor Readings' (file 'Equipment Readings'
 *
 * @author X2EGenerator  FunctionStub.kt
 */
@Service
public class CallPrtLeasorReadingsService {

	public StepResult execute(CallPrtLeasorReadingsParams callPrtLeasorReadingsParams) {
		// TODO: Code generation is not currently supported for this function
		return NO_ACTION;
	}
}
