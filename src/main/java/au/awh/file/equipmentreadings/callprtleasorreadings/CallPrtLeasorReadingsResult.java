package au.awh.file.equipmentreadings.callprtleasorreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CallPrtLeasorReadings (WSPLTW5UPC).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CallPrtLeasorReadingsResult implements Serializable
{
	private static final long serialVersionUID = -9188387179786712576L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
