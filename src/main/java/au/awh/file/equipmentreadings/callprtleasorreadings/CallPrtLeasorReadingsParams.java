package au.awh.file.equipmentreadings.callprtleasorreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 12
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CallPrtLeasorReadings (WSPLTW5UPC).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CallPrtLeasorReadingsParams implements Serializable
{
	private static final long serialVersionUID = -5589973041077531228L;

    private String awhRegionCode = "";
    private String centreCodeKey = "";
    private LocalDate readingRequiredDate = null;
    private String equipmentLeasor = "";
    private String outputQueue = "";
    private String outputQueueLibrary = "";
    private String copies = "";
    private FormsEnum forms = null;
    private HoldClFormatyesnoEnum holdClFormatyesno = null;
    private SaveClFormatyesnoEnum saveClFormatyesno = null;
    private SendViaPrintedReportEnum sendViaEmail = null;
    private LocalDate dateToIso = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public String getOutputQueue() {
		return outputQueue;
	}
	
	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}
	
	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}
	
	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}
	
	public String getCopies() {
		return copies;
	}
	
	public void setCopies(String copies) {
		this.copies = copies;
	}
	
	public FormsEnum getForms() {
		return forms;
	}
	
	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}
	
	public void setForms(String forms) {
		setForms(FormsEnum.valueOf(forms));
	}
	
	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(String holdClFormatyesno) {
		setHoldClFormatyesno(HoldClFormatyesnoEnum.valueOf(holdClFormatyesno));
	}
	
	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(String saveClFormatyesno) {
		setSaveClFormatyesno(SaveClFormatyesnoEnum.valueOf(saveClFormatyesno));
	}
	
	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}
	
	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}
	
	public void setSendViaEmail(String sendViaEmail) {
		setSendViaEmail(SendViaPrintedReportEnum.valueOf(sendViaEmail));
	}
	
	public LocalDate getDateToIso() {
		return dateToIso;
	}
	
	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
	}
	
	public void setDateToIso(String dateToIso) {
		setDateToIso(new LocalDateConverter().convert(dateToIso));
	}
}
