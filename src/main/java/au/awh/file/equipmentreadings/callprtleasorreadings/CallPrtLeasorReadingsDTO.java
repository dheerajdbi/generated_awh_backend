package au.awh.file.equipmentreadings.callprtleasorreadings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CallPrtLeasorReadingsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private FormsEnum forms;
	private HoldClFormatyesnoEnum holdClFormatyesno;
	private LocalDate dateToIso;
	private LocalDate readingRequiredDate;
	private SaveClFormatyesnoEnum saveClFormatyesno;
	private SendViaPrintedReportEnum sendViaEmail;
	private String awhRegionCode;
	private String centreCodeKey;
	private String copies;
	private String equipmentLeasor;
	private String outputQueue;
	private String outputQueueLibrary;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getCopies() {
		return copies;
	}

	public LocalDate getDateToIso() {
		return dateToIso;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}

}
