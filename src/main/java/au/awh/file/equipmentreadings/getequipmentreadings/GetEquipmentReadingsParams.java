package au.awh.file.equipmentreadings.getequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = 6956420151311026099L;

    private long equipmentReadingSgt = 0L;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
}
