package au.awh.file.equipmentreadings.getequipmentreadings;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator
 */
@Service
public class GetEquipmentReadingsService extends AbstractService<GetEquipmentReadingsService, GetEquipmentReadingsDTO>
{
    private final Step execute = define("execute", GetEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	

    @Autowired
    public GetEquipmentReadingsService()
    {
        super(GetEquipmentReadingsService.class, GetEquipmentReadingsDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetEquipmentReadingsParams params) throws ServiceException {
        GetEquipmentReadingsDTO dto = new GetEquipmentReadingsDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetEquipmentReadingsDTO dto, GetEquipmentReadingsParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<EquipmentReadings> equipmentReadingsList = equipmentReadingsRepository.findAllBy();
		if (!equipmentReadingsList.isEmpty()) {
			for (EquipmentReadings equipmentReadings : equipmentReadingsList) {
				processDataRecord(dto, equipmentReadings);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetEquipmentReadingsResult result = new GetEquipmentReadingsResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetEquipmentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END
		// * Assume this is a FIFO partial key access.
		// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetEquipmentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
		dto.setAwhRegionCode("");
		dto.setPlantEquipmentCode("");
		dto.setReadingTimestamp(LocalDateTime.of(1801, 1, 1, 0, 0));
		dto.setReadingType(ReadingTypeEnum._NOT_ENTERED);
		dto.setReadingValue(0L);
		dto.setReadingComment("");
		dto.setUserId("");
		dto.setEquipmentLeasor("");
		dto.setTransferToFromSgt(0L);
		dto.setCentreCodeKey("");
		dto.setAwhBuisnessSegment(null);
		dto.setEquipmentTypeCode("");
		dto.setReadingRequiredDate(LocalDate.of(1801, 1, 1));
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetEquipmentReadingsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
