package au.awh.file.equipmentreadings.geteequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;

import au.awh.config.LocalDateConverter;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 16
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = 46217638839141678L;

	private ReturnCodeEnum _sysReturnCode;
	private String awhRegionCode = ""; // String 56452
	private String plantEquipmentCode = ""; // String 56471
	private LocalDateTime readingTimestamp = null; // LocalDateTime 56485
	private ReadingTypeEnum readingType = null; // ReadingTypeEnum 56488
	private BigDecimal readingValue = BigDecimal.ZERO; // BigDecimal 56486
	private String readingComment = ""; // String 56487
	private String userId = ""; // String 4344
	private String equipmentLeasor = ""; // String 56525
	private BigDecimal transferToFromSgt = BigDecimal.ZERO; // BigDecimal 56539
	private String centreCodeKey = ""; // String 1140
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // AwhBuisnessSegmentEnum 56533
	private String equipmentTypeCode = ""; // String 56496
	private LocalDate readingRequiredDate = null; // LocalDate 56555
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO; // BigDecimal 60183
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO; // BigDecimal 60184
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO; // BigDecimal 60185

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}
	
	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}
	
	public ReadingTypeEnum getReadingType() {
		return readingType;
	}
	
	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}
	
	public void setReadingType(String readingType) {
		setReadingType(ReadingTypeEnum.valueOf(readingType));
	}
	
	public BigDecimal getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}
	
	
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}
	
	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}
	
	
	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}
	
	
	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}
	
	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}
	
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
