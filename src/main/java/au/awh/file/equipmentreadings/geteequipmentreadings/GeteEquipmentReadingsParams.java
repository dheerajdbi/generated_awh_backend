package au.awh.file.equipmentreadings.geteequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = -783799273258466518L;

    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.valueOf(getRecordOption));
	}
}
