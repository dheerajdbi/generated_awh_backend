package au.awh.file.equipmentreadings.chgeupdatereading;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeUpdateReading ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeUpdateReadingResult implements Serializable
{
	private static final long serialVersionUID = 1824190761429387819L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
