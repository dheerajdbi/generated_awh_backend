package au.awh.file.equipmentreadings.chgeupdatereading;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChgeUpdateReadingDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private AwhBuisnessSegmentEnum awhBuisnessSegment;
	private ErrorProcessingEnum errorProcessing;
	private LocalDate readingRequiredDate;
	private LocalDateTime readingTimestamp;
	private ReadingTypeEnum readingType;
	private String awhRegionCode;
	private String centreCodeKey;
	private String equipmentLeasor;
	private String equipmentTypeCode;
	private String plantEquipmentCode;
	private String readingComment;
	private String userId;
	private BigDecimal equipmentReadingSgt;
	private BigDecimal readingValue;
	private BigDecimal transferToFromSgt;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public String getReadingComment() {
		return readingComment;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public BigDecimal getReadingValue() {
		return readingValue;
	}

	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}

	public String getUserId() {
		return userId;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}

	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}

	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
