package au.awh.file.equipmentreadings.createequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CreateEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreateEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = -2927096242138951742L;

	private ReturnCodeEnum _sysReturnCode;
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO; // long 56531

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
