package au.awh.file.equipmentreadings.createequipmentreadings;

// CreateObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.lastsurrogate.getenextsurrogate.GeteNextSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.getenextsurrogate.GeteNextSurrogateDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.getenextsurrogate.GeteNextSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.getenextsurrogate.GeteNextSurrogateResult;
// imports section complete

/**
 * CRTOBJ Service controller for 'Create Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  CreateObjectFunction.kt
 */
@Service
public class CreateEquipmentReadingsService  extends AbstractService<CreateEquipmentReadingsService, CreateEquipmentReadingsDTO>
{
	private final Step execute = define("execute", CreateEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private GeteNextSurrogateService geteNextSurrogateService;

	//private final Step serviceGeteNextSurrogate = define("serviceGeteNextSurrogate",GeteNextSurrogateResult.class, this::processServiceGeteNextSurrogate);
	
	@Autowired
	public CreateEquipmentReadingsService() {
		super(CreateEquipmentReadingsService.class, CreateEquipmentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CreateEquipmentReadingsParams params) throws ServiceException {
		CreateEquipmentReadingsDTO dto = new CreateEquipmentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CreateEquipmentReadingsDTO dto, CreateEquipmentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		EquipmentReadings equipmentReadings = new EquipmentReadings();
		equipmentReadings.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		equipmentReadings.setAwhRegionCode(dto.getAwhRegionCode());
		equipmentReadings.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		equipmentReadings.setReadingTimestamp(dto.getReadingTimestamp());
		equipmentReadings.setReadingType(dto.getReadingType());
		equipmentReadings.setReadingValue(dto.getReadingValue());
		equipmentReadings.setReadingComment(dto.getReadingComment());
		equipmentReadings.setUserId(dto.getUserId());
		equipmentReadings.setEquipmentLeasor(dto.getEquipmentLeasor());
		equipmentReadings.setTransferToFromSgt(dto.getTransferToFromSgt());
		equipmentReadings.setCentreCodeKey(dto.getCentreCodeKey());
		equipmentReadings.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		equipmentReadings.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		equipmentReadings.setReadingRequiredDate(dto.getReadingRequiredDate());
		equipmentReadings.setDifferenceFromPrevious(dto.getDifferenceFromPrevious());
		equipmentReadings.setIdleDaysFromPrevious(dto.getIdleDaysFromPrevious());
		equipmentReadings.setUnavailableDaysFromPrv(dto.getUnavailableDaysFromPrv());

		processingBeforeDataUpdate(dto, equipmentReadings);

		EquipmentReadings equipmentReadings2 = equipmentReadingsRepository.findById(equipmentReadings.getId()).orElse(null);
		if (equipmentReadings2 != null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
			processingIfDataRecordAlreadyExists(dto, equipmentReadings2);
		}
		else {
			try {
				equipmentReadingsRepository.saveAndFlush(equipmentReadings);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto, equipmentReadings);
			} catch (Exception e) {
				System.out.println("Create error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
				processingIfDataUpdateError(dto, equipmentReadings);
			}
		}

		CreateEquipmentReadingsResult result = new CreateEquipmentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(CreateEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		GeteNextSurrogateParams geteNextSurrogateParams = null;
		GeteNextSurrogateResult geteNextSurrogateResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Next Surrogate - Last Surrogate  * Equipment reading Sgt
		geteNextSurrogateParams = new GeteNextSurrogateParams();
		geteNextSurrogateResult = new GeteNextSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, geteNextSurrogateParams);
		geteNextSurrogateParams.setLastSurrogateType("EQPREAD");
		geteNextSurrogateParams.setErrorProcessing("2");
		stepResult = geteNextSurrogateService.execute(geteNextSurrogateParams);
		geteNextSurrogateResult = (GeteNextSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(geteNextSurrogateResult.get_SysReturnCode());
		dto.setEquipmentReadingSgt(geteNextSurrogateResult.getLastSurrogate());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000007 ACT DB1.Equipment Reading Sgt = PAR.Equipment Reading Sgt
		equipmentReadings.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		// DEBUG genFunctionCall END
		return stepResult;
	}

	private StepResult processingIfDataRecordAlreadyExists(CreateEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Already Exists (Generated:43)
		 */
		
		// Unprocessed SUB 43 -
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(CreateEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing after Data Update (Generated:11)
		 */
		
		// Unprocessed SUB 11 -
		return stepResult;
	}

	private StepResult processingIfDataUpdateError(CreateEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * Processing if Data Update Error (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

//
//    /**
//     * GeteNextSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteNextSurrogate(CreateEquipmentReadingsDTO dto, GeteNextSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        CreateEquipmentReadingsParams createEquipmentReadingsParams = new CreateEquipmentReadingsParams();
//        BeanUtils.copyProperties(dto, createEquipmentReadingsParams);
//        stepResult = StepResult.callService(CreateEquipmentReadingsService.class, createEquipmentReadingsParams);
//
//        return stepResult;
//    }
//
}
