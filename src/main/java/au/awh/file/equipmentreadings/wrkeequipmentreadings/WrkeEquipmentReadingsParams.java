package au.awh.file.equipmentreadings.wrkeequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: WrkeEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class WrkeEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = 1640079811168783432L;

    private ErrorProcessingEnum errorProcessing = null;
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.fromCode(errorProcessing));
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
}
