package au.awh.file.equipmentreadings.prtleasorreadingspmt;

import java.time.LocalDate;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.GlobalContext;

import au.awh.file.equipmentreadings.EquipmentReadings;
// generateStatusFieldImportStatements BEGIN 39
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Readings' (WSEQREAD) and function 'Prt Leasor Readings Pmt' (WSPLTW6PVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Configurable
public class PrtLeasorReadingsPmtDTO extends BaseDTO {
	private static final long serialVersionUID = -476438777889472267L;

	@Autowired
	private GlobalContext globalCtx;

// DEBUG fields
	private long equipmentReadingSgt = 0L; // 56531
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private String plantEquipmentCode = ""; // 56471
	private String equipmentLeasor = ""; // 56525
	private String organisationNameDrv = ""; // 38795
	private LocalDate readingRequiredDate = null; // 56555
	private long differenceFromPrevious = 0L; // 60183
	private long idleDaysFromPrevious = 0L; // 60184
	private long unavailableDaysFromPrv = 0L; // 60185
	private LocalDate dateToIso = null; // 41091
	private String centreCodeKey = ""; // 1140
	private String centreNameDrv = ""; // 34917
	private SendViaPrintedReportEnum sendViaEmail = null; // 42734
	private String outputQueue = ""; // 34671
	private String outputQueueLibrary = ""; // 36415
	private String copies = ""; // 9040
	private FormsEnum forms = null; // 9043
	private HoldClFormatyesnoEnum holdClFormatyesno = null; // 9078
	private SaveClFormatyesnoEnum saveClFormatyesno = null; // 9079
// DEBUG detail fields
// DEBUG param
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; //0
// DEBUG local fields
	private String lclLdaCurrentCentreCode = ""; // 4391
// DEBUG Constructor

	public PrtLeasorReadingsPmtDTO() {

	}

	public PrtLeasorReadingsPmtDTO(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(equipmentReadings, this);
	}

	public PrtLeasorReadingsPmtDTO(long equipmentReadingSgt, String awhRegionCode, String plantEquipmentCode, String equipmentLeasor, LocalDate readingRequiredDate, long differenceFromPrevious, long idleDaysFromPrevious, long unavailableDaysFromPrv, String centreCodeKey, SendViaPrintedReportEnum sendViaEmail, String outputQueue, String copies, FormsEnum forms, HoldClFormatyesnoEnum holdClFormatyesno, SaveClFormatyesnoEnum saveClFormatyesno) {
		this.equipmentReadingSgt = equipmentReadingSgt;
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentLeasor = equipmentLeasor;
		this.readingRequiredDate = readingRequiredDate;
		this.differenceFromPrevious = differenceFromPrevious;
		this.idleDaysFromPrevious = idleDaysFromPrevious;
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
		this.centreCodeKey = centreCodeKey;
		this.sendViaEmail = sendViaEmail;
		this.outputQueue = outputQueue;
		this.copies = copies;
		this.forms = forms;
		this.holdClFormatyesno = holdClFormatyesno;
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
	}

	public String getAwhRegionNameDrv() {
		return awhRegionNameDrv;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setOrganisationNameDrv(String organisationNameDrv) {
		this.organisationNameDrv = organisationNameDrv;
	}

	public String getOrganisationNameDrv() {
		return organisationNameDrv;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setDifferenceFromPrevious(long differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}

	public long getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public void setIdleDaysFromPrevious(long idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}

	public long getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public void setUnavailableDaysFromPrv(long unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

	public long getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
	}

	public LocalDate getDateToIso() {
		return dateToIso;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void setSendViaEmail(SendViaPrintedReportEnum sendViaEmail) {
		this.sendViaEmail = sendViaEmail;
	}

	public SendViaPrintedReportEnum getSendViaEmail() {
		return sendViaEmail;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public String getCopies() {
		return copies;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public void setWfLdaClientAccountId(String ldaClientAccountId) {
		globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
	}

	public String getWfLdaClientAccountId() {
		return globalCtx.getString("ldaClientAccountId");
	}

	public void setWfLdaClipCode(String ldaClipCode) {
		globalCtx.setString("ldaClipCode", ldaClipCode);
	}

	public String getWfLdaClipCode() {
		return globalCtx.getString("ldaClipCode");
	}

	public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
	}

	public String getWfLdaDefaultCentreCode() {
		return globalCtx.getString("ldaDefaultCentreCode");
	}

	public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
		globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
	}

	public String getWfLdaDefaultStateCode() {
		return globalCtx.getString("ldaDefaultStateCode");
	}

	public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
	}

	public String getWfLdaDftSaleNbrId() {
		return globalCtx.getString("ldaDftSaleNbrId");
	}

	public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
	}

	public String getWfLdaDftSaleNbrSlgCtr() {
		return globalCtx.getString("ldaDftSaleNbrSlgCtr");
	}

	public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
	}

	public String getWfLdaDftSaleNbrStgCtr() {
		return globalCtx.getString("ldaDftSaleNbrStgCtr");
	}

	public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
	}

	public String getWfLdaDftSaleSeasonCent() {
		return globalCtx.getString("ldaDftSaleSeasonCent");
	}

	public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
	}

	public String getWfLdaDftSaleSeasonYear() {
		return globalCtx.getString("ldaDftSaleSeasonYear");
	}

	public void setWfLdaFolioNumber(String ldaFolioNumber) {
		globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
	}

	public String getWfLdaFolioNumber() {
		return globalCtx.getString("ldaFolioNumber");
	}

	public void setWfLdaLotNumber(String ldaLotNumber) {
		globalCtx.setString("ldaLotNumber", ldaLotNumber);
	}

	public String getWfLdaLotNumber() {
		return globalCtx.getString("ldaLotNumber");
	}

	public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
	}

	public String getWfLdaProcSaleNbrId() {
		return globalCtx.getString("ldaProcSaleNbrId");
	}

	public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
	}

	public String getWfLdaProcSaleNbrSlgCtr() {
		return globalCtx.getString("ldaProcSaleNbrSlgCtr");
	}

	public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
	}

	public String getWfLdaProcSaleNbrStgCtr() {
		return globalCtx.getString("ldaProcSaleNbrStgCtr");
	}

	public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
	}

	public String getWfLdaProcSaleSeasonCent() {
		return globalCtx.getString("ldaProcSaleSeasonCent");
	}

	public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
	}

	public String getWfLdaProcSaleSeasonYear() {
		return globalCtx.getString("ldaProcSaleSeasonYear");
	}

	public void setWfLdaWoolNumber(String ldaWoolNumber) {
		globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
	}

	public String getWfLdaWoolNumber() {
		return globalCtx.getString("ldaWoolNumber");
	}

	public void setLclLdaCurrentCentreCode(String ldaCurrentCentreCode) {
		this.lclLdaCurrentCentreCode = ldaCurrentCentreCode;
	}

	public String getLclLdaCurrentCentreCode() {
		return lclLdaCurrentCentreCode;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setDtoFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(equipmentReadings, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setEntityFields(EquipmentReadings equipmentReadings) {
        BeanUtils.copyProperties(this, equipmentReadings);
    }
}
