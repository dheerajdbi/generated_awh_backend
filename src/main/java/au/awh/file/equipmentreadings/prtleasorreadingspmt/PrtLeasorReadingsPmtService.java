package au.awh.file.equipmentreadings.prtleasorreadingspmt;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.equipmentreadings.EquipmentReadingsRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionService;
import au.awh.file.derivedfields.awhregionnamedrv.AwhRegionNameDrvService;
import au.awh.file.derivedfields.centrenamedrv.CentreNameDrvService;
import au.awh.file.derivedfields.organisationnamedrv.OrganisationNameDrvService;
import au.awh.file.equipmentreadings.callprtleasorreadings.CallPrtLeasorReadingsService;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpParams;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpResult;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpService;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersService;
// asServiceDtoImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionDTO;
import au.awh.file.derivedfields.awhregionnamedrv.AwhRegionNameDrvDTO;
import au.awh.file.derivedfields.centrenamedrv.CentreNameDrvDTO;
import au.awh.file.derivedfields.organisationnamedrv.OrganisationNameDrvDTO;
import au.awh.file.equipmentreadings.callprtleasorreadings.CallPrtLeasorReadingsDTO;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionParams;
import au.awh.file.derivedfields.awhregionnamedrv.AwhRegionNameDrvParams;
import au.awh.file.derivedfields.centrenamedrv.CentreNameDrvParams;
import au.awh.file.derivedfields.organisationnamedrv.OrganisationNameDrvParams;
import au.awh.file.equipmentreadings.callprtleasorreadings.CallPrtLeasorReadingsParams;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersParams;
// asServiceResultImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionResult;
import au.awh.file.derivedfields.awhregionnamedrv.AwhRegionNameDrvResult;
import au.awh.file.derivedfields.centrenamedrv.CentreNameDrvResult;
import au.awh.file.derivedfields.organisationnamedrv.OrganisationNameDrvResult;
import au.awh.file.equipmentreadings.callprtleasorreadings.CallPrtLeasorReadingsResult;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 28
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FormsEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.SendViaPrintedReportEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Prt Leasor Readings Pmt' (WSPLTW6PVK) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service(value = "EquipmentReadings_PrtLeasorReadingsPmt")
public class PrtLeasorReadingsPmtService extends AbstractService<PrtLeasorReadingsPmtService, PrtLeasorReadingsPmtState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

    
    public static final String SCREEN_PROMPT = "prtLeasorReadingsPmt";
    public static final String SCREEN_CONFIRM_PROMPT = "_screenConfirm";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",PrtLeasorReadingsPmtParams.class, this::executeService);
    private final Step response = define("response",PrtLeasorReadingsPmtDTO.class, this::processResponse);
  private final Step displayConfirmPrompt = define("displayConfirmPrompt",PrtLeasorReadingsPmtDTO.class, this::processDisplayConfirmPrompt);
    private final Step serviceSelectOrganization = define("serviceSelectOrganization",SelectOrganisationDumpResult.class, this::processServiceSelectOrganization);
	//private final Step serviceGeteCentresRegion = define("serviceGeteCentresRegion",GeteCentresRegionResult.class, this::processServiceGeteCentresRegion);
	//private final Step serviceSetWsPrintParameters = define("serviceSetWsPrintParameters",SetWsPrintParametersResult.class, this::processServiceSetWsPrintParameters);
	//private final Step serviceAwhRegionNameDrv = define("serviceAwhRegionNameDrv",AwhRegionNameDrvResult.class, this::processServiceAwhRegionNameDrv);
	//private final Step serviceOrganisationNameDrv = define("serviceOrganisationNameDrv",OrganisationNameDrvResult.class, this::processServiceOrganisationNameDrv);
	//private final Step serviceCentreNameDrv = define("serviceCentreNameDrv",CentreNameDrvResult.class, this::processServiceCentreNameDrv);
	//private final Step serviceCallPrtLeasorReadings = define("serviceCallPrtLeasorReadings",CallPrtLeasorReadingsResult.class, this::processServiceCallPrtLeasorReadings);
	

	
    	
@Autowired
private AwhRegionNameDrvService awhRegionNameDrvService;
	
@Autowired
private CallPrtLeasorReadingsService callPrtLeasorReadingsService;
	
@Autowired
private CentreNameDrvService centreNameDrvService;
	
@Autowired
private GeteCentresRegionService geteCentresRegionService;
	
@Autowired
private OrganisationNameDrvService organisationNameDrvService;
	
@Autowired
private SetWsPrintParametersService setWsPrintParametersService;
	
    @Autowired
    public PrtLeasorReadingsPmtService()
    {
        super(PrtLeasorReadingsPmtService.class, PrtLeasorReadingsPmtState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * PrtLeasorReadingsPmt service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(PrtLeasorReadingsPmtState state, PrtLeasorReadingsPmtParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * PrtLeasorReadingsPmt service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(PrtLeasorReadingsPmtState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(PrtLeasorReadingsPmtState state)
    {
        StepResult stepResult = NO_ACTION;

        PrtLeasorReadingsPmtDTO dto = new PrtLeasorReadingsPmtDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(PrtLeasorReadingsPmtState state, PrtLeasorReadingsPmtDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			switch (state.get_SysEntrySelected()) {
			case "equipmentLeasor":
				SelectOrganisationDumpParams param= new SelectOrganisationDumpParams();
				param.setOrganisation(state.getEquipmentLeasor());
				stepResult = 	StepResult.callService(SelectOrganisationDumpService.class,param).thenCall(serviceSelectOrganization);
				break;
			    default:
			        System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
			        stepResult = conductScreenConversation(state);
			        break;
			}
        }
        else
        {
            stepResult = validateScreenInput(state);
            if(stepResult != NO_ACTION) {
                return stepResult.thenCall(response);
            }
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
    private StepResult processDisplayConfirmPrompt(PrtLeasorReadingsPmtState state, PrtLeasorReadingsPmtDTO dto) {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (!state.get_SysDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
        {
            usrUserDefinedAction(state);
        }
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateScreenInput(PrtLeasorReadingsPmtState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrProcessCommandKeys(state);
        if(stepResult != NO_ACTION) {
            return stepResult;
        }
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            stepResult = usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                stepResult = usrScreenFunctionFields(state);
                stepResult = usrValidateRelations(state);
            }
        }

        return stepResult;
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(PrtLeasorReadingsPmtState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(PrtLeasorReadingsPmtState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(PrtLeasorReadingsPmtState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        stepResult = exitProgram(state);

        return stepResult;
    }

    /**
     * Exit this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult exitProgram(PrtLeasorReadingsPmtState state) {
        StepResult stepResult = NO_ACTION;

        PrtLeasorReadingsPmtResult params = new PrtLeasorReadingsPmtResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            GeteCentresRegionResult geteCentresRegionResult = null;
			GeteCentresRegionParams geteCentresRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1000386 ACT GetE Centres Region - AWH Region/Centre  *
			geteCentresRegionParams = new GeteCentresRegionParams();
			geteCentresRegionResult = new GeteCentresRegionResult();
			geteCentresRegionParams.setCentreCodeKey(dto.getLclLdaCurrentCentreCode());
			geteCentresRegionParams.setErrorProcessing("2");
			geteCentresRegionParams.setGetRecordOption("");
			stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
			geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(geteCentresRegionResult , dto);
			stepResult = NO_ACTION;
			if(dto.is_SysExitCallingProgram()){
			//function: prtLeasorReadingsPmt PMTRCD
			    dto.set_SysExitCallingProgram(false);
			    return exitProgram(dto);
			}
			dto.setAwhRegionCode(geteCentresRegionResult.getAwhRegionCode());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000392 ACT PAR.Equipment Leasor = CND.ORIX
			dto.setEquipmentLeasor("ORIX");
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            AwhRegionNameDrvParams awhRegionNameDrvParams = null;
			OrganisationNameDrvResult organisationNameDrvResult = null;
			CentreNameDrvResult centreNameDrvResult = null;
			CentreNameDrvParams centreNameDrvParams = null;
			OrganisationNameDrvParams organisationNameDrvParams = null;
			AwhRegionNameDrvResult awhRegionNameDrvResult = null;
			// DEBUG genFunctionCall BEGIN 1000337 ACT AWH region Name Drv      *FIELD                                             DTLA
			awhRegionNameDrvParams = new AwhRegionNameDrvParams();
			awhRegionNameDrvResult = new AwhRegionNameDrvResult();
			awhRegionNameDrvParams.setAwhRegionCode(dto.getAwhRegionCode());
			stepResult = awhRegionNameDrvService.execute(awhRegionNameDrvParams);
			awhRegionNameDrvResult = (AwhRegionNameDrvResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(awhRegionNameDrvResult , dto);
			stepResult = NO_ACTION;
			if(dto.is_SysExitCallingProgram()){
			//function: prtLeasorReadingsPmt PMTRCD
			    dto.set_SysExitCallingProgram(false);
			    return exitProgram(dto);
			}
			dto.setAwhRegionNameDrv(awhRegionNameDrvResult.getAwhRegionNameDrv());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000341 ACT Organisation Name Drv    *FIELD                                             DTLO
			organisationNameDrvParams = new OrganisationNameDrvParams();
			organisationNameDrvResult = new OrganisationNameDrvResult();
			organisationNameDrvParams.setOrganisation(dto.getEquipmentLeasor());
			
			stepResult = organisationNameDrvService.execute(organisationNameDrvParams);
			organisationNameDrvResult = (OrganisationNameDrvResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(organisationNameDrvResult , dto);
			stepResult = NO_ACTION;
			if(dto.is_SysExitCallingProgram()){
			//function: prtLeasorReadingsPmt PMTRCD
			    dto.set_SysExitCallingProgram(false);
			    return exitProgram(dto);
			}
			dto.setOrganisationNameDrv(organisationNameDrvResult.getOrganisationNameDrv());
			// DEBUG genFunctionCall END
			if (!dto.getCentreCodeKey().equals("")) {
				// DTL.Centre Code           KEY is Entered
				// DEBUG genFunctionCall BEGIN 1000345 ACT Centre Name Drv          *FIELD                                             DTLC
				centreNameDrvParams = new CentreNameDrvParams();
				centreNameDrvResult = new CentreNameDrvResult();
				centreNameDrvParams.setCentreCodeKey(dto.getCentreCodeKey());
				stepResult = centreNameDrvService.execute(centreNameDrvParams);
				centreNameDrvResult = (CentreNameDrvResult)((ReturnFromService)stepResult.getAction()).getResults();
				BeanUtils.copyProperties(centreNameDrvResult , dto);  
				System.out.println("");
				stepResult = NO_ACTION;
				if(dto.is_SysExitCallingProgram()){
				//function: prtLeasorReadingsPmt PMTRCD
				    dto.set_SysExitCallingProgram(false);
				    return exitProgram(dto);
				}
				dto.setCentreNameDrv(centreNameDrvResult.getCentreNameDrv());
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000447 ACT DTL.Centre Name Drv = CND.Optional
				dto.setCentreNameDrv("*Optional");
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            SetWsPrintParametersParams setWsPrintParametersParams = null;
			SetWsPrintParametersResult setWsPrintParametersResult = null;
			// DEBUG genFunctionCall BEGIN 1000399 ACT DTL.Send via Email = CND.Yes
			dto.setSendViaEmail(SendViaPrintedReportEnum._YES);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000405 ACT Set WS print parameters - Utilities Print & Email  *
			setWsPrintParametersParams = new SetWsPrintParametersParams();
			setWsPrintParametersResult = new SetWsPrintParametersResult();
			setWsPrintParametersParams.setBrokerIdKey("");
			setWsPrintParametersParams.setStateCodeKey("");
			setWsPrintParametersParams.setCentreCodeKey(dto.getLclLdaCurrentCentreCode());
			System.out.println("");
			setWsPrintParametersParams.setReport("EQPLEASE");
			stepResult = setWsPrintParametersService.execute(setWsPrintParametersParams);
			if(stepResult!=NO_ACTION)
			setWsPrintParametersResult = (SetWsPrintParametersResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(setWsPrintParametersResult , dto);
			stepResult = NO_ACTION;
			if(dto.is_SysExitCallingProgram()){
			//function: prtLeasorReadingsPmt PMTRCD
			    dto.set_SysExitCallingProgram(false);
			    return exitProgram(dto);
			}
			dto.setOutputQueue(setWsPrintParametersResult.getOutputQueue());
			dto.setOutputQueueLibrary(setWsPrintParametersResult.getOutputQueueLibrary());
			dto.setCopies(setWsPrintParametersResult.getCopies());
			dto.setForms(setWsPrintParametersResult.getForms());
			dto.setHoldClFormatyesno(setWsPrintParametersResult.getHoldClFormatyesno());
			dto.setSaveClFormatyesno(setWsPrintParametersResult.getSaveClFormatyesno());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000418 ACT DTL.Centre Name Drv = CND.Optional
			dto.setCentreNameDrv("*Optional");
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000330 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				stepResult = exitProgram(dto);
				//dummy conditional return to avoid dead code error...
				if(stepResult != NO_ACTION) {
				    return stepResult;
				}
			
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            CallPrtLeasorReadingsResult callPrtLeasorReadingsResult = null;
			CallPrtLeasorReadingsParams callPrtLeasorReadingsParams = null;
			// DEBUG genFunctionCall BEGIN 1000422 ACT Call Prt Leasor Readings - Equipment Readings  *
			callPrtLeasorReadingsParams = new CallPrtLeasorReadingsParams();
			callPrtLeasorReadingsResult = new CallPrtLeasorReadingsResult();
			callPrtLeasorReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
			callPrtLeasorReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
			callPrtLeasorReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
			callPrtLeasorReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			callPrtLeasorReadingsParams.setDateToIso(dto.getDateToIso());
			callPrtLeasorReadingsParams.setOutputQueue(dto.getOutputQueue());
			callPrtLeasorReadingsParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
			callPrtLeasorReadingsParams.setCopies(dto.getCopies());
			callPrtLeasorReadingsParams.setForms(dto.getForms());
			callPrtLeasorReadingsParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
			callPrtLeasorReadingsParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
			callPrtLeasorReadingsParams.setSendViaEmail(dto.getSendViaEmail());
			stepResult = callPrtLeasorReadingsService.execute(callPrtLeasorReadingsParams);
			if(stepResult!=NO_ACTION) {
			callPrtLeasorReadingsResult = (CallPrtLeasorReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(callPrtLeasorReadingsResult , dto);
			}
			stepResult = NO_ACTION;			
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000436 ACT Send information message - 'Report Submitted For Prt'
			//dto.addMessage("", "report.submitted.for.prt", messageSource);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(PrtLeasorReadingsPmtState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000247 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GeteCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentresRegion(PrtLeasorReadingsPmtState state, GeteCentresRegionResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * SetWsPrintParametersService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceSetWsPrintParameters(PrtLeasorReadingsPmtState state, SetWsPrintParametersResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * AwhRegionNameDrvService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAwhRegionNameDrv(PrtLeasorReadingsPmtState state, AwhRegionNameDrvResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * OrganisationNameDrvService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceOrganisationNameDrv(PrtLeasorReadingsPmtState state, OrganisationNameDrvResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * CentreNameDrvService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCentreNameDrv(PrtLeasorReadingsPmtState state, CentreNameDrvResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * CallPrtLeasorReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCallPrtLeasorReadings(PrtLeasorReadingsPmtState state, CallPrtLeasorReadingsResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
    private StepResult processServiceSelectOrganization(PrtLeasorReadingsPmtState state, SelectOrganisationDumpResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if(serviceResult != null ) {
            //BeanUtils.copyProperties(serviceResult, state);
            state.setEquipmentLeasor(serviceResult.getOrganisation());
        }       
        PrtLeasorReadingsPmtParams prtLeasorReadingsPmtParams = new PrtLeasorReadingsPmtParams();
        BeanUtils.copyProperties(state, prtLeasorReadingsPmtParams);
        stepResult =  conductScreenConversation(state);

        return stepResult;
    }

}
