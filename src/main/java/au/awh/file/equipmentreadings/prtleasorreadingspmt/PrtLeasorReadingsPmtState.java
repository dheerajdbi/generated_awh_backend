package au.awh.file.equipmentreadings.prtleasorreadingspmt;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import au.awh.model.GlobalContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 18
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Prt Leasor Readings Pmt' (WSPLTW6PVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Configurable
public class PrtLeasorReadingsPmtState extends PrtLeasorReadingsPmtDTO {
    private static final long serialVersionUID = -2982734963406781773L;

    @Autowired
    private GlobalContext globalCtx;

    // Local fields
    private String lclLdaCurrentCentreCode = "";

    // System fields
    private boolean _SysExitCallingProgram = false;
    private boolean _SysErrorFound = false;

    public PrtLeasorReadingsPmtState() {
    }

    public PrtLeasorReadingsPmtState(EquipmentReadings equipmentReadings) {
        setDtoFields(equipmentReadings);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _SysErrorFound = true;
    }

    public int clearMessages() {
        _SysErrorFound = false;
        return super.clearMessages();
    }

    public void setWfLdaClientAccountId(String ldaClientAccountId) {
        globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
    }

    public String getWfLdaClientAccountId() {
        return globalCtx.getString("ldaClientAccountId");
    }

    public void setWfLdaClipCode(String ldaClipCode) {
        globalCtx.setString("ldaClipCode", ldaClipCode);
    }

    public String getWfLdaClipCode() {
        return globalCtx.getString("ldaClipCode");
    }

    public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
        globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
    }

    public String getWfLdaDefaultCentreCode() {
        return globalCtx.getString("ldaDefaultCentreCode");
    }

    public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
        globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
    }

    public String getWfLdaDefaultStateCode() {
        return globalCtx.getString("ldaDefaultStateCode");
    }

    public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
        globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
    }

    public String getWfLdaDftSaleNbrId() {
        return globalCtx.getString("ldaDftSaleNbrId");
    }

    public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
        globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
    }

    public String getWfLdaDftSaleNbrSlgCtr() {
        return globalCtx.getString("ldaDftSaleNbrSlgCtr");
    }

    public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
        globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
    }

    public String getWfLdaDftSaleNbrStgCtr() {
        return globalCtx.getString("ldaDftSaleNbrStgCtr");
    }

    public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
        globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
    }

    public String getWfLdaDftSaleSeasonCent() {
        return globalCtx.getString("ldaDftSaleSeasonCent");
    }

    public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
        globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
    }

    public String getWfLdaDftSaleSeasonYear() {
        return globalCtx.getString("ldaDftSaleSeasonYear");
    }

    public void setWfLdaFolioNumber(String ldaFolioNumber) {
        globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
    }

    public String getWfLdaFolioNumber() {
        return globalCtx.getString("ldaFolioNumber");
    }

    public void setWfLdaLotNumber(String ldaLotNumber) {
        globalCtx.setString("ldaLotNumber", ldaLotNumber);
    }

    public String getWfLdaLotNumber() {
        return globalCtx.getString("ldaLotNumber");
    }

    public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
        globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
    }

    public String getWfLdaProcSaleNbrId() {
        return globalCtx.getString("ldaProcSaleNbrId");
    }

    public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
        globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
    }

    public String getWfLdaProcSaleNbrSlgCtr() {
        return globalCtx.getString("ldaProcSaleNbrSlgCtr");
    }

    public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
        globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
    }

    public String getWfLdaProcSaleNbrStgCtr() {
        return globalCtx.getString("ldaProcSaleNbrStgCtr");
    }

    public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
        globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
    }

    public String getWfLdaProcSaleSeasonCent() {
        return globalCtx.getString("ldaProcSaleSeasonCent");
    }

    public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
        globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
    }

    public String getWfLdaProcSaleSeasonYear() {
        return globalCtx.getString("ldaProcSaleSeasonYear");
    }

    public void setWfLdaWoolNumber(String ldaWoolNumber) {
        globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
    }

    public String getWfLdaWoolNumber() {
        return globalCtx.getString("ldaWoolNumber");
    }

    public void setLclLdaCurrentCentreCode(String ldaCurrentCentreCode) {
        this.lclLdaCurrentCentreCode = ldaCurrentCentreCode;
    }

    public String getLclLdaCurrentCentreCode() {
        return lclLdaCurrentCentreCode;
    }

    public boolean is_SysExitCallingProgram() {
        return _SysExitCallingProgram;
    }

    public void set_SysExitCallingProgram(boolean exitCallingProgram) {
        this._SysExitCallingProgram = exitCallingProgram;
    }

    public boolean get_SysErrorFound() {
        return _SysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _SysErrorFound = errorFound;
    }

}
