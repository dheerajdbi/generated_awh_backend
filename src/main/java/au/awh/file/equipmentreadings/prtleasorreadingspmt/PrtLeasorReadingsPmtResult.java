package au.awh.file.equipmentreadings.prtleasorreadingspmt;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: PrtLeasorReadingsPmt (WSPLTW6PVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class PrtLeasorReadingsPmtResult implements Serializable
{
	private static final long serialVersionUID = 6893835763475038218L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
