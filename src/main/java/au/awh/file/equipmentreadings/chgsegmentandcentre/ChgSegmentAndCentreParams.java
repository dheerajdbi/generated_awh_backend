package au.awh.file.equipmentreadings.chgsegmentandcentre;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgSegmentAndCentre ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgSegmentAndCentreParams implements Serializable
{
	private static final long serialVersionUID = 5742594163642027348L;

    private long equipmentReadingSgt = 0L;
    private String centreCodeKey = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
}
