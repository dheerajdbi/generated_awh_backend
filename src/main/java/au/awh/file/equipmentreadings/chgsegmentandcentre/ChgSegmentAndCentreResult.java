package au.awh.file.equipmentreadings.chgsegmentandcentre;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgSegmentAndCentre ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgSegmentAndCentreResult implements Serializable
{
	private static final long serialVersionUID = 5728294935916072475L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
