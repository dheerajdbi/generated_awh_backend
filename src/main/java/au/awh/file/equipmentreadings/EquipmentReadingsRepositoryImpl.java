package au.awh.file.equipmentreadings;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 10
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk.WrkEquipmentReadingsWspltwsdfkGDO;
import au.awh.file.equipmentreadings.addmequipmentreadings.AddmEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.prtleasorreadingspmt.PrtLeasorReadingsPmtDTO;
import au.awh.file.equipmentreadings.addmbatchreadinghh.AddmBatchReadingHhDTO;

/**
 * Custom Spring Data JPA repository implementation for model: Equipment Readings (WSEQREAD).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class EquipmentReadingsRepositoryImpl implements EquipmentReadingsRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#getReadingByDate(String, String, ReadingTypeEnum, LocalDate)
	 */
	@Override
	public RestResponsePage<EquipmentReadings> getReadingByDate(
		String awhRegionCode, String centreCodeKey, ReadingTypeEnum readingType, LocalDate readingRequiredDate
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		if (readingType != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingType = :readingType";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (readingType != null) {
			countQuery.setParameter("readingType", readingType);
			query.setParameter("readingType", readingType);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadings> content = query.getResultList();
		RestResponsePage<EquipmentReadings> pageGdo = new RestResponsePage<EquipmentReadings>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#getByReverseDate(String, String, LocalDate, ReadingTypeEnum)
	 */
	@Override
	public RestResponsePage<EquipmentReadings> getByReverseDate(
		String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate, ReadingTypeEnum readingType
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		if (readingType != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingType = :readingType";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		if (readingType != null) {
			countQuery.setParameter("readingType", readingType);
			query.setParameter("readingType", readingType);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadings> content = query.getResultList();
		RestResponsePage<EquipmentReadings> pageGdo = new RestResponsePage<EquipmentReadings>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> wrkEquipmentReadingsWspltwsdfk(
		String awhRegionCode, String plantEquipmentCode
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode like CONCAT('%', :awhRegionCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode like CONCAT('%', :plantEquipmentCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk.WrkEquipmentReadingsWspltwsdfkGDO(equipmentReadings.readingRequiredDate, equipmentReadings.awhRegionCode, equipmentReadings.plantEquipmentCode, equipmentReadings.readingTimestamp, equipmentReadings.id.equipmentReadingSgt, equipmentReadings.readingType, equipmentReadings.readingValue, equipmentReadings.userId, equipmentReadings.equipmentLeasor, equipmentReadings.transferToFromSgt, equipmentReadings.centreCodeKey, equipmentReadings.awhBuisnessSegment, equipmentReadings.equipmentTypeCode, equipmentReadings.differenceFromPrevious, equipmentReadings.idleDaysFromPrevious, equipmentReadings.unavailableDaysFromPrv, equipmentReadings.readingComment) from EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			EquipmentReadings equipmentReadings = new EquipmentReadings();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(equipmentReadings.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"equipmentReadings.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"equipmentReadings." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentReadings.awhRegionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentReadings.plantEquipmentCode"));
			sortOrders.add(new Order(Sort.Direction.DESC,
				"equipmentReadings.readingRequiredDate"));
			sortOrders.add(new Order(Sort.Direction.DESC,
				"equipmentReadings.id.equipmentReadingSgt"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<WrkEquipmentReadingsWspltwsdfkGDO> content = query.getResultList();
		RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> pageGdo = new RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<AddmEquipmentReadingsDTO> addmEquipmentReadings(
		String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode like CONCAT('%', :awhRegionCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode like CONCAT('%', :plantEquipmentCode, '%')";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmentreadings.addmequipmentreadings.AddmEquipmentReadingsDTO(equipmentReadings.id.equipmentReadingSgt, equipmentReadings.awhRegionCode, equipmentReadings.plantEquipmentCode, equipmentReadings.equipmentTypeCode, equipmentReadings.centreCodeKey, equipmentReadings.awhBuisnessSegment, equipmentReadings.readingRequiredDate, equipmentReadings.differenceFromPrevious, equipmentReadings.idleDaysFromPrevious, equipmentReadings.unavailableDaysFromPrv, equipmentReadings.readingTimestamp, equipmentReadings.readingType, equipmentReadings.readingValue, equipmentReadings.readingComment, equipmentReadings.userId, equipmentReadings.equipmentLeasor, equipmentReadings.equipmentReadingsObj.equipmentReadingSgt) from EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<AddmEquipmentReadingsDTO> content = query.getResultList();
		RestResponsePage<AddmEquipmentReadingsDTO> pageGdo = new RestResponsePage<AddmEquipmentReadingsDTO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#getEquipmentReadings(long)
	 */
	@Override
	public EquipmentReadings getEquipmentReadings(
		long equipmentReadingSgt
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (equipmentReadingSgt > 0) {
			whereClause += " equipmentReadings.id.equipmentReadingSgt = :equipmentReadingSgt";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (equipmentReadingSgt > 0) {
			query.setParameter("equipmentReadingSgt", equipmentReadingSgt);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		EquipmentReadings dto = (EquipmentReadings)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<PrtLeasorReadingsPmtDTO> prtLeasorReadingsPmt(
		
Pageable pageable) {
		String sqlString = "SELECT new au.awh.file.equipmentreadings.prtleasorreadingspmt.PrtLeasorReadingsPmtDTO(equipmentReadings.awhRegionCode, equipmentReadings.plantEquipmentCode, equipmentReadings.equipmentLeasor, equipmentReadings.readingRequiredDate, equipmentReadings.differenceFromPrevious, equipmentReadings.idleDaysFromPrevious, equipmentReadings.unavailableDaysFromPrv, equipmentReadings.centreCodeKey) from EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PrtLeasorReadingsPmtDTO> content = query.getResultList();
		RestResponsePage<PrtLeasorReadingsPmtDTO> pageGdo = new RestResponsePage<PrtLeasorReadingsPmtDTO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<AddmBatchReadingHhDTO> addmBatchReadingHh(
		long equipReadingBatchNo
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (equipReadingBatchNo > 0) {
			whereClause += " equipmentReadings.equipReadingBatchNo = :equipReadingBatchNo";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmentreadings.addmbatchreadinghh.AddmBatchReadingHhDTO(equipmentReadings.readingRequiredDate, equipmentReadings.differenceFromPrevious, equipmentReadings.idleDaysFromPrevious, equipmentReadings.unavailableDaysFromPrv, equipmentReadings.id.equipmentReadingSgt, equipmentReadings.awhRegionCode, equipmentReadings.plantEquipmentCode, equipmentReadings.equipmentTypeCode, equipmentReadings.centreCodeKey, equipmentReadings.awhBuisnessSegment, equipmentReadings.readingTimestamp, equipmentReadings.readingType, equipmentReadings.readingValue, equipmentReadings.readingComment, equipmentReadings.userId, equipmentReadings.equipmentLeasor, equipmentReadings.equipmentReadingsObj.equipmentReadingSgt) from EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (equipReadingBatchNo > 0) {
			countQuery.setParameter("equipReadingBatchNo", equipReadingBatchNo);
			query.setParameter("equipReadingBatchNo", equipReadingBatchNo);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<AddmBatchReadingHhDTO> content = query.getResultList();
		RestResponsePage<AddmBatchReadingHhDTO> pageGdo = new RestResponsePage<AddmBatchReadingHhDTO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#tranfSunsqentReadings(String, String, LocalDate, AwhBuisnessSegmentEnum, String)
	 */
	@Override
	public RestResponsePage<EquipmentReadings> tranfSunsqentReadings(
		String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate, AwhBuisnessSegmentEnum transferToBuisSeg, String transferToCentre
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadings> content = query.getResultList();
		RestResponsePage<EquipmentReadings> pageGdo = new RestResponsePage<EquipmentReadings>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#updReadingTotals(String, String, String)
	 */
	@Override
	public RestResponsePage<EquipmentReadings> updReadingTotals(
		String awhRegionCode, String plantEquipmentCode, String centreCodeKey
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadings> content = query.getResultList();
		RestResponsePage<EquipmentReadings> pageGdo = new RestResponsePage<EquipmentReadings>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmentreadings.EquipmentReadingsService#rtvupdEquipmentCode(String, String, String)
	 */
	@Override
	public RestResponsePage<EquipmentReadings> rtvupdEquipmentCode(
		String awhRegionCode, String plantEquipmentCode, String newPlantEquipmentCode
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " equipmentReadings.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentReadings.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentReadings FROM EquipmentReadings equipmentReadings";
		String countQueryString = "SELECT COUNT(equipmentReadings.id.equipmentReadingSgt) FROM EquipmentReadings equipmentReadings";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadings> content = query.getResultList();
		RestResponsePage<EquipmentReadings> pageGdo = new RestResponsePage<EquipmentReadings>(
				content, pageable, count.longValue());

		return pageGdo;
	}

}
