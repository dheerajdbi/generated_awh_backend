package au.awh.file.equipmentreadings.chgcrtequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 13
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgcrtEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgcrtEquipmentReadingsParams implements Serializable
{
	private static final long serialVersionUID = 5385616913337174233L;

    private long equipmentReadingSgt = 0L;
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private ReadingTypeEnum readingType = null;
    private long readingValue = 0L;
    private String readingComment = "";
    private String userId = "";
    private String equipmentLeasor = "";
    private long transferToFromSgt = 0L;
    private String centreCodeKey = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String equipmentTypeCode = "";
    private LocalDate readingRequiredDate = null;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public ReadingTypeEnum getReadingType() {
		return readingType;
	}
	
	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}
	
	public void setReadingType(String readingType) {
		setReadingType(ReadingTypeEnum.valueOf(readingType));
	}
	
	public long getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}
	
	public void setReadingValue(String readingValue) {
		setReadingValue(Long.parseLong(readingValue));
	}
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	public void setTransferToFromSgt(String transferToFromSgt) {
		setTransferToFromSgt(Long.parseLong(transferToFromSgt));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
}
