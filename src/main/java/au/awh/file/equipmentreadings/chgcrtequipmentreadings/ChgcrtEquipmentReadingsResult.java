package au.awh.file.equipmentreadings.chgcrtequipmentreadings;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgcrtEquipmentReadings ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgcrtEquipmentReadingsResult implements Serializable
{
	private static final long serialVersionUID = -6510757503072907949L;

	private ReturnCodeEnum _sysReturnCode;
	private long equipmentReadingSgt = 0L; // long 56531

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
