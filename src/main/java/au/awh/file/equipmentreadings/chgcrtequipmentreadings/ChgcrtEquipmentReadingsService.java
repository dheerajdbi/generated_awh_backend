package au.awh.file.equipmentreadings.chgcrtequipmentreadings;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsService;
// imports for DTO
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsParams;
// imports for Results
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsResult;
// imports section complete

/**
 * CHGOBJ Service controller for 'ChgCrt Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChgcrtEquipmentReadingsService extends AbstractService<ChgcrtEquipmentReadingsService, ChgcrtEquipmentReadingsDTO>
{
	private final Step execute = define("execute", ChgcrtEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private CreateEquipmentReadingsService createEquipmentReadingsService;

	//private final Step serviceCreateEquipmentReadings = define("serviceCreateEquipmentReadings",CreateEquipmentReadingsResult.class, this::processServiceCreateEquipmentReadings);
	
	@Autowired
	public ChgcrtEquipmentReadingsService() {
		super(ChgcrtEquipmentReadingsService.class, ChgcrtEquipmentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgcrtEquipmentReadingsParams params) throws ServiceException {
		ChgcrtEquipmentReadingsDTO dto = new ChgcrtEquipmentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgcrtEquipmentReadingsDTO dto, ChgcrtEquipmentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId();
		equipmentReadingsId.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		EquipmentReadings equipmentReadings = equipmentReadingsRepository.findById(equipmentReadingsId).orElse(null);
		if (equipmentReadings == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, equipmentReadings);
			equipmentReadings.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
			equipmentReadings.setAwhRegionCode(dto.getAwhRegionCode());
			equipmentReadings.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			equipmentReadings.setReadingTimestamp(dto.getReadingTimestamp());
			equipmentReadings.setReadingType(dto.getReadingType());
			equipmentReadings.setReadingValue(dto.getReadingValue());
			equipmentReadings.setReadingComment(dto.getReadingComment());
			equipmentReadings.setUserId(dto.getUserId());
			equipmentReadings.setEquipmentLeasor(dto.getEquipmentLeasor());
			equipmentReadings.setTransferToFromSgt(dto.getTransferToFromSgt());
			equipmentReadings.setCentreCodeKey(dto.getCentreCodeKey());
			equipmentReadings.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			equipmentReadings.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			equipmentReadings.setReadingRequiredDate(dto.getReadingRequiredDate());
			equipmentReadings.setDifferenceFromPrevious(dto.getDifferenceFromPrevious());
			equipmentReadings.setIdleDaysFromPrevious(dto.getIdleDaysFromPrevious());
			equipmentReadings.setUnavailableDaysFromPrv(dto.getUnavailableDaysFromPrv());

			processingBeforeDataUpdate(dto, equipmentReadings);

			try {
				equipmentReadingsRepository.saveAndFlush(equipmentReadings);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChgcrtEquipmentReadingsResult result = new ChgcrtEquipmentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChgcrtEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000034 ACT PAR.Reading Timestamp = JOB.*System timestamp
		;
		// DEBUG genFunctionCall END
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChgcrtEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		CreateEquipmentReadingsResult createEquipmentReadingsResult = null;
		CreateEquipmentReadingsParams createEquipmentReadingsParams = null;
		// * Ignore return code.
		// DEBUG genFunctionCall BEGIN 1000003 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000007 ACT Create Equipment Readings - Equipment Readings  *
		createEquipmentReadingsParams = new CreateEquipmentReadingsParams();
		createEquipmentReadingsResult = new CreateEquipmentReadingsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, createEquipmentReadingsParams);
		createEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		createEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
		createEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		createEquipmentReadingsParams.setReadingTimestamp(dto.getReadingTimestamp());
		createEquipmentReadingsParams.setReadingType(dto.getReadingType());
		createEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
		createEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
		createEquipmentReadingsParams.setUserId(dto.getUserId());
		createEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
		createEquipmentReadingsParams.setTransferToFromSgt(dto.getTransferToFromSgt());
		createEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
		createEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		createEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		createEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
		stepResult = createEquipmentReadingsService.execute(createEquipmentReadingsParams);
		createEquipmentReadingsResult = (CreateEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(createEquipmentReadingsResult.get_SysReturnCode());
		dto.setEquipmentReadingSgt(createEquipmentReadingsResult.getEquipmentReadingSgt());
		// DEBUG genFunctionCall END
		// * Exit with return code for calling function to process.
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChgcrtEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChgcrtEquipmentReadingsDTO dto, EquipmentReadings equipmentReadings) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChgcrtEquipmentReadingsDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		
		// Unprocessed SUB 23 -
		return stepResult;
	}

//
//    /**
//     * CreateEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateEquipmentReadings(ChgcrtEquipmentReadingsDTO dto, CreateEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgcrtEquipmentReadingsParams chgcrtEquipmentReadingsParams = new ChgcrtEquipmentReadingsParams();
//        BeanUtils.copyProperties(dto, chgcrtEquipmentReadingsParams);
//        stepResult = StepResult.callService(ChgcrtEquipmentReadingsService.class, chgcrtEquipmentReadingsParams);
//
//        return stepResult;
//    }
//
}
