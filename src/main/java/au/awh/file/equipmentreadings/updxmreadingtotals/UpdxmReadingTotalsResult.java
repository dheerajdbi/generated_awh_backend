package au.awh.file.equipmentreadings.updxmreadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: UpdxmReadingTotals (WSPLTFVXFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdxmReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = 2435931025989322349L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
