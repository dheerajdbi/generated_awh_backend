package au.awh.file.equipmentreadings.updxmreadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: UpdxmReadingTotals (WSPLTFVXFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdxmReadingTotalsParams implements Serializable
{
	private static final long serialVersionUID = -8342845359550348912L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
}
