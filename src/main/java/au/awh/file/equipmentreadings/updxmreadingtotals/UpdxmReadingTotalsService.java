package au.awh.file.equipmentreadings.updxmreadingtotals;

// ExecuteExternalFunction.kt File=EquipmentReadings (ER) Function=updxmReadingTotals (2027475) WSPLTFVXFK

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.plantequipment.updereadingtotals.UpdeReadingTotalsService;
// imports for DTO
import au.awh.file.plantequipment.updereadingtotals.UpdeReadingTotalsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.updereadingtotals.UpdeReadingTotalsParams;
// imports for Results
import au.awh.file.plantequipment.updereadingtotals.UpdeReadingTotalsResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'UpdX:M Reading Totals' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service
public class UpdxmReadingTotalsService extends AbstractService<UpdxmReadingTotalsService, UpdxmReadingTotalsDTO>
{
	private final Step execute = define("execute", UpdxmReadingTotalsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private UpdeReadingTotalsService updeReadingTotalsService;

	//private final Step serviceUpdeReadingTotals = define("serviceUpdeReadingTotals",UpdeReadingTotalsResult.class, this::processServiceUpdeReadingTotals);
	
	@Autowired
	public UpdxmReadingTotalsService() {
		super(UpdxmReadingTotalsService.class, UpdxmReadingTotalsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(UpdxmReadingTotalsParams params) throws ServiceException {
		UpdxmReadingTotalsDTO dto = new UpdxmReadingTotalsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(UpdxmReadingTotalsDTO dto, UpdxmReadingTotalsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		UpdeReadingTotalsParams updeReadingTotalsParams = null;
		UpdeReadingTotalsResult updeReadingTotalsResult = null;
		// * Ignore initialisation errors.
		// DEBUG genFunctionCall BEGIN 1000005 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// * NOTE: If calling a display function, then you may need to pass
		// * the exit function option back to the calling function to process.
		// * If not, then remove the parameter from this function.
		// DEBUG genFunctionCall BEGIN 1000013 ACT UpdE Reading Totals - Plant Equipment  *
		updeReadingTotalsParams = new UpdeReadingTotalsParams();
		updeReadingTotalsResult = new UpdeReadingTotalsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, updeReadingTotalsParams);
		updeReadingTotalsParams.setAwhRegionCode(dto.getAwhRegionCode());
		updeReadingTotalsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		updeReadingTotalsParams.setErrorProcessing("2");
		stepResult = updeReadingTotalsService.execute(updeReadingTotalsParams);
		updeReadingTotalsResult = (UpdeReadingTotalsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(updeReadingTotalsResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000107 ACT Commit .
		// TODO: Unsupported Internal Function Type '*COMMIT' (message surrogate = 1001566)
		// DEBUG genFunctionCall END
		// * Exit with return code for calling function to process.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Exit program - return code PGM.*Return code
		// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
		// DEBUG genFunctionCall END

		UpdxmReadingTotalsResult result = new UpdxmReadingTotalsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * UpdeReadingTotalsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceUpdeReadingTotals(UpdxmReadingTotalsDTO dto, UpdeReadingTotalsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        UpdxmReadingTotalsParams updxmReadingTotalsParams = new UpdxmReadingTotalsParams();
//        BeanUtils.copyProperties(dto, updxmReadingTotalsParams);
//        stepResult = StepResult.callService(UpdxmReadingTotalsService.class, updxmReadingTotalsParams);
//
//        return stepResult;
//    }
//
}
