package au.awh.file.equipmentreadings.dspequipmentreading;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: DspEquipmentReading (WSPLTWTD1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DspEquipmentReadingParams implements Serializable
{
    private static final long serialVersionUID = -4054372481641135800L;

	private long equipmentReadingSgt = 0L;

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
}

