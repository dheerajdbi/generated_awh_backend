package au.awh.file.equipmentreadings.dspequipmentreading;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.support.JobContext;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsService;
import au.awh.file.utilitiesdisplay.loadscreenhdgkey.LoadScreenHdgKeyService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsDTO;
import au.awh.file.utilitiesdisplay.loadscreenhdgkey.LoadScreenHdgKeyDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsParams;
import au.awh.file.utilitiesdisplay.loadscreenhdgkey.LoadScreenHdgKeyParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsResult;
import au.awh.file.utilitiesdisplay.loadscreenhdgkey.LoadScreenHdgKeyResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 29
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * DSPRCD Service controller for 'Dsp Equipment Reading' (WSPLTWTD1K) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
@Service
public class DspEquipmentReadingService extends AbstractService<DspEquipmentReadingService, DspEquipmentReadingState>
{
    

	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

    @Autowired
	private MessageSource messageSource;

    
    @Autowired
    private GeteEquipmentReadingsService geteEquipmentReadingsService;
    
    @Autowired
    private LoadScreenHdgKeyService loadScreenHdgKeyService;
    
    
	public static final String SCREEN_KEY = "dspEquipmentReadingEntryPanel";
    public static final String SCREEN_DTL = "dspEquipmentReadingPanel";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", DspEquipmentReadingParams.class, this::executeService);
    private final Step keyScreenResponse = define("keyScreen", DspEquipmentReadingState.class, this::processResponseToKeyScreen);
    private final Step detailScreenResponse = define("detailScreen", DspEquipmentReadingState.class, this::processResponseToDetailScreen);

	//private final Step serviceLoadScreenHdgKey = define("serviceLoadScreenHdgKey",LoadScreenHdgKeyResult.class, this::processServiceLoadScreenHdgKey);
	//private final Step serviceGeteEquipmentReadings = define("serviceGeteEquipmentReadings",GeteEquipmentReadingsResult.class, this::processServiceGeteEquipmentReadings);
	

    
    @Autowired
	public DspEquipmentReadingService() {
        super(DspEquipmentReadingService.class, DspEquipmentReadingState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(DspEquipmentReadingState state, DspEquipmentReadingParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = conductKeyScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductKeyScreenConversation(DspEquipmentReadingState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrLoadKeyScreen(state);

        if(shouldBypassKeyScreen(state)){
            stepResult = conductDetailScreenConversation(state);
        } else {
            stepResult = displayKeyScreenConversation(state);
        }

        return stepResult;
    }

    /**
     * Determine if SCREEN_KEY should be bypassed.
     * @param state - Service state class.
     * @return bypass - boolean.
     */
    private boolean shouldBypassKeyScreen(DspEquipmentReadingState state) {
        boolean bypass = false;

        //bypass if all key screens are set
        if(state.getEquipmentReadingSgt() != BigDecimal.ZERO){
            bypass = true;
        }

        return bypass;
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult displayKeyScreenConversation(DspEquipmentReadingState state) {
        StepResult stepResult = NO_ACTION;

     	if (state.getConductKeyScreenConversation()) {
            DspEquipmentReadingDTO model = new DspEquipmentReadingDTO();
            BeanUtils.copyProperties(state, model);
    		stepResult = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
    	}

    	return stepResult;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponseToKeyScreen(DspEquipmentReadingState state, DspEquipmentReadingState model) {
    	StepResult stepResult = NO_ACTION;

        // Update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        } else {
            stepResult = validateKeyScreen(state);
            if (state.get_SysErrorFound()) {
            	stepResult = displayKeyScreenConversation(state);
                state.set_SysErrorFound(true);
            } else {
            	stepResult = conductDetailScreenConversation(state);
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_KEY validate key screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateKeyScreen(DspEquipmentReadingState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrValidateKeyScreen(state);

        EquipmentReadingsId equipmentReadingsId = new EquipmentReadingsId(state.getEquipmentReadingSgt());
        EquipmentReadings equipmentReadings = equipmentReadingsRepository.findById(equipmentReadingsId).orElse(null);
        if (equipmentReadings != null) {
            BeanUtils.copyProperties(equipmentReadings, state);
        }
        else {
            state.set_SysErrorFound(true);
        }

        stepResult = usrLoadDetailScreenFromDbfRecord(state);

        return stepResult;
    }

    /**
     * SCREEN_DETAIL display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductDetailScreenConversation(DspEquipmentReadingState state) {
        StepResult stepResult = NO_ACTION;

		if (state.getConductDetailScreenConversation()) {
            DspEquipmentReadingDTO model = new DspEquipmentReadingDTO();
            BeanUtils.copyProperties(state, model);
    		stepResult = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
		}

		return stepResult;
	}

    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponseToDetailScreen(DspEquipmentReadingState state, DspEquipmentReadingState model) {
    	StepResult stepResult = NO_ACTION;

    	// update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model,state);

        if (isExit(state.get_SysCmdKey())) {
             stepResult = closedown(state);
        } else if (isKeyScreen(state.get_SysCmdKey())) {
            stepResult = usrProcessKeyScreenRequest(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        } else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        } else {
            stepResult = processDetailScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN_DETAIL  process detail screen and return to key screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processDetailScreen(DspEquipmentReadingState state) {
    	StepResult stepResult = NO_ACTION;

        checkInputField();
        if (state.get_SysErrorFound()) {
        	return closedown(state);
        } else {
        	stepResult = usrDetailScreenFunctionFields(state);
            stepResult = usrValidateDetailScreen(state);
            if (state.get_SysErrorFound()) {
            	return closedown(state);
            } else {
            	stepResult = usrPerformConfirmedAction(state);
            	if (state.get_SysErrorFound()) {
                	return closedown(state);
                } else {
                	stepResult = usrProcessCommandKeys(state);
                }
            }
        }

        stepResult = conductKeyScreenConversation(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(DspEquipmentReadingState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        DspEquipmentReadingResult params = new DspEquipmentReadingResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    private StepResult checkInputField() {
        return NO_ACTION;
    } 
    

    /**
	 * ==================================== Synon user point(s) ==========================================
     */

	/**
	 * USER: Initialize Program (Empty:1)
	 */
    private StepResult usrInitializeProgram(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Key Screen (Generated:440)
	 */
    private StepResult usrLoadKeyScreen(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 440 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Key Screen (Generated:415)
	 */
    private StepResult usrValidateKeyScreen(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 415 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Detail Screen from DBF Record (Generated:100)
	 */
    private StepResult usrLoadDetailScreenFromDbfRecord(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteEquipmentReadingsParams geteEquipmentReadingsParams = null;
			GeteEquipmentReadingsResult geteEquipmentReadingsResult = null;
			// DEBUG genFunctionCall BEGIN 1000138 ACT DTL.*Condition name = Condition name of DTL.AWH Buisness Segment
			dto.set_SysConditionName(dto.getAwhBuisnessSegment().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000132 ACT DTL.Condition Name = Condition name of DTL.Reading Type
			dto.setConditionName(dto.getReadingType().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			if (ReadingTypeEnum.isTransfer(dto.getReadingType())) {
				// DTL.Reading Type is Transfer
				// DEBUG genFunctionCall BEGIN 1000153 ACT GetE Equipment Readings - Equipment Readings  *
				geteEquipmentReadingsParams = new GeteEquipmentReadingsParams();
				geteEquipmentReadingsResult = new GeteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteEquipmentReadingsParams);
				geteEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
				geteEquipmentReadingsParams.setErrorProcessing("1");
				geteEquipmentReadingsParams.setGetRecordOption("");
				stepResult = geteEquipmentReadingsService.execute(geteEquipmentReadingsParams);
				geteEquipmentReadingsResult = (GeteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteEquipmentReadingsResult.get_SysReturnCode());
				dto.setTransferToCentre(geteEquipmentReadingsResult.getCentreCodeKey());
				dto.setTransferToBuisSeg(geteEquipmentReadingsResult.getAwhBuisnessSegment());
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000171 ACT DTL.Special Instructions = DTL.Reading Comment
			dto.setSpecialInstructions(dto.getReadingComment());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Key Screen Request (Generated:466)
	 */
    private StepResult usrProcessKeyScreenRequest(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 466 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Detail Screen Function Fields (Generated:425)
	 */
    private StepResult usrDetailScreenFunctionFields(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000109 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000109
			// DEBUG X2EActionDiagramElement 1000109 ACT     AWH region Name Drv      *FIELD                                             DTLA
			// DEBUG X2EActionDiagramElement 1000110 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000111 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000113 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
			// TODO: NEED TO SUPPORT *FIELD Equipment Type Desc Drv  *FIELD                                             DTLE 1000113
			// DEBUG X2EActionDiagramElement 1000113 ACT     Equipment Type Desc Drv  *FIELD                                             DTLE
			// DEBUG X2EActionDiagramElement 1000114 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000115 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000117 ACT Equipment Description Drv*FIELD                                             DTLE
			// TODO: NEED TO SUPPORT *FIELD Equipment Description Drv*FIELD                                             DTLE 1000117
			// DEBUG X2EActionDiagramElement 1000117 ACT     Equipment Description Drv*FIELD                                             DTLE
			// DEBUG X2EActionDiagramElement 1000118 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000119 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000120 PAR DTL 
			// ERROR UNEXPECTED NUMBER OF PARAMETERS
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000122 ACT Centre Name Drv          *FIELD                                             DTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000122
			// DEBUG X2EActionDiagramElement 1000122 ACT     Centre Name Drv          *FIELD                                             DTLC
			// DEBUG X2EActionDiagramElement 1000123 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000124 PAR DTL 
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000126 ACT Organisation Name Drv    *FIELD                                             DTLO
			// TODO: NEED TO SUPPORT *FIELD Organisation Name Drv    *FIELD                                             DTLO 1000126
			// DEBUG X2EActionDiagramElement 1000126 ACT     Organisation Name Drv    *FIELD                                             DTLO
			// DEBUG X2EActionDiagramElement 1000127 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000128 PAR DTL 
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Detail Screen (Generated:199)
	 */
    private StepResult usrValidateDetailScreen(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000089 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000093 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Perform Confirmed Action (Generated:455)
	 */
    private StepResult usrPerformConfirmedAction(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000096 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000100 ACT Exit program - return code CND.*Normal
			// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:389)
	 */
    private StepResult usrProcessCommandKeys(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 389 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:30)
	 */
    private StepResult usrExitProgramProcessing(DspEquipmentReadingState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (isExit(dto.get_SysCmdKey())) {
				// KEY.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000067 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    } 
}
