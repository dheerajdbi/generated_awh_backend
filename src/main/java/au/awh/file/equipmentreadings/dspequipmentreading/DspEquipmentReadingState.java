package au.awh.file.equipmentreadings.dspequipmentreading;

import org.springframework.context.MessageSource;

// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Dsp Equipment Reading' (WSPLTWTD1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DspEquipmentReadingState extends DspEquipmentReadingDTO {
	private static final long serialVersionUID = 4732839029466875619L;

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    // System fields
	private boolean conductKeyScreenConversation = true;
	private boolean conductDetailScreenConversation = true;
    private boolean _sysErrorFound = false;

	public DspEquipmentReadingState() {

    }

    public boolean getConductKeyScreenConversation() {
    	return conductKeyScreenConversation;
    }

    public void setConductKeyScreenConversation(boolean conductKeyScreenConversation) {
    	this.conductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean getConductDetailScreenConversation() {
    	return conductDetailScreenConversation;
    }

    public void setConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        this.conductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}
