package au.awh.file.equipmentreadings.dspequipmentreading;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.springframework.beans.BeanUtils;
import au.awh.file.equipmentreadings.EquipmentReadings;
// generateStatusFieldImportStatements BEGIN 28
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Readings' (WSEQREAD) and function 'Dsp Equipment Reading' (WSPLTWTD1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DspEquipmentReadingDTO extends BaseDTO {
	private static final long serialVersionUID = -4311260193079934827L;

// Entries fields
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO; // 56531
	private String sflselPromptText = ""; // 332
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private String centreCodeKey = ""; // 1140
	private String centreNameDrv = ""; // 34917
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // 56533
	private String conditionName = ""; // 272
	private String plantEquipmentCode = ""; // 56471
	private String equipmentDescriptionDrv = ""; // 56586
	private String equipmentTypeCode = ""; // 56496
	private String equipmentTypeDescDrv = ""; // 56549
	private ReadingTypeEnum readingType = ReadingTypeEnum._NOT_ENTERED; // 56488
	private LocalDateTime readingTimestamp = null; // 56485
	private String userId = ""; // 4344
	private BigDecimal readingValue = BigDecimal.ZERO; // 56486
	private String equipmentLeasor = ""; // 56525
	private String organisationNameDrv = ""; // 38795
	private BigDecimal transferToFromSgt = BigDecimal.ZERO; // 56539
	private AwhBuisnessSegmentEnum transferToBuisSeg = null; // 56580
	private String transferToCentre = ""; // 56579
	private LocalDate readingRequiredDate = null; // 56555
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO; // 60183
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO; // 60184
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO; // 60185
	private String specialInstructions = ""; // 57098
	private String readingComment = ""; // 56487
// Param fields
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; // 29901
// Special fields
	private String _SysconditionName = ""; // 272


	public DspEquipmentReadingDTO() {

    }

	public DspEquipmentReadingDTO(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(equipmentReadings, this);
	}

	public BigDecimal getEquipmentReadingSgt() {
        return equipmentReadingSgt;
    }

	public void setEquipmentReadingSgt (BigDecimal equipmentReadingSgt) {
        this.equipmentReadingSgt = equipmentReadingSgt;
    }

	public String getSflselPromptText() {
        return sflselPromptText;
    }

	public void setSflselPromptText (String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

	public String getAwhRegionCode() {
        return awhRegionCode;
    }

	public void setAwhRegionCode (String awhRegionCode) {
        this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionNameDrv() {
        return awhRegionNameDrv;
    }

	public void setAwhRegionNameDrv (String awhRegionNameDrv) {
        this.awhRegionNameDrv = awhRegionNameDrv;
    }

	public String getCentreCodeKey() {
        return centreCodeKey;
    }

	public void setCentreCodeKey (String centreCodeKey) {
        this.centreCodeKey = centreCodeKey;
    }

	public String getCentreNameDrv() {
        return centreNameDrv;
    }

	public void setCentreNameDrv (String centreNameDrv) {
        this.centreNameDrv = centreNameDrv;
    }

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
        return awhBuisnessSegment;
    }

	public void setAwhBuisnessSegment (AwhBuisnessSegmentEnum awhBuisnessSegment) {
        this.awhBuisnessSegment = awhBuisnessSegment;
    }

	public String getConditionName() {
        return conditionName;
    }

	public void setConditionName (String conditionName) {
        this.conditionName = conditionName;
    }

	public String getPlantEquipmentCode() {
        return plantEquipmentCode;
    }

	public void setPlantEquipmentCode (String plantEquipmentCode) {
        this.plantEquipmentCode = plantEquipmentCode;
    }

	public String getEquipmentDescriptionDrv() {
        return equipmentDescriptionDrv;
    }

	public void setEquipmentDescriptionDrv (String equipmentDescriptionDrv) {
        this.equipmentDescriptionDrv = equipmentDescriptionDrv;
    }

	public String getEquipmentTypeCode() {
        return equipmentTypeCode;
    }

	public void setEquipmentTypeCode (String equipmentTypeCode) {
        this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeDescDrv() {
        return equipmentTypeDescDrv;
    }

	public void setEquipmentTypeDescDrv (String equipmentTypeDescDrv) {
        this.equipmentTypeDescDrv = equipmentTypeDescDrv;
    }

	public ReadingTypeEnum getReadingType() {
        return readingType;
    }

	public void setReadingType (ReadingTypeEnum readingType) {
        this.readingType = readingType;
    }

	public LocalDateTime getReadingTimestamp() {
        return readingTimestamp;
    }

	public void setReadingTimestamp (LocalDateTime readingTimestamp) {
        this.readingTimestamp = readingTimestamp;
    }

	public String getUserId() {
        return userId;
    }

	public void setUserId (String userId) {
        this.userId = userId;
    }

	public BigDecimal getReadingValue() {
        return readingValue;
    }

	public void setReadingValue (BigDecimal readingValue) {
        this.readingValue = readingValue;
    }

	public String getEquipmentLeasor() {
        return equipmentLeasor;
    }

	public void setEquipmentLeasor (String equipmentLeasor) {
        this.equipmentLeasor = equipmentLeasor;
    }

	public String getOrganisationNameDrv() {
        return organisationNameDrv;
    }

	public void setOrganisationNameDrv (String organisationNameDrv) {
        this.organisationNameDrv = organisationNameDrv;
    }

	public BigDecimal getTransferToFromSgt() {
        return transferToFromSgt;
    }

	public void setTransferToFromSgt (BigDecimal transferToFromSgt) {
        this.transferToFromSgt = transferToFromSgt;
    }

	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
        return transferToBuisSeg;
    }

	public void setTransferToBuisSeg (AwhBuisnessSegmentEnum transferToBuisSeg) {
        this.transferToBuisSeg = transferToBuisSeg;
    }

	public String getTransferToCentre() {
        return transferToCentre;
    }

	public void setTransferToCentre (String transferToCentre) {
        this.transferToCentre = transferToCentre;
    }

	public LocalDate getReadingRequiredDate() {
        return readingRequiredDate;
    }

	public void setReadingRequiredDate (LocalDate readingRequiredDate) {
        this.readingRequiredDate = readingRequiredDate;
    }

	public BigDecimal getDifferenceFromPrevious() {
        return differenceFromPrevious;
    }

	public void setDifferenceFromPrevious (BigDecimal differenceFromPrevious) {
        this.differenceFromPrevious = differenceFromPrevious;
    }

	public BigDecimal getIdleDaysFromPrevious() {
        return idleDaysFromPrevious;
    }

	public void setIdleDaysFromPrevious (BigDecimal idleDaysFromPrevious) {
        this.idleDaysFromPrevious = idleDaysFromPrevious;
    }

	public BigDecimal getUnavailableDaysFromPrv() {
        return unavailableDaysFromPrv;
    }

	public void setUnavailableDaysFromPrv (BigDecimal unavailableDaysFromPrv) {
        this.unavailableDaysFromPrv = unavailableDaysFromPrv;
    }

	public String getSpecialInstructions() {
        return specialInstructions;
    }

	public void setSpecialInstructions (String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

	public String getReadingComment() {
        return readingComment;
    }

	public void setReadingComment (String readingComment) {
        this.readingComment = readingComment;
    }

	public ExitProgramOptionEnum getExitProgramOption() {
        return exitProgramOption;
    }

 	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.exitProgramOption = exitProgramOption;
    }

	public String get_SysConditionName() {
        return _SysconditionName;
    }

 	public void set_SysConditionName(String conditionName) {
        this._SysconditionName = conditionName;
    }


    /**
     * Copies the fields of the Entity bean into the DTO bean.
     * @param equipmentReadings EquipmentReadings Entity bean\n")
     */
	public void setDtoFields(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(equipmentReadings, this);
    }

 	/**
     * Copies the fields of the DTO bean into the Entity bean.
 	 * @param equipmentReadings EquipmentReadings Entity bean
     */
    public void setEntityFields(EquipmentReadings equipmentReadings) {
		BeanUtils.copyProperties(this, equipmentReadings);
    }
}
