package au.awh.file.equipmentreadings.dspequipmentreading;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: DspEquipmentReading (WSPLTWTD1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DspEquipmentReadingResult implements Serializable
{
    private static final long serialVersionUID = -4928812678047117391L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}

