package au.awh.file.equipmentreadings.getebyreversedate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GeteByReverseDateDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private AwhBuisnessSegmentEnum awhBuisnessSegment;
	private ErrorProcessingEnum errorProcessing;
	private GetRecordOptionEnum getRecordOption;
	private LocalDate readingRequiredDate;
	private ReadingTypeEnum readingType;
	private String awhRegionCode;
	private String centreCodeKey;
	private String equipmentLeasor;
	private String equipmentTypeCode;
	private String plantEquipmentCode;
	private String readingComment;
	private String userId;
	private long differenceFromPrevious;
	private long equipmentReadingSgt;
	private long idleDaysFromPrevious;
	private long readingValue;
	private long transferToFromSgt;
	private long unavailableDaysFromPrv;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public long getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}

	public long getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public String getReadingComment() {
		return readingComment;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public long getReadingValue() {
		return readingValue;
	}

	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}

	public long getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public String getUserId() {
		return userId;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setDifferenceFromPrevious(long differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}

	public void setIdleDaysFromPrevious(long idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}

	public void setReadingValue(long readingValue) {
		this.readingValue = readingValue;
	}

	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}

	public void setUnavailableDaysFromPrv(long unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
