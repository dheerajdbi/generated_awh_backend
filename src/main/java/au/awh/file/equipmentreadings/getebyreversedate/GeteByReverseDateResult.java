package au.awh.file.equipmentreadings.getebyreversedate;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 14
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteByReverseDate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteByReverseDateResult implements Serializable
{
	private static final long serialVersionUID = 3829147882917414724L;

	private ReturnCodeEnum _sysReturnCode;
	private BigDecimal readingValue = BigDecimal.ZERO; // BigDecimal 56486
	private String readingComment = ""; // String 56487
	private String userId = ""; // String 4344
	private String equipmentLeasor = ""; // String 56525
	private BigDecimal transferToFromSgt = BigDecimal.ZERO; // BigDecimal 56539
	private String centreCodeKey = ""; // String 1140
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // AwhBuisnessSegmentEnum 56533
	private String equipmentTypeCode = ""; // String 56496
	private LocalDate readingRequiredDate = null; // LocalDate 56555
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO; // BigDecimal 56531
	private ReadingTypeEnum readingType = null; // ReadingTypeEnum 56488
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO; // BigDecimal 60183
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO; // BigDecimal 60184
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO; // BigDecimal 60185

	public BigDecimal getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}
	
		
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	
	
	public ReadingTypeEnum getReadingType() {
		return readingType;
	}
	
	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}
	
	public void setReadingType(String readingType) {
		setReadingType(ReadingTypeEnum.valueOf(readingType));
	}
	
	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}
	
	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}
	
	
	
	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}
	
	
	
	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}
	
	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}
	
	
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
