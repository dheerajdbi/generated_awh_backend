package au.awh.file.equipmentreadings.addembatchreadinghh;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: AddemBatchReadingHh ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class AddemBatchReadingHhParams implements Serializable
{
	private static final long serialVersionUID = -3119448294276051738L;

    private ErrorProcessingEnum errorProcessing = null;
    private long equipReadingBatchNo = 0L;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public long getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(long equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(String equipReadingBatchNo) {
		setEquipReadingBatchNo(Long.parseLong(equipReadingBatchNo));
	}
}
