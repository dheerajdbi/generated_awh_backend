package au.awh.file.equipmentreadings;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 10
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk.WrkEquipmentReadingsWspltwsdfkGDO;
import au.awh.file.equipmentreadings.addmequipmentreadings.AddmEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.prtleasorreadingspmt.PrtLeasorReadingsPmtDTO;
import au.awh.file.equipmentreadings.addmbatchreadinghh.AddmBatchReadingHhDTO;

/**
 * Custom Spring Data JPA repository interface for model: Equipment Readings (WSEQREAD).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentReadingsRepositoryCustom {

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param centreCodeKey Centre Code Key
	 * @param readingType Reading Type
	 * @param readingRequiredDate Reading Required Date
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadings
	 */
	RestResponsePage<EquipmentReadings> getReadingByDate(String awhRegionCode, String centreCodeKey, ReadingTypeEnum readingType, LocalDate readingRequiredDate
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param readingRequiredDate Reading Required Date
	 * @param readingType Reading Type
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadings
	 */
	RestResponsePage<EquipmentReadings> getByReverseDate(String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate, ReadingTypeEnum readingType
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of WrkEquipmentReadingsWspltwsdfkGDO
	 */
	RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> wrkEquipmentReadingsWspltwsdfk(String awhRegionCode, String plantEquipmentCode
	, Pageable pageable);

	/**
	 * 
	 * @param equipmentReadingSgt Equipment Reading Sgt
	 * @return EquipmentReadings
	 */
	EquipmentReadings getEquipmentReadings(long equipmentReadingSgt
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param readingRequiredDate Reading Required Date
	 * @param transferToBuisSeg Transfer to Buis Seg
	 * @param transferToCentre Transfer to Centre
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadings
	 */
	RestResponsePage<EquipmentReadings> tranfSunsqentReadings(String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate, AwhBuisnessSegmentEnum transferToBuisSeg, String transferToCentre
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param centreCodeKey Centre Code Key
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadings
	 */
	RestResponsePage<EquipmentReadings> updReadingTotals(String awhRegionCode, String plantEquipmentCode, String centreCodeKey
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param newPlantEquipmentCode New Plant Equipment Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadings
	 */
	RestResponsePage<EquipmentReadings> rtvupdEquipmentCode(String awhRegionCode, String plantEquipmentCode, String newPlantEquipmentCode
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param readingRequiredDate Reading Required Date
	 * @param pageable a Pageable object used for pagination
	 * @return Page of AddmEquipmentReadingsDTO
	 */
	RestResponsePage<AddmEquipmentReadingsDTO> addmEquipmentReadings(String awhRegionCode, String plantEquipmentCode, LocalDate readingRequiredDate
	, Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PrtLeasorReadingsPmtDTO
	 */
	RestResponsePage<PrtLeasorReadingsPmtDTO> prtLeasorReadingsPmt(
	Pageable pageable);

	/**
	 * 
	 * @param equipReadingBatchNo Equip Reading Batch No.
	 * @param pageable a Pageable object used for pagination
	 * @return Page of AddmBatchReadingHhDTO
	 */
	RestResponsePage<AddmBatchReadingHhDTO> addmBatchReadingHh(long equipReadingBatchNo
	, Pageable pageable);
}
