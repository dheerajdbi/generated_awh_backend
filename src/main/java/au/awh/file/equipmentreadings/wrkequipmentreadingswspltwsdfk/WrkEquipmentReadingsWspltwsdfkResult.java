package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.io.Serializable;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END


/**
 * Result(s) for resource: WrkEquipmentReadingsWspltwsdfk (WSPLTWSDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class WrkEquipmentReadingsWspltwsdfkResult implements Serializable
{
	private static final long serialVersionUID = -2405838515670857681L;

	private ExitProgramOptionEnum exitProgramOption = null;


	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}

}