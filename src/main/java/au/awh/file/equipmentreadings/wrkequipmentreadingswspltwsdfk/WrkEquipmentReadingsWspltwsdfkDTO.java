package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.freschelegacy.utils.RestResponsePage;

import au.awh.file.equipmentreadings.EquipmentReadings;
// generateStatusFieldImportStatements BEGIN 8
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Readings' (WSEQREAD) and function 'Wrk Equipment Readings' (WSPLTWSDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class WrkEquipmentReadingsWspltwsdfkDTO extends BaseDTO {
	private static final long serialVersionUID = 1490638709326758301L;

    private RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String awhRegionCode = "";
	private String awhRegionNameDrv = "";
	private String plantEquipmentCode = "";
	private String equipmentDescription = "";
	private EquipReadingUnitEnum equipReadingUnit = null;
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL;
	private String equipmentLeasor = "";


	private WrkEquipmentReadingsWspltwsdfkGDO gdo;

    public WrkEquipmentReadingsWspltwsdfkDTO() {

    }

	public WrkEquipmentReadingsWspltwsdfkDTO(String awhRegionCode, String plantEquipmentCode) {
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
	}

    public void setPageGdo(RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
    }

    public String getAwhRegionNameDrv() {
    	return awhRegionNameDrv;
    }

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
    }

    public String getPlantEquipmentCode() {
    	return plantEquipmentCode;
    }

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
    }

    public String getEquipmentDescription() {
    	return equipmentDescription;
    }

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
    }

    public EquipReadingUnitEnum getEquipReadingUnit() {
    	return equipReadingUnit;
    }

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
    }

    public String getEquipmentLeasor() {
    	return equipmentLeasor;
    }
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
    	return exitProgramOption;
    }
	public void setGdo(WrkEquipmentReadingsWspltwsdfkGDO gdo) {
		this.gdo = gdo;
	}

	public WrkEquipmentReadingsWspltwsdfkGDO getGdo() {
		return gdo;
	}

}