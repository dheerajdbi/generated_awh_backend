package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsService;
import au.awh.file.equipmentreadings.dspeequipmentreading.DspeEquipmentReadingService;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsService;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsService;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.dspeequipmentreading.DspeEquipmentReadingDTO;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsDTO;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsParams;
import au.awh.file.equipmentreadings.dspeequipmentreading.DspeEquipmentReadingParams;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsParams;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsParams;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsResult;
import au.awh.file.equipmentreadings.dspeequipmentreading.DspeEquipmentReadingResult;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsResult;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsResult;
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


// generateImportsForEnum
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 29
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * DSPFIL Service controller for 'Wrk Equipment Readings' (WSPLTWSDFK) of file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class WrkEquipmentReadingsWspltwsdfkService extends AbstractService<WrkEquipmentReadingsWspltwsdfkService, WrkEquipmentReadingsWspltwsdfkState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private EquipmentReadingsRepository equipmentReadingsRepository;
    
    @Autowired
    private AddemEquipmentReadingsService addemEquipmentReadingsService;
    
    @Autowired
    private DspeEquipmentReadingService dspeEquipmentReadingService;
    
    @Autowired
    private EdteEquipmentReadingsService edteEquipmentReadingsService;
    
    @Autowired
    private GeteEquipmentReadingsService geteEquipmentReadingsService;
    
    @Autowired
    private GetePlantEquipmentService getePlantEquipmentService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "wrkEquipmentReadingsWspltwsdfk";
    public static final String SCREEN_RCD = "WrkEquipmentReadingsWspltwsdfk.rcd";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", WrkEquipmentReadingsWspltwsdfkParams.class, this::executeService);
    private final Step response = define("response", WrkEquipmentReadingsWspltwsdfkDTO.class, this::processResponse);

	//private final Step serviceDspeEquipmentReading = define("serviceDspeEquipmentReading",DspeEquipmentReadingResult.class, this::processServiceDspeEquipmentReading);
	//private final Step serviceAddemEquipmentReadings = define("serviceAddemEquipmentReadings",AddemEquipmentReadingsResult.class, this::processServiceAddemEquipmentReadings);
	//private final Step serviceGetePlantEquipment = define("serviceGetePlantEquipment",GetePlantEquipmentResult.class, this::processServiceGetePlantEquipment);
	//private final Step serviceGeteEquipmentReadings = define("serviceGeteEquipmentReadings",GeteEquipmentReadingsResult.class, this::processServiceGeteEquipmentReadings);
	//private final Step serviceEdteEquipmentReadings = define("serviceEdteEquipmentReadings",EdteEquipmentReadingsResult.class, this::processServiceEdteEquipmentReadings);
	

    
    @Autowired
    public WrkEquipmentReadingsWspltwsdfkService() {
        super(WrkEquipmentReadingsWspltwsdfkService.class, WrkEquipmentReadingsWspltwsdfkState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(WrkEquipmentReadingsWspltwsdfkState state, WrkEquipmentReadingsWspltwsdfkParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_CTL initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(WrkEquipmentReadingsWspltwsdfkState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Load pageGdo
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(WrkEquipmentReadingsWspltwsdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrInitializeSubfileControl(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
		    stepResult = loadNextSubfilePage(state);
		}

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(WrkEquipmentReadingsWspltwsdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	List<WrkEquipmentReadingsWspltwsdfkGDO> list = state.getPageGdo().getContent();
        for (WrkEquipmentReadingsWspltwsdfkGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            stepResult = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//            TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//            TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(WrkEquipmentReadingsWspltwsdfkState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            WrkEquipmentReadingsWspltwsdfkDTO model = new WrkEquipmentReadingsWspltwsdfkDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(WrkEquipmentReadingsWspltwsdfkState state, WrkEquipmentReadingsWspltwsdfkDTO model) {
    	StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (isHelp(state.get_SysCmdKey())) {
//        TODO:processHelpRequest(state);//synon built-in function
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(WrkEquipmentReadingsWspltwsdfkState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrProcessSubfilePreConfirm(state);
        if ( stepResult != NO_ACTION) {
            return stepResult;
        }

        if (state.get_SysErrorFound()) {
            return closedown(state);
        } else {
            if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
                return closedown(state);
            } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	stepResult = usrProcessCommandKeys(state);
//		        }

                stepResult = usrProcessSubfileControlPostConfirm(state);
                for (WrkEquipmentReadingsWspltwsdfkGDO gdo : state.getPageGdo().getContent()) {
                    if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                        stepResult = usrProcessSubfileRecordPostConfirm(state, gdo);
                        
//	                    TODO:writeSubfileRecord(state);   // synon built-in function
		            }
		        }
		        stepResult = usrFinalProcessingPostConfirm(state);
                stepResult = usrProcessCommandKeys(state);
            }
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult usrProcessSubfilePreConfirm(WrkEquipmentReadingsWspltwsdfkState state) {
    	StepResult stepResult = NO_ACTION;

	stepResult = usrSubfileControlFunctionFields(state);
        stepResult = usrProcessSubfileControlPreConfirm(state);
    	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
            for (WrkEquipmentReadingsWspltwsdfkGDO gdo : state.getPageGdo().getContent()) {
                
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = usrSubfileRecordFunctionFields(state, gdo);
                    stepResult = usrProcessSubfileRecordPreConfirm(state, gdo);
                    if (stepResult != NO_ACTION) {
                        return stepResult;
                    }
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
                
            }
        }

	stepResult = usrFinalProcessingPreConfirm(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(WrkEquipmentReadingsWspltwsdfkState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        WrkEquipmentReadingsWspltwsdfkResult params = new WrkEquipmentReadingsWspltwsdfkResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(WrkEquipmentReadingsWspltwsdfkState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
	private void dbfReadNextPageRecord(WrkEquipmentReadingsWspltwsdfkState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(WrkEquipmentReadingsWspltwsdfkState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<WrkEquipmentReadingsWspltwsdfkGDO> pageGdo = equipmentReadingsRepository.wrkEquipmentReadingsWspltwsdfk(state.getAwhRegionCode(), state.getPlantEquipmentCode(), pageable);
        state.setPageGdo(pageGdo);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GetePlantEquipmentResult getePlantEquipmentResult = null;
			GetePlantEquipmentParams getePlantEquipmentParams = null;
			// DEBUG genFunctionCall BEGIN 1000558 ACT LCL.First Pass = CND.First Pass
			dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000658 ACT GetE Plant Equipment - Plant Equipment  *
			getePlantEquipmentParams = new GetePlantEquipmentParams();
			getePlantEquipmentResult = new GetePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			
			//getting null pointer-
			
			
			//BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), getePlantEquipmentParams);
			getePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
 			getePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			getePlantEquipmentParams.setErrorProcessing("2");
			getePlantEquipmentParams.setGetRecordOption("");
			stepResult = getePlantEquipmentService.execute(getePlantEquipmentParams);
			getePlantEquipmentResult = (GetePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getePlantEquipmentResult.get_SysReturnCode());
			dto.setEquipmentDescription(getePlantEquipmentResult.getEquipmentDescription());
			dto.setEquipReadingUnit(getePlantEquipmentResult.getEquipReadingUnit());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000696 ACT AWH region Name Drv      *FIELD                                             CTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             CTLA 1000696
			// DEBUG X2EActionDiagramElement 1000696 ACT     AWH region Name Drv      *FIELD                                             CTLA
			// DEBUG X2EActionDiagramElement 1000697 PAR CTL 
			// DEBUG X2EActionDiagramElement 1000698 PAR CTL 
			// DEBUG genFunctionCall END
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(WrkEquipmentReadingsWspltwsdfkState dto, WrkEquipmentReadingsWspltwsdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteEquipmentReadingsParams geteEquipmentReadingsParams = null;
			GeteEquipmentReadingsResult geteEquipmentReadingsResult = null;
			if ((dto.getLclFirstPass() == FirstPassEnum._FIRST_PASS) && !(isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000569 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000573 ACT LCL.First Pass = CND.Not First Pass
				dto.setLclFirstPass(FirstPassEnum._NOT_FIRST_PASS);
				// DEBUG genFunctionCall END
				// 
			} else if ((dto.getLclFirstPass() == FirstPassEnum._NOT_FIRST_PASS) && (isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000584 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000588 ACT LCL.First Pass = CND.First Pass
				dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000626 ACT RCD.*Condition name = Condition name of RCD.Reading Type
			gdo.set_SysConditionName(gdo.getReadingType().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			if (ReadingTypeEnum.isTransfer(gdo.getReadingType())) {
				// RCD.Reading Type is Transfer
				// DEBUG genFunctionCall BEGIN 1000676 ACT GetE Equipment Readings - Equipment Readings  *
				geteEquipmentReadingsParams = new GeteEquipmentReadingsParams();
				geteEquipmentReadingsResult = new GeteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteEquipmentReadingsParams);
				geteEquipmentReadingsParams.setEquipmentReadingSgt(gdo.getTransferToFromSgt());
				geteEquipmentReadingsParams.setErrorProcessing("1");
				geteEquipmentReadingsParams.setGetRecordOption("");
				stepResult = geteEquipmentReadingsService.execute(geteEquipmentReadingsParams);
				geteEquipmentReadingsResult = (GeteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteEquipmentReadingsResult.get_SysReturnCode());
				gdo.setTransferToCentre(geteEquipmentReadingsResult.getCentreCodeKey());
				gdo.setTransferToBuisSeg(geteEquipmentReadingsResult.getAwhBuisnessSegment());
				// DEBUG genFunctionCall END
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000634 ACT AWH region Name Drv      *FIELD                                             CTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             CTLA 1000634
			// DEBUG X2EActionDiagramElement 1000634 ACT     AWH region Name Drv      *FIELD                                             CTLA
			// DEBUG X2EActionDiagramElement 1000635 PAR CTL 
			// DEBUG X2EActionDiagramElement 1000636 PAR CTL 
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	AddemEquipmentReadingsParams addemEquipmentReadingsParams = null;
			AddemEquipmentReadingsResult addemEquipmentReadingsResult = null;
			if (isAdd(dto.get_SysCmdKey())) {
				// CTL.*CMD key is Add
				// DEBUG genFunctionCall BEGIN 1000648 ACT AddE:M Equipment Readings - Equipment Readings  *
				addemEquipmentReadingsParams = new AddemEquipmentReadingsParams();
				addemEquipmentReadingsResult = new AddemEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addemEquipmentReadingsParams);
				addemEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
				addemEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				addemEquipmentReadingsParams.setErrorProcessing("2");
				addemEquipmentReadingsParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
				// TODO SPLIT FUNCTION DSPFIL EquipmentReadings.wrkEquipmentReadingsWspltwsdfk (72) -> EXCINTFUN EquipmentReadings.addemEquipmentReadings
				stepResult = addemEquipmentReadingsService.execute(addemEquipmentReadingsParams);
				addemEquipmentReadingsResult = (AddemEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(addemEquipmentReadingsResult.get_SysReturnCode());
				dto.setLclExitProgramOption(addemEquipmentReadingsResult.getExitProgramOption());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000654 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
			if (isCancel_1(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000042 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000046 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
			// * Condition for normal processing of subfile records.
			// DEBUG genFunctionCall BEGIN 1000340 ACT LCL.Exit Program Option = CND.Normal
			dto.setLclExitProgramOption(ExitProgramOptionEnum._NORMAL);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(WrkEquipmentReadingsWspltwsdfkState dto, WrkEquipmentReadingsWspltwsdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(WrkEquipmentReadingsWspltwsdfkState dto, WrkEquipmentReadingsWspltwsdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	EdteEquipmentReadingsResult edteEquipmentReadingsResult = null;
			DspeEquipmentReadingResult dspeEquipmentReadingResult = null;
			EdteEquipmentReadingsParams edteEquipmentReadingsParams = null;
			DspeEquipmentReadingParams dspeEquipmentReadingParams = null;
			if (!(gdo.get_SysSelected().equals("Not entered"))) {
				// NOT RCD.*SFLSEL is Not entered
				// DEBUG genFunctionCall BEGIN 1000360 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
			}
			if (gdo.get_SysSelected().equals("Change") || gdo.get_SysSelected().equals("2")) {
				// RCD.*SFLSEL is Edit
				// DEBUG genFunctionCall BEGIN 1000705 ACT EdtE Equipment Readings - Equipment Readings  *
				edteEquipmentReadingsParams = new EdteEquipmentReadingsParams();
				edteEquipmentReadingsResult = new EdteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), edteEquipmentReadingsParams);
				edteEquipmentReadingsParams.setErrorProcessing("2");
				edteEquipmentReadingsParams.setEquipmentReadingSgt(gdo.getEquipmentReadingSgt());
				edteEquipmentReadingsParams.setProgramMode("CHG");
				// TODO SPLIT FUNCTION DSPFIL EquipmentReadings.wrkEquipmentReadingsWspltwsdfk (101) -> EXCINTFUN EquipmentReadings.edteEquipmentReadings
				stepResult = edteEquipmentReadingsService.execute(edteEquipmentReadingsParams);
				edteEquipmentReadingsResult = (EdteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(edteEquipmentReadingsResult.get_SysReturnCode());
				dto.setLclExitProgramOption(edteEquipmentReadingsResult.getExitProgramOption());
				// DEBUG genFunctionCall END
				if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._EXIT_REQUESTED) {
					// LCL.Exit Program Option is Exit requested
					// * NOTE: This is only required where fast exit is to be enabled.
					// * Remove this condition check when fast exit is not required.
					// * All displays that are the first from menu must ignore fast exit.
					// DEBUG genFunctionCall BEGIN 1000720 ACT PAR.Exit Program Option = CND.Exit requested
					dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000724 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// * Ignore exit program option.
					// DEBUG genFunctionCall BEGIN 1000731 ACT PAR.Exit Program Option = CND.Normal
					dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000735 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
			if (gdo.get_SysSelected().equals("5#1") || gdo.get_SysSelected().equals("5#2")) {
				// RCD.*SFLSEL is Display
				// DEBUG genFunctionCall BEGIN 1000485 ACT DspE Equipment Reading - Equipment Readings  *
				dspeEquipmentReadingParams = new DspeEquipmentReadingParams();
				dspeEquipmentReadingResult = new DspeEquipmentReadingResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), dspeEquipmentReadingParams);
				dspeEquipmentReadingParams.setErrorProcessing("2");
				dspeEquipmentReadingParams.setEquipmentReadingSgt(gdo.getEquipmentReadingSgt());
				// TODO SPLIT FUNCTION DSPFIL EquipmentReadings.wrkEquipmentReadingsWspltwsdfk (101) -> EXCINTFUN EquipmentReadings.dspeEquipmentReading
				stepResult = dspeEquipmentReadingService.execute(dspeEquipmentReadingParams);
				dspeEquipmentReadingResult = (DspeEquipmentReadingResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(dspeEquipmentReadingResult.get_SysReturnCode());
				dto.setLclExitProgramOption(dspeEquipmentReadingResult.getExitProgramOption());
				// DEBUG genFunctionCall END
				if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._EXIT_REQUESTED) {
					// LCL.Exit Program Option is Exit requested
					// * NOTE: This is only required where fast exit is to be enabled.
					// * Remove this condition check when fast exit is not required.
					// * All displays that are the first from menu must ignore fast exit.
					// DEBUG genFunctionCall BEGIN 1000498 ACT PAR.Exit Program Option = CND.Exit requested
					dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000502 ACT Exit program - return code CND.*Normal
					// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// * Ignore exit program option.
					// DEBUG genFunctionCall BEGIN 1000509 ACT PAR.Exit Program Option = CND.Normal
					dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000513 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(WrkEquipmentReadingsWspltwsdfkState dto, WrkEquipmentReadingsWspltwsdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(WrkEquipmentReadingsWspltwsdfkState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        	
			if (isExit(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000251 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * DspeEquipmentReadingService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDspeEquipmentReading(WrkEquipmentReadingsWspltwsdfkState state, DspeEquipmentReadingParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwsdfkParams wrkEquipmentReadingsWspltwsdfkParams = new WrkEquipmentReadingsWspltwsdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwsdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class, wrkEquipmentReadingsWspltwsdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * AddemEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAddemEquipmentReadings(WrkEquipmentReadingsWspltwsdfkState state, AddemEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwsdfkParams wrkEquipmentReadingsWspltwsdfkParams = new WrkEquipmentReadingsWspltwsdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwsdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class, wrkEquipmentReadingsWspltwsdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetePlantEquipment(WrkEquipmentReadingsWspltwsdfkState state, GetePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwsdfkParams wrkEquipmentReadingsWspltwsdfkParams = new WrkEquipmentReadingsWspltwsdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwsdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class, wrkEquipmentReadingsWspltwsdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipmentReadings(WrkEquipmentReadingsWspltwsdfkState state, GeteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwsdfkParams wrkEquipmentReadingsWspltwsdfkParams = new WrkEquipmentReadingsWspltwsdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwsdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class, wrkEquipmentReadingsWspltwsdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * EdteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEdteEquipmentReadings(WrkEquipmentReadingsWspltwsdfkState state, EdteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkEquipmentReadingsWspltwsdfkParams wrkEquipmentReadingsWspltwsdfkParams = new WrkEquipmentReadingsWspltwsdfkParams();
//        BeanUtils.copyProperties(state, wrkEquipmentReadingsWspltwsdfkParams);
//        stepResult = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class, wrkEquipmentReadingsWspltwsdfkParams);
//
//        return stepResult;
//    }
//


}
