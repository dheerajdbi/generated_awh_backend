package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.equipmentreadings.EquipmentReadings;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import au.awh.config.LocalDateDeserializer;
import au.awh.config.LocalDateSerializer;

// generateStatusFieldImportStatements BEGIN 20
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;


/**
 * Gdo for file 'Equipment Readings' (WSEQREAD) and function 'Wrk Equipment Readings' (WSPLTWSDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class WrkEquipmentReadingsWspltwsdfkGDO implements Serializable {
	private static final long serialVersionUID = -6254705629623794688L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate readingRequiredDate = null;
	private String awhRegionCode = "";
	private String plantEquipmentCode = "";
	private LocalDate readingTimestamp = null;
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
	private ReadingTypeEnum readingType = null;
	private String conditionName = "";
	private BigDecimal readingValue = BigDecimal.ZERO;
	private String userId = "";
	private String equipmentLeasor = "";
	private BigDecimal transferToFromSgt = BigDecimal.ZERO;
	private String centreCodeKey = "";
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
	private AwhBuisnessSegmentEnum transferToBuisSeg = null;
	private String transferToCentre = "";
	private String equipmentTypeCode = "";
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO;
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO;
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO;
	private String readingComment = "";
	private String _SysconditionName = "";

	public WrkEquipmentReadingsWspltwsdfkGDO() {

	}
	
   	public WrkEquipmentReadingsWspltwsdfkGDO(LocalDate readingRequiredDate, String awhRegionCode, String plantEquipmentCode, LocalDate readingTimestamp, BigDecimal equipmentReadingSgt, ReadingTypeEnum readingType, BigDecimal readingValue, String userId, String equipmentLeasor, BigDecimal transferToFromSgt, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, BigDecimal differenceFromPrevious, BigDecimal idleDaysFromPrevious, BigDecimal unavailableDaysFromPrv, String readingComment) {
		this.readingRequiredDate = readingRequiredDate;
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.readingTimestamp = readingTimestamp;
		this.equipmentReadingSgt = equipmentReadingSgt;
		this.readingType = readingType;
		this.readingValue = readingValue;
		this.userId = userId;
		this.equipmentLeasor = equipmentLeasor;
		this.transferToFromSgt = transferToFromSgt;
		this.centreCodeKey = centreCodeKey;
		this.awhBuisnessSegment = awhBuisnessSegment;
		this.equipmentTypeCode = equipmentTypeCode;
		this.differenceFromPrevious = differenceFromPrevious;
		this.idleDaysFromPrevious = idleDaysFromPrevious;
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
		this.readingComment = readingComment;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
    	this.readingRequiredDate = readingRequiredDate;
    }

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setAwhRegionCode(String awhRegionCode) {
    	this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
    	this.plantEquipmentCode = plantEquipmentCode;
    }

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setReadingTimestamp(LocalDate readingTimestamp) {
    	this.readingTimestamp = readingTimestamp;
    }

	public LocalDate getReadingTimestamp() {
		return readingTimestamp;
	}

	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
    	this.equipmentReadingSgt = equipmentReadingSgt;
    }

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
    	this.readingType = readingType;
    }

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public void setConditionName(String conditionName) {
    	this.conditionName = conditionName;
    }

	public String getConditionName() {
		return conditionName;
	}

	public void setReadingValue(BigDecimal readingValue) {
    	this.readingValue = readingValue;
    }

	public BigDecimal getReadingValue() {
		return readingValue;
	}

	public void setUserId(String userId) {
    	this.userId = userId;
    }

	public String getUserId() {
		return userId;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
    	this.equipmentLeasor = equipmentLeasor;
    }

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
    	this.transferToFromSgt = transferToFromSgt;
    }

	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}

	public void setCentreCodeKey(String centreCodeKey) {
    	this.centreCodeKey = centreCodeKey;
    }

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
    	this.awhBuisnessSegment = awhBuisnessSegment;
    }

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public void setTransferToBuisSeg(AwhBuisnessSegmentEnum transferToBuisSeg) {
    	this.transferToBuisSeg = transferToBuisSeg;
    }

	public AwhBuisnessSegmentEnum getTransferToBuisSeg() {
		return transferToBuisSeg;
	}

	public void setTransferToCentre(String transferToCentre) {
    	this.transferToCentre = transferToCentre;
    }

	public String getTransferToCentre() {
		return transferToCentre;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
    	this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
    	this.differenceFromPrevious = differenceFromPrevious;
    }

	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
    	this.idleDaysFromPrevious = idleDaysFromPrevious;
    }

	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
    	this.unavailableDaysFromPrv = unavailableDaysFromPrv;
    }

	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public void setReadingComment(String readingComment) {
    	this.readingComment = readingComment;
    }

	public String getReadingComment() {
		return readingComment;
	}

	public void set_SysConditionName(String conditionName) {
    	this._SysconditionName = conditionName;
    }

	public String get_SysConditionName() {
		return _SysconditionName;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}