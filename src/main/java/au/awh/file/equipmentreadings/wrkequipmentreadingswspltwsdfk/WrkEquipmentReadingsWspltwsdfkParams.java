package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.io.Serializable;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END


/**
 * Parameter(s) for resource: WrkEquipmentReadingsWspltwsdfk (WSPLTWSDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class WrkEquipmentReadingsWspltwsdfkParams implements Serializable
{
	private static final long serialVersionUID = 5574352840155353002L;

	private String awhRegionCode = "";
	private String plantEquipmentCode = "";


	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

}