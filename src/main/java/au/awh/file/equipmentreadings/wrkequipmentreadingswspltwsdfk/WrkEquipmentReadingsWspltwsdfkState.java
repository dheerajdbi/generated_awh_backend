package au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;

// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Equipment Readings' (WSEQREAD) and function 'Wrk Equipment Readings' (WSPLTWSDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
 public class WrkEquipmentReadingsWspltwsdfkState extends WrkEquipmentReadingsWspltwsdfkDTO {
	private static final long serialVersionUID = -1258872421000199897L;

	// Local fields
	private FirstPassEnum lclFirstPass = null;
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL;

	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public WrkEquipmentReadingsWspltwsdfkState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
    	this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
    	return lclExitProgramOption;
    }
	public void setLclFirstPass(FirstPassEnum firstPass) {
    	this.lclFirstPass = firstPass;
    }

    public FirstPassEnum getLclFirstPass() {
    	return lclFirstPass;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}