package au.awh.file.equipmentreadings.chgcrteequipmentreading;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 14
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgcrteEquipmentReading ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgcrteEquipmentReadingParams implements Serializable
{
	private static final long serialVersionUID = 2629894417854646150L;

    private ErrorProcessingEnum errorProcessing = null;
    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private ReadingTypeEnum readingType = null;
    private BigDecimal readingValue = BigDecimal.ZERO;
    private String readingComment = "";
    private String userId = "";
    private String equipmentLeasor = "";
    private BigDecimal transferToFromSgt = BigDecimal.ZERO;
    private String centreCodeKey = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String equipmentTypeCode = "";
    private LocalDate readingRequiredDate = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public ReadingTypeEnum getReadingType() {
		return readingType;
	}
	
	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}
	
	public void setReadingType(String readingType) {
		setReadingType(ReadingTypeEnum.valueOf(readingType));
	}
	
	public BigDecimal getReadingValue() {
		return readingValue;
	}
	
	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}
	
	
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
}
