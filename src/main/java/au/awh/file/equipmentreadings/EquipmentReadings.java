package au.awh.file.equipmentreadings;

import java.io.Serializable;
import au.awh.utils.DateConverters;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.utils.DateConverters;

@Entity
@Table(name="WSEQREAD")
public class EquipmentReadings implements Serializable {
	
	@EmbeddedId
	private EquipmentReadingsId id = new EquipmentReadingsId();

	@Column(name = "ERAWHBSG")
	private AwhBuisnessSegmentEnum awhBuisnessSegment = AwhBuisnessSegmentEnum.fromCode("");
	
	@Column(name = "ERAWHREG")
	private String awhRegionCode = "";
	
	@Column(name = "ERCTRCDEK")
	private String centreCodeKey = "";
	
	@Column(name = "ERDIFFVL")
	private BigDecimal differenceFromPrevious = BigDecimal.ZERO;
	
	@Column(name = "EREQLEASOR")
	private String equipmentLeasor = "";
	
	@Column(name = "ERETCDE")
	private String equipmentTypeCode = "";
	
	@Column(name = "ERIDLEDYS")
	private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO;
	
	@Column(name = "EREQCODE")
	private String plantEquipmentCode = "";
	
	@Column(name = "ERRDCOM")
	private String readingComment = "";
	
	@Column(name = "ERREADDREQ")
	@Convert(converter = DateConverters.class)
	private LocalDate readingRequiredDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "ERAHTS")
	@Convert(converter = DateConverters.class)
	private LocalDateTime readingTimestamp = LocalDateTime.of(1801, 1, 1, 0, 0);
	
	@Column(name = "ERRDTYPE")
	private ReadingTypeEnum readingType = ReadingTypeEnum.fromCode("");
	
	@Column(name = "ERRDVAL")
	private BigDecimal readingValue = BigDecimal.ZERO;
	
	@Column(name = "EREQTRSGT")
	private BigDecimal transferToFromSgt = BigDecimal.ZERO;
	
	@Column(name = "ERUNAVDY") 
	private BigDecimal unavailableDaysFromPrv = BigDecimal.ZERO;
	
	@Column(name = "ERUSRID")
	private String userId = "";

	public EquipmentReadingsId getId() {
		return id;
	}

	public BigDecimal getEquipmentReadingSgt() {
		return id.getEquipmentReadingSgt();
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public LocalDateTime getReadingTimestamp() {
		return readingTimestamp;
	}
	
	public ReadingTypeEnum getReadingType() {
		return readingType;
	}
	
	public BigDecimal getReadingValue() {
		return readingValue;
	}
	
	public String getReadingComment() {
		return readingComment;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public BigDecimal getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	

	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}
	
	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}
	
	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	

	

	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		this.id.setEquipmentReadingSgt(equipmentReadingSgt);
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public void setReadingTimestamp(LocalDateTime readingTimestamp) {
		this.readingTimestamp = readingTimestamp;
	}
	
	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
	}
	
	public void setReadingValue(BigDecimal readingValue) {
		this.readingValue = readingValue;
	}
	
	public void setReadingComment(String readingComment) {
		this.readingComment = readingComment;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public void setTransferToFromSgt(BigDecimal transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}
	
	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
