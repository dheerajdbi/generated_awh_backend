package au.awh.file.equipmentreadings.chgesegmentandcentre;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeSegmentAndCentre ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeSegmentAndCentreResult implements Serializable
{
	private static final long serialVersionUID = -1837794427395489745L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
