package au.awh.file.equipmentreadings.chgesegmentandcentre;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=chgeSegmentAndCentre (2027013) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.chgsegmentandcentre.ChgSegmentAndCentreService;
// imports for DTO
import au.awh.file.equipmentreadings.chgsegmentandcentre.ChgSegmentAndCentreDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.chgsegmentandcentre.ChgSegmentAndCentreParams;
// imports for Results
import au.awh.file.equipmentreadings.chgsegmentandcentre.ChgSegmentAndCentreResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ChgE Segment and Centre' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ChgeSegmentAndCentreService extends AbstractService<ChgeSegmentAndCentreService, ChgeSegmentAndCentreDTO>
{
	private final Step execute = define("execute", ChgeSegmentAndCentreParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private ChgSegmentAndCentreService chgSegmentAndCentreService;

	//private final Step serviceChgSegmentAndCentre = define("serviceChgSegmentAndCentre",ChgSegmentAndCentreResult.class, this::processServiceChgSegmentAndCentre);
	
	@Autowired
	public ChgeSegmentAndCentreService() {
		super(ChgeSegmentAndCentreService.class, ChgeSegmentAndCentreDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgeSegmentAndCentreParams params) throws ServiceException {
		ChgeSegmentAndCentreDTO dto = new ChgeSegmentAndCentreDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgeSegmentAndCentreDTO dto, ChgeSegmentAndCentreParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ChgSegmentAndCentreParams chgSegmentAndCentreParams = null;
		ChgSegmentAndCentreResult chgSegmentAndCentreResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Chg Segment and Centre - Equipment Readings  *
		chgSegmentAndCentreParams = new ChgSegmentAndCentreParams();
		chgSegmentAndCentreResult = new ChgSegmentAndCentreResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, chgSegmentAndCentreParams);
		chgSegmentAndCentreParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		chgSegmentAndCentreParams.setCentreCodeKey(dto.getCentreCodeKey());
		chgSegmentAndCentreParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		stepResult = chgSegmentAndCentreService.execute(chgSegmentAndCentreParams);
		chgSegmentAndCentreResult = (ChgSegmentAndCentreResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(chgSegmentAndCentreResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 2027013 EquipmentReadings.chgeSegmentAndCentre 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		ChgeSegmentAndCentreResult result = new ChgeSegmentAndCentreResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ChgSegmentAndCentreService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgSegmentAndCentre(ChgeSegmentAndCentreDTO dto, ChgSegmentAndCentreParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgeSegmentAndCentreParams chgeSegmentAndCentreParams = new ChgeSegmentAndCentreParams();
//        BeanUtils.copyProperties(dto, chgeSegmentAndCentreParams);
//        stepResult = StepResult.callService(ChgeSegmentAndCentreService.class, chgeSegmentAndCentreParams);
//
//        return stepResult;
//    }
//
}
