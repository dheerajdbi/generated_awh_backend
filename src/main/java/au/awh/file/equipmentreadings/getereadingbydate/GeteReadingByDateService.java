package au.awh.file.equipmentreadings.getereadingbydate;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=geteReadingByDate (1878815) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.getreadingbydate.GetReadingByDateService;
// imports for DTO
import au.awh.file.equipmentreadings.getreadingbydate.GetReadingByDateDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.getreadingbydate.GetReadingByDateParams;
// imports for Results
import au.awh.file.equipmentreadings.getreadingbydate.GetReadingByDateResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Reading by Date' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteReadingByDateService extends AbstractService<GeteReadingByDateService, GeteReadingByDateDTO>
{
	private final Step execute = define("execute", GeteReadingByDateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private GetReadingByDateService getReadingByDateService;

	//private final Step serviceGetReadingByDate = define("serviceGetReadingByDate",GetReadingByDateResult.class, this::processServiceGetReadingByDate);
	
	@Autowired
	public GeteReadingByDateService() {
		super(GeteReadingByDateService.class, GeteReadingByDateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteReadingByDateParams params) throws ServiceException {
		GeteReadingByDateDTO dto = new GeteReadingByDateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteReadingByDateDTO dto, GeteReadingByDateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetReadingByDateResult getReadingByDateResult = null;
		GetReadingByDateParams getReadingByDateParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Reading by Date - Equipment Readings  *
		getReadingByDateParams = new GetReadingByDateParams();
		getReadingByDateResult = new GetReadingByDateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getReadingByDateParams);
		getReadingByDateParams.setAwhRegionCode(dto.getAwhRegionCode());
		getReadingByDateParams.setReadingType(dto.getReadingType());
		getReadingByDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		getReadingByDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
		getReadingByDateParams.setCentreCodeKey(dto.getCentreCodeKey());
		stepResult = getReadingByDateService.execute(getReadingByDateParams);
		getReadingByDateResult = (GetReadingByDateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getReadingByDateResult.get_SysReturnCode());
		dto.setEquipmentReadingSgt(getReadingByDateResult.getEquipmentReadingSgt());
		dto.setReadingTimestamp(getReadingByDateResult.getReadingTimestamp());
		dto.setReadingValue(getReadingByDateResult.getReadingValue());
		dto.setReadingComment(getReadingByDateResult.getReadingComment());
		dto.setUserId(getReadingByDateResult.getUserId());
		dto.setEquipmentLeasor(getReadingByDateResult.getEquipmentLeasor());
		dto.setTransferToFromSgt(getReadingByDateResult.getTransferToFromSgt());
		dto.setAwhBuisnessSegment(getReadingByDateResult.getAwhBuisnessSegment());
		dto.setEquipmentTypeCode(getReadingByDateResult.getEquipmentTypeCode());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// * NOTE: Insert <file> NF message here.
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Equipment Readings     NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878067) EXCINTFUN 1878815 EquipmentReadings.geteReadingByDate 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878815 EquipmentReadings.geteReadingByDate 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteReadingByDateResult result = new GeteReadingByDateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetReadingByDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetReadingByDate(GeteReadingByDateDTO dto, GetReadingByDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteReadingByDateParams geteReadingByDateParams = new GeteReadingByDateParams();
//        BeanUtils.copyProperties(dto, geteReadingByDateParams);
//        stepResult = StepResult.callService(GeteReadingByDateService.class, geteReadingByDateParams);
//
//        return stepResult;
//    }
//
}
