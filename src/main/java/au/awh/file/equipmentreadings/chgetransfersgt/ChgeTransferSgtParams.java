package au.awh.file.equipmentreadings.chgetransfersgt;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgeTransferSgt ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeTransferSgtParams implements Serializable
{
	private static final long serialVersionUID = 1643956537970150811L;

    private ErrorProcessingEnum errorProcessing = null;
    private long equipmentReadingSgt = 0L;
    private long transferToFromSgt = 0L;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(String equipmentReadingSgt) {
		setEquipmentReadingSgt(Long.parseLong(equipmentReadingSgt));
	}
	
	public long getTransferToFromSgt() {
		return transferToFromSgt;
	}
	
	public void setTransferToFromSgt(long transferToFromSgt) {
		this.transferToFromSgt = transferToFromSgt;
	}
	
	public void setTransferToFromSgt(String transferToFromSgt) {
		setTransferToFromSgt(Long.parseLong(transferToFromSgt));
	}
}
