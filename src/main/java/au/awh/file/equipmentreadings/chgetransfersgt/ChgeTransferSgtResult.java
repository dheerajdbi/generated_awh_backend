package au.awh.file.equipmentreadings.chgetransfersgt;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeTransferSgt ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeTransferSgtResult implements Serializable
{
	private static final long serialVersionUID = -7846454733997847279L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
