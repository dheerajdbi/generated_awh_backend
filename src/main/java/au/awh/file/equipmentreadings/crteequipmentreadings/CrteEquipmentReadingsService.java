package au.awh.file.equipmentreadings.crteequipmentreadings;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=crteEquipmentReadings (1878852) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsService;
// imports for DTO
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsParams;
// imports for Results
import au.awh.file.equipmentreadings.createequipmentreadings.CreateEquipmentReadingsResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'CrtE Equipment Readings' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class CrteEquipmentReadingsService extends AbstractService<CrteEquipmentReadingsService, CrteEquipmentReadingsDTO>
{
	private final Step execute = define("execute", CrteEquipmentReadingsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private CreateEquipmentReadingsService createEquipmentReadingsService;

	//private final Step serviceCreateEquipmentReadings = define("serviceCreateEquipmentReadings",CreateEquipmentReadingsResult.class, this::processServiceCreateEquipmentReadings);
	
	@Autowired
	public CrteEquipmentReadingsService() {
		super(CrteEquipmentReadingsService.class, CrteEquipmentReadingsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CrteEquipmentReadingsParams params) throws ServiceException {
		CrteEquipmentReadingsDTO dto = new CrteEquipmentReadingsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CrteEquipmentReadingsDTO dto, CrteEquipmentReadingsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		CreateEquipmentReadingsResult createEquipmentReadingsResult = null;
		CreateEquipmentReadingsParams createEquipmentReadingsParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000274 ACT Create Equipment Readings - Equipment Readings  *
		createEquipmentReadingsParams = new CreateEquipmentReadingsParams();
		createEquipmentReadingsResult = new CreateEquipmentReadingsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, createEquipmentReadingsParams);
		createEquipmentReadingsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		createEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
		createEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		createEquipmentReadingsParams.setReadingTimestamp(job.getSystemTimestamp());
		createEquipmentReadingsParams.setReadingType(dto.getReadingType());
		createEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
		createEquipmentReadingsParams.setReadingComment(dto.getReadingComment());
		createEquipmentReadingsParams.setUserId(job.getUser());
		createEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
		createEquipmentReadingsParams.setTransferToFromSgt(dto.getTransferToFromSgt());
		createEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
		createEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		createEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		createEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
		stepResult = createEquipmentReadingsService.execute(createEquipmentReadingsParams);
		createEquipmentReadingsResult = (CreateEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(createEquipmentReadingsResult.get_SysReturnCode());
		dto.setEquipmentReadingSgt(createEquipmentReadingsResult.getEquipmentReadingSgt());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS) {
				// PGM.*Return code is *Record already exists
				// DEBUG genFunctionCall BEGIN 1000287 ACT Send error message - '*Record already exists'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1001646) EXCINTFUN 1878852 EquipmentReadings.crteEquipmentReadings 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000209 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878852 EquipmentReadings.crteEquipmentReadings 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000217 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000224 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000229 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		CrteEquipmentReadingsResult result = new CrteEquipmentReadingsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * CreateEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateEquipmentReadings(CrteEquipmentReadingsDTO dto, CreateEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        CrteEquipmentReadingsParams crteEquipmentReadingsParams = new CrteEquipmentReadingsParams();
//        BeanUtils.copyProperties(dto, crteEquipmentReadingsParams);
//        stepResult = StepResult.callService(CrteEquipmentReadingsService.class, crteEquipmentReadingsParams);
//
//        return stepResult;
//    }
//
}
