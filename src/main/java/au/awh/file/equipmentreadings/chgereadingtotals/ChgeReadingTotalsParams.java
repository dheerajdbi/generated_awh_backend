package au.awh.file.equipmentreadings.chgereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeReadingTotalsParams implements Serializable
{
	private static final long serialVersionUID = -2536394038317004959L;

    private ErrorProcessingEnum errorProcessing = null;
    private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
    private BigDecimal differenceFromPrevious = BigDecimal.ZERO;
    private BigDecimal idleDaysFromPrevious = BigDecimal.ZERO;
    private BigDecimal unavailableDaysFromPrv =BigDecimal.ZERO;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}
	
	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
		setEquipmentReadingSgt(equipmentReadingSgt);
	}
	
	public BigDecimal getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}
	
	public void setDifferenceFromPrevious(BigDecimal differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}
	
	public BigDecimal getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}
	
	public void setIdleDaysFromPrevious(BigDecimal idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}
	
	public BigDecimal getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}
	
	public void setUnavailableDaysFromPrv(BigDecimal unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}
	
}
