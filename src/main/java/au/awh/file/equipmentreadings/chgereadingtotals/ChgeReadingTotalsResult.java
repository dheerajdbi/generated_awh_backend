package au.awh.file.equipmentreadings.chgereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = 8113825636993026313L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
