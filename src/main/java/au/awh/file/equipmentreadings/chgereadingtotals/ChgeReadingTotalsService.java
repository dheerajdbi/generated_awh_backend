package au.awh.file.equipmentreadings.chgereadingtotals;

// ExecuteInternalFunction.kt File=EquipmentReadings (ER) Function=chgeReadingTotals (2027533) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmentreadings.EquipmentReadings;
import au.awh.file.equipmentreadings.EquipmentReadingsId;
import au.awh.file.equipmentreadings.EquipmentReadingsRepository;
// imports for Services
import au.awh.file.equipmentreadings.changereadingtotals.ChangeReadingTotalsService;
// imports for DTO
import au.awh.file.equipmentreadings.changereadingtotals.ChangeReadingTotalsDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.changereadingtotals.ChangeReadingTotalsParams;
// imports for Results
import au.awh.file.equipmentreadings.changereadingtotals.ChangeReadingTotalsResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ChgE Reading Totals' (file 'Equipment Readings' (WSEQREAD)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ChgeReadingTotalsService extends AbstractService<ChgeReadingTotalsService, ChgeReadingTotalsDTO>
{
	private final Step execute = define("execute", ChgeReadingTotalsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentReadingsRepository equipmentReadingsRepository;

	@Autowired
	private ChangeReadingTotalsService changeReadingTotalsService;

	//private final Step serviceChangeReadingTotals = define("serviceChangeReadingTotals",ChangeReadingTotalsResult.class, this::processServiceChangeReadingTotals);
	
	@Autowired
	public ChgeReadingTotalsService() {
		super(ChgeReadingTotalsService.class, ChgeReadingTotalsDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgeReadingTotalsParams params) throws ServiceException {
		ChgeReadingTotalsDTO dto = new ChgeReadingTotalsDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgeReadingTotalsDTO dto, ChgeReadingTotalsParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ChangeReadingTotalsResult changeReadingTotalsResult = null;
		ChangeReadingTotalsParams changeReadingTotalsParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Change Reading Totals - Equipment Readings  *
		changeReadingTotalsParams = new ChangeReadingTotalsParams();
		changeReadingTotalsResult = new ChangeReadingTotalsResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, changeReadingTotalsParams);
		changeReadingTotalsParams.setEquipmentReadingSgt(dto.getEquipmentReadingSgt());
		changeReadingTotalsParams.setDifferenceFromPrevious(dto.getDifferenceFromPrevious());
		changeReadingTotalsParams.setIdleDaysFromPrevious(dto.getIdleDaysFromPrevious());
		changeReadingTotalsParams.setUnavailableDaysFromPrv(dto.getUnavailableDaysFromPrv());
		stepResult = changeReadingTotalsService.execute(changeReadingTotalsParams);
		changeReadingTotalsResult = (ChangeReadingTotalsResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(changeReadingTotalsResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 2027533 EquipmentReadings.chgeReadingTotals 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		ChgeReadingTotalsResult result = new ChgeReadingTotalsResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ChangeReadingTotalsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeReadingTotals(ChgeReadingTotalsDTO dto, ChangeReadingTotalsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgeReadingTotalsParams chgeReadingTotalsParams = new ChgeReadingTotalsParams();
//        BeanUtils.copyProperties(dto, chgeReadingTotalsParams);
//        stepResult = StepResult.callService(ChgeReadingTotalsService.class, chgeReadingTotalsParams);
//
//        return stepResult;
//    }
//
}
