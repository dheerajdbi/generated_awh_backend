package au.awh.file.equipmentreadings.chgereadingtotals;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChgeReadingTotalsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private ErrorProcessingEnum errorProcessing;
	private long differenceFromPrevious;
	private long equipmentReadingSgt;
	private long idleDaysFromPrevious;
	private long unavailableDaysFromPrv;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public long getDifferenceFromPrevious() {
		return differenceFromPrevious;
	}

	public long getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public long getIdleDaysFromPrevious() {
		return idleDaysFromPrevious;
	}

	public long getUnavailableDaysFromPrv() {
		return unavailableDaysFromPrv;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setDifferenceFromPrevious(long differenceFromPrevious) {
		this.differenceFromPrevious = differenceFromPrevious;
	}

	public void setEquipmentReadingSgt(long equipmentReadingSgt) {
		this.equipmentReadingSgt = equipmentReadingSgt;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setIdleDaysFromPrevious(long idleDaysFromPrevious) {
		this.idleDaysFromPrevious = idleDaysFromPrevious;
	}

	public void setUnavailableDaysFromPrv(long unavailableDaysFromPrv) {
		this.unavailableDaysFromPrv = unavailableDaysFromPrv;
	}

}
