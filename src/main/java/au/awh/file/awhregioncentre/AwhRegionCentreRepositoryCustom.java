package au.awh.file.awhregioncentre;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Custom Spring Data JPA repository interface for model: AWH Region/Centre (WSREGCTR).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface AwhRegionCentreRepositoryCustom {

	/**
	 * 
	 * @param centreCodeKey Centre Code Key
	 * @return AwhRegionCentre
	 */
	AwhRegionCentre getCentresRegion(String centreCodeKey
	);
}
