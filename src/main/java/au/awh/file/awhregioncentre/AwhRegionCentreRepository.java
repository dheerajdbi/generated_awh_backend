package au.awh.file.awhregioncentre;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: AWH Region/Centre (WSREGCTR).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface AwhRegionCentreRepository extends AwhRegionCentreRepositoryCustom, JpaRepository<AwhRegionCentre, AwhRegionCentreId> {

	List<AwhRegionCentre> findAllByIdCentreCodeKey(String centreCodeKey);

	List<AwhRegionCentre> findAllByIdCentreCodeKeyAndIdAwhRegionCode(String centreCodeKey, String awhRegionCode);
}
