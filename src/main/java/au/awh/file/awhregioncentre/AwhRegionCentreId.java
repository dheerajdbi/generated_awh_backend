package au.awh.file.awhregioncentre;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AwhRegionCentreId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "RCAWHREG")
	private String awhRegionCode = "";
	
	@Column(name = "RCCTRCDEK")
	private String centreCodeKey = "";

	public AwhRegionCentreId() {
	
	}

	public AwhRegionCentreId(String awhRegionCode, String centreCodeKey) { 	this.awhRegionCode = awhRegionCode;
		this.centreCodeKey = centreCodeKey;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
