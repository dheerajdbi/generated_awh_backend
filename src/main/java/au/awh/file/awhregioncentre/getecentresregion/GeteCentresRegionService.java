package au.awh.file.awhregioncentre.getecentresregion;

// ExecuteInternalFunction.kt File=AwhRegionCentre (RC) Function=geteCentresRegion (1878473) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.awhregioncentre.AwhRegionCentre;
import au.awh.file.awhregioncentre.AwhRegionCentreId;
import au.awh.file.awhregioncentre.AwhRegionCentreRepository;
// imports for Services
import au.awh.file.awhregioncentre.getcentresregion.GetCentresRegionService;
// imports for DTO
import au.awh.file.awhregioncentre.getcentresregion.GetCentresRegionDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.awhregioncentre.getcentresregion.GetCentresRegionParams;
// imports for Results
import au.awh.file.awhregioncentre.getcentresregion.GetCentresRegionResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Centres Region' (file 'AWH Region/Centre' (WSREGCTR)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteCentresRegionService extends AbstractService<GeteCentresRegionService, GeteCentresRegionDTO>
{
	private final Step execute = define("execute", GeteCentresRegionParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private AwhRegionCentreRepository awhRegionCentreRepository;

	@Autowired
	private GetCentresRegionService getCentresRegionService;

	//private final Step serviceGetCentresRegion = define("serviceGetCentresRegion",GetCentresRegionResult.class, this::processServiceGetCentresRegion);
	
	@Autowired
	public GeteCentresRegionService() {
		super(GeteCentresRegionService.class, GeteCentresRegionDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteCentresRegionParams params) throws ServiceException {
		GeteCentresRegionDTO dto = new GeteCentresRegionDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteCentresRegionDTO dto, GeteCentresRegionParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetCentresRegionResult getCentresRegionResult = null;
		GetCentresRegionParams getCentresRegionParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Centres Region - AWH Region/Centre  *
		getCentresRegionParams = new GetCentresRegionParams();
		getCentresRegionResult = new GetCentresRegionResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getCentresRegionParams);
		getCentresRegionParams.setCentreCodeKey(dto.getCentreCodeKey());
		stepResult = getCentresRegionService.execute(getCentresRegionParams);
		getCentresRegionResult = (GetCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getCentresRegionResult.get_SysReturnCode());
		dto.setAwhRegionCode(getCentresRegionResult.getAwhRegionCode());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// * NOTE: Insert <file> NF message here.
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'AWH Region/Centre      NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878054) EXCINTFUN 1878473 AwhRegionCentre.geteCentresRegion 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878473 AwhRegionCentre.geteCentresRegion 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteCentresRegionResult result = new GeteCentresRegionResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentresRegion(GeteCentresRegionDTO dto, GetCentresRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteCentresRegionParams geteCentresRegionParams = new GeteCentresRegionParams();
//        BeanUtils.copyProperties(dto, geteCentresRegionParams);
//        stepResult = StepResult.callService(GeteCentresRegionService.class, geteCentresRegionParams);
//
//        return stepResult;
//    }
//
}
