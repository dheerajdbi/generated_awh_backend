package au.awh.file.awhregioncentre;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
import au.awh.file.awhregioncentre.AwhRegionCentre;

/**
 * Custom Spring Data JPA repository implementation for model: AWH Region/Centre (WSREGCTR).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class AwhRegionCentreRepositoryImpl implements AwhRegionCentreRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.awhregioncentre.AwhRegionCentreService#getCentresRegion(String)
	 */
	@Override
	public AwhRegionCentre getCentresRegion(
		String centreCodeKey
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			whereClause += " awhRegionCentre.id.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		String sqlString = "SELECT awhRegionCentre FROM AwhRegionCentre awhRegionCentre";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		AwhRegionCentre dto = (AwhRegionCentre)query.getSingleResult();

		return dto;
	}

}
