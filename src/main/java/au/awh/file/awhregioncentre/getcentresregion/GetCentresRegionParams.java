package au.awh.file.awhregioncentre.getcentresregion;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetCentresRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentresRegionParams implements Serializable
{
	private static final long serialVersionUID = 8854384604735030580L;

    private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
