package au.awh.file.awhregioncentre.getcentresregion;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetCentresRegion ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentresRegionResult implements Serializable
{
	private static final long serialVersionUID = 1171341961608251518L;

	private ReturnCodeEnum _sysReturnCode;
	private String awhRegionCode = ""; // String 56452

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
