package au.awh.file.default_.getewsprinterdefault;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.GlobalContext;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeKeyEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GeteWsPrinterDefaultDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Autowired
	private GlobalContext globalCtx;

	private ReturnCodeEnum _sysReturnCode;

	private ErrorProcessingEnum errorProcessing;
	private ReportEnum report;
	private StateCodeKeyEnum stateCodeKey;
	private String brokerIdKey;
	private String centreCodeKey;
	private String defaultFieldName;
	private String defaultValue;
	private String returnedDefault;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getBrokerIdKey() {
		return brokerIdKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getDefaultFieldName() {
		return defaultFieldName;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}

	public ReportEnum getReport() {
		return report;
	}

	public String getReturnedDefault() {
		return returnedDefault;
	}

	public StateCodeKeyEnum getStateCodeKey() {
		return stateCodeKey;
	}

	public String getWfDummyDbAlpha() {
		return globalCtx.getString("dummyDbAlpha");
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setBrokerIdKey(String brokerIdKey) {
		this.brokerIdKey = brokerIdKey;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setDefaultFieldName(String defaultFieldName) {
		this.defaultFieldName = defaultFieldName;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	public void setReport(ReportEnum report) {
		this.report = report;
	}

	public void setReturnedDefault(String returnedDefault) {
		this.returnedDefault = returnedDefault;
	}

	public void setStateCodeKey(StateCodeKeyEnum stateCodeKey) {
		this.stateCodeKey = stateCodeKey;
	}

	public void setWfDummyDbAlpha(String wfDummyDbAlpha) {
		globalCtx.setString("dummyDbAlpha", wfDummyDbAlpha);
	}

}
