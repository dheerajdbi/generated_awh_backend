package au.awh.file.default_.getewsprinterdefault;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReportEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteWsPrinterDefault ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteWsPrinterDefaultParams implements Serializable
{
	private static final long serialVersionUID = 1727808182723497568L;

    private String brokerIdKey = "";
    private String centreCodeKey = "";
    private StateCodeKeyEnum stateCodeKey = null;
    private String defaultFieldName = "";
    private ErrorProcessingEnum errorProcessing = null;
    private ReportEnum report = null;

	public String getBrokerIdKey() {
		return brokerIdKey;
	}
	
	public void setBrokerIdKey(String brokerIdKey) {
		this.brokerIdKey = brokerIdKey;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public StateCodeKeyEnum getStateCodeKey() {
		return stateCodeKey;
	}
	
	public void setStateCodeKey(StateCodeKeyEnum stateCodeKey) {
		this.stateCodeKey = stateCodeKey;
	}
	
	public void setStateCodeKey(String stateCodeKey) {
		setStateCodeKey(StateCodeKeyEnum.valueOf(stateCodeKey));
	}
	
	public String getDefaultFieldName() {
		return defaultFieldName;
	}
	
	public void setDefaultFieldName(String defaultFieldName) {
		this.defaultFieldName = defaultFieldName;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public ReportEnum getReport() {
		return report;
	}
	
	public void setReport(ReportEnum report) {
		this.report = report;
	}
	
	public void setReport(String report) {
		setReport(ReportEnum.valueOf(report));
	}
}
