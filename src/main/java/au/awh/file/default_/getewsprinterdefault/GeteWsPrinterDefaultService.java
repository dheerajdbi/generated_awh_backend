package au.awh.file.default_.getewsprinterdefault;

// ExecuteInternalFunction.kt File=Default_ (GE) Function=geteWsPrinterDefault (1600473) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.default_.Default_;
import au.awh.file.default_.Default_Id;
import au.awh.file.default_.Default_Repository;
// imports for Services
import au.awh.file.default_.getdefault.GetDefaultService;
// imports for DTO
import au.awh.file.default_.getdefault.GetDefaultDTO;
// imports for Enums
import au.awh.model.DefaultTypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// imports for Parameters
import au.awh.file.default_.getdefault.GetDefaultParams;
// imports for Results
import au.awh.file.default_.getdefault.GetDefaultResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE WS Printer Default' (file 'Default' (WSDFLT)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteWsPrinterDefaultService extends AbstractService<GeteWsPrinterDefaultService, GeteWsPrinterDefaultDTO>
{
	private final Step execute = define("execute", GeteWsPrinterDefaultParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private Default_Repository default_Repository;

	@Autowired
	private GetDefaultService getDefaultService;

	//private final Step serviceGetDefault = define("serviceGetDefault",GetDefaultResult.class, this::processServiceGetDefault);
	
	@Autowired
	public GeteWsPrinterDefaultService() {
		super(GeteWsPrinterDefaultService.class, GeteWsPrinterDefaultDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteWsPrinterDefaultParams params) throws ServiceException {
		GeteWsPrinterDefaultDTO dto = new GeteWsPrinterDefaultDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteWsPrinterDefaultDTO dto, GeteWsPrinterDefaultParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetDefaultResult getDefaultResult = null;
		GetDefaultParams getDefaultParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Default - Default  *
		getDefaultParams = new GetDefaultParams();
		getDefaultResult = new GetDefaultResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getDefaultParams);
		getDefaultParams.setDefaultType("WR");
		getDefaultParams.setDefaultKey1(dto.getCentreCodeKey());
		getDefaultParams.setDefaultKey2(dto.getReport().getCode());
		getDefaultParams.setDefaultKey3(dto.getBrokerIdKey());
		getDefaultParams.setDefaultKey4(dto.getStateCodeKey().getCode());
		getDefaultParams.setDefaultFieldName(dto.getDefaultFieldName());
		stepResult = getDefaultService.execute(getDefaultParams);
		getDefaultResult = (GetDefaultResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getDefaultResult.get_SysReturnCode());
		dto.setReturnedDefault(getDefaultResult.getDefaultValue());
		dto.setWfDummyDbAlpha(getDefaultResult.getCurrentLevel().getCode());
		// DEBUG genFunctionCall END
		if (dto.getReturnedDefault().equals("*NULL")) {
			// PAR.Returned Default is *NULL
			// DEBUG genFunctionCall BEGIN 1000116 ACT PAR.Default Value = CND.Not entered
			dto.setDefaultValue("");
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000122 ACT PAR.Default Value = PAR.Returned Default
			dto.setDefaultValue(dto.getReturnedDefault());
			// DEBUG genFunctionCall END
		}
		if ((ErrorProcessingEnum.isYes(dto.getErrorProcessing())) && !(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000054 ACT Send error message - 'BSC Default Error'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1597910) EXCINTFUN 1600473 Default_.geteWsPrinterDefault 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000061 ACT Send error message - 'BSC Default Error'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1597910) EXCINTFUN 1600473 Default_.geteWsPrinterDefault 
				// DEBUG genFunctionCall END
			}
			if (dto.getErrorProcessing() == ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE) {
				// PAR.Error Processing is Send message and ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (dto.getErrorProcessing() == ErrorProcessingEnum._SEND_MESSAGE_AND_QUIT) {
				// PAR.Error Processing is Send message and quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.getErrorProcessing() == ErrorProcessingEnum._SEND_MESSAGE_AND_EXIT) {
				// PAR.Error Processing is Send message and exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteWsPrinterDefaultResult result = new GeteWsPrinterDefaultResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetDefaultService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetDefault(GeteWsPrinterDefaultDTO dto, GetDefaultParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteWsPrinterDefaultParams geteWsPrinterDefaultParams = new GeteWsPrinterDefaultParams();
//        BeanUtils.copyProperties(dto, geteWsPrinterDefaultParams);
//        stepResult = StepResult.callService(GeteWsPrinterDefaultService.class, geteWsPrinterDefaultParams);
//
//        return stepResult;
//    }
//
}
