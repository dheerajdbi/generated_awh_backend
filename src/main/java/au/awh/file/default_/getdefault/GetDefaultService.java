package au.awh.file.default_.getdefault;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.default_.Default_;
import au.awh.file.default_.Default_Id;
import au.awh.file.default_.Default_Repository;
// imports for Services
import au.awh.file.default_.getdefaultstandard.GetDefaultStandardService;
// imports for DTO
import au.awh.file.default_.getdefaultstandard.GetDefaultStandardDTO;
// imports for Enums
import au.awh.model.DefaultTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.default_.getdefaultstandard.GetDefaultStandardParams;
// imports for Results
import au.awh.file.default_.getdefaultstandard.GetDefaultStandardResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Default' (file 'Default' (WSDFLT)
 *
 * @author X2EGenerator
 */
@Service
public class GetDefaultService extends AbstractService<GetDefaultService, GetDefaultDTO>
{
    private final Step execute = define("execute", GetDefaultParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private Default_Repository default_Repository;

	@Autowired
	private GetDefaultStandardService getDefaultStandardService;

	//private final Step serviceGetDefaultStandard = define("serviceGetDefaultStandard",GetDefaultStandardResult.class, this::processServiceGetDefaultStandard);
	

    @Autowired
    public GetDefaultService()
    {
        super(GetDefaultService.class, GetDefaultDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetDefaultParams params) throws ServiceException {
        GetDefaultDTO dto = new GetDefaultDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetDefaultDTO dto, GetDefaultParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

//		List<Default_> default_List = default_Repository.findAllByIdDefaultTypeAndIdDefaultKey1AndIdDefaultKey2AndIdDefaultKey3AndIdDefaultKey4AndIdDefaultFieldName(dto.getDefaultType(), dto.getDefaultKey1(), dto.getDefaultKey2(), dto.getDefaultKey3(), dto.getDefaultKey4(), dto.getDefaultFieldName());
//		if (!default_List.isEmpty()) {
//			for (Default_ default_ : default_List) {
//				processDataRecord(dto, default_);
//			}
//			exitProcessing(dto);
//            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
//		}
//		else {
//			processingIfDataRecordNotFound(dto);
//            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
//		}
 
        GetDefaultResult result = new GetDefaultResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetDefaultDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetDefaultDTO dto, Default_ default_) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000192 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR.Current Level = CND.Yes
		dto.setCurrentLevel(YesOrNoEnum._YES);
		// DEBUG genFunctionCall END
		// * Assume this is a FIFO partial key access.
		// DEBUG genFunctionCall BEGIN 1000190 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetDefaultDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		GetDefaultStandardResult getDefaultStandardResult = null;
		GetDefaultStandardParams getDefaultStandardParams = null;
		// * Ignore return code.
		// DEBUG genFunctionCall BEGIN 1000174 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000199 ACT PAR = CON By name
		dto.setDefaultValue("");
		dto.setCurrentLevel(YesOrNoEnum._NOT_ENTERED);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000017 ACT PAR.Current Level = CND.No
		dto.setCurrentLevel(YesOrNoEnum._NO);
		// DEBUG genFunctionCall END
		if (dto.getDefaultKey4().equals("*")) {
			// PAR.Default Key 4 is *None
		} else {
			// *OTHERWISE
			if (DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) {
				// PAR.Default Type is Allow Key 2 as Wild
				// DEBUG genFunctionCall BEGIN 1000251 ACT Get Default Standard - Default  *
				getDefaultStandardParams = new GetDefaultStandardParams();
				getDefaultStandardResult = new GetDefaultStandardResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, getDefaultStandardParams);
				getDefaultStandardParams.setDefaultType(dto.getDefaultType());
				getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
				getDefaultStandardParams.setDefaultKey2("*");
				getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
				getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
				getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
				stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
				getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
				dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
				// DEBUG genFunctionCall END
				if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
					// PGM.*Return code is *Normal
					// DEBUG genFunctionCall BEGIN 1000263 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
					// 
				} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
					// PGM.*Return code is *Record does not exist
					// DEBUG genFunctionCall BEGIN 1000268 ACT PGM.*Return code = CND.*Normal
					dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000275 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
				}
			}
			if (DefaultTypeEnum.isAllowKey3AsWild(dto.getDefaultType())) {
				// PAR.Default Type is Allow Key 3 as Wild
				// DEBUG genFunctionCall BEGIN 1000280 ACT Get Default Standard - Default  *
				getDefaultStandardParams = new GetDefaultStandardParams();
				getDefaultStandardResult = new GetDefaultStandardResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, getDefaultStandardParams);
				getDefaultStandardParams.setDefaultType(dto.getDefaultType());
				getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
				getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
				getDefaultStandardParams.setDefaultKey3("*");
				getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
				getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
				stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
				getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
				dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
				// DEBUG genFunctionCall END
				if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
					// PGM.*Return code is *Normal
					// DEBUG genFunctionCall BEGIN 1000292 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
					// 
				} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
					// PGM.*Return code is *Record does not exist
					// DEBUG genFunctionCall BEGIN 1000297 ACT PGM.*Return code = CND.*Normal
					dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000304 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
				}
			}
			if ((DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) && (DefaultTypeEnum.isAllowKey3AsWild(dto.getDefaultType()))) {
				// DEBUG genFunctionCall BEGIN 1000222 ACT Get Default Standard - Default  *
				getDefaultStandardParams = new GetDefaultStandardParams();
				getDefaultStandardResult = new GetDefaultStandardResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, getDefaultStandardParams);
				getDefaultStandardParams.setDefaultType(dto.getDefaultType());
				getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
				getDefaultStandardParams.setDefaultKey2("*");
				getDefaultStandardParams.setDefaultKey3("*");
				getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
				getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
				stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
				getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
				dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
				// DEBUG genFunctionCall END
				if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
					// PGM.*Return code is *Normal
					// DEBUG genFunctionCall BEGIN 1000234 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
					// 
				} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
					// PGM.*Return code is *Record does not exist
					// DEBUG genFunctionCall BEGIN 1000239 ACT PGM.*Return code = CND.*Normal
					dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000246 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
				}
			}
			// DEBUG genFunctionCall BEGIN 1000087 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
			getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
			getDefaultStandardParams.setDefaultKey4("*");
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000099 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000207 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000105 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		if (dto.getDefaultKey3().equals("*")) {
			// PAR.Default Key 3 is *None
		} else {
			// *OTHERWISE
			if (DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) {
				// PAR.Default Type is Allow Key 2 as Wild
				// DEBUG genFunctionCall BEGIN 1000313 ACT Get Default Standard - Default  *
				getDefaultStandardParams = new GetDefaultStandardParams();
				getDefaultStandardResult = new GetDefaultStandardResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, getDefaultStandardParams);
				getDefaultStandardParams.setDefaultType(dto.getDefaultType());
				getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
				getDefaultStandardParams.setDefaultKey2("*");
				getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
				getDefaultStandardParams.setDefaultKey4("*");
				getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
				stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
				getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
				dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
				// DEBUG genFunctionCall END
				if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
					// PGM.*Return code is *Normal
					// DEBUG genFunctionCall BEGIN 1000325 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
					// 
				} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
					// PGM.*Return code is *Record does not exist
					// DEBUG genFunctionCall BEGIN 1000330 ACT PGM.*Return code = CND.*Normal
					dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000337 ACT <-- *QUIT
					// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
					// DEBUG genFunctionCall END
				}
			}
			// DEBUG genFunctionCall BEGIN 1000116 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
			getDefaultStandardParams.setDefaultKey3("*");
			getDefaultStandardParams.setDefaultKey4("*");
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000128 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000211 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000134 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		if (dto.getDefaultKey2().equals("*")) {
			// PAR.Default Key 2 is *None
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000145 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2("*");
			getDefaultStandardParams.setDefaultKey3("*");
			getDefaultStandardParams.setDefaultKey4("*");
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000157 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000215 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000163 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		if (dto.getDefaultKey1().equals("*")) {
			// PAR.Default Key 1 is *None
			// DEBUG genFunctionCall BEGIN 1000165 ACT PGM.*Return code = CND.Serious error
			dto.set_SysReturnCode(ReturnCodeEnum._SERIOUS_ERROR);
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000056 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1("*");
			getDefaultStandardParams.setDefaultKey2("*");
			getDefaultStandardParams.setDefaultKey3("*");
			getDefaultStandardParams.setDefaultKey4("*");
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			// * Exit with return code for calling function to process.
			// DEBUG genFunctionCall BEGIN 1000169 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}

        return stepResult;
    }

    private StepResult exitProcessing(GetDefaultDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * GetDefaultStandardService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetDefaultStandard(GetDefaultDTO dto, GetDefaultStandardParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GetDefaultParams getDefaultParams = new GetDefaultParams();
//        BeanUtils.copyProperties(dto, getDefaultParams);
//        stepResult = StepResult.callService(GetDefaultService.class, getDefaultParams);
//
//        return stepResult;
//    }
//
}
