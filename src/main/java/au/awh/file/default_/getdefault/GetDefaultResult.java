package au.awh.file.default_.getdefault;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetDefault ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetDefaultResult implements Serializable
{
	private static final long serialVersionUID = -8113894795489885145L;

	private ReturnCodeEnum _sysReturnCode;
	private String defaultValue = ""; // String 32070
	private YesOrNoEnum currentLevel = null; // YesOrNoEnum 35414

	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public YesOrNoEnum getCurrentLevel() {
		return currentLevel;
	}
	
	public void setCurrentLevel(YesOrNoEnum currentLevel) {
		this.currentLevel = currentLevel;
	}
	
	public void setCurrentLevel(String currentLevel) {
		setCurrentLevel(YesOrNoEnum.valueOf(currentLevel));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
