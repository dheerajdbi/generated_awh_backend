package au.awh.file.default_;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.DefaultTypeEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
import au.awh.file.default_.Default_;

/**
 * Custom Spring Data JPA repository implementation for model: Default (WSDFLT).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class Default_RepositoryImpl implements Default_RepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.default_.Default_Service#getDefault(String, DefaultTypeEnum, String, String, String, String)
	 */
	@Override
	public Default_ getDefault(
		String defaultFieldName, DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(defaultFieldName)) {
			whereClause += " default_.id.defaultFieldName = :defaultFieldName";
			isParamSet = true;
		}

		if (defaultType != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultType = :defaultType";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey1)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey1 = :defaultKey1";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey2)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey2 = :defaultKey2";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey3)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey3 = :defaultKey3";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey4)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey4 = :defaultKey4";
			isParamSet = true;
		}

		String sqlString = "SELECT default_ FROM Default_ default_";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(defaultFieldName)) {
			query.setParameter("defaultFieldName", defaultFieldName);
		}

		if (defaultType != null) {
			query.setParameter("defaultType", defaultType);
		}

		if (StringUtils.isNotEmpty(defaultKey1)) {
			query.setParameter("defaultKey1", defaultKey1);
		}

		if (StringUtils.isNotEmpty(defaultKey2)) {
			query.setParameter("defaultKey2", defaultKey2);
		}

		if (StringUtils.isNotEmpty(defaultKey3)) {
			query.setParameter("defaultKey3", defaultKey3);
		}

		if (StringUtils.isNotEmpty(defaultKey4)) {
			query.setParameter("defaultKey4", defaultKey4);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Default_ dto = (Default_)query.getSingleResult();

		return dto;
	}

	/**
	 * @see au.awh.file.default_.Default_Service#getDefaultStandard(DefaultTypeEnum, String, String, String, String, String)
	 */
	@Override
	public Default_ getDefaultStandard(
		DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4, String defaultFieldName
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (defaultType != null) {
			whereClause += " default_.id.defaultType = :defaultType";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey1)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey1 = :defaultKey1";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey2)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey2 = :defaultKey2";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey3)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey3 = :defaultKey3";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultKey4)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultKey4 = :defaultKey4";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(defaultFieldName)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " default_.id.defaultFieldName = :defaultFieldName";
			isParamSet = true;
		}

		String sqlString = "SELECT default_ FROM Default_ default_";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (defaultType != null) {
			query.setParameter("defaultType", defaultType);
		}

		if (StringUtils.isNotEmpty(defaultKey1)) {
			query.setParameter("defaultKey1", defaultKey1);
		}

		if (StringUtils.isNotEmpty(defaultKey2)) {
			query.setParameter("defaultKey2", defaultKey2);
		}

		if (StringUtils.isNotEmpty(defaultKey3)) {
			query.setParameter("defaultKey3", defaultKey3);
		}

		if (StringUtils.isNotEmpty(defaultKey4)) {
			query.setParameter("defaultKey4", defaultKey4);
		}

		if (StringUtils.isNotEmpty(defaultFieldName)) {
			query.setParameter("defaultFieldName", defaultFieldName);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		Default_ dto = (Default_)query.getSingleResult();

		return dto;
	}

}
