package au.awh.file.default_;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.DefaultTypeEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Custom Spring Data JPA repository interface for model: Default (WSDFLT).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface Default_RepositoryCustom {

	/**
	 * 
	 * @param defaultFieldName Default Field Name
	 * @param defaultType Default Type
	 * @param defaultKey1 Default Key 1
	 * @param defaultKey2 Default Key 2
	 * @param defaultKey3 Default Key 3
	 * @param defaultKey4 Default Key 4
	 * @return Default_
	 */
	Default_ getDefault(String defaultFieldName, DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4
	);

	/**
	 * 
	 * @param defaultType Default Type
	 * @param defaultKey1 Default Key 1
	 * @param defaultKey2 Default Key 2
	 * @param defaultKey3 Default Key 3
	 * @param defaultKey4 Default Key 4
	 * @param defaultFieldName Default Field Name
	 * @return Default_
	 */
	Default_ getDefaultStandard(DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4, String defaultFieldName
	);
}
