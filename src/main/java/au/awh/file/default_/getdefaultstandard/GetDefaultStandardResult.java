package au.awh.file.default_.getdefaultstandard;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetDefaultStandard ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetDefaultStandardResult implements Serializable
{
	private static final long serialVersionUID = -5863890062059682743L;

	private ReturnCodeEnum _sysReturnCode;
	private String defaultValue = ""; // String 32070

	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
