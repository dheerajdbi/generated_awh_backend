package au.awh.file.default_.getdefaultstandard;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.DefaultTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetDefaultStandard ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetDefaultStandardParams implements Serializable
{
	private static final long serialVersionUID = 4579739514378731829L;

    private DefaultTypeEnum defaultType = null;
    private String defaultKey1 = "";
    private String defaultKey2 = "";
    private String defaultKey3 = "";
    private String defaultKey4 = "";
    private String defaultFieldName = "";

	public DefaultTypeEnum getDefaultType() {
		return defaultType;
	}
	
	public void setDefaultType(DefaultTypeEnum defaultType) {
		this.defaultType = defaultType;
	}
	
	public void setDefaultType(String defaultType) {
		setDefaultType(DefaultTypeEnum.valueOf(defaultType));
	}
	
	public String getDefaultKey1() {
		return defaultKey1;
	}
	
	public void setDefaultKey1(String defaultKey1) {
		this.defaultKey1 = defaultKey1;
	}
	
	public String getDefaultKey2() {
		return defaultKey2;
	}
	
	public void setDefaultKey2(String defaultKey2) {
		this.defaultKey2 = defaultKey2;
	}
	
	public String getDefaultKey3() {
		return defaultKey3;
	}
	
	public void setDefaultKey3(String defaultKey3) {
		this.defaultKey3 = defaultKey3;
	}
	
	public String getDefaultKey4() {
		return defaultKey4;
	}
	
	public void setDefaultKey4(String defaultKey4) {
		this.defaultKey4 = defaultKey4;
	}
	
	public String getDefaultFieldName() {
		return defaultFieldName;
	}
	
	public void setDefaultFieldName(String defaultFieldName) {
		this.defaultFieldName = defaultFieldName;
	}
}
