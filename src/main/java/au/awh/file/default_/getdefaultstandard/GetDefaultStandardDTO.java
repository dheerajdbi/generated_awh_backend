package au.awh.file.default_.getdefaultstandard;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.DefaultTypeEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetDefaultStandardDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private DefaultTypeEnum defaultType;
	private String defaultFieldName;
	private String defaultKey1;
	private String defaultKey2;
	private String defaultKey3;
	private String defaultKey4;
	private String defaultValue;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getDefaultFieldName() {
		return defaultFieldName;
	}

	public String getDefaultKey1() {
		return defaultKey1;
	}

	public String getDefaultKey2() {
		return defaultKey2;
	}

	public String getDefaultKey3() {
		return defaultKey3;
	}

	public String getDefaultKey4() {
		return defaultKey4;
	}

	public DefaultTypeEnum getDefaultType() {
		return defaultType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setDefaultFieldName(String defaultFieldName) {
		this.defaultFieldName = defaultFieldName;
	}

	public void setDefaultKey1(String defaultKey1) {
		this.defaultKey1 = defaultKey1;
	}

	public void setDefaultKey2(String defaultKey2) {
		this.defaultKey2 = defaultKey2;
	}

	public void setDefaultKey3(String defaultKey3) {
		this.defaultKey3 = defaultKey3;
	}

	public void setDefaultKey4(String defaultKey4) {
		this.defaultKey4 = defaultKey4;
	}

	public void setDefaultType(DefaultTypeEnum defaultType) {
		this.defaultType = defaultType;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}
