package au.awh.file.default_.getdefaultstandard;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.default_.Default_;
import au.awh.file.default_.Default_Id;
import au.awh.file.default_.Default_Repository;
import au.awh.model.DefaultTypeEnum;
import au.awh.model.ReturnCodeEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Default Standard' (file 'Default' (WSDFLT)
 *
 * @author X2EGenerator
 */
@Service
public class GetDefaultStandardService extends AbstractService<GetDefaultStandardService, GetDefaultStandardDTO>
{
    private final Step execute = define("execute", GetDefaultStandardParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private Default_Repository default_Repository;

	

    @Autowired
    public GetDefaultStandardService()
    {
        super(GetDefaultStandardService.class, GetDefaultStandardDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetDefaultStandardParams params) throws ServiceException {
        GetDefaultStandardDTO dto = new GetDefaultStandardDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetDefaultStandardDTO dto, GetDefaultStandardParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

//		List<Default_> default_List = default_Repository.findAllByIdDefaultTypeAndIdDefaultKey1AndIdDefaultKey2AndIdDefaultKey3AndIdDefaultKey4AndIdDefaultFieldName(dto.getDefaultType(), dto.getDefaultKey1(), dto.getDefaultKey2(), dto.getDefaultKey3(), dto.getDefaultKey4(), dto.getDefaultFieldName());
//		if (!default_List.isEmpty()) {
//			for (Default_ default_ : default_List) {
//				processDataRecord(dto, default_);
//			}
//			exitProcessing(dto);
//            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
//		}
//		else {
//			processingIfDataRecordNotFound(dto);
//            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
//		}
 
        GetDefaultStandardResult result = new GetDefaultStandardResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetDefaultStandardDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetDefaultStandardDTO dto, Default_ default_) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END
		// * Assume this is a FIFO partial key access.
		// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetDefaultStandardDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
		dto.setDefaultValue("");
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetDefaultStandardDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
