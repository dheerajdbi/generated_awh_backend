package au.awh.file.default_;

import au.awh.model.DefaultTypeEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSDFLT")
public class Default_ implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private Default_Id id = new Default_Id();

	@Column(name = "GEDFTVAL")
	private String defaultValue = "";

	public Default_Id getId() {
		return id;
	}

	public DefaultTypeEnum getDefaultType() {
		return id.getDefaultType();
	}
	
	public String getDefaultKey1() {
		return id.getDefaultKey1();
	}
	
	public String getDefaultKey2() {
		return id.getDefaultKey2();
	}
	
	public String getDefaultKey3() {
		return id.getDefaultKey3();
	}
	
	public String getDefaultKey4() {
		return id.getDefaultKey4();
	}
	
	public String getDefaultFieldName() {
		return id.getDefaultFieldName();
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	

	

	public void setDefaultType(DefaultTypeEnum defaultType) {
		this.id.setDefaultType(defaultType);
	}
	
	public void setDefaultKey1(String defaultKey1) {
		this.id.setDefaultKey1(defaultKey1);
	}
	
	public void setDefaultKey2(String defaultKey2) {
		this.id.setDefaultKey2(defaultKey2);
	}
	
	public void setDefaultKey3(String defaultKey3) {
		this.id.setDefaultKey3(defaultKey3);
	}
	
	public void setDefaultKey4(String defaultKey4) {
		this.id.setDefaultKey4(defaultKey4);
	}
	
	public void setDefaultFieldName(String defaultFieldName) {
		this.id.setDefaultFieldName(defaultFieldName);
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
