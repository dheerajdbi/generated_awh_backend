package au.awh.file.default_;

import au.awh.model.DefaultTypeEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Default_Id implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "GEDFFLD")
	private String defaultFieldName = "";
	
	@Column(name = "GEDFK1")
	private String defaultKey1 = "";
	
	@Column(name = "GEDFK2")
	private String defaultKey2 = "";
	
	@Column(name = "GEDFK3")
	private String defaultKey3 = "";
	
	@Column(name = "GEDFK4")
	private String defaultKey4 = "";
	
	@Column(name = "GEDFTYP")
	private DefaultTypeEnum defaultType = null;

	public Default_Id() {
	
	}

	public Default_Id(DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4, String defaultFieldName) { 	this.defaultFieldName = defaultFieldName;
		this.defaultKey1 = defaultKey1;
		this.defaultKey2 = defaultKey2;
		this.defaultKey3 = defaultKey3;
		this.defaultKey4 = defaultKey4;
		this.defaultType = defaultType;
	}

	public String getDefaultFieldName() {
		return defaultFieldName;
	}
	
	public String getDefaultKey1() {
		return defaultKey1;
	}
	
	public String getDefaultKey2() {
		return defaultKey2;
	}
	
	public String getDefaultKey3() {
		return defaultKey3;
	}
	
	public String getDefaultKey4() {
		return defaultKey4;
	}
	
	public DefaultTypeEnum getDefaultType() {
		return defaultType;
	}
	
	public void setDefaultFieldName(String defaultFieldName) {
		this.defaultFieldName = defaultFieldName;
	}
	
	public void setDefaultKey1(String defaultKey1) {
		this.defaultKey1 = defaultKey1;
	}
	
	public void setDefaultKey2(String defaultKey2) {
		this.defaultKey2 = defaultKey2;
	}
	
	public void setDefaultKey3(String defaultKey3) {
		this.defaultKey3 = defaultKey3;
	}
	
	public void setDefaultKey4(String defaultKey4) {
		this.defaultKey4 = defaultKey4;
	}
	
	public void setDefaultType(DefaultTypeEnum defaultType) {
		this.defaultType = defaultType;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
