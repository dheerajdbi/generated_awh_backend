package au.awh.file.default_;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 15
import au.awh.model.DefaultTypeEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Default (WSDFLT).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface Default_Repository extends Default_RepositoryCustom, JpaRepository<Default_, Default_Id> {

	List<Default_> findAllByIdDefaultFieldName(String defaultFieldName);

//	List<Default_> findAllByIdDefaultFieldNameAndIdDefaultTypeAndIdDefaultKey1(String defaultFieldName, DefaultTypeEnum defaultType, String defaultKey1);
//
//	List<Default_> findAllByIdDefaultTypeAndIdDefaultKey1AndIdDefaultKey2AndIdDefaultKey3AndIdDefaultKey4(DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4);
//
//	List<Default_> findAllByIdDefaultTypeAndIdDefaultKey1AndIdDefaultKey2AndIdDefaultKey3AndIdDefaultKey4AndGreaterThanIdDefaultFieldName(DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4, String defaultFieldName);
//
//	List<Default_> findAllByIdDefaultTypeAndIdDefaultKey1AndIdDefaultKey2AndIdDefaultKey3AndIdDefaultKey4AndIdDefaultFieldName(DefaultTypeEnum defaultType, String defaultKey1, String defaultKey2, String defaultKey3, String defaultKey4, String defaultFieldName);
}
