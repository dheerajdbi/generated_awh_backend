package au.awh.file.equipmenttype.getequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetEquipmentTypeParams implements Serializable
{
	private static final long serialVersionUID = 6855721953976471386L;

    private String equipmentTypeCode = "";

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
}
