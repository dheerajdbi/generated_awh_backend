package au.awh.file.equipmenttype.getequipmenttype;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetEquipmentTypeDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private EquipReadingUnitEnum equipReadingUnit;
	private String equipmentTypeCode;
	private String equipmentTypeDesc;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}

}
