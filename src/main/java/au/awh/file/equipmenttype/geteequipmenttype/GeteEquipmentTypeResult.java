package au.awh.file.equipmenttype.geteequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.EquipReadingUnitEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteEquipmentTypeResult implements Serializable
{
	private static final long serialVersionUID = 1846012930869511209L;

	private ReturnCodeEnum _sysReturnCode;
	private String equipmentTypeDesc = ""; // String 56497
	private EquipReadingUnitEnum equipReadingUnit = null; // EquipReadingUnitEnum 56498

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}
	
	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipReadingUnit(String equipReadingUnit) {
		setEquipReadingUnit(EquipReadingUnitEnum.valueOf(equipReadingUnit));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
