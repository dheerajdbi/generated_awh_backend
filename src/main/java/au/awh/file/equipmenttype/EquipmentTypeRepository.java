package au.awh.file.equipmenttype;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Equipment Type (WSEQTYPE).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentTypeRepository extends EquipmentTypeRepositoryCustom, JpaRepository<EquipmentType, EquipmentTypeId> {

	List<EquipmentType> findAllByIdEquipmentTypeCode(String equipmentTypeCode);
}
