package au.awh.file.equipmenttype;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

import au.awh.file.equipmenttype.EquipmentType;
import au.awh.file.equipmenttype.edtequipmenttypes.EdtEquipmentTypesGDO;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeGDO;
import au.awh.file.equipmenttype.pmtforequipmentbkdwn.PmtForEquipmentBkdwnGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Equipment Type (WSEQTYPE).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class EquipmentTypeRepositoryImpl implements EquipmentTypeRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.equipmenttype.EquipmentTypeService#deleteEquipmentType(EquipmentType)
	 */
	@Override
	public void deleteEquipmentType(
		String equipmentTypeCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			whereClause += " equipmentType.id.equipmentTypeCode = :equipmentTypeCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM EquipmentType equipmentType";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			query.setParameter("equipmentTypeCode", equipmentTypeCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<EdtEquipmentTypesGDO> edtEquipmentTypes(
		
Pageable pageable) {
		String sqlString = "SELECT new au.awh.file.equipmenttype.edtequipmenttypes.EdtEquipmentTypesGDO(equipmentType.id.equipmentTypeCode, equipmentType.equipmentTypeDesc, equipmentType.equipReadingUnit) from EquipmentType equipmentType";
		String countQueryString = "SELECT COUNT(equipmentType.id.equipmentTypeCode) FROM EquipmentType equipmentType";

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			EquipmentType equipmentType = new EquipmentType();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(equipmentType.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"equipmentType.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"equipmentType." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentType.id.equipmentTypeCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EdtEquipmentTypesGDO> content = query.getResultList();
		RestResponsePage<EdtEquipmentTypesGDO> pageGdo = new RestResponsePage<EdtEquipmentTypesGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<SelEquipmentTypeGDO> selEquipmentType(
		String equipmentTypeCode
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			whereClause += " (equipmentType.id.equipmentTypeCode >= :equipmentTypeCode OR equipmentType.id.equipmentTypeCode like CONCAT('%', :equipmentTypeCode, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeGDO(equipmentType.id.equipmentTypeCode, equipmentType.equipmentTypeDesc, equipmentType.equipReadingUnit) from EquipmentType equipmentType";
		String countQueryString = "SELECT COUNT(equipmentType.id.equipmentTypeCode) FROM EquipmentType equipmentType";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			EquipmentType equipmentType = new EquipmentType();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(equipmentType.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"equipmentType.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"equipmentType." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentType.id.equipmentTypeCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			countQuery.setParameter("equipmentTypeCode", equipmentTypeCode);
			query.setParameter("equipmentTypeCode", equipmentTypeCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelEquipmentTypeGDO> content = query.getResultList();
		RestResponsePage<SelEquipmentTypeGDO> pageGdo = new RestResponsePage<SelEquipmentTypeGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.equipmenttype.EquipmentTypeService#getEquipmentType(String)
	 */
	@Override
	public EquipmentType getEquipmentType(
		String equipmentTypeCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			whereClause += " equipmentType.id.equipmentTypeCode = :equipmentTypeCode";
			isParamSet = true;
		}

		String sqlString = "SELECT equipmentType FROM EquipmentType equipmentType";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			query.setParameter("equipmentTypeCode", equipmentTypeCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		EquipmentType dto = (EquipmentType)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<PmtForEquipmentBkdwnGDO> pmtForEquipmentBkdwn(
		
String awhRegionCode, String centreCodeKey, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " (equipmentType.awhRegionCode >= :awhRegionCode OR equipmentType.awhRegionCode like CONCAT('%', :awhRegionCode, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " equipmentType.centreCodeKey like CONCAT('%', :centreCodeKey, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.equipmenttype.pmtforequipmentbkdwn.PmtForEquipmentBkdwnGDO(equipmentType.id.equipmentTypeCode, equipmentType.equipmentTypeDesc, equipmentType.equipReadingUnit) from EquipmentType equipmentType";
		String countQueryString = "SELECT COUNT(equipmentType.id.equipmentTypeCode) FROM EquipmentType equipmentType";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			EquipmentType equipmentType = new EquipmentType();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(equipmentType.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"equipmentType.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"equipmentType." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"equipmentType.id.equipmentTypeCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PmtForEquipmentBkdwnGDO> content = query.getResultList();
		RestResponsePage<PmtForEquipmentBkdwnGDO> pageGdo = new RestResponsePage<PmtForEquipmentBkdwnGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

}
