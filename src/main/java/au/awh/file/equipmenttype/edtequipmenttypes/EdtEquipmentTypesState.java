package au.awh.file.equipmenttype.edtequipmenttypes;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;


import au.awh.model.CmdKeyEnum;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Equipment Type' (WSEQTYPE) and function 'Edt Equipment Types' (WSPLTWJEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */
 public class EdtEquipmentTypesState extends EdtEquipmentTypesDTO {
	private static final long serialVersionUID = -5722345989957596670L;


	// System fields
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public EdtEquipmentTypesState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }



    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}
