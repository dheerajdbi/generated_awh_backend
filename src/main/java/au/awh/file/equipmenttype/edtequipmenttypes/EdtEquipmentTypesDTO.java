package au.awh.file.equipmenttype.edtequipmenttypes;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import com.freschelegacy.utils.RestResponsePage;

import au.awh.file.equipmenttype.EquipmentType;
import au.awh.service.data.BaseDTO;

import au.awh.model.CmdKeyEnum;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Dto for file 'Equipment Type' (WSEQTYPE) and function 'Edt Equipment Types' (WSPLTWJEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */

public class EdtEquipmentTypesDTO extends BaseDTO {
	private static final long serialVersionUID = 2058397641430027575L;

    private RestResponsePage<EdtEquipmentTypesGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private boolean confirm = false;

	private ExitProgramOptionEnum exitProgramOption = null;
	private String equipmentTypeCode = "";


	private EdtEquipmentTypesGDO gdo;

    public EdtEquipmentTypesDTO() {

    }
    public void setPageGdo(RestResponsePage<EdtEquipmentTypesGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<EdtEquipmentTypesGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

    public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
    	return exitProgramOption;
    }

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeCode() {
    	return equipmentTypeCode;
    }

	public void setGdo(EdtEquipmentTypesGDO gdo) {
		this.gdo = gdo;
	}

	public EdtEquipmentTypesGDO getGdo() {
		return gdo;
	}

}