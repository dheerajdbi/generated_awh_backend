package au.awh.file.equipmenttype.edtequipmenttypes;

import java.io.Serializable;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END


/**
 * Params for resource: EdtEquipmentTypes (WSPLTWJEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
public class EdtEquipmentTypesResult implements Serializable
{
	private static final long serialVersionUID = -1170756740235181633L;

	private ExitProgramOptionEnum exitProgramOption = null;


	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}

}