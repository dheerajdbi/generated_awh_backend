package au.awh.file.equipmenttype.edtequipmenttypes;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerMapping;


import org.springframework.stereotype.Service;

import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;


import au.awh.file.equipmenttype.EquipmentType;
import au.awh.file.equipmenttype.EquipmentTypeId;
import au.awh.file.equipmenttype.EquipmentTypeRepository;


import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeService;
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeParams;
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeResult;

import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeService;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeParams;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeResult;

import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeService;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeParams;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeResult;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeService;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeService;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeService;
// asServiceDtoImportStatement
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeDTO;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeDTO;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeParams;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeParams;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeParams;
// asServiceResultImportStatement
import au.awh.file.equipmenttype.changeequipmenttype.ChangeEquipmentTypeResult;
import au.awh.file.equipmenttype.createequipmenttype.CreateEquipmentTypeResult;
import au.awh.file.equipmenttype.deleteequipmenttype.DeleteEquipmentTypeResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END



import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * EDTFIL controller for 'Edt Equipment Types' (WSPLTWJEFK) of file 'Equipment Type' (WSEQTYPE)
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 */
@Service(value = "EquipmentType_EdtEquipmentTypes")
public class EdtEquipmentTypesService extends AbstractService<EdtEquipmentTypesService, EdtEquipmentTypesState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private EquipmentTypeRepository equipmentTypeRepository;
	
	@Autowired
	private ChangeEquipmentTypeService changeEquipmentTypeService;
	
	@Autowired
	private CreateEquipmentTypeService createEquipmentTypeService;
	
	@Autowired
	private DeleteEquipmentTypeService deleteEquipmentTypeService;
	

    @Autowired
    private EdtEquipmentTypesService edtEquipmentTypesService;

    @Autowired
    private MessageSource messageSource;

//    @Autowired
//    private EdtEquipmentTypesValidator edtEquipmentTypesValidator;


    
	public static final String SCREEN_CTL = "edtEquipmentTypes";
    public static final String SCREEN_RCD = "EdtEquipmentTypes.rcd";
    public static final String SCREEN_CFM = "_SysConfirmPrompt";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", EdtEquipmentTypesParams.class, this::executeService);
    private final Step response = define("ctlScreen", EdtEquipmentTypesState.class, this::processResponse);
	private final Step confirmScreenResponse = define("cfmscreen", EdtEquipmentTypesDTO.class, this::processConfirmScreenResponse);

	//private final Step serviceCreateEquipmentType = define("serviceCreateEquipmentType",CreateEquipmentTypeResult.class, this::processServiceCreateEquipmentType);
	//private final Step serviceDeleteEquipmentType = define("serviceDeleteEquipmentType",DeleteEquipmentTypeResult.class, this::processServiceDeleteEquipmentType);
	//private final Step serviceChangeEquipmentType = define("serviceChangeEquipmentType",ChangeEquipmentTypeResult.class, this::processServiceChangeEquipmentType);
	

    
    @Autowired
    public EdtEquipmentTypesService() {
        super(EdtEquipmentTypesService.class, EdtEquipmentTypesState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * EDTFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EdtEquipmentTypesState state, EdtEquipmentTypesParams params) {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, state);
        usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN  initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = usrInitializeSubfileHeader(state);

        dbfReadFirstDataRecord(state);
        if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
            state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
        } else {
            state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        stepResult = loadNextSubfilePage(state);

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            for (EdtEquipmentTypesGDO gdo : state.getPageGdo().getContent()) {
                stepResult = usrInitializeSubfileRecordExistingRecord(state, gdo);
                validateSubfileRecord(state, gdo);
                //stepResult = dbfUpdateDataRecord(state, gdo);
            }
        } else {
            EdtEquipmentTypesGDO gdo = new EdtEquipmentTypesGDO();
            stepResult = usrInitializeSubfileRecordNewRecord(state, gdo);
            validateSubfileRecord(state, gdo);
            //stepResult = dbfUpdateDataRecord(state, gdo);
        }

        return stepResult;
    }

    /**
     * SCREEN  validate subfile record.
     *
     * @param gdo - subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileRecord(EdtEquipmentTypesState state, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  initial processing loop method.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                Pageable pageable = PageRequest.of(state.getPage(), state.getSize());
                EdtEquipmentTypesGDO[] content = new EdtEquipmentTypesGDO[state.getSize()];
                for (int i = 0; i < state.getSize(); i++) {
                    content[i] = new EdtEquipmentTypesGDO();
                }
                RestResponsePage<EdtEquipmentTypesGDO> pageGdo = new RestResponsePage<>(Arrays.asList(content), pageable, 1);
                state.setPageGdo(pageGdo);
            }
            EdtEquipmentTypesDTO dto = new EdtEquipmentTypesDTO();
            BeanUtils.copyProperties(state, dto);
            stepResult = callScreen(SCREEN_CTL, dto).thenCall(response);
        } else {
            stepResult = mainLoop(state);
        }

        return stepResult;
    }

    /**
     * SCREEN_KEY returned response processing method.
     *
     * @param state      - Service state class.
     * @param fromScreen - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(EdtEquipmentTypesState state, EdtEquipmentTypesDTO fromScreen) {
        StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(fromScreen, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isChangeMode(state.get_SysCmdKey())) {
        	if (state.get_SysProgramMode() != ProgramModeEnum._STA_ADD) {
        		state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
	            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        	}else {
        		   state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        	}        	
	            stepResult = conductScreenConversation(state);
        }  else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
            stepResult = conductScreenConversation(state);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = processHelpRequest(state); //synon built-in function
            stepResult = conductScreenConversation(state);
        } else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
            stepResult = conductScreenConversation(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    private StepResult processHelpRequest(EdtEquipmentTypesState state) {
        return NO_ACTION;
    }

    /**
     * SCREEN process screen.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = validateSubfileControl(state);
        if (state.get_SysErrorFound() || stepResult != NO_ACTION) {
            return stepResult;
        }

        for (EdtEquipmentTypesGDO gdo : state.getPageGdo().getContent()) {
            if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                stepResult = checkFields(gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = usrValidateSubfileRecordFields(state, gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = checkRelations(gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
                stepResult = usrSubfileRecordFunctionFields(state, gdo);
                stepResult = usrValidateSubfileRecordRelations(state, gdo);
                if (state.get_SysErrorFound()) {
                    return stepResult;
                }
//                stepResult = dbfUpdateDataRecord(state, gdo);
            }
        }

        EdtEquipmentTypesDTO dto = new EdtEquipmentTypesDTO();
        BeanUtils.copyProperties(state, dto);
//
//        //bypass confirm screen for now
//        state.setConfirm(true);
//
//        EdtEquipmentTypesDTO model = new EdtEquipmentTypesDTO();
//        BeanUtils.copyProperties(state,model);
//        stepResult = processConfirmScreenResponse(state, model);
        stepResult = callScreen(SCREEN_CFM, dto).thenCall(confirmScreenResponse);

        return stepResult;
    }

    /**
     * SCREEN  check the fields of GDO.
     *
     * @param gdo -  Service subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult checkFields(EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  check the relations of GDO.
     *
     * @param gdo -  Service subfile record class.
     * @return stepResult - Step result.
     */
    private StepResult checkRelations(EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN  validate subfile control.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileControl(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);

        stepResult = validateSubfileControlField(state);
        if (state.get_SysErrorFound()) {
            return stepResult;
        }

        stepResult = usrSubfileControlFunctionFields(state);
        stepResult = usrValidateSubfileControl(state);

        return stepResult;
    }

    /**
     * SCREEN  validate subfile control field.
     *
     * @param state -  Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateSubfileControlField(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        return stepResult;
    }

    /**
     * SCREEN_CONFIRM returned response processing method.
     *
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processConfirmScreenResponse(EdtEquipmentTypesState state, EdtEquipmentTypesDTO model) {
        StepResult stepResult = NO_ACTION;
        boolean requestReloadSubfile = false;

        BeanUtils.copyProperties(model,state);

//        if (!state.isConfirm()) {
//            return stepResult;
//        }
//        else {
//            state.setConfirm(false);
//        }

        for (EdtEquipmentTypesGDO gdo : state.getPageGdo().getContent()) {
            if (gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                if ("DELETE".equals(gdo.get_SysSelected().toUpperCase()) ||
                        (state.get_SysProgramMode() == ProgramModeEnum._STA_DELETE)) {
                    stepResult = dbfDeleteDataRecord(state, gdo);
                    requestReloadSubfile = true;
                } else {
                    if ("CHANGE".equals(gdo.get_SysSelected())) {
                        stepResult = dbfUpdateDataRecord(state, gdo);
                        requestReloadSubfile = true;
                    } else {
                        // if *Program mode is *ADD
                        stepResult = dbfCreateDataRecord(state, gdo);
                        requestReloadSubfile = false;
                    }
                }
                stepResult = usrExtraProcessingAfterDBFUpdate(state, gdo);
            }
        }

        if (state.get_SysErrorFound()) {
            return stepResult;
        }

        if (requestReloadSubfile) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        }

        if (state.get_SysReloadSubfile() == ReloadSubfileEnum._STA_YES) {
            // request subfile reload if necessary
	    }

        if (isChangeMode(state.get_SysCmdKey())) {
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
                Pageable pageable = PageRequest.of(state.getPage(), state.getSize());
                EdtEquipmentTypesGDO[] content = new EdtEquipmentTypesGDO[state.getSize()];
                for (int i = 0; i < state.getSize(); i++) {
                    content[i] = new EdtEquipmentTypesGDO();
                }
                RestResponsePage<EdtEquipmentTypesGDO> pageGdo = new RestResponsePage<>(Arrays.asList(content), pageable, 1);
                state.setPageGdo(pageGdo);
            } else {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
                state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
            }
        } else {
            for (EdtEquipmentTypesGDO gdo : state.getPageGdo().getContent()) {
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = usrProcessCommandKeys(state, gdo);
                }
            }
            if (stepResult != StepResult.NO_ACTION) {
                return stepResult;
            }
        }
        if (stepResult != StepResult.NO_ACTION) {
            return stepResult;
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     *
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        stepResult = exitProgram(state);

        return stepResult;
    }

    /**
     * Exit this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult exitProgram(EdtEquipmentTypesState state) {
        StepResult stepResult = NO_ACTION;

        EdtEquipmentTypesResult params = new EdtEquipmentTypesResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(EdtEquipmentTypesState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
    private void dbfReadNextPageRecord(EdtEquipmentTypesState state) {
        state.setPage(state.getPage() + 1);
        dbfReadDataRecord(state);
    }

    /**
     * Read record data.
     * @param state - Service state class.
     */
    private void dbfReadDataRecord(EdtEquipmentTypesState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
            System.out.println("Read record error: " + Arrays.toString(ioe.getStackTrace()));
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<EdtEquipmentTypesGDO> pageGdo = equipmentTypeRepository.edtEquipmentTypes(pageable);
        state.setPageGdo(pageGdo);
    }


    /**
     * Create record data.
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfCreateDataRecord(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CRTOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Create Object (Generated:1387)
			 */
			CreateEquipmentTypeResult createEquipmentTypeResult = null;
			CreateEquipmentTypeParams createEquipmentTypeParams = null;
			// DEBUG genFunctionCall BEGIN 1388 ACT Create Equipment Type - Equipment Type  *
			createEquipmentTypeParams = new CreateEquipmentTypeParams();
			createEquipmentTypeResult = new CreateEquipmentTypeResult();
			createEquipmentTypeParams.setEquipmentTypeCode(gdo.getEquipmentTypeCode());
			createEquipmentTypeParams.setEquipmentTypeDesc(gdo.getEquipmentTypeDesc());
			createEquipmentTypeParams.setEquipReadingUnit(gdo.getEquipReadingUnit());
			stepResult = createEquipmentTypeService.execute(createEquipmentTypeParams);
			createEquipmentTypeResult = (CreateEquipmentTypeResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(createEquipmentTypeResult , dto);
			stepResult = NO_ACTION;
			// DEBUG genFunctionCall END
			dto.addNotification(dto.get_SysReturnCode().getDescription(), messageSource);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }


    /**
     * Delete record data
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfDeleteDataRecord(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val functions = x2EFile.functions.filter { it.functionType == "DLTOBJ" }

//        val dbfOBJ: X2EFunction = if (functions.isNotEmpty()) getFunctionForType(x2EFile, "DLTOBJ") else function
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
        	/**
			 * USER: Delete Object (Generated:1389)
			 */
			DeleteEquipmentTypeParams deleteEquipmentTypeParams = null;
			DeleteEquipmentTypeResult deleteEquipmentTypeResult = null;
			// DEBUG genFunctionCall BEGIN 1390 ACT Delete Equipment Type - Equipment Type  *
			deleteEquipmentTypeParams = new DeleteEquipmentTypeParams();
			deleteEquipmentTypeResult = new DeleteEquipmentTypeResult();
			deleteEquipmentTypeParams.setEquipmentTypeCode(gdo.getEquipmentTypeCode());
			stepResult = deleteEquipmentTypeService.execute(deleteEquipmentTypeParams);
			deleteEquipmentTypeResult = (DeleteEquipmentTypeResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(deleteEquipmentTypeResult , dto);
			stepResult = NO_ACTION;
			// DEBUG genFunctionCall END	
			dto.addNotification(dto.get_SysReturnCode().getDescription(), messageSource);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }


    /**
     * Update record data
     * @param dto - Service state class.
     * @param gdo - Service GDO class.
     * @return stepResult - Step result.
     */
    private StepResult dbfUpdateDataRecord(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;


//        val dbfOBJ: X2EFunction = getFunctionForType(x2EFile, "CHGOBJ")
//        val dtoOBJ = dbfOBJ.asServiceDtoClassName()
//        val dtoVAR = dbfOBJ.asServiceDtoVariableName()
        try {
			EquipmentTypeId equipmentTypeId = new EquipmentTypeId(gdo.getEquipmentTypeCode());
			if (equipmentTypeRepository.existsById(equipmentTypeId)) {
			
        		/**
				 * USER: Change Object (Generated:1391)
				 */
				ChangeEquipmentTypeResult changeEquipmentTypeResult = null;
				ChangeEquipmentTypeParams changeEquipmentTypeParams = null;
				// DEBUG genFunctionCall BEGIN 1392 ACT Change Equipment Type - Equipment Type  *
				changeEquipmentTypeParams = new ChangeEquipmentTypeParams();
				changeEquipmentTypeResult = new ChangeEquipmentTypeResult();
				changeEquipmentTypeParams.setEquipmentTypeCode(gdo.getEquipmentTypeCode());
				changeEquipmentTypeParams.setEquipmentTypeDesc(gdo.getEquipmentTypeDesc());
				changeEquipmentTypeParams.setEquipReadingUnit(gdo.getEquipReadingUnit());
				stepResult = changeEquipmentTypeService.execute(changeEquipmentTypeParams);
				changeEquipmentTypeResult = (ChangeEquipmentTypeResult)((ReturnFromService)stepResult.getAction()).getResults();
				BeanUtils.copyProperties(changeEquipmentTypeResult , dto);
				stepResult = NO_ACTION;
				// DEBUG genFunctionCall END						
				dto.addNotification(dto.get_SysReturnCode().getDescription(), null);
            }
            else {
                //throw new ServiceException("diagnosis.nf");            	
                dto.addNotification("equipment.type.nf", messageSource);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
	}


    
	/**
	 * USER: Initialize Program (Generated:15)
	 */
    private StepResult usrInitializeProgram(EdtEquipmentTypesState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
			
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Header (Generated:1488)
	 */
    private StepResult usrInitializeSubfileHeader(EdtEquipmentTypesState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1488 -
        
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record (New Record) (Generated:1027)
	 */
    private StepResult usrInitializeSubfileRecordNewRecord(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1027 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record (Existing Record) (Generated:1023)
	 */
    private StepResult usrInitializeSubfileRecordExistingRecord(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1023 -

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:1463)
	 */
    private StepResult usrSubfileControlFunctionFields(EdtEquipmentTypesState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1463 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Control (Generated:37)
	 */
    private StepResult usrValidateSubfileControl(EdtEquipmentTypesState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				if (isCancel_1(dto.get_SysCmdKey())) {
					// CTL.*CMD key is *Cancel
					// DEBUG genFunctionCall BEGIN 1000042 ACT PAR.Exit Program Option = CND.Cancel requested
					dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000046 ACT Exit program - return code CND.*Normal
					dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
					stepResult = exitProgram(dto);
					//dummy conditional return to avoid dead code error...
					if(stepResult != NO_ACTION) {
					    return stepResult;
					}
			
					// DEBUG genFunctionCall END
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Record Fields (Generated:1496)
	 */
    private StepResult usrValidateSubfileRecordFields(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1496 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:1449)
	 */
    private StepResult usrSubfileRecordFunctionFields(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1449 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Subfile Record Relations (Generated:1171)
	 */
    private StepResult usrValidateSubfileRecordRelations(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				if (!(gdo.get_SysSelected().equals("Not entered"))) {
					// NOT RCD.*SFLSEL is Not entered
					// DEBUG genFunctionCall BEGIN 1000320 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
					// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
					// DEBUG genFunctionCall END
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Extra Processing After DBF Update (Generated:1442)
	 */
    private StepResult usrExtraProcessingAfterDBFUpdate(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1442 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1420)
	 */
    private StepResult usrProcessCommandKeys(EdtEquipmentTypesState dto, EdtEquipmentTypesGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        		
				// Unprocessed SUB 1420 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1262)
	 */
    private StepResult usrExitProgramProcessing(EdtEquipmentTypesState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        		
				if (isExit(dto.get_SysCmdKey())) {
					// CTL.*CMD key is *Exit
					// DEBUG genFunctionCall BEGIN 1000251 ACT PAR.Exit Program Option = CND.Exit requested
					dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
					// DEBUG genFunctionCall END
				}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * CreateEquipmentTypeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateEquipmentType(EdtEquipmentTypesState state, CreateEquipmentTypeResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * DeleteEquipmentTypeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDeleteEquipmentType(EdtEquipmentTypesState state, DeleteEquipmentTypeResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
////
//    /**
//     * ChangeEquipmentTypeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangeEquipmentType(EdtEquipmentTypesState state, ChangeEquipmentTypeResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//


}
