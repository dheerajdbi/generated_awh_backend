package au.awh.file.equipmenttype.edtequipmenttypes;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.equipmenttype.EquipmentType;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.EquipReadingUnitEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;

/**
 * Gdo for file 'Equipment Type' (WSEQTYPE) and function 'Edt Equipment Types' (WSPLTWJEFK).
 *
 * @author X2EGenerator EDTFILJavaControllerGenerator.kt
 *
 */
public class EdtEquipmentTypesGDO implements Serializable {
	private static final long serialVersionUID = 9035272723317605254L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String equipmentTypeCode = "";
	private String equipmentTypeDesc = "";
	private EquipReadingUnitEnum equipReadingUnit = null;

	public EdtEquipmentTypesGDO() {

	}

   	public EdtEquipmentTypesGDO(String equipmentTypeCode, String equipmentTypeDesc, EquipReadingUnitEnum equipReadingUnit) {
		this.equipmentTypeCode = equipmentTypeCode;
		this.equipmentTypeDesc = equipmentTypeDesc;
		this.equipReadingUnit = equipReadingUnit;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
    	this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
    	this.equipmentTypeDesc = equipmentTypeDesc;
    }

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
    	this.equipReadingUnit = equipReadingUnit;
    }

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}


    /**
     * Copies the fields of the Entity bean into the GDO bean.
     *
     * @param equipmentType EquipmentType Entity bean
     */
    public void setDtoFields(EquipmentType equipmentType) {
  BeanUtils.copyProperties(equipmentType, this);
    }

    /**
     * Copies the fields of the GDO bean into the Entity bean.
     *
     * @param equipmentType EquipmentType Entity bean
     */
    public void setEntityFields(EquipmentType equipmentType) {
      BeanUtils.copyProperties(this, equipmentType);
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}