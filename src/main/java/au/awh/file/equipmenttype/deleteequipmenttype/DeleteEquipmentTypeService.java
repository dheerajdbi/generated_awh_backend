package au.awh.file.equipmenttype.deleteequipmenttype;

// DeleteObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmenttype.EquipmentType;
import au.awh.file.equipmenttype.EquipmentTypeId;
import au.awh.file.equipmenttype.EquipmentTypeRepository;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * DELOBJ Service controller for 'Delete Equipment Type' (file 'Equipment Type' (WSEQTYPE)
 *
 * @author X2EGenerator  DeleteObjectFunction.kt
 */
@Service
public class DeleteEquipmentTypeService  extends AbstractService<DeleteEquipmentTypeService, DeleteEquipmentTypeDTO>
{
	private final Step execute = define("execute", DeleteEquipmentTypeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentTypeRepository equipmentTypeRepository;

	
	@Autowired
	public DeleteEquipmentTypeService() {
		super(DeleteEquipmentTypeService.class, DeleteEquipmentTypeDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(DeleteEquipmentTypeParams params) throws ServiceException {
		DeleteEquipmentTypeDTO dto = new DeleteEquipmentTypeDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(DeleteEquipmentTypeDTO dto, DeleteEquipmentTypeParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);

		EquipmentTypeId equipmentTypeId = new EquipmentTypeId();
		equipmentTypeId.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		try {
			equipmentTypeRepository.deleteById(equipmentTypeId);
			equipmentTypeRepository.flush();
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			System.out.println("Delete error: " + Arrays.toString(e.getStackTrace()));
			dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
		}

		DeleteEquipmentTypeResult result = new DeleteEquipmentTypeResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(DeleteEquipmentTypeDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(DeleteEquipmentTypeDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
		return stepResult;
	}


}
