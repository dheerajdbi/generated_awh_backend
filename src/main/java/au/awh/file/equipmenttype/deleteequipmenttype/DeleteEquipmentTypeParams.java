package au.awh.file.equipmenttype.deleteequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: DeleteEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DeleteEquipmentTypeParams implements Serializable
{
	private static final long serialVersionUID = -352773738141170193L;

    private String equipmentTypeCode = "";

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
}
