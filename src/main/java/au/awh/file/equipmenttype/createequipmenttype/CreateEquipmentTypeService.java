package au.awh.file.equipmenttype.createequipmenttype;

// CreateObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmenttype.EquipmentType;
import au.awh.file.equipmenttype.EquipmentTypeId;
import au.awh.file.equipmenttype.EquipmentTypeRepository;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CRTOBJ Service controller for 'Create Equipment Type' (file 'Equipment Type' (WSEQTYPE)
 *
 * @author X2EGenerator  CreateObjectFunction.kt
 */
@Service
public class CreateEquipmentTypeService  extends AbstractService<CreateEquipmentTypeService, CreateEquipmentTypeDTO>
{
	private final Step execute = define("execute", CreateEquipmentTypeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentTypeRepository equipmentTypeRepository;

	
	@Autowired
	public CreateEquipmentTypeService() {
		super(CreateEquipmentTypeService.class, CreateEquipmentTypeDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CreateEquipmentTypeParams params) throws ServiceException {
		CreateEquipmentTypeDTO dto = new CreateEquipmentTypeDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CreateEquipmentTypeDTO dto, CreateEquipmentTypeParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		EquipmentType equipmentType = new EquipmentType();
		equipmentType.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		equipmentType.setEquipmentTypeDesc(dto.getEquipmentTypeDesc());
		equipmentType.setEquipReadingUnit(dto.getEquipReadingUnit());

		processingBeforeDataUpdate(dto, equipmentType);

		EquipmentType equipmentType2 = equipmentTypeRepository.findById(equipmentType.getId()).orElse(null);
		if (equipmentType2 != null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
			processingIfDataRecordAlreadyExists(dto, equipmentType2);
		}
		else {
			try {
				equipmentTypeRepository.saveAndFlush(equipmentType);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto, equipmentType);
			} catch (Exception e) {
				System.out.println("Create error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
				processingIfDataUpdateError(dto, equipmentType);
			}
		}

		CreateEquipmentTypeResult result = new CreateEquipmentTypeResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(CreateEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordAlreadyExists(CreateEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(CreateEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataUpdateError(CreateEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		
		return stepResult;
	}


}
