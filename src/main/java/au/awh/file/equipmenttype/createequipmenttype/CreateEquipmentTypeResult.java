package au.awh.file.equipmenttype.createequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CreateEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreateEquipmentTypeResult implements Serializable
{
	private static final long serialVersionUID = -5508562989351484293L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
