package au.awh.file.equipmenttype.changeequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChangeEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeEquipmentTypeResult implements Serializable
{
	private static final long serialVersionUID = -5659776974059450754L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
