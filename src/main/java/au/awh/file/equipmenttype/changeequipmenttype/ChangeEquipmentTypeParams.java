package au.awh.file.equipmenttype.changeequipmenttype;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.EquipReadingUnitEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChangeEquipmentType ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangeEquipmentTypeParams implements Serializable
{
	private static final long serialVersionUID = 2155336454260408664L;

    private String equipmentTypeCode = "";
    private String equipmentTypeDesc = "";
    private EquipReadingUnitEnum equipReadingUnit = null;

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}
	
	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipReadingUnit(String equipReadingUnit) {
		setEquipReadingUnit(EquipReadingUnitEnum.valueOf(equipReadingUnit));
	}
}
