package au.awh.file.equipmenttype.changeequipmenttype;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.equipmenttype.EquipmentType;
import au.awh.file.equipmenttype.EquipmentTypeId;
import au.awh.file.equipmenttype.EquipmentTypeRepository;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Change Equipment Type' (file 'Equipment Type' (WSEQTYPE)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChangeEquipmentTypeService extends AbstractService<ChangeEquipmentTypeService, ChangeEquipmentTypeDTO>
{
	private final Step execute = define("execute", ChangeEquipmentTypeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private EquipmentTypeRepository equipmentTypeRepository;

	
	@Autowired
	public ChangeEquipmentTypeService() {
		super(ChangeEquipmentTypeService.class, ChangeEquipmentTypeDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChangeEquipmentTypeParams params) throws ServiceException {
		ChangeEquipmentTypeDTO dto = new ChangeEquipmentTypeDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChangeEquipmentTypeDTO dto, ChangeEquipmentTypeParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		EquipmentTypeId equipmentTypeId = new EquipmentTypeId();
		equipmentTypeId.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		EquipmentType equipmentType = equipmentTypeRepository.findById(equipmentTypeId).orElse(null);
		if (equipmentType == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, equipmentType);
			equipmentType.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			equipmentType.setEquipmentTypeDesc(dto.getEquipmentTypeDesc());
			equipmentType.setEquipReadingUnit(dto.getEquipReadingUnit());

			processingBeforeDataUpdate(dto, equipmentType);

			try {
				equipmentTypeRepository.saveAndFlush(equipmentType);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_UPDATED);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChangeEquipmentTypeResult result = new ChangeEquipmentTypeResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChangeEquipmentTypeDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChangeEquipmentTypeDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChangeEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Empty:48)
		 */
		
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChangeEquipmentTypeDTO dto, EquipmentType equipmentType) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:10)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChangeEquipmentTypeDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Empty:23)
		 */
		
		return stepResult;
	}


}
