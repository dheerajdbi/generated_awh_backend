package au.awh.file.equipmenttype.selequipmenttype;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: SelEquipmentType (WSPLTWLSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelEquipmentTypeParams implements Serializable
{
    private static final long serialVersionUID = -2543492589890092414L;

	private String equipmentTypeCode = "";

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
}

