package au.awh.file.equipmenttype.selequipmenttype;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.EquipReadingUnitEnum;
// generateStatusFieldImportStatements END

/**
 * Gdo for file 'Equipment Type' (WSEQTYPE) and function 'Sel Equipment Type' (WSPLTWLSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelEquipmentTypeGDO implements Serializable {
	private static final long serialVersionUID = -1949619199789218928L;

	private String _sysSelected = "";
	private String equipmentTypeCode = "";
	private String equipmentTypeDesc = "";
	private EquipReadingUnitEnum equipReadingUnit = null;

	public SelEquipmentTypeGDO() {

	}

	public SelEquipmentTypeGDO(String equipmentTypeCode, String equipmentTypeDesc, EquipReadingUnitEnum equipReadingUnit) {
		this.equipmentTypeCode = equipmentTypeCode;
		this.equipmentTypeDesc = equipmentTypeDesc;
		this.equipReadingUnit = equipReadingUnit;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

}
