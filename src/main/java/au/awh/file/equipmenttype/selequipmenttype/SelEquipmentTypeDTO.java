package au.awh.file.equipmenttype.selequipmenttype;

import org.springframework.beans.BeanUtils;

import au.awh.model.EquipReadingUnitEnum;

import au.awh.file.equipmenttype.EquipmentType;
import au.awh.service.data.SelectRecordDTO;

/**
 * Dto for file 'Equipment Type' (WSEQTYPE) and function 'Sel Equipment Type' (WSPLTWLSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelEquipmentTypeDTO extends SelectRecordDTO<SelEquipmentTypeGDO> {
	private static final long serialVersionUID = 8377523721647770768L;

	private String equipmentTypeCode = "";
	private String equipmentTypeDesc = "";
	private EquipReadingUnitEnum equipReadingUnit = null;

	public SelEquipmentTypeDTO() {
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param equipmentType EquipmentType Entity bean
     */
    public void setDtoFields(EquipmentType equipmentType) {
        BeanUtils.copyProperties(equipmentType, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param equipmentType EquipmentType Entity bean
     */
    public void setEntityFields(EquipmentType equipmentType) {
        BeanUtils.copyProperties(this, equipmentType);
    }
}
