package au.awh.file.equipmenttype.selequipmenttype;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: SelEquipmentType (WSPLTWLSRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelEquipmentTypeResult implements Serializable
{
    private static final long serialVersionUID = 4985103244946260702L;

	private String equipmentTypeCode = "";

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
}

