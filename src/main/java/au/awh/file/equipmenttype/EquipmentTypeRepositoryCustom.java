package au.awh.file.equipmenttype;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

import au.awh.file.equipmenttype.edtequipmenttypes.EdtEquipmentTypesGDO;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeGDO;
import au.awh.file.equipmenttype.pmtforequipmentbkdwn.PmtForEquipmentBkdwnGDO;

/**
 * Custom Spring Data JPA repository interface for model: Equipment Type (WSEQTYPE).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface EquipmentTypeRepositoryCustom {

	/**
	 * 
	 * @param equipmentTypeCode Equipment Type Code
	 */
	void deleteEquipmentType(String equipmentTypeCode);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EdtEquipmentTypesGDO
	 */
	RestResponsePage<EdtEquipmentTypesGDO> edtEquipmentTypes(
	Pageable pageable);

	/**
	 * 
	 * @param equipmentTypeCode Equipment Type Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelEquipmentTypeGDO
	 */
	RestResponsePage<SelEquipmentTypeGDO> selEquipmentType(String equipmentTypeCode
	, Pageable pageable);

	/**
	 * 
	 * @param equipmentTypeCode Equipment Type Code
	 * @return EquipmentType
	 */
	EquipmentType getEquipmentType(String equipmentTypeCode
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param centreCodeKey Centre Code Key
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PmtForEquipmentBkdwnGDO
	 */
	RestResponsePage<PmtForEquipmentBkdwnGDO> pmtForEquipmentBkdwn(
	String awhRegionCode, String centreCodeKey, Pageable pageable);
}
