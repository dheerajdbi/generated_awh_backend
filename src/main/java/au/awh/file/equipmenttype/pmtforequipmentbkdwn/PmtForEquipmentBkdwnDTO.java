package au.awh.file.equipmenttype.pmtforequipmentbkdwn;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.freschelegacy.utils.RestResponsePage;

import au.awh.model.GlobalContext;
import au.awh.file.equipmenttype.EquipmentType;
// generateStatusFieldImportStatements BEGIN 31
import au.awh.model.FirstPassEnum;
import au.awh.model.PrintEmailOrDisplayEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Equipment Type' (WSEQTYPE) and function 'Pmt for Equipment Bkdwn' (WSPLTFZDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class PmtForEquipmentBkdwnDTO extends BaseDTO {
	private static final long serialVersionUID = -4102814427747103173L;

    private RestResponsePage<PmtForEquipmentBkdwnGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String awhRegionCode = "";
	private String conditionName = "";
	private String centreCodeKey = "";
	private String centreNameDrv = "";
	private LocalDate dateFromIso = null;
	private LocalDate dateToIso = null;
	private PrintEmailOrDisplayEnum printEmailOrDisplay = null;
	private String emailAddress60 = "";
	private String equipmentTypeCode = "";


	private PmtForEquipmentBkdwnGDO gdo;

    public PmtForEquipmentBkdwnDTO() {

    }
    public void setPageGdo(RestResponsePage<PmtForEquipmentBkdwnGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<PmtForEquipmentBkdwnGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
    }

    public String getConditionName() {
    	return conditionName;
    }

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
    }

    public String getCentreCodeKey() {
    	return centreCodeKey;
    }

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
    }

    public String getCentreNameDrv() {
    	return centreNameDrv;
    }

	public void setDateFromIso(LocalDate dateFromIso) {
		this.dateFromIso = dateFromIso;
    }

    public LocalDate getDateFromIso() {
    	return dateFromIso;
    }

	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
    }

    public LocalDate getDateToIso() {
    	return dateToIso;
    }

	public void setPrintEmailOrDisplay(PrintEmailOrDisplayEnum printEmailOrDisplay) {
		this.printEmailOrDisplay = printEmailOrDisplay;
    }

    public PrintEmailOrDisplayEnum getPrintEmailOrDisplay() {
    	return printEmailOrDisplay;
    }

	public void setEmailAddress60(String emailAddress60) {
		this.emailAddress60 = emailAddress60;
    }

    public String getEmailAddress60() {
    	return emailAddress60;
    }

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeCode() {
    	return equipmentTypeCode;
    }

	public void setGdo(PmtForEquipmentBkdwnGDO gdo) {
		this.gdo = gdo;
	}

	public PmtForEquipmentBkdwnGDO getGdo() {
		return gdo;
	}

}