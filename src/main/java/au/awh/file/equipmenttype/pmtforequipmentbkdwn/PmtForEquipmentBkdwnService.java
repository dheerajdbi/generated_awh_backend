package au.awh.file.equipmenttype.pmtforequipmentbkdwn;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;


import com.freschelegacy.utils.DateTimeUtils;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.equipmenttype.EquipmentTypeRepository;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionService;
import au.awh.file.utilitiesdatetime.getlastmonthdates.GetLastMonthDatesService;
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsService;
import au.awh.file.utilitiesprintandemail.getcurrentuseremail.GetCurrentUserEmailService;
// asServiceDtoImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionDTO;
import au.awh.file.utilitiesdatetime.getlastmonthdates.GetLastMonthDatesDTO;
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsDTO;
import au.awh.file.utilitiesprintandemail.getcurrentuseremail.GetCurrentUserEmailDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionParams;
import au.awh.file.utilitiesdatetime.getlastmonthdates.GetLastMonthDatesParams;
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsParams;
import au.awh.file.utilitiesprintandemail.getcurrentuseremail.GetCurrentUserEmailParams;
// asServiceResultImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionResult;
import au.awh.file.utilitiesdatetime.getlastmonthdates.GetLastMonthDatesResult;
import au.awh.file.utilitiesos400api.rtvusrprfdetails.RtvusrprfDetailsResult;
import au.awh.file.utilitiesprintandemail.getcurrentuseremail.GetCurrentUserEmailResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


// generateImportsForEnum
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 15
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.PrintEmailOrDisplayEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
import au.awh.model.SelectedEnum;
// generateStatusFieldImportStatements END

/**
 * DSPFIL Service controller for 'Pmt for Equipment Bkdwn' (WSPLTFZDFK) of file 'Equipment Type' (WSEQTYPE)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class PmtForEquipmentBkdwnService extends AbstractService<PmtForEquipmentBkdwnService, PmtForEquipmentBkdwnState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private EquipmentTypeRepository equipmentTypeRepository;
    
    @Autowired
    private GetCurrentUserEmailService getCurrentUserEmailService;
    
    @Autowired
    private GetLastMonthDatesService getLastMonthDatesService;
    
    @Autowired
    private GeteCentresRegionService geteCentresRegionService;
    
    @Autowired
    private RtvusrprfDetailsService rtvusrprfDetailsService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "pmtForEquipmentBkdwn";
    public static final String SCREEN_RCD = "PmtForEquipmentBkdwn.rcd";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", PmtForEquipmentBkdwnParams.class, this::executeService);
    private final Step response = define("response", PmtForEquipmentBkdwnDTO.class, this::processResponse);

	//private final Step serviceGeteCentresRegion = define("serviceGeteCentresRegion",GeteCentresRegionResult.class, this::processServiceGeteCentresRegion);
	//private final Step serviceGetLastMonthDates = define("serviceGetLastMonthDates",GetLastMonthDatesResult.class, this::processServiceGetLastMonthDates);
	//private final Step serviceGetCurrentUserEmail = define("serviceGetCurrentUserEmail",GetCurrentUserEmailResult.class, this::processServiceGetCurrentUserEmail);
	//private final Step serviceRtvusrprfDetails = define("serviceRtvusrprfDetails",RtvusrprfDetailsResult.class, this::processServiceRtvusrprfDetails);
	

    
    @Autowired
    public PmtForEquipmentBkdwnService() {
        super(PmtForEquipmentBkdwnService.class, PmtForEquipmentBkdwnState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(PmtForEquipmentBkdwnState state, PmtForEquipmentBkdwnParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_CTL initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(PmtForEquipmentBkdwnState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Load pageGdo
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(PmtForEquipmentBkdwnState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrInitializeSubfileControl(state);

		dbfReadFirstDataRecord(state);
		if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
		    stepResult = loadNextSubfilePage(state);
		}

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(PmtForEquipmentBkdwnState state) {
    	StepResult stepResult = NO_ACTION;

    	List<PmtForEquipmentBkdwnGDO> list = state.getPageGdo().getContent();
        for (PmtForEquipmentBkdwnGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            stepResult = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//            TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//            TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(PmtForEquipmentBkdwnState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            PmtForEquipmentBkdwnDTO model = new PmtForEquipmentBkdwnDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(PmtForEquipmentBkdwnState state, PmtForEquipmentBkdwnDTO model) {
    	StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (isHelp(state.get_SysCmdKey())) {
//        TODO:processHelpRequest(state);//synon built-in function
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(PmtForEquipmentBkdwnState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrProcessSubfilePreConfirm(state);
        if ( stepResult != NO_ACTION) {
            return stepResult;
        }

        if (state.get_SysErrorFound()) {
            return closedown(state);
        } else {
            if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
                return closedown(state);
            } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	stepResult = usrProcessCommandKeys(state);
//		        }

                stepResult = usrProcessSubfileControlPostConfirm(state);
                for (PmtForEquipmentBkdwnGDO gdo : state.getPageGdo().getContent()) {
                    if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                        stepResult = usrProcessSubfileRecordPostConfirm(state, gdo);
                        
//	                    TODO:writeSubfileRecord(state);   // synon built-in function
		            }
		        }
		        stepResult = usrFinalProcessingPostConfirm(state);
                stepResult = usrProcessCommandKeys(state);
            }
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult usrProcessSubfilePreConfirm(PmtForEquipmentBkdwnState state) {
    	StepResult stepResult = NO_ACTION;

	stepResult = usrSubfileControlFunctionFields(state);
        stepResult = usrProcessSubfileControlPreConfirm(state);
    	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
            for (PmtForEquipmentBkdwnGDO gdo : state.getPageGdo().getContent()) {
                
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = usrSubfileRecordFunctionFields(state, gdo);
                    stepResult = usrProcessSubfileRecordPreConfirm(state, gdo);
                    if (stepResult != NO_ACTION) {
                        return stepResult;
                    }
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
                
            }
        }

	stepResult = usrFinalProcessingPreConfirm(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(PmtForEquipmentBkdwnState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        PmtForEquipmentBkdwnResult params = new PmtForEquipmentBkdwnResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(PmtForEquipmentBkdwnState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
	private void dbfReadNextPageRecord(PmtForEquipmentBkdwnState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(PmtForEquipmentBkdwnState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<PmtForEquipmentBkdwnGDO> pageGdo = equipmentTypeRepository.pmtForEquipmentBkdwn(state.getAwhRegionCode(), state.getCentreCodeKey(), pageable);
        state.setPageGdo(pageGdo);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteCentresRegionResult geteCentresRegionResult = null;
			GeteCentresRegionParams geteCentresRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1000043 ACT GetE Centres Region - AWH Region/Centre  *
			geteCentresRegionParams = new GeteCentresRegionParams();
			geteCentresRegionResult = new GeteCentresRegionResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteCentresRegionParams);
			geteCentresRegionParams.setCentreCodeKey(dto.getCentreCodeKey());
			geteCentresRegionParams.setErrorProcessing("2");
			geteCentresRegionParams.setGetRecordOption("");
			stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
			geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteCentresRegionResult.get_SysReturnCode());
			dto.setAwhRegionCode(geteCentresRegionResult.getAwhRegionCode());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000195 ACT LCL.First Pass = CND.First Pass
			dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GetLastMonthDatesParams getLastMonthDatesParams = null;
			GetLastMonthDatesResult getLastMonthDatesResult = null;
			// DEBUG genFunctionCall BEGIN 1000307 ACT CTL = PAR By name
			// TODO: *MOVE ALL is not supported for parameters: CTL, PAR,    ,    ,    
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000055 ACT Get Last Month Dates - Utilities Date/Time  *
			getLastMonthDatesParams = new GetLastMonthDatesParams();
			getLastMonthDatesResult = new GetLastMonthDatesResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), getLastMonthDatesParams);
			stepResult = getLastMonthDatesService.execute(getLastMonthDatesParams);
			getLastMonthDatesResult = (GetLastMonthDatesResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getLastMonthDatesResult.get_SysReturnCode());
			dto.setDateFromIso(getLastMonthDatesResult.getDateFromIso());
			dto.setDateToIso(getLastMonthDatesResult.getDateToIso());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000065 ACT CTL.Print, Email or Display = CND.Display
			dto.setPrintEmailOrDisplay(PrintEmailOrDisplayEnum._DISPLAY);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000059 ACT LCL.Print, Email or Display = CTL.Print, Email or Display
			dto.setLclPrintEmailOrDisplay(dto.getPrintEmailOrDisplay());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000241 ACT LCL.Text Field 160 Chars = CND.Not Entered
			dto.setLclTextField160Chars("");
			// DEBUG genFunctionCall END
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(PmtForEquipmentBkdwnState dto, PmtForEquipmentBkdwnGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if ((dto.getLclFirstPass() == FirstPassEnum._FIRST_PASS) && ((gdo.getEquipmentTypeCode().equals("FORC")) || (gdo.getEquipmentTypeCode().equals("FORK")))) {
				// DEBUG genFunctionCall BEGIN 1000179 ACT RCD.Selected = CND.Selected
				gdo.setSelected(SelectedEnum._SELECTED);
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000346 ACT PGM.*Re-read Subfile Record = CND.*YES
			dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum._STA_YES);
			// DEBUG genFunctionCall END
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000002 ACT Centre Name Drv          *FIELD                                             CTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             CTLC 1000002
			// DEBUG X2EActionDiagramElement 1000002 ACT     Centre Name Drv          *FIELD                                             CTLC
			// DEBUG X2EActionDiagramElement 1000003 PAR CTL 
			// DEBUG X2EActionDiagramElement 1000004 PAR CTL 
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	RtvusrprfDetailsResult rtvusrprfDetailsResult = null;
			GetCurrentUserEmailParams getCurrentUserEmailParams = null;
			GetCurrentUserEmailResult getCurrentUserEmailResult = null;
			RtvusrprfDetailsParams rtvusrprfDetailsParams = null;
			// DEBUG genFunctionCall BEGIN 1000189 ACT LCL.First Pass = CND.Not First Pass
			dto.setLclFirstPass(FirstPassEnum._NOT_FIRST_PASS);
			// DEBUG genFunctionCall END
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			dto.getPrintEmailOrDisplay() != dto.getLclPrintEmailOrDisplay()
			// DEBUG ComparatorJavaGenerator END
			) {
				// CTL.Print, Email or Display NE LCL.Print, Email or Display
				// DEBUG genFunctionCall BEGIN 1000079 ACT PGM.*Defer confirm = CND.Defer confirm
				dto.set_SysDeferConfirm(DeferConfirmEnum._DEFER_CONFIRM);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000075 ACT LCL.Print, Email or Display = CTL.Print, Email or Display
				dto.setLclPrintEmailOrDisplay(dto.getPrintEmailOrDisplay());
				// DEBUG genFunctionCall END
				if (dto.getPrintEmailOrDisplay() == PrintEmailOrDisplayEnum._DISPLAY) {
					// CTL.Print, Email or Display is Display
					// DEBUG genFunctionCall BEGIN 1000097 ACT CTL.Email Address 60 = CND.Not Entered
					dto.setEmailAddress60("");
					// DEBUG genFunctionCall END
				} else if (dto.getPrintEmailOrDisplay() == PrintEmailOrDisplayEnum._EMAIL) {
					// CTL.Print, Email or Display is Email
					// DEBUG genFunctionCall BEGIN 1000150 ACT Get current User Email - Utilities Print & Email  *
					getCurrentUserEmailParams = new GetCurrentUserEmailParams();
					getCurrentUserEmailResult = new GetCurrentUserEmailResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), getCurrentUserEmailParams);
					stepResult = getCurrentUserEmailService.execute(getCurrentUserEmailParams);
					getCurrentUserEmailResult = (GetCurrentUserEmailResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(getCurrentUserEmailResult.get_SysReturnCode());
					dto.setEmailAddress60(getCurrentUserEmailResult.getEmailAddress60());
					// DEBUG genFunctionCall END
				} else if (dto.getPrintEmailOrDisplay() == PrintEmailOrDisplayEnum._PRINT) {
					// CTL.Print, Email or Display is Print
					// DEBUG genFunctionCall BEGIN 1000153 ACT RTVUSRPRF Details - Utilities OS/400 API  *
					rtvusrprfDetailsParams = new RtvusrprfDetailsParams();
					rtvusrprfDetailsResult = new RtvusrprfDetailsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), rtvusrprfDetailsParams);
					rtvusrprfDetailsParams.setReturnCode(dto.get_SysReturnCode());
					rtvusrprfDetailsParams.setUserId(job.getUser());
					stepResult = rtvusrprfDetailsService.execute(rtvusrprfDetailsParams);
					rtvusrprfDetailsResult = (RtvusrprfDetailsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(rtvusrprfDetailsResult.get_SysReturnCode());
					dto.set_SysReturnCode(rtvusrprfDetailsResult.getReturnCode());
					dto.setEmailAddress60(rtvusrprfDetailsResult.getOutputQueue());
					// DEBUG genFunctionCall END
				}
			}
			if ((PrintEmailOrDisplayEnum.isPrintOrEmail(dto.getPrintEmailOrDisplay())) && (dto.getEmailAddress60() != null && dto.getEmailAddress60().isEmpty())) {
				// DEBUG genFunctionCall BEGIN 1000230 ACT Send error message - 'Value Required'
				//dto.addMessage("emailAddress60", "value.required", messageSource);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(PmtForEquipmentBkdwnState dto, PmtForEquipmentBkdwnGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(PmtForEquipmentBkdwnState dto, PmtForEquipmentBkdwnGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (gdo.get_SysSelected().equals("Select#1") || gdo.get_SysSelected().equals("*Select#2")) {
				// RCD.*SFLSEL is *Select request
				if (gdo.getSelected() == SelectedEnum._SELECTED) {
					// RCD.Selected is Selected
					// DEBUG genFunctionCall BEGIN 1000209 ACT RCD.Selected = CND.Not Selected
					gdo.setSelected(SelectedEnum._NOT_SELECTED);
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000217 ACT RCD.Selected = CND.Selected
					gdo.setSelected(SelectedEnum._SELECTED);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000317 ACT RCD.*SFLSEL = CND.Not entered
				gdo.set_SysSelected(null);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000273 ACT LCL.Days Difference = CTL.Date To ISO - CTL.Date From ISO *DAYS
			// TODO: The '*DURATION' built-in Function is not supported yet
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(PmtForEquipmentBkdwnState dto, PmtForEquipmentBkdwnGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (gdo.getSelected() == SelectedEnum._SELECTED) {
				// RCD.Selected is Selected
				if (dto.getLclTextField160Chars() != null && dto.getLclTextField160Chars().isEmpty()) {
					// LCL.Text Field 160 Chars is Not Entered
					// DEBUG genFunctionCall BEGIN 1000371 ACT LCL.Text Field 160 Chars = CONCAT(CON.",RCD.Equipment Type Code,CON.*ZERO)
					dto.setLclTextField160Chars("\"".trim().concat(gdo.getEquipmentTypeCode()));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000361 ACT LCL.Text Field 160 Chars = CONCAT(LCL.Text Field 160 Chars,CON.",CON.*ZERO)
					dto.setLclTextField160Chars(dto.getLclTextField160Chars().trim().concat("\""));
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000399 ACT LCL.Text Field 160 Chars = CONCAT(LCL.Text Field 160 Chars,CON.,",CON.0)
					dto.setLclTextField160Chars(dto.getLclTextField160Chars().trim().concat(",\""));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000393 ACT LCL.Text Field 160 Chars = CONCAT(LCL.Text Field 160 Chars,RCD.Equipment Type Code,CON.*ZERO)
					dto.setLclTextField160Chars(dto.getLclTextField160Chars().trim().concat(gdo.getEquipmentTypeCode()));
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000387 ACT LCL.Text Field 160 Chars = CONCAT(LCL.Text Field 160 Chars,CON.",CON.*ZERO)
					dto.setLclTextField160Chars(dto.getLclTextField160Chars().trim().concat("\""));
					// DEBUG genFunctionCall END
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000330 ACT LCL.Date From Txt = CVTVAR(CTL.Date From ISO)
			// Error in CtvarJavaGenerator, Unknowned type for src field 1000330 LCL.Date From Txt = CVTVAR(CTL.Date From ISO) Field objAttribute: REF
			;
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000324 ACT LCL.Date To Txt = CVTVAR(CTL.Date To ISO)
			// Error in CtvarJavaGenerator, Unknowned type for src field 1000324 LCL.Date To Txt = CVTVAR(CTL.Date To ISO) Field objAttribute: REF
			;
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000233 ACT Execute 'Run Equip Segm Script'
			// TODO: Unsupported Message Type EXCMSG (message surrogate = 2028037)
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(PmtForEquipmentBkdwnState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        	
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GeteCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentresRegion(PmtForEquipmentBkdwnState state, GeteCentresRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        PmtForEquipmentBkdwnParams pmtForEquipmentBkdwnParams = new PmtForEquipmentBkdwnParams();
//        BeanUtils.copyProperties(state, pmtForEquipmentBkdwnParams);
//        stepResult = StepResult.callService(PmtForEquipmentBkdwnService.class, pmtForEquipmentBkdwnParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetLastMonthDatesService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetLastMonthDates(PmtForEquipmentBkdwnState state, GetLastMonthDatesParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        PmtForEquipmentBkdwnParams pmtForEquipmentBkdwnParams = new PmtForEquipmentBkdwnParams();
//        BeanUtils.copyProperties(state, pmtForEquipmentBkdwnParams);
//        stepResult = StepResult.callService(PmtForEquipmentBkdwnService.class, pmtForEquipmentBkdwnParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetCurrentUserEmailService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCurrentUserEmail(PmtForEquipmentBkdwnState state, GetCurrentUserEmailParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        PmtForEquipmentBkdwnParams pmtForEquipmentBkdwnParams = new PmtForEquipmentBkdwnParams();
//        BeanUtils.copyProperties(state, pmtForEquipmentBkdwnParams);
//        stepResult = StepResult.callService(PmtForEquipmentBkdwnService.class, pmtForEquipmentBkdwnParams);
//
//        return stepResult;
//    }
////
//    /**
//     * RtvusrprfDetailsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvusrprfDetails(PmtForEquipmentBkdwnState state, RtvusrprfDetailsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        PmtForEquipmentBkdwnParams pmtForEquipmentBkdwnParams = new PmtForEquipmentBkdwnParams();
//        BeanUtils.copyProperties(state, pmtForEquipmentBkdwnParams);
//        stepResult = StepResult.callService(PmtForEquipmentBkdwnService.class, pmtForEquipmentBkdwnParams);
//
//        return stepResult;
//    }
//


}
