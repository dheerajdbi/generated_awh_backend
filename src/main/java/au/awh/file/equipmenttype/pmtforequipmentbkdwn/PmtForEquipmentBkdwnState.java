package au.awh.file.equipmenttype.pmtforequipmentbkdwn;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.GlobalContext;
// generateStatusFieldImportStatements BEGIN 26
import au.awh.model.DeferConfirmEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.PrintEmailOrDisplayEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Equipment Type' (WSEQTYPE) and function 'Pmt for Equipment Bkdwn' (WSPLTFZDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
 @Configurable
public class PmtForEquipmentBkdwnState extends PmtForEquipmentBkdwnDTO {
	private static final long serialVersionUID = 5765333097996242537L;


	@Autowired
	private GlobalContext globalCtx;

	// Local fields
	private FirstPassEnum lclFirstPass = null;
	private String lclDateFromTxt = "";
	private String lclDateToTxt = "";
	private String lclTextField160Chars = "";
	private long lclDaysDifference = 0L;
	private PrintEmailOrDisplayEnum lclPrintEmailOrDisplay = null;

	// System fields
	private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum._STA_NORMAL;
	private DeferConfirmEnum _sysDeferConfirm = null;
	private ReReadSubfileRecordEnum _sysReReadSubfileRecord = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public PmtForEquipmentBkdwnState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	public void setWfLdaClientAccountId(String ldaClientAccountId) {
		globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
	}

	public String getWfLdaClientAccountId() {
		return globalCtx.getString("ldaClientAccountId");
	}

	public void setWfLdaClipCode(String ldaClipCode) {
		globalCtx.setString("ldaClipCode", ldaClipCode);
	}

	public String getWfLdaClipCode() {
		return globalCtx.getString("ldaClipCode");
	}

	public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
	}

	public String getWfLdaDefaultCentreCode() {
		return globalCtx.getString("ldaDefaultCentreCode");
	}

	public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
		globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
	}

	public String getWfLdaDefaultStateCode() {
		return globalCtx.getString("ldaDefaultStateCode");
	}

	public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
	}

	public String getWfLdaDftSaleNbrId() {
		return globalCtx.getString("ldaDftSaleNbrId");
	}

	public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
	}

	public String getWfLdaDftSaleNbrSlgCtr() {
		return globalCtx.getString("ldaDftSaleNbrSlgCtr");
	}

	public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
	}

	public String getWfLdaDftSaleNbrStgCtr() {
		return globalCtx.getString("ldaDftSaleNbrStgCtr");
	}

	public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
	}

	public String getWfLdaDftSaleSeasonCent() {
		return globalCtx.getString("ldaDftSaleSeasonCent");
	}

	public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
	}

	public String getWfLdaDftSaleSeasonYear() {
		return globalCtx.getString("ldaDftSaleSeasonYear");
	}

	public void setWfLdaFolioNumber(String ldaFolioNumber) {
		globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
	}

	public String getWfLdaFolioNumber() {
		return globalCtx.getString("ldaFolioNumber");
	}

	public void setWfLdaLotNumber(String ldaLotNumber) {
		globalCtx.setString("ldaLotNumber", ldaLotNumber);
	}

	public String getWfLdaLotNumber() {
		return globalCtx.getString("ldaLotNumber");
	}

	public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
	}

	public String getWfLdaProcSaleNbrId() {
		return globalCtx.getString("ldaProcSaleNbrId");
	}

	public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
	}

	public String getWfLdaProcSaleNbrSlgCtr() {
		return globalCtx.getString("ldaProcSaleNbrSlgCtr");
	}

	public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
	}

	public String getWfLdaProcSaleNbrStgCtr() {
		return globalCtx.getString("ldaProcSaleNbrStgCtr");
	}

	public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
	}

	public String getWfLdaProcSaleSeasonCent() {
		return globalCtx.getString("ldaProcSaleSeasonCent");
	}

	public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
	}

	public String getWfLdaProcSaleSeasonYear() {
		return globalCtx.getString("ldaProcSaleSeasonYear");
	}

	public void setWfLdaWoolNumber(String ldaWoolNumber) {
		globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
	}

	public String getWfLdaWoolNumber() {
		return globalCtx.getString("ldaWoolNumber");
	}

	public void setLclDateFromTxt(String dateFromTxt) {
    	this.lclDateFromTxt = dateFromTxt;
    }

    public String getLclDateFromTxt() {
    	return lclDateFromTxt;
    }
	public void setLclDateToTxt(String dateToTxt) {
    	this.lclDateToTxt = dateToTxt;
    }

    public String getLclDateToTxt() {
    	return lclDateToTxt;
    }
	public void setLclDaysDifference(long daysDifference) {
    	this.lclDaysDifference = daysDifference;
    }

    public long getLclDaysDifference() {
    	return lclDaysDifference;
    }
	public void setLclFirstPass(FirstPassEnum firstPass) {
    	this.lclFirstPass = firstPass;
    }

    public FirstPassEnum getLclFirstPass() {
    	return lclFirstPass;
    }
	public void setLclPrintEmailOrDisplay(PrintEmailOrDisplayEnum printEmailOrDisplay) {
    	this.lclPrintEmailOrDisplay = printEmailOrDisplay;
    }

    public PrintEmailOrDisplayEnum getLclPrintEmailOrDisplay() {
    	return lclPrintEmailOrDisplay;
    }
	public void setLclTextField160Chars(String textField160Chars) {
    	this.lclTextField160Chars = textField160Chars;
    }

    public String getLclTextField160Chars() {
    	return lclTextField160Chars;
    }

	public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
    	_sysDeferConfirm = deferConfirm;
    }

    public DeferConfirmEnum get_SysDeferConfirm() {
    	return _sysDeferConfirm;
    }

	public void set_SysReReadSubfileRecord(ReReadSubfileRecordEnum reReadSubfileRecord) {
    	_sysReReadSubfileRecord = reReadSubfileRecord;
    }

    public ReReadSubfileRecordEnum get_SysReReadSubfileRecord() {
    	return _sysReReadSubfileRecord;
    }

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
    	_sysReturnCode = returnCode;
    }

    public ReturnCodeEnum get_SysReturnCode() {
    	return _sysReturnCode;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}