package au.awh.file.equipmenttype.pmtforequipmentbkdwn;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.equipmenttype.EquipmentType;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.SelectedEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;


/**
 * Gdo for file 'Equipment Type' (WSEQTYPE) and function 'Pmt for Equipment Bkdwn' (WSPLTFZDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class PmtForEquipmentBkdwnGDO implements Serializable {
	private static final long serialVersionUID = -1880627138203334559L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String equipmentTypeCode = "";
	private String equipmentTypeDesc = "";
	private SelectedEnum selected = null;
	private EquipReadingUnitEnum equipReadingUnit = null;

	public PmtForEquipmentBkdwnGDO() {

	}

   	public PmtForEquipmentBkdwnGDO(String equipmentTypeCode, String equipmentTypeDesc, EquipReadingUnitEnum equipReadingUnit) {
		this.equipmentTypeCode = equipmentTypeCode;
		this.equipmentTypeDesc = equipmentTypeDesc;
		this.equipReadingUnit = equipReadingUnit;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
    	this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
    	this.equipmentTypeDesc = equipmentTypeDesc;
    }

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}

	public void setSelected(SelectedEnum selected) {
    	this.selected = selected;
    }

	public SelectedEnum getSelected() {
		return selected;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
    	this.equipReadingUnit = equipReadingUnit;
    }

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}