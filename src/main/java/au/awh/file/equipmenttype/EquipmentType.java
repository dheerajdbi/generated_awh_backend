package au.awh.file.equipmenttype;

import au.awh.model.EquipReadingUnitEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSEQTYPE")
public class EquipmentType implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private EquipmentTypeId id = new EquipmentTypeId();

	@Column(name = "ETERUNIT")
	private EquipReadingUnitEnum equipReadingUnit = null;
	
	@Column(name = "ETETDESC")
	private String equipmentTypeDesc = "";

	public EquipmentTypeId getId() {
		return id;
	}

	public String getEquipmentTypeCode() {
		return id.getEquipmentTypeCode();
	}

	public String getEquipmentTypeDesc() {
		return equipmentTypeDesc;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	

	

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.id.setEquipmentTypeCode(equipmentTypeCode);
	}

	public void setEquipmentTypeDesc(String equipmentTypeDesc) {
		this.equipmentTypeDesc = equipmentTypeDesc;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
