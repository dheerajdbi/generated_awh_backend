package au.awh.file.plantequipment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Plant Equipment (WSEQP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface PlantEquipmentRepository extends PlantEquipmentRepositoryCustom, JpaRepository<PlantEquipment, PlantEquipmentId> {

	//List<PlantEquipment> findAllByGreaterThanIdPlantEquipmentCode(String plantEquipmentCode);

	List<PlantEquipment> findAllByIdAwhRegionCodeAndCentreCodeKey(String awhRegionCode, String centreCodeKey);

	List<PlantEquipment> findAllByIdAwhRegionCodeAndEquipmentBarcode(String awhRegionCode, String equipmentBarcode);

	List<PlantEquipment> findAllByIdPlantEquipmentCode(String plantEquipmentCode);
}
