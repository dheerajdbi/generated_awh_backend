package au.awh.file.plantequipment.updereadingtotals;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: UpdeReadingTotals ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdeReadingTotalsResult implements Serializable
{
	private static final long serialVersionUID = 5864591185572151427L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
