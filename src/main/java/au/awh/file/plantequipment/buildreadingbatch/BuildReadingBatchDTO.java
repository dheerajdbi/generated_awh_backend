package au.awh.file.plantequipment.buildreadingbatch;

import java.time.LocalDate;
import java.math.BigDecimal;



import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.GlobalContext;

import au.awh.file.plantequipment.PlantEquipment;
// generateStatusFieldImportStatements BEGIN 42
import au.awh.model.DeferConfirmEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.StateCodeKeyEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Plant Equipment' (WSEQP) and function 'Build Reading Batch' (WSPLTWPPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Configurable
public class BuildReadingBatchDTO extends BaseDTO {
	private static final long serialVersionUID = 7165427858511380961L;

	@Autowired
	private GlobalContext globalCtx;

// DEBUG fields
	private LocalDate readingRequiredDate = null; // 56555
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private String plantEquipmentCode = ""; // 56471
	private String equipmentLeasor = ""; // 56525
	private EquipmentStatusEnum equipmentStatus = null; // 56583
	private String equipmentSerialNumber = ""; // 60161
	private LocalDate leaseComencmentDate = null; // 60165
	private LocalDate leaseExpireDate = null; // 60166
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO; // 60169
	private long leaseMaxTotalUnits = 0L; // 60170
	private ReadingFrequencyEnum readingFrequency = null; // 60171
	private String visualEquipmentCode = ""; // 60172
	private String centreCodeKey = ""; // 1140
	private String centreNameDrv = ""; // 34917
	private YesOrNoEnum checkForHolidays = null; // 60195
	private String outputQueue = ""; // 34671
	private String copies = ""; // 9040
	private FormsEnum forms = null; // 9043
	private HoldClFormatyesnoEnum holdClFormatyesno = null; // 9078
	private SaveClFormatyesnoEnum saveClFormatyesno = null; // 9079
	private String outputQueueLibrary = ""; // 36415
// DEBUG detail fields
// DEBUG param
// DEBUG local fields
	private String lclCentreCodeKey = ""; // 1140
	private String lclLdaCurrentCentreCode = ""; // 4391
	private String lclOverflowLineNumberText = ""; // 43094
// DEBUG program fields
	private DeferConfirmEnum deferConfirm = null; // 237
// DEBUG Constructor

	public BuildReadingBatchDTO() {

	}

	public BuildReadingBatchDTO(PlantEquipment plantEquipment) {
		BeanUtils.copyProperties(plantEquipment, this);
	}

	public BuildReadingBatchDTO(LocalDate readingRequiredDate, String awhRegionCode, String plantEquipmentCode, String equipmentLeasor, EquipmentStatusEnum equipmentStatus, String equipmentSerialNumber, LocalDate leaseComencmentDate, LocalDate leaseExpireDate, BigDecimal leaseMonthlyRateS, long leaseMaxTotalUnits, ReadingFrequencyEnum readingFrequency, String visualEquipmentCode, String centreCodeKey, String outputQueue, String copies, FormsEnum forms, HoldClFormatyesnoEnum holdClFormatyesno, SaveClFormatyesnoEnum saveClFormatyesno) {
		this.readingRequiredDate = readingRequiredDate;
		this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentLeasor = equipmentLeasor;
		this.equipmentStatus = equipmentStatus;
		this.equipmentSerialNumber = equipmentSerialNumber;
		this.leaseComencmentDate = leaseComencmentDate;
		this.leaseExpireDate = leaseExpireDate;
		this.leaseMonthlyRateS = leaseMonthlyRateS;
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
		this.readingFrequency = readingFrequency;
		this.visualEquipmentCode = visualEquipmentCode;
		this.centreCodeKey = centreCodeKey;
		this.outputQueue = outputQueue;
		this.copies = copies;
		this.forms = forms;
		this.holdClFormatyesno = holdClFormatyesno;
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
	}

	public String getAwhRegionNameDrv() {
		return awhRegionNameDrv;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}

	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}

	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}

	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}

	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}

	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}

	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}

	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}

	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}

	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}

	public void setLeaseMaxTotalUnits(long leaseMaxTotalUnits) {
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
	}

	public long getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}

	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}

	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}

	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}

	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void setCheckForHolidays(YesOrNoEnum checkForHolidays) {
		this.checkForHolidays = checkForHolidays;
	}

	public YesOrNoEnum getCheckForHolidays() {
		return checkForHolidays;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public String getCopies() {
		return copies;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public void setWfLdaClientAccountId(String ldaClientAccountId) {
		globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
	}

	public String getWfLdaClientAccountId() {
		return globalCtx.getString("ldaClientAccountId");
	}

	public void setWfLdaClipCode(String ldaClipCode) {
		globalCtx.setString("ldaClipCode", ldaClipCode);
	}

	public String getWfLdaClipCode() {
		return globalCtx.getString("ldaClipCode");
	}

	public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
	}

	public String getWfLdaDefaultCentreCode() {
		return globalCtx.getString("ldaDefaultCentreCode");
	}

	public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
		globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
	}

	public String getWfLdaDefaultStateCode() {
		return globalCtx.getString("ldaDefaultStateCode");
	}

	public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
	}

	public String getWfLdaDftSaleNbrId() {
		return globalCtx.getString("ldaDftSaleNbrId");
	}

	public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
	}

	public String getWfLdaDftSaleNbrSlgCtr() {
		return globalCtx.getString("ldaDftSaleNbrSlgCtr");
	}

	public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
	}

	public String getWfLdaDftSaleNbrStgCtr() {
		return globalCtx.getString("ldaDftSaleNbrStgCtr");
	}

	public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
	}

	public String getWfLdaDftSaleSeasonCent() {
		return globalCtx.getString("ldaDftSaleSeasonCent");
	}

	public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
	}

	public String getWfLdaDftSaleSeasonYear() {
		return globalCtx.getString("ldaDftSaleSeasonYear");
	}

	public void setWfLdaFolioNumber(String ldaFolioNumber) {
		globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
	}

	public String getWfLdaFolioNumber() {
		return globalCtx.getString("ldaFolioNumber");
	}

	public void setWfLdaLotNumber(String ldaLotNumber) {
		globalCtx.setString("ldaLotNumber", ldaLotNumber);
	}

	public String getWfLdaLotNumber() {
		return globalCtx.getString("ldaLotNumber");
	}

	public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
	}

	public String getWfLdaProcSaleNbrId() {
		return globalCtx.getString("ldaProcSaleNbrId");
	}

	public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
	}

	public String getWfLdaProcSaleNbrSlgCtr() {
		return globalCtx.getString("ldaProcSaleNbrSlgCtr");
	}

	public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
	}

	public String getWfLdaProcSaleNbrStgCtr() {
		return globalCtx.getString("ldaProcSaleNbrStgCtr");
	}

	public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
	}

	public String getWfLdaProcSaleSeasonCent() {
		return globalCtx.getString("ldaProcSaleSeasonCent");
	}

	public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
	}

	public String getWfLdaProcSaleSeasonYear() {
		return globalCtx.getString("ldaProcSaleSeasonYear");
	}

	public void setWfLdaWoolNumber(String ldaWoolNumber) {
		globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
	}

	public String getWfLdaWoolNumber() {
		return globalCtx.getString("ldaWoolNumber");
	}

	public void setLclCentreCodeKey(String centreCodeKey) {
		this.lclCentreCodeKey = centreCodeKey;
	}

	public String getLclCentreCodeKey() {
		return lclCentreCodeKey;
	}

	public void setLclLdaCurrentCentreCode(String ldaCurrentCentreCode) {
		this.lclLdaCurrentCentreCode = ldaCurrentCentreCode;
	}

	public String getLclLdaCurrentCentreCode() {
		return lclLdaCurrentCentreCode;
	}

	public void setLclOverflowLineNumberText(String overflowLineNumberText) {
		this.lclOverflowLineNumberText = overflowLineNumberText;
	}

	public String getLclOverflowLineNumberText() {
		return lclOverflowLineNumberText;
	}

	public void setDeferConfirm(DeferConfirmEnum deferConfirm) {
		this.deferConfirm = deferConfirm;
	}

	public DeferConfirmEnum getDeferConfirm() {
		return deferConfirm;
	}
    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setDtoFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(plantEquipment, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setEntityFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(this, plantEquipment);
    }
}
