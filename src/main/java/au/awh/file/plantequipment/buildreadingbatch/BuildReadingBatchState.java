package au.awh.file.plantequipment.buildreadingbatch;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import au.awh.model.GlobalContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 21
import au.awh.model.DeferConfirmEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Build Reading Batch' (WSPLTWPPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Configurable
public class BuildReadingBatchState extends BuildReadingBatchDTO {
    private static final long serialVersionUID = -7820968898539142695L;

    @Autowired
    private GlobalContext globalCtx;

    // Local fields
    private String lclCentreCodeKey = "";
    private String lclLdaCurrentCentreCode = "";
    private String lclOverflowLineNumberText = "";

    // System fields
    private DeferConfirmEnum _sysDeferConfirm = null;
    private boolean _sysErrorFound = false;

    public BuildReadingBatchState() {
    }

    public BuildReadingBatchState(PlantEquipment plantEquipment) {
        setDtoFields(plantEquipment);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setWfLdaClientAccountId(String ldaClientAccountId) {
        globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
    }

    public String getWfLdaClientAccountId() {
        return globalCtx.getString("ldaClientAccountId");
    }

    public void setWfLdaClipCode(String ldaClipCode) {
        globalCtx.setString("ldaClipCode", ldaClipCode);
    }

    public String getWfLdaClipCode() {
        return globalCtx.getString("ldaClipCode");
    }

    public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
        globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
    }

    public String getWfLdaDefaultCentreCode() {
        return globalCtx.getString("ldaDefaultCentreCode");
    }

    public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
        globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
    }

    public String getWfLdaDefaultStateCode() {
        return globalCtx.getString("ldaDefaultStateCode");
    }

    public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
        globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
    }

    public String getWfLdaDftSaleNbrId() {
        return globalCtx.getString("ldaDftSaleNbrId");
    }

    public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
        globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
    }

    public String getWfLdaDftSaleNbrSlgCtr() {
        return globalCtx.getString("ldaDftSaleNbrSlgCtr");
    }

    public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
        globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
    }

    public String getWfLdaDftSaleNbrStgCtr() {
        return globalCtx.getString("ldaDftSaleNbrStgCtr");
    }

    public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
        globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
    }

    public String getWfLdaDftSaleSeasonCent() {
        return globalCtx.getString("ldaDftSaleSeasonCent");
    }

    public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
        globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
    }

    public String getWfLdaDftSaleSeasonYear() {
        return globalCtx.getString("ldaDftSaleSeasonYear");
    }

    public void setWfLdaFolioNumber(String ldaFolioNumber) {
        globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
    }

    public String getWfLdaFolioNumber() {
        return globalCtx.getString("ldaFolioNumber");
    }

    public void setWfLdaLotNumber(String ldaLotNumber) {
        globalCtx.setString("ldaLotNumber", ldaLotNumber);
    }

    public String getWfLdaLotNumber() {
        return globalCtx.getString("ldaLotNumber");
    }

    public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
        globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
    }

    public String getWfLdaProcSaleNbrId() {
        return globalCtx.getString("ldaProcSaleNbrId");
    }

    public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
        globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
    }

    public String getWfLdaProcSaleNbrSlgCtr() {
        return globalCtx.getString("ldaProcSaleNbrSlgCtr");
    }

    public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
        globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
    }

    public String getWfLdaProcSaleNbrStgCtr() {
        return globalCtx.getString("ldaProcSaleNbrStgCtr");
    }

    public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
        globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
    }

    public String getWfLdaProcSaleSeasonCent() {
        return globalCtx.getString("ldaProcSaleSeasonCent");
    }

    public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
        globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
    }

    public String getWfLdaProcSaleSeasonYear() {
        return globalCtx.getString("ldaProcSaleSeasonYear");
    }

    public void setWfLdaWoolNumber(String ldaWoolNumber) {
        globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
    }

    public String getWfLdaWoolNumber() {
        return globalCtx.getString("ldaWoolNumber");
    }

    public void setLclCentreCodeKey(String centreCodeKey) {
        this.lclCentreCodeKey = centreCodeKey;
    }

    public String getLclCentreCodeKey() {
        return lclCentreCodeKey;
    }
    public void setLclLdaCurrentCentreCode(String ldaCurrentCentreCode) {
        this.lclLdaCurrentCentreCode = ldaCurrentCentreCode;
    }

    public String getLclLdaCurrentCentreCode() {
        return lclLdaCurrentCentreCode;
    }
    public void setLclOverflowLineNumberText(String overflowLineNumberText) {
        this.lclOverflowLineNumberText = overflowLineNumberText;
    }

    public String getLclOverflowLineNumberText() {
        return lclOverflowLineNumberText;
    }

    public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
        _sysDeferConfirm = deferConfirm;
    }

    public DeferConfirmEnum get_SysDeferConfirm() {
        return _sysDeferConfirm;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
