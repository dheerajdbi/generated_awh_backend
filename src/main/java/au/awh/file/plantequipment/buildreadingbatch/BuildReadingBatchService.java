package au.awh.file.plantequipment.buildreadingbatch;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.plantequipment.PlantEquipmentRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionService;
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingService;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersService;
// asServiceDtoImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionDTO;
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingDTO;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionParams;
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingParams;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersParams;
// asServiceResultImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionResult;
import au.awh.file.plantequipment.bldprteequipmentreading.BldprteEquipmentReadingResult;
import au.awh.file.utilitiesprintandemail.setwsprintparameters.SetWsPrintParametersResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END

import au.awh.file.awhregion.selectawhregion.SelectAwhRegionService;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionParams;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionResult;

    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 29
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.FormsEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReportEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.StateCodeKeyEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Build Reading Batch' (WSPLTWPPVK) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service
public class BuildReadingBatchService extends AbstractService<BuildReadingBatchService, BuildReadingBatchState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

    
    public static final String SCREEN_PROMPT = "buildReadingBatch";
    public static final String SCREEN_CONFIRM_PROMPT = "BuildReadingBatch.key2";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",BuildReadingBatchParams.class, this::executeService);
    private final Step response = define("response",BuildReadingBatchDTO.class, this::processResponse);
//  private final Step displayConfirmPrompt = define("displayConfirmPrompt",BuildReadingBatchDTO.class, this::processDisplayConfirmPrompt);

	//private final Step serviceGeteCentresRegion = define("serviceGeteCentresRegion",GeteCentresRegionResult.class, this::processServiceGeteCentresRegion);
	//private final Step serviceSetWsPrintParameters = define("serviceSetWsPrintParameters",SetWsPrintParametersResult.class, this::processServiceSetWsPrintParameters);
	//private final Step serviceBldprteEquipmentReading = define("serviceBldprteEquipmentReading",BldprteEquipmentReadingResult.class, this::processServiceBldprteEquipmentReading);
	

	private final Step promptSelectAwhRegion = define("promptSelectAwhRegion",SelectAwhRegionResult.class, this::processPromptSelectAwhRegion);
	
    	
@Autowired
private BldprteEquipmentReadingService bldprteEquipmentReadingService;
	
@Autowired
private GeteCentresRegionService geteCentresRegionService;
	
@Autowired
private SetWsPrintParametersService setWsPrintParametersService;
	
    @Autowired
    public BuildReadingBatchService()
    {
        super(BuildReadingBatchService.class, BuildReadingBatchState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * BuildReadingBatch service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(BuildReadingBatchState state, BuildReadingBatchParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * BuildReadingBatch service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(BuildReadingBatchState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(BuildReadingBatchState state)
    {
        StepResult stepResult = NO_ACTION;

        BuildReadingBatchDTO dto = new BuildReadingBatchDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(BuildReadingBatchState state, BuildReadingBatchDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			switch (state.get_SysEntrySelected()) {
			    case "awhRegionCode":
			        SelectAwhRegionParams selectAwhRegionParams = new SelectAwhRegionParams();
			        BeanUtils.copyProperties(state, selectAwhRegionParams);
			        stepResult = StepResult.callService(SelectAwhRegionService.class, selectAwhRegionParams).thenCall(promptSelectAwhRegion);
			        break;
			    default:
			        System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
			        stepResult = conductScreenConversation(state);
			        break;
			}
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
//    private StepResult processDisplayConfirmPrompt(BuildReadingBatchState state, BuildReadingBatchDTO dto) {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        stepResult = conductScreenConversation(state);
//
//        return stepResult;
//    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void validateScreenInput(BuildReadingBatchState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(BuildReadingBatchState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(BuildReadingBatchState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(BuildReadingBatchState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        BuildReadingBatchResult params = new BuildReadingBatchResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            GeteCentresRegionResult geteCentresRegionResult = null;
			GeteCentresRegionParams geteCentresRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1000386 ACT GetE Centres Region - AWH Region/Centre  *
			geteCentresRegionParams = new GeteCentresRegionParams();
			geteCentresRegionResult = new GeteCentresRegionResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteCentresRegionParams);
			geteCentresRegionParams.setCentreCodeKey(dto.getLclLdaCurrentCentreCode());
			geteCentresRegionParams.setErrorProcessing("2");
			geteCentresRegionParams.setGetRecordOption("");
			stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
			geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteCentresRegionResult.get_SysReturnCode());
			dto.setAwhRegionCode(geteCentresRegionResult.getAwhRegionCode());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// DEBUG genFunctionCall BEGIN 1000337 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000337
			// DEBUG X2EActionDiagramElement 1000337 ACT     AWH region Name Drv      *FIELD                                             DTLA
			// DEBUG X2EActionDiagramElement 1000338 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000339 PAR DTL 
			// DEBUG genFunctionCall END
			if (!dto.getCentreCodeKey().equals("")) {
				// DTL.Centre Code           KEY is Entered
				// DEBUG genFunctionCall BEGIN 1000345 ACT Centre Name Drv          *FIELD                                             DTLC
				// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000345
				// DEBUG X2EActionDiagramElement 1000345 ACT     Centre Name Drv          *FIELD                                             DTLC
				// DEBUG X2EActionDiagramElement 1000346 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000347 PAR DTL 
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// DEBUG genFunctionCall BEGIN 1000398 ACT DTL.Centre Name Drv = CND.Optional
			dto.setCentreNameDrv("*Optional");
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000451 ACT DTL.Hold (CL Format *YES/*NO) = CND.Not entered
			dto.setHoldClFormatyesno(HoldClFormatyesnoEnum._NOT_ENTERED);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000459 ACT DTL.Check for Holidays = CND.Yes
			dto.setCheckForHolidays(YesOrNoEnum._YES);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            SetWsPrintParametersParams setWsPrintParametersParams = null;
			SetWsPrintParametersResult setWsPrintParametersResult = null;
			if ((
			// DEBUG ComparatorJavaGenerator BEGIN
			!dto.getCentreCodeKey().equals(dto.getLclCentreCodeKey())
			// DEBUG ComparatorJavaGenerator END
			) && (!dto.getCentreCodeKey().equals(""))) {
				// DEBUG genFunctionCall BEGIN 1000408 ACT LCL.Centre Code           KEY = DTL.Centre Code           KEY
				dto.setLclCentreCodeKey(dto.getCentreCodeKey());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000414 ACT Set WS print parameters - Utilities Print & Email  *
				setWsPrintParametersParams = new SetWsPrintParametersParams();
				setWsPrintParametersResult = new SetWsPrintParametersResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, setWsPrintParametersParams);
				setWsPrintParametersParams.setBrokerIdKey("");
				setWsPrintParametersParams.setStateCodeKey("");
				setWsPrintParametersParams.setCentreCodeKey(dto.getCentreCodeKey());
				setWsPrintParametersParams.setReport("EQPREAD");
				stepResult = setWsPrintParametersService.execute(setWsPrintParametersParams);
				setWsPrintParametersResult = (SetWsPrintParametersResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(setWsPrintParametersResult.get_SysReturnCode());
				dto.setOutputQueue(setWsPrintParametersResult.getOutputQueue());
				dto.setOutputQueueLibrary(setWsPrintParametersResult.getOutputQueueLibrary());
				dto.setCopies(setWsPrintParametersResult.getCopies());
				dto.setForms(setWsPrintParametersResult.getForms());
				dto.setHoldClFormatyesno(setWsPrintParametersResult.getHoldClFormatyesno());
				dto.setSaveClFormatyesno(setWsPrintParametersResult.getSaveClFormatyesno());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000427 ACT PGM.*Defer confirm = CND.Defer confirm
				dto.set_SysDeferConfirm(DeferConfirmEnum._DEFER_CONFIRM);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 29 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            BldprteEquipmentReadingResult bldprteEquipmentReadingResult = null;
			BldprteEquipmentReadingParams bldprteEquipmentReadingParams = null;
			// DEBUG genFunctionCall BEGIN 1000433 ACT BldPrtE Equipment Reading - Plant Equipment  *
			bldprteEquipmentReadingParams = new BldprteEquipmentReadingParams();
			bldprteEquipmentReadingResult = new BldprteEquipmentReadingResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, bldprteEquipmentReadingParams);
			bldprteEquipmentReadingParams.setAwhRegionCode(dto.getAwhRegionCode());
			bldprteEquipmentReadingParams.setCentreCodeKey(dto.getCentreCodeKey());
			bldprteEquipmentReadingParams.setOutputQueue(dto.getOutputQueue());
			bldprteEquipmentReadingParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
			bldprteEquipmentReadingParams.setCopies(dto.getCopies());
			bldprteEquipmentReadingParams.setForms(dto.getForms());
			bldprteEquipmentReadingParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
			bldprteEquipmentReadingParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
			bldprteEquipmentReadingParams.setOverflowLineNumberText(dto.getLclOverflowLineNumberText());
			bldprteEquipmentReadingParams.setErrorProcessing("2");
			bldprteEquipmentReadingParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			bldprteEquipmentReadingParams.setCheckForHolidays(dto.getCheckForHolidays());
			stepResult = bldprteEquipmentReadingService.execute(bldprteEquipmentReadingParams);
			if (stepResult != NO_ACTION) {
				bldprteEquipmentReadingResult = (BldprteEquipmentReadingResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(bldprteEquipmentReadingResult.get_SysReturnCode());
				dto.setOutputQueue(bldprteEquipmentReadingResult.getOutputQueue());
				dto.setOutputQueueLibrary(bldprteEquipmentReadingResult.getOutputQueueLibrary());
				dto.setCopies(bldprteEquipmentReadingResult.getCopies());
				dto.setForms(bldprteEquipmentReadingResult.getForms());
				dto.setHoldClFormatyesno(bldprteEquipmentReadingResult.getHoldClFormatyesno());
				dto.setSaveClFormatyesno(bldprteEquipmentReadingResult.getSaveClFormatyesno());
				dto.setLclOverflowLineNumberText(bldprteEquipmentReadingResult.getOverflowLineNumberText());
			}
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(BuildReadingBatchState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1021 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GeteCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentresRegion(BuildReadingBatchState state, GeteCentresRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        BuildReadingBatchParams buildReadingBatchParams = new BuildReadingBatchParams();
//        BeanUtils.copyProperties(state, buildReadingBatchParams);
//        stepResult = StepResult.callService(BuildReadingBatchService.class, buildReadingBatchParams);
//
//        return stepResult;
//    }
////
//    /**
//     * SetWsPrintParametersService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceSetWsPrintParameters(BuildReadingBatchState state, SetWsPrintParametersParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        BuildReadingBatchParams buildReadingBatchParams = new BuildReadingBatchParams();
//        BeanUtils.copyProperties(state, buildReadingBatchParams);
//        stepResult = StepResult.callService(BuildReadingBatchService.class, buildReadingBatchParams);
//
//        return stepResult;
//    }
////
//    /**
//     * BldprteEquipmentReadingService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceBldprteEquipmentReading(BuildReadingBatchState state, BldprteEquipmentReadingParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        BuildReadingBatchParams buildReadingBatchParams = new BuildReadingBatchParams();
//        BeanUtils.copyProperties(state, buildReadingBatchParams);
//        stepResult = StepResult.callService(BuildReadingBatchService.class, buildReadingBatchParams);
//
//        return stepResult;
//    }
//

    /**
     * SelectAwhRegionService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectAwhRegion(BuildReadingBatchState state, SelectAwhRegionResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(serviceResult, state);

        stepResult = conductScreenConversation(state);

        return stepResult;
    }


}
