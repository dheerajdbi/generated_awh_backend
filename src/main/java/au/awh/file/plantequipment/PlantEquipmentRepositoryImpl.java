package au.awh.file.plantequipment;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 8
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentGDO;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkGDO;
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentDTO;
import au.awh.file.plantequipment.buildreadingbatch.BuildReadingBatchDTO;
import au.awh.file.plantequipment.renamemplantequipment.RenamemPlantEquipmentDTO;

/**
 * Custom Spring Data JPA repository implementation for model: Plant Equipment (WSEQP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class PlantEquipmentRepositoryImpl implements PlantEquipmentRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#deletePlantEquipment(PlantEquipment)
	 */
	@Override
	public void deletePlantEquipment(
		String awhRegionCode, String plantEquipmentCode) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			whereClause += " plantEquipment.id.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		String sqlString = "DELETE FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		em.createNativeQuery("SET foreign_key_checks = 0").executeUpdate();
		query.executeUpdate();
		em.createNativeQuery("SET foreign_key_checks = 1").executeUpdate();
	}

	@Override
	public RestResponsePage<WrkPlantEquipmentGDO> wrkPlantEquipment(
		
String awhRegionCode, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String equipmentLeasor, ReadingTypeEnum readingType, String plantEquipmentCode, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " (plantEquipment.id.awhRegionCode >= :awhRegionCode OR plantEquipment.id.awhRegionCode like CONCAT('%', :awhRegionCode, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.centreCodeKey like CONCAT('%', :centreCodeKey, '%')";
			isParamSet = true;
		}

		if (awhBuisnessSegment != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.awhBuisnessSegment = :awhBuisnessSegment";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.equipmentTypeCode like CONCAT('%', :equipmentTypeCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(equipmentLeasor)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.equipmentLeasor like CONCAT('%', :equipmentLeasor, '%')";
			isParamSet = true;
		}

		if (readingType != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.readingType = :readingType";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode like CONCAT('%', :plantEquipmentCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentGDO(plantEquipment.centreCodeKey, plantEquipment.awhBuisnessSegment, plantEquipment.equipmentTypeCode, plantEquipment.equipmentLeasor, plantEquipment.equipmentSerialNumber, plantEquipment.leaseMonthlyRateS, plantEquipment.leaseMaxTotalUnits, plantEquipment.id.plantEquipmentCode, plantEquipment.equipmentStatus, plantEquipment.readingFrequency, plantEquipment.equipmentDescription, plantEquipment.visualEquipmentCode, plantEquipment.equipmentBarcode, plantEquipment.equipmentComment, plantEquipment.id.awhRegionCode, plantEquipment.equipReadingUnit) from PlantEquipment plantEquipment";
		String countQueryString = "SELECT COUNT(plantEquipment.id.awhRegionCode) FROM PlantEquipment plantEquipment";
		// commented in sqlString plantEquipment.leaseComencmentDate, plantEquipment.leaseExpireDate, 
		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
//		Sort sort = pageable.getSort();
		Sort sort = null;
		String orderByClause = "";

		if (sort != null) {
			PlantEquipment plantEquipment = new PlantEquipment();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(plantEquipment.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"plantEquipment.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"plantEquipment." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.id.awhRegionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.centreCodeKey"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.awhBuisnessSegment"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.equipmentTypeCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.id.plantEquipmentCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (awhBuisnessSegment != null) {
			countQuery.setParameter("awhBuisnessSegment", awhBuisnessSegment);
			query.setParameter("awhBuisnessSegment", awhBuisnessSegment);
		}

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			countQuery.setParameter("equipmentTypeCode", equipmentTypeCode);
			query.setParameter("equipmentTypeCode", equipmentTypeCode);
		}

		if (StringUtils.isNotEmpty(equipmentLeasor)) {
			countQuery.setParameter("equipmentLeasor", equipmentLeasor);
			query.setParameter("equipmentLeasor", equipmentLeasor);
		}

		if (readingType != null) {
			countQuery.setParameter("readingType", readingType);
			query.setParameter("readingType", readingType);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<WrkPlantEquipmentGDO> content = query.getResultList();
		RestResponsePage<WrkPlantEquipmentGDO> pageGdo = new RestResponsePage<WrkPlantEquipmentGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<AddPlantEquipmentDTO> addPlantEquipment(
		String awhRegionCode
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode like CONCAT('%', :awhRegionCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentDTO(plantEquipment.id.awhRegionCode, plantEquipment.equipmentStatus, plantEquipment.equipmentSerialNumber, plantEquipment.leaseComencmentDate, plantEquipment.leaseExpireDate, plantEquipment.leaseMonthlyRateS, plantEquipment.leaseMaxTotalUnits, plantEquipment.readingFrequency, plantEquipment.visualEquipmentCode, plantEquipment.id.plantEquipmentCode) from PlantEquipment plantEquipment";
		String countQueryString = "SELECT COUNT(plantEquipment.id.awhRegionCode) FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<AddPlantEquipmentDTO> content = query.getResultList();
		RestResponsePage<AddPlantEquipmentDTO> pageGdo = new RestResponsePage<AddPlantEquipmentDTO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#chkPlantEquipment(String, String, AwhBuisnessSegmentEnum, String)
	 */
	@Override
	public PlantEquipment chkPlantEquipment(
		String plantEquipmentCode, String awhRegionCode, AwhBuisnessSegmentEnum awhBuisnessSegment, String centreCodeKey
) {
		String whereClause = "";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			whereClause += " plantEquipment.id.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (awhBuisnessSegment != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.awhBuisnessSegment = :awhBuisnessSegment";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (awhBuisnessSegment != null) {
			query.setParameter("awhBuisnessSegment", awhBuisnessSegment);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		PlantEquipment dto = (PlantEquipment)query.getSingleResult();

		return dto;
	}

	@Override
	public RestResponsePage<EquipmentReadingsWspltwmdfkGDO> equipmentReadingsWspltwmdfk(
		BigDecimal equipReadingBatchNo, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String plantEquipmentCode, Pageable pageable) {
		String whereClause = "";
		boolean isParamSet = false;

		if (equipReadingBatchNo.intValue() > 0) {
			whereClause += " equipmentReadingControl.id.equipReadingBatchNo = :equipReadingBatchNo";
			isParamSet = true;
		}

		if (awhBuisnessSegment != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.awhBuisnessSegment = :awhBuisnessSegment";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.equipmentTypeCode like CONCAT('%', :equipmentTypeCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode like CONCAT('%', :plantEquipmentCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkGDO(plantEquipment.id.awhRegionCode, plantEquipment.centreCodeKey, plantEquipment.awhBuisnessSegment, plantEquipment.equipmentTypeCode, plantEquipment.id.plantEquipmentCode, plantEquipment.equipmentDescription, plantEquipment.equipmentBarcode, plantEquipment.equipmentComment, plantEquipment.equipReadingUnit, plantEquipment.equipmentStatus, plantEquipment.equipmentSerialNumber, plantEquipment.leaseComencmentDate, plantEquipment.leaseExpireDate, plantEquipment.leaseMonthlyRateS, plantEquipment.leaseMaxTotalUnits, plantEquipment.readingFrequency, plantEquipment.visualEquipmentCode, plantEquipment.equipmentLeasor) from PlantEquipment plantEquipment, EquipmentReadingControl equipmentReadingControl  WHERE equipmentReadingControl.awhRegionCode = plantEquipment.id.awhRegionCode";
		String countQueryString = "SELECT COUNT(plantEquipment.id.awhRegionCode) FROM PlantEquipment plantEquipment  , EquipmentReadingControl equipmentReadingControl  WHERE equipmentReadingControl.awhRegionCode = plantEquipment.id.awhRegionCode";

		if (isParamSet) {
			sqlString += " AND "+whereClause;
			countQueryString += " AND " +whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			PlantEquipment plantEquipment = new PlantEquipment();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(plantEquipment.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"plantEquipment.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"plantEquipment." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.id.awhRegionCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.centreCodeKey"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.awhBuisnessSegment"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.equipmentTypeCode"));
			sortOrders.add(new Order(Sort.Direction.ASC,
				"plantEquipment.id.plantEquipmentCode"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (equipReadingBatchNo.intValue() > 0) {
			countQuery.setParameter("equipReadingBatchNo", equipReadingBatchNo);
			query.setParameter("equipReadingBatchNo", equipReadingBatchNo);
		}

		if (awhBuisnessSegment != null) {
			countQuery.setParameter("awhBuisnessSegment", awhBuisnessSegment);
			query.setParameter("awhBuisnessSegment", awhBuisnessSegment);
		}

		if (StringUtils.isNotEmpty(equipmentTypeCode)) {
			countQuery.setParameter("equipmentTypeCode", equipmentTypeCode);
			query.setParameter("equipmentTypeCode", equipmentTypeCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			countQuery.setParameter("plantEquipmentCode", plantEquipmentCode);
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<EquipmentReadingsWspltwmdfkGDO> content = query.getResultList();
		RestResponsePage<EquipmentReadingsWspltwmdfkGDO> pageGdo = new RestResponsePage<EquipmentReadingsWspltwmdfkGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	@Override
	public RestResponsePage<BuildReadingBatchDTO> buildReadingBatch(
		
Pageable pageable) {
		String sqlString = "SELECT new au.awh.file.plantequipment.buildreadingbatch.BuildReadingBatchDTO(plantEquipment.id.awhRegionCode, plantEquipment.equipmentLeasor, plantEquipment.equipmentStatus, plantEquipment.equipmentSerialNumber, plantEquipment.leaseComencmentDate, plantEquipment.leaseExpireDate, plantEquipment.leaseMonthlyRateS, plantEquipment.leaseMaxTotalUnits, plantEquipment.readingFrequency, plantEquipment.visualEquipmentCode, plantEquipment.centreCodeKey) from PlantEquipment plantEquipment";
		String countQueryString = "SELECT COUNT(plantEquipment.id.awhRegionCode) FROM PlantEquipment plantEquipment";

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<BuildReadingBatchDTO> content = query.getResultList();
		RestResponsePage<BuildReadingBatchDTO> pageGdo = new RestResponsePage<BuildReadingBatchDTO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#getPlantEquipment(String, String)
	 */
	@Override
	public PlantEquipment getPlantEquipment(
		String awhRegionCode, String plantEquipmentCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		PlantEquipment dto = (PlantEquipment)query.getSingleResult();

		return dto;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#chkReadingsBatchSts(String, String, LocalDate)
	 */
	@Override
	public RestResponsePage<PlantEquipment> chkReadingsBatchSts(
		String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate
, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.centreCodeKey = :centreCodeKey";
			isParamSet = true;
		}

		if (readingRequiredDate != null) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.readingRequiredDate = :readingRequiredDate";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";
		String countQueryString = "SELECT COUNT(plantEquipment.id.awhRegionCode) FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			countQuery.setParameter("awhRegionCode", awhRegionCode);
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(centreCodeKey)) {
			countQuery.setParameter("centreCodeKey", centreCodeKey);
			query.setParameter("centreCodeKey", centreCodeKey);
		}

		if (readingRequiredDate != null) {
			countQuery.setParameter("readingRequiredDate", readingRequiredDate);
			query.setParameter("readingRequiredDate", readingRequiredDate);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<PlantEquipment> content = query.getResultList();
		RestResponsePage<PlantEquipment> pageGdo = new RestResponsePage<PlantEquipment>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#getByRegionBarcode(String, String)
	 */
	@Override
	public PlantEquipment getByRegionBarcode(
		String awhRegionCode, String equipmentBarcode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(equipmentBarcode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.equipmentBarcode = :equipmentBarcode";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(equipmentBarcode)) {
			query.setParameter("equipmentBarcode", equipmentBarcode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		PlantEquipment dto = (PlantEquipment)query.getSingleResult();

		return dto;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#updEquipmentTotals(String, String)
	 */
	@Override
	public PlantEquipment updEquipmentTotals(
		String awhRegionCode, String plantEquipmentCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		PlantEquipment dto = (PlantEquipment)query.getSingleResult();

		return dto;
	}

	@Override
	public RenamemPlantEquipmentDTO renamemPlantEquipment(
		String awhRegionCode, String plantEquipmentCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode like CONCAT('%', :awhRegionCode, '%')";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode like CONCAT('%', :plantEquipmentCode, '%')";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.plantequipment.renamemplantequipment.RenamemPlantEquipmentDTO(plantEquipment.id.awhRegionCode, plantEquipment.equipmentStatus, plantEquipment.equipmentSerialNumber, plantEquipment.leaseComencmentDate, plantEquipment.leaseExpireDate, plantEquipment.leaseMonthlyRateS, plantEquipment.leaseMaxTotalUnits, plantEquipment.readingFrequency, plantEquipment.visualEquipmentCode, plantEquipment.id.plantEquipmentCode) from PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		RenamemPlantEquipmentDTO dto = (RenamemPlantEquipmentDTO)query.getSingleResult();

		return dto;
	}

	/**
	 * @see au.awh.file.plantequipment.PlantEquipmentService#chgkeyPlantEquipment(String, String, String)
	 */
	@Override
	public PlantEquipment chgkeyPlantEquipment(
		String awhRegionCode, String plantEquipmentCode, String newPlantEquipmentCode
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			whereClause += " plantEquipment.id.awhRegionCode = :awhRegionCode";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " plantEquipment.id.plantEquipmentCode = :plantEquipmentCode";
			isParamSet = true;
		}

		String sqlString = "SELECT plantEquipment FROM PlantEquipment plantEquipment";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(awhRegionCode)) {
			query.setParameter("awhRegionCode", awhRegionCode);
		}

		if (StringUtils.isNotEmpty(plantEquipmentCode)) {
			query.setParameter("plantEquipmentCode", plantEquipmentCode);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		PlantEquipment dto = (PlantEquipment)query.getSingleResult();

		return dto;
	}

}
