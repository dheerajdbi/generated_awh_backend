package au.awh.file.plantequipment.dlteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: DltePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DltePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -4041156932113710843L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
