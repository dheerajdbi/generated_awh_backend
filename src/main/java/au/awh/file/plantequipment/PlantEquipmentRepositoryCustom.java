package au.awh.file.plantequipment;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 8
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

import au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentGDO;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkGDO;
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentDTO;
import au.awh.file.plantequipment.buildreadingbatch.BuildReadingBatchDTO;
import au.awh.file.plantequipment.renamemplantequipment.RenamemPlantEquipmentDTO;

/**
 * Custom Spring Data JPA repository interface for model: Plant Equipment (WSEQP).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface PlantEquipmentRepositoryCustom {

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 */
	void deletePlantEquipment(String awhRegionCode, String plantEquipmentCode);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param centreCodeKey Centre Code Key
	 * @param awhBuisnessSegment AWH Buisness Segment
	 * @param equipmentTypeCode Equipment Type Code
	 * @param equipmentLeasor Equipment Leasor
	 * @param readingType Reading Type
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of WrkPlantEquipmentGDO
	 */
	RestResponsePage<WrkPlantEquipmentGDO> wrkPlantEquipment(
	String awhRegionCode, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String equipmentLeasor, ReadingTypeEnum readingType, String plantEquipmentCode, Pageable pageable);

	/**
	 * 
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param awhRegionCode AWH Region Code
	 * @param awhBuisnessSegment AWH Buisness Segment
	 * @param centreCodeKey Centre Code Key
	 * @return PlantEquipment
	 */
	PlantEquipment chkPlantEquipment(String plantEquipmentCode, String awhRegionCode, AwhBuisnessSegmentEnum awhBuisnessSegment, String centreCodeKey
	);

	/**
	 * 
	 * @param equipReadingBatchNo Equip Reading Batch No.
	 * @param awhBuisnessSegment AWH Buisness Segment
	 * @param equipmentTypeCode Equipment Type Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of EquipmentReadingsWspltwmdfkGDO
	 */
	RestResponsePage<EquipmentReadingsWspltwmdfkGDO> equipmentReadingsWspltwmdfk(BigDecimal equipReadingBatchNo
	, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String plantEquipmentCode, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @return PlantEquipment
	 */
	PlantEquipment getPlantEquipment(String awhRegionCode, String plantEquipmentCode
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param centreCodeKey Centre Code Key
	 * @param readingRequiredDate Reading Required Date
	 * @param pageable a Pageable object used for pagination
	 * @return Page of PlantEquipment
	 */
	RestResponsePage<PlantEquipment> chkReadingsBatchSts(String awhRegionCode, String centreCodeKey, LocalDate readingRequiredDate
	, Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param equipmentBarcode Equipment Barcode
	 * @return PlantEquipment
	 */
	PlantEquipment getByRegionBarcode(String awhRegionCode, String equipmentBarcode
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @return PlantEquipment
	 */
	PlantEquipment updEquipmentTotals(String awhRegionCode, String plantEquipmentCode
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @param newPlantEquipmentCode New Plant Equipment Code
	 * @return PlantEquipment
	 */
	PlantEquipment chgkeyPlantEquipment(String awhRegionCode, String plantEquipmentCode, String newPlantEquipmentCode
	);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param pageable a Pageable object used for pagination
	 * @return Page of AddPlantEquipmentDTO
	 */
	RestResponsePage<AddPlantEquipmentDTO> addPlantEquipment(String awhRegionCode
	, Pageable pageable);

	/**
	 * 
	 * @param pageable a Pageable object used for pagination
	 * @return Page of BuildReadingBatchDTO
	 */
	RestResponsePage<BuildReadingBatchDTO> buildReadingBatch(
	Pageable pageable);

	/**
	 * 
	 * @param awhRegionCode AWH Region Code
	 * @param plantEquipmentCode Plant Equipment Code
	 * @return RenamemPlantEquipmentDTO
	 */
	RenamemPlantEquipmentDTO renamemPlantEquipment(String awhRegionCode, String plantEquipmentCode
	);
}
