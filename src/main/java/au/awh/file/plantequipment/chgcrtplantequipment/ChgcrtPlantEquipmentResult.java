package au.awh.file.plantequipment.chgcrtplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgcrtPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgcrtPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -88936469632301100L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
