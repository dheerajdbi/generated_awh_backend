package au.awh.file.plantequipment.chgcrtplantequipment;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentResult;
// imports section complete

/**
 * CHGOBJ Service controller for 'ChgCrt Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChgcrtPlantEquipmentService extends AbstractService<ChgcrtPlantEquipmentService, ChgcrtPlantEquipmentDTO>
{
	private final Step execute = define("execute", ChgcrtPlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private CreatePlantEquipmentService createPlantEquipmentService;

	//private final Step serviceCreatePlantEquipment = define("serviceCreatePlantEquipment",CreatePlantEquipmentResult.class, this::processServiceCreatePlantEquipment);
	
	@Autowired
	public ChgcrtPlantEquipmentService() {
		super(ChgcrtPlantEquipmentService.class, ChgcrtPlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgcrtPlantEquipmentParams params) throws ServiceException {
		ChgcrtPlantEquipmentDTO dto = new ChgcrtPlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgcrtPlantEquipmentDTO dto, ChgcrtPlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		PlantEquipmentId plantEquipmentId = new PlantEquipmentId();
		plantEquipmentId.setAwhRegionCode(dto.getAwhRegionCode());
		plantEquipmentId.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		PlantEquipment plantEquipment = plantEquipmentRepository.findById(plantEquipmentId).orElse(null);
		if (plantEquipment == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, plantEquipment);
			plantEquipment.setAwhRegionCode(dto.getAwhRegionCode());
			plantEquipment.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			plantEquipment.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			plantEquipment.setCentreCodeKey(dto.getCentreCodeKey());
			plantEquipment.setEquipmentDescription(dto.getEquipmentDescription());
			plantEquipment.setEquipmentBarcode(dto.getEquipmentBarcode());
			plantEquipment.setEquipmentComment(dto.getEquipmentComment());
			plantEquipment.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			plantEquipment.setEquipReadingUnit(dto.getEquipReadingUnit());
			plantEquipment.setEquipmentLeasor(dto.getEquipmentLeasor());
			plantEquipment.setEquipmentStatus(dto.getEquipmentStatus());
			plantEquipment.setEquipmentSerialNumber(dto.getEquipmentSerialNumber());
			plantEquipment.setLeaseComencmentDate(dto.getLeaseComencmentDate());
			plantEquipment.setLeaseExpireDate(dto.getLeaseExpireDate());
			plantEquipment.setLeaseMonthlyRateS(dto.getLeaseMonthlyRateS());
			plantEquipment.setLeaseMaxTotalUnits(dto.getLeaseMaxTotalUnits());
			plantEquipment.setReadingFrequency(dto.getReadingFrequency());
			plantEquipment.setVisualEquipmentCode(dto.getVisualEquipmentCode());

			processingBeforeDataUpdate(dto, plantEquipment);

			try {
				plantEquipmentRepository.saveAndFlush(plantEquipment);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChgcrtPlantEquipmentResult result = new ChgcrtPlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChgcrtPlantEquipmentDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		
		// Unprocessed SUB 8 -
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChgcrtPlantEquipmentDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		CreatePlantEquipmentResult createPlantEquipmentResult = null;
		CreatePlantEquipmentParams createPlantEquipmentParams = null;
		// * Ignore return code.
		// DEBUG genFunctionCall BEGIN 1000003 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000007 ACT Create Plant Equipment - Plant Equipment  *
		createPlantEquipmentParams = new CreatePlantEquipmentParams();
		createPlantEquipmentResult = new CreatePlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, createPlantEquipmentParams);
		createPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		createPlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		createPlantEquipmentParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		createPlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
		createPlantEquipmentParams.setEquipmentDescription(dto.getEquipmentDescription());
		createPlantEquipmentParams.setEquipmentBarcode(dto.getEquipmentBarcode());
		createPlantEquipmentParams.setEquipmentComment(dto.getEquipmentComment());
		createPlantEquipmentParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		createPlantEquipmentParams.setEquipReadingUnit(dto.getEquipReadingUnit());
		createPlantEquipmentParams.setEquipmentLeasor(dto.getEquipmentLeasor());
		createPlantEquipmentParams.setEquipmentStatus(dto.getEquipmentStatus());
		stepResult = createPlantEquipmentService.execute(createPlantEquipmentParams);
		createPlantEquipmentResult = (CreatePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(createPlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		// * Exit with return code for calling function to process.
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChgcrtPlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChgcrtPlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		
		// Unprocessed SUB 10 -
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChgcrtPlantEquipmentDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		
		// Unprocessed SUB 23 -
		return stepResult;
	}

//
//    /**
//     * CreatePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreatePlantEquipment(ChgcrtPlantEquipmentDTO dto, CreatePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgcrtPlantEquipmentParams chgcrtPlantEquipmentParams = new ChgcrtPlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chgcrtPlantEquipmentParams);
//        stepResult = StepResult.callService(ChgcrtPlantEquipmentService.class, chgcrtPlantEquipmentParams);
//
//        return stepResult;
//    }
//
}
