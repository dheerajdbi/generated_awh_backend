package au.awh.file.plantequipment.chgfortransfer;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipmentStatusEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgForTransfer ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgForTransferParams implements Serializable
{
	private static final long serialVersionUID = 7853197868820416626L;

    private String centreCodeKey = "";
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private EquipmentStatusEnum equipmentStatus = null;

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentStatus(String equipmentStatus) {
		setEquipmentStatus(EquipmentStatusEnum.valueOf(equipmentStatus));
	}
}
