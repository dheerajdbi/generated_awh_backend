package au.awh.file.plantequipment.chgfortransfer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChgForTransferDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private AwhBuisnessSegmentEnum awhBuisnessSegment;
	private BigDecimal leaseMonthlyRateS;
	private EquipReadingUnitEnum equipReadingUnit;
	private EquipmentStatusEnum equipmentStatus;
	private LocalDate leaseComencmentDate;
	private LocalDate leaseExpireDate;
	private ReadingFrequencyEnum readingFrequency;
	private String awhRegionCode;
	private String centreCodeKey;
	private String equipmentBarcode;
	private String equipmentComment;
	private String equipmentDescription;
	private String equipmentLeasor;
	private String equipmentSerialNumber;
	private String equipmentTypeCode;
	private String plantEquipmentCode;
	private String visualEquipmentCode;
	private BigDecimal leaseMaxTotalUnits;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}

	public String getEquipmentComment() {
		return equipmentComment;
	}

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}

	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}

	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}

	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}

	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}

	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}

	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}

	public void setEquipmentComment(String equipmentComment) {
		this.equipmentComment = equipmentComment;
	}

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}

	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}

	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}

	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
	}

	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}

	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}

}
