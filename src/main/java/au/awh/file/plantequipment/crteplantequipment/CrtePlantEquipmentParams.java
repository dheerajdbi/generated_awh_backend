package au.awh.file.plantequipment.crteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 18
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingFrequencyEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CrtePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CrtePlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 8292208191622171829L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private ErrorProcessingEnum errorProcessing = null;
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String centreCodeKey = "";
    private String equipmentDescription = "";
    private String equipmentBarcode = "";
    private String equipmentComment = "";
    private String equipmentTypeCode = "";
    private EquipReadingUnitEnum equipReadingUnit = null;
    private String equipmentLeasor = "";
    private String equipmentSerialNumber = "";
    private LocalDate leaseComencmentDate = null;
    private LocalDate leaseExpireDate = null;
    private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO;
    private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO;
    private ReadingFrequencyEnum readingFrequency = null;
    private String visualEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public String getEquipmentDescription() {
		return equipmentDescription;
	}
	
	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}
	
	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}
	
	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}
	
	public String getEquipmentComment() {
		return equipmentComment;
	}
	
	public void setEquipmentComment(String equipmentComment) {
		this.equipmentComment = equipmentComment;
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipReadingUnit(String equipReadingUnit) {
		setEquipReadingUnit(EquipReadingUnitEnum.valueOf(equipReadingUnit));
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}
	
	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}
	
	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}
	
	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}
	
	public void setLeaseComencmentDate(String leaseComencmentDate) {
		setLeaseComencmentDate(new LocalDateConverter().convert(leaseComencmentDate));
	}
	
	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}
	
	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}
	
	public void setLeaseExpireDate(String leaseExpireDate) {
		setLeaseExpireDate(new LocalDateConverter().convert(leaseExpireDate));
	}
	
	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}
	
	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}
	
	public void setLeaseMonthlyRateS(String leaseMonthlyRateS) {
		setLeaseMonthlyRateS(new BigDecimal(leaseMonthlyRateS));
	}
	
	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}
	
	
	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
		setLeaseMaxTotalUnits(leaseMaxTotalUnits);
	}
	
	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}
	
	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}
	
	public void setReadingFrequency(String readingFrequency) {
		setReadingFrequency(ReadingFrequencyEnum.valueOf(readingFrequency));
	}
	
	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}
	
	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}
}
