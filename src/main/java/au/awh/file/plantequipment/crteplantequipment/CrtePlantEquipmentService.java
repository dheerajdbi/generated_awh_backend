package au.awh.file.plantequipment.crteplantequipment;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=crtePlantEquipment (1879534) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SflselEnum;
import au.awh.model.WsListFieldIdEnum;
import au.awh.model.WsListItemStatusEnum;
// imports for Parameters
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'CrtE Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class CrtePlantEquipmentService extends AbstractService<CrtePlantEquipmentService, CrtePlantEquipmentDTO>
{
	private final Step execute = define("execute", CrtePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private CreatePlantEquipmentService createPlantEquipmentService;

	//private final Step serviceCreatePlantEquipment = define("serviceCreatePlantEquipment",CreatePlantEquipmentResult.class, this::processServiceCreatePlantEquipment);
	
	@Autowired
	public CrtePlantEquipmentService() {
		super(CrtePlantEquipmentService.class, CrtePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CrtePlantEquipmentParams params) throws ServiceException {
		CrtePlantEquipmentDTO dto = new CrtePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CrtePlantEquipmentDTO dto, CrtePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		CreatePlantEquipmentResult createPlantEquipmentResult = null;
		CreatePlantEquipmentParams createPlantEquipmentParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000274 ACT Create Plant Equipment - Plant Equipment  *
		createPlantEquipmentParams = new CreatePlantEquipmentParams();
		createPlantEquipmentResult = new CreatePlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, createPlantEquipmentParams);
		createPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		createPlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		createPlantEquipmentParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		createPlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
		createPlantEquipmentParams.setEquipmentDescription(dto.getEquipmentDescription());
		createPlantEquipmentParams.setEquipmentBarcode(dto.getEquipmentBarcode());
		createPlantEquipmentParams.setEquipmentComment(dto.getEquipmentComment());
		createPlantEquipmentParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		createPlantEquipmentParams.setEquipReadingUnit(dto.getEquipReadingUnit());
		createPlantEquipmentParams.setEquipmentLeasor(dto.getEquipmentLeasor());
		createPlantEquipmentParams.setEquipmentStatus("AT");
		stepResult = createPlantEquipmentService.execute(createPlantEquipmentParams);
		createPlantEquipmentResult = (CreatePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(createPlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS) {
				// PGM.*Return code is *Record already exists
				// DEBUG genFunctionCall BEGIN 1000287 ACT Send error message - '*Record already exists'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1001646) EXCINTFUN 1879534 PlantEquipment.crtePlantEquipment 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000209 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1879534 PlantEquipment.crtePlantEquipment 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000217 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000224 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000229 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		CrtePlantEquipmentResult result = new CrtePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * CreatePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreatePlantEquipment(CrtePlantEquipmentDTO dto, CreatePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        CrtePlantEquipmentParams crtePlantEquipmentParams = new CrtePlantEquipmentParams();
//        BeanUtils.copyProperties(dto, crtePlantEquipmentParams);
//        stepResult = StepResult.callService(CrtePlantEquipmentService.class, crtePlantEquipmentParams);
//
//        return stepResult;
//    }
//
}
