package au.awh.file.plantequipment.crteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CrtePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CrtePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = 2685872239406436304L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
