package au.awh.file.plantequipment.wrkplantequipment;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.freschelegacy.utils.RestResponsePage;

import au.awh.model.GlobalContext;
import au.awh.file.plantequipment.PlantEquipment;
// generateStatusFieldImportStatements BEGIN 35
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Plant Equipment' (WSEQP) and function 'Wrk Plant Equipment' (WSPLTWCDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class WrkPlantEquipmentDTO extends BaseDTO {
	private static final long serialVersionUID = -4194496556025755767L;

    private RestResponsePage<WrkPlantEquipmentGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private String awhRegionCode = "";
	private String awhRegionNameDrv = "";
	private String textField19Chars = "";
	private String conditionName = "";
	private String centreCodeKey = "";
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
	private String equipmentTypeCode = "";
	private String equipmentLeasor = "";
	private EquipmentStatusEnum equipmentStatus = null;
	private ReadingTypeEnum readingType = null;
	private String plantEquipmentCode = "";
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum.fromCode("");
	private String _SysconditionName = "";


	private WrkPlantEquipmentGDO gdo;

    public WrkPlantEquipmentDTO() {

    }

	public WrkPlantEquipmentDTO(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

    public void setPageGdo(RestResponsePage<WrkPlantEquipmentGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<WrkPlantEquipmentGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
    }

    public String getAwhRegionNameDrv() {
    	return awhRegionNameDrv;
    }

	public void setTextField19Chars(String textField19Chars) {
		this.textField19Chars = textField19Chars;
    }

    public String getTextField19Chars() {
    	return textField19Chars;
    }

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
    }

    public String getConditionName() {
    	return conditionName;
    }

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
    }

    public String getCentreCodeKey() {
    	return centreCodeKey;
    }

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
    }

    public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
    	return awhBuisnessSegment;
    }

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeCode() {
    	return equipmentTypeCode;
    }

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
    }

    public String getEquipmentLeasor() {
    	return equipmentLeasor;
    }

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
    }

    public EquipmentStatusEnum getEquipmentStatus() {
    	return equipmentStatus;
    }

	public void setReadingType(ReadingTypeEnum readingType) {
		this.readingType = readingType;
    }

    public ReadingTypeEnum getReadingType() {
    	return readingType;
    }

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
    }

    public String getPlantEquipmentCode() {
    	return plantEquipmentCode;
    }

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
    	return exitProgramOption;
    }
	public void set_SysConditionName(String conditionName) {
    	this._SysconditionName = conditionName;
    }

	public String get_SysConditionName() {
		return _SysconditionName;
	}

	public void setGdo(WrkPlantEquipmentGDO gdo) {
		this.gdo = gdo;
	}

	public WrkPlantEquipmentGDO getGdo() {
		return gdo;
	}

}