package au.awh.file.plantequipment.wrkplantequipment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import au.awh.utils.BuildInFunctionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionService;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsService;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateService;
import au.awh.file.equipmentreadings.updxeereadingtotals.UpdxeeReadingTotalsService;
import au.awh.file.equipmentreadings.wrkeequipmentreadings.WrkeEquipmentReadingsService;
import au.awh.file.plantequipment.addeplantequipment.AddePlantEquipmentService;
import au.awh.file.plantequipment.displayplantequipment.DisplayPlantEquipmentParams;
import au.awh.file.plantequipment.displayplantequipment.DisplayPlantEquipmentService;
import au.awh.file.plantequipment.dspeplantequipment.DspePlantEquipmentService;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentService;
import au.awh.file.plantequipment.renamemeplantequipment.RenamemePlantEquipmentService;
import au.awh.file.utilitiesdatetime.cvtdatetoddmmformat.CvtDateToDdMmFormatService;
// asServiceDtoImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionDTO;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateDTO;
import au.awh.file.equipmentreadings.updxeereadingtotals.UpdxeeReadingTotalsDTO;
import au.awh.file.equipmentreadings.wrkeequipmentreadings.WrkeEquipmentReadingsDTO;
import au.awh.file.plantequipment.addeplantequipment.AddePlantEquipmentDTO;
import au.awh.file.plantequipment.dspeplantequipment.DspePlantEquipmentDTO;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentDTO;
import au.awh.file.plantequipment.renamemeplantequipment.RenamemePlantEquipmentDTO;
import au.awh.file.utilitiesdatetime.cvtdatetoddmmformat.CvtDateToDdMmFormatDTO;
// asServiceParamsImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionParams;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsParams;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateParams;
import au.awh.file.equipmentreadings.updxeereadingtotals.UpdxeeReadingTotalsParams;
import au.awh.file.equipmentreadings.wrkeequipmentreadings.WrkeEquipmentReadingsParams;
import au.awh.file.plantequipment.addeplantequipment.AddePlantEquipmentParams;
import au.awh.file.plantequipment.dspeplantequipment.DspePlantEquipmentParams;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentParams;
import au.awh.file.plantequipment.renamemeplantequipment.RenamemePlantEquipmentParams;
import au.awh.file.utilitiesdatetime.cvtdatetoddmmformat.CvtDateToDdMmFormatParams;
// asServiceResultImportStatement
import au.awh.file.awhregioncentre.getecentresregion.GeteCentresRegionResult;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsResult;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateResult;
import au.awh.file.equipmentreadings.updxeereadingtotals.UpdxeeReadingTotalsResult;
import au.awh.file.equipmentreadings.wrkeequipmentreadings.WrkeEquipmentReadingsResult;
import au.awh.file.plantequipment.addeplantequipment.AddePlantEquipmentResult;
import au.awh.file.plantequipment.dspeplantequipment.DspePlantEquipmentResult;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentResult;
import au.awh.file.plantequipment.renamemeplantequipment.RenamemePlantEquipmentResult;
import au.awh.file.utilitiesdatetime.cvtdatetoddmmformat.CvtDateToDdMmFormatResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


// generateImportsForEnum
import au.awh.model.CmdKeyEnum;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 35
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.Flag1Enum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
// generateStatusFieldImportStatements END

/**
 * DSPFIL Service controller for 'Wrk Plant Equipment' (WSPLTWCDFK) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class WrkPlantEquipmentService extends AbstractService<WrkPlantEquipmentService, WrkPlantEquipmentState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private PlantEquipmentRepository plantEquipmentRepository;
    
    @Autowired
    private AddePlantEquipmentService addePlantEquipmentService;
    
    @Autowired
    private AddemEquipmentReadingsService addemEquipmentReadingsService;
    
    @Autowired
    private CvtDateToDdMmFormatService cvtDateToDdMmFormatService;
    
    @Autowired
    private DspePlantEquipmentService dspePlantEquipmentService;
    
    @Autowired
    private EdtePlantEquipmentService edtePlantEquipmentService;
    
    @Autowired
    private GeteByReverseDateService geteByReverseDateService;
    
    @Autowired
    private GeteCentresRegionService geteCentresRegionService;
    
    @Autowired
    private RenamemePlantEquipmentService renamemePlantEquipmentService;
    
    @Autowired
    private UpdxeeReadingTotalsService updxeeReadingTotalsService;
    
    @Autowired
    private WrkeEquipmentReadingsService wrkeEquipmentReadingsService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "wrkPlantEquipment";
    public static final String SCREEN_RCD = "WrkPlantEquipment.rcd";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", WrkPlantEquipmentParams.class, this::executeService);
    private final Step response = define("response", WrkPlantEquipmentDTO.class, this::processResponse);
    private final Step serviceEdtePlantEquipment = define("serviceEdtePlantEquipment", EdtePlantEquipmentResult.class, this::processServiceEdtePlantEquipment);
    private final Step postCallDspePlantEquipmentParams = define("serviceDspePlantEquipment",DisplayPlantEquipmentParams.class, this::postCallDspePlantEquipmentParams);

	//private final Step serviceAddePlantEquipment = define("serviceAddePlantEquipment",AddePlantEquipmentResult.class, this::processServiceAddePlantEquipment);
	//private final Step serviceEdtePlantEquipment = define("serviceEdtePlantEquipment",EdtePlantEquipmentResult.class, this::processServiceEdtePlantEquipment);
	//private final Step serviceDspePlantEquipment = define("serviceDspePlantEquipment",DspePlantEquipmentResult.class, this::processServiceDspePlantEquipment);
	//private final Step serviceGeteByReverseDate = define("serviceGeteByReverseDate",GeteByReverseDateResult.class, this::processServiceGeteByReverseDate);
	//private final Step serviceCvtDateToDdMmFormat = define("serviceCvtDateToDdMmFormat",CvtDateToDdMmFormatResult.class, this::processServiceCvtDateToDdMmFormat);
	//private final Step serviceGeteCentresRegion = define("serviceGeteCentresRegion",GeteCentresRegionResult.class, this::processServiceGeteCentresRegion);
	//private final Step serviceAddemEquipmentReadings = define("serviceAddemEquipmentReadings",AddemEquipmentReadingsResult.class, this::processServiceAddemEquipmentReadings);
	//private final Step serviceWrkeEquipmentReadings = define("serviceWrkeEquipmentReadings",WrkeEquipmentReadingsResult.class, this::processServiceWrkeEquipmentReadings);
	//private final Step serviceUpdxeeReadingTotals = define("serviceUpdxeeReadingTotals",UpdxeeReadingTotalsResult.class, this::processServiceUpdxeeReadingTotals);
	//private final Step serviceRenamemePlantEquipment = define("serviceRenamemePlantEquipment",RenamemePlantEquipmentResult.class, this::processServiceRenamemePlantEquipment);
	

    
    @Autowired
    public WrkPlantEquipmentService() {
        super(WrkPlantEquipmentService.class, WrkPlantEquipmentState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(WrkPlantEquipmentState state, WrkPlantEquipmentParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_CTL initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(WrkPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;
        if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
        	stepResult = loadFirstSubfilePage(state);
        }
        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Load pageGdo
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(WrkPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION;
    	stepResult = usrInitializeSubfileControl(state);
		dbfReadFirstDataRecord(state);
		/*if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
		    stepResult = loadNextSubfilePage(state);
		}*/
        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(WrkPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION;

    	List<WrkPlantEquipmentGDO> list = state.getPageGdo().getContent();
        for (WrkPlantEquipmentGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            stepResult = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//            TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//            TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(WrkPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            WrkPlantEquipmentDTO model = new WrkPlantEquipmentDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(WrkPlantEquipmentState state, WrkPlantEquipmentDTO model) {
    	StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (CmdKeyEnum.isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
        	//TODO:processHelpRequest(state);//synon built-in function
        }
        else if (CmdKeyEnum.isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(WrkPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION; 

        stepResult = usrProcessSubfilePreConfirm(state);
        if ( stepResult != NO_ACTION) {
            return stepResult;
        }

        if (state.get_SysErrorFound()) {
            return closedown(state);
        } else {
            if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
                return mainLoop(state);
            } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	stepResult = usrProcessCommandKeys(state);
//		        }

               stepResult = usrProcessSubfileControlPostConfirm(state);
               for (WrkPlantEquipmentGDO gdo : state.getPageGdo().getContent()) {
            	   if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                        stepResult = usrProcessSubfileRecordPostConfirm(state, gdo);
                        //TODO:writeSubfileRecord(state);   // synon built-in function
            	   }
		       }
		       stepResult = usrFinalProcessingPostConfirm(state);
               stepResult = usrProcessCommandKeys(state);
               mainLoop(state);
            }
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
	private StepResult usrProcessSubfilePreConfirm(WrkPlantEquipmentState state) {
		StepResult stepResult = NO_ACTION;

		stepResult = usrSubfileControlFunctionFields(state);
		stepResult = usrProcessSubfileControlPreConfirm(state);
		if (!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
			for (WrkPlantEquipmentGDO gdo : state.getPageGdo().getContent()) {

				if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
					stepResult = usrSubfileRecordFunctionFields(state, gdo);
					stepResult = usrProcessSubfileRecordPreConfirm(state, gdo);
					if (stepResult != NO_ACTION) {
						return stepResult;
					}
					// TODO:writeSubfileRecord(state); // synon built-in
					// function
				}
			}
		}

		stepResult = usrFinalProcessingPreConfirm(state);

		return stepResult;
	}

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(WrkPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        WrkPlantEquipmentResult params = new WrkPlantEquipmentResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(WrkPlantEquipmentState state) {
        //state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
	private void dbfReadNextPageRecord(WrkPlantEquipmentState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(WrkPlantEquipmentState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<WrkPlantEquipmentGDO> pageGdo = plantEquipmentRepository.wrkPlantEquipment(state.getAwhRegionCode(), state.getCentreCodeKey(), state.getAwhBuisnessSegment(), state.getEquipmentTypeCode(), state.getEquipmentLeasor(), state.getReadingType(), state.getPlantEquipmentCode(), pageable);
        state.setPageGdo(pageGdo);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteCentresRegionResult geteCentresRegionResult = null;
			GeteCentresRegionParams geteCentresRegionParams = null;
			// DEBUG genFunctionCall BEGIN 1000884 ACT GetE Centres Region - AWH Region/Centre  *
			geteCentresRegionParams = new GeteCentresRegionParams();
			geteCentresRegionResult = new GeteCentresRegionResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			//BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteCentresRegionParams);
			BeanUtils.copyProperties(dto, geteCentresRegionParams);
			geteCentresRegionParams.setCentreCodeKey(dto.getCentreCodeKey());
			//geteCentresRegionParams.setErrorProcessing("2");
			//geteCentresRegionParams.setGetRecordOption("*BLANK");
			stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
			geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteCentresRegionResult.get_SysReturnCode());
			dto.setAwhRegionCode(geteCentresRegionResult.getAwhRegionCode());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000910 ACT LCL.Reading Required Date = JOB.*System timestamp
			;
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1001115 ACT LCL.Equipment Status = CND.Active
			dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1001176 ACT LCL.New Program Date = JOB.*Job date + CON.-2 *DAYS
			dto.setLclNewProgramDate(BuildInFunctionUtils.getDateIncrement(
			LocalDate.now(), -2, "DY", "1111111", null, Boolean.FALSE, Boolean.FALSE));
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000689 ACT LCL.First Pass = CND.First Pass
			dto.setLclFirstPass(FirstPassEnum.fromCode("Y"));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1001221 ACT LCL.AWH Region Code = CTL.AWH Region Code
			dto.setLclAwhRegionCode(dto.getAwhRegionCode());
			// DEBUG genFunctionCall END
			if (dto.getTextField19Chars() != null && dto.getTextField19Chars().isEmpty()) {
				// CTL.Text Field 19 chars is Not entered
				// DEBUG genFunctionCall BEGIN 1001326 ACT CTL.Text Field 19 chars = CND.Visual Code
				dto.setTextField19Chars("** Visual Codes  **");
				// DEBUG genFunctionCall END
			}
			if (!dto.getAwhRegionCode().equals("")) {
				// CTL.AWH Region Code is Entered
				// DEBUG genFunctionCall BEGIN 1000896 ACT AWH region Name Drv      *FIELD                                             CTLA
				// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
				// DEBUG genFunctionCall END
			}
			if (dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) {
				// LCL.Equipment Status is Active
				// DEBUG genFunctionCall BEGIN 1001103 ACT CTL.*Condition name = CON.*Active Only*
				dto.set_SysConditionName("*Active Only*");
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1001109 ACT CTL.*Condition name = CON.*All Status*
				dto.set_SysConditionName("*All Status*");
				// DEBUG genFunctionCall END
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(WrkPlantEquipmentState dto, WrkPlantEquipmentGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteByReverseDateParams geteByReverseDateParams = null;
			GeteByReverseDateResult geteByReverseDateResult = null;
			CvtDateToDdMmFormatResult cvtDateToDdMmFormatResult = null;
			CvtDateToDdMmFormatParams cvtDateToDdMmFormatParams = null;
			if ((dto.getLclFirstPass() == FirstPassEnum.fromCode("Y")) && !(CmdKeyEnum.isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000723 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000727 ACT LCL.First Pass = CND.Not First Pass
				dto.setLclFirstPass(FirstPassEnum.fromCode("N"));
				// DEBUG genFunctionCall END
				// 
			} else if ((dto.getLclFirstPass() == FirstPassEnum.fromCode("N")) && (CmdKeyEnum.isNextPage(dto.get_SysCmdKey()))) {
				// DEBUG genFunctionCall BEGIN 1000738 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
				// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000742 ACT LCL.First Pass = CND.First Pass
				dto.setLclFirstPass(FirstPassEnum.fromCode("Y"));
				// DEBUG genFunctionCall END
			}
			// * Get previous reading.
			// DEBUG genFunctionCall BEGIN 1001008 ACT LCL.Reading Type = CND.Any
			dto.setLclReadingType(ReadingTypeEnum.fromCode("NE"));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000761 ACT GetE By reverse date - Equipment Readings  *
			geteByReverseDateParams = new GeteByReverseDateParams();
			geteByReverseDateResult = new GeteByReverseDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
			geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
			geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			//geteByReverseDateParams.setErrorProcessing("2");
			//geteByReverseDateParams.setGetRecordOption("I");
			geteByReverseDateParams.setReadingType(dto.getLclReadingType());
			geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
			stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
			geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
			dto.setLclReadingValue(geteByReverseDateResult.getReadingValue());
			dto.setLclDateAdvised(geteByReverseDateResult.getReadingRequiredDate());
			dto.setLclReadingType(geteByReverseDateResult.getReadingType());
			// DEBUG genFunctionCall END
			if (!(dto.getReadingType() == ReadingTypeEnum.fromCode("*BLANK"))) {
				// NOT CTL.Reading Type is Not entered
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				dto.getReadingType() != dto.getLclReadingType()
				// DEBUG ComparatorJavaGenerator END
				) {
					// CTL.Reading Type NE LCL.Reading Type
					// DEBUG genFunctionCall BEGIN 1001247 ACT PGM.*Record selected = CND.*NO
					dto.set_SysRecordSelected(RecordSelectedEnum.fromCode("N"));
					// DEBUG genFunctionCall END
				}
			}
			if ((dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) && (gdo.getEquipmentStatus() == EquipmentStatusEnum.fromCode("RT"))) {
				// DEBUG genFunctionCall BEGIN 1001033 ACT PGM.*Record selected = CND.*NO
				dto.set_SysRecordSelected(RecordSelectedEnum.fromCode("N"));
				// DEBUG genFunctionCall END
			}
			if ((!gdo.getVisualEquipmentCode().equals("")) && (dto.getTextField19Chars().equals("** Visual Codes  **L"))) {
				// DEBUG genFunctionCall BEGIN 1001306 ACT RCD.Display Plant equip Code = RCD.Visual Equipment Code
				gdo.setDisplayPlantEquipCode(String.format("%-10s", gdo.getVisualEquipmentCode()));
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1001314 ACT RCD.Display Plant equip Code = RCD.Plant Equipment Code
				gdo.setDisplayPlantEquipCode(gdo.getPlantEquipmentCode());
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysRecordSelected() == RecordSelectedEnum.fromCode("Y")) {
				// PGM.*Record selected is *YES
				// DEBUG genFunctionCall BEGIN 1001012 ACT RCD.Reading Type = LCL.Reading Type
				gdo.setReadingType(dto.getLclReadingType());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000804 ACT Cvt Date to DD/MM format - Utilities Date/Time  *
				cvtDateToDdMmFormatParams = new CvtDateToDdMmFormatParams();
				cvtDateToDdMmFormatResult = new CvtDateToDdMmFormatResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), cvtDateToDdMmFormatParams);
				cvtDateToDdMmFormatParams.setDateFromIso(dto.getLclDateAdvised());
				stepResult = cvtDateToDdMmFormatService.execute(cvtDateToDdMmFormatParams);
				cvtDateToDdMmFormatResult = (CvtDateToDdMmFormatResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(cvtDateToDdMmFormatResult.get_SysReturnCode());
				gdo.setDateDdmm(cvtDateToDdMmFormatResult.getDateDdmm());
				// DEBUG genFunctionCall END
				if (
				// DEBUG ComparatorJavaGenerator BEGIN
				dto.getLclDateAdvised().isBefore(dto.getLclNewProgramDate()) || dto.getLclDateAdvised().equals(dto.getLclNewProgramDate())
				// DEBUG ComparatorJavaGenerator END
				) {
					// LCL.Date Advised LE LCL.New Program Date
					// DEBUG genFunctionCall BEGIN 1001182 ACT RCD.Flag 1 = CND.No
					gdo.setFlag1(Flag1Enum.fromCode("N"));
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1001192 ACT RCD.Flag 1 = CND.Blank
					gdo.setFlag1(Flag1Enum.fromCode(""));
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1001016 ACT LCL.Reading Type = CND.Service Reading
				dto.setLclReadingType(ReadingTypeEnum.fromCode("SV"));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000810 ACT GetE By reverse date - Equipment Readings  *
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
				//geteByReverseDateParams.setErrorProcessing("2");
				//geteByReverseDateParams.setGetRecordOption("I");
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				dto.setLclServiceValue(geteByReverseDateResult.getReadingValue());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				// DEBUG genFunctionCall END
				// * Calculate value since Service
				if (dto.getLclServiceValue() != BigDecimal.ZERO) {
					// LCL.Service Value is Entered
					// DEBUG genFunctionCall BEGIN 1000832 ACT RCD.Reading Value = LCL.Reading Value - LCL.Service Value
					gdo.setReadingValue(dto.getLclReadingValue().subtract(dto.getLclServiceValue()));
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1001004 ACT RCD.Last Reading = LCL.Reading Value
				gdo.setLastReading(dto.getLclReadingValue());
				// DEBUG genFunctionCall END
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			dto.getReadingType() != dto.getLclReadingType()
			// DEBUG ComparatorJavaGenerator END
			) {
				// CTL.Reading Type NE PAR.Reading Type
				// DEBUG genFunctionCall BEGIN 1001048 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1001054 ACT PAR.Reading Type = CTL.Reading Type
				dto.setLclReadingType(dto.getReadingType());
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	AddePlantEquipmentResult addePlantEquipmentResult = null;
			AddePlantEquipmentParams addePlantEquipmentParams = null;
			if (CmdKeyEnum.isCancel_1(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1001124 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("C"));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1001128 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("10")) {
				// CTL.*CMD key is CF10
				if (dto.getTextField19Chars().equals("** Visual Codes  **L")) {
					// CTL.Text Field 19 chars is Visual Code
					// DEBUG genFunctionCall BEGIN 1001340 ACT CTL.Text Field 19 chars = CND.Leasor Code
					dto.setTextField19Chars("** Leasor Codes **");
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1001358 ACT CTL.Text Field 19 chars = CND.Visual Code
					dto.setTextField19Chars("** Visual Codes  **");
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1001350 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("11")) {
				// CTL.*CMD key is CF11
				if (dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) {
					// LCL.Equipment Status is Active
					// DEBUG genFunctionCall BEGIN 1001134 ACT LCL.Equipment Status = CND.Not Entered
					dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode(""));
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1001142 ACT LCL.Equipment Status = CND.Active
					dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1001148 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				// DEBUG genFunctionCall END
			}
			if (CmdKeyEnum.isAdd(dto.get_SysCmdKey())) {
				// CTL.*CMD key is Add
				// DEBUG genFunctionCall BEGIN 1000450 ACT AddE Plant Equipment - Plant Equipment  *
				addePlantEquipmentParams = new AddePlantEquipmentParams();
				addePlantEquipmentResult = new AddePlantEquipmentResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addePlantEquipmentParams);
				addePlantEquipmentParams.setErrorProcessing("4");
				addePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
				// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (72) -> EXCINTFUN PlantEquipment.addePlantEquipment
				stepResult = addePlantEquipmentService.execute(addePlantEquipmentParams);
				addePlantEquipmentResult = (AddePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(addePlantEquipmentResult.get_SysReturnCode());
				dto.setLclExitProgramOption(addePlantEquipmentResult.getExitProgramOption());
				// DEBUG genFunctionCall END
				if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
					// LCL.Exit Program Option is Exit requested
					// * NOTE: This is only required where fast exit is to be enabled.
					// * Remove this condition check when fast exit is not required.
					// * All displays that are the first from menu must ignore fast exit.
					// DEBUG genFunctionCall BEGIN 1001373 ACT PAR.Exit Program Option = CND.Normal
					dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
					// DEBUG genFunctionCall END
					// 
				} else {
					// *OTHERWISE
					// * Ignore exit program option.
					// DEBUG genFunctionCall BEGIN 1000471 ACT PAR.Exit Program Option = CND.Normal
					dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
					// DEBUG genFunctionCall END
				}
				// * Subfile record processing will be bypassed.
				// DEBUG genFunctionCall BEGIN 1000475 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
				// DEBUG genFunctionCall END
			}
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			!dto.getAwhRegionCode().equals(dto.getLclAwhRegionCode())
			// DEBUG ComparatorJavaGenerator END
			) {
				// CTL.AWH Region Code NE LCL.AWH Region Code
				// DEBUG genFunctionCall BEGIN 1001210 ACT LCL.AWH Region Code = CTL.AWH Region Code
				dto.setLclAwhRegionCode(dto.getAwhRegionCode());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1001214 ACT CTL.Centre Code           KEY = CND.Not entered
				dto.setCentreCodeKey("");
				// DEBUG genFunctionCall END
			}
			if (!dto.getAwhRegionCode().equals("")) {
				// CTL.AWH Region Code is Entered
				// DEBUG genFunctionCall BEGIN 1000906 ACT AWH region Name Drv      *FIELD                                             CTLA
				// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
				// DEBUG genFunctionCall END
			}
			// * Condition for normal processing of subfile records.
			// DEBUG genFunctionCall BEGIN 1000601 ACT LCL.Exit Program Option = CND.Normal
			dto.setLclExitProgramOption(ExitProgramOptionEnum.fromCode(""));
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(WrkPlantEquipmentState dto, WrkPlantEquipmentGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 170 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(WrkPlantEquipmentState dto, WrkPlantEquipmentGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	EdtePlantEquipmentResult edtePlantEquipmentResult = null;
			RenamemePlantEquipmentParams renamemePlantEquipmentParams = null;
			//EdtePlantEquipmentParams edtePlantEquipmentParams = null;
			RenamemePlantEquipmentResult renamemePlantEquipmentResult = null;
			AddemEquipmentReadingsParams addemEquipmentReadingsParams = null;
			UpdxeeReadingTotalsResult updxeeReadingTotalsResult = null;
			UpdxeeReadingTotalsParams updxeeReadingTotalsParams = null;
			AddemEquipmentReadingsResult addemEquipmentReadingsResult = null;
			DspePlantEquipmentResult dspePlantEquipmentResult = null;
			WrkeEquipmentReadingsParams wrkeEquipmentReadingsParams = null;
			DspePlantEquipmentParams dspePlantEquipmentParams = null;
			WrkeEquipmentReadingsResult wrkeEquipmentReadingsResult = null;
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("C")) {
				// LCL.Exit Program Option is Cancel requested
				// * Reprocess this record next time user presses Enter.
				// DEBUG genFunctionCall BEGIN 1000578 ACT PGM.*Re-read Subfile Record = CND.*YES
				dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum.fromCode("1"));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				if (!(gdo.get_SysSelected().equals("Not entered"))) {
					// NOT RCD.*SFLSEL is Not entered
					// DEBUG genFunctionCall BEGIN 1000320 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
					// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("Change") || gdo.get_SysSelected().equals("2")) {
					// RCD.*SFLSEL is Edit
					// DEBUG genFunctionCall BEGIN 1000616 ACT EdtE Plant Equipment - Plant Equipment  *
					EdtePlantEquipmentDTO edtePlantEquipmentParams = new EdtePlantEquipmentDTO();
                    BeanUtils.copyProperties(gdo, edtePlantEquipmentParams);
					/*edtePlantEquipmentParams = new EdtePlantEquipmentParams();*/
					//edtePlantEquipmentResult = new EdtePlantEquipmentResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					// BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), edtePlantEquipmentParams);
					//BeanUtils.copyProperties(gdo, edtePlantEquipmentParams);
					//edtePlantEquipmentParams.setProgramMode("CHG");
					//edtePlantEquipmentParams.setErrorProcessing("4");
					edtePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
					edtePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.edtePlantEquipment
					//stepResult = edtePlantEquipmentService.execute(edtePlantEquipmentParams);
					stepResult =
                            StepResult.callService(
                                    EdtePlantEquipmentService.class,
                                    edtePlantEquipmentParams).thenCall(serviceEdtePlantEquipment);
					//edtePlantEquipmentResult = (EdtePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
					//dto.set_SysReturnCode(edtePlantEquipmentResult.get_SysReturnCode());
					//dto.setLclExitProgramOption(edtePlantEquipmentResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000639 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1000643 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					return stepResult;
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("4") || gdo.get_SysSelected().equals("*Delete#2")) {
					// RCD.*SFLSEL is *Delete request
				}
				if (gdo.get_SysSelected().equals("5#1") || gdo.get_SysSelected().equals("5#2")) {
					// RCD.*SFLSEL is Display
					// DEBUG genFunctionCall BEGIN 1000650 ACT DspE Plant Equipment - Plant Equipment  *
					dspePlantEquipmentParams = new DspePlantEquipmentParams();
					dspePlantEquipmentResult = new DspePlantEquipmentResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(gdo, dspePlantEquipmentParams);
					//dspePlantEquipmentParams.setErrorProcessing("4");
					dspePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
					dspePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.dspePlantEquipment
					//stepResult = dspePlantEquipmentService.execute(dspePlantEquipmentParams);
					//dspePlantEquipmentResult = (DspePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
					stepResult =
                            StepResult.callService(
                                    DspePlantEquipmentService.class, dspePlantEquipmentParams)
                                    .thenCall(postCallDspePlantEquipmentParams);
					//dto.set_SysReturnCode(dspePlantEquipmentResult.get_SysReturnCode());
					dto.setLclExitProgramOption(dspePlantEquipmentResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000674 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
					return stepResult;
				}
				if (gdo.get_SysSelected().equals("R")) {
					// RCD.*SFLSEL is R
					// DEBUG genFunctionCall BEGIN 1000967 ACT WrkE Equipment Readings - Equipment Readings  *
					wrkeEquipmentReadingsParams = new WrkeEquipmentReadingsParams();
					wrkeEquipmentReadingsResult = new WrkeEquipmentReadingsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), wrkeEquipmentReadingsParams);
					wrkeEquipmentReadingsParams.setErrorProcessing("4");
					wrkeEquipmentReadingsParams.setAwhRegionCode(gdo.getAwhRegionCode());
					wrkeEquipmentReadingsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN EquipmentReadings.wrkeEquipmentReadings
					stepResult = wrkeEquipmentReadingsService.execute(wrkeEquipmentReadingsParams);
					 if ( stepResult != NO_ACTION) {
				            return stepResult;
				        }
					wrkeEquipmentReadingsResult = (WrkeEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(wrkeEquipmentReadingsResult.get_SysReturnCode());
					dto.setLclExitProgramOption(wrkeEquipmentReadingsResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000994 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
				}
				if (gdo.get_SysSelected().equals("A")) {
					// RCD.*SFLSEL is A
					// DEBUG genFunctionCall BEGIN 1000925 ACT AddE:M Equipment Readings - Equipment Readings  *
					addemEquipmentReadingsParams = new AddemEquipmentReadingsParams();
					addemEquipmentReadingsResult = new AddemEquipmentReadingsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addemEquipmentReadingsParams);
					addemEquipmentReadingsParams.setErrorProcessing("4");
					addemEquipmentReadingsParams.setAwhRegionCode(gdo.getAwhRegionCode());
					addemEquipmentReadingsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					addemEquipmentReadingsParams.setReadingRequiredDate("0001-01-01");
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN EquipmentReadings.addemEquipmentReadings
					stepResult = addemEquipmentReadingsService.execute(addemEquipmentReadingsParams);
					addemEquipmentReadingsResult = (AddemEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(addemEquipmentReadingsResult.get_SysReturnCode());
					dto.setLclExitProgramOption(addemEquipmentReadingsResult.getExitProgramOption());
					gdo.setReadingValue(addemEquipmentReadingsResult.getReadingValue());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// DEBUG genFunctionCall BEGIN 1001377 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1000952 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1000956 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("B")) {
					// RCD.*SFLSEL is B
					// DEBUG genFunctionCall BEGIN 1001094 ACT UpdXE:E Reading Totals - Equipment Readings  *
					updxeeReadingTotalsParams = new UpdxeeReadingTotalsParams();
					updxeeReadingTotalsResult = new UpdxeeReadingTotalsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), updxeeReadingTotalsParams);
					updxeeReadingTotalsParams.setErrorProcessing("2");
					updxeeReadingTotalsParams.setAwhRegionCode(gdo.getAwhRegionCode());
					updxeeReadingTotalsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					stepResult = updxeeReadingTotalsService.execute(updxeeReadingTotalsParams);
					updxeeReadingTotalsResult = (UpdxeeReadingTotalsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(updxeeReadingTotalsResult.get_SysReturnCode());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// DEBUG genFunctionCall BEGIN 1001075 ACT PAR.Exit Program Option = CND.Exit requested
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("E"));
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1001079 ACT Exit program - return code CND.*Normal
						// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
						// DEBUG genFunctionCall END
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1001086 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1001090 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					// DEBUG genFunctionCall END
				}
				if (gdo.get_SysSelected().equals("N")) {
					// RCD.*SFLSEL is N
					// DEBUG genFunctionCall BEGIN 1001384 ACT ReName:ME Plant Equipment - Plant Equipment  *
					renamemePlantEquipmentParams = new RenamemePlantEquipmentParams();
					renamemePlantEquipmentResult = new RenamemePlantEquipmentResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), renamemePlantEquipmentParams);
					renamemePlantEquipmentParams.setErrorProcessing("4");
					renamemePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
					renamemePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.renamemePlantEquipment
					stepResult = renamemePlantEquipmentService.execute(renamemePlantEquipmentParams);
					renamemePlantEquipmentResult = (RenamemePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(renamemePlantEquipmentResult.get_SysReturnCode());
					dto.setLclExitProgramOption(renamemePlantEquipmentResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
						// LCL.Exit Program Option is Exit requested
						// * NOTE: This is only required where fast exit is to be enabled.
						// * Remove this condition check when fast exit is not required.
						// * All displays that are the first from menu must ignore fast exit.
						// 
					} else {
						// *OTHERWISE
						// * Ignore exit program option.
						// DEBUG genFunctionCall BEGIN 1001408 ACT PAR.Exit Program Option = CND.Normal
						dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
						// DEBUG genFunctionCall END
					}
					// DEBUG genFunctionCall BEGIN 1001412 ACT PGM.*Reload subfile = CND.*YES
					dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
					// DEBUG genFunctionCall END
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(WrkPlantEquipmentState dto, WrkPlantEquipmentGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 209 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 228 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(WrkPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        	
			if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000251 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("E"));
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * AddePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAddePlantEquipment(WrkPlantEquipmentState state, AddePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * EdtePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEdtePlantEquipment(WrkPlantEquipmentState state, EdtePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * DspePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDspePlantEquipment(WrkPlantEquipmentState state, DspePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteByReverseDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteByReverseDate(WrkPlantEquipmentState state, GeteByReverseDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * CvtDateToDdMmFormatService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCvtDateToDdMmFormat(WrkPlantEquipmentState state, CvtDateToDdMmFormatParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteCentresRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteCentresRegion(WrkPlantEquipmentState state, GeteCentresRegionParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * AddemEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAddemEquipmentReadings(WrkPlantEquipmentState state, AddemEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * WrkeEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceWrkeEquipmentReadings(WrkPlantEquipmentState state, WrkeEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * UpdxeeReadingTotalsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceUpdxeeReadingTotals(WrkPlantEquipmentState state, UpdxeeReadingTotalsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * RenamemePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRenamemePlantEquipment(WrkPlantEquipmentState state, RenamemePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        WrkPlantEquipmentParams wrkPlantEquipmentParams = new WrkPlantEquipmentParams();
//        BeanUtils.copyProperties(state, wrkPlantEquipmentParams);
//        stepResult = StepResult.callService(WrkPlantEquipmentService.class, wrkPlantEquipmentParams);
//
//        return stepResult;
//    }
//

    private StepResult processServiceEdtePlantEquipment(WrkPlantEquipmentState state, EdtePlantEquipmentResult serviceResult) {
        StepResult result = NO_ACTION;

        if(serviceResult != null) {
          BeanUtils.copyProperties(serviceResult, state);
        }

        // DEBUG genFunctionCall Parameters OUT
        state.setLclExitProgramOption(serviceResult.getExitProgramOption());
        // DEBUG genFunctionCall Parameters DONE
        // switchBLK 1000621 BLK CAS
        // switchSUB 1000621 BLK CAS
        if (state.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
            // LCL.Exit Program Option is Exit requested
            // switchBLK 1000624 BLK TXT
            // * NOTE: This is only required where fast exit is to be enabled.
            // switchBLK 1000625 BLK TXT
            // * Remove this condition check when fast exit is not required.
            // switchBLK 1000626 BLK TXT
            // * All displays that are the first from menu must ignore fast exit.
            // switchBLK 1000627 BLK ACT
            // switchBLK 1000631 BLK ACT
            // switchBLK 1000634 BLK TXT
            //
        } // switchSUB 1000635 SUB
        else {
            // *OTHERWISE
            // switchBLK 1000637 BLK TXT
            // * Ignore exit program option.
            // switchBLK 1000638 BLK ACT
            // DEBUG genFunctionCall 1000639 ACT PAR.Exit Program Option = CND.Normal
            state.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
            state.setLclExitProgramOption(ExitProgramOptionEnum.fromCode(""));
        }
        // switchBLK 1000642 BLK ACT
        // DEBUG genFunctionCall 1000643 ACT PGM.*Reload subfile = CND.*YES
        state.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));

        result = mainLoop(state);

        return result;
    }
    
	/**
	 * DisplayPlantEquipmentService returned response processing method.
	 * 
	 * @param state
	 *            - Service state class.
	 * @param serviceResult
	 *            - returned service model.
	 * @return
	 */
	private StepResult postCallDspePlantEquipmentParams(WrkPlantEquipmentState state,
			DisplayPlantEquipmentParams serviceResult) {
		StepResult result = NO_ACTION;

		// result probably empty and cause issue to copy params
		// if(serviceResult != null) {
		// BeanUtils.copyProperties(serviceResult, state);
		// }

		if (state.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {

		} else {
			state.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
		}

		result = mainLoop(state);

		return result;
	}

}
