package au.awh.file.plantequipment.wrkplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.plantequipment.PlantEquipment;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import au.awh.config.LocalDateDeserializer;
import au.awh.config.LocalDateSerializer;

// generateStatusFieldImportStatements BEGIN 24
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.Flag1Enum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;


/**
 * Gdo for file 'Plant Equipment' (WSEQP) and function 'Wrk Plant Equipment' (WSPLTWCDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class WrkPlantEquipmentGDO implements Serializable {
	private static final long serialVersionUID = 3880012304164538421L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String centreCodeKey = "";
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
	private String equipmentTypeCode = "";
	private String equipmentLeasor = "";
	private String equipmentSerialNumber = "";
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate leaseComencmentDate = null;
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate leaseExpireDate = null;
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO;
	private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO;
	private String plantEquipmentCode = "";
	private EquipmentStatusEnum equipmentStatus = null;
	private ReadingFrequencyEnum readingFrequency = null;
	private String equipmentDescription = "";
	private BigDecimal dateDdmm = BigDecimal.ZERO;
	private Flag1Enum flag1 = null;
	private BigDecimal lastReading = BigDecimal.ZERO;
	private ReadingTypeEnum readingType = null;
	private BigDecimal readingValue = BigDecimal.ZERO;
	private String visualEquipmentCode = "";
	private String displayPlantEquipCode = "";
	private String equipmentBarcode = "";
	private String equipmentComment = "";
	private String awhRegionCode = "";
	private EquipReadingUnitEnum equipReadingUnit = null;

	public WrkPlantEquipmentGDO() {

	}

   	public WrkPlantEquipmentGDO(String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String equipmentLeasor, String equipmentSerialNumber, /*LocalDate leaseComencmentDate, LocalDate leaseExpireDate,*/ BigDecimal leaseMonthlyRateS, BigDecimal leaseMaxTotalUnits, String plantEquipmentCode, EquipmentStatusEnum equipmentStatus, ReadingFrequencyEnum readingFrequency, String equipmentDescription, String visualEquipmentCode, String equipmentBarcode, String equipmentComment, String awhRegionCode, EquipReadingUnitEnum equipReadingUnit) {
		this.centreCodeKey = centreCodeKey;
		this.awhBuisnessSegment = awhBuisnessSegment;
		this.equipmentTypeCode = equipmentTypeCode;
		this.equipmentLeasor = equipmentLeasor;
		this.equipmentSerialNumber = equipmentSerialNumber;
		//this.leaseComencmentDate = leaseComencmentDate;
		//this.leaseExpireDate = leaseExpireDate;
		this.leaseMonthlyRateS = leaseMonthlyRateS;
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentStatus = equipmentStatus;
		this.readingFrequency = readingFrequency;
		this.equipmentDescription = equipmentDescription;
		this.visualEquipmentCode = visualEquipmentCode;
		this.equipmentBarcode = equipmentBarcode;
		this.equipmentComment = equipmentComment;
		this.awhRegionCode = awhRegionCode;
		this.equipReadingUnit = equipReadingUnit;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setCentreCodeKey(String centreCodeKey) {
    	this.centreCodeKey = centreCodeKey;
    }

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
    	this.awhBuisnessSegment = awhBuisnessSegment;
    }

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
    	this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
    	this.equipmentLeasor = equipmentLeasor;
    }

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
    	this.equipmentSerialNumber = equipmentSerialNumber;
    }

	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}

	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
    	this.leaseComencmentDate = leaseComencmentDate;
    }

	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}

	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
    	this.leaseExpireDate = leaseExpireDate;
    }

	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}

	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
    	this.leaseMonthlyRateS = leaseMonthlyRateS;
    }

	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}

	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
    	this.leaseMaxTotalUnits = leaseMaxTotalUnits;
    }

	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
    	this.plantEquipmentCode = plantEquipmentCode;
    }

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
    	this.equipmentStatus = equipmentStatus;
    }

	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}

	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
    	this.readingFrequency = readingFrequency;
    }

	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}

	public void setEquipmentDescription(String equipmentDescription) {
    	this.equipmentDescription = equipmentDescription;
    }

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public void setDateDdmm(BigDecimal dateDdmm) {
    	this.dateDdmm = dateDdmm;
    }

	public BigDecimal getDateDdmm() {
		return dateDdmm;
	}

	public void setFlag1(Flag1Enum flag1) {
    	this.flag1 = flag1;
    }

	public Flag1Enum getFlag1() {
		return flag1;
	}

	public void setLastReading(BigDecimal lastReading) {
    	this.lastReading = lastReading;
    }

	public BigDecimal getLastReading() {
		return lastReading;
	}

	public void setReadingType(ReadingTypeEnum readingType) {
    	this.readingType = readingType;
    }

	public ReadingTypeEnum getReadingType() {
		return readingType;
	}

	public void setReadingValue(BigDecimal readingValue) {
    	this.readingValue = readingValue;
    }

	public BigDecimal getReadingValue() {
		return readingValue;
	}

	public void setVisualEquipmentCode(String visualEquipmentCode) {
    	this.visualEquipmentCode = visualEquipmentCode;
    }

	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	public void setDisplayPlantEquipCode(String displayPlantEquipCode) {
    	this.displayPlantEquipCode = displayPlantEquipCode;
    }

	public String getDisplayPlantEquipCode() {
		return displayPlantEquipCode;
	}

	public void setEquipmentBarcode(String equipmentBarcode) {
    	this.equipmentBarcode = equipmentBarcode;
    }

	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}

	public void setEquipmentComment(String equipmentComment) {
    	this.equipmentComment = equipmentComment;
    }

	public String getEquipmentComment() {
		return equipmentComment;
	}

	public void setAwhRegionCode(String awhRegionCode) {
    	this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
    	this.equipReadingUnit = equipReadingUnit;
    }

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}