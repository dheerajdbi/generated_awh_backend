package au.awh.file.plantequipment.wrkplantequipment;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.GlobalContext;
// generateStatusFieldImportStatements BEGIN 29
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.StateCodeKeyEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Wrk Plant Equipment' (WSPLTWCDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
 @Configurable
public class WrkPlantEquipmentState extends WrkPlantEquipmentDTO {
	private static final long serialVersionUID = -8294972813669088627L;


	@Autowired
	private GlobalContext globalCtx;

	// Local fields
	private FirstPassEnum lclFirstPass = null;
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum.fromCode("");
	private LocalDate lclDateAdvised = null;
	private LocalDate lclNewProgramDate = null;
	private String lclAwhRegionCode = "";
	private BigDecimal lclReadingValue = BigDecimal.ZERO;
	private ReadingTypeEnum lclReadingType = ReadingTypeEnum.fromCode("");
	private LocalDate lclReadingRequiredDate = null;
	private BigDecimal lclServiceValue = BigDecimal.ZERO;
	private EquipmentStatusEnum lclEquipmentStatus = EquipmentStatusEnum.fromCode("");

	// System fields
	private RecordSelectedEnum _sysRecordSelected = null;
	private ReReadSubfileRecordEnum _sysReReadSubfileRecord = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public WrkPlantEquipmentState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	public void setWfLdaClientAccountId(String ldaClientAccountId) {
		globalCtx.setString("ldaClientAccountId", ldaClientAccountId);
	}

	public String getWfLdaClientAccountId() {
		return globalCtx.getString("ldaClientAccountId");
	}

	public void setWfLdaClipCode(String ldaClipCode) {
		globalCtx.setString("ldaClipCode", ldaClipCode);
	}

	public String getWfLdaClipCode() {
		return globalCtx.getString("ldaClipCode");
	}

	public void setWfLdaDefaultCentreCode(String ldaDefaultCentreCode) {
		globalCtx.setString("ldaDefaultCentreCode", ldaDefaultCentreCode);
	}

	public String getWfLdaDefaultCentreCode() {
		return globalCtx.getString("ldaDefaultCentreCode");
	}

	public void setWfLdaDefaultStateCode(String ldaDefaultStateCode) {
		globalCtx.setString("ldaDefaultStateCode", ldaDefaultStateCode);
	}

	public String getWfLdaDefaultStateCode() {
		return globalCtx.getString("ldaDefaultStateCode");
	}

	public void setWfLdaDftSaleNbrId(String ldaDftSaleNbrId) {
		globalCtx.setString("ldaDftSaleNbrId", ldaDftSaleNbrId);
	}

	public String getWfLdaDftSaleNbrId() {
		return globalCtx.getString("ldaDftSaleNbrId");
	}

	public void setWfLdaDftSaleNbrSlgCtr(String ldaDftSaleNbrSlgCtr) {
		globalCtx.setString("ldaDftSaleNbrSlgCtr", ldaDftSaleNbrSlgCtr);
	}

	public String getWfLdaDftSaleNbrSlgCtr() {
		return globalCtx.getString("ldaDftSaleNbrSlgCtr");
	}

	public void setWfLdaDftSaleNbrStgCtr(String ldaDftSaleNbrStgCtr) {
		globalCtx.setString("ldaDftSaleNbrStgCtr", ldaDftSaleNbrStgCtr);
	}

	public String getWfLdaDftSaleNbrStgCtr() {
		return globalCtx.getString("ldaDftSaleNbrStgCtr");
	}

	public void setWfLdaDftSaleSeasonCent(String ldaDftSaleSeasonCent) {
		globalCtx.setString("ldaDftSaleSeasonCent", ldaDftSaleSeasonCent);
	}

	public String getWfLdaDftSaleSeasonCent() {
		return globalCtx.getString("ldaDftSaleSeasonCent");
	}

	public void setWfLdaDftSaleSeasonYear(String ldaDftSaleSeasonYear) {
		globalCtx.setString("ldaDftSaleSeasonYear", ldaDftSaleSeasonYear);
	}

	public String getWfLdaDftSaleSeasonYear() {
		return globalCtx.getString("ldaDftSaleSeasonYear");
	}

	public void setWfLdaFolioNumber(String ldaFolioNumber) {
		globalCtx.setString("ldaFolioNumber", ldaFolioNumber);
	}

	public String getWfLdaFolioNumber() {
		return globalCtx.getString("ldaFolioNumber");
	}

	public void setWfLdaLotNumber(String ldaLotNumber) {
		globalCtx.setString("ldaLotNumber", ldaLotNumber);
	}

	public String getWfLdaLotNumber() {
		return globalCtx.getString("ldaLotNumber");
	}

	public void setWfLdaProcSaleNbrId(String ldaProcSaleNbrId) {
		globalCtx.setString("ldaProcSaleNbrId", ldaProcSaleNbrId);
	}

	public String getWfLdaProcSaleNbrId() {
		return globalCtx.getString("ldaProcSaleNbrId");
	}

	public void setWfLdaProcSaleNbrSlgCtr(String ldaProcSaleNbrSlgCtr) {
		globalCtx.setString("ldaProcSaleNbrSlgCtr", ldaProcSaleNbrSlgCtr);
	}

	public String getWfLdaProcSaleNbrSlgCtr() {
		return globalCtx.getString("ldaProcSaleNbrSlgCtr");
	}

	public void setWfLdaProcSaleNbrStgCtr(String ldaProcSaleNbrStgCtr) {
		globalCtx.setString("ldaProcSaleNbrStgCtr", ldaProcSaleNbrStgCtr);
	}

	public String getWfLdaProcSaleNbrStgCtr() {
		return globalCtx.getString("ldaProcSaleNbrStgCtr");
	}

	public void setWfLdaProcSaleSeasonCent(String ldaProcSaleSeasonCent) {
		globalCtx.setString("ldaProcSaleSeasonCent", ldaProcSaleSeasonCent);
	}

	public String getWfLdaProcSaleSeasonCent() {
		return globalCtx.getString("ldaProcSaleSeasonCent");
	}

	public void setWfLdaProcSaleSeasonYear(String ldaProcSaleSeasonYear) {
		globalCtx.setString("ldaProcSaleSeasonYear", ldaProcSaleSeasonYear);
	}

	public String getWfLdaProcSaleSeasonYear() {
		return globalCtx.getString("ldaProcSaleSeasonYear");
	}

	public void setWfLdaWoolNumber(String ldaWoolNumber) {
		globalCtx.setString("ldaWoolNumber", ldaWoolNumber);
	}

	public String getWfLdaWoolNumber() {
		return globalCtx.getString("ldaWoolNumber");
	}

	public void setLclAwhRegionCode(String awhRegionCode) {
    	this.lclAwhRegionCode = awhRegionCode;
    }

    public String getLclAwhRegionCode() {
    	return lclAwhRegionCode;
    }
	public void setLclDateAdvised(LocalDate dateAdvised) {
    	this.lclDateAdvised = dateAdvised;
    }

    public LocalDate getLclDateAdvised() {
    	return lclDateAdvised;
    }
	public void setLclEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
    	this.lclEquipmentStatus = equipmentStatus;
    }

    public EquipmentStatusEnum getLclEquipmentStatus() {
    	return lclEquipmentStatus;
    }
	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
    	this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
    	return lclExitProgramOption;
    }
	public void setLclFirstPass(FirstPassEnum firstPass) {
    	this.lclFirstPass = firstPass;
    }

    public FirstPassEnum getLclFirstPass() {
    	return lclFirstPass;
    }
	public void setLclNewProgramDate(LocalDate newProgramDate) {
    	this.lclNewProgramDate = newProgramDate;
    }

    public LocalDate getLclNewProgramDate() {
    	return lclNewProgramDate;
    }
	public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
    	this.lclReadingRequiredDate = readingRequiredDate;
    }

    public LocalDate getLclReadingRequiredDate() {
    	return lclReadingRequiredDate;
    }
	public void setLclReadingType(ReadingTypeEnum readingType) {
    	this.lclReadingType = readingType;
    }

    public ReadingTypeEnum getLclReadingType() {
    	return lclReadingType;
    }
	public void setLclReadingValue(BigDecimal readingValue) {
    	this.lclReadingValue = readingValue;
    }

    public BigDecimal getLclReadingValue() {
    	return lclReadingValue;
    }
	public void setLclServiceValue(BigDecimal serviceValue) {
    	this.lclServiceValue = serviceValue;
    }

    public BigDecimal getLclServiceValue() {
    	return lclServiceValue;
    }

	public void set_SysReReadSubfileRecord(ReReadSubfileRecordEnum reReadSubfileRecord) {
    	_sysReReadSubfileRecord = reReadSubfileRecord;
    }

    public ReReadSubfileRecordEnum get_SysReReadSubfileRecord() {
    	return _sysReReadSubfileRecord;
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
    	_sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
    	return _sysRecordSelected;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}