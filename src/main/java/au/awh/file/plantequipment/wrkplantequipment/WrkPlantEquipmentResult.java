package au.awh.file.plantequipment.wrkplantequipment;

import java.io.Serializable;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END


/**
 * Result(s) for resource: WrkPlantEquipment (WSPLTWCDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class WrkPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -8477702064920442636L;

	private ExitProgramOptionEnum exitProgramOption = null;


	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}

}