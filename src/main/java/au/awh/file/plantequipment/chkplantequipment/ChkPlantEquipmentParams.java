package au.awh.file.plantequipment.chkplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.AwhBuisnessSegmentEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChkPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 968095647730584067L;

    private String plantEquipmentCode = "";
    private String awhRegionCode = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String centreCodeKey = "";

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
