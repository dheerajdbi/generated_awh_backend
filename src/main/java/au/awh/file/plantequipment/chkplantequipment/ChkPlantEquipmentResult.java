package au.awh.file.plantequipment.chkplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChkPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -1190475173924682838L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
