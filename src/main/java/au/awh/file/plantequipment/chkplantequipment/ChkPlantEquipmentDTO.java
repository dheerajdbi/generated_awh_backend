package au.awh.file.plantequipment.chkplantequipment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChkPlantEquipmentDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private AwhBuisnessSegmentEnum awhBuisnessSegment;
	private String awhRegionCode;
	private String centreCodeKey;
	private String plantEquipmentCode;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

}
