package au.awh.file.plantequipment.changeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChangePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChangePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = 6655475342080493159L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
