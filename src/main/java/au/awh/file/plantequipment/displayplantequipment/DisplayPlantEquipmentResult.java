package au.awh.file.plantequipment.displayplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: DisplayPlantEquipment (WSPLTW9D1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DisplayPlantEquipmentResult implements Serializable
{
    private static final long serialVersionUID = -364562592532902667L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}

