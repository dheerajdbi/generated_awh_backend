package au.awh.file.plantequipment.displayplantequipment;

import org.springframework.context.MessageSource;

// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Display Plant Equipment' (WSPLTW9D1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DisplayPlantEquipmentState extends DisplayPlantEquipmentDTO {
	private static final long serialVersionUID = 8310791211348019699L;

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	// Local fields
	private ErrorProcessingEnum lclErrorProcessing = null;
	private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED;

    // System fields
	private boolean conductKeyScreenConversation = true;
	private boolean _sysConductDetailScreenConversation = false;
    private boolean _sysErrorFound = false;

	public DisplayPlantEquipmentState() {

    }

    public void setLclErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.lclErrorProcessing = errorProcessing;
    }
 
	public ErrorProcessingEnum getLclErrorProcessing() {
        return lclErrorProcessing;
    }

    public void setLclReadingType(ReadingTypeEnum readingType) {
		this.lclReadingType = readingType;
    }
 
	public ReadingTypeEnum getLclReadingType() {
        return lclReadingType;
    }

    public boolean getConductKeyScreenConversation() {
    	return conductKeyScreenConversation;
    }

    public void setConductKeyScreenConversation(boolean conductKeyScreenConversation) {
    	this.conductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
		return _sysConductDetailScreenConversation;
	}

	public void set_SysConductDetailScreenConversation(boolean _sysConductDetailScreenConversation) {
		this._sysConductDetailScreenConversation = _sysConductDetailScreenConversation;
	}

	public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}
