package au.awh.file.plantequipment.displayplantequipment;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.BeanUtils;
import au.awh.file.plantequipment.PlantEquipment;
// generateStatusFieldImportStatements BEGIN 36
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Plant Equipment' (WSEQP) and function 'Display Plant Equipment' (WSPLTW9D1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DisplayPlantEquipmentDTO extends BaseDTO {
	private static final long serialVersionUID = -7163706372835226757L;

// Entries fields
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private String plantEquipmentCode = ""; // 56471
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // 56533
	private String visualEquipmentCode = ""; // 60172
	private String centreCodeKey = ""; // 1140
	private String centreNameDrv = ""; // 34917
	private String equipmentLeasor = ""; // 56525
	private EquipmentStatusEnum equipmentStatus = EquipmentStatusEnum._NOT_ENTERED; // 56583
	private String sflselPromptText = ""; // 332
	private String organisationNameDrv = ""; // 38795
	private String conditionName = ""; // 272
	private String conditionName2 = ""; // 31710
	private String equipmentDescription = ""; // 56472
	private String equipmentBarcode = ""; // 56473
	private String equipmentSerialNumber = ""; // 60161
	private LocalDate leaseComencmentDate = null; // 60165
	private LocalDate leaseExpireDate = null; // 60166
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO; // 60169
	private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO; // 60170
	private ReadingFrequencyEnum readingFrequency = ReadingFrequencyEnum._DAILY; // 60171
	private String conditionName1 = ""; // 32088
	private String equipmentComment = ""; // 56474
	private String equipmentTypeCode = ""; // 56496
	private String equipmentTypeDescDrv = ""; // 56549
	private EquipReadingUnitEnum equipReadingUnit = null; // 56498
	private BigDecimal readingValue = BigDecimal.ZERO; // 56486
	private LocalDate readingRequiredDate = null; // 56555
	private BigDecimal previousReadingValue = BigDecimal.ZERO; // 56560
	private ReadingTypeEnum readingType = ReadingTypeEnum._NOT_ENTERED; // 56488
	private LocalDate previousReadingDate = null; // 56590
	private BigDecimal totalUsage = BigDecimal.ZERO; // 60197
	private BigDecimal serviceReading = BigDecimal.ZERO; // 56588
	private LocalDate serviceReadingDate = null; // 56591
	private BigDecimal serviceValue = BigDecimal.ZERO; // 56561
// Param fields
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; // 29901
// Special fields
	private String _SysconditionName = ""; // 272


	public DisplayPlantEquipmentDTO() {

    }

	public DisplayPlantEquipmentDTO(PlantEquipment plantEquipment) {
		BeanUtils.copyProperties(plantEquipment, this);
	}

	public String getAwhRegionCode() {
        return awhRegionCode;
    }

	public void setAwhRegionCode (String awhRegionCode) {
        this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionNameDrv() {
        return awhRegionNameDrv;
    }

	public void setAwhRegionNameDrv (String awhRegionNameDrv) {
        this.awhRegionNameDrv = awhRegionNameDrv;
    }

	public String getPlantEquipmentCode() {
        return plantEquipmentCode;
    }

	public void setPlantEquipmentCode (String plantEquipmentCode) {
        this.plantEquipmentCode = plantEquipmentCode;
    }

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
        return awhBuisnessSegment;
    }

	public void setAwhBuisnessSegment (AwhBuisnessSegmentEnum awhBuisnessSegment) {
        this.awhBuisnessSegment = awhBuisnessSegment;
    }

	public String getVisualEquipmentCode() {
        return visualEquipmentCode;
    }

	public void setVisualEquipmentCode (String visualEquipmentCode) {
        this.visualEquipmentCode = visualEquipmentCode;
    }

	public String getCentreCodeKey() {
        return centreCodeKey;
    }

	public void setCentreCodeKey (String centreCodeKey) {
        this.centreCodeKey = centreCodeKey;
    }

	public String getCentreNameDrv() {
        return centreNameDrv;
    }

	public void setCentreNameDrv (String centreNameDrv) {
        this.centreNameDrv = centreNameDrv;
    }

	public String getEquipmentLeasor() {
        return equipmentLeasor;
    }

	public void setEquipmentLeasor (String equipmentLeasor) {
        this.equipmentLeasor = equipmentLeasor;
    }

	public EquipmentStatusEnum getEquipmentStatus() {
        return equipmentStatus;
    }

	public void setEquipmentStatus (EquipmentStatusEnum equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

	public String getSflselPromptText() {
        return sflselPromptText;
    }

	public void setSflselPromptText (String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

	public String getOrganisationNameDrv() {
        return organisationNameDrv;
    }

	public void setOrganisationNameDrv (String organisationNameDrv) {
        this.organisationNameDrv = organisationNameDrv;
    }

	public String getConditionName() {
        return conditionName;
    }

	public void setConditionName (String conditionName) {
        this.conditionName = conditionName;
    }

	public String getConditionName2() {
        return conditionName2;
    }

	public void setConditionName2 (String conditionName2) {
        this.conditionName2 = conditionName2;
    }

	public String getEquipmentDescription() {
        return equipmentDescription;
    }

	public void setEquipmentDescription (String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

	public String getEquipmentBarcode() {
        return equipmentBarcode;
    }

	public void setEquipmentBarcode (String equipmentBarcode) {
        this.equipmentBarcode = equipmentBarcode;
    }

	public String getEquipmentSerialNumber() {
        return equipmentSerialNumber;
    }

	public void setEquipmentSerialNumber (String equipmentSerialNumber) {
        this.equipmentSerialNumber = equipmentSerialNumber;
    }

	public LocalDate getLeaseComencmentDate() {
        return leaseComencmentDate;
    }

	public void setLeaseComencmentDate (LocalDate leaseComencmentDate) {
        this.leaseComencmentDate = leaseComencmentDate;
    }

	public LocalDate getLeaseExpireDate() {
        return leaseExpireDate;
    }

	public void setLeaseExpireDate (LocalDate leaseExpireDate) {
        this.leaseExpireDate = leaseExpireDate;
    }

	public BigDecimal getLeaseMonthlyRateS() {
        return leaseMonthlyRateS;
    }

	public void setLeaseMonthlyRateS (BigDecimal leaseMonthlyRateS) {
        this.leaseMonthlyRateS = leaseMonthlyRateS;
    }

	public BigDecimal getLeaseMaxTotalUnits() {
        return leaseMaxTotalUnits;
    }

	public void setLeaseMaxTotalUnits (BigDecimal leaseMaxTotalUnits) {
        this.leaseMaxTotalUnits = leaseMaxTotalUnits;
    }

	public ReadingFrequencyEnum getReadingFrequency() {
        return readingFrequency;
    }

	public void setReadingFrequency (ReadingFrequencyEnum readingFrequency) {
        this.readingFrequency = readingFrequency;
    }

	public String getConditionName1() {
        return conditionName1;
    }

	public void setConditionName1 (String conditionName1) {
        this.conditionName1 = conditionName1;
    }

	public String getEquipmentComment() {
        return equipmentComment;
    }

	public void setEquipmentComment (String equipmentComment) {
        this.equipmentComment = equipmentComment;
    }

	public String getEquipmentTypeCode() {
        return equipmentTypeCode;
    }

	public void setEquipmentTypeCode (String equipmentTypeCode) {
        this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeDescDrv() {
        return equipmentTypeDescDrv;
    }

	public void setEquipmentTypeDescDrv (String equipmentTypeDescDrv) {
        this.equipmentTypeDescDrv = equipmentTypeDescDrv;
    }

	public EquipReadingUnitEnum getEquipReadingUnit() {
        return equipReadingUnit;
    }

	public void setEquipReadingUnit (EquipReadingUnitEnum equipReadingUnit) {
        this.equipReadingUnit = equipReadingUnit;
    }

	public BigDecimal getReadingValue() {
        return readingValue;
    }

	public void setReadingValue (BigDecimal readingValue) {
        this.readingValue = readingValue;
    }

	public LocalDate getReadingRequiredDate() {
        return readingRequiredDate;
    }

	public void setReadingRequiredDate (LocalDate readingRequiredDate) {
        this.readingRequiredDate = readingRequiredDate;
    }

	public BigDecimal getPreviousReadingValue() {
        return previousReadingValue;
    }

	public void setPreviousReadingValue (BigDecimal previousReadingValue) {
        this.previousReadingValue = previousReadingValue;
    }

	public ReadingTypeEnum getReadingType() {
        return readingType;
    }

	public void setReadingType (ReadingTypeEnum readingType) {
        this.readingType = readingType;
    }

	public LocalDate getPreviousReadingDate() {
        return previousReadingDate;
    }

	public void setPreviousReadingDate (LocalDate previousReadingDate) {
        this.previousReadingDate = previousReadingDate;
    }

	public BigDecimal getTotalUsage() {
        return totalUsage;
    }

	public void setTotalUsage (BigDecimal totalUsage) {
        this.totalUsage = totalUsage;
    }

	public BigDecimal getServiceReading() {
        return serviceReading;
    }

	public void setServiceReading (BigDecimal serviceReading) {
        this.serviceReading = serviceReading;
    }

	public LocalDate getServiceReadingDate() {
        return serviceReadingDate;
    }

	public void setServiceReadingDate (LocalDate serviceReadingDate) {
        this.serviceReadingDate = serviceReadingDate;
    }

	public BigDecimal getServiceValue() {
        return serviceValue;
    }

	public void setServiceValue (BigDecimal serviceValue) {
        this.serviceValue = serviceValue;
    }

	public ExitProgramOptionEnum getExitProgramOption() {
        return exitProgramOption;
    }

 	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.exitProgramOption = exitProgramOption;
    }

	public String get_SysConditionName() {
        return _SysconditionName;
    }

 	public void set_SysConditionName(String conditionName) {
        this._SysconditionName = conditionName;
    }


    /**
     * Copies the fields of the Entity bean into the DTO bean.
     * @param plantEquipment PlantEquipment Entity bean\n")
     */
	public void setDtoFields(PlantEquipment plantEquipment) {
		BeanUtils.copyProperties(plantEquipment, this);
    }

 	/**
     * Copies the fields of the DTO bean into the Entity bean.
 	 * @param plantEquipment PlantEquipment Entity bean
     */
    public void setEntityFields(PlantEquipment plantEquipment) {
		BeanUtils.copyProperties(this, plantEquipment);
    }
}
