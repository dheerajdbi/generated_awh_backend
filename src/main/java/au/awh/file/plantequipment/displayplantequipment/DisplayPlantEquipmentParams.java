package au.awh.file.plantequipment.displayplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Params for resource: DisplayPlantEquipment (WSPLTW9D1K).
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
public class DisplayPlantEquipmentParams implements Serializable
{
    private static final long serialVersionUID = -2787035153047337617L;

	private String awhRegionCode = "";
	private String plantEquipmentCode = "";
	private boolean _sysConductDetailScreenConversation = false;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public boolean get_SysConductDetailScreenConversation() {
		return _sysConductDetailScreenConversation;
	}

	public void set_SysConductDetailScreenConversation(boolean _sysConductDetailScreenConversation) {
		this._sysConductDetailScreenConversation = _sysConductDetailScreenConversation;
	}
}

