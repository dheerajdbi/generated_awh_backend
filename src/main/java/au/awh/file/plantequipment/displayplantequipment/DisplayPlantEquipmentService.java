package au.awh.file.plantequipment.displayplantequipment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentDTO;
import au.awh.support.JobContext;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateService;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 29
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * DSPRCD Service controller for 'Display Plant Equipment' (WSPLTW9D1K) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator DSPRCDJavaControllerGenerator.kt
 */
@Service
public class DisplayPlantEquipmentService extends AbstractService<DisplayPlantEquipmentService, DisplayPlantEquipmentState>
{
    

	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

    @Autowired
	private MessageSource messageSource;

    
    @Autowired
    private GeteByReverseDateService geteByReverseDateService;
    
    
	public static final String SCREEN_KEY = "displayPlantEquipmentEntryPanel";
    public static final String SCREEN_DTL = "displayPlantEquipmentPanel";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", DisplayPlantEquipmentParams.class, this::executeService);
    private final Step keyScreenResponse = define("keyScreen", DisplayPlantEquipmentState.class, this::processResponseToKeyScreen);
    private final Step detailScreenResponse = define("detailScreen", DisplayPlantEquipmentState.class, this::processResponseToDetailScreen);

	//private final Step serviceGeteByReverseDate = define("serviceGeteByReverseDate",GeteByReverseDateResult.class, this::processServiceGeteByReverseDate);
	

    
    @Autowired
	public DisplayPlantEquipmentService() {
        super(DisplayPlantEquipmentService.class, DisplayPlantEquipmentState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(DisplayPlantEquipmentState state, DisplayPlantEquipmentParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);
        if(state.get_SysConductDetailScreenConversation()){
        	DisplayPlantEquipmentState displayPlantEquipmentState = new DisplayPlantEquipmentState();
        	BeanUtils.copyProperties(state, displayPlantEquipmentState);
        	stepResult =  processResponseToKeyScreen(state, displayPlantEquipmentState);
        } else{ 
        	stepResult = conductKeyScreenConversation(state);
        }

        return stepResult;
    }

    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductKeyScreenConversation(DisplayPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrLoadKeyScreen(state);

        if(shouldBypassKeyScreen(state)){
            stepResult = conductDetailScreenConversation(state);
        } else {
            stepResult = displayKeyScreenConversation(state);
        }

        return stepResult;
    }

    /**
     * Determine if SCREEN_KEY should be bypassed.
     * @param state - Service state class.
     * @return bypass - boolean.
     */
    private boolean shouldBypassKeyScreen(DisplayPlantEquipmentState state) {
        boolean bypass = false;

        //bypass if all key screens are set
        if(state.getAwhRegionCode() != null && !state.getAwhRegionCode().equals("") && state.getPlantEquipmentCode() != null && !state.getPlantEquipmentCode().equals("")){
            bypass = true;
        }

        return bypass;
    }

    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult displayKeyScreenConversation(DisplayPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;

     	if (state.getConductKeyScreenConversation()) {
            DisplayPlantEquipmentDTO model = new DisplayPlantEquipmentDTO();
            BeanUtils.copyProperties(state, model);
    		stepResult = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
    	}

    	return stepResult;
    }

    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponseToKeyScreen(DisplayPlantEquipmentState state, DisplayPlantEquipmentState model) {
    	StepResult stepResult = NO_ACTION;

        // Update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        } else {
            stepResult = validateKeyScreen(state);
            if (state.get_SysErrorFound()) {
            	stepResult = displayKeyScreenConversation(state);
                state.set_SysErrorFound(true);
            } else {
            	stepResult = conductDetailScreenConversation(state);
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_KEY validate key screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult validateKeyScreen(DisplayPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrValidateKeyScreen(state);

        PlantEquipmentId plantEquipmentId = new PlantEquipmentId(state.getAwhRegionCode(), state.getPlantEquipmentCode());
        PlantEquipment plantEquipment = plantEquipmentRepository.findById(plantEquipmentId).orElse(null);
        if (plantEquipment != null) {
            BeanUtils.copyProperties(plantEquipment, state);
        }
        else {
            state.set_SysErrorFound(true);
        }

        stepResult = usrLoadDetailScreenFromDbfRecord(state);

        return stepResult;
    }

    /**
     * SCREEN_DETAIL display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductDetailScreenConversation(DisplayPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;

		if (state.get_SysConductDetailScreenConversation()) {
            DisplayPlantEquipmentDTO model = new DisplayPlantEquipmentDTO();
            BeanUtils.copyProperties(state, model);
    		stepResult = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
		}

		return stepResult;
	}

    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponseToDetailScreen(DisplayPlantEquipmentState state, DisplayPlantEquipmentState model) {
    	StepResult stepResult = NO_ACTION;

    	// update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model,state);

        if (isExit(state.get_SysCmdKey())) {
             stepResult = closedown(state);
        } else if (isKeyScreen(state.get_SysCmdKey())) {
            stepResult = usrProcessKeyScreenRequest(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        } else if (isReset(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        } else if (isHelp(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        } else {
            stepResult = processDetailScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN_DETAIL  process detail screen and return to key screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processDetailScreen(DisplayPlantEquipmentState state) {
    	StepResult stepResult = NO_ACTION;

        checkInputField();
        if (state.get_SysErrorFound()) {
        	return closedown(state);
        } else {
        	stepResult = usrDetailScreenFunctionFields(state);
            stepResult = usrValidateDetailScreen(state);
            if (state.get_SysErrorFound()) {
            	return closedown(state);
            } else {
            	stepResult = usrPerformConfirmedAction(state);
            	if (state.get_SysErrorFound()) {
                	return closedown(state);
                } else {
                	stepResult = usrProcessCommandKeys(state);
                }
            }
        }

        stepResult = conductKeyScreenConversation(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(DisplayPlantEquipmentState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        DisplayPlantEquipmentResult params = new DisplayPlantEquipmentResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    private StepResult checkInputField() {
        return NO_ACTION;
    } 
    

    /**
	 * ==================================== Synon user point(s) ==========================================
     */

	/**
	 * USER: Initialize Program (Empty:1)
	 */
    private StepResult usrInitializeProgram(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Key Screen (Generated:440)
	 */
    private StepResult usrLoadKeyScreen(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 440 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Key Screen (Generated:415)
	 */
    private StepResult usrValidateKeyScreen(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// * Don't send any error messages on load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000091 ACT LCL.Error Processing = CND.Ignore
			dto.setLclErrorProcessing(ErrorProcessingEnum._IGNORE);
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Detail Screen from DBF Record (Generated:100)
	 */
    private StepResult usrLoadDetailScreenFromDbfRecord(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteByReverseDateParams geteByReverseDateParams = null;
			GeteByReverseDateResult geteByReverseDateResult = null;
			// DEBUG genFunctionCall BEGIN 1000299 ACT LCL.Reading Type = CND.Initial reading
			dto.setLclReadingType(ReadingTypeEnum._INITIAL_READING);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000245 ACT GetE By reverse date - Equipment Readings  * Initial
			geteByReverseDateParams = new GeteByReverseDateParams();
			geteByReverseDateResult = new GeteByReverseDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteByReverseDateParams);
			geteByReverseDateParams.setAwhRegionCode(dto.getAwhRegionCode());
			geteByReverseDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
			geteByReverseDateParams.setReadingType(dto.getLclReadingType());
			//geteByReverseDateParams.setErrorProcessing("2");
			//geteByReverseDateParams.setGetRecordOption("I");
			stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
			//geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
			dto.setLclReadingType(geteByReverseDateResult.getReadingType());
			dto.setReadingValue(geteByReverseDateResult.getReadingValue());
			dto.setReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
			// DEBUG genFunctionCall END
			//if (dto.getReadingRequiredDate().equals(LocalDate.parse("0001-01-01", DateTimeFormatter.BASIC_ISO_DATE))) {
				// DTL.Reading Required Date is Not Entered
				// DEBUG genFunctionCall BEGIN 1000305 ACT LCL.Reading Type = CND.Transfer In Reading
				dto.setLclReadingType(ReadingTypeEnum._TRANSFER_IN_READING);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000266 ACT GetE By reverse date - Equipment Readings  * Initial
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(dto.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				//geteByReverseDateParams.setErrorProcessing("2");
				//geteByReverseDateParams.setGetRecordOption("I");
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				//geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				//dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				dto.setReadingValue(geteByReverseDateResult.getReadingValue());
				dto.setReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
				// DEBUG genFunctionCall END
			//}
			// DEBUG genFunctionCall BEGIN 1000311 ACT DTL.Reading Type = CND.Any
			dto.setReadingType(ReadingTypeEnum._ANY);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000227 ACT GetE By reverse date - Equipment Readings  * Previous
			geteByReverseDateParams = new GeteByReverseDateParams();
			geteByReverseDateResult = new GeteByReverseDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteByReverseDateParams);
			geteByReverseDateParams.setAwhRegionCode(dto.getAwhRegionCode());
			geteByReverseDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
			geteByReverseDateParams.setReadingType(dto.getReadingType());
			//geteByReverseDateParams.setErrorProcessing("2");
			//geteByReverseDateParams.setGetRecordOption("I");
			stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
			//geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			//dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
			dto.setReadingType(geteByReverseDateResult.getReadingType());
			dto.setPreviousReadingValue(geteByReverseDateResult.getReadingValue());
			dto.setPreviousReadingDate(geteByReverseDateResult.getReadingRequiredDate());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000315 ACT LCL.Reading Type = CND.Service Reading
			dto.setLclReadingType(ReadingTypeEnum._SERVICE_READING);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000209 ACT GetE By reverse date - Equipment Readings  * Service
			geteByReverseDateParams = new GeteByReverseDateParams();
			geteByReverseDateResult = new GeteByReverseDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteByReverseDateParams);
			geteByReverseDateParams.setAwhRegionCode(dto.getAwhRegionCode());
			geteByReverseDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
			geteByReverseDateParams.setReadingType(dto.getLclReadingType());
			//geteByReverseDateParams.setErrorProcessing("2");
			//geteByReverseDateParams.setGetRecordOption("I");
			stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
			//geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			//dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
			dto.setLclReadingType(geteByReverseDateResult.getReadingType());
			dto.setServiceReading(geteByReverseDateResult.getReadingValue());
			dto.setServiceReadingDate(geteByReverseDateResult.getReadingRequiredDate());
			// DEBUG genFunctionCall END
			if ((dto.getReadingValue() != BigDecimal.ZERO) && (dto.getPreviousReadingValue() != BigDecimal.ZERO)) {
				// DEBUG genFunctionCall BEGIN 1000360 ACT DTL.Total Usage = DTL.Previous Reading Value - DTL.Reading Value
				dto.setTotalUsage(dto.getPreviousReadingValue().subtract(dto.getReadingValue()));
				// DEBUG genFunctionCall END
			}
			if ((dto.getServiceReading() != BigDecimal.ZERO) && (dto.getPreviousReadingValue() != BigDecimal.ZERO)) {
				// DEBUG genFunctionCall BEGIN 1000291 ACT DTL.Service Value = DTL.Previous Reading Value - DTL.Service reading
				dto.setServiceValue(dto.getPreviousReadingValue().subtract(dto.getServiceReading()));
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Key Screen Request (Generated:466)
	 */
    private StepResult usrProcessKeyScreenRequest(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 466 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Detail Screen Function Fields (Generated:425)
	 */
    private StepResult usrDetailScreenFunctionFields(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000109 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000109
			// DEBUG X2EActionDiagramElement 1000109 ACT     AWH region Name Drv      *FIELD                                             DTLA
			// DEBUG X2EActionDiagramElement 1000110 PAR DTL 
			// DEBUG X2EActionDiagramElement 1000111 PAR DTL 
			// DEBUG genFunctionCall END
			if (!dto.getCentreCodeKey().equals("")) {
				// DTL.Centre Code           KEY is Entered
				// DEBUG genFunctionCall BEGIN 1000113 ACT Centre Name Drv          *FIELD                                             DTLC
				// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             DTLC 1000113
				// DEBUG X2EActionDiagramElement 1000113 ACT     Centre Name Drv          *FIELD                                             DTLC
				// DEBUG X2EActionDiagramElement 1000114 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000115 PAR DTL 
				// DEBUG genFunctionCall END
			}
			if (!dto.getEquipmentLeasor().equals("")) {
				// DTL.Equipment Leasor is Entered
				// DEBUG genFunctionCall BEGIN 1000117 ACT Organisation Name Drv    *FIELD                                             DTLO
				// TODO: NEED TO SUPPORT *FIELD Organisation Name Drv    *FIELD                                             DTLO 1000117
				// DEBUG X2EActionDiagramElement 1000117 ACT     Organisation Name Drv    *FIELD                                             DTLO
				// DEBUG X2EActionDiagramElement 1000118 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000119 PAR DTL 
				// DEBUG genFunctionCall END
			}
			if (!dto.getEquipmentTypeCode().equals("")) {
				// DTL.Equipment Type Code is Entered
				// DEBUG genFunctionCall BEGIN 1000121 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
				// TODO: NEED TO SUPPORT *FIELD Equipment Type Desc Drv  *FIELD                                             DTLE 1000121
				// DEBUG X2EActionDiagramElement 1000121 ACT     Equipment Type Desc Drv  *FIELD                                             DTLE
				// DEBUG X2EActionDiagramElement 1000122 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000123 PAR DTL 
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000134 ACT DTL.*Condition name = Condition name of DTL.AWH Buisness Segment
			dto.set_SysConditionName(dto.getAwhBuisnessSegment().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000323 ACT DTL.Condition Name = Condition name of DTL.Equip Reading Unit
			dto.setConditionName(dto.getEquipReadingUnit().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000128 ACT DTL.Condition Name 1 = Condition name of DTL.Reading Frequency
			dto.setConditionName1(dto.getReadingFrequency().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000331 ACT DTL.Condition Name 2 = Condition name of DTL.Equipment Status
			dto.setConditionName2(dto.getEquipmentStatus().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Detail Screen (Generated:199)
	 */
    private StepResult usrValidateDetailScreen(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000375 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000379 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Perform Confirmed Action (Generated:455)
	 */
    private StepResult usrPerformConfirmedAction(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000382 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum._NORMAL);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000386 ACT Exit program - return code CND.*Normal
			// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:389)
	 */
    private StepResult usrProcessCommandKeys(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 389 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:30)
	 */
    private StepResult usrExitProgramProcessing(DisplayPlantEquipmentState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			if (isExit(dto.get_SysCmdKey())) {
				// KEY.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000067 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    } 
}
