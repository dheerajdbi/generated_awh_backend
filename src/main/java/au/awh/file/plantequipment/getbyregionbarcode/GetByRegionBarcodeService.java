package au.awh.file.plantequipment.getbyregionbarcode;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get By Region barcode' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator
 */
@Service
public class GetByRegionBarcodeService extends AbstractService<GetByRegionBarcodeService, GetByRegionBarcodeDTO>
{
    private final Step execute = define("execute", GetByRegionBarcodeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	

    @Autowired
    public GetByRegionBarcodeService()
    {
        super(GetByRegionBarcodeService.class, GetByRegionBarcodeDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetByRegionBarcodeParams params) throws ServiceException {
        GetByRegionBarcodeDTO dto = new GetByRegionBarcodeDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetByRegionBarcodeDTO dto, GetByRegionBarcodeParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<PlantEquipment> plantEquipmentList = plantEquipmentRepository.findAllByIdAwhRegionCodeAndEquipmentBarcode(dto.getAwhRegionCode(), dto.getEquipmentBarcode());
		if (!plantEquipmentList.isEmpty()) {
			for (PlantEquipment plantEquipment : plantEquipmentList) {
				processDataRecord(dto, plantEquipment);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetByRegionBarcodeResult result = new GetByRegionBarcodeResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetByRegionBarcodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetByRegionBarcodeDTO dto, PlantEquipment plantEquipment) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END
		// * Assume this is a FIFO partial key access.
		// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetByRegionBarcodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
		dto.setAwhBuisnessSegment(null);
		dto.setCentreCodeKey("");
		dto.setEquipmentDescription("");
		dto.setEquipmentComment("");
		dto.setEquipmentTypeCode("");
		dto.setEquipReadingUnit(null);
		dto.setEquipmentLeasor("");
		dto.setEquipmentStatus(EquipmentStatusEnum._ACTIVE);
		dto.setPlantEquipmentCode("");
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetByRegionBarcodeDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
