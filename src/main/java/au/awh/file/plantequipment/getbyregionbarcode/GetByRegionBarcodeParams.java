package au.awh.file.plantequipment.getbyregionbarcode;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetByRegionBarcode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetByRegionBarcodeParams implements Serializable
{
	private static final long serialVersionUID = 5034289566148676415L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private String equipmentBarcode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}
	
	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}
}
