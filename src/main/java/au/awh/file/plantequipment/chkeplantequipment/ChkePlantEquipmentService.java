package au.awh.file.plantequipment.chkeplantequipment;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=chkePlantEquipment (1878242) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.chkplantequipment.ChkPlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.chkplantequipment.ChkPlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CheckRecordOptionEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.chkplantequipment.ChkPlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.chkplantequipment.ChkPlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ChkE Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ChkePlantEquipmentService extends AbstractService<ChkePlantEquipmentService, ChkePlantEquipmentDTO>
{
	private final Step execute = define("execute", ChkePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private ChkPlantEquipmentService chkPlantEquipmentService;

	//private final Step serviceChkPlantEquipment = define("serviceChkPlantEquipment",ChkPlantEquipmentResult.class, this::processServiceChkPlantEquipment);
	
	@Autowired
	public ChkePlantEquipmentService() {
		super(ChkePlantEquipmentService.class, ChkePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChkePlantEquipmentParams params) throws ServiceException {
		ChkePlantEquipmentDTO dto = new ChkePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChkePlantEquipmentDTO dto, ChkePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ChkPlantEquipmentParams chkPlantEquipmentParams = null;
		ChkPlantEquipmentResult chkPlantEquipmentResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Chk Plant Equipment - Plant Equipment  *
		chkPlantEquipmentParams = new ChkPlantEquipmentParams();
		chkPlantEquipmentResult = new ChkPlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, chkPlantEquipmentParams);
		chkPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		chkPlantEquipmentParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		chkPlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
		chkPlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		stepResult = chkPlantEquipmentService.execute(chkPlantEquipmentParams);
		chkPlantEquipmentResult = (ChkPlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(chkPlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (dto.getCheckRecordOption() == CheckRecordOptionEnum._RECORD_DOES_NOT_EXIST) {
			// PAR.Check Record Option is Record does not exist
			if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000259 ACT PGM.*Return code = CND.*Record already exists
				dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000251 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
			}
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS) {
				// PGM.*Return code is *Record already exists
				// DEBUG genFunctionCall BEGIN 1000266 ACT Send error message - '*Record already exists'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1001646) EXCINTFUN 1878242 PlantEquipment.chkePlantEquipment 
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000201 ACT Send error message - '*Record not found'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1001645) EXCINTFUN 1878242 PlantEquipment.chkePlantEquipment 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000209 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878242 PlantEquipment.chkePlantEquipment 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000217 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000224 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000229 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		ChkePlantEquipmentResult result = new ChkePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ChkPlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChkPlantEquipment(ChkePlantEquipmentDTO dto, ChkPlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChkePlantEquipmentParams chkePlantEquipmentParams = new ChkePlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chkePlantEquipmentParams);
//        stepResult = StepResult.callService(ChkePlantEquipmentService.class, chkePlantEquipmentParams);
//
//        return stepResult;
//    }
//
}
