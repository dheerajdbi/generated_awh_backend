package au.awh.file.plantequipment.chkeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChkePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -6047571018598169498L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
