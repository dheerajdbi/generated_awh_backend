package au.awh.file.plantequipment.chkeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CheckRecordOptionEnum;
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChkePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkePlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 4063320845359596059L;

    private String plantEquipmentCode = "";
    private String awhRegionCode = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String centreCodeKey = "";
    private ErrorProcessingEnum errorProcessing = null;
    private CheckRecordOptionEnum checkRecordOption = null;

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public CheckRecordOptionEnum getCheckRecordOption() {
		return checkRecordOption;
	}
	
	public void setCheckRecordOption(CheckRecordOptionEnum checkRecordOption) {
		this.checkRecordOption = checkRecordOption;
	}
	
	public void setCheckRecordOption(String checkRecordOption) {
		setCheckRecordOption(CheckRecordOptionEnum.valueOf(checkRecordOption));
	}
}
