package au.awh.file.plantequipment.bldprteequipmentreading;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=bldprteEquipmentReading (1878936) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.bldprtplantequipment.BldprtPlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.bldprtplantequipment.BldprtPlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.plantequipment.bldprtplantequipment.BldprtPlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.bldprtplantequipment.BldprtPlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'BldPrtE Equipment Reading' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class BldprteEquipmentReadingService extends AbstractService<BldprteEquipmentReadingService, BldprteEquipmentReadingDTO>
{
	private final Step execute = define("execute", BldprteEquipmentReadingParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private BldprtPlantEquipmentService bldprtPlantEquipmentService;

	//private final Step serviceBldprtPlantEquipment = define("serviceBldprtPlantEquipment",BldprtPlantEquipmentResult.class, this::processServiceBldprtPlantEquipment);
	
	@Autowired
	public BldprteEquipmentReadingService() {
		super(BldprteEquipmentReadingService.class, BldprteEquipmentReadingDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(BldprteEquipmentReadingParams params) throws ServiceException {
		BldprteEquipmentReadingDTO dto = new BldprteEquipmentReadingDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(BldprteEquipmentReadingDTO dto, BldprteEquipmentReadingParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		BldprtPlantEquipmentResult bldprtPlantEquipmentResult = null;
		BldprtPlantEquipmentParams bldprtPlantEquipmentParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// * NOTE: Add your own PRTFIL function here.
		// DEBUG genFunctionCall BEGIN 1000153 ACT BldPrt Plant Equipment - Plant Equipment  *
		bldprtPlantEquipmentParams = new BldprtPlantEquipmentParams();
		bldprtPlantEquipmentResult = new BldprtPlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, bldprtPlantEquipmentParams);
		bldprtPlantEquipmentParams.setOutputQueue(dto.getOutputQueue());
		bldprtPlantEquipmentParams.setOutputQueueLibrary(dto.getOutputQueueLibrary());
		bldprtPlantEquipmentParams.setCopies(dto.getCopies());
		bldprtPlantEquipmentParams.setForms(dto.getForms());
		bldprtPlantEquipmentParams.setHoldClFormatyesno(dto.getHoldClFormatyesno());
		bldprtPlantEquipmentParams.setSaveClFormatyesno(dto.getSaveClFormatyesno());
		bldprtPlantEquipmentParams.setOverflowLineNumberText(dto.getOverflowLineNumberText());
		bldprtPlantEquipmentParams.setReadingRequiredDate(dto.getReadingRequiredDate());
		bldprtPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		bldprtPlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
		bldprtPlantEquipmentParams.setCheckForHolidays(dto.getCheckForHolidays());
		stepResult = bldprtPlantEquipmentService.execute(bldprtPlantEquipmentParams);
		if (stepResult != NO_ACTION) {
			bldprtPlantEquipmentResult = (BldprtPlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(bldprtPlantEquipmentResult.get_SysReturnCode());
			dto.setOutputQueue(bldprtPlantEquipmentResult.getOutputQueue());
			dto.setOutputQueueLibrary(bldprtPlantEquipmentResult.getOutputQueueLibrary());
			dto.setCopies(bldprtPlantEquipmentResult.getCopies());
			dto.setForms(bldprtPlantEquipmentResult.getForms());
			dto.setHoldClFormatyesno(bldprtPlantEquipmentResult.getHoldClFormatyesno());
			dto.setSaveClFormatyesno(bldprtPlantEquipmentResult.getSaveClFormatyesno());
			dto.setOverflowLineNumberText(bldprtPlantEquipmentResult.getOverflowLineNumberText());
		}
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878936 PlantEquipment.bldprteEquipmentReading 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		BldprteEquipmentReadingResult result = new BldprteEquipmentReadingResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * BldprtPlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceBldprtPlantEquipment(BldprteEquipmentReadingDTO dto, BldprtPlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        BldprteEquipmentReadingParams bldprteEquipmentReadingParams = new BldprteEquipmentReadingParams();
//        BeanUtils.copyProperties(dto, bldprteEquipmentReadingParams);
//        stepResult = StepResult.callService(BldprteEquipmentReadingService.class, bldprteEquipmentReadingParams);
//
//        return stepResult;
//    }
//
}
