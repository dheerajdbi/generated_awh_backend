package au.awh.file.plantequipment.getplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 274899761123183379L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
}
