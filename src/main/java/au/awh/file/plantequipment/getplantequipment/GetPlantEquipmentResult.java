package au.awh.file.plantequipment.getplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 16
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -5383208227744886868L;

	private ReturnCodeEnum _sysReturnCode;
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // AwhBuisnessSegmentEnum 56533
	private String centreCodeKey = ""; // String 1140
	private String equipmentDescription = ""; // String 56472
	private String equipmentBarcode = ""; // String 56473
	private String equipmentComment = ""; // String 56474
	private String equipmentTypeCode = ""; // String 56496
	private EquipReadingUnitEnum equipReadingUnit = null; // EquipReadingUnitEnum 56498
	private String equipmentLeasor = ""; // String 56525
	private EquipmentStatusEnum equipmentStatus = null; // EquipmentStatusEnum 56583
	private String equipmentSerialNumber = ""; // String 60161
	private LocalDate leaseComencmentDate = null; // LocalDate 60165
	private LocalDate leaseExpireDate = null; // LocalDate 60166
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO; // BigDecimal 60169
	private long leaseMaxTotalUnits = 0L; // long 60170
	private ReadingFrequencyEnum readingFrequency = null; // ReadingFrequencyEnum 60171
	private String visualEquipmentCode = ""; // String 60172

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public String getEquipmentDescription() {
		return equipmentDescription;
	}
	
	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}
	
	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}
	
	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}
	
	public String getEquipmentComment() {
		return equipmentComment;
	}
	
	public void setEquipmentComment(String equipmentComment) {
		this.equipmentComment = equipmentComment;
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipReadingUnit(String equipReadingUnit) {
		setEquipReadingUnit(EquipReadingUnitEnum.valueOf(equipReadingUnit));
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentStatus(String equipmentStatus) {
		setEquipmentStatus(EquipmentStatusEnum.valueOf(equipmentStatus));
	}
	
	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}
	
	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}
	
	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}
	
	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}
	
	public void setLeaseComencmentDate(String leaseComencmentDate) {
		setLeaseComencmentDate(new LocalDateConverter().convert(leaseComencmentDate));
	}
	
	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}
	
	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}
	
	public void setLeaseExpireDate(String leaseExpireDate) {
		setLeaseExpireDate(new LocalDateConverter().convert(leaseExpireDate));
	}
	
	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}
	
	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}
	
	public void setLeaseMonthlyRateS(String leaseMonthlyRateS) {
		setLeaseMonthlyRateS(new BigDecimal(leaseMonthlyRateS));
	}
	
	public long getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}
	
	public void setLeaseMaxTotalUnits(long leaseMaxTotalUnits) {
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
	}
	
	public void setLeaseMaxTotalUnits(String leaseMaxTotalUnits) {
		setLeaseMaxTotalUnits(Long.parseLong(leaseMaxTotalUnits));
	}
	
	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}
	
	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}
	
	public void setReadingFrequency(String readingFrequency) {
		setReadingFrequency(ReadingFrequencyEnum.valueOf(readingFrequency));
	}
	
	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}
	
	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
