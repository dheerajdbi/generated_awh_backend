package au.awh.file.plantequipment.getebyregionbarcode;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteByRegionBarcode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteByRegionBarcodeParams implements Serializable
{
	private static final long serialVersionUID = 7841221614213090492L;

    private String awhRegionCode = "";
    private String equipmentBarcode = "";
    private ErrorProcessingEnum errorProcessing = null;
    private GetRecordOptionEnum getRecordOption = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}
	
	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public GetRecordOptionEnum getGetRecordOption() {
		return getRecordOption;
	}
	
	public void setGetRecordOption(GetRecordOptionEnum getRecordOption) {
		this.getRecordOption = getRecordOption;
	}
	
	public void setGetRecordOption(String getRecordOption) {
		setGetRecordOption(GetRecordOptionEnum.valueOf(getRecordOption));
	}
}
