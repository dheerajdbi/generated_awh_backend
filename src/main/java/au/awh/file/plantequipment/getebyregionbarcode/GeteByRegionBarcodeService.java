package au.awh.file.plantequipment.getebyregionbarcode;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=geteByRegionBarcode (1880733) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.getbyregionbarcode.GetByRegionBarcodeService;
// imports for DTO
import au.awh.file.plantequipment.getbyregionbarcode.GetByRegionBarcodeDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.getbyregionbarcode.GetByRegionBarcodeParams;
// imports for Results
import au.awh.file.plantequipment.getbyregionbarcode.GetByRegionBarcodeResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE By Region Barcode' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteByRegionBarcodeService extends AbstractService<GeteByRegionBarcodeService, GeteByRegionBarcodeDTO>
{
	private final Step execute = define("execute", GeteByRegionBarcodeParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private GetByRegionBarcodeService getByRegionBarcodeService;

	//private final Step serviceGetByRegionBarcode = define("serviceGetByRegionBarcode",GetByRegionBarcodeResult.class, this::processServiceGetByRegionBarcode);
	
	@Autowired
	public GeteByRegionBarcodeService() {
		super(GeteByRegionBarcodeService.class, GeteByRegionBarcodeDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteByRegionBarcodeParams params) throws ServiceException {
		GeteByRegionBarcodeDTO dto = new GeteByRegionBarcodeDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteByRegionBarcodeDTO dto, GeteByRegionBarcodeParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetByRegionBarcodeResult getByRegionBarcodeResult = null;
		GetByRegionBarcodeParams getByRegionBarcodeParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get By Region barcode - Plant Equipment  *
		getByRegionBarcodeParams = new GetByRegionBarcodeParams();
		getByRegionBarcodeResult = new GetByRegionBarcodeResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getByRegionBarcodeParams);
		getByRegionBarcodeParams.setAwhRegionCode(dto.getAwhRegionCode());
		getByRegionBarcodeParams.setEquipmentBarcode(dto.getEquipmentBarcode());
		stepResult = getByRegionBarcodeService.execute(getByRegionBarcodeParams);
		getByRegionBarcodeResult = (GetByRegionBarcodeResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getByRegionBarcodeResult.get_SysReturnCode());
		dto.setPlantEquipmentCode(getByRegionBarcodeResult.getPlantEquipmentCode());
		dto.setAwhBuisnessSegment(getByRegionBarcodeResult.getAwhBuisnessSegment());
		dto.setCentreCodeKey(getByRegionBarcodeResult.getCentreCodeKey());
		dto.setEquipmentDescription(getByRegionBarcodeResult.getEquipmentDescription());
		dto.setEquipmentComment(getByRegionBarcodeResult.getEquipmentComment());
		dto.setEquipmentTypeCode(getByRegionBarcodeResult.getEquipmentTypeCode());
		dto.setEquipReadingUnit(getByRegionBarcodeResult.getEquipReadingUnit());
		dto.setEquipmentLeasor(getByRegionBarcodeResult.getEquipmentLeasor());
		dto.setEquipmentStatus(getByRegionBarcodeResult.getEquipmentStatus());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				if (!(dto.getGetRecordOption() == GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND)) {
					// NOT PAR.Get Record Option is No message on not found
					// * NOTE: Insert <file> NF message here.
					// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Plant Equipment        NF'
					// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878065) EXCINTFUN 1880733 PlantEquipment.geteByRegionBarcode 
					// DEBUG genFunctionCall END
				}
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1880733 PlantEquipment.geteByRegionBarcode 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteByRegionBarcodeResult result = new GeteByRegionBarcodeResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetByRegionBarcodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetByRegionBarcode(GeteByRegionBarcodeDTO dto, GetByRegionBarcodeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteByRegionBarcodeParams geteByRegionBarcodeParams = new GeteByRegionBarcodeParams();
//        BeanUtils.copyProperties(dto, geteByRegionBarcodeParams);
//        stepResult = StepResult.callService(GeteByRegionBarcodeService.class, geteByRegionBarcodeParams);
//
//        return stepResult;
//    }
//
}
