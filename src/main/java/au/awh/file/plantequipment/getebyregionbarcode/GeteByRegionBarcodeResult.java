package au.awh.file.plantequipment.getebyregionbarcode;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 10
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteByRegionBarcode ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteByRegionBarcodeResult implements Serializable
{
	private static final long serialVersionUID = -3888722225695797534L;

	private ReturnCodeEnum _sysReturnCode;
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null; // AwhBuisnessSegmentEnum 56533
	private String centreCodeKey = ""; // String 1140
	private String equipmentDescription = ""; // String 56472
	private String equipmentComment = ""; // String 56474
	private String equipmentTypeCode = ""; // String 56496
	private EquipReadingUnitEnum equipReadingUnit = null; // EquipReadingUnitEnum 56498
	private String equipmentLeasor = ""; // String 56525
	private EquipmentStatusEnum equipmentStatus = null; // EquipmentStatusEnum 56583
	private String plantEquipmentCode = ""; // String 56471
	private String equipmentSerialNumber = ""; // String 60161

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public String getEquipmentDescription() {
		return equipmentDescription;
	}
	
	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}
	
	public String getEquipmentComment() {
		return equipmentComment;
	}
	
	public void setEquipmentComment(String equipmentComment) {
		this.equipmentComment = equipmentComment;
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipReadingUnit(String equipReadingUnit) {
		setEquipReadingUnit(EquipReadingUnitEnum.valueOf(equipReadingUnit));
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentStatus(String equipmentStatus) {
		setEquipmentStatus(EquipmentStatusEnum.valueOf(equipmentStatus));
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}
	
	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
