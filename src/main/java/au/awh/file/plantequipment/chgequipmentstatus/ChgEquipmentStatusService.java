package au.awh.file.plantequipment.chgequipmentstatus;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CHGOBJ Service controller for 'Chg Equipment Status' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class ChgEquipmentStatusService extends AbstractService<ChgEquipmentStatusService, ChgEquipmentStatusDTO>
{
	private final Step execute = define("execute", ChgEquipmentStatusParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	
	@Autowired
	public ChgEquipmentStatusService() {
		super(ChgEquipmentStatusService.class, ChgEquipmentStatusDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgEquipmentStatusParams params) throws ServiceException {
		ChgEquipmentStatusDTO dto = new ChgEquipmentStatusDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgEquipmentStatusDTO dto, ChgEquipmentStatusParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		PlantEquipmentId plantEquipmentId = new PlantEquipmentId();
		plantEquipmentId.setAwhRegionCode(dto.getAwhRegionCode());
		plantEquipmentId.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		PlantEquipment plantEquipment = plantEquipmentRepository.findById(plantEquipmentId).orElse(null);
		if (plantEquipment == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, plantEquipment);
			plantEquipment.setAwhRegionCode(dto.getAwhRegionCode());
			plantEquipment.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			plantEquipment.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
			plantEquipment.setCentreCodeKey(dto.getCentreCodeKey());
			plantEquipment.setEquipmentDescription(dto.getEquipmentDescription());
			plantEquipment.setEquipmentBarcode(dto.getEquipmentBarcode());
			plantEquipment.setEquipmentComment(dto.getEquipmentComment());
			plantEquipment.setEquipmentTypeCode(dto.getEquipmentTypeCode());
			plantEquipment.setEquipReadingUnit(dto.getEquipReadingUnit());
			plantEquipment.setEquipmentLeasor(dto.getEquipmentLeasor());
			plantEquipment.setEquipmentStatus(dto.getEquipmentStatus());
			plantEquipment.setEquipmentSerialNumber(dto.getEquipmentSerialNumber());
			plantEquipment.setLeaseComencmentDate(dto.getLeaseComencmentDate());
			plantEquipment.setLeaseExpireDate(dto.getLeaseExpireDate());
			plantEquipment.setLeaseMonthlyRateS(dto.getLeaseMonthlyRateS());
			plantEquipment.setLeaseMaxTotalUnits(dto.getLeaseMaxTotalUnits());
			plantEquipment.setReadingFrequency(dto.getReadingFrequency());
			plantEquipment.setVisualEquipmentCode(dto.getVisualEquipmentCode());

			processingBeforeDataUpdate(dto, plantEquipment);

			try {
				plantEquipmentRepository.saveAndFlush(plantEquipment);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		ChgEquipmentStatusResult result = new ChgEquipmentStatusResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(ChgEquipmentStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		
		// Unprocessed SUB 8 -
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(ChgEquipmentStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		
		// Unprocessed SUB 43 -
		return stepResult;
	}

	private StepResult processingAfterDataRead(ChgEquipmentStatusDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(ChgEquipmentStatusDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		
		// Unprocessed SUB 10 -
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(ChgEquipmentStatusDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		
		// Unprocessed SUB 23 -
		return stepResult;
	}


}
