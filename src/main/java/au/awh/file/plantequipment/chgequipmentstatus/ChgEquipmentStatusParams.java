package au.awh.file.plantequipment.chgequipmentstatus;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.EquipmentStatusEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgEquipmentStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgEquipmentStatusParams implements Serializable
{
	private static final long serialVersionUID = 5194249439872370881L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private EquipmentStatusEnum equipmentStatus = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentStatus(String equipmentStatus) {
		setEquipmentStatus(EquipmentStatusEnum.valueOf(equipmentStatus));
	}
}
