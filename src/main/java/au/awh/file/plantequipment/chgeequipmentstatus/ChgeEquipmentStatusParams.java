package au.awh.file.plantequipment.chgeequipmentstatus;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgeEquipmentStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeEquipmentStatusParams implements Serializable
{
	private static final long serialVersionUID = -7116199840362639783L;

    private ErrorProcessingEnum errorProcessing = null;
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private EquipmentStatusEnum equipmentStatus = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentStatus(String equipmentStatus) {
		setEquipmentStatus(EquipmentStatusEnum.valueOf(equipmentStatus));
	}
}
