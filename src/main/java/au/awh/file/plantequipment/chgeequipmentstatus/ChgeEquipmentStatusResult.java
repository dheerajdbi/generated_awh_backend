package au.awh.file.plantequipment.chgeequipmentstatus;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeEquipmentStatus ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeEquipmentStatusResult implements Serializable
{
	private static final long serialVersionUID = 7077783039954209001L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
