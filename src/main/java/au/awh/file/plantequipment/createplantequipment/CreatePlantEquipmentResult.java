package au.awh.file.plantequipment.createplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CreatePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreatePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = 2782241096136278693L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
