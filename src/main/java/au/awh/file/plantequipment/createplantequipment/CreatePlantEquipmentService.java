package au.awh.file.plantequipment.createplantequipment;

// CreateObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * CRTOBJ Service controller for 'Create Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  CreateObjectFunction.kt
 */
@Service
public class CreatePlantEquipmentService  extends AbstractService<CreatePlantEquipmentService, CreatePlantEquipmentDTO>
{
	private final Step execute = define("execute", CreatePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	
	@Autowired
	public CreatePlantEquipmentService() {
		super(CreatePlantEquipmentService.class, CreatePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CreatePlantEquipmentParams params) throws ServiceException {
		CreatePlantEquipmentDTO dto = new CreatePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CreatePlantEquipmentDTO dto, CreatePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		PlantEquipment plantEquipment = new PlantEquipment();
		plantEquipment.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		plantEquipment.setEquipmentDescription(dto.getEquipmentDescription());
		plantEquipment.setEquipmentBarcode(dto.getEquipmentBarcode());
		plantEquipment.setEquipmentComment(dto.getEquipmentComment());
		plantEquipment.setAwhRegionCode(dto.getAwhRegionCode());
		plantEquipment.setCentreCodeKey(dto.getCentreCodeKey());
		plantEquipment.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		plantEquipment.setEquipReadingUnit(dto.getEquipReadingUnit());
		plantEquipment.setEquipmentLeasor(dto.getEquipmentLeasor());
		plantEquipment.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
		plantEquipment.setEquipmentStatus(dto.getEquipmentStatus());
		plantEquipment.setEquipmentSerialNumber(dto.getEquipmentSerialNumber());
		plantEquipment.setLeaseComencmentDate(dto.getLeaseComencmentDate());
		plantEquipment.setLeaseExpireDate(dto.getLeaseExpireDate());
		plantEquipment.setLeaseMonthlyRateS(dto.getLeaseMonthlyRateS());
		plantEquipment.setLeaseMaxTotalUnits(dto.getLeaseMaxTotalUnits());
		plantEquipment.setReadingFrequency(dto.getReadingFrequency());
		plantEquipment.setVisualEquipmentCode(dto.getVisualEquipmentCode());

		processingBeforeDataUpdate(dto, plantEquipment);

		PlantEquipment plantEquipment2 = plantEquipmentRepository.findById(plantEquipment.getId()).orElse(null);
		if (plantEquipment2 != null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
			processingIfDataRecordAlreadyExists(dto, plantEquipment2);
		}
		else {
			try {
				plantEquipmentRepository.saveAndFlush(plantEquipment);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto, plantEquipment);
			} catch (Exception e) {
				System.out.println("Create error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
				processingIfDataUpdateError(dto, plantEquipment);
			}
		}

		CreatePlantEquipmentResult result = new CreatePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(CreatePlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordAlreadyExists(CreatePlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(CreatePlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataUpdateError(CreatePlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		
		return stepResult;
	}


}
