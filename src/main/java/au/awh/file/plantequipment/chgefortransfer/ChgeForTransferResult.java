package au.awh.file.plantequipment.chgefortransfer;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgeForTransfer ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgeForTransferResult implements Serializable
{
	private static final long serialVersionUID = -5471103497252420632L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
