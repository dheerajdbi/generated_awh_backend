package au.awh.file.plantequipment.edteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EdtePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EdtePlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 301848951217801119L;

    private ErrorProcessingEnum errorProcessing = null;
    private String plantEquipmentCode = "";
    private String awhRegionCode = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String centreCodeKey = "";
    private ProgramModeEnum programMode = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setAwhBuisnessSegment(String awhBuisnessSegment) {
		setAwhBuisnessSegment(AwhBuisnessSegmentEnum.valueOf(awhBuisnessSegment));
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public ProgramModeEnum getProgramMode() {
		return programMode;
	}
	
	public void setProgramMode(ProgramModeEnum programMode) {
		this.programMode = programMode;
	}
	
	public void setProgramMode(String programMode) {
		setProgramMode(ProgramModeEnum.valueOf(programMode));
	}
}
