package au.awh.file.plantequipment.edteplantequipment;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=edtePlantEquipment (1878232) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentService;
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentState;
import au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentResult;
// imports for DTO
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'EdtE Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class EdtePlantEquipmentService extends AbstractService<EdtePlantEquipmentService, EdtePlantEquipmentDTO>
{
	/*private final Step execute = define("execute", EdtePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});*/
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private EdtPlantEquipmentService edtPlantEquipmentService;

	private final Step execute = define("execute", EdtePlantEquipmentDTO.class, this::execute);
//	private final Step serviceEdtPlantEquipment = define("serviceEdtPlantEquipment",EdtPlantEquipmentResult.class, this::processServiceEdtPlantEquipment);
	private final Step postCallEdtPlantEquipment = define("postCallEdtPlantEquipment", EdtePlantEquipmentResult.class, this::postCallEdtPlantEquipment);
	
	@Autowired
	public EdtePlantEquipmentService() {
		super(EdtePlantEquipmentService.class, EdtePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	/*public StepResult execute(EdtePlantEquipmentParams params) throws ServiceException {
		EdtePlantEquipmentDTO dto = new EdtePlantEquipmentDTO();
		return executeService(dto, params);
	}*/

	private StepResult execute(EdtePlantEquipmentDTO dto, EdtePlantEquipmentDTO screenDTO) {
		StepResult stepResult = NO_ACTION;

		/**
		 *  (Generated:2)
		 */
		//EdtPlantEquipmentResult edtPlantEquipmentResult = null;
		//EdtPlantEquipmentParams edtPlantEquipmentParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Edt Plant Equipment - Plant Equipment  *
		//edtPlantEquipmentParams = new EdtPlantEquipmentParams();
		//edtPlantEquipmentResult = new EdtPlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		//BeanUtils.copyProperties(dto, edtPlantEquipmentParams);
		//edtPlantEquipmentParams.setProgramMode(dto.getProgramMode());
		//edtPlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		//edtPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		// TODO SPLIT SCREEN EXCINTFUN PlantEquipment.edtePlantEquipment (2) -> EDTRCD PlantEquipment.edtPlantEquipment
		//stepResult = StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams).thenCall(serviceEdtPlantEquipment);
		//dto.setExitProgramOption(edtPlantEquipmentResult.getExitProgramOption());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK"))) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// DEBUG genFunctionCall Message SNDERRMSG 1367383 ERR Return Code invalid      Send error message
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878232 PlantEquipment.edtePlantEquipment 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}
		EdtPlantEquipmentParams edtPlantEquipmentParams = new EdtPlantEquipmentParams();
		BeanUtils.copyProperties(screenDTO, edtPlantEquipmentParams);
		edtPlantEquipmentParams.setAwhRegionCode(screenDTO.getAwhRegionCode());
		edtPlantEquipmentParams.set_SysConductDetailScreenConversation(true);
		return StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams).thenCall(this.postCallEdtPlantEquipment);
	}


    /**
     * EdtPlantEquipmentService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceEdtPlantEquipment(EdtePlantEquipmentDTO dto, EdtPlantEquipmentResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, dto);
        }

        //TODO: call the continuation of the program
        EdtePlantEquipmentParams edtePlantEquipmentParams = new EdtePlantEquipmentParams();
        BeanUtils.copyProperties(dto, edtePlantEquipmentParams);
        stepResult = StepResult.callService(EdtePlantEquipmentService.class, edtePlantEquipmentParams);

        return stepResult;
    }
    
    public StepResult postCallEdtPlantEquipment(EdtePlantEquipmentDTO dto, EdtePlantEquipmentResult serviceResult) {
	    StepResult result = StepResult.NO_ACTION;
	    
	    BeanUtils.copyProperties(serviceResult, dto);
	    
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum.fromCode(""))) {
			// NOT PGM.*Return code is *Normal
			//switchBLK 1000043 BLK CAS
			//switchSUB 1000043 BLK CAS
			if ((dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("6")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("7")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("0")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("5")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("9"))) {
				// PAR.Error Processing is *Do not send message
				//switchBLK 1000144 BLK TXT
				// 
			}//switchSUB 1000058 SUB    
			 else {
				// *OTHERWISE
				//switchBLK 1000139 BLK TXT
				// * NOTE: Update parameters on generic error message.
				//switchBLK 1000132 BLK ACT
				// DEBUG genFunctionCall 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1367383)
			}
			//switchBLK 1000086 BLK CAS
			//switchSUB 1000086 BLK CAS
			if ((dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("7")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("4"))) {
				// PAR.Error Processing is *Ignore
				//switchBLK 1000103 BLK ACT
				// DEBUG genFunctionCall 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				//switchBLK 1000102 BLK TXT
				// 
			} else //switchSUB 1000090 SUB    
			if ((dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("5")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("2"))) {
				// PAR.Error Processing is *Quit
				//switchBLK 1000092 BLK ACT
				// DEBUG genFunctionCall 1000093 ACT <-- *QUIT
				// TODO: Unsupported Function Type 'Send Error Message' (message surrogate = 1001684)
				//switchBLK 1000094 BLK TXT
				// 
			} else //switchSUB 1000087 SUB    
			if ((dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("6")) || (dto.getErrorProcessing() == ErrorProcessingEnum.fromCode("3"))) {
				// PAR.Error Processing is *Exit
				//switchBLK 1000095 BLK ACT
				// DEBUG genFunctionCall 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			}
		}
		
		EdtePlantEquipmentResult results = new EdtePlantEquipmentResult();
		/*results.set_SysReturnCode(dto.get_SysReturnCode());*/
		results.setExitProgramOption(dto.getExitProgramOption());
		result = StepResult.returnFromService(results);
		
		return result;
	}

}
