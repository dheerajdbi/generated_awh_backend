package au.awh.file.plantequipment.addeplantequipment;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=addePlantEquipment (1878237) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CommitmentControlLevelEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.addplantequipment.AddPlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'AddE Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class AddePlantEquipmentService extends AbstractService<AddePlantEquipmentService, AddePlantEquipmentDTO>
{
	private final Step execute = define("execute", AddePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private AddPlantEquipmentService addPlantEquipmentService;

	private final Step serviceAddPlantEquipment = define("serviceAddPlantEquipment",AddPlantEquipmentResult.class, this::processServiceAddPlantEquipment);
	
	@Autowired
	public AddePlantEquipmentService() {
		super(AddePlantEquipmentService.class, AddePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(AddePlantEquipmentParams params) throws ServiceException {
		AddePlantEquipmentDTO dto = new AddePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(AddePlantEquipmentDTO dto, AddePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		AddPlantEquipmentParams addPlantEquipmentParams = null;
		AddPlantEquipmentResult addPlantEquipmentResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Add Plant Equipment - Plant Equipment  *
		addPlantEquipmentParams = new AddPlantEquipmentParams();
		addPlantEquipmentResult = new AddPlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, addPlantEquipmentParams);
		addPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		// TODO SPLIT SCREEN EXCINTFUN PlantEquipment.addePlantEquipment (2) -> PMTRCD PlantEquipment.addPlantEquipment
		stepResult = StepResult.callService(AddPlantEquipmentService.class, addPlantEquipmentParams).thenCall(serviceAddPlantEquipment);
		dto.setExitProgramOption(addPlantEquipmentResult.getExitProgramOption());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878237 PlantEquipment.addePlantEquipment 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		AddePlantEquipmentResult result = new AddePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


    /**
     * AddPlantEquipmentService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processServiceAddPlantEquipment(AddePlantEquipmentDTO dto, AddPlantEquipmentResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        if (serviceResult != null) {
            BeanUtils.copyProperties(serviceResult, dto);
        }

        //TODO: call the continuation of the program
        AddePlantEquipmentParams addePlantEquipmentParams = new AddePlantEquipmentParams();
        BeanUtils.copyProperties(dto, addePlantEquipmentParams);
        stepResult = StepResult.callService(AddePlantEquipmentService.class, addePlantEquipmentParams);

        return stepResult;
    }

}
