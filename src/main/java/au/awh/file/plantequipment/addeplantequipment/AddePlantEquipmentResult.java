package au.awh.file.plantequipment.addeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: AddePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class AddePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -2536415170848602246L;

	private ReturnCodeEnum _sysReturnCode;
	private ExitProgramOptionEnum exitProgramOption = null; // ExitProgramOptionEnum 29901

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
