package au.awh.file.plantequipment;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PlantEquipmentId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "EQAWHREG")
	private String awhRegionCode = "";
	
	@Column(name = "EQEQCODE")
	private String plantEquipmentCode = "";

	public PlantEquipmentId() {
	
	}

	public PlantEquipmentId(String awhRegionCode, String plantEquipmentCode) { 	this.awhRegionCode = awhRegionCode;
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
