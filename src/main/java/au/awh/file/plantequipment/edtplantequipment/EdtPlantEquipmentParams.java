package au.awh.file.plantequipment.edtplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EdtPlantEquipment (WSPLTWEE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 3516080612320824246L;

    private String plantEquipmentCode = "";
    private String awhRegionCode = "";
    private ProgramModeEnum programMode = null;
    private boolean _sysConductDetailScreenConversation = false;

    public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public ProgramModeEnum getProgramMode() {
		return programMode;
	}
	
	public void setProgramMode(ProgramModeEnum programMode) {
		this.programMode = programMode;
	}
	
	public void setProgramMode(String programMode) {
		setProgramMode(ProgramModeEnum.valueOf(programMode));
	}

	public boolean get_SysConductDetailScreenConversation() {
		return _sysConductDetailScreenConversation;
	}

	public void set_SysConductDetailScreenConversation(boolean _sysConductDetailScreenConversation) {
		this._sysConductDetailScreenConversation = _sysConductDetailScreenConversation;
	}
	
	
}
