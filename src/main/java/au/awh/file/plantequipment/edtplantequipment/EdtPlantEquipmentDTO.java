package au.awh.file.plantequipment.edtplantequipment;

import java.io.Serializable;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;

import au.awh.file.plantequipment.PlantEquipment;

import au.awh.service.data.BaseDTO;
// generateStatusFieldImportStatements BEGIN 30
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
// generateStatusFieldImportStatements END

/**
 * DTO for file 'Plant Equipment' (WSEQP) and function 'Edt Plant Equipment' (WSPLTWEE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtPlantEquipmentDTO extends BaseDTO implements Serializable {
    private static final long serialVersionUID = -8651379167096257545L;

    private String awhRegionCode = "";
    private String awhRegionNameDrv = "";
    private String plantEquipmentCode = "";
    private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
    private String visualEquipmentCode = "";
    private String centreCodeKey = "";
    private String centreNameDrv = "";
    private String conditionName2 = "";
    private String equipmentLeasor = "";
    private EquipmentStatusEnum equipmentStatus = EquipmentStatusEnum.fromCode("");
    private String organisationNameDrv = "";
    private String sflselPromptText = "";
    private String conditionName = "";
    private String equipmentDescription = "";
    private String equipmentBarcode = "";
    private String equipmentSerialNumber = "";
    private LocalDate leaseComencmentDate = null;
    private LocalDate leaseExpireDate = null;
    private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO;
    private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO;
    private ReadingFrequencyEnum readingFrequency = ReadingFrequencyEnum.fromCode("");
    private String conditionName1 = "";
    private String equipmentComment = "";
    private String equipmentTypeCode = "";
    private String equipmentTypeDescDrv = "";
    private EquipReadingUnitEnum equipReadingUnit = null;
    private BigDecimal readingValue = BigDecimal.ZERO;
    private LocalDate readingRequiredDate = null;
    private ProgramModeEnum programMode = null;
    private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum.fromCode("");
	private String _SysconditionName = "";

    public EdtPlantEquipmentDTO() {
    }

    public EdtPlantEquipmentDTO(PlantEquipment plantEquipment) {
        setDtoFields(plantEquipment);
    }

    public String getAwhRegionCode() {
        return awhRegionCode;
    }

    public void setAwhRegionCode(String awhRegionCode) {
        this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionNameDrv() {
        return awhRegionNameDrv;
    }

    public void setAwhRegionNameDrv(String awhRegionNameDrv) {
        this.awhRegionNameDrv = awhRegionNameDrv;
    }

    public String getPlantEquipmentCode() {
        return plantEquipmentCode;
    }

    public void setPlantEquipmentCode(String plantEquipmentCode) {
        this.plantEquipmentCode = plantEquipmentCode;
    }

    public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
        return awhBuisnessSegment;
    }

    public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
        this.awhBuisnessSegment = awhBuisnessSegment;
    }

    public String getVisualEquipmentCode() {
        return visualEquipmentCode;
    }

    public void setVisualEquipmentCode(String visualEquipmentCode) {
        this.visualEquipmentCode = visualEquipmentCode;
    }

    public String getCentreCodeKey() {
        return centreCodeKey;
    }

    public void setCentreCodeKey(String centreCodeKey) {
        this.centreCodeKey = centreCodeKey;
    }

    public String getCentreNameDrv() {
        return centreNameDrv;
    }

    public void setCentreNameDrv(String centreNameDrv) {
        this.centreNameDrv = centreNameDrv;
    }

    public String getConditionName2() {
        return conditionName2;
    }

    public void setConditionName2(String conditionName2) {
        this.conditionName2 = conditionName2;
    }

    public String getEquipmentLeasor() {
        return equipmentLeasor;
    }

    public void setEquipmentLeasor(String equipmentLeasor) {
        this.equipmentLeasor = equipmentLeasor;
    }

    public EquipmentStatusEnum getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

    public String getOrganisationNameDrv() {
        return organisationNameDrv;
    }

    public void setOrganisationNameDrv(String organisationNameDrv) {
        this.organisationNameDrv = organisationNameDrv;
    }

    public String getSflselPromptText() {
        return sflselPromptText;
    }

    public void setSflselPromptText(String sflselPromptText) {
        this.sflselPromptText = sflselPromptText;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getEquipmentDescription() {
        return equipmentDescription;
    }

    public void setEquipmentDescription(String equipmentDescription) {
        this.equipmentDescription = equipmentDescription;
    }

    public String getEquipmentBarcode() {
        return equipmentBarcode;
    }

    public void setEquipmentBarcode(String equipmentBarcode) {
        this.equipmentBarcode = equipmentBarcode;
    }

    public String getEquipmentSerialNumber() {
        return equipmentSerialNumber;
    }

    public void setEquipmentSerialNumber(String equipmentSerialNumber) {
        this.equipmentSerialNumber = equipmentSerialNumber;
    }

    public LocalDate getLeaseComencmentDate() {
        return leaseComencmentDate;
    }

    public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
        this.leaseComencmentDate = leaseComencmentDate;
    }

    public LocalDate getLeaseExpireDate() {
        return leaseExpireDate;
    }

    public void setLeaseExpireDate(LocalDate leaseExpireDate) {
        this.leaseExpireDate = leaseExpireDate;
    }

    public BigDecimal getLeaseMonthlyRateS() {
        return leaseMonthlyRateS;
    }

    public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
        this.leaseMonthlyRateS = leaseMonthlyRateS;
    }

    public BigDecimal getLeaseMaxTotalUnits() {
        return leaseMaxTotalUnits;
    }

    public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
        this.leaseMaxTotalUnits = leaseMaxTotalUnits;
    }

    public ReadingFrequencyEnum getReadingFrequency() {
        return readingFrequency;
    }

    public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
        this.readingFrequency = readingFrequency;
    }

    public String getConditionName1() {
        return conditionName1;
    }

    public void setConditionName1(String conditionName1) {
        this.conditionName1 = conditionName1;
    }

    public String getEquipmentComment() {
        return equipmentComment;
    }

    public void setEquipmentComment(String equipmentComment) {
        this.equipmentComment = equipmentComment;
    }

    public String getEquipmentTypeCode() {
        return equipmentTypeCode;
    }

    public void setEquipmentTypeCode(String equipmentTypeCode) {
        this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeDescDrv() {
        return equipmentTypeDescDrv;
    }

    public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
        this.equipmentTypeDescDrv = equipmentTypeDescDrv;
    }

    public EquipReadingUnitEnum getEquipReadingUnit() {
        return equipReadingUnit;
    }

    public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
        this.equipReadingUnit = equipReadingUnit;
    }

    public BigDecimal getReadingValue() {
        return readingValue;
    }

    public void setReadingValue(BigDecimal readingValue) {
        this.readingValue = readingValue;
    }

    public LocalDate getReadingRequiredDate() {
        return readingRequiredDate;
    }

    public void setReadingRequiredDate(LocalDate readingRequiredDate) {
        this.readingRequiredDate = readingRequiredDate;
    }

    public ProgramModeEnum getProgramMode() {
        return programMode;
    }

    public void setProgramMode(ProgramModeEnum programMode) {
        this.programMode = programMode;
    }

    public ExitProgramOptionEnum getExitProgramOption() {
        return exitProgramOption;
    }

    public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.exitProgramOption = exitProgramOption;
    }

	public void set_SysConditionName(String conditionName) {
    	this._SysconditionName = conditionName;
    }

	public String get_SysConditionName() {
		return _SysconditionName;
	}    /**
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setDtoFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(plantEquipment, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setEntityFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(this, plantEquipment);
    }
}
