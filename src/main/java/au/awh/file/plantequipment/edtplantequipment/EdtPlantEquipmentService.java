package au.awh.file.plantequipment.edtplantequipment;

import java.io.IOException;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.support.JobContext;

import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsService;
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeService;
import au.awh.file.plantequipment.changeplantequipment.ChangePlantEquipmentService;
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentService;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentResult;
// asServiceDtoImportStatement
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsDTO;
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeDTO;
import au.awh.file.plantequipment.changeplantequipment.ChangePlantEquipmentDTO;
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsParams;
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeParams;
import au.awh.file.plantequipment.changeplantequipment.ChangePlantEquipmentParams;
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.equipmentreadings.crteequipmentreadings.CrteEquipmentReadingsResult;
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeResult;
import au.awh.file.plantequipment.changeplantequipment.ChangePlantEquipmentResult;
import au.awh.file.plantequipment.createplantequipment.CreatePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END

import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeService;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeParams;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeResult;


import au.awh.service.ServiceException;

import au.awh.model.CmdKeyEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 40
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * EDTRCD Service controller for 'Edt Plant Equipment' (WSPLTWEE1K) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
@Service
public class EdtPlantEquipmentService extends AbstractService<EdtPlantEquipmentService, EdtPlantEquipmentState>
{
    
    @Autowired
    private JobContext job;
    
    @Autowired
    private PlantEquipmentRepository plantEquipmentRepository;
    
    @Autowired
    private ChangePlantEquipmentService changePlantEquipmentService;
    
    @Autowired
    private CreatePlantEquipmentService createPlantEquipmentService;
    
    @Autowired
    private CrteEquipmentReadingsService crteEquipmentReadingsService;
    
    @Autowired
    private GeteEquipmentTypeService geteEquipmentTypeService;
    
    @Autowired
    private MessageSource messageSource;
        
    public static final String SCREEN_KEY = "edtPlantEquipmentEntryPanel";
    public static final String SCREEN_DTL = "edtPlantEquipmentPanel";
    public static final String SCREEN_CFM = "EdtPlantEquipment.confirm";
    
    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", EdtPlantEquipmentParams.class, this::executeService);
    private final Step keyScreenResponse = define("keyScreen", EdtPlantEquipmentDTO.class, this::processKeyScreenResponse);
    private final Step detailScreenResponse = define("dtlScreen", EdtPlantEquipmentDTO.class, this::processDetailScreenResponse);
    private final Step confirmScreenResponse = define("cfmScreen", EdtPlantEquipmentDTO.class, this::processConfirmScreenResponse);
    
    private final Step promptSelEquipmentType = define("promptSelEquipmentType",SelEquipmentTypeResult.class, this::processPromptSelEquipmentType);
    //private final Step serviceCreatePlantEquipment = define("serviceCreatePlantEquipment",CreatePlantEquipmentResult.class, this::processServiceCreatePlantEquipment);
    //private final Step serviceChangePlantEquipment = define("serviceChangePlantEquipment",ChangePlantEquipmentResult.class, this::processServiceChangePlantEquipment);
    //private final Step serviceGeteEquipmentType = define("serviceGeteEquipmentType",GeteEquipmentTypeResult.class, this::processServiceGeteEquipmentType);
    //private final Step serviceCrteEquipmentReadings = define("serviceCrteEquipmentReadings",CrteEquipmentReadingsResult.class, this::processServiceCrteEquipmentReadings);
    
        @Autowired
    public EdtPlantEquipmentService()
    {
        super(EdtPlantEquipmentService.class, EdtPlantEquipmentState.class);
    }
    
    @Override
    public Step getInitialStep()
    {
        return execute;
    }
    
    /**
     * EDTRCD controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EdtPlantEquipmentState state, EdtPlantEquipmentParams params)
    {
        StepResult stepResult = NO_ACTION;
       
    
        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
    
        state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        usrInitializeProgram(state);
        
        if(state.get_SysConductDetailScreenConversation()){
        	EdtPlantEquipmentDTO edtPlantEquipmentDTO = new EdtPlantEquipmentDTO();
        	BeanUtils.copyProperties(state, edtPlantEquipmentDTO);
        	stepResult =  processKeyScreenResponse(state, edtPlantEquipmentDTO);
        } else{        	        
        	stepResult = conductKeyScreenConversation(state);
        }
        return stepResult;
    }
    
    /**
     * SCREEN_KEY initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductKeyScreenConversation(EdtPlantEquipmentState state) 
    {
        StepResult stepResult = NO_ACTION;
    
        if(shouldBypassKeyScreen(state)){
            stepResult = conductDetailScreenConversation(state);
        } else {
            stepResult = displayKeyScreenConversation(state);
        }
    
        return stepResult;
    }
    
    /**
     * Determine if SCREEN_KEY should be bypassed
     * @param state - Service state class.
     * @return bypass - boolean
     */
    private boolean shouldBypassKeyScreen(EdtPlantEquipmentState state) {
        boolean bypass = false;
    
        //bypass if all key screens are set
        if(state.getAwhRegionCode() != null && !state.getAwhRegionCode().equals("") && state.getPlantEquipmentCode() != null && !state.getPlantEquipmentCode().equals("")){
            bypass = true;
        }
    
        return bypass;
    }
    
    /**
     * SCREEN_KEY display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult displayKeyScreenConversation(EdtPlantEquipmentState state)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysConductKeyScreenConversation()) {
            EdtPlantEquipmentDTO model = new EdtPlantEquipmentDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_KEY, model).thenCall(keyScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_KEY returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processKeyScreenResponse(EdtPlantEquipmentState state, EdtPlantEquipmentDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        model.clearMessages();
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            stepResult = conductKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (CmdKeyEnum.isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    
        }
        else if(CmdKeyEnum.isChangeMode(state.get_SysCmdKey())) {
            if(state.get_SysProgramMode() == ProgramModeEnum._STA_ADD) {
                state.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            }
            else {
                state.set_SysProgramMode(ProgramModeEnum._STA_ADD);
            }
            stepResult = displayKeyScreenConversation(state);
        }
        else {
            checkKeyFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = displayKeyScreenConversation(state);
                return stepResult;
            }
            usrValidateKeyScreen(state);
            dbfReadDataRecord(state);
            if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
                usrInitializeExistingScreen(state);
            }
            else {
                usrInitializeNewScreen(state);
            }
    
            stepResult = conductDetailScreenConversation(state);
        }
    
        return stepResult;
    }
    
    /**
     * Check key fields set in SCREEN_KEY.
     * For now it's just if fields are null or strings are empty
     * @param state - Service state class.
     */
    private void checkKeyFields(EdtPlantEquipmentState state) {
    }
    
    /**
     * SCREEN_DTL display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductDetailScreenConversation(EdtPlantEquipmentState state)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysConductDetailScreenConversation()) {
            EdtPlantEquipmentDTO model = new EdtPlantEquipmentDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_DTL, model).thenCall(detailScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_DETAIL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processDetailScreenResponse(EdtPlantEquipmentState state, EdtPlantEquipmentDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        model.clearMessages();
    
        BeanUtils.copyProperties(model, state);
    
        if (CmdKeyEnum.isHelp(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isReset(state.get_SysCmdKey())) {
            stepResult = conductDetailScreenConversation(state);
        }
        else if (CmdKeyEnum.isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (CmdKeyEnum.isCancel(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isPrompt(state.get_SysCmdKey())) {
    		switch (state.get_SysEntrySelected())
    		    {
    		        case "typeCode":
    		            SelEquipmentTypeParams selEquipmentTypeParams = new SelEquipmentTypeParams();
    		            BeanUtils.copyProperties(state, selEquipmentTypeParams);
    		            stepResult = StepResult.callService(SelEquipmentTypeService.class, selEquipmentTypeParams).thenCall(promptSelEquipmentType);
    		            break;
    		        default:
    		            System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
    		            stepResult = conductDetailScreenConversation(state);
    		            break;
    		    }
        }
        else if (CmdKeyEnum.isKeyScreen(state.get_SysCmdKey())) {
            stepResult = displayKeyScreenConversation(state);
        }
        else if (CmdKeyEnum.isDelete(state.get_SysCmdKey())) {
            //delete action confirmed in client side
            dbfDeleteDataRecord(state);
            stepResult = displayKeyScreenConversation(state);
        }
        else {
            checkFields(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = conductDetailScreenConversation(state);
                return stepResult;
            }
            usrValidateDetailScreenFields(state);
            checkRelations(state);
            if(state.get_SysErrorFound()) {
                state.set_SysErrorFound(false);
                stepResult = conductDetailScreenConversation(state);
                return stepResult;
            }
            usrDetailScreenFunctionFields(state);
            //TODO: make confirm screen
            stepResult = processConfirmScreenResponse(state, null);//callScreen(SCREEN_CFM, state).thenCall(confirmScreenResponse);
        }
    
        return stepResult;
    }
    
    /**
     * SCREEN_CONFIRM returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processConfirmScreenResponse(EdtPlantEquipmentState state, EdtPlantEquipmentDTO model)
    {
        StepResult stepResult = NO_ACTION;
    
        if (state.get_SysProgramMode() == ProgramModeEnum._STA_CHANGE) {
            dbfUpdateDataRecord(state);
        }
        else {
            dbfCreateDataRecord(state);
        }
    
        stepResult = usrProcessCommandKeys(state);
        if (stepResult != StepResult.NO_ACTION) {
            return stepResult;
        }
        stepResult = displayKeyScreenConversation(state);
    
        return stepResult;
    }
    
    /**
     * Check fields set in SCREEN_DETAIL.
     * @param state - Service state class.
     */
    private void checkFields(EdtPlantEquipmentState state) {
    
    }
    
    /**
     * Check relations set in SCREEN_DETAIL.
     * @param state - Service state class.
     */
    private void checkRelations(EdtPlantEquipmentState state) {
    
    }
    
    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EdtPlantEquipmentState state)
    {
        StepResult stepResult = NO_ACTION;
    
        stepResult = usrExitCommandProcessing(state);
    
        EdtePlantEquipmentResult params = new EdtePlantEquipmentResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);
    
        return stepResult;
    }
        
    /**
     * ------------------------ Generated DBF methods ----------------------
     */
    
    private StepResult dbfReadDataRecord(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;
    
        PlantEquipmentId plantEquipmentId = new PlantEquipmentId(dto.getAwhRegionCode(), dto.getPlantEquipmentCode());
        PlantEquipment plantEquipment = plantEquipmentRepository.findById(plantEquipmentId).orElse(null);
    
        if (plantEquipment == null) {
            dto.set_SysProgramMode(ProgramModeEnum._STA_ADD);
        }
        else {
            dto.set_SysProgramMode(ProgramModeEnum._STA_CHANGE);
            BeanUtils.copyProperties(plantEquipment, dto);
        }
        return stepResult;
    }
    
    private StepResult dbfCreateDataRecord(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;
        try {
            /**
    		 * USER: Create Object (Generated:425)
    		 */
    		CreatePlantEquipmentResult createPlantEquipmentResult = null;
    		CreatePlantEquipmentParams createPlantEquipmentParams = null;
    		// DEBUG genFunctionCall BEGIN 426 ACT Create Plant Equipment - Plant Equipment  *
    		createPlantEquipmentParams = new CreatePlantEquipmentParams();
    		createPlantEquipmentResult = new CreatePlantEquipmentResult();
    		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
    		BeanUtils.copyProperties(dto, createPlantEquipmentParams);
    		createPlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
    		createPlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
    		createPlantEquipmentParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
    		createPlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
    		createPlantEquipmentParams.setEquipmentDescription(dto.getEquipmentDescription());
    		createPlantEquipmentParams.setEquipmentBarcode(dto.getEquipmentBarcode());
    		createPlantEquipmentParams.setEquipmentComment(dto.getEquipmentComment());
    		createPlantEquipmentParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
    		createPlantEquipmentParams.setEquipReadingUnit(dto.getEquipReadingUnit());
    		createPlantEquipmentParams.setEquipmentLeasor(dto.getEquipmentLeasor());
    		createPlantEquipmentParams.setEquipmentStatus(dto.getEquipmentStatus());
    		createPlantEquipmentParams.setEquipmentSerialNumber(dto.getEquipmentSerialNumber());
    		createPlantEquipmentParams.setLeaseComencmentDate(dto.getLeaseComencmentDate());
    		createPlantEquipmentParams.setLeaseExpireDate(dto.getLeaseExpireDate());
    		createPlantEquipmentParams.setLeaseMonthlyRateS(dto.getLeaseMonthlyRateS());
    		createPlantEquipmentParams.setLeaseMaxTotalUnits(dto.getLeaseMaxTotalUnits());
    		createPlantEquipmentParams.setReadingFrequency(dto.getReadingFrequency());
    		createPlantEquipmentParams.setVisualEquipmentCode(dto.getVisualEquipmentCode());
    		stepResult = createPlantEquipmentService.execute(createPlantEquipmentParams);
    		createPlantEquipmentResult = (CreatePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
    		dto.set_SysReturnCode(createPlantEquipmentResult.get_SysReturnCode());
    		// DEBUG genFunctionCall END
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }
    private StepResult dbfDeleteDataRecord(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;
        try {
            /**
    		 * USER: Delete Object (Generated:383)
    		 */
    
    		// DEBUG genFunctionCall BEGIN 384 ACT PGM.*Return code = CND.*Normal
    		dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
    		// DEBUG genFunctionCall END
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    
        return stepResult;
    }
    
    private StepResult dbfUpdateDataRecord(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;
        try {
    		PlantEquipmentId plantEquipmentId = new PlantEquipmentId(dto.getAwhRegionCode(), dto.getPlantEquipmentCode());
    		if (plantEquipmentRepository.existsById(plantEquipmentId)) {
    
            	/**
    			 * USER: Change Object (Generated:427)
    			 */
    			ChangePlantEquipmentParams changePlantEquipmentParams = null;
    			ChangePlantEquipmentResult changePlantEquipmentResult = null;
    			// DEBUG genFunctionCall BEGIN 428 ACT Change Plant Equipment - Plant Equipment  *
    			changePlantEquipmentParams = new ChangePlantEquipmentParams();
    			changePlantEquipmentResult = new ChangePlantEquipmentResult();
    			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
    			BeanUtils.copyProperties(dto, changePlantEquipmentParams);
    			changePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
    			changePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
    			changePlantEquipmentParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
    			changePlantEquipmentParams.setCentreCodeKey(dto.getCentreCodeKey());
    			changePlantEquipmentParams.setEquipmentDescription(dto.getEquipmentDescription());
    			changePlantEquipmentParams.setEquipmentBarcode(dto.getEquipmentBarcode());
    			changePlantEquipmentParams.setEquipmentComment(dto.getEquipmentComment());
    			changePlantEquipmentParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
    			changePlantEquipmentParams.setEquipReadingUnit(dto.getEquipReadingUnit());
    			changePlantEquipmentParams.setEquipmentLeasor(dto.getEquipmentLeasor());
    			changePlantEquipmentParams.setEquipmentStatus(dto.getEquipmentStatus());
    			changePlantEquipmentParams.setEquipmentSerialNumber(dto.getEquipmentSerialNumber());
    			changePlantEquipmentParams.setLeaseComencmentDate(dto.getLeaseComencmentDate());
    			changePlantEquipmentParams.setLeaseExpireDate(dto.getLeaseExpireDate());
    			changePlantEquipmentParams.setLeaseMonthlyRateS(dto.getLeaseMonthlyRateS());
    			changePlantEquipmentParams.setLeaseMaxTotalUnits(dto.getLeaseMaxTotalUnits());
    			changePlantEquipmentParams.setReadingFrequency(dto.getReadingFrequency());
    			changePlantEquipmentParams.setVisualEquipmentCode(dto.getVisualEquipmentCode());
    			stepResult = changePlantEquipmentService.execute(changePlantEquipmentParams);
    			changePlantEquipmentResult = (ChangePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
    			dto.set_SysReturnCode(changePlantEquipmentResult.get_SysReturnCode());
    			// DEBUG genFunctionCall END
    	        }
            else {
                throw new ServiceException("diagnosis.nf");
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }
    
    
/**
 * ------------------ Generated ActionDiagram UserPoint -----------------
 */

    private StepResult usrInitializeProgram(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Program (Generated:10)
			 */
			
			if (dto.getProgramMode() == ProgramModeEnum.fromCode("ADD")) {
				// PAR.*Program mode is *ADD
				// DEBUG genFunctionCall BEGIN 1000005 ACT PGM.*Program mode = CND.*ADD
				dto.set_SysProgramMode(ProgramModeEnum.fromCode("ADD"));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000012 ACT PGM.*Program mode = CND.*CHANGE
				dto.set_SysProgramMode(ProgramModeEnum.fromCode("CHG"));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000079 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrValidateKeyScreen(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Key Screen (Generated:418)
			 */
			
			// * Don't send any error messages on load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000091 ACT LCL.Error Processing = CND.Ignore
			dto.setLclErrorProcessing(ErrorProcessingEnum.fromCode("7"));
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrInitializeNewScreen(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Initialize Detail Screen (New Record) (Generated:239)
			 */
			
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000203 ACT DTL.Equipment Status = CND.Active
			dto.setEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000275 ACT *Set Cursor: DTL.Centre Code           KEY  (*Override=*NO)
			// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrInitializeExistingScreen(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * Initialize Detail Screen (Existing Record) (Generated:242)
			 */
			
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000269 ACT *Set Cursor: DTL.Equipment Description  (*Override=*NO)
			// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrValidateDetailScreenFields(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Validate Detail Screen Fields (Generated:502)
			 */
			GeteEquipmentTypeParams geteEquipmentTypeParams = null;
			GeteEquipmentTypeResult geteEquipmentTypeResult = null;
			// * Send any error messages on validate of detail screen.
			// DEBUG genFunctionCall BEGIN 1000097 ACT LCL.Error Processing = CND.Send message and ignore
			dto.setLclErrorProcessing(ErrorProcessingEnum.fromCode("4"));
			// DEBUG genFunctionCall END
			if (EquipReadingUnitEnum.isAllValues(dto.getEquipReadingUnit())) {
				// DTL.Equip Reading Unit is *ALL values
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000147 ACT GetE Equipment Type - Equipment Type  *
				geteEquipmentTypeParams = new GeteEquipmentTypeParams();
				geteEquipmentTypeResult = new GeteEquipmentTypeResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteEquipmentTypeParams);
				geteEquipmentTypeParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
				geteEquipmentTypeParams.setErrorProcessing("2");
				geteEquipmentTypeParams.setGetRecordOption("*BLANK");
				stepResult = geteEquipmentTypeService.execute(geteEquipmentTypeParams);
				geteEquipmentTypeResult = (GeteEquipmentTypeResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteEquipmentTypeResult.get_SysReturnCode());
				dto.setEquipmentTypeDescDrv(geteEquipmentTypeResult.getEquipmentTypeDesc());
				dto.setEquipReadingUnit(geteEquipmentTypeResult.getEquipReadingUnit());
				// DEBUG genFunctionCall END
			}
			if (dto.getCentreCodeKey() != null && dto.getCentreCodeKey().isEmpty()) {
				// DTL.Centre Code           KEY is Not entered
				// DEBUG genFunctionCall BEGIN 1000166 ACT Send error message - 'Value Required'
				// DEBUG genFunctionCall Message SNDERRMSG 1365919 ERR Value Required            Send error message
				dto.addMessage("centreCodeKey", "value.required", messageSource);
				// DEBUG genFunctionCall END
			}
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			(dto.getLeaseComencmentDate() != null) && dto.getLeaseComencmentDate().isAfter(dto.getLeaseComencmentDate())
			// DEBUG ComparatorJavaGenerator END
			) {
				// DTL.Lease Comencment Date GT DTL.Lease Comencment Date
				// DEBUG genFunctionCall BEGIN 1000236 ACT Send error message - 'Commence > Expre'
				// DEBUG genFunctionCall Message SNDERRMSG 2027107 ERR Commemce > Expre          Send error message
				dto.addMessage("readingRequiredDate", "commence.>.expre", messageSource);
				// DEBUG genFunctionCall END
			}
			if ((dto.get_SysProgramMode() == ProgramModeEnum.fromCode("ADD")) && (dto.getReadingRequiredDate().equals(LocalDate.parse("0001-01-01", DateTimeFormatter.BASIC_ISO_DATE)))) {
				// DEBUG genFunctionCall BEGIN 1000288 ACT Send error message - 'ISO Date Required'
				// DEBUG genFunctionCall Message SNDERRMSG 2039813 ERR ISO Date Required         Send error message
				dto.addMessage("readingRequiredDate", "iso.date.required", messageSource);
				// DEBUG genFunctionCall END
			}
			if (CmdKeyEnum.isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000019 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("C"));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000023 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrDetailScreenFunctionFields(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * CALC: Detail Screen Function Fields (Generated:469)
			 */
			
			// * Use LCL.Error Processing so that key screen not displayed
			// * when an error message is sent in load of detail screen.
			// DEBUG genFunctionCall BEGIN 1000109 ACT AWH region Name Drv      *FIELD                                             DTLA
			// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
			// DEBUG genFunctionCall END
			if (!dto.getCentreCodeKey().equals("")) {
				// DTL.Centre Code           KEY is Entered
				// DEBUG genFunctionCall BEGIN 1000113 ACT Centre Name Drv          *FIELD                                             DTLC
				// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
				// DEBUG genFunctionCall END
			}
			if (!dto.getEquipmentLeasor().equals("")) {
				// DTL.Equipment Leasor is Entered
				// DEBUG genFunctionCall BEGIN 1000117 ACT Organisation Name Drv    *FIELD                                             DTLO
				// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
				// DEBUG genFunctionCall END
			}
			if (!dto.getEquipmentTypeCode().equals("")) {
				// DTL.Equipment Type Code is Entered
				// DEBUG genFunctionCall BEGIN 1000121 ACT Equipment Type Desc Drv  *FIELD                                             DTLE
				// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000134 ACT DTL.*Condition name = Condition name of DTL.AWH Buisness Segment
			dto.set_SysConditionName(dto.getAwhBuisnessSegment().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000222 ACT DTL.Condition Name = Condition name of DTL.Equip Reading Unit
			dto.setConditionName(dto.getEquipReadingUnit().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000252 ACT DTL.Condition Name 1 = Condition name of DTL.Reading Frequency
			dto.setConditionName1(dto.getReadingFrequency().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END
			if (dto.getEquipmentStatus() == EquipmentStatusEnum.fromCode("*BLANK")) {
				// DTL.Equipment Status is Not Entered
				// DEBUG genFunctionCall BEGIN 1000263 ACT DTL.Equipment Status = CND.Active
				dto.setEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000128 ACT DTL.Condition Name 2 = Condition name of DTL.Equipment Status
			dto.setConditionName2(dto.getEquipmentStatus().getDescription());
			// Retrieve condition
			// DEBUG genFunctionCall END

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrProcessCommandKeys(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Process Command Keys (Generated:446)
			 */
			CrteEquipmentReadingsResult crteEquipmentReadingsResult = null;
			CrteEquipmentReadingsParams crteEquipmentReadingsParams = null;
			if (dto.get_SysProgramMode() == ProgramModeEnum.fromCode("ADD")) {
				// PGM.*Program mode is *ADD
				// DEBUG genFunctionCall BEGIN 1000176 ACT CrtE Equipment Readings - Equipment Readings  *
				crteEquipmentReadingsParams = new CrteEquipmentReadingsParams();
				crteEquipmentReadingsResult = new CrteEquipmentReadingsResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, crteEquipmentReadingsParams);
				crteEquipmentReadingsParams.setEquipmentReadingSgt(dto.getLclEquipmentReadingSgt());
				crteEquipmentReadingsParams.setAwhRegionCode(dto.getAwhRegionCode());
				crteEquipmentReadingsParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
				crteEquipmentReadingsParams.setReadingType("IL");
				crteEquipmentReadingsParams.setReadingValue(dto.getReadingValue());
				crteEquipmentReadingsParams.setReadingComment("*BLANK");
				crteEquipmentReadingsParams.setEquipmentLeasor(dto.getEquipmentLeasor());
				crteEquipmentReadingsParams.setTransferToFromSgt(BigDecimal.ZERO);
				crteEquipmentReadingsParams.setCentreCodeKey(dto.getCentreCodeKey());
				crteEquipmentReadingsParams.setAwhBuisnessSegment(dto.getAwhBuisnessSegment());
				crteEquipmentReadingsParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
				crteEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				crteEquipmentReadingsParams.setErrorProcessing("2");
				stepResult = crteEquipmentReadingsService.execute(crteEquipmentReadingsParams);
				crteEquipmentReadingsResult = (CrteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(crteEquipmentReadingsResult.get_SysReturnCode());
				dto.setLclEquipmentReadingSgt(crteEquipmentReadingsResult.getEquipmentReadingSgt());
				// DEBUG genFunctionCall END
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }

    private StepResult usrExitCommandProcessing(EdtPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
           /**
			 * USER: Exit Program Processing (Generated:64)
			 */
			
			if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
				// KEY.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000067 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("E"));
				// DEBUG genFunctionCall END
			}

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return stepResult;
    }


    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * CreatePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreatePlantEquipment(EdtPlantEquipmentState state, CreatePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtPlantEquipmentParams edtPlantEquipmentParams = new EdtPlantEquipmentParams();
//        BeanUtils.copyProperties(state, edtPlantEquipmentParams);
//        stepResult = StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChangePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChangePlantEquipment(EdtPlantEquipmentState state, ChangePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtPlantEquipmentParams edtPlantEquipmentParams = new EdtPlantEquipmentParams();
//        BeanUtils.copyProperties(state, edtPlantEquipmentParams);
//        stepResult = StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteEquipmentTypeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipmentType(EdtPlantEquipmentState state, GeteEquipmentTypeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtPlantEquipmentParams edtPlantEquipmentParams = new EdtPlantEquipmentParams();
//        BeanUtils.copyProperties(state, edtPlantEquipmentParams);
//        stepResult = StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * CrteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCrteEquipmentReadings(EdtPlantEquipmentState state, CrteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EdtPlantEquipmentParams edtPlantEquipmentParams = new EdtPlantEquipmentParams();
//        BeanUtils.copyProperties(state, edtPlantEquipmentParams);
//        stepResult = StepResult.callService(EdtPlantEquipmentService.class, edtPlantEquipmentParams);
//
//        return stepResult;
//    }
//


    /**
     * SelEquipmentTypeService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelEquipmentType(EdtPlantEquipmentState state, SelEquipmentTypeResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(serviceResult, state);

        stepResult = conductDetailScreenConversation(state);

        return stepResult;
    }

}
