package au.awh.file.plantequipment.edtplantequipment;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.model.ReloadSubfileEnum;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Edt Plant Equipment' (WSPLTWEE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtPlantEquipmentState extends EdtPlantEquipmentDTO {
    private static final long serialVersionUID = -627706563202563832L;
    // Local fields
    private BigDecimal lclEquipmentReadingSgt = BigDecimal.ZERO;
    private ErrorProcessingEnum lclErrorProcessing = null;

    // System fields
    private ProgramModeEnum _sysProgramMode = null;
    private ReturnCodeEnum _sysReturnCode = ReturnCodeEnum.fromCode("");
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysConductKeyScreenConversation = true;
    private boolean _sysConductDetailScreenConversation = false;
    private boolean _sysErrorFound = false;

    public EdtPlantEquipmentState() {
    }

    public EdtPlantEquipmentState(PlantEquipment plantEquipment) {
        setDtoFields(plantEquipment);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
        this.lclEquipmentReadingSgt = equipmentReadingSgt;
    }

    public BigDecimal getLclEquipmentReadingSgt() {
        return lclEquipmentReadingSgt;
    }

    public void setLclErrorProcessing(ErrorProcessingEnum errorProcessing) {
        this.lclErrorProcessing = errorProcessing;
    }

    public ErrorProcessingEnum getLclErrorProcessing() {
        return lclErrorProcessing;
    }

    public void set_SysProgramMode(ProgramModeEnum programMode) {
        _sysProgramMode = programMode;
    }

    public ProgramModeEnum get_SysProgramMode() {
        return _sysProgramMode;
    }

    public void set_SysReturnCode(ReturnCodeEnum returnCode) {
        _sysReturnCode = returnCode;
    }

    public ReturnCodeEnum get_SysReturnCode() {
        return _sysReturnCode;
    }

    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfileEnum() {
        return _sysReloadSubfile;
    }

    public boolean get_SysConductKeyScreenConversation() {
        return _sysConductKeyScreenConversation;
    }

    public void set_SysConductKeyScreenConversation(boolean conductKeyScreenConversation) {
        _sysConductKeyScreenConversation = conductKeyScreenConversation;
    }

    public boolean get_SysConductDetailScreenConversation() {
        return _sysConductDetailScreenConversation;
    }

    public void set_SysConductDetailScreenConversation(boolean conductDetailScreenConversation) {
        _sysConductDetailScreenConversation = conductDetailScreenConversation;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
