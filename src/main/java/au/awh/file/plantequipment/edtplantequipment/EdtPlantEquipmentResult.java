package au.awh.file.plantequipment.edtplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: EdtPlantEquipment (WSPLTWEE1K).
 *
 * @author X2EGenerator EDTRCDJavaControllerGenerator.kt
 */
public class EdtPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = 4088843520802242815L;

    private ExitProgramOptionEnum exitProgramOption = null;

    public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
