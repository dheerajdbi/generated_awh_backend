package au.awh.file.plantequipment.chgkeyeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgkeyePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgkeyePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -3455460274298795271L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
