package au.awh.file.plantequipment.chgkeyeplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.ErrorProcessingEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgkeyePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgkeyePlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = -6289695405431241221L;

    private ErrorProcessingEnum errorProcessing = null;
    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private String newPlantEquipmentCode = "";

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}
	
	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}
}
