package au.awh.file.plantequipment.chgkeyeplantequipment;

// ExecuteInternalFunction.kt File=PlantEquipment (EQ) Function=chgkeyePlantEquipment (2046608) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.plantequipment.chgkeyplantequipment.ChgkeyPlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.chgkeyplantequipment.ChgkeyPlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.chgkeyplantequipment.ChgkeyPlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.chgkeyplantequipment.ChgkeyPlantEquipmentResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'ChgKeyE Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class ChgkeyePlantEquipmentService extends AbstractService<ChgkeyePlantEquipmentService, ChgkeyePlantEquipmentDTO>
{
	private final Step execute = define("execute", ChgkeyePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private ChgkeyPlantEquipmentService chgkeyPlantEquipmentService;

	//private final Step serviceChgkeyPlantEquipment = define("serviceChgkeyPlantEquipment",ChgkeyPlantEquipmentResult.class, this::processServiceChgkeyPlantEquipment);
	
	@Autowired
	public ChgkeyePlantEquipmentService() {
		super(ChgkeyePlantEquipmentService.class, ChgkeyePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(ChgkeyePlantEquipmentParams params) throws ServiceException {
		ChgkeyePlantEquipmentDTO dto = new ChgkeyePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(ChgkeyePlantEquipmentDTO dto, ChgkeyePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		ChgkeyPlantEquipmentResult chgkeyPlantEquipmentResult = null;
		ChgkeyPlantEquipmentParams chgkeyPlantEquipmentParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT ChgKey Plant Equipment - Plant Equipment  *
		chgkeyPlantEquipmentParams = new ChgkeyPlantEquipmentParams();
		chgkeyPlantEquipmentResult = new ChgkeyPlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, chgkeyPlantEquipmentParams);
		stepResult = chgkeyPlantEquipmentService.execute(chgkeyPlantEquipmentParams);
		chgkeyPlantEquipmentResult = (ChgkeyPlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(chgkeyPlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000151 ACT Rollback
			// TODO: Unsupported Internal Function Type '*ROLLBACK' (message surrogate = 1001707)
			// DEBUG genFunctionCall END
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 2046608 PlantEquipment.chgkeyePlantEquipment 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		ChgkeyePlantEquipmentResult result = new ChgkeyePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * ChgkeyPlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgkeyPlantEquipment(ChgkeyePlantEquipmentDTO dto, ChgkeyPlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgkeyePlantEquipmentParams chgkeyePlantEquipmentParams = new ChgkeyePlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chgkeyePlantEquipmentParams);
//        stepResult = StepResult.callService(ChgkeyePlantEquipmentService.class, chgkeyePlantEquipmentParams);
//
//        return stepResult;
//    }
//
}
