package au.awh.file.plantequipment;

import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.utils.DateConverters;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSEQP")
public class PlantEquipment implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private PlantEquipmentId id = new PlantEquipmentId();

	@Column(name = "EQAWHBSG")
	private AwhBuisnessSegmentEnum awhBuisnessSegment = AwhBuisnessSegmentEnum.fromCode("");
	
	@Column(name = "EQCTRCDEK")
	private String centreCodeKey = "";
	
	@Column(name = "EQERUNIT")
	private EquipReadingUnitEnum equipReadingUnit = EquipReadingUnitEnum.fromCode("");
	
	@Column(name = "EQEQBRCDE")
	private String equipmentBarcode = "";
	
	@Column(name = "EQEQCOMM")
	private String equipmentComment = "";
	
	@Column(name = "EQEQDES")
	private String equipmentDescription = "";
	
	@Column(name = "EQEQLEASOR")
	private String equipmentLeasor = "";
	
	@Column(name = "EQEQSERNO")
	private String equipmentSerialNumber = "";
	
	@Column(name = "EQEQTRS")
	private EquipmentStatusEnum equipmentStatus = EquipmentStatusEnum.fromCode("AT");
	
	@Column(name = "EQETCDE")
	private String equipmentTypeCode = "";
	
	@Column(name = "EQLCOMDTE")
	@Convert(converter = DateConverters.class)
	private LocalDate leaseComencmentDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "EQLEXPDTE")
	@Convert(converter = DateConverters.class)
	private LocalDate leaseExpireDate = LocalDate.of(1801, 1, 1);
	
	@Column(name = "EQLEMAXUN")
	private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO;
	
	@Column(name = "EQLERATE")
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO;
	
	@Column(name = "EQREADFQ")
	private ReadingFrequencyEnum readingFrequency = ReadingFrequencyEnum.fromCode("");
	
	@Column(name = "EQVISCODE")
	private String visualEquipmentCode = "";

	public PlantEquipmentId getId() {
		return id;
	}

	public String getPlantEquipmentCode() {
		return id.getPlantEquipmentCode();
	}
	
	public String getAwhRegionCode() {
		return id.getAwhRegionCode();
	}

	public String getEquipmentDescription() {
		return equipmentDescription;
	}
	
	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}
	
	public String getEquipmentComment() {
		return equipmentComment;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}
	
	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}
	
	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}
	
	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}
	
	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}
	
	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}
	
	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}
	
	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}
	
	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}
	
	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}
	
	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	

	

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.id.setPlantEquipmentCode(plantEquipmentCode);
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.id.setAwhRegionCode(awhRegionCode);
	}

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}
	
	public void setEquipmentBarcode(String equipmentBarcode) {
		this.equipmentBarcode = equipmentBarcode;
	}
	
	public void setEquipmentComment(String equipmentComment) {
		this.equipmentComment = equipmentComment;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
	
	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
		this.equipReadingUnit = equipReadingUnit;
	}
	
	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
	}
	
	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
	}
	
	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}
	
	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}
	
	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}
	
	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}
	
	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}
	
	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
	}
	
	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}
	
	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
