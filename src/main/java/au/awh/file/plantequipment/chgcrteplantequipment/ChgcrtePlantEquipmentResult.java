package au.awh.file.plantequipment.chgcrteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgcrtePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgcrtePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -2144983812220828249L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
