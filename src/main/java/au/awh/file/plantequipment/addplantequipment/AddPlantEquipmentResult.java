package au.awh.file.plantequipment.addplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: AddPlantEquipment (WSPLTWFPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -2462109786608090038L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
