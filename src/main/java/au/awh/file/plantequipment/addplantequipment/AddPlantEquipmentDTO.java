package au.awh.file.plantequipment.addplantequipment;

import java.time.LocalDate;
import java.io.Serializable;
import java.math.BigDecimal;



import org.springframework.beans.BeanUtils;

import au.awh.file.plantequipment.PlantEquipment;
// generateStatusFieldImportStatements BEGIN 12
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
// generateStatusFieldImportStatements END

import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Plant Equipment' (WSEQP) and function 'Add Plant Equipment' (WSPLTWFPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddPlantEquipmentDTO extends BaseDTO implements Serializable {
	private static final long serialVersionUID = 5614632528540222893L;

// DEBUG fields
	private String awhRegionCode = ""; // 56452
	private String awhRegionNameDrv = ""; // 56540
	private EquipmentStatusEnum equipmentStatus = null; // 56583
	private String equipmentSerialNumber = ""; // 60161
	private LocalDate leaseComencmentDate = null; // 60165
	private LocalDate leaseExpireDate = null; // 60166
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO; // 60169
	private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO; // 60170
	private ReadingFrequencyEnum readingFrequency = null; // 60171
	private String visualEquipmentCode = ""; // 60172
	private String plantEquipmentCode = ""; // 56471
// DEBUG detail fields
// DEBUG param
	private ExitProgramOptionEnum exitProgramOption = ExitProgramOptionEnum._NORMAL; //0
// DEBUG local fields
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL; // 29901
// DEBUG Constructor

	public AddPlantEquipmentDTO() {

	}

	public AddPlantEquipmentDTO(PlantEquipment plantEquipment) {
		BeanUtils.copyProperties(plantEquipment, this);
	}

	public AddPlantEquipmentDTO(String awhRegionCode, EquipmentStatusEnum equipmentStatus, String equipmentSerialNumber, LocalDate leaseComencmentDate, LocalDate leaseExpireDate, BigDecimal leaseMonthlyRateS, BigDecimal leaseMaxTotalUnits, ReadingFrequencyEnum readingFrequency, String visualEquipmentCode, String plantEquipmentCode) {
		this.awhRegionCode = awhRegionCode;
		this.equipmentStatus = equipmentStatus;
		this.equipmentSerialNumber = equipmentSerialNumber;
		this.leaseComencmentDate = leaseComencmentDate;
		this.leaseExpireDate = leaseExpireDate;
		this.leaseMonthlyRateS = leaseMonthlyRateS;
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
		this.readingFrequency = readingFrequency;
		this.visualEquipmentCode = visualEquipmentCode;
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
	}

	public String getAwhRegionNameDrv() {
		return awhRegionNameDrv;
	}

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
	}

	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}

	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
		this.equipmentSerialNumber = equipmentSerialNumber;
	}

	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}

	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
		this.leaseComencmentDate = leaseComencmentDate;
	}

	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}

	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
		this.leaseExpireDate = leaseExpireDate;
	}

	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}

	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
		this.leaseMonthlyRateS = leaseMonthlyRateS;
	}

	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}

	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
	}

	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}

	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
		this.readingFrequency = readingFrequency;
	}

	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}

	public void setVisualEquipmentCode(String visualEquipmentCode) {
		this.visualEquipmentCode = visualEquipmentCode;
	}

	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}

	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.lclExitProgramOption = exitProgramOption;
	}

	public ExitProgramOptionEnum getLclExitProgramOption() {
		return lclExitProgramOption;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setDtoFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(plantEquipment, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param plantEquipment PlantEquipment Entity bean
     */
    public void setEntityFields(PlantEquipment plantEquipment) {
        BeanUtils.copyProperties(this, plantEquipment);
    }
}
