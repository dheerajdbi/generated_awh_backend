package au.awh.file.plantequipment.addplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: AddPlantEquipment (WSPLTWFPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 655414002344742410L;

	private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}
