package au.awh.file.plantequipment.addplantequipment;
    

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import au.awh.support.JobContext;

import au.awh.file.plantequipment.PlantEquipmentRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.plantequipment.chkeplantequipment.ChkePlantEquipmentService;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentService;
// asServiceDtoImportStatement
import au.awh.file.plantequipment.chkeplantequipment.ChkePlantEquipmentDTO;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentDTO;
// asServiceParamsImportStatement
import au.awh.file.plantequipment.chkeplantequipment.ChkePlantEquipmentParams;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentParams;
// asServiceResultImportStatement
import au.awh.file.plantequipment.chkeplantequipment.ChkePlantEquipmentResult;
import au.awh.file.plantequipment.edteplantequipment.EdtePlantEquipmentResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END

import au.awh.file.awhregion.selectawhregion.SelectAwhRegionService;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionParams;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionResult;

    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 24
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CheckRecordOptionEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements END

/**
 * PMTRCD Service controller for 'Add Plant Equipment' (WSPLTWFPVK) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
@Service
public class AddPlantEquipmentService extends AbstractService<AddPlantEquipmentService, AddPlantEquipmentState> {
    
    @Autowired
    private JobContext job;

    @Autowired
    private MessageSource messageSource;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

    
    public static final String SCREEN_PROMPT = "addPlantEquipment";
    public static final String SCREEN_CONFIRM_PROMPT = "AddPlantEquipment.key2";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute",AddPlantEquipmentParams.class, this::executeService);
    private final Step response = define("response",AddPlantEquipmentDTO.class, this::processResponse);
//  private final Step displayConfirmPrompt = define("displayConfirmPrompt",AddPlantEquipmentDTO.class, this::processDisplayConfirmPrompt);

	//private final Step serviceEdtePlantEquipment = define("serviceEdtePlantEquipment",EdtePlantEquipmentResult.class, this::processServiceEdtePlantEquipment);
	//private final Step serviceChkePlantEquipment = define("serviceChkePlantEquipment",ChkePlantEquipmentResult.class, this::processServiceChkePlantEquipment);
	

	private final Step promptSelectAwhRegion = define("promptSelectAwhRegion",SelectAwhRegionResult.class, this::processPromptSelectAwhRegion);
	
    	
@Autowired
private ChkePlantEquipmentService chkePlantEquipmentService;
	
@Autowired
private EdtePlantEquipmentService edtePlantEquipmentService;
	
    @Autowired
    public AddPlantEquipmentService()
    {
        super(AddPlantEquipmentService.class, AddPlantEquipmentState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * AddPlantEquipment service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(AddPlantEquipmentState state, AddPlantEquipmentParams params)  {
        StepResult stepResult = NO_ACTION;

        if(params != null) {
            BeanUtils.copyProperties(params, state);
        }
        initialize(state);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * AddPlantEquipment service initialization.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private void initialize(AddPlantEquipmentState state) {
        usrInitializeProgram(state);
        usrScreenFunctionFields(state);
        usrLoadScreen(state);
    }

    /**
     * SCREEN_PROMPT display processing loop method.
     * @param state   - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(AddPlantEquipmentState state)
    {
        StepResult stepResult = NO_ACTION;

        AddPlantEquipmentDTO dto = new AddPlantEquipmentDTO();
        BeanUtils.copyProperties(state, dto);
        stepResult = callScreen(SCREEN_PROMPT, dto).thenCall(response);

        return stepResult;
    }

    /**
     * SCREEN_PROMPT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(AddPlantEquipmentState state, AddPlantEquipmentDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey()))
        {
            stepResult = closedown(state);
        }
        else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey()))
        {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey()))
        {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isPrompt(state.get_SysCmdKey())) {
			switch (state.get_SysEntrySelected()) {
			    case "regionCode":
			        SelectAwhRegionParams selectAwhRegionParams = new SelectAwhRegionParams();
			        BeanUtils.copyProperties(state, selectAwhRegionParams);
			        stepResult = StepResult.callService(SelectAwhRegionService.class, selectAwhRegionParams).thenCall(promptSelectAwhRegion);
			        break;
			    default:
			        System.out.println("Field "+ state.get_SysEntrySelected() + " is not promptable");
			        stepResult = conductScreenConversation(state);
			        break;
			}
        }
        else
        {
            validateScreenInput(state);
            if (!state.get_SysErrorFound()){
                //BeanUtils.copyProperties(state, dto);
                //TODO: for now, confirm prompt is skipped
                stepResult = usrUserDefinedAction(state);//callScreen(SCREEN_CONFIRM_PROMPT, dto).thenCall(displayConfirmPrompt);
                if(stepResult != NO_ACTION) {
                    return stepResult.thenCall(response);
                }
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

//    /**
//     * SCREEN_CONFIRM_PROMPT returned response processing method.
//     * @param state - Service state class.
//     * @param dto - returned screen model.
//     * @return stepResult - Step result.
//     */
//    private StepResult processDisplayConfirmPrompt(AddPlantEquipmentState state, AddPlantEquipmentDTO dto) {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(dto, state);
//        if (!state.getDeferConfirm().getCode().equals(DeferConfirmEnum._PROCEED_TO_CONFIRM.getCode()))
//        {
//            usrUserDefinedAction(state);
//        }
//        stepResult = conductScreenConversation(state);
//
//        return stepResult;
//    }

    /**
     * Validate input in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void validateScreenInput(AddPlantEquipmentState state)
    {
        usrProcessCommandKeys(state);
        checkFields(state);
        if (!state.get_SysErrorFound())
        {
            usrValidateFields(state);
            checkRelations(state);
            if (!state.get_SysErrorFound())
            {
                usrScreenFunctionFields(state);
                usrValidateRelations(state);
            }
        }
    }

    /**
     * Check fields set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkFields(AddPlantEquipmentState state) {

    }

    /**
     * Check relations set in SCREEN_PROMPT.
     * @param state - Service state class.
     */
    private void checkRelations(AddPlantEquipmentState state) {

    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(AddPlantEquipmentState state)
    {
        StepResult stepResult = NO_ACTION;

        stepResult = usrExitProgramProcessing(state);
        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);

        AddPlantEquipmentResult params = new AddPlantEquipmentResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }

    
    
/**
 * ---------------------- Programmatic user-point --------------------------
 */
	/**
	 * USER: Initialize Program (Generated:10)
	 */
    private StepResult usrInitializeProgram(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// * Need to remove processing for select existing record.
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:1052)
	 */
    private StepResult usrScreenFunctionFields(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (!dto.getAwhRegionCode().equals("")) {
				// DTL.AWH Region Code is Entered
				// DEBUG genFunctionCall BEGIN 1000360 ACT AWH region Name Drv      *FIELD                                             DTLA
				// TODO: NEED TO SUPPORT *FIELD AWH region Name Drv      *FIELD                                             DTLA 1000360
				// DEBUG X2EActionDiagramElement 1000360 ACT     AWH region Name Drv      *FIELD                                             DTLA
				// DEBUG X2EActionDiagramElement 1000361 PAR DTL 
				// DEBUG X2EActionDiagramElement 1000362 PAR DTL 
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Screen (Generated:1058)
	 */
    private StepResult usrLoadScreen(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1058 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:1035)
	 */
    private StepResult usrProcessCommandKeys(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000330 ACT PAR.Exit Program Option = CND.Cancel requested
				dto.setExitProgramOption(ExitProgramOptionEnum._CANCEL_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000334 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Fields (Generated:1065)
	 */
    private StepResult usrValidateFields(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 1065 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Validate Relations (Generated:29)
	 */
    private StepResult usrValidateRelations(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            ChkePlantEquipmentParams chkePlantEquipmentParams = null;
			ChkePlantEquipmentResult chkePlantEquipmentResult = null;
			// DEBUG genFunctionCall BEGIN 1000251 ACT ChkE Plant Equipment - Plant Equipment  *
			chkePlantEquipmentParams = new ChkePlantEquipmentParams();
			chkePlantEquipmentResult = new ChkePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, chkePlantEquipmentParams);
			chkePlantEquipmentParams.setCheckRecordOption("N");
			chkePlantEquipmentParams.setErrorProcessing("4");
			chkePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
			chkePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			stepResult = chkePlantEquipmentService.execute(chkePlantEquipmentParams);
			chkePlantEquipmentResult = (ChkePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(chkePlantEquipmentResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: User Defined Action (Generated:41)
	 */
    private StepResult usrUserDefinedAction(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            EdtePlantEquipmentResult edtePlantEquipmentResult = null;
			EdtePlantEquipmentParams edtePlantEquipmentParams = null;
			// DEBUG genFunctionCall BEGIN 1000198 ACT EdtE Plant Equipment - Plant Equipment  *
			edtePlantEquipmentParams = new EdtePlantEquipmentParams();
			edtePlantEquipmentResult = new EdtePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, edtePlantEquipmentParams);
			edtePlantEquipmentParams.setProgramMode("ADD");
			edtePlantEquipmentParams.setErrorProcessing("2");
			edtePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
			edtePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
			// TODO SPLIT FUNCTION PMTRCD PlantEquipment.addPlantEquipment (41) -> EXCINTFUN PlantEquipment.edtePlantEquipment
			//stepResult = edtePlantEquipmentService.execute(edtePlantEquipmentParams);
			edtePlantEquipmentResult = (EdtePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(edtePlantEquipmentResult.get_SysReturnCode());
			dto.setLclExitProgramOption(edtePlantEquipmentResult.getExitProgramOption());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum._EXIT_REQUESTED) {
				// LCL.Exit Program Option is Exit requested
				// DEBUG genFunctionCall BEGIN 1000237 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000241 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:1021)
	 */
    private StepResult usrExitProgramProcessing(AddPlantEquipmentState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// DTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000247 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum._EXIT_REQUESTED);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * EdtePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEdtePlantEquipment(AddPlantEquipmentState state, EdtePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddPlantEquipmentParams addPlantEquipmentParams = new AddPlantEquipmentParams();
//        BeanUtils.copyProperties(state, addPlantEquipmentParams);
//        stepResult = StepResult.callService(AddPlantEquipmentService.class, addPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChkePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChkePlantEquipment(AddPlantEquipmentState state, ChkePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        AddPlantEquipmentParams addPlantEquipmentParams = new AddPlantEquipmentParams();
//        BeanUtils.copyProperties(state, addPlantEquipmentParams);
//        stepResult = StepResult.callService(AddPlantEquipmentService.class, addPlantEquipmentParams);
//
//        return stepResult;
//    }
//

    /**
     * SelectAwhRegionService returned response processing method.
     * @param state - Service state class.
     * @param serviceResult - returned service model.
     * @return
     */
    private StepResult processPromptSelectAwhRegion(AddPlantEquipmentState state, SelectAwhRegionResult serviceResult)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(serviceResult, state);

        stepResult = conductScreenConversation(state);

        return stepResult;
    }


}
