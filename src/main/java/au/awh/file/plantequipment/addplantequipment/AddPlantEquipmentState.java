package au.awh.file.plantequipment.addplantequipment;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Add Plant Equipment' (WSPLTWFPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class AddPlantEquipmentState extends AddPlantEquipmentDTO {
    private static final long serialVersionUID = -5122380212711439895L;

    // Local fields
    private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL;

    // System fields
    private boolean _sysErrorFound = false;

    public AddPlantEquipmentState() {
    }

    public AddPlantEquipmentState(PlantEquipment plantEquipment) {
        setDtoFields(plantEquipment);
    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

    public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
        this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
        return lclExitProgramOption;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }

}
