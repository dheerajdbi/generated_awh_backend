package au.awh.file.plantequipment.chkreadingsbatchsts;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.EquipReadingCtrlStsEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChkReadingsBatchSts ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkReadingsBatchStsResult implements Serializable
{
	private static final long serialVersionUID = 1080733843272060098L;

	private ReturnCodeEnum _sysReturnCode;
	private EquipReadingCtrlStsEnum equipReadingCtrlSts = null; // EquipReadingCtrlStsEnum 56566

	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}
	
	public void setEquipReadingCtrlSts(String equipReadingCtrlSts) {
		setEquipReadingCtrlSts(EquipReadingCtrlStsEnum.valueOf(equipReadingCtrlSts));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
