package au.awh.file.plantequipment.chkreadingsbatchsts;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ChkReadingsBatchStsDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private EquipReadingCtrlStsEnum equipReadingCtrlSts;
	private LocalDate readingRequiredDate;
	private String awhRegionCode;
	private String centreCodeKey;

	private String lclCentreCodeKey;
	private String lclDateListName;
	private BigDecimal lclDay;
	private LocalDate lclReadingRequiredDate;
	private ReadingTypeEnum lclReadingType;
	private BigDecimal lclReadingValue;
	private RecordSelectedEnum lclRecordSelected;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public EquipReadingCtrlStsEnum getEquipReadingCtrlSts() {
		return equipReadingCtrlSts;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public String getLclCentreCodeKey() {
		return lclCentreCodeKey;
	}

	public String getLclDateListName() {
		return lclDateListName;
	}

	public BigDecimal getLclDay() {
		return lclDay;
	}

	public LocalDate getLclReadingRequiredDate() {
		return lclReadingRequiredDate;
	}

	public ReadingTypeEnum getLclReadingType() {
		return lclReadingType;
	}

	public BigDecimal getLclReadingValue() {
		return lclReadingValue;
	}

	public RecordSelectedEnum getLclRecordSelected() {
		return lclRecordSelected;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		this.equipReadingCtrlSts = equipReadingCtrlSts;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setLclCentreCodeKey(String lclCentreCodeKey) {
		this.lclCentreCodeKey = lclCentreCodeKey;
	}

	public void setLclDateListName(String lclDateListName) {
		this.lclDateListName = lclDateListName;
	}

	public void setLclDay(BigDecimal lclDay) {
		this.lclDay = lclDay;
	}

	public void setLclReadingRequiredDate(LocalDate lclReadingRequiredDate) {
		this.lclReadingRequiredDate = lclReadingRequiredDate;
	}

	public void setLclReadingType(ReadingTypeEnum lclReadingType) {
		this.lclReadingType = lclReadingType;
	}

	public void setLclReadingValue(BigDecimal lclReadingValue) {
		this.lclReadingValue = lclReadingValue;
	}

	public void setLclRecordSelected(RecordSelectedEnum lclRecordSelected) {
		this.lclRecordSelected = lclRecordSelected;
	}

}
