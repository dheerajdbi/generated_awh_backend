package au.awh.file.plantequipment.chkreadingsbatchsts;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListService;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateService;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateService;
// imports for DTO
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListDTO;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateDTO;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.DateDetailTypeEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SelectedDaysDatesEnum;
// imports for Parameters
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListParams;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateParams;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateParams;
// imports for Results
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListResult;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateResult;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Chk Readings Batch Sts' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator
 */
@Service
public class ChkReadingsBatchStsService extends AbstractService<ChkReadingsBatchStsService, ChkReadingsBatchStsDTO>
{
    private final Step execute = define("execute", ChkReadingsBatchStsParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private GetCentreHolidayListService getCentreHolidayListService;

	@Autowired
	private GeteByReverseDateService geteByReverseDateService;

	@Autowired
	private GeteReadingByDateService geteReadingByDateService;

	//private final Step serviceGeteReadingByDate = define("serviceGeteReadingByDate",GeteReadingByDateResult.class, this::processServiceGeteReadingByDate);
	//private final Step serviceGetCentreHolidayList = define("serviceGetCentreHolidayList",GetCentreHolidayListResult.class, this::processServiceGetCentreHolidayList);
	//private final Step serviceGeteByReverseDate = define("serviceGeteByReverseDate",GeteByReverseDateResult.class, this::processServiceGeteByReverseDate);
	

    @Autowired
    public ChkReadingsBatchStsService()
    {
        super(ChkReadingsBatchStsService.class, ChkReadingsBatchStsDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(ChkReadingsBatchStsParams params) throws ServiceException {
        ChkReadingsBatchStsDTO dto = new ChkReadingsBatchStsDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(ChkReadingsBatchStsDTO dto, ChkReadingsBatchStsParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<PlantEquipment> plantEquipmentList = plantEquipmentRepository.findAllByIdAwhRegionCodeAndCentreCodeKey(dto.getAwhRegionCode(), dto.getCentreCodeKey());
		if (!plantEquipmentList.isEmpty()) {
			for (PlantEquipment plantEquipment : plantEquipmentList) {
				processDataRecord(dto, plantEquipment);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        ChkReadingsBatchStsResult result = new ChkReadingsBatchStsResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(ChkReadingsBatchStsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000034 ACT PAR.Equip Reading Ctrl Sts = CND.Complete
		dto.setEquipReadingCtrlSts(EquipReadingCtrlStsEnum._COMPLETE);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processDataRecord(ChkReadingsBatchStsDTO dto, PlantEquipment plantEquipment) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		GeteByReverseDateParams geteByReverseDateParams = null;
		GeteByReverseDateResult geteByReverseDateResult = null;
		GetCentreHolidayListResult getCentreHolidayListResult = null;
		GeteReadingByDateParams geteReadingByDateParams = null;
		GeteReadingByDateResult geteReadingByDateResult = null;
		GetCentreHolidayListParams getCentreHolidayListParams = null;
		if (
		// DEBUG ComparatorJavaGenerator BEGIN
		!plantEquipment.getCentreCodeKey().equals(dto.getLclCentreCodeKey())
		// DEBUG ComparatorJavaGenerator END
		) {
			// DB1.Centre Code           KEY NE LCL.Centre Code           KEY
			// DEBUG genFunctionCall BEGIN 1000065 ACT LCL.Centre Code           KEY = DB1.Centre Code           KEY
			dto.setLclCentreCodeKey(plantEquipment.getCentreCodeKey());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000069 ACT Get Centre Holiday List - *Date List Header  *
			getCentreHolidayListParams = new GetCentreHolidayListParams();
			getCentreHolidayListResult = new GetCentreHolidayListResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getCentreHolidayListParams);
			getCentreHolidayListParams.setCentreCodeKey(plantEquipment.getCentreCodeKey());
			stepResult = getCentreHolidayListService.execute(getCentreHolidayListParams);
			getCentreHolidayListResult = (GetCentreHolidayListResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getCentreHolidayListResult.get_SysReturnCode());
			dto.setLclDateListName(getCentreHolidayListResult.getDateListName());
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1000118 ACT LCL.*Record selected = CND.*YES
		dto.setLclRecordSelected(RecordSelectedEnum._STA_YES);
		// DEBUG genFunctionCall END
		if (plantEquipment.getEquipmentStatus() == EquipmentStatusEnum._ACTIVE) {
			// DB1.Equipment Status is Active
			if (plantEquipment.getReadingFrequency() == ReadingFrequencyEnum._DAILY) {
				// DB1.Reading Frequency is Daily
			} else {
				// *OTHERWISE
				if (plantEquipment.getReadingFrequency() == ReadingFrequencyEnum._MONTHLY) {
					// DB1.Reading Frequency is Monthly
					// DEBUG genFunctionCall BEGIN 1000084 ACT LCL.Day = PAR.Reading Required Date *DAY OF MONTH
					// TODO: The '*DATE DETAIL' built-in Function is not supported yet
					// DEBUG genFunctionCall END
					if (dto.getLclDay().intValue() > 1) {
						// LCL.Day is GT 1
						// DEBUG genFunctionCall BEGIN 1000096 ACT LCL.*Record selected = CND.*NO
						dto.setLclRecordSelected(RecordSelectedEnum._STA_NO);
						// DEBUG genFunctionCall END
					}
				} else if (plantEquipment.getReadingFrequency() == ReadingFrequencyEnum._WEEKLY) {
					// DB1.Reading Frequency is Weekly
					// DEBUG genFunctionCall BEGIN 1000102 ACT LCL.Day = PAR.Reading Required Date *DAY OF WEEK
					// TODO: The '*DATE DETAIL' built-in Function is not supported yet
					// DEBUG genFunctionCall END
					if (dto.getLclDay().intValue() > 1) {
						// LCL.Day is GT 1
						// DEBUG genFunctionCall BEGIN 1000114 ACT LCL.*Record selected = CND.*NO
						dto.setLclRecordSelected(RecordSelectedEnum._STA_NO);
						// DEBUG genFunctionCall END
					}
				}
			}
		}
		if ((plantEquipment.getEquipmentStatus() == EquipmentStatusEnum._ACTIVE) && (dto.getLclRecordSelected() == RecordSelectedEnum._STA_YES)) {
			// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Reading by Date - Equipment Readings  *
			geteReadingByDateParams = new GeteReadingByDateParams();
			geteReadingByDateResult = new GeteReadingByDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteReadingByDateParams);
			geteReadingByDateParams.setAwhRegionCode(plantEquipment.getAwhRegionCode());
			geteReadingByDateParams.setCentreCodeKey(plantEquipment.getCentreCodeKey());
			geteReadingByDateParams.setReadingType("ST");
			geteReadingByDateParams.setPlantEquipmentCode(plantEquipment.getPlantEquipmentCode());
			geteReadingByDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			geteReadingByDateParams.setErrorProcessing("0");
			geteReadingByDateParams.setGetRecordOption("");
			stepResult = geteReadingByDateService.execute(geteReadingByDateParams);
			geteReadingByDateResult = (GeteReadingByDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteReadingByDateResult.get_SysReturnCode());
			dto.setLclReadingValue(geteReadingByDateResult.getReadingValue());
			// DEBUG genFunctionCall END
			if (dto.getLclReadingValue() != BigDecimal.ZERO) {
				// LCL.Reading Value is Entered
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000186 ACT LCL.Reading Type = CND.Initial reading
				dto.setLclReadingType(ReadingTypeEnum._INITIAL_READING);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000137 ACT GetE By reverse date - Equipment Readings  * Initial
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto, geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(plantEquipment.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(plantEquipment.getPlantEquipmentCode());
				geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				geteByReverseDateParams.setErrorProcessing("2");
				geteByReverseDateParams.setGetRecordOption("I");
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				dto.setLclReadingValue(geteByReverseDateResult.getReadingValue());
				dto.setLclReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
				// DEBUG genFunctionCall END
				if (dto.getLclReadingRequiredDate().equals(LocalDate.parse("0001-01-01", DateTimeFormatter.BASIC_ISO_DATE))) {
					// LCL.Reading Required Date is Not Entered
					// DEBUG genFunctionCall BEGIN 1000161 ACT LCL.Reading Type = CND.Transfer In Reading
					dto.setLclReadingType(ReadingTypeEnum._TRANSFER_IN_READING);
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000165 ACT GetE By reverse date - Equipment Readings  * Initial
					geteByReverseDateParams = new GeteByReverseDateParams();
					geteByReverseDateResult = new GeteByReverseDateResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto, geteByReverseDateParams);
					geteByReverseDateParams.setAwhRegionCode(plantEquipment.getAwhRegionCode());
					geteByReverseDateParams.setPlantEquipmentCode(plantEquipment.getPlantEquipmentCode());
					geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
					geteByReverseDateParams.setReadingType(dto.getLclReadingType());
					geteByReverseDateParams.setErrorProcessing("2");
					geteByReverseDateParams.setGetRecordOption("I");
					stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
					geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
					dto.setLclReadingType(geteByReverseDateResult.getReadingType());
					dto.setLclReadingValue(geteByReverseDateResult.getReadingValue());
					dto.setLclReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
					// DEBUG genFunctionCall END
					if (
					// DEBUG ComparatorJavaGenerator BEGIN
					dto.getLclReadingRequiredDate().isAfter(dto.getReadingRequiredDate()) || dto.getLclReadingRequiredDate().equals(dto.getReadingRequiredDate())
					// DEBUG ComparatorJavaGenerator END
					) {
						// LCL.Reading Required Date GE PAR.Reading Required Date
					} else {
						// *OTHERWISE
						// DEBUG genFunctionCall BEGIN 1000025 ACT PAR.Equip Reading Ctrl Sts = CND.In-Progress
						dto.setEquipReadingCtrlSts(EquipReadingCtrlStsEnum._IN_DAS_PROGRESS);
						// DEBUG genFunctionCall END
						// DEBUG genFunctionCall BEGIN 1000052 ACT <-- *QUIT
						// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
						// DEBUG genFunctionCall END
					}
				}
			}
		}

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(ChkReadingsBatchStsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000044 ACT PAR.Equip Reading Ctrl Sts = CND.Created
		dto.setEquipReadingCtrlSts(EquipReadingCtrlStsEnum._CREATED);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(ChkReadingsBatchStsDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * GeteReadingByDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteReadingByDate(ChkReadingsBatchStsDTO dto, GeteReadingByDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChkReadingsBatchStsParams chkReadingsBatchStsParams = new ChkReadingsBatchStsParams();
//        BeanUtils.copyProperties(dto, chkReadingsBatchStsParams);
//        stepResult = StepResult.callService(ChkReadingsBatchStsService.class, chkReadingsBatchStsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetCentreHolidayListService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentreHolidayList(ChkReadingsBatchStsDTO dto, GetCentreHolidayListParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChkReadingsBatchStsParams chkReadingsBatchStsParams = new ChkReadingsBatchStsParams();
//        BeanUtils.copyProperties(dto, chkReadingsBatchStsParams);
//        stepResult = StepResult.callService(ChkReadingsBatchStsService.class, chkReadingsBatchStsParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteByReverseDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteByReverseDate(ChkReadingsBatchStsDTO dto, GeteByReverseDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChkReadingsBatchStsParams chkReadingsBatchStsParams = new ChkReadingsBatchStsParams();
//        BeanUtils.copyProperties(dto, chkReadingsBatchStsParams);
//        stepResult = StepResult.callService(ChkReadingsBatchStsService.class, chkReadingsBatchStsParams);
//
//        return stepResult;
//    }
//
}
