package au.awh.file.plantequipment.chkreadingsbatchsts;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChkReadingsBatchSts ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChkReadingsBatchStsParams implements Serializable
{
	private static final long serialVersionUID = 8634893338688897305L;

    private String awhRegionCode = "";
    private String centreCodeKey = "";
    private LocalDate readingRequiredDate = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
}
