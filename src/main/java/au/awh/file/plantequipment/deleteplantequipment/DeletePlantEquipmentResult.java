package au.awh.file.plantequipment.deleteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: DeletePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DeletePlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -386035372194250728L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
