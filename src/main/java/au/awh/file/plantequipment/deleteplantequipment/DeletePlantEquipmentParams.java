package au.awh.file.plantequipment.deleteplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: DeletePlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class DeletePlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = 4504766811408779873L;

    private String plantEquipmentCode = "";
    private String awhRegionCode = "";

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}
