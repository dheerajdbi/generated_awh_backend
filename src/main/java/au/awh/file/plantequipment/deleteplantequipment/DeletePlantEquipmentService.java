package au.awh.file.plantequipment.deleteplantequipment;

// DeleteObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * DELOBJ Service controller for 'Delete Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator  DeleteObjectFunction.kt
 */
@Service
public class DeletePlantEquipmentService  extends AbstractService<DeletePlantEquipmentService, DeletePlantEquipmentDTO>
{
	private final Step execute = define("execute", DeletePlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	
	@Autowired
	public DeletePlantEquipmentService() {
		super(DeletePlantEquipmentService.class, DeletePlantEquipmentDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(DeletePlantEquipmentParams params) throws ServiceException {
		DeletePlantEquipmentDTO dto = new DeletePlantEquipmentDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(DeletePlantEquipmentDTO dto, DeletePlantEquipmentParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataUpdate(dto);

		PlantEquipmentId plantEquipmentId = new PlantEquipmentId();
		plantEquipmentId.setAwhRegionCode(dto.getAwhRegionCode());
		plantEquipmentId.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		try {
			plantEquipmentRepository.deleteById(plantEquipmentId);
			plantEquipmentRepository.flush();
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			processingAfterDataUpdate(dto);
		} catch (Exception e) {
			System.out.println("Delete error: " + Arrays.toString(e.getStackTrace()));
			dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
		}

		DeletePlantEquipmentResult result = new DeletePlantEquipmentResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(DeletePlantEquipmentDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Empty:9)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(DeletePlantEquipmentDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:17)
		 */
		
		return stepResult;
	}


}
