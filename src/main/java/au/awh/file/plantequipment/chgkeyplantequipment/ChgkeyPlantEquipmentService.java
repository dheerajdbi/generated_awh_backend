package au.awh.file.plantequipment.chgkeyplantequipment;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.plantequipment.PlantEquipment;
import au.awh.file.plantequipment.PlantEquipmentId;
import au.awh.file.plantequipment.PlantEquipmentRepository;
// imports for Services
import au.awh.file.equipmentreadings.rtvupdeequipmentcode.RtvupdeEquipmentCodeService;
import au.awh.file.plantequipment.crteplantequipment.CrtePlantEquipmentService;
import au.awh.file.plantequipment.dlteplantequipment.DltePlantEquipmentService;
// imports for DTO
import au.awh.file.equipmentreadings.rtvupdeequipmentcode.RtvupdeEquipmentCodeDTO;
import au.awh.file.plantequipment.crteplantequipment.CrtePlantEquipmentDTO;
import au.awh.file.plantequipment.dlteplantequipment.DltePlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmentreadings.rtvupdeequipmentcode.RtvupdeEquipmentCodeParams;
import au.awh.file.plantequipment.crteplantequipment.CrtePlantEquipmentParams;
import au.awh.file.plantequipment.dlteplantequipment.DltePlantEquipmentParams;
// imports for Results
import au.awh.file.equipmentreadings.rtvupdeequipmentcode.RtvupdeEquipmentCodeResult;
import au.awh.file.plantequipment.crteplantequipment.CrtePlantEquipmentResult;
import au.awh.file.plantequipment.dlteplantequipment.DltePlantEquipmentResult;
// imports section complete


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'ChgKey Plant Equipment' (file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator
 */
@Service
public class ChgkeyPlantEquipmentService extends AbstractService<ChgkeyPlantEquipmentService, ChgkeyPlantEquipmentDTO>
{
    private final Step execute = define("execute", ChgkeyPlantEquipmentParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private PlantEquipmentRepository plantEquipmentRepository;

	@Autowired
	private CrtePlantEquipmentService crtePlantEquipmentService;

	@Autowired
	private DltePlantEquipmentService dltePlantEquipmentService;

	@Autowired
	private RtvupdeEquipmentCodeService rtvupdeEquipmentCodeService;

	//private final Step serviceDltePlantEquipment = define("serviceDltePlantEquipment",DltePlantEquipmentResult.class, this::processServiceDltePlantEquipment);
	//private final Step serviceCrtePlantEquipment = define("serviceCrtePlantEquipment",CrtePlantEquipmentResult.class, this::processServiceCrtePlantEquipment);
	//private final Step serviceRtvupdeEquipmentCode = define("serviceRtvupdeEquipmentCode",RtvupdeEquipmentCodeResult.class, this::processServiceRtvupdeEquipmentCode);
	

    @Autowired
    public ChgkeyPlantEquipmentService()
    {
        super(ChgkeyPlantEquipmentService.class, ChgkeyPlantEquipmentDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(ChgkeyPlantEquipmentParams params) throws ServiceException {
        ChgkeyPlantEquipmentDTO dto = new ChgkeyPlantEquipmentDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(ChgkeyPlantEquipmentDTO dto, ChgkeyPlantEquipmentParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<PlantEquipment> plantEquipmentList = plantEquipmentRepository.findAllByIdPlantEquipmentCode(dto.getPlantEquipmentCode());
		if (!plantEquipmentList.isEmpty()) {
			for (PlantEquipment plantEquipment : plantEquipmentList) {
				processDataRecord(dto, plantEquipment);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        ChgkeyPlantEquipmentResult result = new ChgkeyPlantEquipmentResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(ChgkeyPlantEquipmentDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(ChgkeyPlantEquipmentDTO dto, PlantEquipment plantEquipment) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		CrtePlantEquipmentParams crtePlantEquipmentParams = null;
		RtvupdeEquipmentCodeParams rtvupdeEquipmentCodeParams = null;
		DltePlantEquipmentResult dltePlantEquipmentResult = null;
		DltePlantEquipmentParams dltePlantEquipmentParams = null;
		RtvupdeEquipmentCodeResult rtvupdeEquipmentCodeResult = null;
		CrtePlantEquipmentResult crtePlantEquipmentResult = null;
		// DEBUG genFunctionCall BEGIN 1000006 ACT DltE Plant Equipment - Plant Equipment  *
		dltePlantEquipmentParams = new DltePlantEquipmentParams();
		dltePlantEquipmentResult = new DltePlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, dltePlantEquipmentParams);
		dltePlantEquipmentParams.setErrorProcessing("5");
		stepResult = dltePlantEquipmentService.execute(dltePlantEquipmentParams);
		dltePlantEquipmentResult = (DltePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(dltePlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		// * NOTE: Need to define new key as parameter and pass to CrtE
		// DEBUG genFunctionCall BEGIN 1000022 ACT CrtE Plant Equipment - Plant Equipment  *
		crtePlantEquipmentParams = new CrtePlantEquipmentParams();
		crtePlantEquipmentResult = new CrtePlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, crtePlantEquipmentParams);
		crtePlantEquipmentParams.setErrorProcessing("5");
		crtePlantEquipmentParams.setAwhRegionCode(plantEquipment.getAwhRegionCode());
		crtePlantEquipmentParams.setPlantEquipmentCode(dto.getNewPlantEquipmentCode());
		crtePlantEquipmentParams.setAwhBuisnessSegment(plantEquipment.getAwhBuisnessSegment());
		crtePlantEquipmentParams.setCentreCodeKey(plantEquipment.getCentreCodeKey());
		crtePlantEquipmentParams.setEquipmentDescription(plantEquipment.getEquipmentDescription());
		crtePlantEquipmentParams.setEquipmentBarcode(plantEquipment.getEquipmentBarcode());
		crtePlantEquipmentParams.setEquipmentComment(plantEquipment.getEquipmentComment());
		crtePlantEquipmentParams.setEquipmentTypeCode(plantEquipment.getEquipmentTypeCode());
		crtePlantEquipmentParams.setEquipReadingUnit(plantEquipment.getEquipReadingUnit());
		crtePlantEquipmentParams.setEquipmentLeasor(plantEquipment.getEquipmentLeasor());
		crtePlantEquipmentParams.setEquipmentSerialNumber(plantEquipment.getEquipmentSerialNumber());
		crtePlantEquipmentParams.setLeaseComencmentDate(plantEquipment.getLeaseComencmentDate());
		crtePlantEquipmentParams.setLeaseExpireDate(plantEquipment.getLeaseExpireDate());
		crtePlantEquipmentParams.setLeaseMonthlyRateS(plantEquipment.getLeaseMonthlyRateS());
		crtePlantEquipmentParams.setLeaseMaxTotalUnits(plantEquipment.getLeaseMaxTotalUnits());
		crtePlantEquipmentParams.setReadingFrequency(plantEquipment.getReadingFrequency());
		crtePlantEquipmentParams.setVisualEquipmentCode(plantEquipment.getVisualEquipmentCode());
		stepResult = crtePlantEquipmentService.execute(crtePlantEquipmentParams);
		crtePlantEquipmentResult = (CrtePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(crtePlantEquipmentResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000087 ACT RtvUpdE Equipment Code - Equipment Readings  * Update Readings
		rtvupdeEquipmentCodeParams = new RtvupdeEquipmentCodeParams();
		rtvupdeEquipmentCodeResult = new RtvupdeEquipmentCodeResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, rtvupdeEquipmentCodeParams);
		rtvupdeEquipmentCodeParams.setAwhRegionCode(plantEquipment.getAwhRegionCode());
		rtvupdeEquipmentCodeParams.setPlantEquipmentCode(plantEquipment.getPlantEquipmentCode());
		rtvupdeEquipmentCodeParams.setNewPlantEquipmentCode(dto.getNewPlantEquipmentCode());
		rtvupdeEquipmentCodeParams.setErrorProcessing("2");
		rtvupdeEquipmentCodeParams.setGetRecordOption("I");
		stepResult = rtvupdeEquipmentCodeService.execute(rtvupdeEquipmentCodeParams);
		rtvupdeEquipmentCodeResult = (RtvupdeEquipmentCodeResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(rtvupdeEquipmentCodeResult.get_SysReturnCode());
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(ChgkeyPlantEquipmentDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(ChgkeyPlantEquipmentDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }

//
//    /**
//     * DltePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceDltePlantEquipment(ChgkeyPlantEquipmentDTO dto, DltePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgkeyPlantEquipmentParams chgkeyPlantEquipmentParams = new ChgkeyPlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chgkeyPlantEquipmentParams);
//        stepResult = StepResult.callService(ChgkeyPlantEquipmentService.class, chgkeyPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * CrtePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCrtePlantEquipment(ChgkeyPlantEquipmentDTO dto, CrtePlantEquipmentParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgkeyPlantEquipmentParams chgkeyPlantEquipmentParams = new ChgkeyPlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chgkeyPlantEquipmentParams);
//        stepResult = StepResult.callService(ChgkeyPlantEquipmentService.class, chgkeyPlantEquipmentParams);
//
//        return stepResult;
//    }
////
//    /**
//     * RtvupdeEquipmentCodeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceRtvupdeEquipmentCode(ChgkeyPlantEquipmentDTO dto, RtvupdeEquipmentCodeParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        ChgkeyPlantEquipmentParams chgkeyPlantEquipmentParams = new ChgkeyPlantEquipmentParams();
//        BeanUtils.copyProperties(dto, chgkeyPlantEquipmentParams);
//        stepResult = StepResult.callService(ChgkeyPlantEquipmentService.class, chgkeyPlantEquipmentParams);
//
//        return stepResult;
//    }
//
}
