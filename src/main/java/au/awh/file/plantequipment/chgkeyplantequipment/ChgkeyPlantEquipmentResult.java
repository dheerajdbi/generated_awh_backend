package au.awh.file.plantequipment.chgkeyplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: ChgkeyPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgkeyPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -3041463143206794356L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
