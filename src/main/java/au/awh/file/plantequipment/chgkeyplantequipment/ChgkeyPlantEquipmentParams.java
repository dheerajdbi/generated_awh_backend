package au.awh.file.plantequipment.chgkeyplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 3
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: ChgkeyPlantEquipment ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class ChgkeyPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = -3177165838143370849L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";
    private String newPlantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
	
	public String getNewPlantEquipmentCode() {
		return newPlantEquipmentCode;
	}
	
	public void setNewPlantEquipmentCode(String newPlantEquipmentCode) {
		this.newPlantEquipmentCode = newPlantEquipmentCode;
	}
}
