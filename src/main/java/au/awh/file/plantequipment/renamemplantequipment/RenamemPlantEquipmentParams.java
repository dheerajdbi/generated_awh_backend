package au.awh.file.plantequipment.renamemplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Params for resource: RenamemPlantEquipment (WSPLTNMPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class RenamemPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = -7118786263339414685L;

	private String awhRegionCode = "";
	private String plantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
}
