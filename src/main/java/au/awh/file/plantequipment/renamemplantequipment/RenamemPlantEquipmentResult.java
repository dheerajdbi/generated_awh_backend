package au.awh.file.plantequipment.renamemplantequipment;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.ExitProgramOptionEnum;
// generateStatusFieldImportStatements END

/**
 * Params for resource: RenamemPlantEquipment (WSPLTNMPVK).
 *
 * @author X2EGenerator PMTRCDJavaControllerGenerator.kt
 */
public class RenamemPlantEquipmentResult implements Serializable
{
	private static final long serialVersionUID = -4873227067527246161L;

	private ExitProgramOptionEnum exitProgramOption = null;

	public ExitProgramOptionEnum getExitProgramOption() {
		return exitProgramOption;
	}
	
	public void setExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
		this.exitProgramOption = exitProgramOption;
	}
	
	public void setExitProgramOption(String exitProgramOption) {
		setExitProgramOption(ExitProgramOptionEnum.valueOf(exitProgramOption));
	}
}
