package au.awh.file.plantequipment.bldprtplantequipment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class BldprtPlantEquipmentDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private FormsEnum forms;
	private HoldClFormatyesnoEnum holdClFormatyesno;
	private LocalDate readingRequiredDate;
	private SaveClFormatyesnoEnum saveClFormatyesno;
	private String awhRegionCode;
	private String centreCodeKey;
	private String copies;
	private String outputQueue;
	private String outputQueueLibrary;
	private String overflowLineNumberText;
	private YesOrNoEnum checkForHolidays;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public YesOrNoEnum getCheckForHolidays() {
		return checkForHolidays;
	}

	public String getCopies() {
		return copies;
	}

	public FormsEnum getForms() {
		return forms;
	}

	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}

	public String getOutputQueue() {
		return outputQueue;
	}

	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}

	public String getOverflowLineNumberText() {
		return overflowLineNumberText;
	}

	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}

	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setCheckForHolidays(YesOrNoEnum checkForHolidays) {
		this.checkForHolidays = checkForHolidays;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}

	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}

	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}

	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}

	public void setOverflowLineNumberText(String overflowLineNumberText) {
		this.overflowLineNumberText = overflowLineNumberText;
	}

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}

	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}

}
