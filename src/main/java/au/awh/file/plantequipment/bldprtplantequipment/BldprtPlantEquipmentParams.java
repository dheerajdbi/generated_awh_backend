package au.awh.file.plantequipment.bldprtplantequipment;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 11
import au.awh.model.FormsEnum;
import au.awh.model.HoldClFormatyesnoEnum;
import au.awh.model.SaveClFormatyesnoEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: BldprtPlantEquipment (WSPLTWOPFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class BldprtPlantEquipmentParams implements Serializable
{
	private static final long serialVersionUID = -6605942922973133725L;

    private String awhRegionCode = "";
    private String centreCodeKey = "";
    private String copies = "";
    private FormsEnum forms = null;
    private HoldClFormatyesnoEnum holdClFormatyesno = null;
    private SaveClFormatyesnoEnum saveClFormatyesno = null;
    private String outputQueue = "";
    private String outputQueueLibrary = "";
    private String overflowLineNumberText = "";
    private LocalDate readingRequiredDate = null;
    private YesOrNoEnum checkForHolidays = null;

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
	
	public String getCopies() {
		return copies;
	}
	
	public void setCopies(String copies) {
		this.copies = copies;
	}
	
	public FormsEnum getForms() {
		return forms;
	}
	
	public void setForms(FormsEnum forms) {
		this.forms = forms;
	}
	
	public void setForms(String forms) {
		setForms(FormsEnum.valueOf(forms));
	}
	
	public HoldClFormatyesnoEnum getHoldClFormatyesno() {
		return holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(HoldClFormatyesnoEnum holdClFormatyesno) {
		this.holdClFormatyesno = holdClFormatyesno;
	}
	
	public void setHoldClFormatyesno(String holdClFormatyesno) {
		setHoldClFormatyesno(HoldClFormatyesnoEnum.valueOf(holdClFormatyesno));
	}
	
	public SaveClFormatyesnoEnum getSaveClFormatyesno() {
		return saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(SaveClFormatyesnoEnum saveClFormatyesno) {
		this.saveClFormatyesno = saveClFormatyesno;
	}
	
	public void setSaveClFormatyesno(String saveClFormatyesno) {
		setSaveClFormatyesno(SaveClFormatyesnoEnum.valueOf(saveClFormatyesno));
	}
	
	public String getOutputQueue() {
		return outputQueue;
	}
	
	public void setOutputQueue(String outputQueue) {
		this.outputQueue = outputQueue;
	}
	
	public String getOutputQueueLibrary() {
		return outputQueueLibrary;
	}
	
	public void setOutputQueueLibrary(String outputQueueLibrary) {
		this.outputQueueLibrary = outputQueueLibrary;
	}
	
	public String getOverflowLineNumberText() {
		return overflowLineNumberText;
	}
	
	public void setOverflowLineNumberText(String overflowLineNumberText) {
		this.overflowLineNumberText = overflowLineNumberText;
	}
	
	public LocalDate getReadingRequiredDate() {
		return readingRequiredDate;
	}
	
	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
	}
	
	public void setReadingRequiredDate(String readingRequiredDate) {
		setReadingRequiredDate(new LocalDateConverter().convert(readingRequiredDate));
	}
	
	public YesOrNoEnum getCheckForHolidays() {
		return checkForHolidays;
	}
	
	public void setCheckForHolidays(YesOrNoEnum checkForHolidays) {
		this.checkForHolidays = checkForHolidays;
	}
	
	public void setCheckForHolidays(String checkForHolidays) {
		setCheckForHolidays(YesOrNoEnum.valueOf(checkForHolidays));
	}
}
