package au.awh.file.plantequipment.equipmentreadingswspltwmdfk;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.context.MessageSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import au.awh.model.GlobalContext;
// generateStatusFieldImportStatements BEGIN 11
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
// generateStatusFieldImportStatements END
import au.awh.model.ReloadSubfileEnum;

/**
 * State for file 'Plant Equipment' (WSEQP) and function 'Equipment Readings' (WSPLTWMDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
 @Configurable
public class EquipmentReadingsWspltwmdfkState extends EquipmentReadingsWspltwmdfkDTO {
	private static final long serialVersionUID = 5511298243662071098L;


	@Autowired
	private GlobalContext globalCtx;

	// Local fields
	private String lclDateListName = "";
	private String lclCentreCodeKey = "";
	private FirstPassEnum lclFirstPass = null;
	private BigDecimal lclDay = BigDecimal.ZERO;
	private ExitProgramOptionEnum lclExitProgramOption = ExitProgramOptionEnum._NORMAL;
	private BigDecimal lclReadingValue = BigDecimal.ZERO;
	private ReadingTypeEnum lclReadingType = ReadingTypeEnum._NOT_ENTERED;
	private LocalDate lclReadingRequiredDate = null;
	private EquipReadingCtrlStsEnum lclEquipReadingCtrlSts = null;

	// System fields
	private RecordSelectedEnum _sysRecordSelected = null;
	private ReReadSubfileRecordEnum _sysReReadSubfileRecord = null;
    private ReloadSubfileEnum _sysReloadSubfile = ReloadSubfileEnum._STA_NO;
    private boolean _sysErrorFound = false;

    public EquipmentReadingsWspltwmdfkState() {

    }

    public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
        super.addMessage(fieldId, messageId, messageSource);
        _sysErrorFound = true;
    }

    public int clearMessages() {
        _sysErrorFound = false;
        return super.clearMessages();
    }

	public void setWfReadingRequiredDate(LocalDate readingRequiredDate) {
		globalCtx.setLocalDate("readingRequiredDate", readingRequiredDate);
	}

	public LocalDate getWfReadingRequiredDate() {
		return globalCtx.getLocalDate("readingRequiredDate");
	}

	public void setLclCentreCodeKey(String centreCodeKey) {
    	this.lclCentreCodeKey = centreCodeKey;
    }

    public String getLclCentreCodeKey() {
    	return lclCentreCodeKey;
    }
	public void setLclDateListName(String dateListName) {
    	this.lclDateListName = dateListName;
    }

    public String getLclDateListName() {
    	return lclDateListName;
    }
	public void setLclDay(BigDecimal day) {
    	this.lclDay = day;
    }

    public BigDecimal getLclDay() {
    	return lclDay;
    }
	public void setLclEquipReadingCtrlSts(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
    	this.lclEquipReadingCtrlSts = equipReadingCtrlSts;
    }

    public EquipReadingCtrlStsEnum getLclEquipReadingCtrlSts() {
    	return lclEquipReadingCtrlSts;
    }
	public void setLclExitProgramOption(ExitProgramOptionEnum exitProgramOption) {
    	this.lclExitProgramOption = exitProgramOption;
    }

    public ExitProgramOptionEnum getLclExitProgramOption() {
    	return lclExitProgramOption;
    }
	public void setLclFirstPass(FirstPassEnum firstPass) {
    	this.lclFirstPass = firstPass;
    }

    public FirstPassEnum getLclFirstPass() {
    	return lclFirstPass;
    }
	public void setLclReadingRequiredDate(LocalDate readingRequiredDate) {
    	this.lclReadingRequiredDate = readingRequiredDate;
    }

    public LocalDate getLclReadingRequiredDate() {
    	return lclReadingRequiredDate;
    }
	public void setLclReadingType(ReadingTypeEnum readingType) {
    	this.lclReadingType = readingType;
    }

    public ReadingTypeEnum getLclReadingType() {
    	return lclReadingType;
    }
	public void setLclReadingValue(BigDecimal readingValue) {
    	this.lclReadingValue = readingValue;
    }

    public BigDecimal getLclReadingValue() {
    	return lclReadingValue;
    }

	public void set_SysReReadSubfileRecord(ReReadSubfileRecordEnum reReadSubfileRecord) {
    	_sysReReadSubfileRecord = reReadSubfileRecord;
    }

    public ReReadSubfileRecordEnum get_SysReReadSubfileRecord() {
    	return _sysReReadSubfileRecord;
    }

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
    	_sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
    	return _sysRecordSelected;
    }


    public void set_SysReloadSubfile(ReloadSubfileEnum reloadSubfile) {
        _sysReloadSubfile = reloadSubfile;
    }

    public ReloadSubfileEnum get_SysReloadSubfile() {
        return _sysReloadSubfile;
    }

    public boolean get_SysErrorFound() {
        return _sysErrorFound;
    }

    public void set_SysErrorFound(boolean errorFound) {
        _sysErrorFound = errorFound;
    }
}