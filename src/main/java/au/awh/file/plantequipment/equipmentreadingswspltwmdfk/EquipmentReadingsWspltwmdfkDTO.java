package au.awh.file.plantequipment.equipmentreadingswspltwmdfk;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.freschelegacy.utils.RestResponsePage;

import au.awh.model.GlobalContext;
import au.awh.file.plantequipment.PlantEquipment;
// generateStatusFieldImportStatements BEGIN 16
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.ReadingTypeEnum;
// generateStatusFieldImportStatements END
import au.awh.service.data.BaseDTO;

/**
 * Dto for file 'Plant Equipment' (WSEQP) and function 'Equipment Readings' (WSPLTWMDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */

public class EquipmentReadingsWspltwmdfkDTO extends BaseDTO {
	private static final long serialVersionUID = -2057769153993487618L;

    private RestResponsePage<EquipmentReadingsWspltwmdfkGDO> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";

	private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;
	private LocalDate readingRequiredDate = null;
	private String centreCodeKey = "";
	private String centreNameDrv = "";
	private String equipmentLeasor = "";
	private String awhRegionCode = "";
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
	private String equipmentTypeCode = "";
	private String plantEquipmentCode = "";


	private EquipmentReadingsWspltwmdfkGDO gdo;

    public EquipmentReadingsWspltwmdfkDTO() {

    }
    public void setPageGdo(RestResponsePage<EquipmentReadingsWspltwmdfkGDO> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<EquipmentReadingsWspltwmdfkGDO> getPageGdo() {
		return pageGdo;
	}

	public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSortData(String sortData) {
        this.sortData = sortData;
    }

    public String getSortData() {
        return sortData;
    }

	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
    }

    public BigDecimal getEquipReadingBatchNo() {
    	return equipReadingBatchNo;
    }

	public void setReadingRequiredDate(LocalDate readingRequiredDate) {
		this.readingRequiredDate = readingRequiredDate;
    }

    public LocalDate getReadingRequiredDate() {
    	return readingRequiredDate;
    }

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
    }

    public String getCentreCodeKey() {
    	return centreCodeKey;
    }

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
    }

    public String getCentreNameDrv() {
    	return centreNameDrv;
    }

	public void setEquipmentLeasor(String equipmentLeasor) {
		this.equipmentLeasor = equipmentLeasor;
    }

    public String getEquipmentLeasor() {
    	return equipmentLeasor;
    }

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
    }

    public String getAwhRegionCode() {
    	return awhRegionCode;
    }

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		this.awhBuisnessSegment = awhBuisnessSegment;
    }

    public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
    	return awhBuisnessSegment;
    }

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
    }

    public String getEquipmentTypeCode() {
    	return equipmentTypeCode;
    }

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
    }

    public String getPlantEquipmentCode() {
    	return plantEquipmentCode;
    }

	public void setGdo(EquipmentReadingsWspltwmdfkGDO gdo) {
		this.gdo = gdo;
	}

	public EquipmentReadingsWspltwmdfkGDO getGdo() {
		return gdo;
	}

}