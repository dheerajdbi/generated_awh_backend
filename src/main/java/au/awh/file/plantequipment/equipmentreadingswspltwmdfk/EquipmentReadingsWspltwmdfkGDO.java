package au.awh.file.plantequipment.equipmentreadingswspltwmdfk;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.BeanUtils;
import au.awh.file.plantequipment.PlantEquipment;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import au.awh.config.LocalDateDeserializer;
import au.awh.config.LocalDateSerializer;

// generateStatusFieldImportStatements BEGIN 25
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReadingFrequencyEnum;
// generateStatusFieldImportStatements END
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;


/**
 * Gdo for file 'Plant Equipment' (WSEQP) and function 'Equipment Readings' (WSPLTWMDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 *
 */
public class EquipmentReadingsWspltwmdfkGDO implements Serializable {
	private static final long serialVersionUID = 7667537318270812999L;

    private RecordSelectedEnum _sysRecordSelected = RecordSelectedEnum._STA_NO;
    private RecordDataChangedEnum _sysRecordDataChanged = RecordDataChangedEnum._STA_NO;
	private String _sysSelected = "";

	private String awhRegionCode = "";
	private String centreCodeKey = "";
	private AwhBuisnessSegmentEnum awhBuisnessSegment = null;
	private String equipmentTypeCode = "";
	private String displayPlantEquipCode = "";
	private String plantEquipmentCode = "";
	private String equipmentDescriptionDrv = "";
	private String equipmentDescription = "";
	private BigDecimal readingValue = BigDecimal.ZERO;
	private BigDecimal serviceReading = BigDecimal.ZERO;
	private String equipmentBarcode = "";
	private String equipmentComment = "";
	private EquipReadingUnitEnum equipReadingUnit = null;
	private EquipmentStatusEnum equipmentStatus = null;
	private String equipmentSerialNumber = "";
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate leaseComencmentDate = null;
	@JsonDeserialize(using= LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate leaseExpireDate = null;
	private BigDecimal leaseMonthlyRateS = BigDecimal.ZERO;
	private BigDecimal leaseMaxTotalUnits = BigDecimal.ZERO;
	private ReadingFrequencyEnum readingFrequency = null;
	private String visualEquipmentCode = "";
	private BigDecimal previousReadingValue = BigDecimal.ZERO;
	private String readingComment = "";
	private BigDecimal equipmentReadingSgt = BigDecimal.ZERO;
	private String equipmentLeasor = "";

	public EquipmentReadingsWspltwmdfkGDO() {

	}

   	public EquipmentReadingsWspltwmdfkGDO(String awhRegionCode, String centreCodeKey, AwhBuisnessSegmentEnum awhBuisnessSegment, String equipmentTypeCode, String plantEquipmentCode, String equipmentDescription, String equipmentBarcode, String equipmentComment, EquipReadingUnitEnum equipReadingUnit, EquipmentStatusEnum equipmentStatus, String equipmentSerialNumber, LocalDate leaseComencmentDate, LocalDate leaseExpireDate, BigDecimal leaseMonthlyRateS, BigDecimal leaseMaxTotalUnits, ReadingFrequencyEnum readingFrequency, String visualEquipmentCode, String equipmentLeasor) {
		this.awhRegionCode = awhRegionCode;
		this.centreCodeKey = centreCodeKey;
		this.awhBuisnessSegment = awhBuisnessSegment;
		this.equipmentTypeCode = equipmentTypeCode;
		this.plantEquipmentCode = plantEquipmentCode;
		this.equipmentDescription = equipmentDescription;
		this.equipmentBarcode = equipmentBarcode;
		this.equipmentComment = equipmentComment;
		this.equipReadingUnit = equipReadingUnit;
		this.equipmentStatus = equipmentStatus;
		this.equipmentSerialNumber = equipmentSerialNumber;
		this.leaseComencmentDate = leaseComencmentDate;
		this.leaseExpireDate = leaseExpireDate;
		this.leaseMonthlyRateS = leaseMonthlyRateS;
		this.leaseMaxTotalUnits = leaseMaxTotalUnits;
		this.readingFrequency = readingFrequency;
		this.visualEquipmentCode = visualEquipmentCode;
		this.equipmentLeasor = equipmentLeasor;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setAwhRegionCode(String awhRegionCode) {
    	this.awhRegionCode = awhRegionCode;
    }

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public void setCentreCodeKey(String centreCodeKey) {
    	this.centreCodeKey = centreCodeKey;
    }

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public void setAwhBuisnessSegment(AwhBuisnessSegmentEnum awhBuisnessSegment) {
    	this.awhBuisnessSegment = awhBuisnessSegment;
    }

	public AwhBuisnessSegmentEnum getAwhBuisnessSegment() {
		return awhBuisnessSegment;
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
    	this.equipmentTypeCode = equipmentTypeCode;
    }

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public void setDisplayPlantEquipCode(String displayPlantEquipCode) {
    	this.displayPlantEquipCode = displayPlantEquipCode;
    }

	public String getDisplayPlantEquipCode() {
		return displayPlantEquipCode;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
    	this.plantEquipmentCode = plantEquipmentCode;
    }

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public void setEquipmentDescriptionDrv(String equipmentDescriptionDrv) {
    	this.equipmentDescriptionDrv = equipmentDescriptionDrv;
    }

	public String getEquipmentDescriptionDrv() {
		return equipmentDescriptionDrv;
	}

	public void setEquipmentDescription(String equipmentDescription) {
    	this.equipmentDescription = equipmentDescription;
    }

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public void setReadingValue(BigDecimal readingValue) {
    	this.readingValue = readingValue;
    }

	public BigDecimal getReadingValue() {
		return readingValue;
	}

	public void setServiceReading(BigDecimal serviceReading) {
    	this.serviceReading = serviceReading;
    }

	public BigDecimal getServiceReading() {
		return serviceReading;
	}

	public void setEquipmentBarcode(String equipmentBarcode) {
    	this.equipmentBarcode = equipmentBarcode;
    }

	public String getEquipmentBarcode() {
		return equipmentBarcode;
	}

	public void setEquipmentComment(String equipmentComment) {
    	this.equipmentComment = equipmentComment;
    }

	public String getEquipmentComment() {
		return equipmentComment;
	}

	public void setEquipReadingUnit(EquipReadingUnitEnum equipReadingUnit) {
    	this.equipReadingUnit = equipReadingUnit;
    }

	public EquipReadingUnitEnum getEquipReadingUnit() {
		return equipReadingUnit;
	}

	public void setEquipmentStatus(EquipmentStatusEnum equipmentStatus) {
    	this.equipmentStatus = equipmentStatus;
    }

	public EquipmentStatusEnum getEquipmentStatus() {
		return equipmentStatus;
	}

	public void setEquipmentSerialNumber(String equipmentSerialNumber) {
    	this.equipmentSerialNumber = equipmentSerialNumber;
    }

	public String getEquipmentSerialNumber() {
		return equipmentSerialNumber;
	}

	public void setLeaseComencmentDate(LocalDate leaseComencmentDate) {
    	this.leaseComencmentDate = leaseComencmentDate;
    }

	public LocalDate getLeaseComencmentDate() {
		return leaseComencmentDate;
	}

	public void setLeaseExpireDate(LocalDate leaseExpireDate) {
    	this.leaseExpireDate = leaseExpireDate;
    }

	public LocalDate getLeaseExpireDate() {
		return leaseExpireDate;
	}

	public void setLeaseMonthlyRateS(BigDecimal leaseMonthlyRateS) {
    	this.leaseMonthlyRateS = leaseMonthlyRateS;
    }

	public BigDecimal getLeaseMonthlyRateS() {
		return leaseMonthlyRateS;
	}

	public void setLeaseMaxTotalUnits(BigDecimal leaseMaxTotalUnits) {
    	this.leaseMaxTotalUnits = leaseMaxTotalUnits;
    }

	public BigDecimal getLeaseMaxTotalUnits() {
		return leaseMaxTotalUnits;
	}

	public void setReadingFrequency(ReadingFrequencyEnum readingFrequency) {
    	this.readingFrequency = readingFrequency;
    }

	public ReadingFrequencyEnum getReadingFrequency() {
		return readingFrequency;
	}

	public void setVisualEquipmentCode(String visualEquipmentCode) {
    	this.visualEquipmentCode = visualEquipmentCode;
    }

	public String getVisualEquipmentCode() {
		return visualEquipmentCode;
	}

	public void setPreviousReadingValue(BigDecimal previousReadingValue) {
    	this.previousReadingValue = previousReadingValue;
    }

	public BigDecimal getPreviousReadingValue() {
		return previousReadingValue;
	}

	public void setReadingComment(String readingComment) {
    	this.readingComment = readingComment;
    }

	public String getReadingComment() {
		return readingComment;
	}

	public void setEquipmentReadingSgt(BigDecimal equipmentReadingSgt) {
    	this.equipmentReadingSgt = equipmentReadingSgt;
    }

	public BigDecimal getEquipmentReadingSgt() {
		return equipmentReadingSgt;
	}

	public void setEquipmentLeasor(String equipmentLeasor) {
    	this.equipmentLeasor = equipmentLeasor;
    }

	public String getEquipmentLeasor() {
		return equipmentLeasor;
	}

	public void set_SysRecordSelected(RecordSelectedEnum recordSelected) {
        _sysRecordSelected = recordSelected;
    }

    public RecordSelectedEnum get_SysRecordSelected() {
        return _sysRecordSelected;
    }

    public void set_SysRecordDataChanged(RecordDataChangedEnum recordDataChanged) {
		_sysRecordDataChanged = recordDataChanged;
	}

	public RecordDataChangedEnum get_SysRecordDataChanged() {
		return _sysRecordDataChanged;
	}

}