package au.awh.file.plantequipment.equipmentreadingswspltwmdfk;

import java.io.Serializable;
import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END


/**
 * Parameter(s) for resource: EquipmentReadingsWspltwmdfk (WSPLTWMDFK).
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
public class EquipmentReadingsWspltwmdfkParams implements Serializable
{
	private static final long serialVersionUID = -6169659574633609367L;

	private BigDecimal equipReadingBatchNo = BigDecimal.ZERO;


	public BigDecimal getEquipReadingBatchNo() {
		return equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(BigDecimal equipReadingBatchNo) {
		this.equipReadingBatchNo = equipReadingBatchNo;
	}
	
	public void setEquipReadingBatchNo(String equipReadingBatchNo) {
		setEquipReadingBatchNo(new BigDecimal(equipReadingBatchNo));
	}

}