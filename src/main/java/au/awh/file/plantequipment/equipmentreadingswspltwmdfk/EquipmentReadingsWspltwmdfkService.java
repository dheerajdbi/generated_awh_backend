package au.awh.file.plantequipment.equipmentreadingswspltwmdfk;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import au.awh.utils.BuildInFunctionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.plantequipment.PlantEquipmentRepository;
import au.awh.service.ServiceException;
import au.awh.support.JobContext;

// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListService;
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusService;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlService;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsService;
import au.awh.file.equipmentreadings.chgcrteequipmentreading.ChgcrteEquipmentReadingService;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsService;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateService;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsService;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateService;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsService;
// asServiceDtoImportStatement
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListDTO;
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusDTO;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlDTO;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.chgcrteequipmentreading.ChgcrteEquipmentReadingDTO;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateDTO;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsDTO;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateDTO;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsDTO;
// asServiceParamsImportStatement
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListParams;
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusParams;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlParams;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsParams;
import au.awh.file.equipmentreadings.chgcrteequipmentreading.ChgcrteEquipmentReadingParams;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsParams;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateParams;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsParams;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateParams;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsParams;
// asServiceResultImportStatement
import au.awh.file.datelistheader.getcentreholidaylist.GetCentreHolidayListResult;
import au.awh.file.equipmentreadingcontrol.chgestatus.ChgeStatusResult;
import au.awh.file.equipmentreadingcontrol.geteequipreadingctrl.GeteEquipReadingCtrlResult;
import au.awh.file.equipmentreadings.addemequipmentreadings.AddemEquipmentReadingsResult;
import au.awh.file.equipmentreadings.chgcrteequipmentreading.ChgcrteEquipmentReadingResult;
import au.awh.file.equipmentreadings.edteequipmentreadings.EdteEquipmentReadingsResult;
import au.awh.file.equipmentreadings.getebyreversedate.GeteByReverseDateResult;
import au.awh.file.equipmentreadings.geteequipmentreadings.GeteEquipmentReadingsResult;
import au.awh.file.equipmentreadings.getereadingbydate.GeteReadingByDateResult;
import au.awh.file.plantequipment.chkreadingsbatchsts.ChkReadingsBatchStsResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


// generateImportsForEnum
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordDataChangedEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.ProgramModeEnum;
// generateStatusFieldImportStatements BEGIN 35
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.CmdKeyEnum;
import au.awh.model.DateDetailTypeEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.DurationTypeEnum;
import au.awh.model.EquipReadingCtrlStsEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.FirstPassEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OverrideEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReReadSubfileRecordEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReadingTypeEnum;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.SelectedDaysDatesEnum;
// generateStatusFieldImportStatements END

/**
 * DSPFIL Service controller for 'Equipment Readings' (WSPLTWMDFK) of file 'Plant Equipment' (WSEQP)
 *
 * @author X2EGenerator DSPFILJavaControllerGenerator.kt
 */
@Service
public class EquipmentReadingsWspltwmdfkService extends AbstractService<EquipmentReadingsWspltwmdfkService, EquipmentReadingsWspltwmdfkState>
{
    
	@Autowired
	private JobContext job;

    @Autowired
    private PlantEquipmentRepository plantEquipmentRepository;
    
    @Autowired
    private AddemEquipmentReadingsService addemEquipmentReadingsService;
    
    @Autowired
    private ChgcrteEquipmentReadingService chgcrteEquipmentReadingService;
    
    @Autowired
    private ChgeStatusService chgeStatusService;
    
    @Autowired
    private ChkReadingsBatchStsService chkReadingsBatchStsService;
    
    @Autowired
    private EdteEquipmentReadingsService edteEquipmentReadingsService;
    
    @Autowired
    private GetCentreHolidayListService getCentreHolidayListService;
    
    @Autowired
    private GeteByReverseDateService geteByReverseDateService;
    
    @Autowired
    private GeteEquipReadingCtrlService geteEquipReadingCtrlService;
    
    @Autowired
    private GeteEquipmentReadingsService geteEquipmentReadingsService;
    
    @Autowired
    private GeteReadingByDateService geteReadingByDateService;
    

    @Autowired
    private MessageSource messageSource;
    
	public static final String SCREEN_CTL = "equipmentReadingsWspltwmdfk";
    public static final String SCREEN_RCD = "EquipmentReadingsWspltwmdfk.rcd";

    /**
     * Controller prototype steps.
     */
    private final Step execute = define("execute", EquipmentReadingsWspltwmdfkParams.class, this::executeService);
    private final Step response = define("response", EquipmentReadingsWspltwmdfkDTO.class, this::processResponse);

	//private final Step serviceGeteReadingByDate = define("serviceGeteReadingByDate",GeteReadingByDateResult.class, this::processServiceGeteReadingByDate);
	//private final Step serviceGeteByReverseDate = define("serviceGeteByReverseDate",GeteByReverseDateResult.class, this::processServiceGeteByReverseDate);
	//private final Step serviceGeteEquipReadingCtrl = define("serviceGeteEquipReadingCtrl",GeteEquipReadingCtrlResult.class, this::processServiceGeteEquipReadingCtrl);
	//private final Step serviceChgcrteEquipmentReading = define("serviceChgcrteEquipmentReading",ChgcrteEquipmentReadingResult.class, this::processServiceChgcrteEquipmentReading);
	//private final Step serviceChkReadingsBatchSts = define("serviceChkReadingsBatchSts",ChkReadingsBatchStsResult.class, this::processServiceChkReadingsBatchSts);
	//private final Step serviceChgeStatus = define("serviceChgeStatus",ChgeStatusResult.class, this::processServiceChgeStatus);
	//private final Step serviceEdteEquipmentReadings = define("serviceEdteEquipmentReadings",EdteEquipmentReadingsResult.class, this::processServiceEdteEquipmentReadings);
	//private final Step serviceGeteEquipmentReadings = define("serviceGeteEquipmentReadings",GeteEquipmentReadingsResult.class, this::processServiceGeteEquipmentReadings);
	//private final Step serviceAddemEquipmentReadings = define("serviceAddemEquipmentReadings",AddemEquipmentReadingsResult.class, this::processServiceAddemEquipmentReadings);
	//private final Step serviceGetCentreHolidayList = define("serviceGetCentreHolidayList",GetCentreHolidayListResult.class, this::processServiceGetCentreHolidayList);
	

    
    @Autowired
    public EquipmentReadingsWspltwmdfkService() {
        super(EquipmentReadingsWspltwmdfkService.class, EquipmentReadingsWspltwmdfkState.class);
    }

    @Override
    public Step getInitialStep() {
        return execute;
    }

    /**
     * DSPFIL controller starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(EquipmentReadingsWspltwmdfkState state, EquipmentReadingsWspltwmdfkParams params) {
    	StepResult stepResult = NO_ACTION;

        if(params != null) {
    	    BeanUtils.copyProperties(params, state);
        }
        stepResult = usrInitializeProgram(state);

        stepResult = mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_CTL initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(EquipmentReadingsWspltwmdfkState state) {
        StepResult stepResult = NO_ACTION;

        stepResult = loadFirstSubfilePage(state);

        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * Load pageGdo
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadFirstSubfilePage(EquipmentReadingsWspltwmdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	stepResult = usrInitializeSubfileControl(state);

		dbfReadFirstDataRecord(state);
		/*if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
		    stepResult = loadNextSubfilePage(state);
		}*/

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult loadNextSubfilePage(EquipmentReadingsWspltwmdfkState state) {
    	StepResult stepResult = NO_ACTION;

    	List<EquipmentReadingsWspltwmdfkGDO> list = state.getPageGdo().getContent();
        for (EquipmentReadingsWspltwmdfkGDO gdo : list) {
            gdo.set_SysRecordSelected(RecordSelectedEnum._STA_YES);
            stepResult = usrInitializeSubfileRecordFromDBFRecord(state, gdo);
//            TODO:moveDbfRecordFieldsToSubfileRecord(state);  // synon built-in function
            if (gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
//            TODO:writeSubfileRecord(state);  // synon built-in function
            }
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(EquipmentReadingsWspltwmdfkState state) {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            EquipmentReadingsWspltwmdfkDTO model = new EquipmentReadingsWspltwmdfkDTO();
            BeanUtils.copyProperties(state, model);
            stepResult = callScreen(SCREEN_CTL, model).thenCall(response);
        }

        return stepResult;
    }

    /**
     * SCREEN_CTL returned response processing method.
     * @param state - Service state class.
     * @param model - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(EquipmentReadingsWspltwmdfkState state, EquipmentReadingsWspltwmdfkDTO model) {
    	StepResult stepResult = NO_ACTION;

        // update state from vm and use state (not vm) as processResponseToKeyScreen() argument.
        BeanUtils.copyProperties(model, state);

        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isCancel(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        } else if (isReset(state.get_SysCmdKey())) {
            state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
        } else if (isHelp(state.get_SysCmdKey())) {
//        TODO:processHelpRequest(state);//synon built-in function
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextPageRecord(state);
            stepResult = loadNextSubfilePage(state);
        } else {
            stepResult = processScreen(state);
        }

        return stepResult;
    }

    /**
     * SCREEN process screen.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult processScreen(EquipmentReadingsWspltwmdfkState state) {
    	StepResult stepResult = NO_ACTION;

        stepResult = usrProcessSubfilePreConfirm(state);
        if ( stepResult != NO_ACTION) {
            return stepResult;
        }

        if (state.get_SysErrorFound()) {
            return closedown(state);
        } else {
            if(state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
                return closedown(state);
            } else {
//	        	if(!state.get_SysProgramConfirm().getCode().equals(ProgramConfirmEnum._STA_YES.getCode())) {
//	        		return closedown(state);
//		        } else {
//		        	stepResult = usrProcessCommandKeys(state);
//		        }

                stepResult = usrProcessSubfileControlPostConfirm(state);
                for (EquipmentReadingsWspltwmdfkGDO gdo : state.getPageGdo().getContent()) {
                    if(gdo.get_SysRecordDataChanged().getCode().equals(RecordDataChangedEnum._STA_YES.getCode())) {
                        stepResult = usrProcessSubfileRecordPostConfirm(state, gdo);
                        
//	                    TODO:writeSubfileRecord(state);   // synon built-in function
		            }
		        }
		        stepResult = usrFinalProcessingPostConfirm(state);
                stepResult = usrProcessCommandKeys(state);
            }
        }

        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN Process subfile Pre Confirm.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult usrProcessSubfilePreConfirm(EquipmentReadingsWspltwmdfkState state) {
    	StepResult stepResult = NO_ACTION;

	stepResult = usrSubfileControlFunctionFields(state);
        stepResult = usrProcessSubfileControlPreConfirm(state);
    	if(!state.get_SysReloadSubfile().getCode().equals(ReloadSubfileEnum._STA_YES.getCode())) {
            for (EquipmentReadingsWspltwmdfkGDO gdo : state.getPageGdo().getContent()) {
                
                if(gdo.get_SysRecordSelected().getCode().equals(RecordSelectedEnum._STA_YES.getCode())) {
                    stepResult = usrSubfileRecordFunctionFields(state, gdo);
                    stepResult = usrProcessSubfileRecordPreConfirm(state, gdo);
                    if (stepResult != NO_ACTION) {
                        return stepResult;
                    }
//                  TODO:writeSubfileRecord(state);   // synon built-in function
                }
                
            }
        }

	stepResult = usrFinalProcessingPreConfirm(state);

        return stepResult;
    }

    /**
     * Terminate this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(EquipmentReadingsWspltwmdfkState state) {
        StepResult stepResult = NO_ACTION;

        state.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
        stepResult = usrExitProgramProcessing(state);

        EquipmentReadingsWspltwmdfkResult params = new EquipmentReadingsWspltwmdfkResult();
        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(params);

        return stepResult;
    }
    
    /* ------------------------- Generated DBF method --------------------------- */

    private void dbfReadFirstDataRecord(EquipmentReadingsWspltwmdfkState state) {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page.
     * @param state - Service state class.
     */
	private void dbfReadNextPageRecord(EquipmentReadingsWspltwmdfkState state)
	{
		state.setPage(state.getPage() + 1);
		dbfReadDataRecord(state);
	}

    private void dbfReadDataRecord(EquipmentReadingsWspltwmdfkState state) {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet()) {
                if (entry.getValue() == null) {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe) {
        }

        if (CollectionUtils.isEmpty(sortOrders)) {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
        }

        RestResponsePage<EquipmentReadingsWspltwmdfkGDO> pageGdo = plantEquipmentRepository.equipmentReadingsWspltwmdfk(state.getEquipReadingBatchNo(), state.getAwhBuisnessSegment(), state.getEquipmentTypeCode(), state.getPlantEquipmentCode(), pageable);
        state.setPageGdo(pageGdo);
    }
    
	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteEquipReadingCtrlParams geteEquipReadingCtrlParams = null;
			GeteEquipReadingCtrlResult geteEquipReadingCtrlResult = null;
			// DEBUG genFunctionCall BEGIN 1000128 ACT GetE Equip Reading Ctrl - Equipment Reading Control  *
			geteEquipReadingCtrlParams = new GeteEquipReadingCtrlParams();
			geteEquipReadingCtrlResult = new GeteEquipReadingCtrlResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteEquipReadingCtrlParams);
			geteEquipReadingCtrlParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			stepResult = geteEquipReadingCtrlService.execute(geteEquipReadingCtrlParams);
			geteEquipReadingCtrlResult = (GeteEquipReadingCtrlResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteEquipReadingCtrlResult.get_SysReturnCode());
			dto.setAwhRegionCode(geteEquipReadingCtrlResult.getAwhRegionCode());
			dto.setCentreCodeKey(geteEquipReadingCtrlResult.getCentreCodeKey());
			dto.setReadingRequiredDate(geteEquipReadingCtrlResult.getReadingRequiredDate());
			dto.setLclEquipReadingCtrlSts(geteEquipReadingCtrlResult.getEquipReadingCtrlSts());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000045 ACT LCL.Reading Required Date = PAR.Reading Required Date + CON.-1 *DAYS
			dto.setLclReadingRequiredDate(BuildInFunctionUtils.getDateIncrement(
			dto.getLclReadingRequiredDate(), -1, "DY", "1111111", null, Boolean.FALSE, Boolean.FALSE));
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Control (Generated:182)
	 */
    private StepResult usrInitializeSubfileControl(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000193 ACT Centre Name Drv          *FIELD                                             CTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             CTLC 1000193
			// DEBUG X2EActionDiagramElement 1000193 ACT     Centre Name Drv          *FIELD                                             CTLC
			// DEBUG X2EActionDiagramElement 1000194 PAR CTL 
			// DEBUG X2EActionDiagramElement 1000195 PAR CTL 
			// DEBUG genFunctionCall END
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Initialize Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrInitializeSubfileRecordFromDBFRecord(EquipmentReadingsWspltwmdfkState dto, EquipmentReadingsWspltwmdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteByReverseDateParams geteByReverseDateParams = null;
			GeteByReverseDateResult geteByReverseDateResult = null;
			GetCentreHolidayListResult getCentreHolidayListResult = null;
			GeteReadingByDateParams geteReadingByDateParams = null;
			GeteReadingByDateResult geteReadingByDateResult = null;
			GetCentreHolidayListParams getCentreHolidayListParams = null;
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			!gdo.getCentreCodeKey().equals(dto.getLclCentreCodeKey())
			// DEBUG ComparatorJavaGenerator END
			) {
				// DB1.Centre Code           KEY NE LCL.Centre Code           KEY
				// DEBUG genFunctionCall BEGIN 1000492 ACT LCL.Centre Code           KEY = DB1.Centre Code           KEY
				dto.setLclCentreCodeKey(gdo.getCentreCodeKey());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000500 ACT Get Centre Holiday List - *Date List Header  *
				//getCentreHolidayListParams = new GetCentreHolidayListParams();
				//getCentreHolidayListResult = new GetCentreHolidayListResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				//BeanUtils.copyProperties(dto, getCentreHolidayListParams);
				//getCentreHolidayListParams.setCentreCodeKey(dto.getCentreCodeKey());
				//stepResult = getCentreHolidayListService.execute(getCentreHolidayListParams);
				//getCentreHolidayListResult = (GetCentreHolidayListResult)((ReturnFromService)stepResult.getAction()).getResults();
				//dto.set_SysReturnCode(getCentreHolidayListResult.get_SysReturnCode());
				//dto.setLclDateListName(getCentreHolidayListResult.getDateListName());
				// DEBUG genFunctionCall END
			}
			// * Get Initial Reading If not found (Also Transfer in) then not required.
			// DEBUG genFunctionCall BEGIN 1000668 ACT LCL.Reading Type = CND.Initial reading
			dto.setLclReadingType(ReadingTypeEnum._INITIAL_READING);
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000672 ACT GetE By reverse date - Equipment Readings  * Initial
			geteByReverseDateParams = new GeteByReverseDateParams();
			geteByReverseDateResult = new GeteByReverseDateResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, geteByReverseDateParams);
			geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
			geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			geteByReverseDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			geteByReverseDateParams.setReadingType(dto.getLclReadingType());
			//geteByReverseDateParams.setErrorProcessing("2");
			//geteByReverseDateParams.setGetRecordOption("I");
			stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
			geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
			dto.setLclReadingType(geteByReverseDateResult.getReadingType());
			//dto.setWfReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
			// DEBUG genFunctionCall END
			if (dto.getWfReadingRequiredDate().equals(LocalDate.parse("0001-01-01", DateTimeFormatter.BASIC_ISO_DATE))) {
				// WRK.Reading Required Date is Not Entered
				// DEBUG genFunctionCall BEGIN 1000696 ACT LCL.Reading Type = CND.Transfer In Reading
				dto.setLclReadingType(ReadingTypeEnum._TRANSFER_IN_READING);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000700 ACT GetE By reverse date - Equipment Readings  * Initial
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
				geteByReverseDateParams.setReadingRequiredDate(job.getSystemTimestamp().toLocalDate());
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				geteByReverseDateParams.setErrorProcessing("2");
				geteByReverseDateParams.setGetRecordOption("I");
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				dto.setWfReadingRequiredDate(geteByReverseDateResult.getReadingRequiredDate());
				// DEBUG genFunctionCall END
			}
			if (!(EquipmentStatusEnum.isNotOnSite(gdo.getEquipmentStatus())) && (
			// DEBUG ComparatorJavaGenerator BEGIN
			gdo.getCentreCodeKey().equals(dto.getCentreCodeKey())
			// DEBUG ComparatorJavaGenerator END
			) && (!dto.getWfReadingRequiredDate().equals(DateTimeUtils.toLocalDate(0001-01-01)))) {
				// * See if reading already entered for this date
				// DEBUG genFunctionCall BEGIN 1000006 ACT GetE Reading by Date - Equipment Readings  *
				geteReadingByDateParams = new GeteReadingByDateParams();
				geteReadingByDateResult = new GeteReadingByDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteReadingByDateParams);
				geteReadingByDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
				geteReadingByDateParams.setReadingType("ST");
				geteReadingByDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
				geteReadingByDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				geteReadingByDateParams.setErrorProcessing("2");
				geteReadingByDateParams.setGetRecordOption("I");
				geteReadingByDateParams.setCentreCodeKey(gdo.getCentreCodeKey());
				stepResult = geteReadingByDateService.execute(geteReadingByDateParams);
				geteReadingByDateResult = (GeteReadingByDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteReadingByDateResult.get_SysReturnCode());
				gdo.setEquipmentReadingSgt(geteReadingByDateResult.getEquipmentReadingSgt());
				gdo.setReadingValue(geteReadingByDateResult.getReadingValue());
				gdo.setReadingComment(geteReadingByDateResult.getReadingComment());
				dto.setLclReadingRequiredDate(geteReadingByDateResult.getReadingRequiredDate());
				// DEBUG genFunctionCall END
				// * Get previous reading.
				// DEBUG genFunctionCall BEGIN 1000306 ACT LCL.Reading Type = CND.Any
				dto.setLclReadingType(ReadingTypeEnum._ANY);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000025 ACT GetE By reverse date - Equipment Readings  *
				geteByReverseDateParams = new GeteByReverseDateParams();
				geteByReverseDateResult = new GeteByReverseDateResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
				geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
				geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
				geteByReverseDateParams.setErrorProcessing("2");
				geteByReverseDateParams.setGetRecordOption("I");
				geteByReverseDateParams.setReadingType(dto.getLclReadingType());
				geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
				stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
				geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
				gdo.setPreviousReadingValue(geteByReverseDateResult.getReadingValue());
				dto.setLclReadingType(geteByReverseDateResult.getReadingType());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000410 ACT PGM.*Re-read Subfile Record = CND.*YES
				dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum._STA_YES);
				// DEBUG genFunctionCall END
				if ((dto.getLclFirstPass() == FirstPassEnum._FIRST_PASS) && !(isNextPage(dto.get_SysCmdKey()))) {
					// DEBUG genFunctionCall BEGIN 1000162 ACT *Set Cursor: RCD.Reading Value  (*Override=*YES)
					// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000166 ACT LCL.First Pass = CND.Not First Pass
					dto.setLclFirstPass(FirstPassEnum._NOT_FIRST_PASS);
					// DEBUG genFunctionCall END
					// 
				} else if ((dto.getLclFirstPass() == FirstPassEnum._NOT_FIRST_PASS) && (isNextPage(dto.get_SysCmdKey()))) {
					// DEBUG genFunctionCall BEGIN 1000177 ACT *Set Cursor: RCD.Reading Value  (*Override=*YES)
					// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000181 ACT LCL.First Pass = CND.First Pass
					dto.setLclFirstPass(FirstPassEnum._FIRST_PASS);
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000389 ACT RCD.Service reading = RCD.Reading Value
				gdo.setServiceReading(gdo.getReadingValue());
				// DEBUG genFunctionCall END
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000233 ACT PGM.*Record selected = CND.*NO
				dto.set_SysRecordSelected(RecordSelectedEnum._STA_NO);
				// DEBUG genFunctionCall END
			}
			if (dto.get_SysRecordSelected() == RecordSelectedEnum._STA_YES) {
				// PGM.*Record selected is *YES
				if (gdo.getReadingFrequency() == ReadingFrequencyEnum._DAILY) {
					// DB1.Reading Frequency is Daily
				} else {
					// *OTHERWISE
					if (gdo.getReadingFrequency() == ReadingFrequencyEnum._MONTHLY) {
						// DB1.Reading Frequency is Monthly
						// DEBUG genFunctionCall BEGIN 1000528 ACT LCL.Day = PAR.Reading Required Date *DAY OF MONTH
						// TODO: The '*DATE DETAIL' built-in Function is not supported yet
						// DEBUG genFunctionCall END
						if (dto.getLclDay().intValue() > 1) {
							// LCL.Day is GT 1
							// DEBUG genFunctionCall BEGIN 1000540 ACT PGM.*Record selected = CND.*NO
							dto.set_SysRecordSelected(RecordSelectedEnum._STA_NO);
							// DEBUG genFunctionCall END
						}
					} else if (gdo.getReadingFrequency() == ReadingFrequencyEnum._WEEKLY) {
						// DB1.Reading Frequency is Weekly
						// DEBUG genFunctionCall BEGIN 1000546 ACT LCL.Day = PAR.Reading Required Date *DAY OF WEEK
						// TODO: The '*DATE DETAIL' built-in Function is not supported yet
						// DEBUG genFunctionCall END
						if (dto.getLclDay().intValue() > 1) {
							// LCL.Day is GT 1
							// DEBUG genFunctionCall BEGIN 1000558 ACT PGM.*Record selected = CND.*NO
							dto.set_SysRecordSelected(RecordSelectedEnum._STA_NO);
							// DEBUG genFunctionCall END
						}
					}
				}
			}
			if (dto.get_SysRecordSelected() == RecordSelectedEnum._STA_YES) {
				// PGM.*Record selected is *YES
				if (!gdo.getVisualEquipmentCode().equals("")) {
					// RCD.Visual Equipment Code is Entered
					// DEBUG genFunctionCall BEGIN 1000596 ACT RCD.Display Plant equip Code = RCD.Visual Equipment Code
					gdo.setDisplayPlantEquipCode(String.format("%-10s", gdo.getVisualEquipmentCode()));
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000638 ACT RCD.Display Plant equip Code = RCD.Plant Equipment Code
					gdo.setDisplayPlantEquipCode(gdo.getPlantEquipmentCode());
					// DEBUG genFunctionCall END
				}
			}
        	
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Control Function Fields (Generated:175)
	 */
    private StepResult usrSubfileControlFunctionFields(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000002 ACT Centre Name Drv          *FIELD                                             CTLC
			// TODO: NEED TO SUPPORT *FIELD Centre Name Drv          *FIELD                                             CTLC 1000002
			// DEBUG X2EActionDiagramElement 1000002 ACT     Centre Name Drv          *FIELD                                             CTLC
			// DEBUG X2EActionDiagramElement 1000003 PAR CTL 
			// DEBUG X2EActionDiagramElement 1000004 PAR CTL 
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Pre-Confirm) (Generated:72)
	 */
    private StepResult usrProcessSubfileControlPreConfirm(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 72 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Subfile Record Function Fields (Generated:170)
	 */
    private StepResult usrSubfileRecordFunctionFields(EquipmentReadingsWspltwmdfkState dto, EquipmentReadingsWspltwmdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000469 ACT Equipment Description Drv*FIELD                                             RCDE
			// TODO: NEED TO SUPPORT *FIELD Equipment Description Drv*FIELD                                             RCDE 1000469
			// DEBUG X2EActionDiagramElement 1000469 ACT     Equipment Description Drv*FIELD                                             RCDE
			// DEBUG X2EActionDiagramElement 1000470 PAR RCD 
			// DEBUG X2EActionDiagramElement 1000471 PAR RCD 
			// DEBUG X2EActionDiagramElement 1000472 PAR RCD 
			// ERROR UNEXPECTED NUMBER OF PARAMETERS
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Pre-Confirm) (Generated:101)
	 */
    private StepResult usrProcessSubfileRecordPreConfirm(EquipmentReadingsWspltwmdfkState dto, EquipmentReadingsWspltwmdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// DEBUG genFunctionCall BEGIN 1000420 ACT PGM.*Re-read Subfile Record = CND.*YES
			dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum._STA_YES);
			// DEBUG genFunctionCall END
			if ((
			// DEBUG ComparatorJavaGenerator BEGIN
			gdo.getReadingValue().intValue() < gdo.getPreviousReadingValue().intValue()
			// DEBUG ComparatorJavaGenerator END
			) && (gdo.getEquipmentReadingSgt() != BigDecimal.ZERO) || (
			// DEBUG ComparatorJavaGenerator BEGIN
			gdo.getReadingValue().intValue() < gdo.getPreviousReadingValue().intValue()
			// DEBUG ComparatorJavaGenerator END
			) && (gdo.getReadingValue() != BigDecimal.ZERO)) {
				// DEBUG genFunctionCall BEGIN 1000275 ACT Send error message - 'Reading must be more prev'
				//dto.addMessage("readingValue", "reading.must.be.more.prev", messageSource);
				// DEBUG genFunctionCall END
			} else if ((gdo.getEquipReadingUnit() == EquipReadingUnitEnum._HOURS) && (gdo.getPreviousReadingValue() != BigDecimal.ZERO)) {
				// DEBUG genFunctionCall BEGIN 1000256 ACT LCL.Reading Value = RCD.Reading Value - RCD.Previous Reading Value
				dto.setLclReadingValue(gdo.getReadingValue().subtract(gdo.getPreviousReadingValue()));
				// DEBUG genFunctionCall END
				if ((dto.getLclReadingValue().intValue() > 250) && (gdo.getReadingFrequency() == ReadingFrequencyEnum._MONTHLY)) {
					// DEBUG genFunctionCall BEGIN 1000612 ACT Send information message - 'Usage is > 250 hrs'
					//dto.addMessage("readingValue", "usage.is.>.250.hrs", messageSource);
					// DEBUG genFunctionCall END
				} else if ((dto.getLclReadingValue().intValue() > 24) && (gdo.getReadingFrequency() == ReadingFrequencyEnum._DAILY)) {
					// DEBUG genFunctionCall BEGIN 1000623 ACT Send information message - 'Usage > 24 hrs'
					//dto.addMessage("readingValue", "usage.>.24.hrs", messageSource);
					// DEBUG genFunctionCall END
				} else if ((dto.getLclReadingValue().intValue() > 100) && (gdo.getReadingFrequency() == ReadingFrequencyEnum._WEEKLY)) {
					// DEBUG genFunctionCall BEGIN 1000268 ACT Send information message - 'Usage > 24 hrs'
					//dto.addMessage("readingValue", "usage.>.24.hrs", messageSource);
					// DEBUG genFunctionCall END
				}
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Pre-confirm) (Generated:222)
	 */
    private StepResult usrFinalProcessingPreConfirm(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 222 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process subfile control (Post-confirm) (Generated:225)
	 */
    private StepResult usrProcessSubfileControlPostConfirm(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 225 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Record (Post-Confirm Pass) (Generated:209)
	 */
    private StepResult usrProcessSubfileRecordPostConfirm(EquipmentReadingsWspltwmdfkState dto, EquipmentReadingsWspltwmdfkGDO gdo) {
        StepResult stepResult = NO_ACTION;

        try {
        	GeteEquipmentReadingsParams geteEquipmentReadingsParams = null;
			EdteEquipmentReadingsResult edteEquipmentReadingsResult = null;
			GeteEquipmentReadingsResult geteEquipmentReadingsResult = null;
			AddemEquipmentReadingsParams addemEquipmentReadingsParams = null;
			ChgcrteEquipmentReadingResult chgcrteEquipmentReadingResult = null;
			EdteEquipmentReadingsParams edteEquipmentReadingsParams = null;
			AddemEquipmentReadingsResult addemEquipmentReadingsResult = null;
			ChgcrteEquipmentReadingParams chgcrteEquipmentReadingParams = null;
			if (
			// DEBUG ComparatorJavaGenerator BEGIN
			gdo.getReadingValue() != gdo.getServiceReading()
			// DEBUG ComparatorJavaGenerator END
			) {
				// RCD.Reading Value NE RCD.Service reading
				// DEBUG genFunctionCall BEGIN 1000138 ACT ChgCrtE Equipment Reading - Equipment Readings  *
				chgcrteEquipmentReadingParams = new ChgcrteEquipmentReadingParams();
				chgcrteEquipmentReadingResult = new ChgcrteEquipmentReadingResult();
				// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
				BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), chgcrteEquipmentReadingParams);
				chgcrteEquipmentReadingParams.setErrorProcessing("2");
				chgcrteEquipmentReadingParams.setEquipmentReadingSgt(gdo.getEquipmentReadingSgt());
				chgcrteEquipmentReadingParams.setAwhRegionCode(gdo.getAwhRegionCode());
				chgcrteEquipmentReadingParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
				chgcrteEquipmentReadingParams.setReadingType("ST");
				chgcrteEquipmentReadingParams.setReadingValue(gdo.getReadingValue());
				chgcrteEquipmentReadingParams.setReadingComment("");
				chgcrteEquipmentReadingParams.setUserId(job.getUser());
				chgcrteEquipmentReadingParams.setEquipmentLeasor(gdo.getEquipmentLeasor());
				chgcrteEquipmentReadingParams.setTransferToFromSgt(BigDecimal.ZERO);
				chgcrteEquipmentReadingParams.setCentreCodeKey(gdo.getCentreCodeKey());
				chgcrteEquipmentReadingParams.setAwhBuisnessSegment(gdo.getAwhBuisnessSegment());
				chgcrteEquipmentReadingParams.setEquipmentTypeCode(gdo.getEquipmentTypeCode());
				chgcrteEquipmentReadingParams.setReadingRequiredDate(dto.getReadingRequiredDate());
				stepResult = chgcrteEquipmentReadingService.execute(chgcrteEquipmentReadingParams);
				chgcrteEquipmentReadingResult = (ChgcrteEquipmentReadingResult)((ReturnFromService)stepResult.getAction()).getResults();
				dto.set_SysReturnCode(chgcrteEquipmentReadingResult.get_SysReturnCode());
				gdo.setEquipmentReadingSgt(chgcrteEquipmentReadingResult.getEquipmentReadingSgt());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000426 ACT RCD.Service reading = RCD.Reading Value
				gdo.setServiceReading(gdo.getReadingValue());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000443 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
			if ((dto.get_SysCmdKey() == CmdKeyEnum._CF10) && (true)) {
				if (gdo.getEquipmentReadingSgt() != BigDecimal.ZERO) {
					// RCD.Equipment Reading Sgt is Entered
					// DEBUG genFunctionCall BEGIN 1000344 ACT EdtE Equipment Readings - Equipment Readings  *
					edteEquipmentReadingsParams = new EdteEquipmentReadingsParams();
					edteEquipmentReadingsResult = new EdteEquipmentReadingsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), edteEquipmentReadingsParams);
					edteEquipmentReadingsParams.setEquipmentReadingSgt(gdo.getEquipmentReadingSgt());
					edteEquipmentReadingsParams.setProgramMode("CHG");
					edteEquipmentReadingsParams.setErrorProcessing("2");
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.equipmentReadingsWspltwmdfk (209) -> EXCINTFUN EquipmentReadings.edteEquipmentReadings
					stepResult = edteEquipmentReadingsService.execute(edteEquipmentReadingsParams);
					edteEquipmentReadingsResult = (EdteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(edteEquipmentReadingsResult.get_SysReturnCode());
					dto.setLclExitProgramOption(edteEquipmentReadingsResult.getExitProgramOption());
					// DEBUG genFunctionCall END
					// DEBUG genFunctionCall BEGIN 1000350 ACT GetE Equipment Readings - Equipment Readings  *
					geteEquipmentReadingsParams = new GeteEquipmentReadingsParams();
					geteEquipmentReadingsResult = new GeteEquipmentReadingsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteEquipmentReadingsParams);
					geteEquipmentReadingsParams.setEquipmentReadingSgt(gdo.getEquipmentReadingSgt());
					geteEquipmentReadingsParams.setErrorProcessing("0");
					geteEquipmentReadingsParams.setGetRecordOption("");
					stepResult = geteEquipmentReadingsService.execute(geteEquipmentReadingsParams);
					geteEquipmentReadingsResult = (GeteEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(geteEquipmentReadingsResult.get_SysReturnCode());
					gdo.setReadingValue(geteEquipmentReadingsResult.getReadingValue());
					gdo.setReadingComment(geteEquipmentReadingsResult.getReadingComment());
					gdo.setEquipmentTypeCode(geteEquipmentReadingsResult.getEquipmentTypeCode());
					// DEBUG genFunctionCall END
				} else {
					// *OTHERWISE
					// DEBUG genFunctionCall BEGIN 1000373 ACT AddE:M Equipment Readings - Equipment Readings  *
					addemEquipmentReadingsParams = new AddemEquipmentReadingsParams();
					addemEquipmentReadingsResult = new AddemEquipmentReadingsResult();
					// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
					BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addemEquipmentReadingsParams);
					addemEquipmentReadingsParams.setAwhRegionCode(gdo.getAwhRegionCode());
					addemEquipmentReadingsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
					addemEquipmentReadingsParams.setErrorProcessing("2");
					addemEquipmentReadingsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
					// TODO SPLIT FUNCTION DSPFIL PlantEquipment.equipmentReadingsWspltwmdfk (209) -> EXCINTFUN EquipmentReadings.addemEquipmentReadings
					stepResult = addemEquipmentReadingsService.execute(addemEquipmentReadingsParams);
					addemEquipmentReadingsResult = (AddemEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
					dto.set_SysReturnCode(addemEquipmentReadingsResult.get_SysReturnCode());
					dto.setLclExitProgramOption(addemEquipmentReadingsResult.getExitProgramOption());
					gdo.setEquipmentReadingSgt(addemEquipmentReadingsResult.getEquipmentReadingSgt());
					gdo.setReadingValue(addemEquipmentReadingsResult.getReadingValue());
					gdo.setReadingComment(addemEquipmentReadingsResult.getReadingComment());
					// DEBUG genFunctionCall END
				}
				// DEBUG genFunctionCall BEGIN 1000432 ACT RCD.Service reading = RCD.Reading Value
				gdo.setServiceReading(gdo.getReadingValue());
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000449 ACT PGM.*Reload subfile = CND.*YES
				dto.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Final processing (Post-confirm) (Generated:228)
	 */
    private StepResult usrFinalProcessingPostConfirm(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	ChkReadingsBatchStsResult chkReadingsBatchStsResult = null;
			ChgeStatusParams chgeStatusParams = null;
			ChgeStatusResult chgeStatusResult = null;
			ChkReadingsBatchStsParams chkReadingsBatchStsParams = null;
			// DEBUG genFunctionCall BEGIN 1000239 ACT Chk Readings Batch Sts - Plant Equipment  *
			chkReadingsBatchStsParams = new ChkReadingsBatchStsParams();
			chkReadingsBatchStsResult = new ChkReadingsBatchStsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), chkReadingsBatchStsParams);
			chkReadingsBatchStsParams.setAwhRegionCode(dto.getAwhRegionCode());
			chkReadingsBatchStsParams.setCentreCodeKey(dto.getCentreCodeKey());
			chkReadingsBatchStsParams.setReadingRequiredDate(dto.getReadingRequiredDate());
			stepResult = chkReadingsBatchStsService.execute(chkReadingsBatchStsParams);
			chkReadingsBatchStsResult = (ChkReadingsBatchStsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(chkReadingsBatchStsResult.get_SysReturnCode());
			dto.setLclEquipReadingCtrlSts(chkReadingsBatchStsResult.getEquipReadingCtrlSts());
			// DEBUG genFunctionCall END
			// DEBUG genFunctionCall BEGIN 1000248 ACT ChgE Status - Equipment Reading Control  *
			chgeStatusParams = new ChgeStatusParams();
			chgeStatusResult = new ChgeStatusResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), chgeStatusParams);
			chgeStatusParams.setEquipReadingBatchNo(dto.getEquipReadingBatchNo());
			chgeStatusParams.setEquipReadingCtrlSts(dto.getLclEquipReadingCtrlSts());
			chgeStatusParams.setErrorProcessing("2");
			stepResult = chgeStatusService.execute(chgeStatusParams);
			chgeStatusResult = (ChgeStatusResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(chgeStatusResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:140)
	 */
    private StepResult usrProcessCommandKeys(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;

        try {
        	
			// Unprocessed SUB 140 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(EquipmentReadingsWspltwmdfkState dto) {
        StepResult stepResult = NO_ACTION;
        
        try {
        	
			// Unprocessed SUB 132 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * GeteReadingByDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteReadingByDate(EquipmentReadingsWspltwmdfkState state, GeteReadingByDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteByReverseDateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteByReverseDate(EquipmentReadingsWspltwmdfkState state, GeteByReverseDateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteEquipReadingCtrlService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipReadingCtrl(EquipmentReadingsWspltwmdfkState state, GeteEquipReadingCtrlParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgcrteEquipmentReadingService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgcrteEquipmentReading(EquipmentReadingsWspltwmdfkState state, ChgcrteEquipmentReadingParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChkReadingsBatchStsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChkReadingsBatchSts(EquipmentReadingsWspltwmdfkState state, ChkReadingsBatchStsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * ChgeStatusService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceChgeStatus(EquipmentReadingsWspltwmdfkState state, ChgeStatusParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * EdteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceEdteEquipmentReadings(EquipmentReadingsWspltwmdfkState state, EdteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GeteEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipmentReadings(EquipmentReadingsWspltwmdfkState state, GeteEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * AddemEquipmentReadingsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceAddemEquipmentReadings(EquipmentReadingsWspltwmdfkState state, AddemEquipmentReadingsParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
////
//    /**
//     * GetCentreHolidayListService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentreHolidayList(EquipmentReadingsWspltwmdfkState state, GetCentreHolidayListParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, state);
//        }
//
//        //TODO: call the continuation of the program
//        EquipmentReadingsWspltwmdfkParams equipmentReadingsWspltwmdfkParams = new EquipmentReadingsWspltwmdfkParams();
//        BeanUtils.copyProperties(state, equipmentReadingsWspltwmdfkParams);
//        stepResult = StepResult.callService(EquipmentReadingsWspltwmdfkService.class, equipmentReadingsWspltwmdfkParams);
//
//        return stepResult;
//    }
//


}
