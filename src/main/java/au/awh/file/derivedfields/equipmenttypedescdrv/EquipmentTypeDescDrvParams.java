package au.awh.file.derivedfields.equipmenttypedescdrv;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EquipmentTypeDescDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EquipmentTypeDescDrvParams implements Serializable
{
	private static final long serialVersionUID = -4087099349383756423L;

    private String equipmentTypeCode = "";

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}
	
	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}
}
