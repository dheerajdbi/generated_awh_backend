package au.awh.file.derivedfields.equipmenttypedescdrv;

// ExecuteExternalFunction.kt File=DerivedFields () Function=equipmentTypeDescDrv (1878694) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeService;
// imports for DTO
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeDTO;
// imports for Enums
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeParams;
// imports for Results
import au.awh.file.equipmenttype.geteequipmenttype.GeteEquipmentTypeResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'Equipment Type Desc Drv' (file 'Derived Fields' (DRVFLD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service(value = "DerivedFields_EquipmentTypeDescDrv")
public class EquipmentTypeDescDrvService extends AbstractService<EquipmentTypeDescDrvService, EquipmentTypeDescDrvDTO>
{
	private final Step execute = define("execute", EquipmentTypeDescDrvParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GeteEquipmentTypeService geteEquipmentTypeService;

	//private final Step serviceGeteEquipmentType = define("serviceGeteEquipmentType",GeteEquipmentTypeResult.class, this::processServiceGeteEquipmentType);
	
	@Autowired
	public EquipmentTypeDescDrvService() {
		super(EquipmentTypeDescDrvService.class, EquipmentTypeDescDrvDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(EquipmentTypeDescDrvParams params) throws ServiceException {
		EquipmentTypeDescDrvDTO dto = new EquipmentTypeDescDrvDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(EquipmentTypeDescDrvDTO dto, EquipmentTypeDescDrvParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		GeteEquipmentTypeParams geteEquipmentTypeParams = null;
		GeteEquipmentTypeResult geteEquipmentTypeResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Equipment Type - Equipment Type  *
		geteEquipmentTypeParams = new GeteEquipmentTypeParams();
		geteEquipmentTypeResult = new GeteEquipmentTypeResult();
		geteEquipmentTypeParams.setEquipmentTypeCode(dto.getEquipmentTypeCode());
		geteEquipmentTypeParams.setErrorProcessing("2");
		geteEquipmentTypeParams.setGetRecordOption("");
		stepResult = geteEquipmentTypeService.execute(geteEquipmentTypeParams);
		geteEquipmentTypeResult = (GeteEquipmentTypeResult)((ReturnFromService)stepResult.getAction()).getResults();
		BeanUtils.copyProperties(geteEquipmentTypeResult , dto);
		stepResult = NO_ACTION;
		if(dto.is_SysExitCallingProgram()){
		//function: equipmentTypeDescDrv *FIELD
		    return exitProgram(dto, dto.get_SysReturnCode());
		}
		dto.setEquipmentTypeDescDrv(geteEquipmentTypeResult.getEquipmentTypeDesc());
		// DEBUG genFunctionCall END

		stepResult = exitProgram(dto, null);

		return stepResult;
	}
	private StepResult exitProgram(EquipmentTypeDescDrvDTO dto, ReturnCodeEnum returnCode) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		EquipmentTypeDescDrvResult result = new EquipmentTypeDescDrvResult();
		BeanUtils.copyProperties(dto, result);
		result.set_SysReturnCode(returnCode);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


//
//    /**
//     * GeteEquipmentTypeService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteEquipmentType(EquipmentTypeDescDrvDTO dto, GeteEquipmentTypeResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, dto);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
}
