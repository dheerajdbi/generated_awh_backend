package au.awh.file.derivedfields.equipmenttypedescdrv;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EquipmentTypeDescDrvDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;

	private String equipmentTypeCode;
	private String equipmentTypeDescDrv;


	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public String getEquipmentTypeCode() {
		return equipmentTypeCode;
	}

	public String getEquipmentTypeDescDrv() {
		return equipmentTypeDescDrv;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		_SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

	public void setEquipmentTypeCode(String equipmentTypeCode) {
		this.equipmentTypeCode = equipmentTypeCode;
	}

	public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
		this.equipmentTypeDescDrv = equipmentTypeDescDrv;
	}

}
