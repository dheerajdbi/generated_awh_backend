package au.awh.file.derivedfields.equipmenttypedescdrv;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: EquipmentTypeDescDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EquipmentTypeDescDrvResult implements Serializable
{
	private static final long serialVersionUID = 7248905445238198036L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;
	private String equipmentTypeDescDrv = ""; // String 56549

	public String getEquipmentTypeDescDrv() {
		return equipmentTypeDescDrv;
	}
	
	public void setEquipmentTypeDescDrv(String equipmentTypeDescDrv) {
		this.equipmentTypeDescDrv = equipmentTypeDescDrv;
	}
	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		this._SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		this._SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

}
