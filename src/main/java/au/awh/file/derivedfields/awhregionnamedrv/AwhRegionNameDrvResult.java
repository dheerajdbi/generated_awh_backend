package au.awh.file.derivedfields.awhregionnamedrv;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: AwhRegionNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class AwhRegionNameDrvResult implements Serializable
{
	private static final long serialVersionUID = -6300089911355817162L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;
	private String awhRegionNameDrv = ""; // String 56540

	public String getAwhRegionNameDrv() {
		return awhRegionNameDrv;
	}
	
	public void setAwhRegionNameDrv(String awhRegionNameDrv) {
		this.awhRegionNameDrv = awhRegionNameDrv;
	}
	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		this._SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		this._SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

}
