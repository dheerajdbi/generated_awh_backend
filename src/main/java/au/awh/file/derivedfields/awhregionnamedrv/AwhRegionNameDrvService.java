package au.awh.file.derivedfields.awhregionnamedrv;

// ExecuteExternalFunction.kt File=DerivedFields () Function=awhRegionNameDrv (1878465) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.awhregion.geteawhregion.GeteAwhRegionService;
// imports for DTO
import au.awh.file.awhregion.geteawhregion.GeteAwhRegionDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.awhregion.geteawhregion.GeteAwhRegionParams;
// imports for Results
import au.awh.file.awhregion.geteawhregion.GeteAwhRegionResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'AWH region Name Drv' (file 'Derived Fields' (DRVFLD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service(value = "DerivedFields_AwhRegionNameDrv")
public class AwhRegionNameDrvService extends AbstractService<AwhRegionNameDrvService, AwhRegionNameDrvDTO>
{
	private final Step execute = define("execute", AwhRegionNameDrvParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GeteAwhRegionService geteAwhRegionService;

	//private final Step serviceGeteAwhRegion = define("serviceGeteAwhRegion",GeteAwhRegionResult.class, this::processServiceGeteAwhRegion);
	
	@Autowired
	public AwhRegionNameDrvService() {
		super(AwhRegionNameDrvService.class, AwhRegionNameDrvDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(AwhRegionNameDrvParams params) throws ServiceException {
		AwhRegionNameDrvDTO dto = new AwhRegionNameDrvDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(AwhRegionNameDrvDTO dto, AwhRegionNameDrvParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		GeteAwhRegionParams geteAwhRegionParams = null;
		GeteAwhRegionResult geteAwhRegionResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT GetE AWH Region - AWH Region  *
		geteAwhRegionParams = new GeteAwhRegionParams();
		geteAwhRegionResult = new GeteAwhRegionResult();
		geteAwhRegionParams.setAwhRegionCode(dto.getAwhRegionCode());
		geteAwhRegionParams.setErrorProcessing("2");
		geteAwhRegionParams.setGetRecordOption("");
		stepResult = geteAwhRegionService.execute(geteAwhRegionParams);
		geteAwhRegionResult = (GeteAwhRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
		BeanUtils.copyProperties(geteAwhRegionResult , dto);
		stepResult = NO_ACTION;
		if(dto.is_SysExitCallingProgram()){
		//function: awhRegionNameDrv *FIELD
		    return exitProgram(dto, dto.get_SysReturnCode());
		}
		dto.setAwhRegionNameDrv(geteAwhRegionResult.getAwhRegionName());
		// DEBUG genFunctionCall END

		stepResult = exitProgram(dto, null);

		return stepResult;
	}
	private StepResult exitProgram(AwhRegionNameDrvDTO dto, ReturnCodeEnum returnCode) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		AwhRegionNameDrvResult result = new AwhRegionNameDrvResult();
		BeanUtils.copyProperties(dto, result);
		result.set_SysReturnCode(returnCode);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


//
//    /**
//     * GeteAwhRegionService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteAwhRegion(AwhRegionNameDrvDTO dto, GeteAwhRegionResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, dto);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
}
