package au.awh.file.derivedfields.awhregionnamedrv;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: AwhRegionNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class AwhRegionNameDrvParams implements Serializable
{
	private static final long serialVersionUID = -8542986213957353284L;

    private String awhRegionCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
}
