package au.awh.file.derivedfields.organisationnamedrv;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: OrganisationNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class OrganisationNameDrvParams implements Serializable
{
	private static final long serialVersionUID = 8334923647773985995L;

    private String organisation = "";

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
}
