package au.awh.file.derivedfields.organisationnamedrv;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: OrganisationNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class OrganisationNameDrvResult implements Serializable
{
	private static final long serialVersionUID = -3197715860933753591L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;
	private String organisationNameDrv = ""; // String 38795

	public String getOrganisationNameDrv() {
		return organisationNameDrv;
	}
	
	public void setOrganisationNameDrv(String organisationNameDrv) {
		this.organisationNameDrv = organisationNameDrv;
	}
	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		this._SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		this._SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

}
