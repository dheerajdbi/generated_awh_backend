package au.awh.file.derivedfields.organisationnamedrv;

// ExecuteExternalFunction.kt File=DerivedFields () Function=organisationNameDrv (1462942) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.organisationdump.geteorganisationdump.GeteOrganisationDumpService;
// imports for DTO
import au.awh.file.organisationdump.geteorganisationdump.GeteOrganisationDumpDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.organisationdump.geteorganisationdump.GeteOrganisationDumpParams;
// imports for Results
import au.awh.file.organisationdump.geteorganisationdump.GeteOrganisationDumpResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'Organisation Name Drv' (file 'Derived Fields' (DRVFLD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service(value = "DerivedFields_OrganisationNameDrv")
public class OrganisationNameDrvService extends AbstractService<OrganisationNameDrvService, OrganisationNameDrvDTO>
{
	private final Step execute = define("execute", OrganisationNameDrvParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GeteOrganisationDumpService geteOrganisationDumpService;

	//private final Step serviceGeteOrganisationDump = define("serviceGeteOrganisationDump",GeteOrganisationDumpResult.class, this::processServiceGeteOrganisationDump);
	
	@Autowired
	public OrganisationNameDrvService() {
		super(OrganisationNameDrvService.class, OrganisationNameDrvDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(OrganisationNameDrvParams params) throws ServiceException {
		OrganisationNameDrvDTO dto = new OrganisationNameDrvDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(OrganisationNameDrvDTO dto, OrganisationNameDrvParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		GeteOrganisationDumpParams geteOrganisationDumpParams = null;
		GeteOrganisationDumpResult geteOrganisationDumpResult = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Organisation (Dump) - Organisation (Dump)  *
		geteOrganisationDumpParams = new GeteOrganisationDumpParams();
		geteOrganisationDumpResult = new GeteOrganisationDumpResult();
		geteOrganisationDumpParams.setOrganisation(dto.getOrganisation());
		geteOrganisationDumpParams.setErrorProcessing("1");
		geteOrganisationDumpParams.setGetRecordOption("");
		stepResult = geteOrganisationDumpService.execute(geteOrganisationDumpParams);
		geteOrganisationDumpResult = (GeteOrganisationDumpResult)((ReturnFromService)stepResult.getAction()).getResults();
		BeanUtils.copyProperties(geteOrganisationDumpResult , dto);
		stepResult = NO_ACTION;
		if(dto.is_SysExitCallingProgram()){
		//function: organisationNameDrv *FIELD
		    return exitProgram(dto, dto.get_SysReturnCode());
		}
		dto.setOrganisationNameDrv(geteOrganisationDumpResult.getOrganisationName50a());
		// DEBUG genFunctionCall END

		stepResult = exitProgram(dto, null);

		return stepResult;
	}
	private StepResult exitProgram(OrganisationNameDrvDTO dto, ReturnCodeEnum returnCode) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		OrganisationNameDrvResult result = new OrganisationNameDrvResult();
		BeanUtils.copyProperties(dto, result);
		result.set_SysReturnCode(returnCode);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


//
//    /**
//     * GeteOrganisationDumpService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGeteOrganisationDump(OrganisationNameDrvDTO dto, GeteOrganisationDumpResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, dto);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
}
