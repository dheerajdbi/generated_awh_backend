package au.awh.file.derivedfields.centrenamedrv;

// ExecuteExternalFunction.kt File=DerivedFields () Function=centreNameDrv (1406488) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.centre.getcentredetails.GetCentreDetailsService;
// imports for DTO
import au.awh.file.centre.getcentredetails.GetCentreDetailsDTO;
// imports for Enums
import au.awh.model.CentreTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
// imports for Parameters
import au.awh.file.centre.getcentredetails.GetCentreDetailsParams;
// imports for Results
import au.awh.file.centre.getcentredetails.GetCentreDetailsResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'Centre Name Drv' (file 'Derived Fields' (DRVFLD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service(value = "DerivedFields_CentreNameDrv")
public class CentreNameDrvService extends AbstractService<CentreNameDrvService, CentreNameDrvDTO>
{
	private final Step execute = define("execute", CentreNameDrvParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GetCentreDetailsService getCentreDetailsService;

	//private final Step serviceGetCentreDetails = define("serviceGetCentreDetails",GetCentreDetailsResult.class, this::processServiceGetCentreDetails);
	
	@Autowired
	public CentreNameDrvService() {
		super(CentreNameDrvService.class, CentreNameDrvDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CentreNameDrvParams params) throws ServiceException {
		CentreNameDrvDTO dto = new CentreNameDrvDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CentreNameDrvDTO dto, CentreNameDrvParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		GetCentreDetailsResult getCentreDetailsResult = null;
		GetCentreDetailsParams getCentreDetailsParams = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT Get Centre Details - Centre  *
		getCentreDetailsParams = new GetCentreDetailsParams();
		getCentreDetailsResult = new GetCentreDetailsResult();
		getCentreDetailsParams.setCentreCodeKey(dto.getCentreCodeKey());
		stepResult = getCentreDetailsService.execute(getCentreDetailsParams);
		getCentreDetailsResult = (GetCentreDetailsResult)((ReturnFromService)stepResult.getAction()).getResults();
		BeanUtils.copyProperties(getCentreDetailsResult , dto);
		stepResult = NO_ACTION;
		dto.setCentreNameDrv(getCentreDetailsResult.getCentreName());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000014 ACT PAR.Centre Name Drv = CND.Invalid
			dto.setCentreNameDrv("*** Invalid ***");
			// DEBUG genFunctionCall END
		}

		stepResult = exitProgram(dto, null);

		return stepResult;
	}
	private StepResult exitProgram(CentreNameDrvDTO dto, ReturnCodeEnum returnCode) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		CentreNameDrvResult result = new CentreNameDrvResult();
		BeanUtils.copyProperties(dto, result);
		result.set_SysReturnCode(returnCode);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


//
//    /**
//     * GetCentreDetailsService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetCentreDetails(CentreNameDrvDTO dto, GetCentreDetailsResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, dto);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
}
