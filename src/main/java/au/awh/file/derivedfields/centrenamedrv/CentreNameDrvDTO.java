package au.awh.file.derivedfields.centrenamedrv;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CentreNameDrvDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;

	private String centreCodeKey;
	private String centreNameDrv;


	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public String getCentreCodeKey() {
		return centreCodeKey;
	}

	public String getCentreNameDrv() {
		return centreNameDrv;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		_SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}

	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}

}
