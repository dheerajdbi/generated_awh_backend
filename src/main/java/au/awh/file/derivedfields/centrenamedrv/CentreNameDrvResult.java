package au.awh.file.derivedfields.centrenamedrv;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CentreNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CentreNameDrvResult implements Serializable
{
	private static final long serialVersionUID = -1871362864069457264L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;
	private String centreNameDrv = ""; // String 34917

	public String getCentreNameDrv() {
		return centreNameDrv;
	}
	
	public void setCentreNameDrv(String centreNameDrv) {
		this.centreNameDrv = centreNameDrv;
	}
	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		this._SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		this._SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

}
