package au.awh.file.derivedfields.centrenamedrv;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CentreNameDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CentreNameDrvParams implements Serializable
{
	private static final long serialVersionUID = -3236256634483161302L;

    private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
