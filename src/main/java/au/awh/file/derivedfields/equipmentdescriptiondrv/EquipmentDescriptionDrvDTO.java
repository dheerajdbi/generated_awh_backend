package au.awh.file.derivedfields.equipmentdescriptiondrv;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EquipmentDescriptionDrvDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;

	private String awhRegionCode;
	private String equipmentDescriptionDrv;
	private String plantEquipmentCode;

	private String lclEquipmentDescription;
	private EquipmentStatusEnum lclEquipmentStatus;

	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public String getAwhRegionCode() {
		return awhRegionCode;
	}

	public String getEquipmentDescriptionDrv() {
		return equipmentDescriptionDrv;
	}

	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}

	public String getLclEquipmentDescription() {
		return lclEquipmentDescription;
	}

	public EquipmentStatusEnum getLclEquipmentStatus() {
		return lclEquipmentStatus;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		_SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}

	public void setEquipmentDescriptionDrv(String equipmentDescriptionDrv) {
		this.equipmentDescriptionDrv = equipmentDescriptionDrv;
	}

	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}

	public void setLclEquipmentDescription(String lclEquipmentDescription) {
		this.lclEquipmentDescription = lclEquipmentDescription;
	}

	public void setLclEquipmentStatus(EquipmentStatusEnum lclEquipmentStatus) {
		this.lclEquipmentStatus = lclEquipmentStatus;
	}

}
