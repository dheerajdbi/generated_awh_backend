package au.awh.file.derivedfields.equipmentdescriptiondrv;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: EquipmentDescriptionDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EquipmentDescriptionDrvResult implements Serializable
{
	private static final long serialVersionUID = -8553297619357906562L;

	private boolean _SysExitCallingProgram = false;
	private ReturnCodeEnum _SysReturnCode;
	private String equipmentDescriptionDrv = ""; // String 56586

	public String getEquipmentDescriptionDrv() {
		return equipmentDescriptionDrv;
	}
	
	public void setEquipmentDescriptionDrv(String equipmentDescriptionDrv) {
		this.equipmentDescriptionDrv = equipmentDescriptionDrv;
	}
	public boolean is_SysExitCallingProgram() {
		return _SysExitCallingProgram;
	}

	public void set_SysExitCallingProgram(boolean exitCallingProgram) {
		this._SysExitCallingProgram = exitCallingProgram;
	}

	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		this._SysReturnCode = returnCode;
	}

	public void set_SysReturnCode(String returnCode) {
		this._SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
	}

}
