package au.awh.file.derivedfields.equipmentdescriptiondrv;

// ExecuteExternalFunction.kt File=DerivedFields () Function=equipmentDescriptionDrv (1879815) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
// imports for Services
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentService;
// imports for DTO
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentDTO;
// imports for Enums
import au.awh.model.AwhBuisnessSegmentEnum;
import au.awh.model.EquipReadingUnitEnum;
import au.awh.model.EquipmentStatusEnum;
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.ReadingFrequencyEnum;
import au.awh.model.ReturnCodeEnum;
// imports for Parameters
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentParams;
// imports for Results
import au.awh.file.plantequipment.geteplantequipment.GetePlantEquipmentResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'Equipment Description Drv' (file 'Derived Fields' (DRVFLD)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service(value = "DerivedFields_EquipmentDescriptionDrv")
public class EquipmentDescriptionDrvService extends AbstractService<EquipmentDescriptionDrvService, EquipmentDescriptionDrvDTO>
{
	private final Step execute = define("execute", EquipmentDescriptionDrvParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private GetePlantEquipmentService getePlantEquipmentService;

	//private final Step serviceGetePlantEquipment = define("serviceGetePlantEquipment",GetePlantEquipmentResult.class, this::processServiceGetePlantEquipment);
	
	@Autowired
	public EquipmentDescriptionDrvService() {
		super(EquipmentDescriptionDrvService.class, EquipmentDescriptionDrvDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(EquipmentDescriptionDrvParams params) throws ServiceException {
		EquipmentDescriptionDrvDTO dto = new EquipmentDescriptionDrvDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(EquipmentDescriptionDrvDTO dto, EquipmentDescriptionDrvParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		GetePlantEquipmentResult getePlantEquipmentResult = null;
		GetePlantEquipmentParams getePlantEquipmentParams = null;
		// DEBUG genFunctionCall BEGIN 1000002 ACT GetE Plant Equipment - Plant Equipment  *
		getePlantEquipmentParams = new GetePlantEquipmentParams();
		getePlantEquipmentResult = new GetePlantEquipmentResult();
		getePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		getePlantEquipmentParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
		getePlantEquipmentParams.setErrorProcessing("2");
		getePlantEquipmentParams.setGetRecordOption("");
		stepResult = getePlantEquipmentService.execute(getePlantEquipmentParams);
		getePlantEquipmentResult = (GetePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		BeanUtils.copyProperties(getePlantEquipmentResult , dto);
		stepResult = NO_ACTION;
		if(dto.is_SysExitCallingProgram()){
		//function: equipmentDescriptionDrv *FIELD
		    return exitProgram(dto, dto.get_SysReturnCode());
		}
		dto.setLclEquipmentDescription(getePlantEquipmentResult.getEquipmentDescription());
		dto.setLclEquipmentStatus(getePlantEquipmentResult.getEquipmentStatus());
		// DEBUG genFunctionCall END
		if (dto.getLclEquipmentStatus() == EquipmentStatusEnum._RETIRED) {
			// LCL.Equipment Status is Retired
			// DEBUG genFunctionCall BEGIN 1000039 ACT PAR.Equipment Description Drv = CONCAT(LCL.Equipment Description,CON.Retired,CND.*One)
			dto.setEquipmentDescriptionDrv(String.format("%s%1s%s", dto.getLclEquipmentDescription().trim(), "", "Retired"));
			// DEBUG genFunctionCall END
		} else if (dto.getLclEquipmentStatus() == EquipmentStatusEnum._UNAVAILABLE) {
			// LCL.Equipment Status is Unavailable
			// DEBUG genFunctionCall BEGIN 1000047 ACT PAR.Equipment Description Drv = CONCAT(LCL.Equipment Description,CON.Unavailable,CND.*One)
			dto.setEquipmentDescriptionDrv(String.format("%s%1s%s", dto.getLclEquipmentDescription().trim(), "", "Unavailable"));
			// DEBUG genFunctionCall END
		} else if (dto.getLclEquipmentStatus() == EquipmentStatusEnum._TRANSFERED) {
			// LCL.Equipment Status is Transfered
			// DEBUG genFunctionCall BEGIN 1000027 ACT PAR.Equipment Description Drv = CONCAT(LCL.Equipment Description,CON.Transfered,CND.*One)
			dto.setEquipmentDescriptionDrv(String.format("%s%1s%s", dto.getLclEquipmentDescription().trim(), "", "Transfered"));
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000063 ACT PAR.Equipment Description Drv = LCL.Equipment Description
			dto.setEquipmentDescriptionDrv(dto.getLclEquipmentDescription());
			// DEBUG genFunctionCall END
		}

		stepResult = exitProgram(dto, null);

		return stepResult;
	}
	private StepResult exitProgram(EquipmentDescriptionDrvDTO dto, ReturnCodeEnum returnCode) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		EquipmentDescriptionDrvResult result = new EquipmentDescriptionDrvResult();
		BeanUtils.copyProperties(dto, result);
		result.set_SysReturnCode(returnCode);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


//
//    /**
//     * GetePlantEquipmentService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetePlantEquipment(EquipmentDescriptionDrvDTO dto, GetePlantEquipmentResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, dto);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//
}
