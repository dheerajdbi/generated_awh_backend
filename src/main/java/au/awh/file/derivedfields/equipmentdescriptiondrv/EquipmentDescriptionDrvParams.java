package au.awh.file.derivedfields.equipmentdescriptiondrv;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: EquipmentDescriptionDrv ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class EquipmentDescriptionDrvParams implements Serializable
{
	private static final long serialVersionUID = -3426728757027127267L;

    private String awhRegionCode = "";
    private String plantEquipmentCode = "";

	public String getAwhRegionCode() {
		return awhRegionCode;
	}
	
	public void setAwhRegionCode(String awhRegionCode) {
		this.awhRegionCode = awhRegionCode;
	}
	
	public String getPlantEquipmentCode() {
		return plantEquipmentCode;
	}
	
	public void setPlantEquipmentCode(String plantEquipmentCode) {
		this.plantEquipmentCode = plantEquipmentCode;
	}
}
