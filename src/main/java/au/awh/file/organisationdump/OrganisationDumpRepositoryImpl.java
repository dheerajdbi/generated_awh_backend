package au.awh.file.organisationdump;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

import au.awh.file.organisationdump.OrganisationDump;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpGDO;

/**
 * Custom Spring Data JPA repository implementation for model: Organisation (Dump) (WDORG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class OrganisationDumpRepositoryImpl implements OrganisationDumpRepositoryCustom {
	@PersistenceContext
	EntityManager em;

	@Override
	public RestResponsePage<SelectOrganisationDumpGDO> selectOrganisationDump(
		String organisation
, String organisationName50a, Pageable pageable) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(organisation)) {
			whereClause += " (organisationDump.id.organisation >= :organisation OR organisationDump.id.organisation like CONCAT( :organisation, '%'))";
			isParamSet = true;
		}

		if (StringUtils.isNotEmpty(organisationName50a)) {
			if (isParamSet) {
				whereClause += " AND";
			}

			whereClause += " (organisationDump.organisationName50a >= :organisationName50a OR organisationDump.organisationName50a like CONCAT('%', :organisationName50a, '%'))";
			isParamSet = true;
		}

		String sqlString = "SELECT new au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpGDO(organisationDump.id.organisation, organisationDump.organisationSurrogate, organisationDump.organisationName50a, organisationDump.organisationType, organisationDump.organisationStatus, organisationDump.organisationIsAwh, organisationDump.organisationAwexCode) from OrganisationDump organisationDump";
		String countQueryString = "SELECT COUNT(organisationDump.id.organisation) FROM OrganisationDump organisationDump";

		if (isParamSet) {
			sqlString += whereClause;
			countQueryString += whereClause;
		}

		// Fetching sorting details
		Sort sort = pageable.getSort();
		String orderByClause = "";

		if (sort != null) {
			OrganisationDump organisationDump = new OrganisationDump();
			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}
     			if(Arrays.stream(organisationDump.getId().getClass().getDeclaredFields()).anyMatch(f->f.getName().equals(order.getProperty()))) {
					orderByClause += (
						"organisationDump.id." + order.getProperty() + " " +
						order.getDirection()
					);
				} else {
					orderByClause += (
						"organisationDump." + order.getProperty() + " " +
						order.getDirection()
					);
				}
			}
		}

		if (sort == null) {
			List<Order> sortOrders = new java.util.ArrayList<Order>();
			sortOrders.add(new Order(Sort.Direction.ASC,
				"organisationDump.id.organisation"));
			sort = Sort.by(sortOrders);

			for (Order order : sort) {
				if (orderByClause.isEmpty()) {
					orderByClause = " ORDER BY ";
				} else {
					orderByClause += ", ";
				}

				orderByClause += (
					order.getProperty() + " " + order.getDirection()
				);
			}
		}

		// Adding sort details
		sqlString += orderByClause;

		Query countQuery = em.createQuery(countQueryString);
		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(organisation)) {
			countQuery.setParameter("organisation", organisation);
			query.setParameter("organisation", organisation);
		}

		if (StringUtils.isNotEmpty(organisationName50a)) {
			countQuery.setParameter("organisationName50a", organisationName50a);
			query.setParameter("organisationName50a", organisationName50a);
		}

		Long count = (Long)countQuery.getSingleResult();
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		@SuppressWarnings("unchecked")
		List<SelectOrganisationDumpGDO> content = query.getResultList();
		RestResponsePage<SelectOrganisationDumpGDO> pageGdo = new RestResponsePage<SelectOrganisationDumpGDO>(
				content, pageable, count.longValue());

		return pageGdo;
	}

	/**
	 * @see au.awh.file.organisationdump.OrganisationDumpService#getOrganisationDump(String)
	 */
	@Override
	public OrganisationDump getOrganisationDump(
		String organisation
) {
		String whereClause = " WHERE ";
		boolean isParamSet = false;

		if (StringUtils.isNotEmpty(organisation)) {
			whereClause += " organisationDump.id.organisation = :organisation";
			isParamSet = true;
		}

		String sqlString = "SELECT organisationDump FROM OrganisationDump organisationDump";

		if (isParamSet) {
			sqlString += whereClause;
		}

		Query query = em.createQuery(sqlString);

		if (StringUtils.isNotEmpty(organisation)) {
			query.setParameter("organisation", organisation);
		}

		if (query.getResultList().size() == 0) {
			return null;
		}

		query.setMaxResults(1);

		OrganisationDump dto = (OrganisationDump)query.getSingleResult();

		return dto;
	}

}
