package au.awh.file.organisationdump.geteorganisationdump;

// ExecuteInternalFunction.kt File=OrganisationDump (IZ) Function=geteOrganisationDump (1463384) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.organisationdump.OrganisationDump;
import au.awh.file.organisationdump.OrganisationDumpId;
import au.awh.file.organisationdump.OrganisationDumpRepository;
// imports for Services
import au.awh.file.organisationdump.getorganisationdump.GetOrganisationDumpService;
// imports for DTO
import au.awh.file.organisationdump.getorganisationdump.GetOrganisationDumpDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.GetRecordOptionEnum;
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.organisationdump.getorganisationdump.GetOrganisationDumpParams;
// imports for Results
import au.awh.file.organisationdump.getorganisationdump.GetOrganisationDumpResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Organisation (Dump)' (file 'Organisation (Dump)' (WDORG)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteOrganisationDumpService extends AbstractService<GeteOrganisationDumpService, GeteOrganisationDumpDTO>
{
	private final Step execute = define("execute", GeteOrganisationDumpParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private OrganisationDumpRepository organisationDumpRepository;

	@Autowired
	private GetOrganisationDumpService getOrganisationDumpService;

	//private final Step serviceGetOrganisationDump = define("serviceGetOrganisationDump",GetOrganisationDumpResult.class, this::processServiceGetOrganisationDump);
	
	@Autowired
	public GeteOrganisationDumpService() {
		super(GeteOrganisationDumpService.class, GeteOrganisationDumpDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteOrganisationDumpParams params) throws ServiceException {
		GeteOrganisationDumpDTO dto = new GeteOrganisationDumpDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteOrganisationDumpDTO dto, GeteOrganisationDumpParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetOrganisationDumpParams getOrganisationDumpParams = null;
		GetOrganisationDumpResult getOrganisationDumpResult = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Get Organisation (Dump) - Organisation (Dump)  *
		getOrganisationDumpParams = new GetOrganisationDumpParams();
		getOrganisationDumpResult = new GetOrganisationDumpResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getOrganisationDumpParams);
		getOrganisationDumpParams.setOrganisation(dto.getOrganisation());
		stepResult = getOrganisationDumpService.execute(getOrganisationDumpParams);
		getOrganisationDumpResult = (GetOrganisationDumpResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getOrganisationDumpResult.get_SysReturnCode());
		dto.setOrganisationSurrogate(getOrganisationDumpResult.getOrganisationSurrogate());
		dto.setOrganisationName50a(getOrganisationDumpResult.getOrganisationName50a());
		dto.setOrganisationType(getOrganisationDumpResult.getOrganisationType());
		dto.setOrganisationStatus(getOrganisationDumpResult.getOrganisationStatus());
		dto.setOrganisationIsAwh(getOrganisationDumpResult.getOrganisationIsAwh());
		dto.setOrganisationAwexCode(getOrganisationDumpResult.getOrganisationAwexCode());
		// DEBUG genFunctionCall END
		if ((dto.getGetRecordOption() == GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND) && (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST)) {
			// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
			// DEBUG genFunctionCall END
		}
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Organisation (Dump)    NF'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1461087) EXCINTFUN 1463384 OrganisationDump.geteOrganisationDump 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * NOTE: Update parameters on generic error message.
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1463384 OrganisationDump.geteOrganisationDump 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteOrganisationDumpResult result = new GeteOrganisationDumpResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetOrganisationDumpService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetOrganisationDump(GeteOrganisationDumpDTO dto, GetOrganisationDumpParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteOrganisationDumpParams geteOrganisationDumpParams = new GeteOrganisationDumpParams();
//        BeanUtils.copyProperties(dto, geteOrganisationDumpParams);
//        stepResult = StepResult.callService(GeteOrganisationDumpService.class, geteOrganisationDumpParams);
//
//        return stepResult;
//    }
//
}
