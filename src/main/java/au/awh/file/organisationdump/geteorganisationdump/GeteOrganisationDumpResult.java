package au.awh.file.organisationdump.geteorganisationdump;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 6
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteOrganisationDump ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteOrganisationDumpResult implements Serializable
{
	private static final long serialVersionUID = -3021596762073773142L;

	private ReturnCodeEnum _sysReturnCode;
	private long organisationSurrogate = 0L; // long 37589
	private String organisationName50a = ""; // String 37605
	private OrganisationTypeEnum organisationType = null; // OrganisationTypeEnum 37590
	private OrganisationStatusEnum organisationStatus = null; // OrganisationStatusEnum 37591
	private YesOrNoEnum organisationIsAwh = null; // YesOrNoEnum 37592
	private String organisationAwexCode = ""; // String 37654

	public long getOrganisationSurrogate() {
		return organisationSurrogate;
	}
	
	public void setOrganisationSurrogate(long organisationSurrogate) {
		this.organisationSurrogate = organisationSurrogate;
	}
	
	public void setOrganisationSurrogate(String organisationSurrogate) {
		setOrganisationSurrogate(Long.parseLong(organisationSurrogate));
	}
	
	public String getOrganisationName50a() {
		return organisationName50a;
	}
	
	public void setOrganisationName50a(String organisationName50a) {
		this.organisationName50a = organisationName50a;
	}
	
	public OrganisationTypeEnum getOrganisationType() {
		return organisationType;
	}
	
	public void setOrganisationType(OrganisationTypeEnum organisationType) {
		this.organisationType = organisationType;
	}
	
	public void setOrganisationType(String organisationType) {
		setOrganisationType(OrganisationTypeEnum.valueOf(organisationType));
	}
	
	public OrganisationStatusEnum getOrganisationStatus() {
		return organisationStatus;
	}
	
	public void setOrganisationStatus(OrganisationStatusEnum organisationStatus) {
		this.organisationStatus = organisationStatus;
	}
	
	public void setOrganisationStatus(String organisationStatus) {
		setOrganisationStatus(OrganisationStatusEnum.valueOf(organisationStatus));
	}
	
	public YesOrNoEnum getOrganisationIsAwh() {
		return organisationIsAwh;
	}
	
	public void setOrganisationIsAwh(YesOrNoEnum organisationIsAwh) {
		this.organisationIsAwh = organisationIsAwh;
	}
	
	public void setOrganisationIsAwh(String organisationIsAwh) {
		setOrganisationIsAwh(YesOrNoEnum.valueOf(organisationIsAwh));
	}
	
	public String getOrganisationAwexCode() {
		return organisationAwexCode;
	}
	
	public void setOrganisationAwexCode(String organisationAwexCode) {
		this.organisationAwexCode = organisationAwexCode;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
