package au.awh.file.organisationdump.selectorganisationdump;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Gdo for file 'Organisation (Dump)' (WDORG) and function 'Select Organisation (Dump' (WDSYSJ3SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationDumpGDO implements Serializable {
	private static final long serialVersionUID = 6793517286116508305L;

	private String _sysSelected = "";
	private String organisation = "";
	private long organisationSurrogate = 0L;
	private String organisationName50a = "";
	private OrganisationTypeEnum organisationType = null;
	private OrganisationStatusEnum organisationStatus = null;
	private YesOrNoEnum organisationIsAwh = null;
	private String organisationAwexCode = "";

	public SelectOrganisationDumpGDO() {

	}

	public SelectOrganisationDumpGDO(String organisation, long organisationSurrogate, String organisationName50a, OrganisationTypeEnum organisationType, OrganisationStatusEnum organisationStatus, YesOrNoEnum organisationIsAwh, String organisationAwexCode) {
		this.organisation = organisation;
		this.organisationSurrogate = organisationSurrogate;
		this.organisationName50a = organisationName50a;
		this.organisationType = organisationType;
		this.organisationStatus = organisationStatus;
		this.organisationIsAwh = organisationIsAwh;
		this.organisationAwexCode = organisationAwexCode;
	}

	public void set_SysSelected(String selected) {
		_sysSelected = selected;
	}

	public String get_SysSelected() {
		return _sysSelected;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisationSurrogate(long organisationSurrogate) {
		this.organisationSurrogate = organisationSurrogate;
	}

	public long getOrganisationSurrogate() {
		return organisationSurrogate;
	}

	public void setOrganisationName50a(String organisationName50a) {
		this.organisationName50a = organisationName50a;
	}

	public String getOrganisationName50a() {
		return organisationName50a;
	}

	public void setOrganisationType(OrganisationTypeEnum organisationType) {
		this.organisationType = organisationType;
	}

	public OrganisationTypeEnum getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationStatus(OrganisationStatusEnum organisationStatus) {
		this.organisationStatus = organisationStatus;
	}

	public OrganisationStatusEnum getOrganisationStatus() {
		return organisationStatus;
	}

	public void setOrganisationIsAwh(YesOrNoEnum organisationIsAwh) {
		this.organisationIsAwh = organisationIsAwh;
	}

	public YesOrNoEnum getOrganisationIsAwh() {
		return organisationIsAwh;
	}

	public void setOrganisationAwexCode(String organisationAwexCode) {
		this.organisationAwexCode = organisationAwexCode;
	}

	public String getOrganisationAwexCode() {
		return organisationAwexCode;
	}

}
