package au.awh.file.organisationdump.selectorganisationdump;

import org.springframework.beans.BeanUtils;


import au.awh.file.organisationdump.OrganisationDump;
import au.awh.service.data.SelectRecordDTO;

/**
 * Dto for file 'Organisation (Dump)' (WDORG) and function 'Select Organisation (Dump' (WDSYSJ3SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationDumpDTO extends SelectRecordDTO<SelectOrganisationDumpGDO> {
	private static final long serialVersionUID = -7300757519878109545L;

	private String organisation = "";
	private String organisationName50a = "";

	public SelectOrganisationDumpDTO() {
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisationName50a(String organisationName50a) {
		this.organisationName50a = organisationName50a;
	}

	public String getOrganisationName50a() {
		return organisationName50a;
	}

    /**\
     * Copies the fields of the Entity bean into the DTO bean.
     *
     * @param organisationDump OrganisationDump Entity bean
     */
    public void setDtoFields(OrganisationDump organisationDump) {
        BeanUtils.copyProperties(organisationDump, this);
    }

    /**
     * Copies the fields of the DTO bean into the Entity bean.
     *
     * @param organisationDump OrganisationDump Entity bean
     */
    public void setEntityFields(OrganisationDump organisationDump) {
        BeanUtils.copyProperties(this, organisationDump);
    }
}
