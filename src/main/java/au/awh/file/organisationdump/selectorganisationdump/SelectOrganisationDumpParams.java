package au.awh.file.organisationdump.selectorganisationdump;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Params for resource: SelectOrganisationDump (WDSYSJ3SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationDumpParams implements Serializable
{
    private static final long serialVersionUID = -7371256884050048849L;

	private String organisation = "";

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
}

