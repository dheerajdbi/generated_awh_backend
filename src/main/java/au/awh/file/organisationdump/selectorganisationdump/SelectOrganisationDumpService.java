package au.awh.file.organisationdump.selectorganisationdump;
    
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;
import au.awh.support.JobContext;

import java.io.IOException;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freschelegacy.utils.RestResponsePage;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;

import au.awh.file.organisationdump.OrganisationDumpRepository;


// DEBUG X2EFunction.generateServiceImportStatements2 BEGIN
// asServiceImportStatement
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileService;
// asServiceDtoImportStatement
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileDTO;
// asServiceParamsImportStatement
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileParams;
// asServiceResultImportStatement
import au.awh.file.utilitiesdisplay.loadscreenhdgsubfile.LoadScreenHdgSubfileResult;

// DEBUG X2EFunction.generateServiceImportStatements2 END


    
import au.awh.model.CmdKeyEnum;
import static au.awh.model.CmdKeyEnum.isActions;
import static au.awh.model.CmdKeyEnum.isAdd;
import static au.awh.model.CmdKeyEnum.isAddOrganisation;
import static au.awh.model.CmdKeyEnum.isAllSelected;
import static au.awh.model.CmdKeyEnum.isAllValues;
import static au.awh.model.CmdKeyEnum.isAlternateView;
import static au.awh.model.CmdKeyEnum.isCancel;
import static au.awh.model.CmdKeyEnum.isCancelOrExit;
import static au.awh.model.CmdKeyEnum.isCancel_1;
import static au.awh.model.CmdKeyEnum.isChangeBrokerCentre;
import static au.awh.model.CmdKeyEnum.isChangeClipSale;
import static au.awh.model.CmdKeyEnum.isChangeMode;
import static au.awh.model.CmdKeyEnum.isChangeRdb;
import static au.awh.model.CmdKeyEnum.isClearSelects;
import static au.awh.model.CmdKeyEnum.isCommandEntryWindow;
import static au.awh.model.CmdKeyEnum.isDelete;
import static au.awh.model.CmdKeyEnum.isDisplayAudit;
import static au.awh.model.CmdKeyEnum.isDisplayHideProcessed;
import static au.awh.model.CmdKeyEnum.isDisplayMessages;
import static au.awh.model.CmdKeyEnum.isDisplayUserIndex;
import static au.awh.model.CmdKeyEnum.isDuplicate;
import static au.awh.model.CmdKeyEnum.isDuplicateField;
import static au.awh.model.CmdKeyEnum.isExit;
import static au.awh.model.CmdKeyEnum.isExpand;
import static au.awh.model.CmdKeyEnum.isExtract;
import static au.awh.model.CmdKeyEnum.isF7f8;
import static au.awh.model.CmdKeyEnum.isGoToaddMode;
import static au.awh.model.CmdKeyEnum.isGoTochangeMode;
import static au.awh.model.CmdKeyEnum.isHelp;
import static au.awh.model.CmdKeyEnum.isHelpForHelp;
import static au.awh.model.CmdKeyEnum.isIdeographicConversion;
import static au.awh.model.CmdKeyEnum.isKeyScreen;
import static au.awh.model.CmdKeyEnum.isKeysUsedByObjectMove;
import static au.awh.model.CmdKeyEnum.isLocationItems;
import static au.awh.model.CmdKeyEnum.isLocationPromptList;
import static au.awh.model.CmdKeyEnum.isMessages;
import static au.awh.model.CmdKeyEnum.isNext;
import static au.awh.model.CmdKeyEnum.isNextPage;
import static au.awh.model.CmdKeyEnum.isOk;
import static au.awh.model.CmdKeyEnum.isPgupOrPageDown;
import static au.awh.model.CmdKeyEnum.isPosition;
import static au.awh.model.CmdKeyEnum.isPrevious;
import static au.awh.model.CmdKeyEnum.isPreviousPage;
import static au.awh.model.CmdKeyEnum.isPrintReport;
import static au.awh.model.CmdKeyEnum.isPrintSheets;
import static au.awh.model.CmdKeyEnum.isPrompt;
import static au.awh.model.CmdKeyEnum.isRefresh;
import static au.awh.model.CmdKeyEnum.isRepositionWindow;
import static au.awh.model.CmdKeyEnum.isReset;
import static au.awh.model.CmdKeyEnum.isResize;
import static au.awh.model.CmdKeyEnum.isSelect;
import static au.awh.model.CmdKeyEnum.isSelectAll;
import static au.awh.model.CmdKeyEnum.isSelectAll_1;
import static au.awh.model.CmdKeyEnum.isSubmitToBatch;
import static au.awh.model.CmdKeyEnum.isSubset;
import static au.awh.model.CmdKeyEnum.isUpdate;
import au.awh.model.RecordSelectedEnum;
import au.awh.model.ReloadSubfileEnum;
import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 10
import au.awh.model.CmdKeyEnum;
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * SELRCD Service controller for 'Select Organisation (Dump' (WDSYSJ3SRK) of file 'Organisation (Dump)' (WDORG)
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
@Service(value = "OrganisationDump_SelectOrganisationDump")
public class SelectOrganisationDumpService extends AbstractService<SelectOrganisationDumpService, SelectOrganisationDumpState> {
    
	@Autowired
	private JobContext job;

	@Autowired
	private OrganisationDumpRepository organisationDumpRepository;
        
    @Autowired
    private LoadScreenHdgSubfileService loadScreenHdgSubfileService;
    

    
    public static final String SCREEN_SELECT = "selectOrganisationDump";

	private final Step execute = define("execute", SelectOrganisationDumpParams.class, this::executeService);
	private final Step response = define("response", SelectOrganisationDumpDTO.class, this::processResponse);
	//private final Step serviceLoadScreenHdgSubfile = define("serviceLoadScreenHdgSubfile",LoadScreenHdgSubfileResult.class, this::processServiceLoadScreenHdgSubfile);
	
    

    @Autowired
    public SelectOrganisationDumpService()
    {
        super(SelectOrganisationDumpService.class, SelectOrganisationDumpState.class);
    }

    @Override
    public Step getInitialStep()
    {
        return execute;
    }

    /**
     * SelectOrganisationDump service starting point.
     * @param state  - Service state class.
     * @param params - Service input/output parameters class.
     * @return stepResult - Step result.
     */
    private StepResult executeService(SelectOrganisationDumpState state, SelectOrganisationDumpParams params)
    {
        StepResult stepResult = NO_ACTION;

        if (params != null) {
            BeanUtils.copyProperties(params, state);
        }
        usrInitializeProgram(state);
        stepResult =  mainLoop(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT initial processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult mainLoop(SelectOrganisationDumpState state)
    {
        StepResult stepResult = NO_ACTION;

        dbfReadFirstDataRecord(state);
        if (state.getPageGdo() != null && state.getPageGdo().getSize() > 0) {
            loadNextSubfilePage(state);
        }
        state.set_SysReloadSubfile(ReloadSubfileEnum._STA_NO);
        stepResult = conductScreenConversation(state);

        return stepResult;
    }

    /**
     * SCREEN_SELECT display processing loop method.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult conductScreenConversation(SelectOrganisationDumpState state)
    {
        StepResult stepResult = NO_ACTION;

        if (state.get_SysReloadSubfile().equals(ReloadSubfileEnum._STA_NO)) {
            SelectOrganisationDumpDTO dto = new SelectOrganisationDumpDTO();
            state.setModalScreen(true);            
            state.setNextScreen("SelectOrganisationDump");
            BeanUtils.copyProperties(state, dto);
            stepResult = callScreen(SCREEN_SELECT, dto).thenCall(response);
        } else {
            stepResult = mainLoop(state);
        }

        return stepResult;
    }

    /**
     * SCREEN_SELECT returned response processing method.
     * @param state - Service state class.
     * @param dto - returned screen model.
     * @return stepResult - Step result.
     */
    private StepResult processResponse(SelectOrganisationDumpState state, SelectOrganisationDumpDTO dto)
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(dto, state);
        if (isExit(state.get_SysCmdKey())) {
            stepResult = closedown(state);
        }
        else if (isReset(state.get_SysCmdKey())) {
            //TODO: processResetRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isHelp(state.get_SysCmdKey())) {
            //TODO:processHelpRequest(state);//synon built-in function
            stepResult = conductScreenConversation(state);
        }
        else if (isNextPage(state.get_SysCmdKey())) {
            dbfReadNextDataRecord(state);
            loadNextSubfilePage(state);
            stepResult = conductScreenConversation(state);
        }
        else if (isPreviousPage(state.get_SysCmdKey())) {
        	dbfReadPreviousDataRecord(state);
            loadNextSubfilePage(state);
            stepResult = conductScreenConversation(state);
        }
        else {
            stepResult = usrProcessSubfileControl(state);
            //TODO:readFirstChangedSubfileRecord(state);//synon built-in function
//			while (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) {//TODO:while(Changed subfile record found)
//              for (SelectOrganisationDumpGDO gdo : ((Page<SelectOrganisationDumpGDO>) state.getPageGdo()).getContent())
//		        {
                    if (state.getOrganisation() != null && !state.getOrganisation().equals("") && state.get_SysEntrySelected()!=null && !state.get_SysEntrySelected().equals("")) {
                        SelectOrganisationDumpGDO gdo = null;
                        for(SelectOrganisationDumpGDO obj: state.getPageGdo().getContent()) {
                            if(obj.getOrganisation().equals(state.getOrganisation())) {
                                gdo = obj;
                                break;
                            }
                        }
                        usrProcessSelectedLine(state, gdo);
                        SelectOrganisationDumpResult params = new SelectOrganisationDumpResult();
                        BeanUtils.copyProperties(state, params);
                        stepResult = StepResult.returnFromService(params);
                        return stepResult;
                    }else {
                    	state.set_SysReturnCode(ReturnCodeEnum._FINISHED);
                    }
//                    usrProcessChangedSubfileRecord(state, gdo);
//                    usrScreenFunctionFields(state, gdo);
//                    //TODO:updateSubfileRecord(state, gdo);//synon built-in function
//                    //TODO:readNextChangedSubfileRecord(state);//synon built-in function
//                }
            if (!state.get_SysReturnCode().getCode().equals(ReturnCodeEnum._STA_NORMAL.getCode())) //TODO:if(positioning field values have changed)
            {
                state.set_SysReloadSubfile(ReloadSubfileEnum._STA_YES);
            }
            stepResult = usrProcessCommandKeys(state);
            if(stepResult != NO_ACTION) {
                return stepResult;
            }
            stepResult = conductScreenConversation(state);
        }

        return stepResult;
    }

    /**
     * Iterate on data loaded to do stuff on each record.
     * @param state - Service state class.
     */
    private void loadNextSubfilePage(SelectOrganisationDumpState state)
    {
        for (SelectOrganisationDumpGDO gdo : ((Page<SelectOrganisationDumpGDO>) state.getPageGdo()).getContent())
        {
            state.setRecordSelect(RecordSelectedEnum._STA_YES);
            //TODO:moveDbfRecordFieldsToSubfileRecord(state);//synon built-in function
            usrScreenFunctionFields(state, gdo);
            usrLoadSubfileRecordFromDbfRecord(state, gdo);
            if(state.getRecordSelect().getCode().equals(RecordSelectedEnum._STA_YES.getCode()))
            {
                //TODO:writeSubfileRecord(state);//synon built-in function
            }
        }
    }

    /**
     * Terminate this program
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult closedown(SelectOrganisationDumpState state)
    {
        StepResult stepResult = NO_ACTION;

        usrExitProgramProcessing(state);

        stepResult = exitProgram(state);

        return stepResult;
    }

    /**
     * Exit this program.
     * @param state - Service state class.
     * @return stepResult - Step result.
     */
    private StepResult exitProgram(SelectOrganisationDumpState state) {
        StepResult stepResult = NO_ACTION;

//        SelectOrganisationDumpResult params = new SelectOrganisationDumpResult();
//        BeanUtils.copyProperties(state, params);
        stepResult = StepResult.returnFromService(null);

        return stepResult;
    }
    
    /**
     * ------------------------- Generated DBF method ---------------------------
     */

    /**
     * Read data of the first page
     * @param state - Service state class.
     */
    private void dbfReadFirstDataRecord(SelectOrganisationDumpState state)
    {
        state.setPage(0);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the next page
     * @param state - Service state class.
     */
    private void dbfReadNextDataRecord(SelectOrganisationDumpState state)
    {
        state.setPage(state.getPage() + 1);
        dbfReadDataRecord(state);
    }

    /**
     * Read data of the previous page
     * @param state - Service state class.
     */
    private void dbfReadPreviousDataRecord(SelectOrganisationDumpState state)
    {
    	if(state.getPage()>0)
        state.setPage(state.getPage() - 1);
        dbfReadDataRecord(state);
    }
    /**
     * Read data of the actual page
     * @param state - Service state class.
     */
    private void dbfReadDataRecord(SelectOrganisationDumpState state)
    {
        List<Order> sortOrders = new ArrayList<Order>();
        Pageable pageable;

        try
        {
            @SuppressWarnings("unchecked")
            Map<String, String> sortDataMap = new ObjectMapper().readValue(state.getSortData(), LinkedHashMap.class);

            for (Map.Entry<String, String> entry : sortDataMap.entrySet())
            {
                if (entry.getValue() == null)
                {
                    continue;
                }

                sortOrders.add(new Order(Direction.fromString(entry.getValue()), entry.getKey()));
            }
        }
        catch (IOException ioe)
        {
        }

        if (CollectionUtils.isEmpty(sortOrders))
        {
            pageable = PageRequest.of(state.getPage(), state.getSize());
        }
        else
        {
            pageable = PageRequest.of(state.getPage(), state.getSize(), Sort.by(sortOrders));
    }

		RestResponsePage<SelectOrganisationDumpGDO> pageGdo = organisationDumpRepository.selectOrganisationDump(state.getOrganisation(),state.getOrganisationName50a(), pageable);
		state.setPageGdo(pageGdo);
	}
    
    /**
     * ---------------------- Programmatic user-point --------------------------
     */

	/**
	 * USER: Initialize Program (Generated:20)
	 */
    private StepResult usrInitializeProgram(SelectOrganisationDumpState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            LoadScreenHdgSubfileParams loadScreenHdgSubfileParams = null;
			LoadScreenHdgSubfileResult loadScreenHdgSubfileResult = null;
			// DEBUG genFunctionCall BEGIN 1000002 ACT Load Screen Hdg - Subfile - Utilities Display  *
			loadScreenHdgSubfileParams = new LoadScreenHdgSubfileParams();
			loadScreenHdgSubfileResult = new LoadScreenHdgSubfileResult();
			loadScreenHdgSubfileParams.setApplicationHeaderName("AWH Wool Dumping System");
			loadScreenHdgSubfileParams.setBrokerId("");
			loadScreenHdgSubfileParams.setStateCode(StateCodeEnum._NOT_ENTERED);
			loadScreenHdgSubfileParams.setCentreCode("");
			stepResult = loadScreenHdgSubfileService.execute(loadScreenHdgSubfileParams);
			if(stepResult!=NO_ACTION) { 
			loadScreenHdgSubfileResult = (LoadScreenHdgSubfileResult)((ReturnFromService)stepResult.getAction()).getResults();
			BeanUtils.copyProperties(loadScreenHdgSubfileResult , dto);
			}
			stepResult = NO_ACTION;
			// DEBUG genFunctionCall END
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Subfile Control (Generated:72)
	 */
    private StepResult usrProcessSubfileControl(SelectOrganisationDumpState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isCancel_1(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Cancel
				// DEBUG genFunctionCall BEGIN 1000011 ACT Exit program - return code CND.*No value selected
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NO_VALUE_SELECTED);
				stepResult = exitProgram(dto);
				//dummy conditional return to avoid dead code error...
				if(stepResult != NO_ACTION) {
				    return stepResult;
				}
			
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Selected Line (Generated:107)
	 */
    private StepResult usrProcessSelectedLine(SelectOrganisationDumpState dto, SelectOrganisationDumpGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 107 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Changed Subfile Record (Generated:101)
	 */
    private StepResult usrProcessChangedSubfileRecord(SelectOrganisationDumpState dto, SelectOrganisationDumpGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 101 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * CALC: Screen Function Fields (Generated:165)
	 */
    private StepResult usrScreenFunctionFields(SelectOrganisationDumpState dto, SelectOrganisationDumpGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 165 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Process Command Keys (Generated:143)
	 */
    private StepResult usrProcessCommandKeys(SelectOrganisationDumpState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 143 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Load Subfile Record from DBF Record (Generated:41)
	 */
    private StepResult usrLoadSubfileRecordFromDbfRecord(SelectOrganisationDumpState dto, SelectOrganisationDumpGDO gdo)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			// Unprocessed SUB 41 -
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

	/**
	 * USER: Exit Program Processing (Generated:132)
	 */
    private StepResult usrExitProgramProcessing(SelectOrganisationDumpState dto)
    {
        StepResult stepResult = NO_ACTION;

        try
        {
            
			if (isExit(dto.get_SysCmdKey())) {
				// CTL.*CMD key is *Exit
				// DEBUG genFunctionCall BEGIN 1000017 ACT Send error message - 'Function key not allowed'
				//dto.addMessage("", "function.key.not.allowed", messageSource);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000019 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return stepResult;
    }

    /**
     * ---------------------- Programmatic user-point: process call service --------------------------
     */
//
//    /**
//     * LoadScreenHdgSubfileService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceLoadScreenHdgSubfile(SelectOrganisationDumpState state, LoadScreenHdgSubfileResult serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        BeanUtils.copyProperties(serviceResult, state);
//
//        //TODO: call the continuation of the program
//        //stepResult = ??;
//
//        return stepResult;
//    }
//


}
