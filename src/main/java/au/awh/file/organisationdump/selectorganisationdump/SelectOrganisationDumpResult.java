package au.awh.file.organisationdump.selectorganisationdump;

import java.io.Serializable;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: SelectOrganisationDump (WDSYSJ3SRK).
 *
 * @author X2EGenerator SELRCDJavaControllerGenerator.kt
 */
public class SelectOrganisationDumpResult implements Serializable
{
    private static final long serialVersionUID = -3348576011739184217L;

	private String organisation = "";

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
}

