package au.awh.file.organisationdump;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 7
import au.awh.model.OrganisationTypeEnum;
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Organisation (Dump) (WDORG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface OrganisationDumpRepository extends OrganisationDumpRepositoryCustom, JpaRepository<OrganisationDump, OrganisationDumpId> {

	List<OrganisationDump> findAllBy();

	List<OrganisationDump> findAllByIdOrganisation(String organisation);

	List<OrganisationDump> findAllByIdOrganisationGreaterThan(String organisation);

	List<OrganisationDump> findAllByOrganisationAwexCode(String organisationAwexCode);

	List<OrganisationDump> findAllByOrganisationName50a(String organisationName50a);

	List<OrganisationDump> findAllByOrganisationSurrogate(long organisationSurrogate);
}
