package au.awh.file.organisationdump;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.List;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpGDO;

/**
 * Custom Spring Data JPA repository interface for model: Organisation (Dump) (WDORG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface OrganisationDumpRepositoryCustom {

	/**
	 * 
	 * @param organisation Organisation
	 * @param organisationName50a Organisation Name 50A
	 * @param pageable a Pageable object used for pagination
	 * @return Page of SelectOrganisationDumpGDO
	 */
	RestResponsePage<SelectOrganisationDumpGDO> selectOrganisationDump(String organisation
	, String organisationName50a, Pageable pageable);

	/**
	 * 
	 * @param organisation Organisation
	 * @return OrganisationDump
	 */
	OrganisationDump getOrganisationDump(String organisation
	);
}
