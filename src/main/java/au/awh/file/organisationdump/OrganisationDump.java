package au.awh.file.organisationdump;

import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.YesOrNoEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WDORG")
public class OrganisationDump implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private OrganisationDumpId id = new OrganisationDumpId();

	@Column(name = "IZORGAWEX")
	private String organisationAwexCode = "";
	
	@Column(name = "IZORGAWH")
	private YesOrNoEnum organisationIsAwh = YesOrNoEnum._NOT_ENTERED;
	
	@Column(name = "IZORGNAM")
	private String organisationName50a = "";
	
	@Column(name = "IZORGSTS")
	private OrganisationStatusEnum organisationStatus = null;
	
	@Column(name = "IZORGSGT")
	private long organisationSurrogate = 0L;
	
	@Column(name = "IZORGTYP")
	private OrganisationTypeEnum organisationType = null;

	public OrganisationDumpId getId() {
		return id;
	}

	public String getOrganisation() {
		return id.getOrganisation();
	}

	public long getOrganisationSurrogate() {
		return organisationSurrogate;
	}
	
	public OrganisationTypeEnum getOrganisationType() {
		return organisationType;
	}
	
	public OrganisationStatusEnum getOrganisationStatus() {
		return organisationStatus;
	}
	
	public YesOrNoEnum getOrganisationIsAwh() {
		return organisationIsAwh;
	}
	
	public String getOrganisationName50a() {
		return organisationName50a;
	}
	
	public String getOrganisationAwexCode() {
		return organisationAwexCode;
	}

	

	

	public void setOrganisation(String organisation) {
		this.id.setOrganisation(organisation);
	}

	public void setOrganisationSurrogate(long organisationSurrogate) {
		this.organisationSurrogate = organisationSurrogate;
	}
	
	public void setOrganisationType(OrganisationTypeEnum organisationType) {
		this.organisationType = organisationType;
	}
	
	public void setOrganisationStatus(OrganisationStatusEnum organisationStatus) {
		this.organisationStatus = organisationStatus;
	}
	
	public void setOrganisationIsAwh(YesOrNoEnum organisationIsAwh) {
		this.organisationIsAwh = organisationIsAwh;
	}
	
	public void setOrganisationName50a(String organisationName50a) {
		this.organisationName50a = organisationName50a;
	}
	
	public void setOrganisationAwexCode(String organisationAwexCode) {
		this.organisationAwexCode = organisationAwexCode;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
