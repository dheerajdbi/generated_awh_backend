package au.awh.file.organisationdump.getorganisationdump;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetOrganisationDumpDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private OrganisationStatusEnum organisationStatus;
	private OrganisationTypeEnum organisationType;
	private String organisation;
	private String organisationAwexCode;
	private String organisationName50a;
	private YesOrNoEnum organisationIsAwh;
	private long organisationSurrogate;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getOrganisation() {
		return organisation;
	}

	public String getOrganisationAwexCode() {
		return organisationAwexCode;
	}

	public YesOrNoEnum getOrganisationIsAwh() {
		return organisationIsAwh;
	}

	public String getOrganisationName50a() {
		return organisationName50a;
	}

	public OrganisationStatusEnum getOrganisationStatus() {
		return organisationStatus;
	}

	public long getOrganisationSurrogate() {
		return organisationSurrogate;
	}

	public OrganisationTypeEnum getOrganisationType() {
		return organisationType;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public void setOrganisationAwexCode(String organisationAwexCode) {
		this.organisationAwexCode = organisationAwexCode;
	}

	public void setOrganisationIsAwh(YesOrNoEnum organisationIsAwh) {
		this.organisationIsAwh = organisationIsAwh;
	}

	public void setOrganisationName50a(String organisationName50a) {
		this.organisationName50a = organisationName50a;
	}

	public void setOrganisationStatus(OrganisationStatusEnum organisationStatus) {
		this.organisationStatus = organisationStatus;
	}

	public void setOrganisationSurrogate(long organisationSurrogate) {
		this.organisationSurrogate = organisationSurrogate;
	}

	public void setOrganisationType(OrganisationTypeEnum organisationType) {
		this.organisationType = organisationType;
	}

}
