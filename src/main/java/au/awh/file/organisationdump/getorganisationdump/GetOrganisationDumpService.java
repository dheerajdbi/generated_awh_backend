package au.awh.file.organisationdump.getorganisationdump;
//retrieveObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.organisationdump.OrganisationDump;
import au.awh.file.organisationdump.OrganisationDumpId;
import au.awh.file.organisationdump.OrganisationDumpRepository;
import au.awh.model.OrganisationStatusEnum;
import au.awh.model.OrganisationTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;


import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.AbstractService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

/**
 * RTVOBJ Service controller for 'Get Organisation (Dump)' (file 'Organisation (Dump)' (WDORG)
 *
 * @author X2EGenerator
 */
@Service
public class GetOrganisationDumpService extends AbstractService<GetOrganisationDumpService, GetOrganisationDumpDTO>
{
    private final Step execute = define("execute", GetOrganisationDumpParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private OrganisationDumpRepository organisationDumpRepository;

	

    @Autowired
    public GetOrganisationDumpService()
    {
        super(GetOrganisationDumpService.class, GetOrganisationDumpDTO.class);
    }

 	@Override
	public Step getInitialStep() {
		return execute;
	}

    /**
     * Non-CallStack service entry-point as a plain Java method.
     */
    public StepResult execute(GetOrganisationDumpParams params) throws ServiceException {
        GetOrganisationDumpDTO dto = new GetOrganisationDumpDTO();
        return executeService(dto, params);
    }

	private StepResult executeService(GetOrganisationDumpDTO dto, GetOrganisationDumpParams params) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;

        BeanUtils.copyProperties(params, dto);

		initializeRoutine(dto);

		List<OrganisationDump> organisationDumpList = organisationDumpRepository.findAllByIdOrganisation(dto.getOrganisation());
		if (!organisationDumpList.isEmpty()) {
			for (OrganisationDump organisationDump : organisationDumpList) {
				processDataRecord(dto, organisationDump);
			}
			exitProcessing(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		}
		else {
			processingIfDataRecordNotFound(dto);
            dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		}
 
        GetOrganisationDumpResult result = new GetOrganisationDumpResult();
        BeanUtils.copyProperties(dto, result);
        stepResult = StepResult.returnFromService(result);

        return stepResult;
	}

    private StepResult initializeRoutine(GetOrganisationDumpDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Initialize Routine (Generated:48)
		 */
		
		// Unprocessed SUB 48 -

        return stepResult;
    }

    private StepResult processDataRecord(GetOrganisationDumpDTO dto, OrganisationDump organisationDump) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Process Data Record (Generated:41)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
		// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
		// DEBUG genFunctionCall END
		// * Assume this is a FIFO partial key access.
		// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult processingIfDataRecordNotFound(GetOrganisationDumpDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Processing if Data Record Not Found (Generated:52)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
		dto.setOrganisationSurrogate(0L);
		dto.setOrganisationName50a("");
		dto.setOrganisationType(null);
		dto.setOrganisationStatus(null);
		dto.setOrganisationIsAwh(YesOrNoEnum._NOT_ENTERED);
		dto.setOrganisationAwexCode("");
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
		dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
		// DEBUG genFunctionCall END

        return stepResult;
    }

    private StepResult exitProcessing(GetOrganisationDumpDTO dto) throws ServiceException
    {
        StepResult stepResult = NO_ACTION;
		/**
		 * USER: Exit Processing (Generated:61)
		 */
		
		// Unprocessed SUB 61 -

        return stepResult;
    }


}
