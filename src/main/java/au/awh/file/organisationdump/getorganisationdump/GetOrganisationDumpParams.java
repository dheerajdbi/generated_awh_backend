package au.awh.file.organisationdump.getorganisationdump;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetOrganisationDump ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetOrganisationDumpParams implements Serializable
{
	private static final long serialVersionUID = -8331219527149269969L;

    private String organisation = "";

	public String getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
}
