package au.awh.file.lastsurrogate.updcrtlastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.LastUsedSurrogateTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: UpdcrtLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdcrtLastSurrogateParams implements Serializable
{
	private static final long serialVersionUID = 9180849305704680743L;

    private LastUsedSurrogateTypeEnum lastSurrogateType = null;

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}
	
	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}
	
	public void setLastSurrogateType(String lastSurrogateType) {
		setLastSurrogateType(LastUsedSurrogateTypeEnum.valueOf(lastSurrogateType));
	}
}
