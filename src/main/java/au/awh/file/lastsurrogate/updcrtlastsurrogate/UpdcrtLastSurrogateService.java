package au.awh.file.lastsurrogate.updcrtlastsurrogate;

// ChangeObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
// imports for Services
import au.awh.file.lastsurrogate.crtelastsurrogate.CrteLastSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.crtelastsurrogate.CrteLastSurrogateDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.crtelastsurrogate.CrteLastSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.crtelastsurrogate.CrteLastSurrogateResult;
// imports section complete

/**
 * CHGOBJ Service controller for 'UpdCrt Last Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  ChangeObjectFunction.kt
 */
@Service
public class UpdcrtLastSurrogateService extends AbstractService<UpdcrtLastSurrogateService, UpdcrtLastSurrogateDTO>
{
	private final Step execute = define("execute", UpdcrtLastSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	@Autowired
	private CrteLastSurrogateService crteLastSurrogateService;

	//private final Step serviceCrteLastSurrogate = define("serviceCrteLastSurrogate",CrteLastSurrogateResult.class, this::processServiceCrteLastSurrogate);
	
	@Autowired
	public UpdcrtLastSurrogateService() {
		super(UpdcrtLastSurrogateService.class, UpdcrtLastSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(UpdcrtLastSurrogateParams params) throws ServiceException {
		UpdcrtLastSurrogateDTO dto = new UpdcrtLastSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(UpdcrtLastSurrogateDTO dto, UpdcrtLastSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		processingBeforeDataRead(dto);

		LastSurrogateId lastSurrogateId = new LastSurrogateId();
		lastSurrogateId.setLastSurrogateType(dto.getLastSurrogateType());
		LastSurrogate lastSurrogate = lastSurrogateRepository.findById(lastSurrogateId).orElse(null);
		if (lastSurrogate == null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST);
			processingIfDataRecordNotFound(dto);
		}
		else {
			processingAfterDataRead(dto, lastSurrogate);
			lastSurrogate.setLastSurrogateType(dto.getLastSurrogateType());
			lastSurrogate.setLastSurrogate(dto.getLastSurrogate());
			lastSurrogate.setLastSurrogateMax(dto.getLastSurrogateMax());
			lastSurrogate.setLastSurrogateLoop(dto.getLastSurrogateLoop());

			processingBeforeDataUpdate(dto, lastSurrogate);

			try {
				lastSurrogateRepository.saveAndFlush(lastSurrogate);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto);
			} catch (Exception e) {
				System.out.println("Change error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
			}
		}

		UpdcrtLastSurrogateResult result = new UpdcrtLastSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataRead(UpdcrtLastSurrogateDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Generated:8)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000064 ACT PAR.Last Surrogate = CND.Not Entered
		dto.setLastSurrogate(0);
		// DEBUG genFunctionCall END
		return stepResult;
	}

	private StepResult processingIfDataRecordNotFound(UpdcrtLastSurrogateDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Not Found (Generated:43)
		 */
		CrteLastSurrogateResult crteLastSurrogateResult = null;
		CrteLastSurrogateParams crteLastSurrogateParams = null;
		// * Ignore the error and try and create it.
		// DEBUG genFunctionCall BEGIN 1000042 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000032 ACT CrtE Last Surrogate - Last Surrogate  *
		crteLastSurrogateParams = new CrteLastSurrogateParams();
		crteLastSurrogateResult = new CrteLastSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, crteLastSurrogateParams);
		crteLastSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
		crteLastSurrogateParams.setLastSurrogate("1");
		crteLastSurrogateParams.setLastSurrogateMax("999999999");
		crteLastSurrogateParams.setLastSurrogateLoop("N");
		crteLastSurrogateParams.setErrorProcessing(dto.getLclErrorProcessing());
		stepResult = crteLastSurrogateService.execute(crteLastSurrogateParams);
		crteLastSurrogateResult = (CrteLastSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(crteLastSurrogateResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL) {
			// PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000057 ACT PAR.Last Surrogate = CND.Minimum
			dto.setLastSurrogate(1);
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000053 ACT PAR.Last Surrogate = CND.Not Entered
			dto.setLastSurrogate(0);
			// DEBUG genFunctionCall END
		}
		// * Exit with return code for calling function to process.
		// * Bypass default error handling.
		// DEBUG genFunctionCall BEGIN 1000061 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END
		return stepResult;
	}

	private StepResult processingAfterDataRead(UpdcrtLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Read (Generated:48)
		 */
		
		// Unprocessed SUB 48 -
		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(UpdcrtLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Update (Generated:10)
		 */
		
		if (
		// DEBUG ComparatorJavaGenerator BEGIN
		lastSurrogate.getLastSurrogate() >= lastSurrogate.getLastSurrogateMax()
		// DEBUG ComparatorJavaGenerator END
		) {
			// DB1.Last Surrogate GE DB1.Last Surrogate Max
			if (lastSurrogate.getLastSurrogateLoop() == YesOrNoEnum._YES) {
				// DB1.Last Surrogate Loop is Yes
				// DEBUG genFunctionCall BEGIN 1000077 ACT DB1.Last Surrogate = CND.Not Entered
				lastSurrogate.setLastSurrogate(0);
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000013 ACT PGM.*Return code = CND.*Value comparison
				dto.set_SysReturnCode(ReturnCodeEnum._STA_VALUE_COMPARISON);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000017 ACT PAR.Last Surrogate = CND.Not Entered
				dto.setLastSurrogate(0);
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1000021 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		// DEBUG genFunctionCall BEGIN 1000023 ACT DB1.Last Surrogate = DB1.Last Surrogate + CON.1
		lastSurrogate.setLastSurrogate(lastSurrogate.getLastSurrogate() + 1);
		// DEBUG genFunctionCall END
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(UpdcrtLastSurrogateDTO dto) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing After Data Update (Generated:23)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000028 ACT PAR.Last Surrogate = DB1.Last Surrogate
		// DEBUG genFunctionCall END
		return stepResult;
	}

//
//    /**
//     * CrteLastSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCrteLastSurrogate(UpdcrtLastSurrogateDTO dto, CrteLastSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        UpdcrtLastSurrogateParams updcrtLastSurrogateParams = new UpdcrtLastSurrogateParams();
//        BeanUtils.copyProperties(dto, updcrtLastSurrogateParams);
//        stepResult = StepResult.callService(UpdcrtLastSurrogateService.class, updcrtLastSurrogateParams);
//
//        return stepResult;
//    }
//
}
