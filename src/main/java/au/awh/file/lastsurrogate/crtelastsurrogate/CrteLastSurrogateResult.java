package au.awh.file.lastsurrogate.crtelastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CrteLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CrteLastSurrogateResult implements Serializable
{
	private static final long serialVersionUID = -4749478368059440453L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
