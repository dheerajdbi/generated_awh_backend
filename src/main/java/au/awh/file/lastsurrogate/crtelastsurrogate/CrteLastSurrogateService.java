package au.awh.file.lastsurrogate.crtelastsurrogate;

// ExecuteInternalFunction.kt File=LastSurrogate (SG) Function=crteLastSurrogate (1459815) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
// imports for Services
import au.awh.file.lastsurrogate.createlastsurrogate.CreateLastSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.createlastsurrogate.CreateLastSurrogateDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.SflselEnum;
import au.awh.model.WsListFieldIdEnum;
import au.awh.model.WsListItemStatusEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.createlastsurrogate.CreateLastSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.createlastsurrogate.CreateLastSurrogateResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'CrtE Last Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class CrteLastSurrogateService extends AbstractService<CrteLastSurrogateService, CrteLastSurrogateDTO>
{
	private final Step execute = define("execute", CrteLastSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	@Autowired
	private CreateLastSurrogateService createLastSurrogateService;

	//private final Step serviceCreateLastSurrogate = define("serviceCreateLastSurrogate",CreateLastSurrogateResult.class, this::processServiceCreateLastSurrogate);
	
	@Autowired
	public CrteLastSurrogateService() {
		super(CrteLastSurrogateService.class, CrteLastSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CrteLastSurrogateParams params) throws ServiceException {
		CrteLastSurrogateDTO dto = new CrteLastSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CrteLastSurrogateDTO dto, CrteLastSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		CreateLastSurrogateResult createLastSurrogateResult = null;
		CreateLastSurrogateParams createLastSurrogateParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000274 ACT Create Last Surrogate - Last Surrogate  *
		createLastSurrogateParams = new CreateLastSurrogateParams();
		createLastSurrogateResult = new CreateLastSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, createLastSurrogateParams);
		createLastSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
		createLastSurrogateParams.setLastSurrogate(dto.getLastSurrogate());
		createLastSurrogateParams.setLastSurrogateMax(dto.getLastSurrogateMax());
		createLastSurrogateParams.setLastSurrogateLoop(dto.getLastSurrogateLoop());
		stepResult = createLastSurrogateService.execute(createLastSurrogateParams);
		createLastSurrogateResult = (CreateLastSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(createLastSurrogateResult.get_SysReturnCode());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS) {
				// PGM.*Return code is *Record already exists
				// DEBUG genFunctionCall BEGIN 1000287 ACT Send error message - '*Record already exists'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1001646) EXCINTFUN 1459815 LastSurrogate.crteLastSurrogate 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000209 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1459815 LastSurrogate.crteLastSurrogate 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000217 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000224 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000229 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		CrteLastSurrogateResult result = new CrteLastSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * CreateLastSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceCreateLastSurrogate(CrteLastSurrogateDTO dto, CreateLastSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        CrteLastSurrogateParams crteLastSurrogateParams = new CrteLastSurrogateParams();
//        BeanUtils.copyProperties(dto, crteLastSurrogateParams);
//        stepResult = StepResult.callService(CrteLastSurrogateService.class, crteLastSurrogateParams);
//
//        return stepResult;
//    }
//
}
