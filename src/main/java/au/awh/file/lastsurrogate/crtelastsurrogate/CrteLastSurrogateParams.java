package au.awh.file.lastsurrogate.crtelastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 5
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.YesOrNoEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CrteLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CrteLastSurrogateParams implements Serializable
{
	private static final long serialVersionUID = 5283963597369000216L;

    private LastUsedSurrogateTypeEnum lastSurrogateType = null;
    private ErrorProcessingEnum errorProcessing = null;
    private long lastSurrogate = 0L;
    private long lastSurrogateMax = 0L;
    private YesOrNoEnum lastSurrogateLoop = null;

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}
	
	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}
	
	public void setLastSurrogateType(String lastSurrogateType) {
		setLastSurrogateType(LastUsedSurrogateTypeEnum.valueOf(lastSurrogateType));
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public long getLastSurrogate() {
		return lastSurrogate;
	}
	
	public void setLastSurrogate(long lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}
	
	public void setLastSurrogate(String lastSurrogate) {
		setLastSurrogate(Long.parseLong(lastSurrogate));
	}
	
	public long getLastSurrogateMax() {
		return lastSurrogateMax;
	}
	
	public void setLastSurrogateMax(long lastSurrogateMax) {
		this.lastSurrogateMax = lastSurrogateMax;
	}
	
	public void setLastSurrogateMax(String lastSurrogateMax) {
		setLastSurrogateMax(Long.parseLong(lastSurrogateMax));
	}
	
	public YesOrNoEnum getLastSurrogateLoop() {
		return lastSurrogateLoop;
	}
	
	public void setLastSurrogateLoop(YesOrNoEnum lastSurrogateLoop) {
		this.lastSurrogateLoop = lastSurrogateLoop;
	}
	
	public void setLastSurrogateLoop(String lastSurrogateLoop) {
		setLastSurrogateLoop(YesOrNoEnum.valueOf(lastSurrogateLoop));
	}
}
