package au.awh.file.lastsurrogate.createlastsurrogate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CreateLastSurrogateDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private LastUsedSurrogateTypeEnum lastSurrogateType;
	private YesOrNoEnum lastSurrogateLoop;
	private long lastSurrogate;
	private long lastSurrogateMax;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public long getLastSurrogate() {
		return lastSurrogate;
	}

	public YesOrNoEnum getLastSurrogateLoop() {
		return lastSurrogateLoop;
	}

	public long getLastSurrogateMax() {
		return lastSurrogateMax;
	}

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setLastSurrogate(long lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}

	public void setLastSurrogateLoop(YesOrNoEnum lastSurrogateLoop) {
		this.lastSurrogateLoop = lastSurrogateLoop;
	}

	public void setLastSurrogateMax(long lastSurrogateMax) {
		this.lastSurrogateMax = lastSurrogateMax;
	}

	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}

}
