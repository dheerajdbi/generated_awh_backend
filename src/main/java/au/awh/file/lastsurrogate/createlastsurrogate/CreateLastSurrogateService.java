package au.awh.file.lastsurrogate.createlastsurrogate;

// CreateObjectFunction.kt

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;

/**
 * CRTOBJ Service controller for 'Create Last Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  CreateObjectFunction.kt
 */
@Service
public class CreateLastSurrogateService  extends AbstractService<CreateLastSurrogateService, CreateLastSurrogateDTO>
{
	private final Step execute = define("execute", CreateLastSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	
	@Autowired
	public CreateLastSurrogateService() {
		super(CreateLastSurrogateService.class, CreateLastSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CreateLastSurrogateParams params) throws ServiceException {
		CreateLastSurrogateDTO dto = new CreateLastSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CreateLastSurrogateDTO dto, CreateLastSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		LastSurrogate lastSurrogate = new LastSurrogate();
		lastSurrogate.setLastSurrogateType(dto.getLastSurrogateType());
		lastSurrogate.setLastSurrogate(dto.getLastSurrogate());
		lastSurrogate.setLastSurrogateMax(dto.getLastSurrogateMax());
		lastSurrogate.setLastSurrogateLoop(dto.getLastSurrogateLoop());

		processingBeforeDataUpdate(dto, lastSurrogate);

		LastSurrogate lastSurrogate2 = lastSurrogateRepository.findById(lastSurrogate.getId()).orElse(null);
		if (lastSurrogate2 != null) {
			dto.set_SysReturnCode(ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
			processingIfDataRecordAlreadyExists(dto, lastSurrogate2);
		}
		else {
			try {
				lastSurrogateRepository.saveAndFlush(lastSurrogate);
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				processingAfterDataUpdate(dto, lastSurrogate);
			} catch (Exception e) {
				System.out.println("Create error: " + Arrays.toString(e.getStackTrace()));
				dto.set_SysReturnCode(ReturnCodeEnum._STA_DATA_UPDATE_ERROR);
				processingIfDataUpdateError(dto, lastSurrogate);
			}
		}

		CreateLastSurrogateResult result = new CreateLastSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

	private StepResult processingBeforeDataUpdate(CreateLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing Before Data Read (Empty:8)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataRecordAlreadyExists(CreateLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing if Data Record Already Exists (Empty:43)
		 */
		
		return stepResult;
	}

	private StepResult processingAfterDataUpdate(CreateLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * USER: Processing after Data Update (Empty:11)
		 */
		
		return stepResult;
	}

	private StepResult processingIfDataUpdateError(CreateLastSurrogateDTO dto, LastSurrogate lastSurrogate) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		/**
		 * Processing if Data Update Error (Empty:48)
		 */
		
		return stepResult;
	}


}
