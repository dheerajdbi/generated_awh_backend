package au.awh.file.lastsurrogate.createlastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CreateLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CreateLastSurrogateResult implements Serializable
{
	private static final long serialVersionUID = -4464853580536723781L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
