package au.awh.file.lastsurrogate;

import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.YesOrNoEnum;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name="WSLSRG")
public class LastSurrogate implements Serializable {
	private static final long serialVersionUID = -1L;

	
	
	

	@EmbeddedId
	private LastSurrogateId id = new LastSurrogateId();

	@Column(name = "SGLSRG")
	private long lastSurrogate = 0L;
	
	@Column(name = "SGLSRGLOOP")
	private YesOrNoEnum lastSurrogateLoop = YesOrNoEnum._NOT_ENTERED;
	
	@Column(name = "SGLSRGMAX")
	private long lastSurrogateMax = 0L;

	public LastSurrogateId getId() {
		return id;
	}

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return id.getLastSurrogateType();
	}

	public long getLastSurrogate() {
		return lastSurrogate;
	}
	
	public long getLastSurrogateMax() {
		return lastSurrogateMax;
	}
	
	public YesOrNoEnum getLastSurrogateLoop() {
		return lastSurrogateLoop;
	}

	

	

	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.id.setLastSurrogateType(lastSurrogateType);
	}

	public void setLastSurrogate(long lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}
	
	public void setLastSurrogateMax(long lastSurrogateMax) {
		this.lastSurrogateMax = lastSurrogateMax;
	}
	
	public void setLastSurrogateLoop(YesOrNoEnum lastSurrogateLoop) {
		this.lastSurrogateLoop = lastSurrogateLoop;
	}

	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
