package au.awh.file.lastsurrogate.getxnextsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
import au.awh.model.LastUsedSurrogateTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetxNextSurrogate (WDSYSM3XFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetxNextSurrogateParams implements Serializable
{
	private static final long serialVersionUID = 8976411544628969597L;

    private LastUsedSurrogateTypeEnum lastSurrogateType = null;

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}
	
	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}
	
	public void setLastSurrogateType(String lastSurrogateType) {
		setLastSurrogateType(LastUsedSurrogateTypeEnum.valueOf(lastSurrogateType));
	}
}
