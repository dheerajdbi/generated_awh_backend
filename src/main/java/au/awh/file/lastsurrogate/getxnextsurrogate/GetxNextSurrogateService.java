package au.awh.file.lastsurrogate.getxnextsurrogate;

// ExecuteExternalFunction.kt File=LastSurrogate (SG) Function=getxNextSurrogate (1464894) WDSYSM3XFK

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
// imports for Services
import au.awh.file.lastsurrogate.updcrtelastsurrogate.UpdcrteLastSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.updcrtelastsurrogate.UpdcrteLastSurrogateDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.updcrtelastsurrogate.UpdcrteLastSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.updcrtelastsurrogate.UpdcrteLastSurrogateResult;
// imports section complete

/**
 * EXCEXTFNC Service controller for 'GetX Next Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  ExecuteExternalFunction.kt
 */
@Service
public class GetxNextSurrogateService extends AbstractService<GetxNextSurrogateService, GetxNextSurrogateDTO>
{
	private final Step execute = define("execute", GetxNextSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	@Autowired
	private UpdcrteLastSurrogateService updcrteLastSurrogateService;

	//private final Step serviceUpdcrteLastSurrogate = define("serviceUpdcrteLastSurrogate",UpdcrteLastSurrogateResult.class, this::processServiceUpdcrteLastSurrogate);
	
	@Autowired
	public GetxNextSurrogateService() {
		super(GetxNextSurrogateService.class, GetxNextSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GetxNextSurrogateParams params) throws ServiceException {
		GetxNextSurrogateDTO dto = new GetxNextSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GetxNextSurrogateDTO dto, GetxNextSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:1)
		 */
		UpdcrteLastSurrogateResult updcrteLastSurrogateResult = null;
		UpdcrteLastSurrogateParams updcrteLastSurrogateParams = null;
		// * Ignore initialisation errors.
		// DEBUG genFunctionCall BEGIN 1000005 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000025 ACT UpdCrtE Last Surrogate - Last Surrogate  *
		updcrteLastSurrogateParams = new UpdcrteLastSurrogateParams();
		updcrteLastSurrogateResult = new UpdcrteLastSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, updcrteLastSurrogateParams);
		updcrteLastSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
		updcrteLastSurrogateParams.setErrorProcessing("2");
		stepResult = updcrteLastSurrogateService.execute(updcrteLastSurrogateParams);
		updcrteLastSurrogateResult = (UpdcrteLastSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(updcrteLastSurrogateResult.get_SysReturnCode());
		dto.setLastSurrogate(updcrteLastSurrogateResult.getLastSurrogate());
		// DEBUG genFunctionCall END
		// * Exit with return code for calling function to process.
		// DEBUG genFunctionCall BEGIN 1000010 ACT Exit program - return code PGM.*Return code
		// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
		// DEBUG genFunctionCall END

		GetxNextSurrogateResult result = new GetxNextSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * UpdcrteLastSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceUpdcrteLastSurrogate(GetxNextSurrogateDTO dto, UpdcrteLastSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GetxNextSurrogateParams getxNextSurrogateParams = new GetxNextSurrogateParams();
//        BeanUtils.copyProperties(dto, getxNextSurrogateParams);
//        stepResult = StepResult.callService(GetxNextSurrogateService.class, getxNextSurrogateParams);
//
//        return stepResult;
//    }
//
}
