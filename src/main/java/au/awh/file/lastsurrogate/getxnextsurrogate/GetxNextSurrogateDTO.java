package au.awh.file.lastsurrogate.getxnextsurrogate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.GlobalContext;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetxNextSurrogateDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Autowired
	private GlobalContext globalCtx;

	private ReturnCodeEnum _sysReturnCode;

	private LastUsedSurrogateTypeEnum lastSurrogateType;
	private long lastSurrogate;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public long getLastSurrogate() {
		return lastSurrogate;
	}

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}

	public String getWfLastSurrogateLoop() {
		return globalCtx.getString("lastSurrogateLoop");
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setLastSurrogate(long lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}

	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}

	public void setWfLastSurrogateLoop(String wfLastSurrogateLoop) {
		globalCtx.setString("lastSurrogateLoop", wfLastSurrogateLoop);
	}

}
