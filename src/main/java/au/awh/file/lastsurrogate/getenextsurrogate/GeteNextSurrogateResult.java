package au.awh.file.lastsurrogate.getenextsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GeteNextSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteNextSurrogateResult implements Serializable
{
	private static final long serialVersionUID = -4245102742054024501L;

	private ReturnCodeEnum _sysReturnCode;
	private BigDecimal lastSurrogate = BigDecimal.ZERO; // long 37072

	public BigDecimal getLastSurrogate() {
		return lastSurrogate;
	}
	
	public void setLastSurrogate(BigDecimal lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}
	
	
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
