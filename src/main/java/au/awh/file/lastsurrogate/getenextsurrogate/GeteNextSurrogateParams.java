package au.awh.file.lastsurrogate.getenextsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GeteNextSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GeteNextSurrogateParams implements Serializable
{
	private static final long serialVersionUID = 8390382840748590752L;

    private LastUsedSurrogateTypeEnum lastSurrogateType = null;
    private ErrorProcessingEnum errorProcessing = null;

	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}
	
	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}
	
	public void setLastSurrogateType(String lastSurrogateType) {
		setLastSurrogateType(LastUsedSurrogateTypeEnum.valueOf(lastSurrogateType));
	}
	
	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
}
