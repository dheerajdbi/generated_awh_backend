package au.awh.file.lastsurrogate.getenextsurrogate;

// ExecuteInternalFunction.kt File=LastSurrogate (SG) Function=geteNextSurrogate (1459792) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
// imports for Services
import au.awh.file.lastsurrogate.getxnextsurrogate.GetxNextSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.getxnextsurrogate.GetxNextSurrogateDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.getxnextsurrogate.GetxNextSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.getxnextsurrogate.GetxNextSurrogateResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'GetE Next Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GeteNextSurrogateService extends AbstractService<GeteNextSurrogateService, GeteNextSurrogateDTO>
{
	private final Step execute = define("execute", GeteNextSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	@Autowired
	private GetxNextSurrogateService getxNextSurrogateService;

	//private final Step serviceGetxNextSurrogate = define("serviceGetxNextSurrogate",GetxNextSurrogateResult.class, this::processServiceGetxNextSurrogate);
	
	@Autowired
	public GeteNextSurrogateService() {
		super(GeteNextSurrogateService.class, GeteNextSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GeteNextSurrogateParams params) throws ServiceException {
		GeteNextSurrogateDTO dto = new GeteNextSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GeteNextSurrogateDTO dto, GeteNextSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		GetxNextSurrogateResult getxNextSurrogateResult = null;
		GetxNextSurrogateParams getxNextSurrogateParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000214 ACT GetX Next Surrogate - Last Surrogate  *
		getxNextSurrogateParams = new GetxNextSurrogateParams();
		getxNextSurrogateResult = new GetxNextSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getxNextSurrogateParams);
		getxNextSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
		stepResult = getxNextSurrogateService.execute(getxNextSurrogateParams);
		getxNextSurrogateResult = (GetxNextSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getxNextSurrogateResult.get_SysReturnCode());
		dto.setLastSurrogate(getxNextSurrogateResult.getLastSurrogate());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum._STA_VALUE_COMPARISON) {
				// PGM.*Return code is *Value comparison
				// DEBUG genFunctionCall BEGIN 1000207 ACT Send error message - 'Last Used Surrogate limit'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1419844) EXCINTFUN 1459792 LastSurrogate.geteNextSurrogate 
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - 'Last Used Surrogate get'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1350759) EXCINTFUN 1459792 LastSurrogate.geteNextSurrogate 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		GeteNextSurrogateResult result = new GeteNextSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * GetxNextSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceGetxNextSurrogate(GeteNextSurrogateDTO dto, GetxNextSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        GeteNextSurrogateParams geteNextSurrogateParams = new GeteNextSurrogateParams();
//        BeanUtils.copyProperties(dto, geteNextSurrogateParams);
//        stepResult = StepResult.callService(GeteNextSurrogateService.class, geteNextSurrogateParams);
//
//        return stepResult;
//    }
//
}
