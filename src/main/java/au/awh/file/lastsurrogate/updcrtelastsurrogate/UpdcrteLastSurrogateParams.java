package au.awh.file.lastsurrogate.updcrtelastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 2
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: UpdcrteLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdcrteLastSurrogateParams implements Serializable
{
	private static final long serialVersionUID = -1596057591239064812L;

    private ErrorProcessingEnum errorProcessing = null;
    private LastUsedSurrogateTypeEnum lastSurrogateType = null;

	public ErrorProcessingEnum getErrorProcessing() {
		return errorProcessing;
	}
	
	public void setErrorProcessing(ErrorProcessingEnum errorProcessing) {
		this.errorProcessing = errorProcessing;
	}
	
	public void setErrorProcessing(String errorProcessing) {
		setErrorProcessing(ErrorProcessingEnum.valueOf(errorProcessing));
	}
	
	public LastUsedSurrogateTypeEnum getLastSurrogateType() {
		return lastSurrogateType;
	}
	
	public void setLastSurrogateType(LastUsedSurrogateTypeEnum lastSurrogateType) {
		this.lastSurrogateType = lastSurrogateType;
	}
	
	public void setLastSurrogateType(String lastSurrogateType) {
		setLastSurrogateType(LastUsedSurrogateTypeEnum.valueOf(lastSurrogateType));
	}
}
