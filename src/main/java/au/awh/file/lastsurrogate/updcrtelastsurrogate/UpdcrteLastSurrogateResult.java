package au.awh.file.lastsurrogate.updcrtelastsurrogate;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: UpdcrteLastSurrogate ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class UpdcrteLastSurrogateResult implements Serializable
{
	private static final long serialVersionUID = 1034930719207345160L;

	private ReturnCodeEnum _sysReturnCode;
	private long lastSurrogate = 0L; // long 37072

	public long getLastSurrogate() {
		return lastSurrogate;
	}
	
	public void setLastSurrogate(long lastSurrogate) {
		this.lastSurrogate = lastSurrogate;
	}
	
	public void setLastSurrogate(String lastSurrogate) {
		setLastSurrogate(Long.parseLong(lastSurrogate));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
