package au.awh.file.lastsurrogate.updcrtelastsurrogate;

// ExecuteInternalFunction.kt File=LastSurrogate (SG) Function=updcrteLastSurrogate (1464895) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.lastsurrogate.LastSurrogate;
import au.awh.file.lastsurrogate.LastSurrogateId;
import au.awh.file.lastsurrogate.LastSurrogateRepository;
// imports for Services
import au.awh.file.lastsurrogate.updcrtlastsurrogate.UpdcrtLastSurrogateService;
// imports for DTO
import au.awh.file.lastsurrogate.updcrtlastsurrogate.UpdcrtLastSurrogateDTO;
// imports for Enums
import au.awh.model.ErrorProcessingEnum;
import au.awh.model.ExitProgramOptionEnum;
import au.awh.model.LastUsedSurrogateTypeEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.YesOrNoEnum;
// imports for Parameters
import au.awh.file.lastsurrogate.updcrtlastsurrogate.UpdcrtLastSurrogateParams;
// imports for Results
import au.awh.file.lastsurrogate.updcrtlastsurrogate.UpdcrtLastSurrogateResult;
// imports section complete

/**
 * EXCINTFNC Service controller for 'UpdCrtE Last Surrogate' (file 'Last Surrogate' (WSLSRG)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class UpdcrteLastSurrogateService extends AbstractService<UpdcrteLastSurrogateService, UpdcrteLastSurrogateDTO>
{
	private final Step execute = define("execute", UpdcrteLastSurrogateParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private LastSurrogateRepository lastSurrogateRepository;

	@Autowired
	private UpdcrtLastSurrogateService updcrtLastSurrogateService;

	//private final Step serviceUpdcrtLastSurrogate = define("serviceUpdcrtLastSurrogate",UpdcrtLastSurrogateResult.class, this::processServiceUpdcrtLastSurrogate);
	
	@Autowired
	public UpdcrteLastSurrogateService() {
		super(UpdcrteLastSurrogateService.class, UpdcrteLastSurrogateDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(UpdcrteLastSurrogateParams params) throws ServiceException {
		UpdcrteLastSurrogateDTO dto = new UpdcrteLastSurrogateDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(UpdcrteLastSurrogateDTO dto, UpdcrteLastSurrogateParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		UpdcrtLastSurrogateResult updcrtLastSurrogateResult = null;
		UpdcrtLastSurrogateParams updcrtLastSurrogateParams = null;
		// * WARNING: This function must never be implemented as a sub-routine
		// * as the use of *QUIT for the requested action will not work.
		// DEBUG genFunctionCall BEGIN 1000010 ACT UpdCrt Last Surrogate - Last Surrogate  *
		updcrtLastSurrogateParams = new UpdcrtLastSurrogateParams();
		updcrtLastSurrogateResult = new UpdcrtLastSurrogateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, updcrtLastSurrogateParams);
		updcrtLastSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
		stepResult = updcrtLastSurrogateService.execute(updcrtLastSurrogateParams);
		updcrtLastSurrogateResult = (UpdcrtLastSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(updcrtLastSurrogateResult.get_SysReturnCode());
		dto.setLastSurrogate(updcrtLastSurrogateResult.getLastSurrogate());
		// DEBUG genFunctionCall END
		if (!(dto.get_SysReturnCode() == ReturnCodeEnum._STA_NORMAL)) {
			// NOT PGM.*Return code is *Normal
			if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Do not send message
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1464895 LastSurrogate.updcrteLastSurrogate 
				// DEBUG genFunctionCall END
			}
			if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Ignore
				// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum._STA_NORMAL);
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Quit
				// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
				// PAR.Error Processing is *Exit
				// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
			}
		}

		UpdcrteLastSurrogateResult result = new UpdcrteLastSurrogateResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}

//
//    /**
//     * UpdcrtLastSurrogateService returned response processing method.
//     * @param state - Service state class.
//     * @param serviceResult - returned service model.
//     * @return
//     */
//    private StepResult processServiceUpdcrtLastSurrogate(UpdcrteLastSurrogateDTO dto, UpdcrtLastSurrogateParams serviceResult)
//    {
//        StepResult stepResult = NO_ACTION;
//
//        if(serviceResult != null) {
//            BeanUtils.copyProperties(serviceResult, dto);
//        }
//
//        //TODO: call the continuation of the program
//        UpdcrteLastSurrogateParams updcrteLastSurrogateParams = new UpdcrteLastSurrogateParams();
//        BeanUtils.copyProperties(dto, updcrteLastSurrogateParams);
//        stepResult = StepResult.callService(UpdcrteLastSurrogateService.class, updcrteLastSurrogateParams);
//
//        return stepResult;
//    }
//
}
