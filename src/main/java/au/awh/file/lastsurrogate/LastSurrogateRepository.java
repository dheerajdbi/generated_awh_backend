package au.awh.file.lastsurrogate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: Last Surrogate (WSLSRG).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface LastSurrogateRepository extends LastSurrogateRepositoryCustom, JpaRepository<LastSurrogate, LastSurrogateId> {
}
