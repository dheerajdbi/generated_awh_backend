package au.awh.file.datelistheader.getcentreholidaylist;

// ExecuteInternalFunction.kt File=DateListHeader (YH) Function=getCentreHolidayList (1345248) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.file.datelistheader.DateListHeader;
import au.awh.file.datelistheader.DateListHeaderId;
import au.awh.file.datelistheader.DateListHeaderRepository;
import au.awh.model.ReturnCodeEnum;

/**
 * EXCINTFNC Service controller for 'Get Centre Holiday List' (file '*Date List Header' (Y2DLSTH)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class GetCentreHolidayListService extends AbstractService<GetCentreHolidayListService, GetCentreHolidayListDTO>
{
	private final Step execute = define("execute", GetCentreHolidayListParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	@Autowired
	private DateListHeaderRepository dateListHeaderRepository;

	
	@Autowired
	public GetCentreHolidayListService() {
		super(GetCentreHolidayListService.class, GetCentreHolidayListDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GetCentreHolidayListParams params) throws ServiceException {
		GetCentreHolidayListDTO dto = new GetCentreHolidayListDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GetCentreHolidayListDTO dto, GetCentreHolidayListParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		
		if ((dto.getCentreCodeKey().equals("PB")) || (dto.getCentreCodeKey().equals("RH")) || (dto.getCentreCodeKey().equals("NF"))) {
			// DEBUG genFunctionCall BEGIN 1000020 ACT PAR.*Date List name = CONCAT(CON.F,CON.HOLIDAY,CON.2)
			dto.setDateListName(String.format("%s%2s%s", "F", "", "HOLIDAY"));
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000002 ACT PAR.*Date List name = CONCAT(PAR.Centre Code           KEY,CON. HOLIDAY,CND.*All)
			dto.setDateListName(dto.getCentreCodeKey().concat("HOLIDAY"));
			// DEBUG genFunctionCall END
		}

		GetCentreHolidayListResult result = new GetCentreHolidayListResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
