package au.awh.file.datelistheader.getcentreholidaylist;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetCentreHolidayList ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentreHolidayListResult implements Serializable
{
	private static final long serialVersionUID = -3523844702887196002L;

	private ReturnCodeEnum _sysReturnCode;
	private String dateListName = ""; // String 598

	public String getDateListName() {
		return dateListName;
	}
	
	public void setDateListName(String dateListName) {
		this.dateListName = dateListName;
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
