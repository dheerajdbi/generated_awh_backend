package au.awh.file.datelistheader.getcentreholidaylist;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: GetCentreHolidayList ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetCentreHolidayListParams implements Serializable
{
	private static final long serialVersionUID = 4186720571976271218L;

    private String centreCodeKey = "";

	public String getCentreCodeKey() {
		return centreCodeKey;
	}
	
	public void setCentreCodeKey(String centreCodeKey) {
		this.centreCodeKey = centreCodeKey;
	}
}
