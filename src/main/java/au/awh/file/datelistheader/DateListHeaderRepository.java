package au.awh.file.datelistheader;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.math.BigDecimal;

// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END
/**
 * Spring Data JPA repository interface for model: *Date List Header (Y2DLSTH).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public interface DateListHeaderRepository extends DateListHeaderRepositoryCustom, JpaRepository<DateListHeader, DateListHeaderId> {

	List<DateListHeader> findAllBy();
}
