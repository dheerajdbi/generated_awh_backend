package au.awh.file.datelistheader;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DateListHeaderId implements Serializable {
	private static final long serialVersionUID = -1L;

	@Column(name = "YHDLN")
	private String dateListName = "";

	public DateListHeaderId() {
	
	}

	public DateListHeaderId(String dateListName) { 	this.dateListName = dateListName;
	}

	public String getDateListName() {
		return dateListName;
	}
	
	public void setDateListName(String dateListName) {
		this.dateListName = dateListName;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
