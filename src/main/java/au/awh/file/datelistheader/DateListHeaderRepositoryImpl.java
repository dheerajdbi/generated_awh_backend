package au.awh.file.datelistheader;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.freschelegacy.utils.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import org.springframework.stereotype.Repository;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END
import au.awh.file.datelistheader.DateListHeader;

/**
 * Custom Spring Data JPA repository implementation for model: *Date List Header (Y2DLSTH).
 *
 * @author X2EGenerator SpringDataJpaRepositoryGenerator
 */
@Repository
public class DateListHeaderRepositoryImpl implements DateListHeaderRepositoryCustom {
	@PersistenceContext
	EntityManager em;

}
