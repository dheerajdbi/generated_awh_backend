package au.awh.file.utilitiesdisplay.loadscreenhdgsubfile;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: LoadScreenHdgSubfile (WSLDSCRSFL).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgSubfileResult implements Serializable
{
	private static final long serialVersionUID = 4923336771263416059L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
