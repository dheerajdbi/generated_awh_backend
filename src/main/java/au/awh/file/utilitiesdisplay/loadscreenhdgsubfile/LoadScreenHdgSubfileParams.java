package au.awh.file.utilitiesdisplay.loadscreenhdgsubfile;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: LoadScreenHdgSubfile (WSLDSCRSFL).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgSubfileParams implements Serializable
{
	private static final long serialVersionUID = -6665684427187535751L;

    private String applicationHeaderName = "";
    private String brokerId = "";
    private StateCodeEnum stateCode = null;
    private String centreCode = "";

	public String getApplicationHeaderName() {
		return applicationHeaderName;
	}
	
	public void setApplicationHeaderName(String applicationHeaderName) {
		this.applicationHeaderName = applicationHeaderName;
	}
	
	public String getBrokerId() {
		return brokerId;
	}
	
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	
	public StateCodeEnum getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}
	
	public void setStateCode(String stateCode) {
		setStateCode(StateCodeEnum.valueOf(stateCode));
	}
	
	public String getCentreCode() {
		return centreCode;
	}
	
	public void setCentreCode(String centreCode) {
		this.centreCode = centreCode;
	}
}
