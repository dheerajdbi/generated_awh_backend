package au.awh.file.utilitiesdisplay.loadscreenhdghh;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: LoadScreenHdgHH (WSUTDQUFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgHHParams implements Serializable
{
	private static final long serialVersionUID = 6123013890239447392L;

    private String centreName = "";

	public String getCentreName() {
		return centreName;
	}
	
	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}
}
