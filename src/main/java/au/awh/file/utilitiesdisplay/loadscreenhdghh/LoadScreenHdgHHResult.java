package au.awh.file.utilitiesdisplay.loadscreenhdghh;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: LoadScreenHdgHH (WSUTDQUFK).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgHHResult implements Serializable
{
	private static final long serialVersionUID = 2158999864865803223L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
