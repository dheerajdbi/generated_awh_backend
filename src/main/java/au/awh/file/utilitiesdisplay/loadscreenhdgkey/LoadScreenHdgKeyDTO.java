package au.awh.file.utilitiesdisplay.loadscreenhdgkey;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;
import au.awh.model.StateCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class LoadScreenHdgKeyDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private StateCodeEnum stateCode;
	private String applicationHeaderName;
	private String brokerId;
	private String centreCode;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public String getApplicationHeaderName() {
		return applicationHeaderName;
	}

	public String getBrokerId() {
		return brokerId;
	}

	public String getCentreCode() {
		return centreCode;
	}

	public StateCodeEnum getStateCode() {
		return stateCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setApplicationHeaderName(String applicationHeaderName) {
		this.applicationHeaderName = applicationHeaderName;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}

	public void setCentreCode(String centreCode) {
		this.centreCode = centreCode;
	}

	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}

}
