package au.awh.file.utilitiesdisplay.loadscreenhdgkey;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 0
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: LoadScreenHdgKey (WSLDSCRKEY).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgKeyResult implements Serializable
{
	private static final long serialVersionUID = -1194977306496252204L;

	private ReturnCodeEnum _sysReturnCode;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
