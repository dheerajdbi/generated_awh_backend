package au.awh.file.utilitiesdisplay.loadscreenhdgkey;

import java.io.Serializable;

import java.math.BigDecimal;
// generateStatusFieldImportStatements BEGIN 4
import au.awh.model.StateCodeEnum;
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: LoadScreenHdgKey (WSLDSCRKEY).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class LoadScreenHdgKeyParams implements Serializable
{
	private static final long serialVersionUID = -7307439089558600497L;

    private String centreCode = "";
    private String brokerId = "";
    private StateCodeEnum stateCode = null;
    private String applicationHeaderName = "";

	public String getCentreCode() {
		return centreCode;
	}
	
	public void setCentreCode(String centreCode) {
		this.centreCode = centreCode;
	}
	
	public String getBrokerId() {
		return brokerId;
	}
	
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	
	public StateCodeEnum getStateCode() {
		return stateCode;
	}
	
	public void setStateCode(StateCodeEnum stateCode) {
		this.stateCode = stateCode;
	}
	
	public void setStateCode(String stateCode) {
		setStateCode(StateCodeEnum.valueOf(stateCode));
	}
	
	public String getApplicationHeaderName() {
		return applicationHeaderName;
	}
	
	public void setApplicationHeaderName(String applicationHeaderName) {
		this.applicationHeaderName = applicationHeaderName;
	}
}
