package au.awh.file.utilitiesdatetime.getlastmonthdates;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 2
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: GetLastMonthDates (WSSYSF0UPR).
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class GetLastMonthDatesResult implements Serializable
{
	private static final long serialVersionUID = -2814200199498741242L;

	private ReturnCodeEnum _sysReturnCode;
	private LocalDate dateFromIso = null; // LocalDate 41090
	private LocalDate dateToIso = null; // LocalDate 41091

	public LocalDate getDateFromIso() {
		return dateFromIso;
	}
	
	public void setDateFromIso(LocalDate dateFromIso) {
		this.dateFromIso = dateFromIso;
	}
	
	public void setDateFromIso(String dateFromIso) {
		setDateFromIso(new LocalDateConverter().convert(dateFromIso));
	}
	
	public LocalDate getDateToIso() {
		return dateToIso;
	}
	
	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
	}
	
	public void setDateToIso(String dateToIso) {
		setDateToIso(new LocalDateConverter().convert(dateToIso));
	}
	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
