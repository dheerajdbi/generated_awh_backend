package au.awh.file.utilitiesdatetime.getlastmonthdates;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GetLastMonthDatesDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private LocalDate dateFromIso;
	private LocalDate dateToIso;


	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public LocalDate getDateFromIso() {
		return dateFromIso;
	}

	public LocalDate getDateToIso() {
		return dateToIso;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setDateFromIso(LocalDate dateFromIso) {
		this.dateFromIso = dateFromIso;
	}

	public void setDateToIso(LocalDate dateToIso) {
		this.dateToIso = dateToIso;
	}

}
