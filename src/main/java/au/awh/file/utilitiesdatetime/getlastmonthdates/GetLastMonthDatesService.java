package au.awh.file.utilitiesdatetime.getlastmonthdates;

// ExecuteUserProgramFunction.kt File=UtilitiesDateTime (G5) Function=getLastMonthDates (2028022) WSSYSF0UPR

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.ReturnCodeEnum;

/**
 * EXCUSRPGM Service controller for 'Get Last Month Dates' (file 'Utilities Date/Time' (WSG5RCP)
 *
 * @author X2EGenerator  ExecuteUserProgramFunction.kt
 */
@Service
public class GetLastMonthDatesService extends AbstractService<GetLastMonthDatesService, GetLastMonthDatesDTO>
{
	private final Step execute = define("execute", GetLastMonthDatesParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public GetLastMonthDatesService() {
		super(GetLastMonthDatesService.class, GetLastMonthDatesDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(GetLastMonthDatesParams params) throws ServiceException {
		GetLastMonthDatesDTO dto = new GetLastMonthDatesDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(GetLastMonthDatesDTO dto, GetLastMonthDatesParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Empty:2)
		 */
		

		GetLastMonthDatesResult result = new GetLastMonthDatesResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
