package au.awh.file.utilitiesdatetime.cvtdatetoddmmformat;

// ExecuteInternalFunction.kt File=UtilitiesDateTime (G5) Function=cvtDateToDdMmFormat (1663944) 

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.freschelegacy.utils.DateTimeUtils;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import com.freschesolutions.libs.callstack.action.ReturnFromService;
import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.awh.service.ServiceException;
import au.awh.support.JobContext;
import au.awh.model.DateDetailTypeEnum;
import au.awh.model.DateListAutoloadEnum;
import au.awh.model.ExcludedDaysOfWeekEnum;
import au.awh.model.ReturnCodeEnum;
import au.awh.model.RoundedEnum;
import au.awh.model.SelectedDaysDatesEnum;

/**
 * EXCINTFNC Service controller for 'Cvt Date to DD/MM format' (file 'Utilities Date/Time' (WSG5RCP)
 *
 * @author X2EGenerator  ExecuteInternalFunction.kt
 */
@Service
public class CvtDateToDdMmFormatService extends AbstractService<CvtDateToDdMmFormatService, CvtDateToDdMmFormatDTO>
{
	private final Step execute = define("execute", CvtDateToDdMmFormatParams.class, (t, u) -> {
		try {
			return executeService(t, u);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	});
	
	@Autowired
	private JobContext job;

	
	@Autowired
	public CvtDateToDdMmFormatService() {
		super(CvtDateToDdMmFormatService.class, CvtDateToDdMmFormatDTO.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	/**
	 * Non-CallStack service entry-point as a plain Java method.
	 */
	public StepResult execute(CvtDateToDdMmFormatParams params) throws ServiceException {
		CvtDateToDdMmFormatDTO dto = new CvtDateToDdMmFormatDTO();
		return executeService(dto, params);
	}

	private StepResult executeService(CvtDateToDdMmFormatDTO dto, CvtDateToDdMmFormatParams params) throws ServiceException {
		StepResult stepResult = NO_ACTION;

		BeanUtils.copyProperties(params, dto);

		/**
		 *  (Generated:2)
		 */
		
		// DEBUG genFunctionCall BEGIN 1000004 ACT LCL.Month NBR = PAR.Date From ISO *MONTH
		// TODO: The '*DATE DETAIL' built-in Function is not supported yet
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000002 ACT LCL.Day = PAR.Date From ISO *DAY OF MONTH
		// TODO: The '*DATE DETAIL' built-in Function is not supported yet
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000034 ACT LCL.Date DDMM = LCL.Day * CON.100 *
		dto.setLclDateDdmm(dto.getLclDay() * 100);
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000041 ACT PAR.Date DDMM = LCL.Date DDMM + LCL.Month NBR
		dto.setDateDdmm(dto.getLclDateDdmm() + dto.getLclMonthNbr());
		// DEBUG genFunctionCall END

		CvtDateToDdMmFormatResult result = new CvtDateToDdMmFormatResult();
		BeanUtils.copyProperties(dto, result);
		stepResult = StepResult.returnFromService(result);

		return stepResult;
	}


}
