package au.awh.file.utilitiesdatetime.cvtdatetoddmmformat;

import java.io.Serializable;

import java.math.BigDecimal;

import java.time.LocalDate;

import au.awh.config.LocalDateConverter;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Parameter(s) for resource: CvtDateToDdMmFormat ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CvtDateToDdMmFormatParams implements Serializable
{
	private static final long serialVersionUID = -9163417077625896109L;

    private LocalDate dateFromIso = null;

	public LocalDate getDateFromIso() {
		return dateFromIso;
	}
	
	public void setDateFromIso(LocalDate dateFromIso) {
		this.dateFromIso = dateFromIso;
	}
	
	public void setDateFromIso(String dateFromIso) {
		setDateFromIso(new LocalDateConverter().convert(dateFromIso));
	}
}
