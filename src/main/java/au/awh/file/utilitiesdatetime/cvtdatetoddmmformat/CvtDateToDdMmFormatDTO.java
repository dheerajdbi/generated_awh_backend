package au.awh.file.utilitiesdatetime.cvtdatetoddmmformat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import au.awh.model.ReturnCodeEnum;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CvtDateToDdMmFormatDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private ReturnCodeEnum _sysReturnCode;

	private LocalDate dateFromIso;
	private long dateDdmm;

	private long lclDateDdmm;
	private long lclDay;
	private long lclMonthNbr;

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public long getDateDdmm() {
		return dateDdmm;
	}

	public LocalDate getDateFromIso() {
		return dateFromIso;
	}

	public long getLclDateDdmm() {
		return lclDateDdmm;
	}

	public long getLclDay() {
		return lclDay;
	}

	public long getLclMonthNbr() {
		return lclMonthNbr;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

	public void setDateDdmm(long dateDdmm) {
		this.dateDdmm = dateDdmm;
	}

	public void setDateFromIso(LocalDate dateFromIso) {
		this.dateFromIso = dateFromIso;
	}

	public void setLclDateDdmm(long lclDateDdmm) {
		this.lclDateDdmm = lclDateDdmm;
	}

	public void setLclDay(long lclDay) {
		this.lclDay = lclDay;
	}

	public void setLclMonthNbr(long lclMonthNbr) {
		this.lclMonthNbr = lclMonthNbr;
	}

}
