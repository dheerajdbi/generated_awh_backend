package au.awh.file.utilitiesdatetime.cvtdatetoddmmformat;

import java.io.Serializable;

import java.math.BigDecimal;

import au.awh.model.ReturnCodeEnum;
// generateStatusFieldImportStatements BEGIN 1
// generateStatusFieldImportStatements END

/**
 * Result(s) for resource: CvtDateToDdMmFormat ().
 *
 * @author X2EGenerator FunctionBase.kt
 */
public class CvtDateToDdMmFormatResult implements Serializable
{
	private static final long serialVersionUID = -7317732426143609035L;

	private ReturnCodeEnum _sysReturnCode;
	private BigDecimal dateDdmm = BigDecimal.ZERO; // long 45767

	public BigDecimal getDateDdmm() {
		return dateDdmm;
	}
	
	public void setDateDdmm(BigDecimal dateDdmm) {
		this.dateDdmm = dateDdmm;
	}
	

	public ReturnCodeEnum get_SysReturnCode() {
		return _sysReturnCode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_sysReturnCode = returnCode;
	}

}
