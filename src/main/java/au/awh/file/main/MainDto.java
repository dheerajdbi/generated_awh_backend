package au.awh.file.main;

import java.io.Serializable;

import au.awh.service.data.BaseDTO;

public class MainDto extends BaseDTO implements Serializable {

	private String option;
	 private java.util.Map<String,String> confirmMessage=new java.util.TreeMap<String,String>();	
	    public java.util.Map<String,String> getConfirmMessage() {
	    	  return confirmMessage;}


	    	  public void setConfirmMessage(java.util.Map<String,String> confirmMessage) {
	    	  this.confirmMessage = confirmMessage;}

	    	  public void setConfirmationMessages(String messageType,String message){
	    		  this.confirmMessage.put(messageType, message);
	    	  }

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}
	
}
