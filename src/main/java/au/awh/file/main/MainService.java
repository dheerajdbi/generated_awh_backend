package au.awh.file.main;

import static com.freschesolutions.libs.callstack.StepResult.NO_ACTION;
import static com.freschesolutions.libs.callstack.StepResult.callScreen;

import au.awh.file.awhregion.editawhregion.EditAwhRegionParams;
import au.awh.file.awhregion.editawhregion.EditAwhRegionService;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionParams;
import au.awh.file.awhregion.selectawhregion.SelectAwhRegionService;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreParams;
import au.awh.file.centre.wsut5502selectcentre.Wsut5502SelectCentreService;
import au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk.WrkEquipmentReadingsWspltwqdfkParams;
import au.awh.file.equipmentreadingcontrol.wrkequipmentreadingswspltwqdfk.WrkEquipmentReadingsWspltwqdfkService;
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingParams;
import au.awh.file.equipmentreadings.dspequipmentreading.DspEquipmentReadingService;
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsParams;
import au.awh.file.equipmentreadings.edtequipmentreadings.EdtEquipmentReadingsService;
import au.awh.file.equipmentreadings.prtleasorreadingspmt.PrtLeasorReadingsPmtParams;
import au.awh.file.equipmentreadings.prtleasorreadingspmt.PrtLeasorReadingsPmtService;
import au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk.WrkEquipmentReadingsWspltwsdfkParams;
import au.awh.file.equipmentreadings.wrkequipmentreadingswspltwsdfk.WrkEquipmentReadingsWspltwsdfkService;
import au.awh.file.equipmenttype.edtequipmenttypes.EdtEquipmentTypesParams;
import au.awh.file.equipmenttype.edtequipmenttypes.EdtEquipmentTypesService;
import au.awh.file.equipmenttype.pmtforequipmentbkdwn.PmtForEquipmentBkdwnParams;
import au.awh.file.equipmenttype.pmtforequipmentbkdwn.PmtForEquipmentBkdwnService;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeParams;
import au.awh.file.equipmenttype.selequipmenttype.SelEquipmentTypeService;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpParams;
import au.awh.file.organisationdump.selectorganisationdump.SelectOrganisationDumpService;
import au.awh.file.plantequipment.buildreadingbatch.BuildReadingBatchParams;
import au.awh.file.plantequipment.buildreadingbatch.BuildReadingBatchService;
import au.awh.file.plantequipment.displayplantequipment.DisplayPlantEquipmentParams;
import au.awh.file.plantequipment.displayplantequipment.DisplayPlantEquipmentService;
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentParams;
import au.awh.file.plantequipment.edtplantequipment.EdtPlantEquipmentService;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkParams;
import au.awh.file.plantequipment.equipmentreadingswspltwmdfk.EquipmentReadingsWspltwmdfkService;
import au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentParams;
import au.awh.file.plantequipment.wrkplantequipment.WrkPlantEquipmentService;
import com.freschesolutions.libs.callstack.AbstractService;
import com.freschesolutions.libs.callstack.Step;
import com.freschesolutions.libs.callstack.StepResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainService extends AbstractService<MainService, MainDto> {
	public static final String SCREEN_KEY = "main";
	private final Step keyScreenResponse = define("keyScreen", MainDto.class, this::processKeyScreenResponse);
	private final Step processServiceResponse = define("processServiceResponse", Object.class,
			this::processServiceResponse);
	private final Step execute = define("execute", MainDto.class, this::execute);

	@Autowired
	public MainService() {
		super(MainService.class, MainDto.class);
	}

	@Override
	public Step getInitialStep() {
		return execute;
	}

	private StepResult execute(MainDto state, MainDto params) {
		StepResult result = callScreen(SCREEN_KEY, state).thenCall(keyScreenResponse);

		return result;
	}

	private StepResult processKeyScreenResponse(MainDto state, MainDto model) {
		StepResult result = NO_ACTION;

		BeanUtils.copyProperties(model, state);
		switch (state.getOption()) {

		case "WSPLTWJEFK":
			result = StepResult.callService(EdtEquipmentTypesService.class, new EdtEquipmentTypesParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWHSRK":
			result = StepResult.callService(SelectAwhRegionService.class, new SelectAwhRegionParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWTD1K":
			result = StepResult.callService(DspEquipmentReadingService.class, new DspEquipmentReadingParams())
					.thenCall(processServiceResponse);
			break;
		case "WDSYSJ3SRK":
			result = StepResult.callService(SelectOrganisationDumpService.class, new SelectOrganisationDumpParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTW9D1K":
			result = StepResult.callService(DisplayPlantEquipmentService.class, new DisplayPlantEquipmentParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWLSRK":
			result = StepResult.callService(SelEquipmentTypeService.class, new SelEquipmentTypeParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWQDFK":
			result = StepResult.callService(WrkEquipmentReadingsWspltwqdfkService.class,
					new WrkEquipmentReadingsWspltwqdfkParams()).thenCall(processServiceResponse);
			break;
		case "WSPLTFZDFK":
			result = StepResult.callService(PmtForEquipmentBkdwnService.class, new PmtForEquipmentBkdwnParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWCDFK":
			result = StepResult.callService(WrkPlantEquipmentService.class, new WrkPlantEquipmentParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWWE1K":
			result = StepResult.callService(EdtEquipmentReadingsService.class, new EdtEquipmentReadingsParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWIEFK":
			result = StepResult.callService(EditAwhRegionService.class, new EditAwhRegionParams())
					.thenCall(processServiceResponse);
			break;
		case "WSPLTWSDFK":
			result = StepResult.callService(WrkEquipmentReadingsWspltwsdfkService.class,
					new WrkEquipmentReadingsWspltwsdfkParams()).thenCall(processServiceResponse);
			break;
		case "WSPLTWPPVK":
			result = StepResult.callService(BuildReadingBatchService.class, new BuildReadingBatchParams())
					.thenCall(processServiceResponse);
			break;
//		case "WSCF1220":
//			result = StepResult.callService(Wscf1220ChangeCentreService.class, new Wscf1220ChangeCentreParams())
//					.thenCall(processServiceResponse);
//			break;
		case "WSPLTWMDFK":
			result = StepResult
					.callService(EquipmentReadingsWspltwmdfkService.class, new EquipmentReadingsWspltwmdfkParams())
					.thenCall(processServiceResponse);
			break;
		case "WSUT5502":
			result = StepResult.callService(Wsut5502SelectCentreService.class, new Wsut5502SelectCentreParams())
					.thenCall(processServiceResponse);
			break;
		case "EditEquipmentTypesWSPLTWJEFK":
			result = StepResult.callService(EdtEquipmentTypesService.class, new EdtEquipmentTypesParams())
					.thenCall(processServiceResponse);
			break;
			
		case "WSPLTW6PVK":
			result = StepResult.callService(PrtLeasorReadingsPmtService.class, new PrtLeasorReadingsPmtParams())
					.thenCall(processServiceResponse);
			break;


		}

		return result;
	}

	private StepResult processServiceResponse(MainDto dto, Object response) {
		return StepResult.call(execute);
	}
}
