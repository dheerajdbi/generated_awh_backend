package au.awh.service.data;

/**
 * Transfer object for relaying message to programs or clients.
 *
 * @author Robin Rizvi
 * @since (2016-06-29.15:03:12)
 */
public class MessageDTO {
	private String code;
	private String field;
	private String message;

	public MessageDTO() {
	}

	public MessageDTO(String code, String field, String message) {
		this.code = code;
		this.field = field;
		this.message = message;
	}

	/**
	 * Gets message code.
	 *
	 * @return message code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets the field name that corresponds to this message.
	 *
	 * @return field name
	 */
	public String getField() {
		return field;
	}

	/**
	 * Gets message text.
	 *
	 * @return message text
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets message code.
	 *
	 * @param code message code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Sets the field name that corresponds to this message.
	 *
	 * @param fieldName name of field
	 */
	public void setField(String fieldName) {
		field = fieldName;
	}

	/**
	 * Sets message text.
	 *
	 * @param message message text
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
