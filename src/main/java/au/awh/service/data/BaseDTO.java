package au.awh.service.data;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.awh.model.CmdKeyEnum;
import au.awh.model.DeferConfirmEnum;
import au.awh.model.ProgramModeEnum;
import au.awh.model.ReturnCodeEnum;

/**
 * Common fields of Data Transfer Object.
 *
 * @author Amit Arya
 * @since (2015-11-17.12:47:12)
 */
public class BaseDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    private String _SysEntrySelected = "";
	private Map<String, String> _SysMessages = new TreeMap<String, String>();

	private CmdKeyEnum _SysCmdKey = CmdKeyEnum._STA_NONE;
	private DeferConfirmEnum _SysDeferConfirm = DeferConfirmEnum._PROCEED_TO_CONFIRM;
	private String _SysNotification = "";
 	private ProgramModeEnum  _SysProgramMode  = ProgramModeEnum._STA_CHANGE;
	private ReturnCodeEnum   _SysReturnCode   = ReturnCodeEnum._STA_NORMAL;

	public String get_SysEntrySelected() {
		return _SysEntrySelected;
	}

	public CmdKeyEnum get_SysCmdKey() {
		return _SysCmdKey;
	}

	public DeferConfirmEnum get_SysDeferConfirm() {
		return _SysDeferConfirm;
	}

 	public ProgramModeEnum get_SysProgramMode() {
		return _SysProgramMode;
	}

 	public ReturnCodeEnum get_SysReturnCode() {
		return _SysReturnCode;
	}

	public void set_SysEntrySelected(String entrySelected) {
		_SysEntrySelected = entrySelected;
	}

	public void set_SysCmdKey(CmdKeyEnum cmdKey) {
		_SysCmdKey = cmdKey;
	}

	public void set_SysDeferConfirm(DeferConfirmEnum deferConfirm) {
		_SysDeferConfirm = deferConfirm;
	}

	public void set_SysProgramMode(ProgramModeEnum programMode) {
		_SysProgramMode = programMode;
	}

	public void set_SysReturnCode(ReturnCodeEnum returnCode) {
		_SysReturnCode = returnCode;
    }

    @JsonIgnore()
    public void set_SysReturnCode(String returnCode) {
		_SysReturnCode = ReturnCodeEnum.valueOf(returnCode);
    }

 	public int clearMessages() {
 		int size = _SysMessages.size();
 	    _SysMessages.clear();
 		return size;
 	}

 	public boolean hasMessages() {
 		return _SysMessages.size() > 0;
 	}

 	public void addMessage(String fieldId, String messageId, MessageSource messageSource) {
 		_SysMessages.put(fieldId,  messageSource.getMessage(messageId, null, null, null));
 	}

    /**
     * Defined for serialization, DOT NOT USE DIRECTLY.
     * Use clearMessages(), hasMessages() and addMessage()
     */
    @Deprecated
 	public Map<String, String> get_SysMessages() {
		return _SysMessages;
	}

 	@Deprecated
 	public void set_SysMessages(Map<String, String> _SysMessages) {
		this._SysMessages = _SysMessages;
	}

  	public void clearNotification() {
 		_SysNotification = "";
 	}

 	public void addNotification(String notificationMessage, MessageSource messageSource) {
 		if(messageSource!=null)
 		_SysNotification = messageSource.getMessage(notificationMessage, null, null, null);
 		else _SysNotification =notificationMessage;
 	}

    /**
     * Defined for serialization, DOT NOT USE DIRECTLY.
     * Use clearNotification(), addNotification()
     */
    @Deprecated
 	public String get_SysNotification() {
		return _SysNotification;
	}
    
    @Deprecated
 	public void set_SysNotification(String _SysNotification) {
		this._SysNotification = _SysNotification;
	}      
 }
