package au.awh.service.data;

import org.springframework.data.domain.Page;

import com.freschelegacy.utils.RestResponsePage;
import au.awh.model.RecordSelectedEnum;
import au.awh.service.data.BaseDTO;

/**
 * Generic Dto for SELRCD
 */
public class SelectRecordDTO<T> extends BaseDTO {
	private static final long serialVersionUID = 4359661418457023962L;

	private RestResponsePage<T> pageGdo;
    private int page = 0;
    private int size = 10;
    private String sortData = "";
    private RecordSelectedEnum recordSelect = null;
    private String nextScreen="";	
	private boolean modalScreen=false;
	public SelectRecordDTO() {
	}

	public void setPageGdo(RestResponsePage<T> pageGdo) {
		this.pageGdo = pageGdo;
	}

	public RestResponsePage<T> getPageGdo() {
		return pageGdo;
	}

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getSortData()
    {
        return sortData;
    }

    public void setSortData(String sortData)
    {
        this.sortData = sortData;
    }

    public RecordSelectedEnum getRecordSelect()
    {
        return recordSelect;
    }

    public void setRecordSelect(RecordSelectedEnum recordSelect)
    {
        this.recordSelect = recordSelect;
    }

	public String getNextScreen() {
		return nextScreen;
	}

	public void setNextScreen(String nextScreen) {
		this.nextScreen = nextScreen;
	}

	public boolean isModalScreen() {
		return modalScreen;
	}

	public void setModalScreen(boolean modalScreen) {
		this.modalScreen = modalScreen;
	}
}