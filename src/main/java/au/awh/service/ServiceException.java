package au.awh.service;

/**
 * @author martin.paquin
 *
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	public ServiceException() {
	}

	/**
	 * @param message exception message
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * @param cause Throwable object
	 */
	public ServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message exception message
	 * @param cause Throwable object
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message exception message
	 * @param cause Throwable object
	 * @param enableSuppression Boolean object
	 * @param writableStackTrace Boolean object
	 */
	public ServiceException(String message, Throwable cause,
		boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
