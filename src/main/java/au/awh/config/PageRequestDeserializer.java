package au.awh.config;

import java.io.IOException;

import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.Sort;

/**
 * Defines a Custom Deserializer to deserialize a PageRequest object with Jackson.
 *
 */
final public class PageRequestDeserializer extends JsonDeserializer<PageRequest> {

//input example:    {\"sort\":{\"unsorted\":true,\"sorted\":false},"
//                  + " \"offset\":0,\"pageSize\":1,\"pageNumber\":0,\"paged\":true,\"unpaged\":false}
	@Override
	public PageRequest deserialize(JsonParser parser,
		DeserializationContext context) throws IOException {
	    ObjectCodec oc = parser.getCodec();
        JsonNode node = oc.readTree(parser);
        int page = node.get("pageNumber").intValue();
        int size = node.get("pageSize").intValue();
        JsonNode sortNode = node.get("sort");
        if(sortNode.get("sorted").asBoolean()) {
            throw new IOException("Deserialization of Sort not done");
        }
        Sort sort = Sort.unsorted();

        return PageRequest.of(page,size,sort);
	}
}
