package au.awh.config;

import au.awh.web.RequestFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import org.springframework.core.env.Environment;

import org.springframework.web.bind.annotation.RestController;

// Marks this class as configuration
@Configuration
// Specifies which package to scan
@ComponentScan(
    basePackages = "au.awh", excludeFilters = {
        @ComponentScan.Filter(
            value=Configuration.class, type=FilterType.ANNOTATION
        ),
        @ComponentScan.Filter(
            value=RestController.class, type=FilterType.ANNOTATION
        ),
        @ComponentScan.Filter(
            value=RequestFilter.class, type=FilterType.ASSIGNABLE_TYPE
        )
    }
)

@EnableSpringConfigured
public class SpringConfig {

    @Autowired
    private Environment env;

    @Bean(name="messageSource")
    public ReloadableResourceBundleMessageSource getMessageSource() {
        ReloadableResourceBundleMessageSource resource =
            new ReloadableResourceBundleMessageSource();
        resource.setBasename("classpath:messages");
        resource.setDefaultEncoding("UTF-8");

        return resource;
    }

}
