package au.awh.config;

import java.io.IOException;

import java.time.LocalTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Defines a Custom Deserializer to deserialize a LocalTime object with Jackson.
 *
 * @author Amit Arya
 * @since (2016-07-14.11:31:32)
 */
final public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {

    @Override
    public LocalTime deserialize(JsonParser parser,
        DeserializationContext context) throws IOException {
        return LocalTime.parse(parser.getText());
    }
}
