package au.awh.config;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import org.springframework.core.env.Environment;

import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration.JedisClientConfigurationBuilder;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;

/**
 * Configuration of Map based store to be used for session data storage.
 *
 * @author Robin Rizvi
 * @since (2015-11-20.15:57:12)
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds=3600)
@PropertySource({ "classpath:sessionstorage.properties" })
public class SessionStorageConfig {

    @Autowired
    Environment env;

    /**
     * ConnectionFactory bean that connects Spring Session to the Redis Server.
     *
     * @return configured RedisConnectionFactory
     */
    @Bean
    @Profile("dev")
    public JedisConnectionFactory connectionFactory() {
        String hostName = env.getProperty("storage.hostname");
        String password = env.getProperty("storage.password");
        Integer port = Integer.parseInt(env.getProperty("storage.port"));
        Integer database = Integer.parseInt(env.getProperty("storage.index"));
        Integer timeout = Integer.parseInt(env.getProperty("storage.timeout"));
        
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(hostName, port);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(password));
        redisStandaloneConfiguration.setDatabase(database);

        JedisClientConfigurationBuilder  jedisClientConfiguration = JedisClientConfiguration.builder();
        if(timeout > 0) {
            jedisClientConfiguration.connectTimeout(Duration.ofMillis(timeout));
        }
                
        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration.build());
    }

    /**
     * HttpSessionStrategy bean that customizes Spring Session's HttpSession
     * integration, to use HTTP headers to convey the current session
     * information instead of cookies.
     *
     * @return configured HeaderHttpSessionStrategy
     */
    @Bean
    public RedisHttpSessionConfiguration httpSessionStrategy() {
        return new RedisHttpSessionConfiguration();
    }

    /**
     * @see SessionStorageConfig#connectionFactory()
     * @return configured RedisConnectionFactory
     */
    @Bean
    @Profile("prod")
    public JedisConnectionFactory prodConnectionFactory() {
        String hostName = env.getProperty("prod.storage.hostname");
        String password = env.getProperty("prod.storage.password");
        Integer port = Integer.parseInt(env.getProperty("prod.storage.port"));
        Integer database = Integer.parseInt(env.getProperty("prod.storage.index"));
        Integer timeout = Integer.parseInt(env.getProperty("prod.storage.timeout"));
        
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(hostName, port);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(password));
        redisStandaloneConfiguration.setDatabase(database);

        JedisClientConfigurationBuilder  jedisClientConfiguration = JedisClientConfiguration.builder();
        if(timeout > 0) {
            jedisClientConfiguration.connectTimeout(Duration.ofMillis(timeout));
        }
                
        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration.build());
    }

    /**
     * @see SessionStorageConfig#connectionFactory()
     * @return configured RedisConnectionFactory
     */
    @Bean
    @Profile("test")
    public JedisConnectionFactory testConnectionFactory() {
        String hostName = env.getProperty("test.storage.hostname");
        String password = env.getProperty("test.storage.password");
        Integer port = Integer.parseInt(env.getProperty("test.storage.port"));
        Integer database = Integer.parseInt(env.getProperty("test.storage.index"));
        Integer timeout = Integer.parseInt(env.getProperty("test.storage.timeout"));
        
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(hostName, port);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(password));
        redisStandaloneConfiguration.setDatabase(database);

        JedisClientConfigurationBuilder  jedisClientConfiguration = JedisClientConfiguration.builder();
        if(timeout > 0) {
            jedisClientConfiguration.connectTimeout(Duration.ofMillis(timeout));
        }
                
        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration.build());
    }
 }
