package au.awh.config;

import java.lang.management.ManagementFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.management.JMException;
import javax.management.ObjectName;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import org.springframework.core.env.Environment;

import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

import org.springframework.jmx.export.MBeanExporter;

import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Enable and scan Spring Data repositories.
 */
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:datasource.properties", "classpath:infrastructure.properties" })
public class InfrastructureConfig {
    @Autowired
    private DataSource dataSource;

    // Get the property values
    @Autowired
    Environment env;

    // Spring factory bean to set up Spring with JPA
    private LocalContainerEntityManagerFactoryBean em;

    @Bean(name="transactionManager")
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(em.getObject());

        return transactionManager;
    }

    @Bean(name="entityManagerFactory")
    @Profile("dev")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        if (em == null) {
            em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource);
            em.setPackagesToScan(new String[] { "au.awh.file", "au.awh.model" });

            JpaVendorAdapter vendorAdapter = jpaVendorAdapter();
            em.setJpaVendorAdapter(vendorAdapter);

            Properties properties = new Properties();
            properties.put("hibernate.validator.apply_to_ddl",
                env.getProperty("hibernate.validator.apply_to_ddl"));
            properties.put("hibernate.validator.autoregister_listeners",
                env.getProperty("hibernate.validator.autoregister_listeners"));
            properties.put("hibernate.dialect",
                env.getProperty("hibernate.dialect"));
            properties.put("hibernate.show_sql",
                env.getProperty("hibernate.show_sql"));
            properties.put("hibernate.format_sql",
                env.getProperty("hibernate.format_sql"));
            properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("hibernate.hbm2ddl.auto"));
            properties.put("hibernate.generate_statistics",
                env.getProperty("hibernate.generate_statistics"));

            em.setJpaProperties(properties);
        }

        return em;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Configuration of MBeanExporter used for registering MBeans.
     *
     * @return MBean exporter for exposing beans through JMX
     */
    @Bean
    @Profile({"dev", "test"})
    public MBeanExporter jmxExporter() {
        MBeanExporter mBeanExporter = new MBeanExporter();
        Map<String, Object> beans = new HashMap<>();

        try {
            ManagementFactory.getPlatformMBeanServer().getMBeanInfo(
                new ObjectName("Persistence:type=Statistics"));
        } catch (JMException e) {
            // Register a MBean if its not already registered
            beans.put("Persistence:type=Statistics", "PersistenceStatistics");
        }

        // Beans that will be exported
        mBeanExporter.setBeans(beans);

        return mBeanExporter;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter vendorAdapter =
            new HibernateJpaVendorAdapter();

        return vendorAdapter;
    }

    @Bean(name="entityManagerFactory")
    @Profile("prod")
    public LocalContainerEntityManagerFactoryBean prodEntityManagerFactory() {
        if (em == null) {
            em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource);
            em.setPackagesToScan(new String[] { "au.awh.file", "au.awh.model" });

            JpaVendorAdapter vendorAdapter = jpaVendorAdapter();
            em.setJpaVendorAdapter(vendorAdapter);

            Properties properties = new Properties();
            properties.put("hibernate.validator.apply_to_ddl",
                env.getProperty("prod.hibernate.validator.apply_to_ddl"));
            properties.put("hibernate.validator.autoregister_listeners",
                env.getProperty("prod.hibernate.validator.autoregister_listeners"));
            properties.put("hibernate.dialect",
                env.getProperty("prod.hibernate.dialect"));
            properties.put("hibernate.show_sql",
                env.getProperty("prod.hibernate.show_sql"));
            properties.put("hibernate.format_sql",
                env.getProperty("prod.hibernate.format_sql"));
            properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("prod.hibernate.hbm2ddl.auto"));
            properties.put("hibernate.generate_statistics",
                env.getProperty("prod.hibernate.generate_statistics"));

            em.setJpaProperties(properties);
        }

        return em;
    }

    @Bean(name="entityManagerFactory")
    @Profile("test")
    public LocalContainerEntityManagerFactoryBean testEntityManagerFactory() {
        if (em == null) {
            em = new LocalContainerEntityManagerFactoryBean();
            em.setDataSource(dataSource);
            em.setPackagesToScan(new String[] { "au.awh.file", "au.awh.model" });

            JpaVendorAdapter vendorAdapter = jpaVendorAdapter();
            em.setJpaVendorAdapter(vendorAdapter);

            Properties properties = new Properties();
            properties.put("hibernate.validator.apply_to_ddl",
                env.getProperty("test.hibernate.validator.apply_to_ddl"));
            properties.put("hibernate.validator.autoregister_listeners",
                env.getProperty("test.hibernate.validator.autoregister_listeners"));
            properties.put("hibernate.dialect",
                env.getProperty("test.hibernate.dialect"));
            properties.put("hibernate.show_sql",
                env.getProperty("test.hibernate.show_sql"));
            properties.put("hibernate.format_sql",
                env.getProperty("test.hibernate.format_sql"));
            properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("test.hibernate.hbm2ddl.auto"));
            properties.put("hibernate.generate_statistics",
                env.getProperty("test.hibernate.generate_statistics"));

            em.setJpaProperties(properties);
        }

        return em;
    }
}
