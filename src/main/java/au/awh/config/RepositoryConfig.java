package au.awh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Enable and scan Spring Data repositories.
 *
 */
@Configuration
@EnableJpaRepositories("au.awh.file")
public class RepositoryConfig {
    //
}
