package au.awh.config;

import java.io.IOException;

import java.time.LocalTime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Defines a Custom Serializer to serialize a LocalTime object with Jackson.
 *
 * @author Amit Arya
 * @since (2016-07-14.11:29:32)
 */
final public class LocalTimeSerializer extends JsonSerializer<LocalTime> {

    @Override
    public void serialize(LocalTime value, JsonGenerator gen,
        SerializerProvider provider) throws IOException {
        gen.writeString(value.toString());
    }
}
