package au.awh.config;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import com.fasterxml.jackson.databind.SerializationFeature;

import cz.jirutka.spring.exhandler.support.HttpMessageConverterUtils;


@Configuration
@PropertySource({"classpath:swagger.properties"})
public class WebMvcConfig implements WebMvcConfigurer {
    private static final int CACHE_PERIOD = 31556926; // one year
    @Autowired
    Environment env;

    @Override
    public void addFormatters(FormatterRegistry formatterRegistry) {
        // Add your custom formatters and/or converters
        formatterRegistry.addConverter(new LocalDateConverter());
        formatterRegistry.addConverter(new LocalTimeConverter());
        // Add converter for handling primitive type conversions and other
        // conversions for which a converter is not explicitly registered
        formatterRegistry.addConverter(new ParameterConverter());
        // Remove registered converters for primitive types since they will be
        // handled by the ParameterConverter
        formatterRegistry.removeConvertible(String.class, Number.class);
        formatterRegistry.removeConvertible(String.class, Boolean.class);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // Static resources from both WEB-INF and webjars
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("/resources/")
                .setCachePeriod(CACHE_PERIOD);
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/")
                .setCachePeriod(CACHE_PERIOD);
        registry.addResourceHandler("/index.html")
                .addResourceLocations("/index.html");
        registry.addResourceHandler("/assets/**")
                .addResourceLocations("/assets/");
        registry.addResourceHandler("/libraries/**")
                .addResourceLocations("/libraries/");
        registry.addResourceHandler("/controllers/**")
                .addResourceLocations("/controllers/");
        registry.addResourceHandler("/directives/**")
                .addResourceLocations("/directives/");
        registry.addResourceHandler("/services/**")
                .addResourceLocations("/services/");
        registry.addResourceHandler("/views/**")
                .addResourceLocations("/views/");
        registry.addResourceHandler("/app.js").addResourceLocations("/app.js");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
    }


    @Override
    public void configureDefaultServletHandling(
        DefaultServletHandlerConfigurer configurer) {
        // Serving static files using the Servlet container's default Servlet
        configurer.enable();
    }

    @Override
    public void configureHandlerExceptionResolvers(
        List<HandlerExceptionResolver> resolvers) {
        resolvers.add(exceptionHandlerExceptionResolver()); // resolves @ExceptionHandler
//        resolvers.add(restExceptionResolver());
	}

    @Override
    public void configureMessageConverters(
        List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.dateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
        builder.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        builder.serializerByType(LocalDate.class, new LocalDateSerializer());
        builder.deserializerByType(LocalDate.class, new LocalDateDeserializer());

        builder.serializerByType(LocalTime.class, new LocalTimeSerializer());
        builder.deserializerByType(LocalTime.class, new LocalTimeDeserializer());

        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
        converters.add(new ByteArrayHttpMessageConverter());
    }

    @Bean
    public ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver() {
        ExceptionHandlerExceptionResolver resolver =
            new ExceptionHandlerExceptionResolver();
        resolver.setMessageConverters(HttpMessageConverterUtils.getDefaultHttpMessageConverters());

        return resolver;
    }

    @Bean
    public MessageSource httpErrorMessageSource() {
        ReloadableResourceBundleMessageSource m =
            new ReloadableResourceBundleMessageSource();
        m.setBasename("classpath:/org/example/messages");
        m.setDefaultEncoding("UTF-8");

        return m;
    }

    @Bean(name="messageSource")
    public ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource() {
        ReloadableResourceBundleMessageSource messageSource =
            new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:au.awh/messages");
        messageSource.setDefaultEncoding("UTF-8");

        return messageSource;
    }
 
//    TODO: replace
//    @Bean
//    public RestHandlerExceptionResolver restExceptionResolver() {
//        return RestHandlerExceptionResolver.builder()
//                                           .messageSource(httpErrorMessageSource())
//                                           .defaultContentType(MediaType.APPLICATION_JSON)
//                                           .addErrorMessageHandler(EmptyResultDataAccessException.class,
//            HttpStatus.NOT_FOUND).build();
//    }

//    Not necessary?
//    @Bean
//    public ViewResolver viewResolver() {
//        // Example: the 'info' view logical name is mapped to the file '/WEB-INF/jsp/info.jsp'
//        InternalResourceViewResolver bean = new InternalResourceViewResolver();
//        bean.setPrefix("/views");
//        bean.setSuffix(".html");
//
//        return bean;
//    }
}
