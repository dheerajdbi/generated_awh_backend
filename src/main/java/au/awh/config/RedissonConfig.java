package au.awh.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class RedissonConfig {

    public static final String ENV_REDISSON_URL = "redisson.url";

    @Bean
    public Config getRedissonConfig(Environment env) {
        Config config = new Config();
        config.useSingleServer().setAddress(env.getRequiredProperty(ENV_REDISSON_URL));
        return config;
    }

    @Bean
    public RedissonClient redissonClient(Config config) {
        return Redisson.create(config);
    }
}
