package au.awh.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * Session storage initializer that ensures that session filters are registered
 * for each request.
 *
 * @author Robin Rizvi
 * @since (2015-11-21.14:57:12)
 */
public class CrmSessionStorageInitializer
	extends AbstractHttpSessionApplicationInitializer {}
