package au.awh.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import org.springframework.core.env.Environment;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Enable and scan Spring Data repositories.
 */
@Configuration
@PropertySource({ "classpath:datasource.properties" })
public class DataSourceConfig {
    @Autowired
    private Environment env;

    @Bean
    @Profile("dev")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("driverClass"));
        dataSource.setUrl(env.getProperty("url"));
        dataSource.setUsername(env.getProperty("db.user"));
        dataSource.setPassword(env.getProperty("db.password"));

        return dataSource;
    }

    @Bean
    @Profile("prod")
    public DataSource prodDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("prod.driverClass"));
        dataSource.setUrl(env.getProperty("prod.url"));
        dataSource.setUsername(env.getProperty("prod.db.user"));
        dataSource.setPassword(env.getProperty("prod.db.password"));

        return dataSource;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    @Profile("test")
    public DataSource testDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("test.driverClass"));
        dataSource.setUrl(env.getProperty("test.url"));
        dataSource.setUsername(env.getProperty("test.db.user"));
        dataSource.setPassword(env.getProperty("test.db.password"));

        return dataSource;
    }
}
