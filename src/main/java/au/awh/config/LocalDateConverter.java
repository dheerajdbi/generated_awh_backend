package au.awh.config;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;

/**
 * Converts String to LocalDate for Spring Type Conversion.
 *
 * @author Amit Arya
 * @since (2016-07-13.13:03:22)
 */
final public class LocalDateConverter implements Converter<String, LocalDate> {

    private static final DateTimeFormatter formatter =
        DateTimeFormatter.ISO_LOCAL_DATE;

    public LocalDate convert(String source) {
        return LocalDate.parse(source, formatter);
    }
}
