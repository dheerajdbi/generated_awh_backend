package au.awh.config;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Defaults;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;

/**
 * Handles type conversion for controller's handler parameters whenever a
 * converter is not explicitly registered for a type.
 *
 * @author Robin Rizvi
 * @since (2016-01-27.13:24:12)
 */
public final class ParameterConverter implements GenericConverter {
    @Override
    public Object convert(Object source, TypeDescriptor sourceType,
        TypeDescriptor targetType) {
        // If parameter is null, return default value for primitives or null for
        // others
        if ((source == null) || "null".equals(source)) {
            if (targetType.isPrimitive()) {
                return Defaults.defaultValue(targetType.getType());
            }

            return null;
        }

        // Since built in convertibles for primitives have been unregistered,
        // parse string values to appropriate types when the value is not null
        if (targetType.isPrimitive()) {
            if (targetType.isAssignableTo(TypeDescriptor.valueOf(Long.class))) {
                source = source.toString().replaceFirst("L$", "");
            }

            PropertyEditor editor =
                PropertyEditorManager.findEditor(targetType.getType());
            editor.setAsText((String)source);
            source = editor.getValue();
        }

        return source;
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        Set<ConvertiblePair> convertiblePairs = new HashSet<ConvertiblePair>();
        convertiblePairs.add(new ConvertiblePair(String.class, Object.class));

        return convertiblePairs;
    }
}
