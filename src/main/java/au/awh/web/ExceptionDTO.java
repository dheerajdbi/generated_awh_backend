package au.awh.web;

import java.util.HashMap;
import java.util.Map;

/**
 * Transfer object for transferring exception information to the clients.
 *
 * @author Robin Rizvi
 * @since (2015-11-04.15:03:12)
 */
public class ExceptionDTO {
	private Map<String, String> fieldErrors;
	private String code;
	private String message;

	public ExceptionDTO() {
		this.code = "";
		this.message = "";
		this.fieldErrors = new HashMap<String, String>();
	}

	/**
	 * Gets exception code.
	 *
	 * @return exception code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets map of fields and their errors.
	 *
	 * @return fields' errors
	 */
	public Map<String, String> getFieldErrors() {
		return fieldErrors;
	}

	/**
	 * Gets exception message.
	 *
	 * @return exception message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets exception code.
	 *
	 * @param code exception code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Sets error corresponding to a field.
	 *
	 * @param fieldName name of field
	 * @param fieldErrorMessage error message of field
	 */
	public void setFieldError(String fieldName, String fieldErrorMessage) {
		this.fieldErrors.put(fieldName, fieldErrorMessage);
	}

	/**
	 * Sets map of fields and their errors.
	 *
	 * @param fieldErrors map of fields and their errors
	 */
	public void setFieldErrors(Map<String, String> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	/**
	 * Sets exception message.
	 *
	 * @param message exception message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
