package au.awh.web;

import java.io.IOException;

import java.net.URLDecoder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.freschelegacy.utils.ReportUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Filters HTTP requests and performs required conversions on the request.
 *
 * @author Robin Rizvi
 * @since (2016-01-04.15:03:12)
 */
@Component(value="requestFilter")
public class RequestFilter extends GenericFilterBean {

	@Autowired
	RequestMappingHandlerMapping handlerMapping;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		String requestURI = req.getRequestURI();

		// Set web root path
		ReportUtils.setWebRootPath(req.getSession().getServletContext()
				  					  .getRealPath(""));

		// Handle empty path parameters
		if (!requestURI.endsWith(req.getContextPath() + "/") &&
				(requestURI.contains("//") || requestURI.endsWith("/"))) {
			boolean urlHandlerFound = false;

			try {
				urlHandlerFound = (handlerMapping.getHandler(req) != null);
			} catch (Exception e) {
			}

			// Check if a path match is found on ignoring trailing null path
			// parameters
			if (!requestURI.matches(".*//[^/].*") && urlHandlerFound) {
				chain.doFilter(request, response);
			} else {
				// Append Uri with a trailing slash
				requestURI += "/";
				requestURI = requestURI.replace(req.getContextPath(), "");

				// Replace empty path parameters with null so that Spring
				// dispatcher can map the request
				requestURI = requestURI.replaceAll("/(?=/)", "/null");

				// Decode request url string since request url is not auto
				// decoded when server side redirect is performed
				requestURI = URLDecoder.decode(requestURI, "UTF-8");

				req.getRequestDispatcher(requestURI).forward(req, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}
}
