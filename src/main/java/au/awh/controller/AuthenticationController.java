package au.awh.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.awh.service.ServiceException;

/**
 * Controller for authentication details resource.
 *
 * @author Robin Rizvi
 * @since (2015-10-06.16:57:12)
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

	@RequestMapping(value={"/user"}, method=RequestMethod.GET, produces="application/json;charset=utf-8")
	public Principal login(Principal user) throws ServiceException {
		return user;
	}
}
