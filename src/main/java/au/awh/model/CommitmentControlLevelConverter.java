package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for CommitmentControlLevelEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class CommitmentControlLevelConverter implements AttributeConverter<CommitmentControlLevelEnum, String> {

	@Override
	public String convertToDatabaseColumn(CommitmentControlLevelEnum commitmentControlLevel) {
		if (commitmentControlLevel == null) {
			return "";
		}

		return commitmentControlLevel.getCode();
	}

	@Override
	public CommitmentControlLevelEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return CommitmentControlLevelEnum.fromCode("");
		}

		return CommitmentControlLevelEnum.fromCode(StringUtils.strip(dbData));
	}
}
