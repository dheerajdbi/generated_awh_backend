package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for EquipmentStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class EquipmentStatusConverter implements AttributeConverter<EquipmentStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(EquipmentStatusEnum equipmentStatus) {
		if (equipmentStatus == null) {
			return "";
		}

		return equipmentStatus.getCode();
	}

	@Override
	public EquipmentStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return EquipmentStatusEnum.fromCode("");
		}

		return EquipmentStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
