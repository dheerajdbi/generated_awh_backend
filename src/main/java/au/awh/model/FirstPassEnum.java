package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for FirstPass.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum FirstPassEnum {  // 1555 STS FSTPASU First Pass

	_NOT_FIRST_PASS("N", "Not First Pass") /* 19369 */ ,
	_FIRST_PASS("Y", "First Pass") /* 19367 */ ;
    
	private final String code;
	private final String description;

	FirstPassEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static FirstPassEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<FirstPassEnum> getAllValues() { /* 19368 *ALL values */
        return Arrays.asList(FirstPassEnum._FIRST_PASS, FirstPassEnum._NOT_FIRST_PASS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(FirstPassEnum firstPass) {  /* 19368 *ALL values */
        return FirstPassEnum.getAllValues().contains(firstPass);
    }
}