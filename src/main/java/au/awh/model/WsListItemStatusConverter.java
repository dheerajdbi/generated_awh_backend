package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for WsListItemStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class WsListItemStatusConverter implements AttributeConverter<WsListItemStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(WsListItemStatusEnum wsListItemStatus) {
		if (wsListItemStatus == null) {
			return "";
		}

		return wsListItemStatus.getCode();
	}

	@Override
	public WsListItemStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return WsListItemStatusEnum.fromCode("");
		}

		return WsListItemStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
