package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReadingType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReadingTypeEnum {  // 13980 STS RDTYPE Reading Type

	_NOT_ENTERED("", "Not entered") /* 58722 */ ,
	_MAKE_AVAILABLE("AV", "Make Available") /* 64198 */ ,
	_INITIAL_READING("IL", "Initial reading") /* 58668 */ ,
	_METER_RESET("MR", "Meter Reset") /* 64195 */ ,
	_ANY("NE", "Any") /* 61200 */ ,
	_RETIREMENT("RT", "Retirement") /* 58669 */ ,
	_STANDARD_READING("ST", "Standard Reading") /* 58603 */ ,
	_SERVICE_READING("SV", "Service Reading") /* 58605 */ ,
	_TRANSFER_IN_READING("TI", "Transfer In Reading") /* 58607 */ ,
	_TRANSFER_OUT("TO", "Transfer Out") /* 58606 */ ,
	_MAKE_UNAVAILABLE("UA", "Make UnAvailable") /* 64197 */ ;
    
	private final String code;
	private final String description;

	ReadingTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReadingTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReadingTypeEnum> getAllValues() { /* 58604 *ALL values */
        return Arrays.asList(ReadingTypeEnum._ANY, ReadingTypeEnum._INITIAL_READING, ReadingTypeEnum._MAKE_AVAILABLE, ReadingTypeEnum._MAKE_UNAVAILABLE, ReadingTypeEnum._METER_RESET, ReadingTypeEnum._NOT_ENTERED, ReadingTypeEnum._RETIREMENT, ReadingTypeEnum._SERVICE_READING, ReadingTypeEnum._STANDARD_READING, ReadingTypeEnum._TRANSFER_IN_READING, ReadingTypeEnum._TRANSFER_OUT);
    }

    public static List<ReadingTypeEnum> getEndOfPeriodReadings() { /* 58773 End of Period Readings */
        return Arrays.asList(ReadingTypeEnum._RETIREMENT, ReadingTypeEnum._STANDARD_READING, ReadingTypeEnum._TRANSFER_OUT);
    }

    public static List<ReadingTypeEnum> getPreviousReading() { /* 58679 Previous Reading */
        return Arrays.asList(ReadingTypeEnum._INITIAL_READING, ReadingTypeEnum._STANDARD_READING, ReadingTypeEnum._TRANSFER_IN_READING);
    }

    public static List<ReadingTypeEnum> getTransfer() { /* 58745 Transfer */
        return Arrays.asList(ReadingTypeEnum._TRANSFER_IN_READING, ReadingTypeEnum._TRANSFER_OUT);
    }

    public static List<ReadingTypeEnum> getValidForAddition() { /* 58721 Valid for Addition */
        return Arrays.asList(ReadingTypeEnum._INITIAL_READING, ReadingTypeEnum._MAKE_AVAILABLE, ReadingTypeEnum._MAKE_UNAVAILABLE, ReadingTypeEnum._METER_RESET, ReadingTypeEnum._RETIREMENT, ReadingTypeEnum._SERVICE_READING, ReadingTypeEnum._STANDARD_READING, ReadingTypeEnum._TRANSFER_OUT);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReadingTypeEnum readingType) {  /* 58604 *ALL values */
        return ReadingTypeEnum.getAllValues().contains(readingType);
    }

    public static boolean isEndOfPeriodReadings(ReadingTypeEnum readingType) {  /* 58773 End of Period Readings */
        return ReadingTypeEnum.getEndOfPeriodReadings().contains(readingType);
    }

    public static boolean isPreviousReading(ReadingTypeEnum readingType) {  /* 58679 Previous Reading */
        return ReadingTypeEnum.getPreviousReading().contains(readingType);
    }

    public static boolean isTransfer(ReadingTypeEnum readingType) {  /* 58745 Transfer */
        return ReadingTypeEnum.getTransfer().contains(readingType);
    }

    public static boolean isValidForAddition(ReadingTypeEnum readingType) {  /* 58721 Valid for Addition */
        return ReadingTypeEnum.getValidForAddition().contains(readingType);
    }
}