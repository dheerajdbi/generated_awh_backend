package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for WsListFieldId.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum WsListFieldIdEnum {  // 8030 STS ANVN WS List Field Id

	_NOT_ENTERED("", "Not entered") /* 36091 */ ,
	_BUYER_INVOICE_OUTQ("ARVN", "Buyer Invoice OUTQ") /* 35834 */ ,
	_BUYER_INVOICE_OUTQLIB("ASVN", "Buyer Invoice OUTQLIB") /* 35835 */ ,
	_INVOICE_SUMMARY_OUTQ("ATVN", "Invoice Summary OUTQ") /* 35840 */ ,
	_INVOICE_SUMMARY_OUTQLIB("AUVN", "Invoice Summary OUTQLIB") /* 35841 */ ,
	_BROKER("BKRIDK", "Broker") /* 35657 */ ,
	_ENTITY("BKRSTCTR", "Entity") /* 35723 */ ,
	_BUYER("BYRORG", "Buyer") /* 35725 */ ,
	_BUYER_INVOICE_COPIES("C9CD", "Buyer Invoice Copies") /* 35836 */ ,
	_COPIES("CPY", "Copies") /* 36980 */ ,
	_BUYER_INVOICE_HOLD("D0ST", "Buyer Invoice Hold") /* 35838 */ ,
	_BUYER_INVOICE_SAVE("D1ST", "Buyer Invoice Save") /* 35839 */ ,
	_INVOICE_SUMMARY_FORM("D2ST", "Invoice Summary Form") /* 35843 */ ,
	_INVOICE_SUMMARY_HOLD("D3ST", "Invoice Summary Hold") /* 35844 */ ,
	_INVOICE_SUMMARY_SAVE("D4ST", "Invoice Summary Save") /* 35845 */ ,
	_INVOICE_SUMMARY_COPIES("DACD", "Invoice Summary Copies") /* 35842 */ ,
	_DATE_CORED("DTECOR", "Date Cored") /* 37459 */ ,
	_DATE_FROM("DTEFR", "Date From") /* 36970 */ ,
	_DATE_TO("DTETO", "Date To") /* 36971 */ ,
	_SOLD_LOTS_ONLY("DYST", "Sold Lots Only") /* 35848 */ ,
	_BUYER_INVOICE_FORM("DZST", "Buyer Invoice Form") /* 35837 */ ,
	_EXTRACT_BUYER_INVOICE("EXBYRINV", "Extract Buyer Invoice") /* 35792 */ ,
	_EXTRACT_POST_DAS_SALE_CERT("EXPSCERT", "Extract Post-Sale Cert") /* 35794 */ ,
	_FORMS("FRM", "Forms") /* 36981 */ ,
	_EXTRACT_WOOL_LEVY("FWST", "Extract Wool Levy") /* 36972 */ ,
	_PRINT_WOOL_LEVY("FXST", "Print Wool Levy") /* 36973 */ ,
	_PRINT_WOOL_LEVY_SALES("GZST", "Print Wool Levy Sales") /* 37589 */ ,
	_HOLD_LPA_STA_YES_FSL_STA_NO_RPA_("HLDCLFMT", "Hold (*YES/*NO)") /* 36982 */ ,
	_LABORATORY("LABCDEK", "Laboratory") /* 35730 */ ,
	_LOT_NUMBER("LOTNBRK", "Lot Number") /* 35729 */ ,
	_OUTPUT_QUEUE("OUTQ", "Output Queue") /* 36978 */ ,
	_OUTPUT_QUEUE_LIBRARY("OUTQLIB", "Output Queue Library") /* 36979 */ ,
	_PROGRESSIVE_OR_ALL_LPA_BINV_RPA_("PABINV", "Progressive or All (Binv)") /* 52390 */ ,
	_PROGRESSIVE_OR_ALL_LPA_PSC_RPA_("PAPSC", "Progressive or All (Psc)") /* 52391 */ ,
	_PRINT_BUYER_INVOICE("PTBYRINV", "Print Buyer Invoice") /* 35793 */ ,
	_PRINT_INVOICE_SUMMARY("PTINVSUM", "Print Invoice Summary") /* 35795 */ ,
	_SAVE_LPA_STA_YES_FSL_STA_NO_RPA_("SAVCLFMT", "Save (*YES/*NO)") /* 36983 */ ,
	_SALE_SHORT("SL4CHAR", "Sale Short") /* 35727 */ ,
	_SALE_DATE("SLDTE", "Sale Date") /* 35728 */ ,
	_SALE_STORAGE_CENTRE("SNSTGCTK", "Sale Storage Centre") /* 35724 */ ,
	_SALE_SEASON("SSCCYYA", "Sale Season") /* 35726 */ ;
    
	private final String code;
	private final String description;

	WsListFieldIdEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static WsListFieldIdEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<WsListFieldIdEnum> getAllValues() { /* 35658 *ALL values */
        return Arrays.asList(WsListFieldIdEnum._BROKER, WsListFieldIdEnum._BUYER, WsListFieldIdEnum._BUYER_INVOICE_COPIES, WsListFieldIdEnum._BUYER_INVOICE_FORM, WsListFieldIdEnum._BUYER_INVOICE_HOLD, WsListFieldIdEnum._BUYER_INVOICE_OUTQ, WsListFieldIdEnum._BUYER_INVOICE_OUTQLIB, WsListFieldIdEnum._BUYER_INVOICE_SAVE, WsListFieldIdEnum._COPIES, WsListFieldIdEnum._DATE_CORED, WsListFieldIdEnum._DATE_FROM, WsListFieldIdEnum._DATE_TO, WsListFieldIdEnum._ENTITY, WsListFieldIdEnum._EXTRACT_BUYER_INVOICE, WsListFieldIdEnum._EXTRACT_POST_DAS_SALE_CERT, WsListFieldIdEnum._EXTRACT_WOOL_LEVY, WsListFieldIdEnum._FORMS, WsListFieldIdEnum._HOLD_LPA_STA_YES_FSL_STA_NO_RPA_, WsListFieldIdEnum._INVOICE_SUMMARY_COPIES, WsListFieldIdEnum._INVOICE_SUMMARY_FORM, WsListFieldIdEnum._INVOICE_SUMMARY_HOLD, WsListFieldIdEnum._INVOICE_SUMMARY_OUTQ, WsListFieldIdEnum._INVOICE_SUMMARY_OUTQLIB, WsListFieldIdEnum._INVOICE_SUMMARY_SAVE, WsListFieldIdEnum._LABORATORY, WsListFieldIdEnum._LOT_NUMBER, WsListFieldIdEnum._NOT_ENTERED, WsListFieldIdEnum._OUTPUT_QUEUE, WsListFieldIdEnum._OUTPUT_QUEUE_LIBRARY, WsListFieldIdEnum._PRINT_BUYER_INVOICE, WsListFieldIdEnum._PRINT_INVOICE_SUMMARY, WsListFieldIdEnum._PRINT_WOOL_LEVY, WsListFieldIdEnum._PRINT_WOOL_LEVY_SALES, WsListFieldIdEnum._PROGRESSIVE_OR_ALL_LPA_BINV_RPA_, WsListFieldIdEnum._PROGRESSIVE_OR_ALL_LPA_PSC_RPA_, WsListFieldIdEnum._SALE_DATE, WsListFieldIdEnum._SALE_SEASON, WsListFieldIdEnum._SALE_SHORT, WsListFieldIdEnum._SALE_STORAGE_CENTRE, WsListFieldIdEnum._SAVE_LPA_STA_YES_FSL_STA_NO_RPA_, WsListFieldIdEnum._SOLD_LOTS_ONLY);
    }

    /**
     * Check
     */

    public static boolean isAllValues(WsListFieldIdEnum wsListFieldId) {  /* 35658 *ALL values */
        return WsListFieldIdEnum.getAllValues().contains(wsListFieldId);
    }
}