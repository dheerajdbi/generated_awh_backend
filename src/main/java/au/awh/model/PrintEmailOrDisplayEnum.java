package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for PrintEmailOrDisplay.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum PrintEmailOrDisplayEnum {  // 15113 STS PREMDSP Print, Email or Display

	_DISPLAY("D", "Display") /* 64239 */ ,
	_EMAIL("E", "Email") /* 64238 */ ,
	_PRINT("P", "Print") /* 64237 */ ;
    
	private final String code;
	private final String description;

	PrintEmailOrDisplayEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static PrintEmailOrDisplayEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<PrintEmailOrDisplayEnum> getAllValues() { /* 64240 *All Values */
        return Arrays.asList(PrintEmailOrDisplayEnum._DISPLAY, PrintEmailOrDisplayEnum._EMAIL, PrintEmailOrDisplayEnum._PRINT);
    }

    public static List<PrintEmailOrDisplayEnum> getPrintOrEmail() { /* 64241 Print or Email */
        return Arrays.asList(PrintEmailOrDisplayEnum._EMAIL, PrintEmailOrDisplayEnum._PRINT);
    }

    /**
     * Check
     */

    public static boolean isAllValues(PrintEmailOrDisplayEnum printEmailOrDisplay) {  /* 64240 *All Values */
        return PrintEmailOrDisplayEnum.getAllValues().contains(printEmailOrDisplay);
    }

    public static boolean isPrintOrEmail(PrintEmailOrDisplayEnum printEmailOrDisplay) {  /* 64241 Print or Email */
        return PrintEmailOrDisplayEnum.getPrintOrEmail().contains(printEmailOrDisplay);
    }
}