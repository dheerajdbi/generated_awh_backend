package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for CheckRecordOption.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CheckRecordOptionEnum {  // 8140 STS EJST Check Record Option

	_NORMAL("", "Normal") /* 35958 */ ,
	_EXACT_RECORD_EXISTS("1", "Exact record exists") /* 45036 */ ,
	_EXACT_RCD_DOES_NOT_EXIST("2", "Exact rcd does not exist") /* 45035 */ ,
	_RECORD_DOES_NOT_EXIST("N", "Record does not exist") /* 35960 */ ,
	_UNCOMPLETE_ONLY("U", "Uncomplete Only") /* 63606 */ ;
    
	private final String code;
	private final String description;

	CheckRecordOptionEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CheckRecordOptionEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CheckRecordOptionEnum> getAllValues() { /* 35959 *ALL values */
        return Arrays.asList(CheckRecordOptionEnum._EXACT_RCD_DOES_NOT_EXIST, CheckRecordOptionEnum._EXACT_RECORD_EXISTS, CheckRecordOptionEnum._NORMAL, CheckRecordOptionEnum._RECORD_DOES_NOT_EXIST, CheckRecordOptionEnum._UNCOMPLETE_ONLY);
    }

    public static List<CheckRecordOptionEnum> getDoesNotExist() { /* 45037 Does not exist */
        return Arrays.asList(CheckRecordOptionEnum._EXACT_RCD_DOES_NOT_EXIST, CheckRecordOptionEnum._RECORD_DOES_NOT_EXIST);
    }

    public static List<CheckRecordOptionEnum> getExactRecord() { /* 45038 Exact record */
        return Arrays.asList(CheckRecordOptionEnum._EXACT_RCD_DOES_NOT_EXIST, CheckRecordOptionEnum._EXACT_RECORD_EXISTS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(CheckRecordOptionEnum checkRecordOption) {  /* 35959 *ALL values */
        return CheckRecordOptionEnum.getAllValues().contains(checkRecordOption);
    }

    public static boolean isDoesNotExist(CheckRecordOptionEnum checkRecordOption) {  /* 45037 Does not exist */
        return CheckRecordOptionEnum.getDoesNotExist().contains(checkRecordOption);
    }

    public static boolean isExactRecord(CheckRecordOptionEnum checkRecordOption) {  /* 45038 Exact record */
        return CheckRecordOptionEnum.getExactRecord().contains(checkRecordOption);
    }
}