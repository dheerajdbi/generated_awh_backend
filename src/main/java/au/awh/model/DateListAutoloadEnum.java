package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DateListAutoload.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DateListAutoloadEnum {  // 190 STS DAU *Date list autoload

	_STA_NO("N", "*NO") /* 16056 */ ,
	_STA_YES("Y", "*YES") /* 16054 */ ;
    
	private final String code;
	private final String description;

	DateListAutoloadEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DateListAutoloadEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DateListAutoloadEnum> getAllValues() { /* 16055 *ALL values */
        return Arrays.asList(DateListAutoloadEnum._STA_NO, DateListAutoloadEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DateListAutoloadEnum dateListAutoload) {  /* 16055 *ALL values */
        return DateListAutoloadEnum.getAllValues().contains(dateListAutoload);
    }
}