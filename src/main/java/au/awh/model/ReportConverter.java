package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReportEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReportConverter implements AttributeConverter<ReportEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReportEnum report) {
		if (report == null) {
			return "";
		}

		return report.getCode();
	}

	@Override
	public ReportEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReportEnum.fromCode("");
		}

		return ReportEnum.fromCode(StringUtils.strip(dbData));
	}
}
