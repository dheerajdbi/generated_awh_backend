package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for CommitmentControlLevel.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CommitmentControlLevelEnum {  // 9070 STS CMTCTLLV Commitment Control Level

	_MASTER("M", "Master") /* 39462 */ ,
	_NONE("N", "None") /* 39460 */ ,
	_SLAVE("S", "Slave") /* 39463 */ ;
    
	private final String code;
	private final String description;

	CommitmentControlLevelEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CommitmentControlLevelEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CommitmentControlLevelEnum> getAllValues() { /* 39461 *ALL values */
        return Arrays.asList(CommitmentControlLevelEnum._MASTER, CommitmentControlLevelEnum._NONE, CommitmentControlLevelEnum._SLAVE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(CommitmentControlLevelEnum commitmentControlLevel) {  /* 39461 *ALL values */
        return CommitmentControlLevelEnum.getAllValues().contains(commitmentControlLevel);
    }
}