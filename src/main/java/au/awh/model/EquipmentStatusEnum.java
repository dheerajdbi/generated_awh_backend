package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for EquipmentStatus.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum EquipmentStatusEnum {  // 14005 STS EQTRS Equipment Status

	_NOT_ENTERED("", "Not Entered") /* 64136 */ ,
	_ACTIVE("AT", "Active") /* 58730 */ ,
	_RETIRED("RT", "Retired") /* 58732 */ ,
	_TRANSFERED("TR", "Transfered") /* 58733 */ ,
	_UNAVAILABLE("UA", "Unavailable") /* 64196 */ ;
    
	private final String code;
	private final String description;

	EquipmentStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static EquipmentStatusEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<EquipmentStatusEnum> getAllValues() { /* 58731 *ALL values */
        return Arrays.asList(EquipmentStatusEnum._ACTIVE, EquipmentStatusEnum._NOT_ENTERED, EquipmentStatusEnum._RETIRED, EquipmentStatusEnum._TRANSFERED, EquipmentStatusEnum._UNAVAILABLE);
    }

    public static List<EquipmentStatusEnum> getNotActive() { /* 58750 Not Active */
        return Arrays.asList(EquipmentStatusEnum._RETIRED, EquipmentStatusEnum._TRANSFERED, EquipmentStatusEnum._UNAVAILABLE);
    }

    public static List<EquipmentStatusEnum> getNotOnSite() { /* 64205 Not on Site */
        return Arrays.asList(EquipmentStatusEnum._RETIRED, EquipmentStatusEnum._TRANSFERED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(EquipmentStatusEnum equipmentStatus) {  /* 58731 *ALL values */
        return EquipmentStatusEnum.getAllValues().contains(equipmentStatus);
    }

    public static boolean isNotActive(EquipmentStatusEnum equipmentStatus) {  /* 58750 Not Active */
        return EquipmentStatusEnum.getNotActive().contains(equipmentStatus);
    }

    public static boolean isNotOnSite(EquipmentStatusEnum equipmentStatus) {  /* 64205 Not on Site */
        return EquipmentStatusEnum.getNotOnSite().contains(equipmentStatus);
    }
}