package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DurationType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DurationTypeEnum {  // 202 STS DUT *Duration type

	_STA_DAYS("DY", "*DAYS") /* 16058 */ ,
	_STA_MONTHS("MO", "*MONTHS") /* 16060 */ ,
	_STA_YYMMDD("YD", "*YYMMDD") /* 16063 */ ,
	_STA_YYMM("YM", "*YYMM") /* 16062 */ ,
	_STA_YEARS("YR", "*YEARS") /* 16061 */ ;
    
	private final String code;
	private final String description;

	DurationTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DurationTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DurationTypeEnum> getAllValues() { /* 16059 *ALL values */
        return Arrays.asList(DurationTypeEnum._STA_DAYS, DurationTypeEnum._STA_MONTHS, DurationTypeEnum._STA_YEARS, DurationTypeEnum._STA_YYMM, DurationTypeEnum._STA_YYMMDD);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DurationTypeEnum durationType) {  /* 16059 *ALL values */
        return DurationTypeEnum.getAllValues().contains(durationType);
    }
}