package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ExitProgramOptionEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ExitProgramOptionConverter implements AttributeConverter<ExitProgramOptionEnum, String> {

	@Override
	public String convertToDatabaseColumn(ExitProgramOptionEnum exitProgramOption) {
		if (exitProgramOption == null) {
			return "";
		}

		return exitProgramOption.getCode();
	}

	@Override
	public ExitProgramOptionEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ExitProgramOptionEnum.fromCode("");
		}

		return ExitProgramOptionEnum.fromCode(StringUtils.strip(dbData));
	}
}
