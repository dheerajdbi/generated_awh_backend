package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for YesOrNo.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum YesOrNoEnum {  // 2687 STS YESORNO Yes Or No

	_NOT_ENTERED("", "Not Entered") /* 24561 */ ,
	_STA_NULL("*", "*Null") /* 61575 */ ,
	_UNKNOWN("?", "Unknown") /* 62886 */ ,
	_COTTON_BALES_ONLY("B", "Cotton Bales Only") /* 52368 */ ,
	_COST_CENTRE("C", "Cost Centre") /* 54351 */ ,
	_HO("H", "HO") /* 60773 */ ,
	_MOSES("K", "Moses") /* 65397 */ ,
	_NO("N", "No") /* 22069 */ ,
	_RFP_REQUIRED("R", "RFP Required") /* 60310 */ ,
	_SOUTHERN("S", "Southern") /* 61221 */ ,
	_U("U", "U") /* 60611 */ ,
	_VICTORIA("V", "Victoria") /* 57699 */ ,
	_WEANERS_UNKNOWN("W", "Weaners Unknown") /* 62889 */ ,
	_X("X", "X") /* 64247 */ ,
	_YES("Y", "Yes") /* 22067 */ ;
    
	private final String code;
	private final String description;

	YesOrNoEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static YesOrNoEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<YesOrNoEnum> getAllValues() { /* 22068 *ALL values */
        return Arrays.asList(YesOrNoEnum._COST_CENTRE, YesOrNoEnum._COTTON_BALES_ONLY, YesOrNoEnum._HO, YesOrNoEnum._MOSES, YesOrNoEnum._NO, YesOrNoEnum._NOT_ENTERED, YesOrNoEnum._STA_NULL, YesOrNoEnum._RFP_REQUIRED, YesOrNoEnum._SOUTHERN, YesOrNoEnum._U, YesOrNoEnum._UNKNOWN, YesOrNoEnum._VICTORIA, YesOrNoEnum._WEANERS_UNKNOWN, YesOrNoEnum._X, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getCXOrY() { /* 64248 C X or Y */
        return Arrays.asList(YesOrNoEnum._COST_CENTRE, YesOrNoEnum._X, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getNotNo() { /* 34597 Not No */
        return Arrays.asList(YesOrNoEnum._NOT_ENTERED, YesOrNoEnum._RFP_REQUIRED, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getNotYes() { /* 37073 Not Yes */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._NOT_ENTERED);
    }

    public static List<YesOrNoEnum> getNotYesOrNo() { /* 55079 Not Yes or No */
        return Arrays.asList(YesOrNoEnum._COST_CENTRE, YesOrNoEnum._COTTON_BALES_ONLY, YesOrNoEnum._NOT_ENTERED);
    }

    public static List<YesOrNoEnum> getValid() { /* 24591 Valid */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYNOrBlank() { /* 55779 Y, N or blank */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._NOT_ENTERED, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesNoOrS() { /* 64852 Yes No or S */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._SOUTHERN, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesOrBlank() { /* 35976 Yes or Blank */
        return Arrays.asList(YesOrNoEnum._NOT_ENTERED, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesOrCostCentre() { /* 54353 Yes or Cost Centre */
        return Arrays.asList(YesOrNoEnum._COST_CENTRE, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesOrNo() { /* 29824 Yes or No */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesOrRfp() { /* 60661 Yes or RFP */
        return Arrays.asList(YesOrNoEnum._RFP_REQUIRED, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesOrS() { /* 65524 Yes or S */
        return Arrays.asList(YesOrNoEnum._SOUTHERN, YesOrNoEnum._YES);
    }

    public static List<YesOrNoEnum> getYesnoBlankOrRfp() { /* 60311 Yes,No Blank or RFP */
        return Arrays.asList(YesOrNoEnum._NO, YesOrNoEnum._NOT_ENTERED, YesOrNoEnum._RFP_REQUIRED, YesOrNoEnum._YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(YesOrNoEnum yesOrNo) {  /* 22068 *ALL values */
        return YesOrNoEnum.getAllValues().contains(yesOrNo);
    }

    public static boolean isCXOrY(YesOrNoEnum yesOrNo) {  /* 64248 C X or Y */
        return YesOrNoEnum.getCXOrY().contains(yesOrNo);
    }

    public static boolean isNotNo(YesOrNoEnum yesOrNo) {  /* 34597 Not No */
        return YesOrNoEnum.getNotNo().contains(yesOrNo);
    }

    public static boolean isNotYes(YesOrNoEnum yesOrNo) {  /* 37073 Not Yes */
        return YesOrNoEnum.getNotYes().contains(yesOrNo);
    }

    public static boolean isNotYesOrNo(YesOrNoEnum yesOrNo) {  /* 55079 Not Yes or No */
        return YesOrNoEnum.getNotYesOrNo().contains(yesOrNo);
    }

    public static boolean isValid(YesOrNoEnum yesOrNo) {  /* 24591 Valid */
        return YesOrNoEnum.getValid().contains(yesOrNo);
    }

    public static boolean isYNOrBlank(YesOrNoEnum yesOrNo) {  /* 55779 Y, N or blank */
        return YesOrNoEnum.getYNOrBlank().contains(yesOrNo);
    }

    public static boolean isYesNoOrS(YesOrNoEnum yesOrNo) {  /* 64852 Yes No or S */
        return YesOrNoEnum.getYesNoOrS().contains(yesOrNo);
    }

    public static boolean isYesOrBlank(YesOrNoEnum yesOrNo) {  /* 35976 Yes or Blank */
        return YesOrNoEnum.getYesOrBlank().contains(yesOrNo);
    }

    public static boolean isYesOrCostCentre(YesOrNoEnum yesOrNo) {  /* 54353 Yes or Cost Centre */
        return YesOrNoEnum.getYesOrCostCentre().contains(yesOrNo);
    }

    public static boolean isYesOrNo(YesOrNoEnum yesOrNo) {  /* 29824 Yes or No */
        return YesOrNoEnum.getYesOrNo().contains(yesOrNo);
    }

    public static boolean isYesOrRfp(YesOrNoEnum yesOrNo) {  /* 60661 Yes or RFP */
        return YesOrNoEnum.getYesOrRfp().contains(yesOrNo);
    }

    public static boolean isYesOrS(YesOrNoEnum yesOrNo) {  /* 65524 Yes or S */
        return YesOrNoEnum.getYesOrS().contains(yesOrNo);
    }

    public static boolean isYesnoBlankOrRfp(YesOrNoEnum yesOrNo) {  /* 60311 Yes,No Blank or RFP */
        return YesOrNoEnum.getYesnoBlankOrRfp().contains(yesOrNo);
    }
}