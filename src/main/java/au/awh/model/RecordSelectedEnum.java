package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for RecordSelected.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum RecordSelectedEnum {  // 41 STS RSL *Record selected

	_STA_NO("N", "*NO") /* 15493 */ ,
	_STA_YES("Y", "*YES") /* 15491 */ ;
    
	private final String code;
	private final String description;

	RecordSelectedEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static RecordSelectedEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<RecordSelectedEnum> getAllValues() { /* 15492 *ALL values */
        return Arrays.asList(RecordSelectedEnum._STA_NO, RecordSelectedEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(RecordSelectedEnum recordSelected) {  /* 15492 *ALL values */
        return RecordSelectedEnum.getAllValues().contains(recordSelected);
    }
}