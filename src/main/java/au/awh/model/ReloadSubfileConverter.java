package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReloadSubfileEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReloadSubfileConverter implements AttributeConverter<ReloadSubfileEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReloadSubfileEnum reloadSubfile) {
		if (reloadSubfile == null) {
			return "";
		}

		return reloadSubfile.getCode();
	}

	@Override
	public ReloadSubfileEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReloadSubfileEnum.fromCode("");
		}

		return ReloadSubfileEnum.fromCode(StringUtils.strip(dbData));
	}
}
