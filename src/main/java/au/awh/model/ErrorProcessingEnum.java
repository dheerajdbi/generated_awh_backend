package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ErrorProcessing.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ErrorProcessingEnum {  // 8135 STS EIST Error Processing

	_NO_PROCESSING("0", "No Processing") /* 35930 */ ,
	_SEND_MESSAGE("1", "Send message") /* 35932 */ ,
	_SEND_MESSAGE_AND_QUIT("2", "Send message and quit") /* 35933 */ ,
	_SEND_MESSAGE_AND_EXIT("3", "Send message and exit") /* 35934 */ ,
	_SEND_MESSAGE_AND_IGNORE("4", "Send message and ignore") /* 35936 */ ,
	_QUIT("5", "Quit") /* 35939 */ ,
	_EXIT("6", "Exit") /* 35940 */ ,
	_IGNORE("7", "Ignore") /* 35941 */ ,
	_SEND_MESSAGE_AND_RELOAD("8", "Send message and reload") /* 41705 */ ,
	_RELOAD("9", "Reload") /* 41706 */ ;
    
	private final String code;
	private final String description;

	ErrorProcessingEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ErrorProcessingEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ErrorProcessingEnum> getAllValues() { /* 35931 *ALL values */
        return Arrays.asList(ErrorProcessingEnum._EXIT, ErrorProcessingEnum._IGNORE, ErrorProcessingEnum._NO_PROCESSING, ErrorProcessingEnum._QUIT, ErrorProcessingEnum._RELOAD, ErrorProcessingEnum._SEND_MESSAGE, ErrorProcessingEnum._SEND_MESSAGE_AND_EXIT, ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE, ErrorProcessingEnum._SEND_MESSAGE_AND_QUIT, ErrorProcessingEnum._SEND_MESSAGE_AND_RELOAD);
    }

    public static List<ErrorProcessingEnum> getDoNotSendMessage() { /* 35946 *Do not send message */
        return Arrays.asList(ErrorProcessingEnum._EXIT, ErrorProcessingEnum._IGNORE, ErrorProcessingEnum._NO_PROCESSING, ErrorProcessingEnum._QUIT, ErrorProcessingEnum._RELOAD);
    }

    public static List<ErrorProcessingEnum> getExit() { /* 35944 *Exit */
        return Arrays.asList(ErrorProcessingEnum._EXIT, ErrorProcessingEnum._SEND_MESSAGE_AND_EXIT);
    }

    public static List<ErrorProcessingEnum> getIgnore() { /* 35945 *Ignore */
        return Arrays.asList(ErrorProcessingEnum._IGNORE, ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE);
    }

    public static List<ErrorProcessingEnum> getQuit() { /* 35943 *Quit */
        return Arrays.asList(ErrorProcessingEnum._QUIT, ErrorProcessingEnum._SEND_MESSAGE_AND_QUIT);
    }

    public static List<ErrorProcessingEnum> getReload() { /* 41707 *Reload */
        return Arrays.asList(ErrorProcessingEnum._RELOAD, ErrorProcessingEnum._SEND_MESSAGE_AND_RELOAD);
    }

    public static List<ErrorProcessingEnum> getSendMessage() { /* 35942 *Send message */
        return Arrays.asList(ErrorProcessingEnum._SEND_MESSAGE, ErrorProcessingEnum._SEND_MESSAGE_AND_EXIT, ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE, ErrorProcessingEnum._SEND_MESSAGE_AND_QUIT, ErrorProcessingEnum._SEND_MESSAGE_AND_RELOAD);
    }

    public static List<ErrorProcessingEnum> getYes() { /* 35935 Yes */
        return Arrays.asList(ErrorProcessingEnum._SEND_MESSAGE, ErrorProcessingEnum._SEND_MESSAGE_AND_EXIT, ErrorProcessingEnum._SEND_MESSAGE_AND_IGNORE, ErrorProcessingEnum._SEND_MESSAGE_AND_QUIT);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ErrorProcessingEnum errorProcessing) {  /* 35931 *ALL values */
        return ErrorProcessingEnum.getAllValues().contains(errorProcessing);
    }

    public static boolean isDoNotSendMessage(ErrorProcessingEnum errorProcessing) {  /* 35946 *Do not send message */
        return ErrorProcessingEnum.getDoNotSendMessage().contains(errorProcessing);
    }

    public static boolean isExit(ErrorProcessingEnum errorProcessing) {  /* 35944 *Exit */
        return ErrorProcessingEnum.getExit().contains(errorProcessing);
    }

    public static boolean isIgnore(ErrorProcessingEnum errorProcessing) {  /* 35945 *Ignore */
        return ErrorProcessingEnum.getIgnore().contains(errorProcessing);
    }

    public static boolean isQuit(ErrorProcessingEnum errorProcessing) {  /* 35943 *Quit */
        return ErrorProcessingEnum.getQuit().contains(errorProcessing);
    }

    public static boolean isReload(ErrorProcessingEnum errorProcessing) {  /* 41707 *Reload */
        return ErrorProcessingEnum.getReload().contains(errorProcessing);
    }

    public static boolean isSendMessage(ErrorProcessingEnum errorProcessing) {  /* 35942 *Send message */
        return ErrorProcessingEnum.getSendMessage().contains(errorProcessing);
    }

    public static boolean isYes(ErrorProcessingEnum errorProcessing) {  /* 35935 Yes */
        return ErrorProcessingEnum.getYes().contains(errorProcessing);
    }
}