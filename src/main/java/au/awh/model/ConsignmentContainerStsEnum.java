package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ConsignmentContainerSts.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ConsignmentContainerStsEnum {  // 8870 STS CCTRSTS Consignment Container Sts

	_NOT_ENTERED("", "not entered") /* 46660 */ ,
	_READY("10", "Ready") /* 38813 */ ,
	_ALLOCATED("20", "Allocated") /* 38815 */ ,
	_MATCHED("25", "Matched") /* 45622 */ ,
	_RECEIVED("30", "Received") /* 38816 */ ,
	_PACKING("40", "Packing") /* 38817 */ ,
	_PACKED("50", "Packed") /* 38818 */ ,
	_SEALED("60", "Sealed") /* 38819 */ ,
	_DESPATCHED("70", "Despatched") /* 38820 */ ,
	_AUDIT("85", "Audit") /* 46438 */ ,
	_HIGH("99", "High") /* 51778 */ ;
    
	private final String code;
	private final String description;

	ConsignmentContainerStsEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ConsignmentContainerStsEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ConsignmentContainerStsEnum> getAllValues() { /* 38814 *ALL values */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._AUDIT, ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._HIGH, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._NOT_ENTERED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._READY, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getAllocatedRecvOrPacking() { /* 43448 Allocated/Recv or Packing */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getAllocatedToDespatched() { /* 51263 Allocated to Despatched */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getContainerStsChgList() { /* 41876 Container Sts Chg List */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getContainerisationAllowed() { /* 42245 Containerisation Allowed */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._READY, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getDeAllocateAllowed() { /* 40692 De-Allocate Allowed */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._READY, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getDeleteAllowed() { /* 40080 Delete Allowed */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._READY);
    }

    public static List<ConsignmentContainerStsEnum> getGreaterThan20() { /* 43159 Greater Than 20 */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getGreaterThan25() { /* 45634 Greater Than 25 */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getMoveToAllowed() { /* 41340 Move to Allowed */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._READY, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getNotPacked() { /* 44043 Not Packed */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._READY, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getNotReceived() { /* 45843 Not Received */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._MATCHED, ConsignmentContainerStsEnum._READY);
    }

    public static List<ConsignmentContainerStsEnum> getPackedOrSealed() { /* 56637 Packed or sealed */
        return Arrays.asList(ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getPackedSealedOrDesp() { /* 44044 Packed, Sealed or Desp */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getPackingDespatched() { /* 55841 Packing > despatched */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getPackingOr() { /* 55782 Packing or > */
        return Arrays.asList(ConsignmentContainerStsEnum._AUDIT, ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._HIGH, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getPackingOrPacked() { /* 56089 Packing or Packed */
        return Arrays.asList(ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING);
    }

    public static List<ConsignmentContainerStsEnum> getRcvpackingSealed() { /* 55843 Rcv,Packing, Sealed */
        return Arrays.asList(ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getReadyOrAllocated() { /* 43275 Ready or Allocated */
        return Arrays.asList(ConsignmentContainerStsEnum._ALLOCATED, ConsignmentContainerStsEnum._READY);
    }

    public static List<ConsignmentContainerStsEnum> getReceivedOrPacking() { /* 41762 Received or Packing */
        return Arrays.asList(ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED);
    }

    public static List<ConsignmentContainerStsEnum> getReceivedToDespatched() { /* 56177 Received to Despatched */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._PACKED, ConsignmentContainerStsEnum._PACKING, ConsignmentContainerStsEnum._RECEIVED, ConsignmentContainerStsEnum._SEALED);
    }

    public static List<ConsignmentContainerStsEnum> getSealedOrDespatched() { /* 42740 Sealed or Despatched */
        return Arrays.asList(ConsignmentContainerStsEnum._DESPATCHED, ConsignmentContainerStsEnum._SEALED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 38814 *ALL values */
        return ConsignmentContainerStsEnum.getAllValues().contains(consignmentContainerSts);
    }

    public static boolean isAllocatedRecvOrPacking(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 43448 Allocated/Recv or Packing */
        return ConsignmentContainerStsEnum.getAllocatedRecvOrPacking().contains(consignmentContainerSts);
    }

    public static boolean isAllocatedToDespatched(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 51263 Allocated to Despatched */
        return ConsignmentContainerStsEnum.getAllocatedToDespatched().contains(consignmentContainerSts);
    }

    public static boolean isContainerStsChgList(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 41876 Container Sts Chg List */
        return ConsignmentContainerStsEnum.getContainerStsChgList().contains(consignmentContainerSts);
    }

    public static boolean isContainerisationAllowed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 42245 Containerisation Allowed */
        return ConsignmentContainerStsEnum.getContainerisationAllowed().contains(consignmentContainerSts);
    }

    public static boolean isDeAllocateAllowed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 40692 De-Allocate Allowed */
        return ConsignmentContainerStsEnum.getDeAllocateAllowed().contains(consignmentContainerSts);
    }

    public static boolean isDeleteAllowed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 40080 Delete Allowed */
        return ConsignmentContainerStsEnum.getDeleteAllowed().contains(consignmentContainerSts);
    }

    public static boolean isGreaterThan20(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 43159 Greater Than 20 */
        return ConsignmentContainerStsEnum.getGreaterThan20().contains(consignmentContainerSts);
    }

    public static boolean isGreaterThan25(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 45634 Greater Than 25 */
        return ConsignmentContainerStsEnum.getGreaterThan25().contains(consignmentContainerSts);
    }

    public static boolean isMoveToAllowed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 41340 Move to Allowed */
        return ConsignmentContainerStsEnum.getMoveToAllowed().contains(consignmentContainerSts);
    }

    public static boolean isNotPacked(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 44043 Not Packed */
        return ConsignmentContainerStsEnum.getNotPacked().contains(consignmentContainerSts);
    }

    public static boolean isNotReceived(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 45843 Not Received */
        return ConsignmentContainerStsEnum.getNotReceived().contains(consignmentContainerSts);
    }

    public static boolean isPackedOrSealed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 56637 Packed or sealed */
        return ConsignmentContainerStsEnum.getPackedOrSealed().contains(consignmentContainerSts);
    }

    public static boolean isPackedSealedOrDesp(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 44044 Packed, Sealed or Desp */
        return ConsignmentContainerStsEnum.getPackedSealedOrDesp().contains(consignmentContainerSts);
    }

    public static boolean isPackingDespatched(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 55841 Packing > despatched */
        return ConsignmentContainerStsEnum.getPackingDespatched().contains(consignmentContainerSts);
    }

    public static boolean isPackingOr(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 55782 Packing or > */
        return ConsignmentContainerStsEnum.getPackingOr().contains(consignmentContainerSts);
    }

    public static boolean isPackingOrPacked(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 56089 Packing or Packed */
        return ConsignmentContainerStsEnum.getPackingOrPacked().contains(consignmentContainerSts);
    }

    public static boolean isRcvpackingSealed(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 55843 Rcv,Packing, Sealed */
        return ConsignmentContainerStsEnum.getRcvpackingSealed().contains(consignmentContainerSts);
    }

    public static boolean isReadyOrAllocated(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 43275 Ready or Allocated */
        return ConsignmentContainerStsEnum.getReadyOrAllocated().contains(consignmentContainerSts);
    }

    public static boolean isReceivedOrPacking(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 41762 Received or Packing */
        return ConsignmentContainerStsEnum.getReceivedOrPacking().contains(consignmentContainerSts);
    }

    public static boolean isReceivedToDespatched(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 56177 Received to Despatched */
        return ConsignmentContainerStsEnum.getReceivedToDespatched().contains(consignmentContainerSts);
    }

    public static boolean isSealedOrDespatched(ConsignmentContainerStsEnum consignmentContainerSts) {  /* 42740 Sealed or Despatched */
        return ConsignmentContainerStsEnum.getSealedOrDespatched().contains(consignmentContainerSts);
    }
}