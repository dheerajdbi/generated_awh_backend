package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for EquipReadingCtrlStsEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class EquipReadingCtrlStsConverter implements AttributeConverter<EquipReadingCtrlStsEnum, String> {

	@Override
	public String convertToDatabaseColumn(EquipReadingCtrlStsEnum equipReadingCtrlSts) {
		if (equipReadingCtrlSts == null) {
			return "";
		}

		return equipReadingCtrlSts.getCode();
	}

	@Override
	public EquipReadingCtrlStsEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return EquipReadingCtrlStsEnum.fromCode("");
		}

		return EquipReadingCtrlStsEnum.fromCode(StringUtils.strip(dbData));
	}
}
