package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for OrgContactDataSource.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum OrgContactDataSourceEnum {  // 9040 STS OCTCDTA Org Contact Data Source

	_AWEX("A", "AWEX") /* 39347 */ ,
	_DUMPING_SYSTEM("D", "Dumping System") /* 39349 */ ;
    
	private final String code;
	private final String description;

	OrgContactDataSourceEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static OrgContactDataSourceEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<OrgContactDataSourceEnum> getAllValues() { /* 39348 *ALL values */
        return Arrays.asList(OrgContactDataSourceEnum._AWEX, OrgContactDataSourceEnum._DUMPING_SYSTEM);
    }

    /**
     * Check
     */

    public static boolean isAllValues(OrgContactDataSourceEnum orgContactDataSource) {  /* 39348 *ALL values */
        return OrgContactDataSourceEnum.getAllValues().contains(orgContactDataSource);
    }
}