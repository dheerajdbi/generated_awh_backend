package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SaveClFormatyesnoEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SaveClFormatyesnoConverter implements AttributeConverter<SaveClFormatyesnoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SaveClFormatyesnoEnum saveClFormatyesno) {
		if (saveClFormatyesno == null) {
			return "";
		}

		return saveClFormatyesno.getCode();
	}

	@Override
	public SaveClFormatyesnoEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SaveClFormatyesnoEnum.fromCode("");
		}

		return SaveClFormatyesnoEnum.fromCode(StringUtils.strip(dbData));
	}
}
