package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ConsignmentContainerStsEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ConsignmentContainerStsConverter implements AttributeConverter<ConsignmentContainerStsEnum, String> {

	@Override
	public String convertToDatabaseColumn(ConsignmentContainerStsEnum consignmentContainerSts) {
		if (consignmentContainerSts == null) {
			return "";
		}

		return consignmentContainerSts.getCode();
	}

	@Override
	public ConsignmentContainerStsEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ConsignmentContainerStsEnum.fromCode("");
		}

		return ConsignmentContainerStsEnum.fromCode(StringUtils.strip(dbData));
	}
}
