package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for EquipReadingCtrlSts.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum EquipReadingCtrlStsEnum {  // 13999 STS PJST Equip Reading Ctrl Sts

	_CREATED("C", "Created") /* 58682 */ ,
	_IN_DAS_PROGRESS("I", "In-Progress") /* 58684 */ ,
	_COMPLETE("X", "Complete") /* 58685 */ ;
    
	private final String code;
	private final String description;

	EquipReadingCtrlStsEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static EquipReadingCtrlStsEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<EquipReadingCtrlStsEnum> getAllValues() { /* 58683 *ALL values */
        return Arrays.asList(EquipReadingCtrlStsEnum._COMPLETE, EquipReadingCtrlStsEnum._CREATED, EquipReadingCtrlStsEnum._IN_DAS_PROGRESS);
    }

    public static List<EquipReadingCtrlStsEnum> getIncomplete() { /* 64150 Incomplete */
        return Arrays.asList(EquipReadingCtrlStsEnum._CREATED, EquipReadingCtrlStsEnum._IN_DAS_PROGRESS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(EquipReadingCtrlStsEnum equipReadingCtrlSts) {  /* 58683 *ALL values */
        return EquipReadingCtrlStsEnum.getAllValues().contains(equipReadingCtrlSts);
    }

    public static boolean isIncomplete(EquipReadingCtrlStsEnum equipReadingCtrlSts) {  /* 64150 Incomplete */
        return EquipReadingCtrlStsEnum.getIncomplete().contains(equipReadingCtrlSts);
    }
}