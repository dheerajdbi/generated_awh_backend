package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for CheckRecordOptionEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class CheckRecordOptionConverter implements AttributeConverter<CheckRecordOptionEnum, String> {

	@Override
	public String convertToDatabaseColumn(CheckRecordOptionEnum checkRecordOption) {
		if (checkRecordOption == null) {
			return "";
		}

		return checkRecordOption.getCode();
	}

	@Override
	public CheckRecordOptionEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return CheckRecordOptionEnum.fromCode("");
		}

		return CheckRecordOptionEnum.fromCode(StringUtils.strip(dbData));
	}
}
