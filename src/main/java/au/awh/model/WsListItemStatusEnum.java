package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for WsListItemStatus.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum WsListItemStatusEnum {  // 8034 STS DFST WS List Item Status

	_NORMAL("", "Normal") /* 35601 */ ,
	_HIDDEN("H", "Hidden") /* 35599 */ ;
    
	private final String code;
	private final String description;

	WsListItemStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static WsListItemStatusEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<WsListItemStatusEnum> getAllValues() { /* 35600 *ALL values */
        return Arrays.asList(WsListItemStatusEnum._HIDDEN, WsListItemStatusEnum._NORMAL);
    }

    /**
     * Check
     */

    public static boolean isAllValues(WsListItemStatusEnum wsListItemStatus) {  /* 35600 *ALL values */
        return WsListItemStatusEnum.getAllValues().contains(wsListItemStatus);
    }
}