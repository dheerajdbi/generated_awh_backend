package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for OrgContactStatus.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum OrgContactStatusEnum {  // 9043 STS OCTCSTS Org Contact Status

	_ACTIVE("A", "Active") /* 39350 */ ,
	_INACTIVE("I", "Inactive") /* 39352 */ ;
    
	private final String code;
	private final String description;

	OrgContactStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static OrgContactStatusEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<OrgContactStatusEnum> getAllValues() { /* 39351 *ALL values */
        return Arrays.asList(OrgContactStatusEnum._ACTIVE, OrgContactStatusEnum._INACTIVE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(OrgContactStatusEnum orgContactStatus) {  /* 39351 *ALL values */
        return OrgContactStatusEnum.getAllValues().contains(orgContactStatus);
    }
}