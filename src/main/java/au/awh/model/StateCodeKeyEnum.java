package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for StateCodeKey.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum StateCodeKeyEnum {  // 5154 STS STTCDEK State Code            KEY

	_NOT_ENTERED("", "Not entered") /* 27275 */ ,
	_DEFAULT_STA_("*", "Default *") /* 34789 */ ,
	_PLU_("+", "+") /* 35896 */ ,
	_ZERO("0", "Zero") /* 27284 */ ,
	_NEW_SOUTH_WALES("2", "New South Wales") /* 27278 */ ,
	_VICTORIA("3", "Victoria") /* 27282 */ ,
	_QUEENSLAND("4", "Queensland") /* 27279 */ ,
	_SOUTH_AUSTRALIA("5", "South Australia") /* 27280 */ ,
	_WESTERN_AUSTRALIA("6", "Western Australia") /* 27283 */ ,
	_TASMANIA("7", "Tasmania") /* 27281 */ ,
	_EXPRESS("E", "Express") /* 47944 */ ,
	_FAST("F", "Fast") /* 47996 */ ,
	_PREMIER("I", "Premier") /* 27277 */ ,
	_NORMAL("N", "Normal") /* 47943 */ ,
	_W("W", "W") /* 64642 */ ,
	_EXPRESS_AT_CORELINE("Y", "Express at Coreline") /* 64349 */ ,
	_FAST_AT_CORELINE("Z", "Fast at Coreline") /* 64350 */ ;
    
	private final String code;
	private final String description;

	StateCodeKeyEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static StateCodeKeyEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<StateCodeKeyEnum> getAllValues() { /* 27276 *ALL values */
        return Arrays.asList(StateCodeKeyEnum._DEFAULT_STA_, StateCodeKeyEnum._EXPRESS, StateCodeKeyEnum._EXPRESS_AT_CORELINE, StateCodeKeyEnum._FAST, StateCodeKeyEnum._FAST_AT_CORELINE, StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._NORMAL, StateCodeKeyEnum._NOT_ENTERED, StateCodeKeyEnum._PLU_, StateCodeKeyEnum._PREMIER, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._TASMANIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._W, StateCodeKeyEnum._WESTERN_AUSTRALIA, StateCodeKeyEnum._ZERO);
    }

    public static List<StateCodeKeyEnum> getAllowedAtCoreline() { /* 64763 Allowed at Coreline */
        return Arrays.asList(StateCodeKeyEnum._EXPRESS, StateCodeKeyEnum._FAST, StateCodeKeyEnum._NORMAL);
    }

    public static List<StateCodeKeyEnum> getCoreline() { /* 64706 Coreline */
        return Arrays.asList(StateCodeKeyEnum._EXPRESS, StateCodeKeyEnum._EXPRESS_AT_CORELINE, StateCodeKeyEnum._FAST, StateCodeKeyEnum._FAST_AT_CORELINE, StateCodeKeyEnum._NORMAL);
    }

    public static List<StateCodeKeyEnum> getExpressList() { /* 64742 Express List */
        return Arrays.asList(StateCodeKeyEnum._EXPRESS, StateCodeKeyEnum._EXPRESS_AT_CORELINE);
    }

    public static List<StateCodeKeyEnum> getFastList() { /* 64743 Fast List */
        return Arrays.asList(StateCodeKeyEnum._FAST, StateCodeKeyEnum._FAST_AT_CORELINE);
    }

    public static List<StateCodeKeyEnum> getInvalidState() { /* 27285 Invalid State */
        return Arrays.asList(StateCodeKeyEnum._NOT_ENTERED, StateCodeKeyEnum._ZERO);
    }

    public static List<StateCodeKeyEnum> getNotBlank() { /* 29298 Not Blank */
        return Arrays.asList(StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._PREMIER, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._TASMANIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._W, StateCodeKeyEnum._WESTERN_AUSTRALIA, StateCodeKeyEnum._ZERO);
    }

    public static List<StateCodeKeyEnum> getNumeric2To6() { /* 27286 Numeric ( 2 to 6 ) */
        return Arrays.asList(StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._TASMANIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._WESTERN_AUSTRALIA);
    }

    public static List<StateCodeKeyEnum> getState23456() { /* 43174 State 2 3 4 5 6 */
        return Arrays.asList(StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._WESTERN_AUSTRALIA);
    }

    public static List<StateCodeKeyEnum> getState234567I() { /* 43584 State 2 3 4 5 6 7 I */
        return Arrays.asList(StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._PREMIER, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._TASMANIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._WESTERN_AUSTRALIA);
    }

    public static List<StateCodeKeyEnum> getStateI7() { /* 43255 State I 7 */
        return Arrays.asList(StateCodeKeyEnum._PREMIER, StateCodeKeyEnum._TASMANIA);
    }

    public static List<StateCodeKeyEnum> getTestPriority() { /* 47942 Test Priority */
        return Arrays.asList(StateCodeKeyEnum._EXPRESS, StateCodeKeyEnum._EXPRESS_AT_CORELINE, StateCodeKeyEnum._FAST, StateCodeKeyEnum._FAST_AT_CORELINE, StateCodeKeyEnum._NORMAL);
    }

    public static List<StateCodeKeyEnum> getValidState() { /* 27287 Valid State */
        return Arrays.asList(StateCodeKeyEnum._DEFAULT_STA_, StateCodeKeyEnum._NEW_SOUTH_WALES, StateCodeKeyEnum._PREMIER, StateCodeKeyEnum._QUEENSLAND, StateCodeKeyEnum._SOUTH_AUSTRALIA, StateCodeKeyEnum._TASMANIA, StateCodeKeyEnum._VICTORIA, StateCodeKeyEnum._W, StateCodeKeyEnum._WESTERN_AUSTRALIA);
    }

    /**
     * Check
     */

    public static boolean isAllValues(StateCodeKeyEnum stateCodeKey) {  /* 27276 *ALL values */
        return StateCodeKeyEnum.getAllValues().contains(stateCodeKey);
    }

    public static boolean isAllowedAtCoreline(StateCodeKeyEnum stateCodeKey) {  /* 64763 Allowed at Coreline */
        return StateCodeKeyEnum.getAllowedAtCoreline().contains(stateCodeKey);
    }

    public static boolean isCoreline(StateCodeKeyEnum stateCodeKey) {  /* 64706 Coreline */
        return StateCodeKeyEnum.getCoreline().contains(stateCodeKey);
    }

    public static boolean isExpressList(StateCodeKeyEnum stateCodeKey) {  /* 64742 Express List */
        return StateCodeKeyEnum.getExpressList().contains(stateCodeKey);
    }

    public static boolean isFastList(StateCodeKeyEnum stateCodeKey) {  /* 64743 Fast List */
        return StateCodeKeyEnum.getFastList().contains(stateCodeKey);
    }

    public static boolean isInvalidState(StateCodeKeyEnum stateCodeKey) {  /* 27285 Invalid State */
        return StateCodeKeyEnum.getInvalidState().contains(stateCodeKey);
    }

    public static boolean isNotBlank(StateCodeKeyEnum stateCodeKey) {  /* 29298 Not Blank */
        return StateCodeKeyEnum.getNotBlank().contains(stateCodeKey);
    }

    public static boolean isNumeric2To6(StateCodeKeyEnum stateCodeKey) {  /* 27286 Numeric ( 2 to 6 ) */
        return StateCodeKeyEnum.getNumeric2To6().contains(stateCodeKey);
    }

    public static boolean isState23456(StateCodeKeyEnum stateCodeKey) {  /* 43174 State 2 3 4 5 6 */
        return StateCodeKeyEnum.getState23456().contains(stateCodeKey);
    }

    public static boolean isState234567I(StateCodeKeyEnum stateCodeKey) {  /* 43584 State 2 3 4 5 6 7 I */
        return StateCodeKeyEnum.getState234567I().contains(stateCodeKey);
    }

    public static boolean isStateI7(StateCodeKeyEnum stateCodeKey) {  /* 43255 State I 7 */
        return StateCodeKeyEnum.getStateI7().contains(stateCodeKey);
    }

    public static boolean isTestPriority(StateCodeKeyEnum stateCodeKey) {  /* 47942 Test Priority */
        return StateCodeKeyEnum.getTestPriority().contains(stateCodeKey);
    }

    public static boolean isValidState(StateCodeKeyEnum stateCodeKey) {  /* 27287 Valid State */
        return StateCodeKeyEnum.getValidState().contains(stateCodeKey);
    }
}