package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DisplayFlagUsrEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DisplayFlagUsrConverter implements AttributeConverter<DisplayFlagUsrEnum, String> {

	@Override
	public String convertToDatabaseColumn(DisplayFlagUsrEnum displayFlagUsr) {
		if (displayFlagUsr == null) {
			return "";
		}

		return displayFlagUsr.getCode();
	}

	@Override
	public DisplayFlagUsrEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DisplayFlagUsrEnum.fromCode("");
		}

		return DisplayFlagUsrEnum.fromCode(StringUtils.strip(dbData));
	}
}
