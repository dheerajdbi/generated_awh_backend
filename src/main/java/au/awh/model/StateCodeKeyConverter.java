package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for StateCodeKeyEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class StateCodeKeyConverter implements AttributeConverter<StateCodeKeyEnum, String> {

	@Override
	public String convertToDatabaseColumn(StateCodeKeyEnum stateCodeKey) {
		if (stateCodeKey == null) {
			return "";
		}

		return stateCodeKey.getCode();
	}

	@Override
	public StateCodeKeyEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return StateCodeKeyEnum.fromCode("");
		}

		return StateCodeKeyEnum.fromCode(StringUtils.strip(dbData));
	}
}
