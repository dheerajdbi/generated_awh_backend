package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for AwhBuisnessSegment.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum AwhBuisnessSegmentEnum {  // 13990 STS AWHBSG AWH Buisness Segment

	_WOOL_HANDLING("01", "Wool Handling") /* 58628 */ ,
	_DUMPING("02", "Dumping") /* 58630 */ ,
	_COTTON("03", "Cotton") /* 58631 */ ,
	_LOGISTICS("04", "Logistics") /* 58632 */ ,
	_PROPERTY("05", "Property") /* 58633 */ ,
	_DRY_BULK("06", "Dry Bulk") /* 58723 */ ,
	_RE_DAS_HANDLE("09", "Re-Handle") /* 64167 */ ;
    
	private final String code;
	private final String description;

	AwhBuisnessSegmentEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static AwhBuisnessSegmentEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<AwhBuisnessSegmentEnum> getAllValues() { /* 58629 *ALL values */
        return Arrays.asList(AwhBuisnessSegmentEnum._COTTON, AwhBuisnessSegmentEnum._DRY_BULK, AwhBuisnessSegmentEnum._DUMPING, AwhBuisnessSegmentEnum._LOGISTICS, AwhBuisnessSegmentEnum._PROPERTY, AwhBuisnessSegmentEnum._RE_DAS_HANDLE, AwhBuisnessSegmentEnum._WOOL_HANDLING);
    }

    /**
     * Check
     */

    public static boolean isAllValues(AwhBuisnessSegmentEnum awhBuisnessSegment) {  /* 58629 *ALL values */
        return AwhBuisnessSegmentEnum.getAllValues().contains(awhBuisnessSegment);
    }
}