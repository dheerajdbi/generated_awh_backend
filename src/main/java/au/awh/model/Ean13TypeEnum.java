package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Ean13Type.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum Ean13TypeEnum {  // 8503 STS EAN13T EAN 13 Type

	_FOLIO_NUMBER("", "Folio Number") /* 37399 */ ,
	_LOT_UNIQUE_ID("0", "Lot Unique Id") /* 37402 */ ,
	_FOLIO_UNIQUE_ID("1", "Folio Unique Id") /* 37401 */ ,
	_LOCATION_SURROGATE("2", "Location Surrogate") /* 37403 */ ,
	_SF_BOX_LOCATION_SURROGATE("3", "SF Box Location Surrogate") /* 37742 */ ,
	_LOCATION_SURROGATE_COMGEN("4", "Location Surrogate ComGen") /* 38032 */ ,
	_DUMP_SYSTEM("5", "Dump System") /* 41200 */ ,
	_EQUIPMENT("6", "Equipment") /* 58779 */ ,
	_DELIVERY_LOAD("7", "Delivery Load") /* 61982 */ ;
    
	private final String code;
	private final String description;

	Ean13TypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static Ean13TypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<Ean13TypeEnum> getAllValues() { /* 37400 *ALL values */
        return Arrays.asList(Ean13TypeEnum._DELIVERY_LOAD, Ean13TypeEnum._DUMP_SYSTEM, Ean13TypeEnum._EQUIPMENT, Ean13TypeEnum._FOLIO_NUMBER, Ean13TypeEnum._FOLIO_UNIQUE_ID, Ean13TypeEnum._LOCATION_SURROGATE, Ean13TypeEnum._LOCATION_SURROGATE_COMGEN, Ean13TypeEnum._LOT_UNIQUE_ID, Ean13TypeEnum._SF_BOX_LOCATION_SURROGATE);
    }

    public static List<Ean13TypeEnum> getGrabSample() { /* 37507 Grab Sample */
        return Arrays.asList(Ean13TypeEnum._FOLIO_UNIQUE_ID, Ean13TypeEnum._LOT_UNIQUE_ID);
    }

    public static List<Ean13TypeEnum> getLocation() { /* 37743 Location */
        return Arrays.asList(Ean13TypeEnum._LOCATION_SURROGATE, Ean13TypeEnum._LOCATION_SURROGATE_COMGEN, Ean13TypeEnum._SF_BOX_LOCATION_SURROGATE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(Ean13TypeEnum ean13Type) {  /* 37400 *ALL values */
        return Ean13TypeEnum.getAllValues().contains(ean13Type);
    }

    public static boolean isGrabSample(Ean13TypeEnum ean13Type) {  /* 37507 Grab Sample */
        return Ean13TypeEnum.getGrabSample().contains(ean13Type);
    }

    public static boolean isLocation(Ean13TypeEnum ean13Type) {  /* 37743 Location */
        return Ean13TypeEnum.getLocation().contains(ean13Type);
    }
}