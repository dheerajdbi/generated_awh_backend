package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DateListAutoloadEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DateListAutoloadConverter implements AttributeConverter<DateListAutoloadEnum, String> {

	@Override
	public String convertToDatabaseColumn(DateListAutoloadEnum dateListAutoload) {
		if (dateListAutoload == null) {
			return "";
		}

		return dateListAutoload.getCode();
	}

	@Override
	public DateListAutoloadEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DateListAutoloadEnum.fromCode("");
		}

		return DateListAutoloadEnum.fromCode(StringUtils.strip(dbData));
	}
}
