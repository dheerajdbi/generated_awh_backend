package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for SendViaPrintedReport.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SendViaPrintedReportEnum {  // 10476 STS SNDVPRT Send via Printed Report

	_USE_HIGHER_LEVEL("", "Use Higher Level") /* 46092 */ ,
	_NO("N", "No") /* 46091 */ ,
	_YES("Y", "Yes") /* 46089 */ ;
    
	private final String code;
	private final String description;

	SendViaPrintedReportEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SendViaPrintedReportEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SendViaPrintedReportEnum> getAllValues() { /* 46090 *ALL values */
        return Arrays.asList(SendViaPrintedReportEnum._NO, SendViaPrintedReportEnum._USE_HIGHER_LEVEL, SendViaPrintedReportEnum._YES);
    }

    public static List<SendViaPrintedReportEnum> getEntered() { /* 46112 Entered */
        return Arrays.asList(SendViaPrintedReportEnum._NO, SendViaPrintedReportEnum._YES);
    }

    public static List<SendViaPrintedReportEnum> getNotYes() { /* 54757 Not Yes */
        return Arrays.asList(SendViaPrintedReportEnum._NO, SendViaPrintedReportEnum._USE_HIGHER_LEVEL);
    }

    public static List<SendViaPrintedReportEnum> getYesOrNo() { /* 50020 Yes or No */
        return Arrays.asList(SendViaPrintedReportEnum._NO, SendViaPrintedReportEnum._YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(SendViaPrintedReportEnum sendViaPrintedReport) {  /* 46090 *ALL values */
        return SendViaPrintedReportEnum.getAllValues().contains(sendViaPrintedReport);
    }

    public static boolean isEntered(SendViaPrintedReportEnum sendViaPrintedReport) {  /* 46112 Entered */
        return SendViaPrintedReportEnum.getEntered().contains(sendViaPrintedReport);
    }

    public static boolean isNotYes(SendViaPrintedReportEnum sendViaPrintedReport) {  /* 54757 Not Yes */
        return SendViaPrintedReportEnum.getNotYes().contains(sendViaPrintedReport);
    }

    public static boolean isYesOrNo(SendViaPrintedReportEnum sendViaPrintedReport) {  /* 50020 Yes or No */
        return SendViaPrintedReportEnum.getYesOrNo().contains(sendViaPrintedReport);
    }
}