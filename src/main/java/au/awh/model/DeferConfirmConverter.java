package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DeferConfirmEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DeferConfirmConverter implements AttributeConverter<DeferConfirmEnum, String> {

	@Override
	public String convertToDatabaseColumn(DeferConfirmEnum deferConfirm) {
		if (deferConfirm == null) {
			return "";
		}

		return deferConfirm.getCode();
	}

	@Override
	public DeferConfirmEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DeferConfirmEnum.fromCode("");
		}

		return DeferConfirmEnum.fromCode(StringUtils.strip(dbData));
	}
}
