package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DurationTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DurationTypeConverter implements AttributeConverter<DurationTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(DurationTypeEnum durationType) {
		if (durationType == null) {
			return "";
		}

		return durationType.getCode();
	}

	@Override
	public DurationTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DurationTypeEnum.fromCode("");
		}

		return DurationTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
