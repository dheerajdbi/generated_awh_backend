package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SflselEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SflselConverter implements AttributeConverter<SflselEnum, String> {

	@Override
	public String convertToDatabaseColumn(SflselEnum sflsel) {
		if (sflsel == null) {
			return "";
		}

		return sflsel.getCode();
	}

	@Override
	public SflselEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SflselEnum.fromCode("");
		}

		return SflselEnum.fromCode(StringUtils.strip(dbData));
	}
}
