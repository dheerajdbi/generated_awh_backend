package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for OrgContactStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class OrgContactStatusConverter implements AttributeConverter<OrgContactStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(OrgContactStatusEnum orgContactStatus) {
		if (orgContactStatus == null) {
			return "";
		}

		return orgContactStatus.getCode();
	}

	@Override
	public OrgContactStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return OrgContactStatusEnum.fromCode("");
		}

		return OrgContactStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
