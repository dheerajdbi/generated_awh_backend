package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReadingFrequencyEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReadingFrequencyConverter implements AttributeConverter<ReadingFrequencyEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReadingFrequencyEnum readingFrequency) {
		if (readingFrequency == null) {
			return "";
		}

		return readingFrequency.getCode();
	}

	@Override
	public ReadingFrequencyEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReadingFrequencyEnum.fromCode("");
		}

		return ReadingFrequencyEnum.fromCode(StringUtils.strip(dbData));
	}
}
