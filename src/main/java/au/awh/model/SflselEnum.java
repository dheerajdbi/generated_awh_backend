package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Sflsel.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SflselEnum {  // 10 STS SEL *SFLSEL

	_NOT_ENTERED("", "Not entered") /* 18168 */ ,
	_NBR_("#", "#") /* 42633 */ ,
	_STA_("*", "*") /* 38111 */ ,
	_STA_SELECTION_CHAR_VALUE("/", "*Selection char value") /* 15625 */ ,
	_STA_SELECTION_CHAR_VALUE_NBR_2("/", "*Selection char value #2") /* 16122 */ ,
	_00("00", "00") /* 33217 */ ,
	_SELECT_NBR_1("1", "Select#1") /* 16788 */ ,
	_STA_SELECT_NBR_2("1", "*Select#2") /* 16121 */ ,
	_14("14", "14") /* 35497 */ ,
	_CHANGE("2", "Change") /* 16789 */ ,
	_2("2", "2") /* 32111 */ ,
	_24("24", "24") /* 33227 */ ,
	_2A("2A", "2A") /* 33236 */ ,
	_2B("2B", "2B") /* 33237 */ ,
	_2C("2C", "2C") /* 33238 */ ,
	_3("3", "3") /* 16790 */ ,
	_3_NBR_2("3", "3#2") /* 38104 */ ,
	_31("31", "31") /* 31956 */ ,
	_32("32", "32") /* 31957 */ ,
	_4("4", "4") /* 16791 */ ,
	_STA_DELETE_NBR_2("4", "*Delete#2") /* 16120 */ ,
	_40("40", "40") /* 33224 */ ,
	_42("42", "42") /* 33225 */ ,
	_44("44", "44") /* 33249 */ ,
	_45("45", "45") /* 33226 */ ,
	_5_NBR_1("5", "5#1") /* 16792 */ ,
	_5_NBR_2("5", "5#2") /* 16123 */ ,
	_50("50", "50") /* 31965 */ ,
	_51("51", "51") /* 31953 */ ,
	_6_NBR_1("6", "6#1") /* 30034 */ ,
	_6_NBR_2("6", "6#2") /* 31934 */ ,
	_7_NBR_1("7", "7#1") /* 37208 */ ,
	_7_NBR_2("7", "7#2") /* 37209 */ ,
	_8_NBR_1("8", "8#1") /* 32707 */ ,
	_8_NBR_2("8", "8#2") /* 32708 */ ,
	_80("80", "80") /* 31935 */ ,
	_81("81", "81") /* 31936 */ ,
	_82("82", "82") /* 33228 */ ,
	_84("84", "84") /* 33229 */ ,
	_9("9", "9") /* 31967 */ ,
	_99("99", "99") /* 31966 */ ,
	_SPECIAL_EDIT_AT_("@", "Special Edit @") /* 57622 */ ,
	_A("A", "A") /* 16945 */ ,
	_B("B", "B") /* 17334 */ ,
	_C("C", "C") /* 17826 */ ,
	_D("D", "D") /* 15480 */ ,
	_E("E", "E") /* 17827 */ ,
	_F("F", "F") /* 17659 */ ,
	_F_NBR_2("F", "F#2") /* 40804 */ ,
	_G("G", "G") /* 17828 */ ,
	_H("H", "H") /* 17829 */ ,
	_I("I", "I") /* 17336 */ ,
	_J("J", "J") /* 43267 */ ,
	_J_NBR_2("J", "J#2") /* 43268 */ ,
	_K("K", "K") /* 45802 */ ,
	_L("L", "L") /* 17335 */ ,
	_M("M", "M") /* 18552 */ ,
	_N("N", "N") /* 21572 */ ,
	_O("O", "O") /* 27584 */ ,
	_O0("O0", "O0") /* 33244 */ ,
	_P("P", "P") /* 17660 */ ,
	_Q("Q", "Q") /* 20777 */ ,
	_R("R", "R") /* 17133 */ ,
	_S("S", "S") /* 19578 */ ,
	_T("T", "T") /* 19003 */ ,
	_U("U", "U") /* 18110 */ ,
	_U_NBR_2("U", "U#2") /* 45379 */ ,
	_V("V", "V") /* 23074 */ ,
	_W("W", "W") /* 21571 */ ,
	_X("X", "X") /* 15482 */ ,
	_Y("Y", "Y") /* 64509 */ ,
	_STA_ZOOM("Z", "*Zoom") /* 15494 */ ,
	_STA_ZOOM_NBR_2("Z", "*Zoom#2") /* 41460 */ ;
    
	private final String code;
	private final String description;

	SflselEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SflselEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SflselEnum> getAddDiscrepancy() { /* 65687 Add Discrepancy */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAddole() { /* 52873 ADDOLE */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAliases() { /* 40852 Aliases */
        return Arrays.asList(SflselEnum._8_NBR_1, SflselEnum._8_NBR_2);
    }

    public static List<SflselEnum> getAllValues() { /* 15481 *ALL values */
        return Arrays.asList(SflselEnum._STA_, SflselEnum._A, SflselEnum._B, SflselEnum._C, SflselEnum._C, SflselEnum._CHANGE, SflselEnum._D, SflselEnum._STA_DELETE_NBR_2, SflselEnum._E, SflselEnum._F, SflselEnum._F_NBR_2, SflselEnum._G, SflselEnum._H, SflselEnum._I, SflselEnum._J, SflselEnum._J_NBR_2, SflselEnum._K, SflselEnum._L, SflselEnum._M, SflselEnum._N, SflselEnum._NBR_, SflselEnum._NOT_ENTERED, SflselEnum._O, SflselEnum._O0, SflselEnum._P, SflselEnum._P, SflselEnum._Q, SflselEnum._R, SflselEnum._R, SflselEnum._S, SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2, SflselEnum._STA_SELECTION_CHAR_VALUE, SflselEnum._STA_SELECTION_CHAR_VALUE_NBR_2, SflselEnum._SPECIAL_EDIT_AT_, SflselEnum._T, SflselEnum._U, SflselEnum._U_NBR_2, SflselEnum._V, SflselEnum._V, SflselEnum._W, SflselEnum._X, SflselEnum._00, SflselEnum._14, SflselEnum._2, SflselEnum._24, SflselEnum._2A, SflselEnum._2B, SflselEnum._2C, SflselEnum._3, SflselEnum._31, SflselEnum._32, SflselEnum._3_NBR_2, SflselEnum._4, SflselEnum._40, SflselEnum._42, SflselEnum._44, SflselEnum._45, SflselEnum._50, SflselEnum._51, SflselEnum._5_NBR_1, SflselEnum._5_NBR_2, SflselEnum._6_NBR_1, SflselEnum._6_NBR_2, SflselEnum._7_NBR_1, SflselEnum._7_NBR_2, SflselEnum._80, SflselEnum._81, SflselEnum._82, SflselEnum._84, SflselEnum._8_NBR_1, SflselEnum._8_NBR_2, SflselEnum._9, SflselEnum._9, SflselEnum._99, SflselEnum._Y, SflselEnum._STA_ZOOM, SflselEnum._STA_ZOOM_NBR_2);
    }

    public static List<SflselEnum> getAllocate() { /* 21591 Allocate */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAllowedField() { /* 39511 Allowed Field */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAppraisals() { /* 26604 Appraisals */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAssignClientId() { /* 50241 Assign Client Id */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAssignToRelease() { /* 49644 Assign to Release */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getAwtaDetails() { /* 64833 AWTA Details */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getBagDetails() { /* 17832 Bag Details */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getBaleDetails() { /* 17831 Bale Details */
        return Arrays.asList(SflselEnum._B);
    }

    public static List<SflselEnum> getBales() { /* 19514 Bales */
        return Arrays.asList(SflselEnum._B);
    }

    public static List<SflselEnum> getBeforeOrAfter() { /* 18879 Before or After */
        return Arrays.asList(SflselEnum._A, SflselEnum._B);
    }

    public static List<SflselEnum> getBinSplits() { /* 19579 Bin Splits */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getBulkClass() { /* 19523 Bulk Class */
        return Arrays.asList(SflselEnum._B);
    }

    public static List<SflselEnum> getCarrierDetails() { /* 17830 Carrier Details */
        return Arrays.asList(SflselEnum._C);
    }

    public static List<SflselEnum> getCatalogueSymbols() { /* 20192 Catalogue Symbols */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getChangeOrDelete() { /* 19825 Change or Delete */
        return Arrays.asList(SflselEnum._CHANGE, SflselEnum._4);
    }

    public static List<SflselEnum> getChargeRateSequences() { /* 42329 Charge Rate Sequences */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getClientDetail() { /* 20236 Client Detail */
        return Arrays.asList(SflselEnum._C);
    }

    public static List<SflselEnum> getClipActivity() { /* 26956 Clip Activity */
        return Arrays.asList(SflselEnum._C);
    }

    public static List<SflselEnum> getCollationDetails() { /* 52817 Collation Details */
        return Arrays.asList(SflselEnum._C);
    }

    public static List<SflselEnum> getComments() { /* 37691 Comments */
        return Arrays.asList(SflselEnum._C, SflselEnum._C);
    }

    public static List<SflselEnum> getComplete() { /* 32181 Complete */
        return Arrays.asList(SflselEnum._C, SflselEnum._C);
    }

    public static List<SflselEnum> getConfirm() { /* 32711 Confirm */
        return Arrays.asList(SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2);
    }

    public static List<SflselEnum> getConfirmReceivalBale() { /* 41457 Confirm Receival Bale */
        return Arrays.asList(SflselEnum._C);
    }

    public static List<SflselEnum> getConfirmSheets() { /* 32709 Confirm Sheets */
        return Arrays.asList(SflselEnum._C, SflselEnum._C);
    }

    public static List<SflselEnum> getConsignment() { /* 37693 Consignment */
        return Arrays.asList(SflselEnum._O);
    }

    public static List<SflselEnum> getContainers() { /* 49533 Containers */
        return Arrays.asList(SflselEnum._C, SflselEnum._C);
    }

    public static List<SflselEnum> getCopy() { /* 37976 Copy */
        return Arrays.asList(SflselEnum._3, SflselEnum._3_NBR_2);
    }

    public static List<SflselEnum> getDeAllocate() { /* 44152 De-Allocate */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDelete() { /* 32698 Delete */
        return Arrays.asList(SflselEnum._STA_DELETE_NBR_2, SflselEnum._4);
    }

    public static List<SflselEnum> getDeleteRequest() { /* 15564 *Delete request */
        return Arrays.asList(SflselEnum._STA_DELETE_NBR_2, SflselEnum._4);
    }

    public static List<SflselEnum> getDescription_() { /* 34323 Description */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDiscountRates() { /* 24378 Discount Rates */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDiscrepancies() { /* 17658 Discrepancies */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDiscrepancy() { /* 65592 Discrepancy */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDisplay() { /* 33470 Display */
        return Arrays.asList(SflselEnum._5_NBR_1, SflselEnum._5_NBR_2);
    }

    public static List<SflselEnum> getDisplayAudit() { /* 35498 Display Audit */
        return Arrays.asList(SflselEnum._14);
    }

    public static List<SflselEnum> getDisplayDocument() { /* 40164 Display Document */
        return Arrays.asList(SflselEnum._8_NBR_1, SflselEnum._8_NBR_2);
    }

    public static List<SflselEnum> getDisplayMessages() { /* 31933 Display Messages */
        return Arrays.asList(SflselEnum._6_NBR_1, SflselEnum._6_NBR_2);
    }

    public static List<SflselEnum> getDnpxOrBlank() { /* 37679 D,N,P,X or Blank */
        return Arrays.asList(SflselEnum._D, SflselEnum._N, SflselEnum._NOT_ENTERED, SflselEnum._P, SflselEnum._X);
    }

    public static List<SflselEnum> getDoNotLotDetails() { /* 19731 Do not Lot Details */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDocumentDistribution() { /* 40881 Document Distribution */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDocuments() { /* 45720 Documents */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getDropReinstate() { /* 35276 Drop/Reinstate */
        return Arrays.asList(SflselEnum._D);
    }

    public static List<SflselEnum> getEdit() { /* 32110 Edit */
        return Arrays.asList(SflselEnum._CHANGE, SflselEnum._2);
    }

    public static List<SflselEnum> getErrors() { /* 20668 Errors */
        return Arrays.asList(SflselEnum._E);
    }

    public static List<SflselEnum> getExtraMidpoints() { /* 25691 Extra Midpoints */
        return Arrays.asList(SflselEnum._X);
    }

    public static List<SflselEnum> getFinancialDetails() { /* 23287 Financial Details */
        return Arrays.asList(SflselEnum._F);
    }

    public static List<SflselEnum> getFixHeldUp() { /* 65732 Fix Held Up */
        return Arrays.asList(SflselEnum._F, SflselEnum._F_NBR_2);
    }

    public static List<SflselEnum> getFolioDetails() { /* 19516 Folio Details */
        return Arrays.asList(SflselEnum._F);
    }

    public static List<SflselEnum> getFolios() { /* 20191 Folios */
        return Arrays.asList(SflselEnum._F);
    }

    public static List<SflselEnum> getFullLotting() { /* 17661 Full Lotting */
        return Arrays.asList(SflselEnum._F);
    }

    public static List<SflselEnum> getFullPartLotClip() { /* 18352 Full/Part Lot Clip */
        return Arrays.asList(SflselEnum._F, SflselEnum._P);
    }

    public static List<SflselEnum> getHeldUp() { /* 65710 Held Up */
        return Arrays.asList(SflselEnum._H);
    }

    public static List<SflselEnum> getHold() { /* 26145 Hold */
        return Arrays.asList(SflselEnum._H);
    }

    public static List<SflselEnum> getInsert() { /* 33687 Insert */
        return Arrays.asList(SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2);
    }

    public static List<SflselEnum> getInsert1a() { /* 49122 Insert (1A) */
        return Arrays.asList(SflselEnum._SELECT_NBR_1);
    }

    public static List<SflselEnum> getInterlotAnalysis() { /* 23293 Interlot Analysis */
        return Arrays.asList(SflselEnum._I);
    }

    public static List<SflselEnum> getJob() { /* 43269 Job */
        return Arrays.asList(SflselEnum._J, SflselEnum._J_NBR_2);
    }

    public static List<SflselEnum> getLines() { /* 33922 Lines */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLmi() { /* 35069 LMI */
        return Arrays.asList(SflselEnum._I);
    }

    public static List<SflselEnum> getLocation() { /* 20234 Location */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLocationItems() { /* 37271 Location Items */
        return Arrays.asList(SflselEnum._8_NBR_1, SflselEnum._8_NBR_2);
    }

    public static List<SflselEnum> getLocations() { /* 37210 Locations */
        return Arrays.asList(SflselEnum._7_NBR_1, SflselEnum._7_NBR_2);
    }

    public static List<SflselEnum> getLocationsDetails() { /* 17825 Locations Details */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLocks() { /* 37538 Locks */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLotDetails() { /* 19515 Lot Details */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLotsFolios() { /* 19521 Lots/Folios */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getLottedAnalysis() { /* 23308 Lotted Analysis */
        return Arrays.asList(SflselEnum._L);
    }

    public static List<SflselEnum> getMaintainWoolGroup() { /* 48413 Maintain Wool Group */
        return Arrays.asList(SflselEnum._W);
    }

    public static List<SflselEnum> getMakeBale() { /* 34317 Make Bale */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getMarkSheet() { /* 32699 Mark Sheet */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getMarks() { /* 37692 Marks */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getMerge() { /* 32758 Merge */
        return Arrays.asList(SflselEnum._3);
    }

    public static List<SflselEnum> getMobBreak() { /* 48792 Mob Break */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getMove() { /* 37525 Move */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getMoveToProductionChg() { /* 52847 Move to Production CHG */
        return Arrays.asList(SflselEnum._P);
    }

    public static List<SflselEnum> getMoveToTestLibrary() { /* 52846 Move to test library */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getNoSort() { /* 35072 No Sort */
        return Arrays.asList(SflselEnum._N);
    }

    public static List<SflselEnum> getOne() { /* 41619 One */
        return Arrays.asList(SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2);
    }

    public static List<SflselEnum> getPickSheet() { /* 32700 Pick Sheet */
        return Arrays.asList(SflselEnum._P, SflselEnum._P);
    }

    public static List<SflselEnum> getPickUpAdvice() { /* 50673 Pick Up Advice */
        return Arrays.asList(SflselEnum._P, SflselEnum._P);
    }

    public static List<SflselEnum> getPriceBuyerChanges() { /* 20193 Price/Buyer Changes */
        return Arrays.asList(SflselEnum._P);
    }

    public static List<SflselEnum> getPrint() { /* 32078 Print */
        return Arrays.asList(SflselEnum._P, SflselEnum._P);
    }

    public static List<SflselEnum> getPrint6() { /* 37822 Print (6) */
        return Arrays.asList(SflselEnum._6_NBR_1, SflselEnum._6_NBR_2);
    }

    public static List<SflselEnum> getQuotes() { /* 20778 Quotes */
        return Arrays.asList(SflselEnum._Q);
    }

    public static List<SflselEnum> getRUOrLRcvls() { /* 21427 R, U, OR L (RCVLS) */
        return Arrays.asList(SflselEnum._L, SflselEnum._R, SflselEnum._U);
    }

    public static List<SflselEnum> getRateSequences() { /* 19670 Rate Sequences */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getRates() { /* 19669 Rates */
        return Arrays.asList(SflselEnum._R);
    }

    public static List<SflselEnum> getReady() { /* 32186 Ready */
        return Arrays.asList(SflselEnum._R, SflselEnum._R);
    }

    public static List<SflselEnum> getReceivals() { /* 17969 Receivals */
        return Arrays.asList(SflselEnum._R, SflselEnum._R);
    }

    public static List<SflselEnum> getRegionAndEquityPrcGrps() { /* 25692 Region & Equity Prc Grps */
        return Arrays.asList(SflselEnum._A, SflselEnum._B, SflselEnum._C, SflselEnum._E, SflselEnum._N, SflselEnum._Q, SflselEnum._S, SflselEnum._W);
    }

    public static List<SflselEnum> getRehandleAnalysis() { /* 23301 Rehandle Analysis */
        return Arrays.asList(SflselEnum._R);
    }

    public static List<SflselEnum> getRejectOrAccept() { /* 57041 Reject or Accept */
        return Arrays.asList(SflselEnum._A, SflselEnum._R);
    }

    public static List<SflselEnum> getReleases() { /* 49534 Releases */
        return Arrays.asList(SflselEnum._R, SflselEnum._R);
    }

    public static List<SflselEnum> getRemove() { /* 21353 Remove */
        return Arrays.asList(SflselEnum._4);
    }

    public static List<SflselEnum> getRequest() { /* 32695 Request */
        return Arrays.asList(SflselEnum._8_NBR_1, SflselEnum._8_NBR_2);
    }

    public static List<SflselEnum> getRetry() { /* 24258 Retry */
        return Arrays.asList(SflselEnum._R);
    }

    public static List<SflselEnum> getReverseTransfer() { /* 23314 Reverse Transfer */
        return Arrays.asList(SflselEnum._R);
    }

    public static List<SflselEnum> getSaleResults() { /* 22599 Sale Results */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getSales() { /* 23708 Sales */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getSelect() { /* 36099 Select */
        return Arrays.asList(SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2);
    }

    public static List<SflselEnum> getSelectBAR() { /* 18758 Select B/A/R */
        return Arrays.asList(SflselEnum._A, SflselEnum._B, SflselEnum._R);
    }

    public static List<SflselEnum> getSelectChgDltDsp() { /* 18742 Select CHG/DLT/DSP */
        return Arrays.asList(SflselEnum._CHANGE, SflselEnum._4, SflselEnum._5_NBR_1);
    }

    public static List<SflselEnum> getSelectLots() { /* 42699 Select Lots */
        return Arrays.asList(SflselEnum._SELECT_NBR_1);
    }

    public static List<SflselEnum> getSelectRequest() { /* 15563 *Select request */
        return Arrays.asList(SflselEnum._SELECT_NBR_1, SflselEnum._STA_SELECT_NBR_2);
    }

    public static List<SflselEnum> getSelectionCharacter() { /* 15624 *Selection character */
        return Arrays.asList(SflselEnum._STA_SELECTION_CHAR_VALUE, SflselEnum._STA_SELECTION_CHAR_VALUE_NBR_2);
    }

    public static List<SflselEnum> getSendMessage() { /* 37539 Send Message */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getSendWdo() { /* 41066 Send WDO */
        return Arrays.asList(SflselEnum._CHANGE);
    }

    public static List<SflselEnum> getSoldData() { /* 26644 Sold Data */
        return Arrays.asList(SflselEnum._S, SflselEnum._U);
    }

    public static List<SflselEnum> getSort() { /* 35065 Sort */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getSplit() { /* 32701 Split */
        return Arrays.asList(SflselEnum._S);
    }

    public static List<SflselEnum> getSubmitPendingMblPrint() { /* 35156 Submit Pending MBL Print */
        return Arrays.asList(SflselEnum._M);
    }

    public static List<SflselEnum> getTableRangeRates() { /* 20646 Table Range Rates */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTestResultDetail() { /* 20235 Test Result Detail */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTestResults() { /* 19517 Test Results */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTestResultsForClip() { /* 19522 Test Results for Clip */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTestedSoldAnalysis() { /* 23317 Tested/Sold Analysis */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTextAfter() { /* 52803 Text (After) */
        return Arrays.asList(SflselEnum._A);
    }

    public static List<SflselEnum> getTextBefore() { /* 52802 Text (Before) */
        return Arrays.asList(SflselEnum._B);
    }

    public static List<SflselEnum> getTransferDetails() { /* 20007 Transfer Details */
        return Arrays.asList(SflselEnum._T);
    }

    public static List<SflselEnum> getTransfers() { /* 20190 Transfers */
        return Arrays.asList(SflselEnum._X);
    }

    public static List<SflselEnum> getUnconfirm() { /* 32710 Unconfirm */
        return Arrays.asList(SflselEnum._STA_DELETE_NBR_2, SflselEnum._4);
    }

    public static List<SflselEnum> getUnconfirmReceivalBale() { /* 41458 Unconfirm Receival Bale */
        return Arrays.asList(SflselEnum._U);
    }

    public static List<SflselEnum> getUnlottedBale() { /* 35088 Unlotted Bale */
        return Arrays.asList(SflselEnum._U);
    }

    public static List<SflselEnum> getUnsoldData() { /* 26645 Unsold Data */
        return Arrays.asList(SflselEnum._U);
    }

    public static List<SflselEnum> getUnsort() { /* 35109 UnSort */
        return Arrays.asList(SflselEnum._U);
    }

    public static List<SflselEnum> getUpdateRecordChgDlt() { /* 18073 Update Record - CHG/DLT */
        return Arrays.asList(SflselEnum._CHANGE, SflselEnum._4);
    }

    public static List<SflselEnum> getUsage() { /* 45380 Usage */
        return Arrays.asList(SflselEnum._U, SflselEnum._U_NBR_2);
    }

    public static List<SflselEnum> getValidSystemValues() { /* 21544 Valid System values */
        return Arrays.asList(SflselEnum._NOT_ENTERED, SflselEnum._P);
    }

    public static List<SflselEnum> getVoyages() { /* 40427 Voyages */
        return Arrays.asList(SflselEnum._V, SflselEnum._V);
    }

    public static List<SflselEnum> getWipUpdateOption() { /* 43789 WIP Update Option */
        return Arrays.asList(SflselEnum._A, SflselEnum._C, SflselEnum._CHANGE, SflselEnum._D, SflselEnum._E, SflselEnum._G, SflselEnum._I, SflselEnum._Q, SflselEnum._R, SflselEnum._S, SflselEnum._SELECT_NBR_1, SflselEnum._V, SflselEnum._W, SflselEnum._X, SflselEnum._2);
    }

    public static List<SflselEnum> getX8() { /* 42271 8 */
        return Arrays.asList(SflselEnum._8_NBR_1, SflselEnum._8_NBR_2);
    }

    public static List<SflselEnum> getX9Request() { /* 31969 9 request */
        return Arrays.asList(SflselEnum._9, SflselEnum._9);
    }

    public static List<SflselEnum> getZoomRequest() { /* 15599 *Zoom request */
        return Arrays.asList(SflselEnum._STA_ZOOM, SflselEnum._STA_ZOOM_NBR_2);
    }

    /**
     * Check
     */

    public static boolean isAddDiscrepancy(SflselEnum sflsel) {  /* 65687 Add Discrepancy */
        return SflselEnum.getAddDiscrepancy().contains(sflsel);
    }

    public static boolean isAddole(SflselEnum sflsel) {  /* 52873 ADDOLE */
        return SflselEnum.getAddole().contains(sflsel);
    }

    public static boolean isAliases(SflselEnum sflsel) {  /* 40852 Aliases */
        return SflselEnum.getAliases().contains(sflsel);
    }

    public static boolean isAllValues(SflselEnum sflsel) {  /* 15481 *ALL values */
        return SflselEnum.getAllValues().contains(sflsel);
    }

    public static boolean isAllocate(SflselEnum sflsel) {  /* 21591 Allocate */
        return SflselEnum.getAllocate().contains(sflsel);
    }

    public static boolean isAllowedField(SflselEnum sflsel) {  /* 39511 Allowed Field */
        return SflselEnum.getAllowedField().contains(sflsel);
    }

    public static boolean isAppraisals(SflselEnum sflsel) {  /* 26604 Appraisals */
        return SflselEnum.getAppraisals().contains(sflsel);
    }

    public static boolean isAssignClientId(SflselEnum sflsel) {  /* 50241 Assign Client Id */
        return SflselEnum.getAssignClientId().contains(sflsel);
    }

    public static boolean isAssignToRelease(SflselEnum sflsel) {  /* 49644 Assign to Release */
        return SflselEnum.getAssignToRelease().contains(sflsel);
    }

    public static boolean isAwtaDetails(SflselEnum sflsel) {  /* 64833 AWTA Details */
        return SflselEnum.getAwtaDetails().contains(sflsel);
    }

    public static boolean isBagDetails(SflselEnum sflsel) {  /* 17832 Bag Details */
        return SflselEnum.getBagDetails().contains(sflsel);
    }

    public static boolean isBaleDetails(SflselEnum sflsel) {  /* 17831 Bale Details */
        return SflselEnum.getBaleDetails().contains(sflsel);
    }

    public static boolean isBales(SflselEnum sflsel) {  /* 19514 Bales */
        return SflselEnum.getBales().contains(sflsel);
    }

    public static boolean isBeforeOrAfter(SflselEnum sflsel) {  /* 18879 Before or After */
        return SflselEnum.getBeforeOrAfter().contains(sflsel);
    }

    public static boolean isBinSplits(SflselEnum sflsel) {  /* 19579 Bin Splits */
        return SflselEnum.getBinSplits().contains(sflsel);
    }

    public static boolean isBulkClass(SflselEnum sflsel) {  /* 19523 Bulk Class */
        return SflselEnum.getBulkClass().contains(sflsel);
    }

    public static boolean isCarrierDetails(SflselEnum sflsel) {  /* 17830 Carrier Details */
        return SflselEnum.getCarrierDetails().contains(sflsel);
    }

    public static boolean isCatalogueSymbols(SflselEnum sflsel) {  /* 20192 Catalogue Symbols */
        return SflselEnum.getCatalogueSymbols().contains(sflsel);
    }

    public static boolean isChangeOrDelete(SflselEnum sflsel) {  /* 19825 Change or Delete */
        return SflselEnum.getChangeOrDelete().contains(sflsel);
    }

    public static boolean isChargeRateSequences(SflselEnum sflsel) {  /* 42329 Charge Rate Sequences */
        return SflselEnum.getChargeRateSequences().contains(sflsel);
    }

    public static boolean isClientDetail(SflselEnum sflsel) {  /* 20236 Client Detail */
        return SflselEnum.getClientDetail().contains(sflsel);
    }

    public static boolean isClipActivity(SflselEnum sflsel) {  /* 26956 Clip Activity */
        return SflselEnum.getClipActivity().contains(sflsel);
    }

    public static boolean isCollationDetails(SflselEnum sflsel) {  /* 52817 Collation Details */
        return SflselEnum.getCollationDetails().contains(sflsel);
    }

    public static boolean isComments(SflselEnum sflsel) {  /* 37691 Comments */
        return SflselEnum.getComments().contains(sflsel);
    }

    public static boolean isComplete(SflselEnum sflsel) {  /* 32181 Complete */
        return SflselEnum.getComplete().contains(sflsel);
    }

    public static boolean isConfirm(SflselEnum sflsel) {  /* 32711 Confirm */
        return SflselEnum.getConfirm().contains(sflsel);
    }

    public static boolean isConfirmReceivalBale(SflselEnum sflsel) {  /* 41457 Confirm Receival Bale */
        return SflselEnum.getConfirmReceivalBale().contains(sflsel);
    }

    public static boolean isConfirmSheets(SflselEnum sflsel) {  /* 32709 Confirm Sheets */
        return SflselEnum.getConfirmSheets().contains(sflsel);
    }

    public static boolean isConsignment(SflselEnum sflsel) {  /* 37693 Consignment */
        return SflselEnum.getConsignment().contains(sflsel);
    }

    public static boolean isContainers(SflselEnum sflsel) {  /* 49533 Containers */
        return SflselEnum.getContainers().contains(sflsel);
    }

    public static boolean isCopy(SflselEnum sflsel) {  /* 37976 Copy */
        return SflselEnum.getCopy().contains(sflsel);
    }

    public static boolean isDeAllocate(SflselEnum sflsel) {  /* 44152 De-Allocate */
        return SflselEnum.getDeAllocate().contains(sflsel);
    }

    public static boolean isDelete(SflselEnum sflsel) {  /* 32698 Delete */
        return SflselEnum.getDelete().contains(sflsel);
    }

    public static boolean isDeleteRequest(SflselEnum sflsel) {  /* 15564 *Delete request */
        return SflselEnum.getDeleteRequest().contains(sflsel);
    }

    public static boolean isDescription_(SflselEnum sflsel) {  /* 34323 Description */
        return SflselEnum.getDescription_().contains(sflsel);
    }

    public static boolean isDiscountRates(SflselEnum sflsel) {  /* 24378 Discount Rates */
        return SflselEnum.getDiscountRates().contains(sflsel);
    }

    public static boolean isDiscrepancies(SflselEnum sflsel) {  /* 17658 Discrepancies */
        return SflselEnum.getDiscrepancies().contains(sflsel);
    }

    public static boolean isDiscrepancy(SflselEnum sflsel) {  /* 65592 Discrepancy */
        return SflselEnum.getDiscrepancy().contains(sflsel);
    }

    public static boolean isDisplay(SflselEnum sflsel) {  /* 33470 Display */
        return SflselEnum.getDisplay().contains(sflsel);
    }

    public static boolean isDisplayAudit(SflselEnum sflsel) {  /* 35498 Display Audit */
        return SflselEnum.getDisplayAudit().contains(sflsel);
    }

    public static boolean isDisplayDocument(SflselEnum sflsel) {  /* 40164 Display Document */
        return SflselEnum.getDisplayDocument().contains(sflsel);
    }

    public static boolean isDisplayMessages(SflselEnum sflsel) {  /* 31933 Display Messages */
        return SflselEnum.getDisplayMessages().contains(sflsel);
    }

    public static boolean isDnpxOrBlank(SflselEnum sflsel) {  /* 37679 D,N,P,X or Blank */
        return SflselEnum.getDnpxOrBlank().contains(sflsel);
    }

    public static boolean isDoNotLotDetails(SflselEnum sflsel) {  /* 19731 Do not Lot Details */
        return SflselEnum.getDoNotLotDetails().contains(sflsel);
    }

    public static boolean isDocumentDistribution(SflselEnum sflsel) {  /* 40881 Document Distribution */
        return SflselEnum.getDocumentDistribution().contains(sflsel);
    }

    public static boolean isDocuments(SflselEnum sflsel) {  /* 45720 Documents */
        return SflselEnum.getDocuments().contains(sflsel);
    }

    public static boolean isDropReinstate(SflselEnum sflsel) {  /* 35276 Drop/Reinstate */
        return SflselEnum.getDropReinstate().contains(sflsel);
    }

    public static boolean isEdit(SflselEnum sflsel) {  /* 32110 Edit */
        return SflselEnum.getEdit().contains(sflsel);
    }

    public static boolean isErrors(SflselEnum sflsel) {  /* 20668 Errors */
        return SflselEnum.getErrors().contains(sflsel);
    }

    public static boolean isExtraMidpoints(SflselEnum sflsel) {  /* 25691 Extra Midpoints */
        return SflselEnum.getExtraMidpoints().contains(sflsel);
    }

    public static boolean isFinancialDetails(SflselEnum sflsel) {  /* 23287 Financial Details */
        return SflselEnum.getFinancialDetails().contains(sflsel);
    }

    public static boolean isFixHeldUp(SflselEnum sflsel) {  /* 65732 Fix Held Up */
        return SflselEnum.getFixHeldUp().contains(sflsel);
    }

    public static boolean isFolioDetails(SflselEnum sflsel) {  /* 19516 Folio Details */
        return SflselEnum.getFolioDetails().contains(sflsel);
    }

    public static boolean isFolios(SflselEnum sflsel) {  /* 20191 Folios */
        return SflselEnum.getFolios().contains(sflsel);
    }

    public static boolean isFullLotting(SflselEnum sflsel) {  /* 17661 Full Lotting */
        return SflselEnum.getFullLotting().contains(sflsel);
    }

    public static boolean isFullPartLotClip(SflselEnum sflsel) {  /* 18352 Full/Part Lot Clip */
        return SflselEnum.getFullPartLotClip().contains(sflsel);
    }

    public static boolean isHeldUp(SflselEnum sflsel) {  /* 65710 Held Up */
        return SflselEnum.getHeldUp().contains(sflsel);
    }

    public static boolean isHold(SflselEnum sflsel) {  /* 26145 Hold */
        return SflselEnum.getHold().contains(sflsel);
    }

    public static boolean isInsert(SflselEnum sflsel) {  /* 33687 Insert */
        return SflselEnum.getInsert().contains(sflsel);
    }

    public static boolean isInsert1a(SflselEnum sflsel) {  /* 49122 Insert (1A) */
        return SflselEnum.getInsert1a().contains(sflsel);
    }

    public static boolean isInterlotAnalysis(SflselEnum sflsel) {  /* 23293 Interlot Analysis */
        return SflselEnum.getInterlotAnalysis().contains(sflsel);
    }

    public static boolean isJob(SflselEnum sflsel) {  /* 43269 Job */
        return SflselEnum.getJob().contains(sflsel);
    }

    public static boolean isLines(SflselEnum sflsel) {  /* 33922 Lines */
        return SflselEnum.getLines().contains(sflsel);
    }

    public static boolean isLmi(SflselEnum sflsel) {  /* 35069 LMI */
        return SflselEnum.getLmi().contains(sflsel);
    }

    public static boolean isLocation(SflselEnum sflsel) {  /* 20234 Location */
        return SflselEnum.getLocation().contains(sflsel);
    }

    public static boolean isLocationItems(SflselEnum sflsel) {  /* 37271 Location Items */
        return SflselEnum.getLocationItems().contains(sflsel);
    }

    public static boolean isLocations(SflselEnum sflsel) {  /* 37210 Locations */
        return SflselEnum.getLocations().contains(sflsel);
    }

    public static boolean isLocationsDetails(SflselEnum sflsel) {  /* 17825 Locations Details */
        return SflselEnum.getLocationsDetails().contains(sflsel);
    }

    public static boolean isLocks(SflselEnum sflsel) {  /* 37538 Locks */
        return SflselEnum.getLocks().contains(sflsel);
    }

    public static boolean isLotDetails(SflselEnum sflsel) {  /* 19515 Lot Details */
        return SflselEnum.getLotDetails().contains(sflsel);
    }

    public static boolean isLotsFolios(SflselEnum sflsel) {  /* 19521 Lots/Folios */
        return SflselEnum.getLotsFolios().contains(sflsel);
    }

    public static boolean isLottedAnalysis(SflselEnum sflsel) {  /* 23308 Lotted Analysis */
        return SflselEnum.getLottedAnalysis().contains(sflsel);
    }

    public static boolean isMaintainWoolGroup(SflselEnum sflsel) {  /* 48413 Maintain Wool Group */
        return SflselEnum.getMaintainWoolGroup().contains(sflsel);
    }

    public static boolean isMakeBale(SflselEnum sflsel) {  /* 34317 Make Bale */
        return SflselEnum.getMakeBale().contains(sflsel);
    }

    public static boolean isMarkSheet(SflselEnum sflsel) {  /* 32699 Mark Sheet */
        return SflselEnum.getMarkSheet().contains(sflsel);
    }

    public static boolean isMarks(SflselEnum sflsel) {  /* 37692 Marks */
        return SflselEnum.getMarks().contains(sflsel);
    }

    public static boolean isMerge(SflselEnum sflsel) {  /* 32758 Merge */
        return SflselEnum.getMerge().contains(sflsel);
    }

    public static boolean isMobBreak(SflselEnum sflsel) {  /* 48792 Mob Break */
        return SflselEnum.getMobBreak().contains(sflsel);
    }

    public static boolean isMove(SflselEnum sflsel) {  /* 37525 Move */
        return SflselEnum.getMove().contains(sflsel);
    }

    public static boolean isMoveToProductionChg(SflselEnum sflsel) {  /* 52847 Move to Production CHG */
        return SflselEnum.getMoveToProductionChg().contains(sflsel);
    }

    public static boolean isMoveToTestLibrary(SflselEnum sflsel) {  /* 52846 Move to test library */
        return SflselEnum.getMoveToTestLibrary().contains(sflsel);
    }

    public static boolean isNoSort(SflselEnum sflsel) {  /* 35072 No Sort */
        return SflselEnum.getNoSort().contains(sflsel);
    }

    public static boolean isOne(SflselEnum sflsel) {  /* 41619 One */
        return SflselEnum.getOne().contains(sflsel);
    }

    public static boolean isPickSheet(SflselEnum sflsel) {  /* 32700 Pick Sheet */
        return SflselEnum.getPickSheet().contains(sflsel);
    }

    public static boolean isPickUpAdvice(SflselEnum sflsel) {  /* 50673 Pick Up Advice */
        return SflselEnum.getPickUpAdvice().contains(sflsel);
    }

    public static boolean isPriceBuyerChanges(SflselEnum sflsel) {  /* 20193 Price/Buyer Changes */
        return SflselEnum.getPriceBuyerChanges().contains(sflsel);
    }

    public static boolean isPrint(SflselEnum sflsel) {  /* 32078 Print */
        return SflselEnum.getPrint().contains(sflsel);
    }

    public static boolean isPrint6(SflselEnum sflsel) {  /* 37822 Print (6) */
        return SflselEnum.getPrint6().contains(sflsel);
    }

    public static boolean isQuotes(SflselEnum sflsel) {  /* 20778 Quotes */
        return SflselEnum.getQuotes().contains(sflsel);
    }

    public static boolean isRUOrLRcvls(SflselEnum sflsel) {  /* 21427 R, U, OR L (RCVLS) */
        return SflselEnum.getRUOrLRcvls().contains(sflsel);
    }

    public static boolean isRateSequences(SflselEnum sflsel) {  /* 19670 Rate Sequences */
        return SflselEnum.getRateSequences().contains(sflsel);
    }

    public static boolean isRates(SflselEnum sflsel) {  /* 19669 Rates */
        return SflselEnum.getRates().contains(sflsel);
    }

    public static boolean isReady(SflselEnum sflsel) {  /* 32186 Ready */
        return SflselEnum.getReady().contains(sflsel);
    }

    public static boolean isReceivals(SflselEnum sflsel) {  /* 17969 Receivals */
        return SflselEnum.getReceivals().contains(sflsel);
    }

    public static boolean isRegionAndEquityPrcGrps(SflselEnum sflsel) {  /* 25692 Region & Equity Prc Grps */
        return SflselEnum.getRegionAndEquityPrcGrps().contains(sflsel);
    }

    public static boolean isRehandleAnalysis(SflselEnum sflsel) {  /* 23301 Rehandle Analysis */
        return SflselEnum.getRehandleAnalysis().contains(sflsel);
    }

    public static boolean isRejectOrAccept(SflselEnum sflsel) {  /* 57041 Reject or Accept */
        return SflselEnum.getRejectOrAccept().contains(sflsel);
    }

    public static boolean isReleases(SflselEnum sflsel) {  /* 49534 Releases */
        return SflselEnum.getReleases().contains(sflsel);
    }

    public static boolean isRemove(SflselEnum sflsel) {  /* 21353 Remove */
        return SflselEnum.getRemove().contains(sflsel);
    }

    public static boolean isRequest(SflselEnum sflsel) {  /* 32695 Request */
        return SflselEnum.getRequest().contains(sflsel);
    }

    public static boolean isRetry(SflselEnum sflsel) {  /* 24258 Retry */
        return SflselEnum.getRetry().contains(sflsel);
    }

    public static boolean isReverseTransfer(SflselEnum sflsel) {  /* 23314 Reverse Transfer */
        return SflselEnum.getReverseTransfer().contains(sflsel);
    }

    public static boolean isSaleResults(SflselEnum sflsel) {  /* 22599 Sale Results */
        return SflselEnum.getSaleResults().contains(sflsel);
    }

    public static boolean isSales(SflselEnum sflsel) {  /* 23708 Sales */
        return SflselEnum.getSales().contains(sflsel);
    }

    public static boolean isSelect(SflselEnum sflsel) {  /* 36099 Select */
        return SflselEnum.getSelect().contains(sflsel);
    }

    public static boolean isSelectBAR(SflselEnum sflsel) {  /* 18758 Select B/A/R */
        return SflselEnum.getSelectBAR().contains(sflsel);
    }

    public static boolean isSelectChgDltDsp(SflselEnum sflsel) {  /* 18742 Select CHG/DLT/DSP */
        return SflselEnum.getSelectChgDltDsp().contains(sflsel);
    }

    public static boolean isSelectLots(SflselEnum sflsel) {  /* 42699 Select Lots */
        return SflselEnum.getSelectLots().contains(sflsel);
    }

    public static boolean isSelectRequest(SflselEnum sflsel) {  /* 15563 *Select request */
        return SflselEnum.getSelectRequest().contains(sflsel);
    }

    public static boolean isSelectionCharacter(SflselEnum sflsel) {  /* 15624 *Selection character */
        return SflselEnum.getSelectionCharacter().contains(sflsel);
    }

    public static boolean isSendMessage(SflselEnum sflsel) {  /* 37539 Send Message */
        return SflselEnum.getSendMessage().contains(sflsel);
    }

    public static boolean isSendWdo(SflselEnum sflsel) {  /* 41066 Send WDO */
        return SflselEnum.getSendWdo().contains(sflsel);
    }

    public static boolean isSoldData(SflselEnum sflsel) {  /* 26644 Sold Data */
        return SflselEnum.getSoldData().contains(sflsel);
    }

    public static boolean isSort(SflselEnum sflsel) {  /* 35065 Sort */
        return SflselEnum.getSort().contains(sflsel);
    }

    public static boolean isSplit(SflselEnum sflsel) {  /* 32701 Split */
        return SflselEnum.getSplit().contains(sflsel);
    }

    public static boolean isSubmitPendingMblPrint(SflselEnum sflsel) {  /* 35156 Submit Pending MBL Print */
        return SflselEnum.getSubmitPendingMblPrint().contains(sflsel);
    }

    public static boolean isTableRangeRates(SflselEnum sflsel) {  /* 20646 Table Range Rates */
        return SflselEnum.getTableRangeRates().contains(sflsel);
    }

    public static boolean isTestResultDetail(SflselEnum sflsel) {  /* 20235 Test Result Detail */
        return SflselEnum.getTestResultDetail().contains(sflsel);
    }

    public static boolean isTestResults(SflselEnum sflsel) {  /* 19517 Test Results */
        return SflselEnum.getTestResults().contains(sflsel);
    }

    public static boolean isTestResultsForClip(SflselEnum sflsel) {  /* 19522 Test Results for Clip */
        return SflselEnum.getTestResultsForClip().contains(sflsel);
    }

    public static boolean isTestedSoldAnalysis(SflselEnum sflsel) {  /* 23317 Tested/Sold Analysis */
        return SflselEnum.getTestedSoldAnalysis().contains(sflsel);
    }

    public static boolean isTextAfter(SflselEnum sflsel) {  /* 52803 Text (After) */
        return SflselEnum.getTextAfter().contains(sflsel);
    }

    public static boolean isTextBefore(SflselEnum sflsel) {  /* 52802 Text (Before) */
        return SflselEnum.getTextBefore().contains(sflsel);
    }

    public static boolean isTransferDetails(SflselEnum sflsel) {  /* 20007 Transfer Details */
        return SflselEnum.getTransferDetails().contains(sflsel);
    }

    public static boolean isTransfers(SflselEnum sflsel) {  /* 20190 Transfers */
        return SflselEnum.getTransfers().contains(sflsel);
    }

    public static boolean isUnconfirm(SflselEnum sflsel) {  /* 32710 Unconfirm */
        return SflselEnum.getUnconfirm().contains(sflsel);
    }

    public static boolean isUnconfirmReceivalBale(SflselEnum sflsel) {  /* 41458 Unconfirm Receival Bale */
        return SflselEnum.getUnconfirmReceivalBale().contains(sflsel);
    }

    public static boolean isUnlottedBale(SflselEnum sflsel) {  /* 35088 Unlotted Bale */
        return SflselEnum.getUnlottedBale().contains(sflsel);
    }

    public static boolean isUnsoldData(SflselEnum sflsel) {  /* 26645 Unsold Data */
        return SflselEnum.getUnsoldData().contains(sflsel);
    }

    public static boolean isUnsort(SflselEnum sflsel) {  /* 35109 UnSort */
        return SflselEnum.getUnsort().contains(sflsel);
    }

    public static boolean isUpdateRecordChgDlt(SflselEnum sflsel) {  /* 18073 Update Record - CHG/DLT */
        return SflselEnum.getUpdateRecordChgDlt().contains(sflsel);
    }

    public static boolean isUsage(SflselEnum sflsel) {  /* 45380 Usage */
        return SflselEnum.getUsage().contains(sflsel);
    }

    public static boolean isValidSystemValues(SflselEnum sflsel) {  /* 21544 Valid System values */
        return SflselEnum.getValidSystemValues().contains(sflsel);
    }

    public static boolean isVoyages(SflselEnum sflsel) {  /* 40427 Voyages */
        return SflselEnum.getVoyages().contains(sflsel);
    }

    public static boolean isWipUpdateOption(SflselEnum sflsel) {  /* 43789 WIP Update Option */
        return SflselEnum.getWipUpdateOption().contains(sflsel);
    }

    public static boolean isX8(SflselEnum sflsel) {  /* 42271 8 */
        return SflselEnum.getX8().contains(sflsel);
    }

    public static boolean isX9Request(SflselEnum sflsel) {  /* 31969 9 request */
        return SflselEnum.getX9Request().contains(sflsel);
    }

    public static boolean isZoomRequest(SflselEnum sflsel) {  /* 15599 *Zoom request */
        return SflselEnum.getZoomRequest().contains(sflsel);
    }
}