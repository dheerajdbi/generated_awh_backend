package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ValidationOkEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ValidationOkConverter implements AttributeConverter<ValidationOkEnum, String> {

	@Override
	public String convertToDatabaseColumn(ValidationOkEnum validationOk) {
		if (validationOk == null) {
			return "";
		}

		return validationOk.getCode();
	}

	@Override
	public ValidationOkEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ValidationOkEnum.fromCode("");
		}

		return ValidationOkEnum.fromCode(StringUtils.strip(dbData));
	}
}
