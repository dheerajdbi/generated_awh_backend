package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for CentreType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CentreTypeEnum {  // 368 STS CTRTYP Centre Type

	_AUCTION_CENTRE("A", "Auction Centre") /* 16271 */ ,
	_SELLING_CENTRE("G", "Selling Centre") /* 18236 */ ,
	_STORAGE_CENTRE("S", "Storage Centre") /* 16272 */ ;
    
	private final String code;
	private final String description;

	CentreTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CentreTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CentreTypeEnum> getAllValues() { /* 16261 *ALL values */
        return Arrays.asList(CentreTypeEnum._AUCTION_CENTRE, CentreTypeEnum._SELLING_CENTRE, CentreTypeEnum._STORAGE_CENTRE);
    }

    public static List<CentreTypeEnum> getAuctionSelling() { /* 18743 Auction/Selling */
        return Arrays.asList(CentreTypeEnum._AUCTION_CENTRE, CentreTypeEnum._SELLING_CENTRE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(CentreTypeEnum centreType) {  /* 16261 *ALL values */
        return CentreTypeEnum.getAllValues().contains(centreType);
    }

    public static boolean isAuctionSelling(CentreTypeEnum centreType) {  /* 18743 Auction/Selling */
        return CentreTypeEnum.getAuctionSelling().contains(centreType);
    }
}