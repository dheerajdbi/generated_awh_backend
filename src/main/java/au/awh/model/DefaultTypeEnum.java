package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DefaultType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DefaultTypeEnum {  // 8068 STS DFTYP Default Type

	_BC_CENTRE_TRANSFER("BC", "BC Centre Transfer") /* 38132 */ ,
	_BUYER_PAYMENT("BP", "Buyer Payment") /* 38335 */ ,
	_BSC_REGION_FSL_GROUP("BR", "BSC Region/Group") /* 57374 */ ,
	_BSC_DEFAULTS("BS", "BSC Defaults") /* 46418 */ ,
	_CENTRE_BROKER_DEFAULTS("CB", "Centre Broker Defaults") /* 47701 */ ,
	_CLIENT_COMMODITY_GRADE("CC", "Client Commodity Grade") /* 63731 */ ,
	_DUMP_CLIENT("CD", "Dump Client") /* 39340 */ ,
	_CORE_LINE("CL", "Core Line") /* 35771 */ ,
	_COTTON_ORGANISATION("CO", "Cotton Organisation") /* 50533 */ ,
	_CENTRE_FSL_STORE("CS", "Centre/Store") /* 61667 */ ,
	_CURRENT_TRANSMITTER("CT", "Current Transmitter") /* 38962 */ ,
	_CONTAINER_DEPOT_CLIENT("CX", "Container Depot Client") /* 49430 */ ,
	_DUMP_DEPOT("D1", "Dump Depot") /* 49871 */ ,
	_DOCUMENT_TYPE_CONNECTION("DC", "Document Type Connection") /* 39120 */ ,
	_DOCUMENT_TYPE_DISTR("DD", "Document Type Distr") /* 40590 */ ,
	_DELIVERY("DL", "Delivery") /* 36171 */ ,
	_DUMP("DM", "Dump") /* 39580 */ ,
	_DUMP_ORGANISATION("DO", "Dump Organisation") /* 48334 */ ,
	_DUMP_DEPARTURE_PORT("DP", "Dump Departure Port") /* 40116 */ ,
	_DOC_TYPE_ORG_RECIPIENT("DR", "Doc Type Org Recipient") /* 39739 */ ,
	_DUMP_STORAGE_ORGANISATION("DS", "Dump Storage Organisation") /* 39956 */ ,
	_DOCO_ORG_CENTRE("DX", "Doco Org Centre") /* 48577 */ ,
	_GL_ACCOUNTS_1("G1", "GL Accounts 1") /* 38653 */ ,
	_GL_ACCOUNTS("GL", "GL Accounts") /* 38640 */ ,
	_GRAIN_ORGANISATION("GR", "Grain Organisation") /* 56079 */ ,
	_GRAB_SAMPLE("GS", "Grab Sample") /* 37567 */ ,
	_LMI("LM", "LMI") /* 37852 */ ,
	_DUMP_MARINE_TERMINAL("MT", "Dump Marine Terminal") /* 59847 */ ,
	_DUMP_STORAGE_ORG_CLIENT("OC", "Dump Storage Org Client") /* 39981 */ ,
	_PRICE_GROUP_ALIAS("PG", "Price Group Alias") /* 51304 */ ,
	_DUMP_REPORT("RD", "Dump Report") /* 45302 */ ,
	_DUMP_SHIPPING_COY("SC", "Dump Shipping Coy") /* 44648 */ ,
	_SHOW_FLOOR("SF", "Show Floor") /* 37448 */ ,
	_SAMPLE_PROCEEDS_REPORT("SP", "Sample Proceeds Report") /* 46518 */ ,
	_STORAGE("ST", "Storage") /* 36050 */ ,
	_SYSTEM_LEVEL("SY", "System Level") /* 39688 */ ,
	_USER("UR", "User") /* 36172 */ ,
	_WOOL_LEVY("WL", "Wool Levy") /* 37132 */ ,
	_WOOL_SYSTEM_REPORT("WR", "Wool System Report") /* 46422 */ ,
	_WOOL_SYSTEM_EXTRACTS("WS", "Wool System Extracts") /* 56737 */ ,
	_CLIENT_DUMP("XD", "Client Dump") /* 52726 */ ;
    
	private final String code;
	private final String description;

	DefaultTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DefaultTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DefaultTypeEnum> getAllValues() { /* 35772 *ALL values */
        return Arrays.asList(DefaultTypeEnum._BC_CENTRE_TRANSFER, DefaultTypeEnum._BSC_DEFAULTS, DefaultTypeEnum._BSC_REGION_FSL_GROUP, DefaultTypeEnum._BUYER_PAYMENT, DefaultTypeEnum._CENTRE_BROKER_DEFAULTS, DefaultTypeEnum._CENTRE_FSL_STORE, DefaultTypeEnum._CLIENT_COMMODITY_GRADE, DefaultTypeEnum._CLIENT_DUMP, DefaultTypeEnum._CONTAINER_DEPOT_CLIENT, DefaultTypeEnum._CORE_LINE, DefaultTypeEnum._COTTON_ORGANISATION, DefaultTypeEnum._CURRENT_TRANSMITTER, DefaultTypeEnum._DELIVERY, DefaultTypeEnum._DOC_TYPE_ORG_RECIPIENT, DefaultTypeEnum._DOCO_ORG_CENTRE, DefaultTypeEnum._DOCUMENT_TYPE_CONNECTION, DefaultTypeEnum._DOCUMENT_TYPE_DISTR, DefaultTypeEnum._DUMP, DefaultTypeEnum._DUMP_CLIENT, DefaultTypeEnum._DUMP_DEPARTURE_PORT, DefaultTypeEnum._DUMP_DEPOT, DefaultTypeEnum._DUMP_MARINE_TERMINAL, DefaultTypeEnum._DUMP_ORGANISATION, DefaultTypeEnum._DUMP_REPORT, DefaultTypeEnum._DUMP_SHIPPING_COY, DefaultTypeEnum._DUMP_STORAGE_ORG_CLIENT, DefaultTypeEnum._DUMP_STORAGE_ORGANISATION, DefaultTypeEnum._GL_ACCOUNTS, DefaultTypeEnum._GL_ACCOUNTS_1, DefaultTypeEnum._GRAB_SAMPLE, DefaultTypeEnum._GRAIN_ORGANISATION, DefaultTypeEnum._LMI, DefaultTypeEnum._PRICE_GROUP_ALIAS, DefaultTypeEnum._SAMPLE_PROCEEDS_REPORT, DefaultTypeEnum._SHOW_FLOOR, DefaultTypeEnum._STORAGE, DefaultTypeEnum._SYSTEM_LEVEL, DefaultTypeEnum._USER, DefaultTypeEnum._WOOL_LEVY, DefaultTypeEnum._WOOL_SYSTEM_EXTRACTS, DefaultTypeEnum._WOOL_SYSTEM_REPORT);
    }

    public static List<DefaultTypeEnum> getAllowKey2AsWild() { /* 44351 Allow Key 2 as Wild */
        return Arrays.asList(DefaultTypeEnum._DOCUMENT_TYPE_DISTR, DefaultTypeEnum._DUMP_STORAGE_ORG_CLIENT);
    }

    public static List<DefaultTypeEnum> getAllowKey3AsWild() { /* 44350 Allow Key 3 as Wild */
        return Arrays.asList(DefaultTypeEnum._DOCUMENT_TYPE_DISTR);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DefaultTypeEnum defaultType) {  /* 35772 *ALL values */
        return DefaultTypeEnum.getAllValues().contains(defaultType);
    }

    public static boolean isAllowKey2AsWild(DefaultTypeEnum defaultType) {  /* 44351 Allow Key 2 as Wild */
        return DefaultTypeEnum.getAllowKey2AsWild().contains(defaultType);
    }

    public static boolean isAllowKey3AsWild(DefaultTypeEnum defaultType) {  /* 44350 Allow Key 3 as Wild */
        return DefaultTypeEnum.getAllowKey3AsWild().contains(defaultType);
    }
}