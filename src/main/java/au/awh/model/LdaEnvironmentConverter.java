package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for LdaEnvironmentEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class LdaEnvironmentConverter implements AttributeConverter<LdaEnvironmentEnum, String> {

	@Override
	public String convertToDatabaseColumn(LdaEnvironmentEnum ldaEnvironment) {
		if (ldaEnvironment == null) {
			return "";
		}

		return ldaEnvironment.getCode();
	}

	@Override
	public LdaEnvironmentEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return LdaEnvironmentEnum.fromCode("");
		}

		return LdaEnvironmentEnum.fromCode(StringUtils.strip(dbData));
	}
}
