package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for RecordDataChanged.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum RecordDataChangedEnum {  // 179 STS RDC *Record Data Changed

	_STA_NO("N", "*NO") /* 16029 */ ,
	_STA_YES("Y", "*YES") /* 16027 */ ;
    
	private final String code;
	private final String description;

	RecordDataChangedEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static RecordDataChangedEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<RecordDataChangedEnum> getAllValues() { /* 16028 *ALL values */
        return Arrays.asList(RecordDataChangedEnum._STA_NO, RecordDataChangedEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(RecordDataChangedEnum recordDataChanged) {  /* 16028 *ALL values */
        return RecordDataChangedEnum.getAllValues().contains(recordDataChanged);
    }
}