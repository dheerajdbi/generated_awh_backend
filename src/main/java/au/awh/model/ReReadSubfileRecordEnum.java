package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReReadSubfileRecord.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReReadSubfileRecordEnum {  // 174 STS SNC *Re-read Subfile Record

	_STA_NO("0", "*NO") /* 15987 */ ,
	_STA_YES("1", "*YES") /* 15985 */ ;
    
	private final String code;
	private final String description;

	ReReadSubfileRecordEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReReadSubfileRecordEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReReadSubfileRecordEnum> getAllValues() { /* 15986 *ALL values */
        return Arrays.asList(ReReadSubfileRecordEnum._STA_NO, ReReadSubfileRecordEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReReadSubfileRecordEnum reReadSubfileRecord) {  /* 15986 *ALL values */
        return ReReadSubfileRecordEnum.getAllValues().contains(reReadSubfileRecord);
    }
}