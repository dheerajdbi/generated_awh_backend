package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReadingTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReadingTypeConverter implements AttributeConverter<ReadingTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReadingTypeEnum readingType) {
		if (readingType == null) {
			return "";
		}

		return readingType.getCode();
	}

	@Override
	public ReadingTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReadingTypeEnum.fromCode("");
		}

		return ReadingTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
