package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DefaultTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DefaultTypeConverter implements AttributeConverter<DefaultTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(DefaultTypeEnum defaultType) {
		if (defaultType == null) {
			return "";
		}

		return defaultType.getCode();
	}

	@Override
	public DefaultTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DefaultTypeEnum.fromCode("");
		}

		return DefaultTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
