package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for EquipReadingUnitEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class EquipReadingUnitConverter implements AttributeConverter<EquipReadingUnitEnum, String> {

	@Override
	public String convertToDatabaseColumn(EquipReadingUnitEnum equipReadingUnit) {
		if (equipReadingUnit == null) {
			return "";
		}

		return equipReadingUnit.getCode();
	}

	@Override
	public EquipReadingUnitEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return EquipReadingUnitEnum.fromCode("");
		}

		return EquipReadingUnitEnum.fromCode(StringUtils.strip(dbData));
	}
}
