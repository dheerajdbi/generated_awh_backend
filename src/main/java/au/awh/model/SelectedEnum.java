package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Selected.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SelectedEnum {  // 4406 STS SELD Selected

	_NOT_SELECTED("", "Not Selected") /* 25532 */ ,
	_SELECTED("1", "Selected") /* 25530 */ ,
	_EQUITY("Q", "Equity") /* 43506 */ ;
    
	private final String code;
	private final String description;

	SelectedEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SelectedEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SelectedEnum> getAllValues() { /* 25531 *ALL values */
        return Arrays.asList(SelectedEnum._EQUITY, SelectedEnum._NOT_SELECTED, SelectedEnum._SELECTED);
    }

    public static List<SelectedEnum> getSelectedNotSelected() { /* 50073 Selected/Not Selected */
        return Arrays.asList(SelectedEnum._NOT_SELECTED, SelectedEnum._SELECTED);
    }

    public static List<SelectedEnum> getSelectedOrEquity() { /* 43507 Selected or Equity */
        return Arrays.asList(SelectedEnum._EQUITY, SelectedEnum._SELECTED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(SelectedEnum selected) {  /* 25531 *ALL values */
        return SelectedEnum.getAllValues().contains(selected);
    }

    public static boolean isSelectedNotSelected(SelectedEnum selected) {  /* 50073 Selected/Not Selected */
        return SelectedEnum.getSelectedNotSelected().contains(selected);
    }

    public static boolean isSelectedOrEquity(SelectedEnum selected) {  /* 43507 Selected or Equity */
        return SelectedEnum.getSelectedOrEquity().contains(selected);
    }
}