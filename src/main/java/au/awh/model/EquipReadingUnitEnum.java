package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for EquipReadingUnit.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum EquipReadingUnitEnum {  // 13984 STS ERUNIT Equip Reading Unit

	_MEGAJOULES("MJ", "Megajoules") /* 64251 */ ,
	_HOURS("hrs", "Hours") /* 58598 */ ,
	_KILOWATT_DAS_HOUR("kWh", "Kilowatt-hour") /* 64250 */ ,
	_KILOGRAMS("kg", "Kilograms") /* 58602 */ ,
	_KILOMETRES("km", "Kilometres") /* 58600 */ ,
	_TONNES("tn", "Tonnes") /* 58601 */ ;
    
	private final String code;
	private final String description;

	EquipReadingUnitEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static EquipReadingUnitEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<EquipReadingUnitEnum> getAllValues() { /* 58599 *ALL values */
        return Arrays.asList(EquipReadingUnitEnum._HOURS, EquipReadingUnitEnum._KILOGRAMS, EquipReadingUnitEnum._KILOMETRES, EquipReadingUnitEnum._KILOWATT_DAS_HOUR, EquipReadingUnitEnum._MEGAJOULES, EquipReadingUnitEnum._TONNES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(EquipReadingUnitEnum equipReadingUnit) {  /* 58599 *ALL values */
        return EquipReadingUnitEnum.getAllValues().contains(equipReadingUnit);
    }
}