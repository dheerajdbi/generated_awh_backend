package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for RecordSelectedEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class RecordSelectedConverter implements AttributeConverter<RecordSelectedEnum, String> {

	@Override
	public String convertToDatabaseColumn(RecordSelectedEnum recordSelected) {
		if (recordSelected == null) {
			return "";
		}

		return recordSelected.getCode();
	}

	@Override
	public RecordSelectedEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return RecordSelectedEnum.fromCode("");
		}

		return RecordSelectedEnum.fromCode(StringUtils.strip(dbData));
	}
}
