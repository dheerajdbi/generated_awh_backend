package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for Flag1Enum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class Flag1Converter implements AttributeConverter<Flag1Enum, String> {

	@Override
	public String convertToDatabaseColumn(Flag1Enum flag1) {
		if (flag1 == null) {
			return "";
		}

		return flag1.getCode();
	}

	@Override
	public Flag1Enum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return Flag1Enum.fromCode("");
		}

		return Flag1Enum.fromCode(StringUtils.strip(dbData));
	}
}
