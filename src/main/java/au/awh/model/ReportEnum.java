package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Report.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReportEnum {  // 10301 STS REPORT Report

	_NOT_ENTERED("", "Not entered") /* 45310 */ ,
	_ACCOUNT_SALE_NEW("ACSALNEW", "Account Sale New") /* 47488 */ ,
	_ACCOUNT_SALE_OLD("ACSALOLD", "Account Sale Old") /* 47487 */ ,
	_AQIS_CTR_INSP_DECLARATION("AQCRTINS", "AQIS Ctr Insp Declaration") /* 51325 */ ,
	_AQIS_CTR_INSP_DECL_GRAIN("AQCRTINSGR", "AQIS Ctr Insp Decl Grain") /* 55956 */ ,
	_AQIS_EMPTY_CTR_INSP_RPT("AQECINS", "AQIS Empty Ctr Insp Rpt") /* 51323 */ ,
	_AQIS_EMPTY_CTR_INS_GRAIN("AQECINSGR", "AQIS Empty Ctr Ins Grain") /* 55957 */ ,
	_AQIS_EXP_INSP_EIS_GRAIN("AQEISGR", "AQIS Exp Insp EIS Grain") /* 57402 */ ,
	_AQIS_EXP_INSP_RECORD("AQEXPINS", "AQIS Exp Insp Record") /* 51324 */ ,
	_AQIS_REPORTS_GRAIN("AQISRPGR", "Aqis reports Grain") /* 55955 */ ,
	_AQIS_REPORTS("AQISRPTS", "Aqis reports") /* 51326 */ ,
	_BULK_APPRAISAL("BULKAPPR", "Bulk Appraisal") /* 65050 */ ,
	_CARRIER_CONTAINER_RCV_RPT("CARCONRECR", "Carrier Container Rcv Rpt") /* 53330 */ ,
	_CARRIER_STRG_CTR_REPORT("CARSTG", "Carrier Strg Ctr Report") /* 50006 */ ,
	_CTR_ADV_ALL_RECD_REPORT("CDALLRECV", "Ctr Adv All Recd Report") /* 55630 */ ,
	_CLASSER_SPEC_PRINT("CLSRSPEC", "Classer Spec Print") /* 60963 */ ,
	_CONTAINER_RECEIVAL("CNTRRCVL", "Container Receival") /* 51373 */ ,
	_CONV_BALES_TO_RACKS("CNVTORCKS", "Conv Bales to Racks") /* 62478 */ ,
	_CO_DAS_LOC_PICKING_LIST("COLOCPCK", "Co-Loc Picking List") /* 48995 */ ,
	_COTTON_BITSA_REPORT("COTBITSA", "Cotton Bitsa Report") /* 61938 */ ,
	_COTTON_CONTAINER_RECEIVAL("COTCTRRCV", "Cotton Container Receival") /* 51372 */ ,
	_COTTON_DAILY_LOAD_SUMMARY("COTDLS", "Cotton Daily Load Summary") /* 51912 */ ,
	_COTTON_DAMAGE_REPORT("COTDMGRPT", "Cotton Damage Report") /* 65367 */ ,
	_COTTON_INVOICE("COTINV", "Cotton Invoice") /* 53005 */ ,
	_COTTON_MISSING_BALES("COTMISBAL", "Cotton Missing Bales") /* 62604 */ ,
	_COTTON_PICKING_REPORT("COTPCKRP", "Cotton Picking Report") /* 51049 */ ,
	_COTTON_ECP("COTTECP", "Cotton ECP") /* 51629 */ ,
	_COTTON_ERA("COTTERA", "Cotton ERA") /* 51491 */ ,
	_COTTON_RCVL_LOAD_RECEIPT("CTCTTRPFK", "Cotton Rcvl Load Receipt") /* 50562 */ ,
	_LOADS_RECEIVED_REPORT("CTRCVDJPFK", "Loads Received Report") /* 59572 */ ,
	_CONTAINER_DEPOT_INVOICE("CTRDPINV", "Container Depot Invoice") /* 57506 */ ,
	_CONTAINER_AT_GATE_REPORT("CTRGATE", "Container at Gate Report") /* 57911 */ ,
	_CTR_OUT_SAMPLE_LABELS("CTRSMPLBL", "Ctr Out Sample Labels") /* 57310 */ ,
	_CENTRE_TRANSFER_LOAD("CTRTRFLOAD", "Centre Transfer Load") /* 64510 */ ,
	_COTTON_PACKING_SUMMARY("CTVPSUM", "Cotton Packing Summary") /* 51653 */ ,
	_COTTON_WORK_ORDER("CTWKOCWPFK", "Cotton Work order") /* 51787 */ ,
	_DUMP_ECP("DUMPECP", "Dump ECP") /* 51628 */ ,
	_DUMP_ERA("DUMPERA", "Dump ERA") /* 51492 */ ,
	_EQUIPMENT_LEASOR_REPORT("EQPLEASE", "Equipment Leasor Report") /* 58771 */ ,
	_EQUIPMENT_READING_REPORT("EQPREAD", "Equipment Reading Report") /* 58699 */ ,
	_EX28_GRAIN_EXPORT("EX28GRAIN", "EX28 Grain Export") /* 56007 */ ,
	_GRAIN_DESPATCH_DOCKET("GRAINDESP", "Grain Despatch Docket") /* 55885 */ ,
	_GRAIN_ECP("GRAINECP", "Grain ECP") /* 55804 */ ,
	_GRAIN_ERA("GRAINERA", "Grain ERA") /* 55835 */ ,
	_GRAIN_BULK_DESP_DOCKET("GRBLKDSP", "Grain Bulk Desp Docket") /* 56654 */ ,
	_GRAIN_LOAD_SAMPLE_LABEL("GRLOADSMP", "Grain Load Sample Label") /* 57338 */ ,
	_GRAIN_INWARDS_WB_TICKET("GRNINTKT", "Grain Inwards WB Ticket") /* 56269 */ ,
	_CONTAINER_BARCODE_GRAIN("GRPRTF2PFK", "Container Barcode Grain") /* 55811 */ ,
	_GROWER_STORAGE_WARNING("GRSTGWNG", "Grower Storage Warning") /* 52151 */ ,
	_GRAIN_VPSUM("GRVPSUM", "Grain VPSUM") /* 55990 */ ,
	_MARKERS_BALE_REPORT("MARKBALE", "Markers Bale Report") /* 63102 */ ,
	_OFFERING_LETTERS("OFFLET", "Offering Letters") /* 47643 */ ,
	_PRINT_DOCUNMENT("PRTDOC", "Print Docunment") /* 47647 */ ,
	_RACKING_STOCK_CHECK("RCKSTKCHK", "Racking Stock Check") /* 62841 */ ,
	_RECEIVAL_ADVICE("RCVADV", "Receival Advice") /* 47674 */ ,
	_SORT_BALE_REPORT("SORTBALE", "Sort Bale Report") /* 63747 */ ,
	_STOCK_CHECK_REPORT("STKCHK", "Stock Check Report") /* 45308 */ ,
	_UNKNOWN_WOOL_RECEIVAL("UKWNADV", "Unknown Wool Receival") /* 59152 */ ,
	_UNAVAILABLE_CONTAINERS("UNAVCTRS", "Unavailable Containers") /* 60726 */ ,
	_VOYAGE_CHANGES("VOYCHG", "Voyage Changes") /* 59796 */ ,
	_BIN_PRICE_UPLOAD("WDBI4000", "Bin Price upload") /* 46646 */ ,
	_CTR_DEPOT_CTR_RELEASE("WDCDPOAPFK", "Ctr Depot Ctr Release") /* 49704 */ ,
	_RELEASES_FOR_DEPOT("WDCDPOCPFK", "Releases for Depot") /* 49722 */ ,
	_CTR_MOVEMENTS_REPORT("WDCDPOHPFK", "Ctr Movements Report") /* 49774 */ ,
	_PRINT_CTRDPT_ACCT_INV_STG("WDCDPQCPFK", "Print CtrDpt Acct Inv Stg") /* 49956 */ ,
	_CONTAINER_ADVICE_REPORT("WDCDPY3PFK", "Container Advice Report") /* 55382 */ ,
	_VESSEL_PACKING_SUMMARY("WDCTRQMPFK", "Vessel Packing Summary") /* 45691 */ ,
	_PRINT_ORG_ACCOUNT_INV_STG("WDINVTGPFK", "Print Org Account Inv Stg") /* 46683 */ ,
	_STOCK_AUDIT_REPORT("WDLOCAPPFK", "Stock Audit Report") /* 47356 */ ,
	_STOCK_REPORT("WDLOCASPFK", "Stock Report") /* 47359 */ ,
	_SIMS_IN_LOCATION_REPORT("WDLOCRGPFK", "Sims in Location Report") /* 45800 */ ,
	_TRANSPORTATION_DAILY_SUMM("WDRCVJ2PFK", "Transportation Daily Summ") /* 49131 */ ,
	_STORE_DELV_PERFORMANCE("WDRCVPDPVK", "Store Delv Performance") /* 45521 */ ,
	_CARRIER_WEEKLY_CONTAINER("WDSYSV8PFK", "Carrier Weekly Container") /* 46645 */ ,
	_WEEKLY_CTR_STORAGE_REPORT("WKCTRSTRG", "Weekly Ctr Storage Report") /* 49994 */ ,
	_ACCOUNT_SALE("WSAG7000", "Account Sale") /* 46651 */ ,
	_DMFR_ANOMALIES("WSCP7250", "DMFR Anomalies") /* 48832 */ ,
	_DALGETY_SHOW_FLOOR_REPORT("WSCPB3PVK", "Dalgety Show Floor Report") /* 63541 */ ,
	_DOOR_SALES_INVOICE("WSDSLR8PFK", "Door Sales Invoice") /* 50195 */ ,
	_EXT_LOAD_REPORT("WSEXTYIPFK", "Ext Load Report") /* 58969 */ ,
	_RACKING_LABELS("WSLOCTXPFK", "Racking Labels") /* 62068 */ ,
	_LOCATION_SPACE_REPORT("WSLOCUUPFK", "Location Space Report") /* 62213 */ ,
	_ICS_MARKERS_BALE_LIST("WSLT7113", "ICS Markers Bale List") /* 47536 */ ,
	_ICS_MARKERS_CHECKLIST("WSLT7114", "ICS Markers Checklist") /* 47537 */ ,
	_FLOORSHEET("WSLT7162N", "Floorsheet") /* 64880 */ ,
	_GROWER_ADVICE("WSMA7000", "Grower Advice") /* 46473 */ ,
	_GROWER_ADVICE_SUMMARY("WSMA7002", "Grower Advice Summary") /* 47966 */ ,
	_GROWER_ADVICE_LASER("WSMA7007", "Grower Advice Laser") /* 46590 */ ,
	_PRT_FSL_EMAIL_RCV_BROKER("WSRCVUNPFK", "Prt/Email Rcv Broker") /* 58252 */ ;
    
	private final String code;
	private final String description;

	ReportEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReportEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReportEnum> getAllValues() { /* 45309 *ALL values */
        return Arrays.asList(ReportEnum._ACCOUNT_SALE, ReportEnum._ACCOUNT_SALE_NEW, ReportEnum._ACCOUNT_SALE_OLD, ReportEnum._AQIS_CTR_INSP_DECL_GRAIN, ReportEnum._AQIS_CTR_INSP_DECLARATION, ReportEnum._AQIS_EMPTY_CTR_INS_GRAIN, ReportEnum._AQIS_EMPTY_CTR_INSP_RPT, ReportEnum._AQIS_EXP_INSP_EIS_GRAIN, ReportEnum._AQIS_EXP_INSP_RECORD, ReportEnum._AQIS_REPORTS, ReportEnum._AQIS_REPORTS_GRAIN, ReportEnum._BIN_PRICE_UPLOAD, ReportEnum._BULK_APPRAISAL, ReportEnum._CARRIER_CONTAINER_RCV_RPT, ReportEnum._CARRIER_STRG_CTR_REPORT, ReportEnum._CARRIER_WEEKLY_CONTAINER, ReportEnum._CENTRE_TRANSFER_LOAD, ReportEnum._CLASSER_SPEC_PRINT, ReportEnum._CO_DAS_LOC_PICKING_LIST, ReportEnum._CONTAINER_ADVICE_REPORT, ReportEnum._CONTAINER_AT_GATE_REPORT, ReportEnum._CONTAINER_BARCODE_GRAIN, ReportEnum._CONTAINER_DEPOT_INVOICE, ReportEnum._CONTAINER_RECEIVAL, ReportEnum._CONV_BALES_TO_RACKS, ReportEnum._COTTON_BITSA_REPORT, ReportEnum._COTTON_CONTAINER_RECEIVAL, ReportEnum._COTTON_DAILY_LOAD_SUMMARY, ReportEnum._COTTON_DAMAGE_REPORT, ReportEnum._COTTON_ECP, ReportEnum._COTTON_ERA, ReportEnum._COTTON_INVOICE, ReportEnum._COTTON_MISSING_BALES, ReportEnum._COTTON_PACKING_SUMMARY, ReportEnum._COTTON_PICKING_REPORT, ReportEnum._COTTON_RCVL_LOAD_RECEIPT, ReportEnum._COTTON_WORK_ORDER, ReportEnum._CTR_ADV_ALL_RECD_REPORT, ReportEnum._CTR_DEPOT_CTR_RELEASE, ReportEnum._CTR_MOVEMENTS_REPORT, ReportEnum._CTR_OUT_SAMPLE_LABELS, ReportEnum._DALGETY_SHOW_FLOOR_REPORT, ReportEnum._DMFR_ANOMALIES, ReportEnum._DOOR_SALES_INVOICE, ReportEnum._DUMP_ECP, ReportEnum._DUMP_ERA, ReportEnum._EQUIPMENT_LEASOR_REPORT, ReportEnum._EQUIPMENT_READING_REPORT, ReportEnum._EX28_GRAIN_EXPORT, ReportEnum._EXT_LOAD_REPORT, ReportEnum._FLOORSHEET, ReportEnum._GRAIN_BULK_DESP_DOCKET, ReportEnum._GRAIN_DESPATCH_DOCKET, ReportEnum._GRAIN_ECP, ReportEnum._GRAIN_ERA, ReportEnum._GRAIN_INWARDS_WB_TICKET, ReportEnum._GRAIN_LOAD_SAMPLE_LABEL, ReportEnum._GRAIN_VPSUM, ReportEnum._GROWER_ADVICE, ReportEnum._GROWER_ADVICE_LASER, ReportEnum._GROWER_ADVICE_SUMMARY, ReportEnum._GROWER_STORAGE_WARNING, ReportEnum._ICS_MARKERS_BALE_LIST, ReportEnum._ICS_MARKERS_CHECKLIST, ReportEnum._LOADS_RECEIVED_REPORT, ReportEnum._LOCATION_SPACE_REPORT, ReportEnum._MARKERS_BALE_REPORT, ReportEnum._NOT_ENTERED, ReportEnum._OFFERING_LETTERS, ReportEnum._PRINT_CTRDPT_ACCT_INV_STG, ReportEnum._PRINT_DOCUNMENT, ReportEnum._PRINT_ORG_ACCOUNT_INV_STG, ReportEnum._PRT_FSL_EMAIL_RCV_BROKER, ReportEnum._RACKING_LABELS, ReportEnum._RACKING_STOCK_CHECK, ReportEnum._RECEIVAL_ADVICE, ReportEnum._RELEASES_FOR_DEPOT, ReportEnum._SIMS_IN_LOCATION_REPORT, ReportEnum._SORT_BALE_REPORT, ReportEnum._STOCK_AUDIT_REPORT, ReportEnum._STOCK_CHECK_REPORT, ReportEnum._STOCK_REPORT, ReportEnum._STORE_DELV_PERFORMANCE, ReportEnum._TRANSPORTATION_DAILY_SUMM, ReportEnum._UNAVAILABLE_CONTAINERS, ReportEnum._UNKNOWN_WOOL_RECEIVAL, ReportEnum._VESSEL_PACKING_SUMMARY, ReportEnum._VOYAGE_CHANGES, ReportEnum._WEEKLY_CTR_STORAGE_REPORT);
    }

    public static List<ReportEnum> getDumpSystemReports() { /* 48996 Dump System Reports */
        return Arrays.asList(ReportEnum._AQIS_CTR_INSP_DECL_GRAIN, ReportEnum._AQIS_CTR_INSP_DECLARATION, ReportEnum._AQIS_EMPTY_CTR_INS_GRAIN, ReportEnum._AQIS_EMPTY_CTR_INSP_RPT, ReportEnum._AQIS_EXP_INSP_EIS_GRAIN, ReportEnum._AQIS_EXP_INSP_RECORD, ReportEnum._AQIS_REPORTS, ReportEnum._AQIS_REPORTS_GRAIN, ReportEnum._CARRIER_STRG_CTR_REPORT, ReportEnum._CARRIER_WEEKLY_CONTAINER, ReportEnum._CLASSER_SPEC_PRINT, ReportEnum._CO_DAS_LOC_PICKING_LIST, ReportEnum._CONTAINER_ADVICE_REPORT, ReportEnum._CONTAINER_AT_GATE_REPORT, ReportEnum._CONTAINER_BARCODE_GRAIN, ReportEnum._CONTAINER_DEPOT_INVOICE, ReportEnum._CONTAINER_RECEIVAL, ReportEnum._COTTON_BITSA_REPORT, ReportEnum._COTTON_CONTAINER_RECEIVAL, ReportEnum._COTTON_DAILY_LOAD_SUMMARY, ReportEnum._COTTON_DAMAGE_REPORT, ReportEnum._COTTON_ECP, ReportEnum._COTTON_ERA, ReportEnum._COTTON_INVOICE, ReportEnum._COTTON_MISSING_BALES, ReportEnum._COTTON_PACKING_SUMMARY, ReportEnum._COTTON_PICKING_REPORT, ReportEnum._COTTON_RCVL_LOAD_RECEIPT, ReportEnum._COTTON_WORK_ORDER, ReportEnum._CTR_ADV_ALL_RECD_REPORT, ReportEnum._CTR_DEPOT_CTR_RELEASE, ReportEnum._CTR_MOVEMENTS_REPORT, ReportEnum._CTR_OUT_SAMPLE_LABELS, ReportEnum._DUMP_ECP, ReportEnum._DUMP_ERA, ReportEnum._EX28_GRAIN_EXPORT, ReportEnum._GRAIN_BULK_DESP_DOCKET, ReportEnum._GRAIN_DESPATCH_DOCKET, ReportEnum._GRAIN_ECP, ReportEnum._GRAIN_ERA, ReportEnum._GRAIN_INWARDS_WB_TICKET, ReportEnum._GRAIN_LOAD_SAMPLE_LABEL, ReportEnum._GRAIN_VPSUM, ReportEnum._LOADS_RECEIVED_REPORT, ReportEnum._PRINT_CTRDPT_ACCT_INV_STG, ReportEnum._PRINT_ORG_ACCOUNT_INV_STG, ReportEnum._SIMS_IN_LOCATION_REPORT, ReportEnum._STOCK_CHECK_REPORT, ReportEnum._STORE_DELV_PERFORMANCE, ReportEnum._UNAVAILABLE_CONTAINERS, ReportEnum._VESSEL_PACKING_SUMMARY, ReportEnum._VOYAGE_CHANGES, ReportEnum._WEEKLY_CTR_STORAGE_REPORT);
    }

    public static List<ReportEnum> getWoolSystemReports() { /* 46923 Wool System Reports */
        return Arrays.asList(ReportEnum._ACCOUNT_SALE_NEW, ReportEnum._ACCOUNT_SALE_OLD, ReportEnum._BIN_PRICE_UPLOAD, ReportEnum._BULK_APPRAISAL, ReportEnum._CONV_BALES_TO_RACKS, ReportEnum._DALGETY_SHOW_FLOOR_REPORT, ReportEnum._DMFR_ANOMALIES, ReportEnum._DOOR_SALES_INVOICE, ReportEnum._EQUIPMENT_LEASOR_REPORT, ReportEnum._EQUIPMENT_READING_REPORT, ReportEnum._EXT_LOAD_REPORT, ReportEnum._GROWER_ADVICE, ReportEnum._GROWER_ADVICE_LASER, ReportEnum._GROWER_ADVICE_SUMMARY, ReportEnum._GROWER_STORAGE_WARNING, ReportEnum._LOCATION_SPACE_REPORT, ReportEnum._MARKERS_BALE_REPORT, ReportEnum._NOT_ENTERED, ReportEnum._OFFERING_LETTERS, ReportEnum._PRT_FSL_EMAIL_RCV_BROKER, ReportEnum._RACKING_LABELS, ReportEnum._RACKING_STOCK_CHECK, ReportEnum._RECEIVAL_ADVICE, ReportEnum._SORT_BALE_REPORT, ReportEnum._STOCK_CHECK_REPORT, ReportEnum._UNKNOWN_WOOL_RECEIVAL);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReportEnum report) {  /* 45309 *ALL values */
        return ReportEnum.getAllValues().contains(report);
    }

    public static boolean isDumpSystemReports(ReportEnum report) {  /* 48996 Dump System Reports */
        return ReportEnum.getDumpSystemReports().contains(report);
    }

    public static boolean isWoolSystemReports(ReportEnum report) {  /* 46923 Wool System Reports */
        return ReportEnum.getWoolSystemReports().contains(report);
    }
}