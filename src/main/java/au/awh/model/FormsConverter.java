package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for FormsEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class FormsConverter implements AttributeConverter<FormsEnum, String> {

	@Override
	public String convertToDatabaseColumn(FormsEnum forms) {
		if (forms == null) {
			return "";
		}

		return forms.getCode();
	}

	@Override
	public FormsEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return FormsEnum.fromCode("");
		}

		return FormsEnum.fromCode(StringUtils.strip(dbData));
	}
}
