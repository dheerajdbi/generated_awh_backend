package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReturnCode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReturnCodeEnum {  // 38 STS RTN *Return code

	_STA_NORMAL("", "*Normal") /* 15512 */ ,
	_CHECK_DIGIT_INVALID("*CHKDGT", "Check Digit Invalid") /* 41799 */ ,
	_CONTAINER_NUMBER_INVALID("*INVCHR", "Container Number Invalid") /* 41806 */ ,
	_INVALID_ECP_STATUS("*INVECP", "Invalid ECP Status") /* 42016 */ ,
	_STA_EXIT_LPA_F3_RPA_("03", "*Exit (F3)") /* 17594 */ ,
	_STA_ADD_LPA_F6_RPA_("06", "*Add (F6)") /* 22677 */ ,
	_FUNCTION_KEY_11("11", "Function Key 11") /* 25785 */ ,
	_STA_CANCEL_LPA_F12_RPA_("12", "*Cancel (F12)") /* 17621 */ ,
	_QCMDCHK_ERROR("9999999", "QCMDCHK Error") /* 53890 */ ,
	_ACTION_ITEM_FOUND("AIFND", "Action Item Found") /* 38624 */ ,
	_ALREADY_MARKED("ALRMARK", "Already Marked") /* 65087 */ ,
	_AT_END("AT-END", "At End") /* 22679 */ ,
	_B_FSL_C_REFERENCE_EXHAUSTED("BCREF X", "B/C Reference Exhausted") /* 19355 */ ,
	_ORG_EXCLUDED_FROM_CHARGE("CHGEXCE", "Org Excluded from Charge") /* 42756 */ ,
	_CANNOT_ALLOCATE_OBJECT_DOT_DOT_DOT_("CPF1002", "Cannot allocate object...") /* 33694 */ ,
	_CPF2130_OBJECT_EXISTS("CPF2130", "CPF2130 Object exists") /* 45206 */ ,
	_JOB_NOT_FOUND("CPF3C53", "Job not found") /* 37542 */ ,
	_JOB_DOES_NOT_EXIST("CPF3C55", "Job does not exist") /* 37543 */ ,
	_DOCUMENT_NOT_FOUND_IN_FLR("CPF8A82", "Document not found in Flr") /* 45128 */ ,
	_CPF9801_OBJECT_NOT_FOUND("CPF9801", "CPF9801 Object not found") /* 35978 */ ,
	_CPF9815_MBR_NOT_FOUND("CPF9815", "CPF9815 Mbr not found") /* 32176 */ ,
	_CPF9870_OBJECT_EXISTS("CPF9870", "CPF9870 Object exists") /* 46247 */ ,
	_ERROR_IN_COMMAND("CPF9897", "Error in command") /* 58050 */ ,
	_CPFA095_STMF_NOT_COPIED("CPFA095", "CPFA095 STMF not copied") /* 48724 */ ,
	_UNDESPACTHED_CONTAINER("CRTNDPT", "Undespacthed Container") /* 42932 */ ,
	_CNTR_STS_INV_FOR_DE_DAS_ALLOC("CTRSINV", "Cntr Sts Inv for De-Alloc") /* 40693 */ ,
	_DATA_ERROR("DATAERR", "Data Error") /* 25547 */ ,
	_DELETE_THIS_RECORD("DELETE", "Delete this record") /* 53517 */ ,
	_DELIVERED("DELV", "Delivered") /* 37728 */ ,
	_ERA_CANNOT_BE_PRINTED("ERANPRT", "ERA cannot be printed") /* 42985 */ ,
	_FILE_IN_USE("F-INUSE", "File in Use") /* 23530 */ ,
	_FINISHED("FINISHD", "Finished") /* 28314 */ ,
	_NO_EMAILS_FOUND("HRM0004", "No Emails Found") /* 49066 */ ,
	_NOT_ALL_EMAILS_PROCESSD("HRM0007", "Not all Emails Processd") /* 65201 */ ,
	_SIM_ALREADY_IN_LOCATION("INLOC", "Sim Already In Location") /* 45520 */ ,
	_ITDEBUG("ITDEBUG", "ITDEBUG") /* 53405 */ ,
	_JUMP_FUNCTION_ENTERED("JUMP", "Jump Function Entered") /* 18002 */ ,
	_MULTIPLE_RECORDS_FOUND("MLTIREC", "Multiple Records Found") /* 37529 */ ,
	_NOT_SERIOUS_ERROR("N-ERROR", "Not Serious Error") /* 20057 */ ,
	_NO_DATA("NO DATA", "No data") /* 55838 */ ,
	_NO_RESPONSE("NORESP", "No Response") /* 64584 */ ,
	_NO_USER_ID_FOUND("NOUSER", "No User id Found") /* 64969 */ ,
	_NOT_CONFIRMED("NTCFMD", "Not Confirmed") /* 51526 */ ,
	_NUMERIC_ERROR("NUMERR", "Numeric Error") /* 56957 */ ,
	_OBJECT_ALREADY_LOCKED("OBJLCK", "Object Already Locked") /* 36179 */ ,
	_OTHER_GOODS_AREA("OTHERGD", "Other goods Area") /* 62948 */ ,
	_OVERWEIGHT_BALE_FOUND("OVRWGT", "Overweight Bale Found") /* 48812 */ ,
	_PENDING_CTR_INV_FOR_DEALC("PNDIVDA", "Pending Ctr Inv for DeAlc") /* 40695 */ ,
	_RECORD_SELECTED("RECSEL", "Record Selected") /* 17965 */ ,
	_RESTART_NEW_PARM_VALUE("RESTART", "Restart New Parm Value") /* 54530 */ ,
	_SERIOUS_ERROR("S-ERROR", "Serious error") /* 17983 */ ,
	_TABLE_LIMIT_EXCEEDED("T-LIMIT", "Table Limit Exceeded") /* 22721 */ ,
	_TRANSMITTED("TRANS", "Transmitted") /* 45219 */ ,
	_LOT_HAS_TRI_APO_S_AND_BIS("TRIBI", "Lot has Tri's and Bis") /* 45554 */ ,
	_UNKNOWN_HOST("UNKNOWN", "Unknown Host") /* 64582 */ ,
	_UNPACKED_CONTAINER("UNPACK", "Unpacked Container") /* 45415 */ ,
	_UNSEALED_CONTAINER("UNSEALD", "Unsealed Container") /* 45426 */ ,
	_WEIGHT_ZERO("WGT0", "Weight Zero") /* 56699 */ ,
	_WEIGHT_EXPIRED("WGTEXP", "Weight Expired") /* 56700 */ ,
	_WEIGHT_NOT_STEADY("WGTNS", "Weight Not Steady") /* 56698 */ ,
	_CLIP_TO_DATE_EX("WSU0030", "Clip to Date           EX") /* 19696 */ ,
	_FLOCK_ANALYSIS_NOT_FOUND("WSU0047", "Flock Analysis Not Found") /* 25688 */ ,
	_BULK_CLASS_BALE_NF("WSU0125", "Bulk Class Bale        NF") /* 34912 */ ,
	_LOT_FSL_FOLIO_CAT_SYMB_EXISTS("WSU0134", "Lot/Folio Cat Symb Exists") /* 20823 */ ,
	_CLIENT_INSTRUCTION_EXISTS("WSU0387", "Client Instruction Exists") /* 20824 */ ,
	_DELIVERY_FOLIO_CONTROL_NF("WSU1307", "Delivery Folio Control NF") /* 36666 */ ,
	_BROKER_ENTITY_CONTROL_ERR("WSU2581", "Broker Entity Control Err") /* 54221 */ ,
	_DELETE_NOT_ALLOWED("WSU2588", "Delete not allowed") /* 29441 */ ,
	_RPT_CTL_FILE_DETAILS_NF("WSU2648", "Rpt Ctl File Details NF") /* 54070 */ ,
	_OVERFLOW_FSL_UNDERFLOW("WSU3719", "Overflow/Underflow") /* 36782 */ ,
	_BALES_FOR_SIM_NOT_IN_CTR("WSU4432", "Bales for sim not in ctr") /* 41962 */ ,
	_INVALID_SIM_IN_CONTAINER("WSU4433", "Invalid Sim In Container") /* 41968 */ ,
	_INVALID_ITEM_IN_CONTAINER("WSU4434", "Invalid Item In Container") /* 41969 */ ,
	_UNPAID_WOOL_IN_CONTAINER("WSU4660", "Unpaid wool in container") /* 43800 */ ,
	_STORAGE_FREE_PERIOD_ACTIV("WSU4808", "Storage Free Period Activ") /* 45063 */ ,
	_INPUT_CLSR_SPEC_CLT_INSEX("WSU5003", "Input Clsr Spec Clt InsEX") /* 47713 */ ,
	_NEGATIVE_ON_HOLD_CALC("WSU5260", "Negative on hold Calc") /* 49778 */ ,
	_QCLSCAN_NO_INPUT_STRING("WSU5568", "QCLSCAN No Input String") /* 52716 */ ,
	_QCLSCAN_NO_PATTERN_STRING("WSU5569", "QCLSCAN No Pattern String") /* 52717 */ ,
	_DTAARA_IN_USE("WSU5724", "DTAARA in use") /* 53282 */ ,
	_DTAARA_NOT_READY("WSU5725", "DTAARA Not Ready") /* 53284 */ ,
	_STA_VALUE_REQUIRED("Y2U0001", "*Value required") /* 31529 */ ,
	_STA_RECORD_ALREADY_EXISTS("Y2U0003", "*Record already exists") /* 15601 */ ,
	_STA_DATA_UPDATE_ERROR("Y2U0004", "*Data update error") /* 15600 */ ,
	_STA_RECORD_DOES_NOT_EXIST("Y2U0005", "*Record does not exist") /* 15602 */ ,
	_STA_UPDATE_NOT_ACCEPTED("Y2U0007", "*Update not accepted") /* 31422 */ ,
	_STA_RECORD_NO_LONGER_ON_FILE("Y2U0009", "*Record no longer on file") /* 19813 */ ,
	_STA_RECORD_ADDED("Y2U0011", "*Record added") /* 21098 */ ,
	_STA_RECORD_UPDATED("Y2U0012", "*Record updated") /* 21098 */ ,
	_STA_RECORD_DELETED("Y2U0013", "*Record deleted") /* 21137 */ ,
	_STA_VALUE_NOT_FOUND_IN_LIST("Y2U0014", "*Value not found in list") /* 45228 */ ,
	_STA_NO_VALUE_SELECTED("Y2U0016", "*No value selected") /* 34074 */ ,
	_STA_ROLLBACK_REQUIRED("Y2U0018", "*ROLLBACK required") /* 35612 */ ,
	_STA_VALUE_COMPARISON("Y2U0022", "*Value comparison") /* 31423 */ ,
	_STA_VALUE_NOT_IN_RANGE("Y2U0024", "*Value not in range") /* 39966 */ ,
	_STA_ERROR_OCCURED_ON_CALL_DOT_DOT_DOT_("Y2U0032", "*Error occured on CALL...") /* 32302 */ ,
	_STA_ARRAY_FULL_DAS_CANNOT_ADD_DOT_DOT_DOT_("Y2U0036", "*Array full-cannot add...") /* 32444 */ ,
	_STA_UNABLE_TO_INCREMENT_DATE("Y2U0059", "*Unable to increment date") /* 16162 */ ,
	_STA_UNABLE_TO_CALC_DOT_DURATION("Y2U0060", "*Unable to calc. duration") /* 16163 */ ,
	_STA_ARRAY_INDEX_ERROR("Y2U0068", "*Array index error") /* 16227 */ ,
	_STA_SUBSTRING_ERROR("Y2U0510", "*Substring error") /* 15912 */ ,
	_STA_USER_QUIT_REQUESTED("Y2U9999", "*User QUIT requested") /* 15514 */ ;
    
	private final String code;
	private final String description;

	ReturnCodeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReturnCodeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReturnCodeEnum> getAllValues() { /* 15513 *ALL values */
        return Arrays.asList(ReturnCodeEnum._ACTION_ITEM_FOUND, ReturnCodeEnum._STA_ADD_LPA_F6_RPA_, ReturnCodeEnum._ALREADY_MARKED, ReturnCodeEnum._STA_ARRAY_FULL_DAS_CANNOT_ADD_DOT_DOT_DOT_, ReturnCodeEnum._STA_ARRAY_INDEX_ERROR, ReturnCodeEnum._AT_END, ReturnCodeEnum._B_FSL_C_REFERENCE_EXHAUSTED, ReturnCodeEnum._BALES_FOR_SIM_NOT_IN_CTR, ReturnCodeEnum._BROKER_ENTITY_CONTROL_ERR, ReturnCodeEnum._BULK_CLASS_BALE_NF, ReturnCodeEnum._STA_CANCEL_LPA_F12_RPA_, ReturnCodeEnum._CANNOT_ALLOCATE_OBJECT_DOT_DOT_DOT_, ReturnCodeEnum._CHECK_DIGIT_INVALID, ReturnCodeEnum._CLIENT_INSTRUCTION_EXISTS, ReturnCodeEnum._CLIP_TO_DATE_EX, ReturnCodeEnum._CNTR_STS_INV_FOR_DE_DAS_ALLOC, ReturnCodeEnum._CONTAINER_NUMBER_INVALID, ReturnCodeEnum._CPF2130_OBJECT_EXISTS, ReturnCodeEnum._CPF9801_OBJECT_NOT_FOUND, ReturnCodeEnum._CPF9815_MBR_NOT_FOUND, ReturnCodeEnum._CPF9870_OBJECT_EXISTS, ReturnCodeEnum._CPFA095_STMF_NOT_COPIED, ReturnCodeEnum._DATA_ERROR, ReturnCodeEnum._STA_DATA_UPDATE_ERROR, ReturnCodeEnum._DELETE_NOT_ALLOWED, ReturnCodeEnum._DELETE_THIS_RECORD, ReturnCodeEnum._DELIVERED, ReturnCodeEnum._DELIVERY_FOLIO_CONTROL_NF, ReturnCodeEnum._DOCUMENT_NOT_FOUND_IN_FLR, ReturnCodeEnum._DTAARA_IN_USE, ReturnCodeEnum._DTAARA_NOT_READY, ReturnCodeEnum._ERA_CANNOT_BE_PRINTED, ReturnCodeEnum._ERROR_IN_COMMAND, ReturnCodeEnum._STA_ERROR_OCCURED_ON_CALL_DOT_DOT_DOT_, ReturnCodeEnum._STA_EXIT_LPA_F3_RPA_, ReturnCodeEnum._FILE_IN_USE, ReturnCodeEnum._FINISHED, ReturnCodeEnum._FLOCK_ANALYSIS_NOT_FOUND, ReturnCodeEnum._FUNCTION_KEY_11, ReturnCodeEnum._INPUT_CLSR_SPEC_CLT_INSEX, ReturnCodeEnum._INVALID_ECP_STATUS, ReturnCodeEnum._INVALID_ITEM_IN_CONTAINER, ReturnCodeEnum._INVALID_SIM_IN_CONTAINER, ReturnCodeEnum._ITDEBUG, ReturnCodeEnum._JOB_DOES_NOT_EXIST, ReturnCodeEnum._JOB_NOT_FOUND, ReturnCodeEnum._JUMP_FUNCTION_ENTERED, ReturnCodeEnum._LOT_FSL_FOLIO_CAT_SYMB_EXISTS, ReturnCodeEnum._LOT_HAS_TRI_APO_S_AND_BIS, ReturnCodeEnum._MULTIPLE_RECORDS_FOUND, ReturnCodeEnum._NEGATIVE_ON_HOLD_CALC, ReturnCodeEnum._NO_DATA, ReturnCodeEnum._NO_EMAILS_FOUND, ReturnCodeEnum._NO_RESPONSE, ReturnCodeEnum._NO_USER_ID_FOUND, ReturnCodeEnum._STA_NO_VALUE_SELECTED, ReturnCodeEnum._STA_NORMAL, ReturnCodeEnum._NOT_ALL_EMAILS_PROCESSD, ReturnCodeEnum._NOT_CONFIRMED, ReturnCodeEnum._NOT_SERIOUS_ERROR, ReturnCodeEnum._NUMERIC_ERROR, ReturnCodeEnum._OBJECT_ALREADY_LOCKED, ReturnCodeEnum._ORG_EXCLUDED_FROM_CHARGE, ReturnCodeEnum._OTHER_GOODS_AREA, ReturnCodeEnum._OVERFLOW_FSL_UNDERFLOW, ReturnCodeEnum._OVERWEIGHT_BALE_FOUND, ReturnCodeEnum._PENDING_CTR_INV_FOR_DEALC, ReturnCodeEnum._QCLSCAN_NO_INPUT_STRING, ReturnCodeEnum._QCLSCAN_NO_PATTERN_STRING, ReturnCodeEnum._QCMDCHK_ERROR, ReturnCodeEnum._STA_RECORD_ADDED, ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS, ReturnCodeEnum._STA_RECORD_DELETED, ReturnCodeEnum._STA_RECORD_DOES_NOT_EXIST, ReturnCodeEnum._STA_RECORD_NO_LONGER_ON_FILE, ReturnCodeEnum._RECORD_SELECTED, ReturnCodeEnum._RESTART_NEW_PARM_VALUE, ReturnCodeEnum._STA_ROLLBACK_REQUIRED, ReturnCodeEnum._RPT_CTL_FILE_DETAILS_NF, ReturnCodeEnum._SERIOUS_ERROR, ReturnCodeEnum._SIM_ALREADY_IN_LOCATION, ReturnCodeEnum._STORAGE_FREE_PERIOD_ACTIV, ReturnCodeEnum._STA_SUBSTRING_ERROR, ReturnCodeEnum._TABLE_LIMIT_EXCEEDED, ReturnCodeEnum._TRANSMITTED, ReturnCodeEnum._STA_UNABLE_TO_CALC_DOT_DURATION, ReturnCodeEnum._STA_UNABLE_TO_INCREMENT_DATE, ReturnCodeEnum._UNDESPACTHED_CONTAINER, ReturnCodeEnum._UNKNOWN_HOST, ReturnCodeEnum._UNPACKED_CONTAINER, ReturnCodeEnum._UNPAID_WOOL_IN_CONTAINER, ReturnCodeEnum._UNSEALED_CONTAINER, ReturnCodeEnum._STA_UPDATE_NOT_ACCEPTED, ReturnCodeEnum._STA_USER_QUIT_REQUESTED, ReturnCodeEnum._STA_VALUE_COMPARISON, ReturnCodeEnum._STA_VALUE_NOT_FOUND_IN_LIST, ReturnCodeEnum._STA_VALUE_NOT_IN_RANGE, ReturnCodeEnum._STA_VALUE_REQUIRED, ReturnCodeEnum._WEIGHT_EXPIRED, ReturnCodeEnum._WEIGHT_NOT_STEADY, ReturnCodeEnum._WEIGHT_ZERO);
    }

    public static List<ReturnCodeEnum> getCancelOrnormal() { /* 22708 *Cancel or *Normal */
        return Arrays.asList(ReturnCodeEnum._STA_CANCEL_LPA_F12_RPA_, ReturnCodeEnum._STA_NORMAL);
    }

    public static List<ReturnCodeEnum> getExitcancelOrnormal() { /* 21709 *Exit, *Cancel or *Normal */
        return Arrays.asList(ReturnCodeEnum._STA_CANCEL_LPA_F12_RPA_, ReturnCodeEnum._STA_EXIT_LPA_F3_RPA_, ReturnCodeEnum._STA_NORMAL);
    }

    public static List<ReturnCodeEnum> getNormalOrExists() { /* 45715 Normal or exists */
        return Arrays.asList(ReturnCodeEnum._STA_NORMAL, ReturnCodeEnum._STA_RECORD_ALREADY_EXISTS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReturnCodeEnum returnCode) {  /* 15513 *ALL values */
        return ReturnCodeEnum.getAllValues().contains(returnCode);
    }

    public static boolean isCancelOrnormal(ReturnCodeEnum returnCode) {  /* 22708 *Cancel or *Normal */
        return ReturnCodeEnum.getCancelOrnormal().contains(returnCode);
    }

    public static boolean isExitcancelOrnormal(ReturnCodeEnum returnCode) {  /* 21709 *Exit, *Cancel or *Normal */
        return ReturnCodeEnum.getExitcancelOrnormal().contains(returnCode);
    }

    public static boolean isNormalOrExists(ReturnCodeEnum returnCode) {  /* 45715 Normal or exists */
        return ReturnCodeEnum.getNormalOrExists().contains(returnCode);
    }
}