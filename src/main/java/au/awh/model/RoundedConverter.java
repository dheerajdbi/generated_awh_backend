package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for RoundedEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class RoundedConverter implements AttributeConverter<RoundedEnum, String> {

	@Override
	public String convertToDatabaseColumn(RoundedEnum rounded) {
		if (rounded == null) {
			return "";
		}

		return rounded.getCode();
	}

	@Override
	public RoundedEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return RoundedEnum.fromCode("");
		}

		return RoundedEnum.fromCode(StringUtils.strip(dbData));
	}
}
