package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for OverrideEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class OverrideConverter implements AttributeConverter<OverrideEnum, String> {

	@Override
	public String convertToDatabaseColumn(OverrideEnum override) {
		if (override == null) {
			return "";
		}

		return override.getCode();
	}

	@Override
	public OverrideEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return OverrideEnum.fromCode("");
		}

		return OverrideEnum.fromCode(StringUtils.strip(dbData));
	}
}
