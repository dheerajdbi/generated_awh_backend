package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for StateCode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum StateCodeEnum {  // 571 STS STTCDE State Code

	_NOT_ENTERED("", "Not entered") /* 17812 */ ,
	_NEW_SOUTH_WALES("2", "New South Wales") /* 16888 */ ,
	_VICTORIA("3", "Victoria") /* 16890 */ ,
	_QUEENSLAND("4", "Queensland") /* 16891 */ ,
	_SOUTH_AUSTRALIA("5", "South Australia") /* 16892 */ ,
	_WESTERN_AUSTRALIA("6", "Western Australia") /* 16893 */ ,
	_TASMANIA("7", "Tasmania") /* 16894 */ ,
	_PREMIER("I", "Premier") /* 25882 */ ;
    
	private final String code;
	private final String description;

	StateCodeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static StateCodeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<StateCodeEnum> getAllValues() { /* 16889 *ALL values */
        return Arrays.asList(StateCodeEnum._NEW_SOUTH_WALES, StateCodeEnum._NOT_ENTERED, StateCodeEnum._PREMIER, StateCodeEnum._QUEENSLAND, StateCodeEnum._SOUTH_AUSTRALIA, StateCodeEnum._TASMANIA, StateCodeEnum._VICTORIA, StateCodeEnum._WESTERN_AUSTRALIA);
    }

    public static List<StateCodeEnum> getValidValues() { /* 28551 Valid Values */
        return Arrays.asList(StateCodeEnum._NEW_SOUTH_WALES, StateCodeEnum._PREMIER, StateCodeEnum._QUEENSLAND, StateCodeEnum._SOUTH_AUSTRALIA, StateCodeEnum._TASMANIA, StateCodeEnum._VICTORIA, StateCodeEnum._WESTERN_AUSTRALIA);
    }

    public static List<StateCodeEnum> getX234567() { /* 27469 2,3,4,5,6,7 */
        return Arrays.asList(StateCodeEnum._NEW_SOUTH_WALES, StateCodeEnum._QUEENSLAND, StateCodeEnum._SOUTH_AUSTRALIA, StateCodeEnum._TASMANIA, StateCodeEnum._VICTORIA, StateCodeEnum._WESTERN_AUSTRALIA);
    }

    /**
     * Check
     */

    public static boolean isAllValues(StateCodeEnum stateCode) {  /* 16889 *ALL values */
        return StateCodeEnum.getAllValues().contains(stateCode);
    }

    public static boolean isValidValues(StateCodeEnum stateCode) {  /* 28551 Valid Values */
        return StateCodeEnum.getValidValues().contains(stateCode);
    }

    public static boolean isX234567(StateCodeEnum stateCode) {  /* 27469 2,3,4,5,6,7 */
        return StateCodeEnum.getX234567().contains(stateCode);
    }
}