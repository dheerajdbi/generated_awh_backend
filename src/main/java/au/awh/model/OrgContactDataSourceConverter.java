package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for OrgContactDataSourceEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class OrgContactDataSourceConverter implements AttributeConverter<OrgContactDataSourceEnum, String> {

	@Override
	public String convertToDatabaseColumn(OrgContactDataSourceEnum orgContactDataSource) {
		if (orgContactDataSource == null) {
			return "";
		}

		return orgContactDataSource.getCode();
	}

	@Override
	public OrgContactDataSourceEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return OrgContactDataSourceEnum.fromCode("");
		}

		return OrgContactDataSourceEnum.fromCode(StringUtils.strip(dbData));
	}
}
