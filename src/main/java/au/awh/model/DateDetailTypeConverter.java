package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for DateDetailTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class DateDetailTypeConverter implements AttributeConverter<DateDetailTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(DateDetailTypeEnum dateDetailType) {
		if (dateDetailType == null) {
			return "";
		}

		return dateDetailType.getCode();
	}

	@Override
	public DateDetailTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return DateDetailTypeEnum.fromCode("");
		}

		return DateDetailTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
