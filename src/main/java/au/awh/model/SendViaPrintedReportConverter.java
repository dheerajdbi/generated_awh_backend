package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SendViaPrintedReportEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SendViaPrintedReportConverter implements AttributeConverter<SendViaPrintedReportEnum, String> {

	@Override
	public String convertToDatabaseColumn(SendViaPrintedReportEnum sendViaPrintedReport) {
		if (sendViaPrintedReport == null) {
			return "";
		}

		return sendViaPrintedReport.getCode();
	}

	@Override
	public SendViaPrintedReportEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SendViaPrintedReportEnum.fromCode("");
		}

		return SendViaPrintedReportEnum.fromCode(StringUtils.strip(dbData));
	}
}
