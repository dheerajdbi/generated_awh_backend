package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for OrganisationStatus.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum OrganisationStatusEnum {  // 8801 STS ORGSTS Organisation Status

	_ACTIVE("A", "Active") /* 38619 */ ,
	_INACTIVE("I", "Inactive") /* 38621 */ ;
    
	private final String code;
	private final String description;

	OrganisationStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static OrganisationStatusEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<OrganisationStatusEnum> getAllValues() { /* 38620 *ALL values */
        return Arrays.asList(OrganisationStatusEnum._ACTIVE, OrganisationStatusEnum._INACTIVE);
    }

    /**
     * Check
     */

    public static boolean isAllValues(OrganisationStatusEnum organisationStatus) {  /* 38620 *ALL values */
        return OrganisationStatusEnum.getAllValues().contains(organisationStatus);
    }
}