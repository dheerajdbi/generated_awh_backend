package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for RecordDataChangedEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class RecordDataChangedConverter implements AttributeConverter<RecordDataChangedEnum, String> {

	@Override
	public String convertToDatabaseColumn(RecordDataChangedEnum recordDataChanged) {
		if (recordDataChanged == null) {
			return "";
		}

		return recordDataChanged.getCode();
	}

	@Override
	public RecordDataChangedEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return RecordDataChangedEnum.fromCode("");
		}

		return RecordDataChangedEnum.fromCode(StringUtils.strip(dbData));
	}
}
