package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for FirstPassEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class FirstPassConverter implements AttributeConverter<FirstPassEnum, String> {

	@Override
	public String convertToDatabaseColumn(FirstPassEnum firstPass) {
		if (firstPass == null) {
			return "";
		}

		return firstPass.getCode();
	}

	@Override
	public FirstPassEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return FirstPassEnum.fromCode("");
		}

		return FirstPassEnum.fromCode(StringUtils.strip(dbData));
	}
}
