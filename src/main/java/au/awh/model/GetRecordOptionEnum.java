package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for GetRecordOption.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum GetRecordOptionEnum {  // 8141 STS EKST Get Record Option

	_NORMAL("", "Normal") /* 35963 */ ,
	_IGN_MLTI_REC_AND_NOT_FOUND("B", "Ign Mlti Rec & Not Found") /* 37531 */ ,
	_IGNORE_RECORD_EXISTS("E", "Ignore record exists") /* 40764 */ ,
	_IGNORE_RECORD_NOT_FOUND("I", "Ignore record not found") /* 35961 */ ,
	_IGNORE_MULTI_REC("M", "Ignore Multi Rec") /* 37530 */ ,
	_NO_MESSAGE_ON_NOT_FOUND("N", "No message on not found") /* 40510 */ ;
    
	private final String code;
	private final String description;

	GetRecordOptionEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static GetRecordOptionEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<GetRecordOptionEnum> getAllValues() { /* 35962 *ALL values */
        return Arrays.asList(GetRecordOptionEnum._IGN_MLTI_REC_AND_NOT_FOUND, GetRecordOptionEnum._IGNORE_MULTI_REC, GetRecordOptionEnum._IGNORE_RECORD_EXISTS, GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND, GetRecordOptionEnum._NO_MESSAGE_ON_NOT_FOUND, GetRecordOptionEnum._NORMAL);
    }

    public static List<GetRecordOptionEnum> getIgnoreMultiRecords() { /* 37533 Ignore Multi Records */
        return Arrays.asList(GetRecordOptionEnum._IGN_MLTI_REC_AND_NOT_FOUND, GetRecordOptionEnum._IGNORE_MULTI_REC);
    }

    public static List<GetRecordOptionEnum> getIgnoreNotFound() { /* 37532 Ignore Not Found */
        return Arrays.asList(GetRecordOptionEnum._IGN_MLTI_REC_AND_NOT_FOUND, GetRecordOptionEnum._IGNORE_RECORD_NOT_FOUND);
    }

    /**
     * Check
     */

    public static boolean isAllValues(GetRecordOptionEnum getRecordOption) {  /* 35962 *ALL values */
        return GetRecordOptionEnum.getAllValues().contains(getRecordOption);
    }

    public static boolean isIgnoreMultiRecords(GetRecordOptionEnum getRecordOption) {  /* 37533 Ignore Multi Records */
        return GetRecordOptionEnum.getIgnoreMultiRecords().contains(getRecordOption);
    }

    public static boolean isIgnoreNotFound(GetRecordOptionEnum getRecordOption) {  /* 37532 Ignore Not Found */
        return GetRecordOptionEnum.getIgnoreNotFound().contains(getRecordOption);
    }
}