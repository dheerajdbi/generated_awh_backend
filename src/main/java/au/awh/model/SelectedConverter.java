package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SelectedEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SelectedConverter implements AttributeConverter<SelectedEnum, String> {

	@Override
	public String convertToDatabaseColumn(SelectedEnum selected) {
		if (selected == null) {
			return "";
		}

		return selected.getCode();
	}

	@Override
	public SelectedEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SelectedEnum.fromCode("");
		}

		return SelectedEnum.fromCode(StringUtils.strip(dbData));
	}
}
