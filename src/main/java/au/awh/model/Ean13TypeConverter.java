package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for Ean13TypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class Ean13TypeConverter implements AttributeConverter<Ean13TypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(Ean13TypeEnum ean13Type) {
		if (ean13Type == null) {
			return "";
		}

		return ean13Type.getCode();
	}

	@Override
	public Ean13TypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return Ean13TypeEnum.fromCode("");
		}

		return Ean13TypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
