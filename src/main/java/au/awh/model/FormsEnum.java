package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Forms.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum FormsEnum {  // 1692 STS FRM Forms

	_NOT_ENTERED("", "Not entered") /* 35902 */ ,
	_STA_SAME("*SAME", "*SAME") /* 35232 */ ,
	_STA_STD("*STD", "*STD") /* 19856 */ ,
	_STA_STDP("*STDP", "*STDP") /* 35555 */ ,
	_1_UP_LABELS("1 UP LBLS", "1 Up Labels") /* 25076 */ ,
	_2_UP_LABELS("2 UP LBLS", "2 Up Labels") /* 25077 */ ,
	_3_UP_LABELS("3 UP LBLS", "3 Up Labels") /* 25078 */ ,
	_280_X_210("A4", "280 x 210") /* 22192 */ ,
	_A4_2_PART("A4/2", "A4 2 Part") /* 24036 */ ,
	_A4_3_PART("A4/3", "A4 3 Part") /* 24037 */ ,
	_A4_LASER_2_UP_LABELS("A42UPLBL", "A4 Laser 2 Up Labels") /* 54279 */ ,
	_A4_BARCODE("A4BC", "A4 Barcode") /* 35716 */ ,
	_A4_LASER_CATALOGUE("A4CAT", "A4 Laser Catalogue") /* 28847 */ ,
	_A4_LASER_CATALOGUE_2("A4CAT2", "A4 Laser Catalogue 2") /* 43824 */ ,
	_280_X_210_A4_LETTER_HEAD("A4LH", "280 x 210 A4 Letter Head") /* 23237 */ ,
	_A4_PORTRAIT_AWH_LOGO("A4PORAWH", "A4 Portrait AWH logo") /* 46636 */ ,
	_A4_PORTRAIT_AWN_LOGO("A4PORAWN", "A4 Portrait AWN logo") /* 59825 */ ,
	_A4_PORTRAIT_AWN_LOGO_SPLT("A4PORAWNS", "A4 Portrait AWN logo Splt") /* 64080 */ ,
	_A4_PORTRAIT_MOSES("A4PORMOSES", "A4 Portrait Moses") /* 65398 */ ,
	_A4_PORTRAIT("A4PORTRAIT", "A4 Portrait") /* 34842 */ ,
	_140_X_210("A5", "140 x 210") /* 22193 */ ,
	_A5_2_PART("A5/2", "A5 2 Part") /* 24038 */ ,
	_A5_3_PART("A5/3", "A5 3 Part") /* 24039 */ ,
	_ACCOUNT_SALE("ACCSALE", "Account Sale") /* 46656 */ ,
	_ACCOUNT_SALE_WITH_EMAIL("ACCSALEP", "Account Sale With Email") /* 50409 */ ,
	_CONTAINER_APPROVAL_RECORD("AQECINS", "Container Approval Record") /* 63837 */ ,
	_EXPORT_COMPLIANCE_RECORD("AQEXPINS", "Export Compliance Record") /* 63836 */ ,
	_ARROW_LEFT("ARWLEFT", "Arrow Left") /* 62182 */ ,
	_ARROW_LEFT_RIGHT("ARWLFRT", "Arrow Left Right") /* 62798 */ ,
	_ARROW_RIGHT("ARWRIGHT", "Arrow Right") /* 62183 */ ,
	_ARROW_RIGHT_LEFT("ARWRITL", "Arrow Right Left") /* 62797 */ ,
	_AWHEML("AWHEML", "AWHEML") /* 63963 */ ,
	_AWH_LANDSCAPE_EMAIL("AWHEML132", "AWH Landscape Email") /* 59574 */ ,
	_BALE_TAG_BARCODE("BALETAG", "Bale Tag Barcode") /* 63081 */ ,
	_BULK_CLASS_CENTRE_TRANSF("BCCTRTRF", "Bulk Class Centre Transf") /* 64275 */ ,
	_BUYERS_INVOICE("BINV", "Buyers Invoice") /* 22191 */ ,
	_BROKER_INVOICE("BKRINV", "Broker Invoice") /* 48234 */ ,
	_BUYER_INVOICE_SUMMARY("BUYERINV", "Buyer Invoice Summary") /* 48781 */ ,
	_CARRIER_CONTAINER_RCV_RPT("CARCONRCVR", "Carrier Container Rcv Rpt") /* 53334 */ ,
	_CATALOGUE("CAT", "Catalogue") /* 20378 */ ,
	_CLIENT("CLT", "Client") /* 20379 */ ,
	_CO_DAS_LOCARED_PICK_LIST("COLOCPCK", "Co-locared Pick list") /* 48993 */ ,
	_CONSIGNMENT_REPORT("CONSIGN", "Consignment Report") /* 43986 */ ,
	_CONSIGNMENT_BARCODE("CONSIGNBC", "Consignment Barcode") /* 43982 */ ,
	_COTTON_RECEIVAL_LOAD("COTRCVLLOD", "Cotton Receival Load") /* 52533 */ ,
	_CR33_LOCATION_BARCODE("CR33", "CR33 Location Barcode") /* 38019 */ ,
	_COTTON_DAILY_SUMMARY("CTINSTRRPT", "Cotton Daily Summary") /* 51951 */ ,
	_CONTAIER_BARCODE_GRAIN("CTRBCGRN", "Contaier Barcode Grain") /* 55814 */ ,
	_CONTAINER_AT_GATE("CTRGATE", "Container at Gate") /* 57913 */ ,
	_CONTAINER_RECEIVAL_REPORT("CTRRCVL", "Container Receival Report") /* 53335 */ ,
	_CONTAINER_RELEASE_ADVICE("CTRRLSADV", "Container Release Advice") /* 55359 */ ,
	_CONTAINER_STORAGE_ADVICE("CTRSTGADV", "Container Storage Advice") /* 55102 */ ,
	_COTTON_VESSEL_PACK_SUMM("CTVPSUM", "Cotton Vessel Pack Summ") /* 51501 */ ,
	_CTR_DEPOT_EMPTY_RELEASE("DPCTRRLS", "Ctr Depot Empty Release") /* 50079 */ ,
	_EQUIPMENT_READINGS("EQPREAD", "Equipment Readings") /* 58702 */ ,
	_FLOORSHEET_GROWER("FLRSHEETGR", "Floorsheet Grower") /* 37598 */ ,
	_FLOORSHEET_INTERLOT("FLRSHEETIN", "Floorsheet Interlot") /* 37599 */ ,
	_FLOORSHEET_REGRAB("FLRSHEETRG", "Floorsheet Regrab") /* 38012 */ ,
	_GRAIN_VESSEL_PACKING_SUMM("GRVPSUM", "Grain Vessel Packing Summ") /* 55986 */ ,
	_GRAIN_VSSL_PACK_SUM_AQIS("GRVPSUMAQ", "Grain Vssl pack Sum AQIS") /* 56116 */ ,
	_GROWER_ADVICE_LASER("GRWADV", "Grower Advice Laser") /* 46603 */ ,
	_GROWER_ADVICE_WITH_EMAIL("GRWADVP", "Grower Advice with EMail") /* 50406 */ ,
	_A4_LASER_3_UP_LABELS("J8160LBL", "A4 Laser 3 Up Labels") /* 54280 */ ,
	_A4_LASER_1_UP_LABELS("J8160LBL1", "A4 Laser 1 Up Labels") /* 54278 */ ,
	_L7157_33_UP_AVERY_LBLS("L7157", "L7157 33 up Avery Lbls") /* 63907 */ ,
	_L7168_LOCATION_BARCODE("L7168", "L7168 Location Barcode") /* 38033 */ ,
	_L7168_SMALL_LOCN_BARCODE("L7168X", "L7168 Small Locn Barcode") /* 54256 */ ,
	_LOAD_DETAIL("LOADDTL", "Load Detail") /* 45539 */ ,
	_LOAD_SUMMARY("LOADSUM", "Load Summary") /* 45540 */ ,
	_MARKERS_BALE_LIST("MARKBALE", "Markers Bale List") /* 62456 */ ,
	_MISSING_WOOL_IN_SIM_SEQ("MWRSIM", "Missing Wool in SIM Seq") /* 45625 */ ,
	_RACK_LABELS("RACK", "Rack Labels") /* 61873 */ ,
	_RACKING_BARCODE_FULL("RACKBCFL", "Racking Barcode FULL") /* 62060 */ ,
	_RACK_COLUMN_LABELS("RACKCL", "Rack Column Labels") /* 62062 */ ,
	_RACK_LABEL_4_BC_ON_LEFT("RCKA5FL", "Rack Label 4 BC on Left") /* 62187 */ ,
	_RACK_LABEL_4_BC_ON_RIGHT("RCKA5FR", "Rack Label 4 BC on Right") /* 62186 */ ,
	_RACK_LABEL_5_BC_ON_RIGHT("RCKBCA5", "Rack Label 5 BC on Right") /* 62126 */ ,
	_RACK_LABEL_5_BC_ON_LEFT("RCKBCA5L", "Rack Label 5 BC on Left") /* 62445 */ ,
	_RACKING_BC_FULL_A5("RCKBCFLA5", "Racking BC Full A5") /* 62103 */ ,
	_SALE_APPRAISAL_ADVICE("SALAPPR", "Sale Appraisal Advice") /* 59913 */ ,
	_SALE_PRICE_ADVICE("SALPRC", "Sale Price Advice") /* 49232 */ ,
	_SALE_OFFER_PRICE_ADVICE("SALPRCOFF", "Sale Offer Price Advice") /* 64106 */ ,
	_4XA6_ON_A4_AWH_LOGO_WA("SFAWHWABX", "4xA6 on A4 AWH Logo WA") /* 63234 */ ,
	_1X_ON_A4_SHEET_AWH_LOGO("SFBOXAWH", "1x on A4 Sheet AWH logo") /* 46338 */ ,
	_3_AWN("SFBOXAWN03", "3 AWN") /* 64505 */ ,
	_1XA5_ON_A4_SHEET("SFBOXLABEL", "1xA5 on A4 Sheet") /* 37964 */ ,
	_3_LANDMARK_BROOKLYN("SFBOXLBL03", "3 Landmark Brooklyn") /* 60846 */ ,
	_57X99X10_AVERY_L7173("SFBOXLBL10", "57x99x10 Avery L7173") /* 37995 */ ,
	_4XA6_ON_A4_SHEET_NSW("SFBOXLBLA6", "4xA6 on A4 Sheet NSW") /* 37963 */ ,
	_4XA6_FOR_BROOKLYN("SFBOXLBLBK", "4xA6 for Brooklyn") /* 37988 */ ,
	_4X145X90_FOR_WA_SFBOX("SFBOXLBLWA", "4x145x90 for WA SfBox") /* 37965 */ ,
	_DALGETY_SF_A5_LOGO("SFDALLOGO", "Dalgety SF A5 Logo") /* 63446 */ ,
	_4XA6_WITH_LNDMRK_LOGO_WA("SFLMKWABX", "4xA6 with LndMrk Logo WA") /* 63264 */ ,
	_SIMS_IN_LOCATIONS_FORM("SIMINLOC", "Sims in Locations Form") /* 45801 */ ,
	_STORAGE_CM_FREIGHT("STGCMFRT", "Storage CM Freight") /* 61053 */ ,
	_UBL("UBL", "UBL") /* 20377 */ ,
	_VESSEL_PACKING_SUMMARY("VPSUM", "Vessel Packing Summary") /* 45677 */ ,
	_WOOL_PICKING("WOOLPICK", "Wool Picking") /* 61984 */ ,
	_WOOL_RACKING_STOCK_CHECK("WSRACSC", "Wool Racking Stock Check") /* 63665 */ ;
    
	private final String code;
	private final String description;

	FormsEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static FormsEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<FormsEnum> getAllValues() { /* 19857 *ALL values */
        return Arrays.asList(FormsEnum._A4_2_PART, FormsEnum._A4_3_PART, FormsEnum._A4_BARCODE, FormsEnum._A4_LASER_1_UP_LABELS, FormsEnum._A4_LASER_2_UP_LABELS, FormsEnum._A4_LASER_3_UP_LABELS, FormsEnum._A4_LASER_CATALOGUE, FormsEnum._A4_LASER_CATALOGUE_2, FormsEnum._A4_PORTRAIT, FormsEnum._A4_PORTRAIT_AWH_LOGO, FormsEnum._A4_PORTRAIT_AWN_LOGO, FormsEnum._A4_PORTRAIT_AWN_LOGO_SPLT, FormsEnum._A4_PORTRAIT_MOSES, FormsEnum._A5_2_PART, FormsEnum._A5_3_PART, FormsEnum._ACCOUNT_SALE, FormsEnum._ACCOUNT_SALE_WITH_EMAIL, FormsEnum._ARROW_LEFT, FormsEnum._ARROW_LEFT_RIGHT, FormsEnum._ARROW_RIGHT, FormsEnum._ARROW_RIGHT_LEFT, FormsEnum._AWH_LANDSCAPE_EMAIL, FormsEnum._AWHEML, FormsEnum._BALE_TAG_BARCODE, FormsEnum._BROKER_INVOICE, FormsEnum._BULK_CLASS_CENTRE_TRANSF, FormsEnum._BUYER_INVOICE_SUMMARY, FormsEnum._BUYERS_INVOICE, FormsEnum._CARRIER_CONTAINER_RCV_RPT, FormsEnum._CATALOGUE, FormsEnum._CLIENT, FormsEnum._CO_DAS_LOCARED_PICK_LIST, FormsEnum._CONSIGNMENT_BARCODE, FormsEnum._CONSIGNMENT_REPORT, FormsEnum._CONTAIER_BARCODE_GRAIN, FormsEnum._CONTAINER_APPROVAL_RECORD, FormsEnum._CONTAINER_AT_GATE, FormsEnum._CONTAINER_RECEIVAL_REPORT, FormsEnum._CONTAINER_RELEASE_ADVICE, FormsEnum._CONTAINER_STORAGE_ADVICE, FormsEnum._COTTON_DAILY_SUMMARY, FormsEnum._COTTON_RECEIVAL_LOAD, FormsEnum._COTTON_VESSEL_PACK_SUMM, FormsEnum._CR33_LOCATION_BARCODE, FormsEnum._CTR_DEPOT_EMPTY_RELEASE, FormsEnum._DALGETY_SF_A5_LOGO, FormsEnum._EQUIPMENT_READINGS, FormsEnum._EXPORT_COMPLIANCE_RECORD, FormsEnum._FLOORSHEET_GROWER, FormsEnum._FLOORSHEET_INTERLOT, FormsEnum._FLOORSHEET_REGRAB, FormsEnum._GRAIN_VESSEL_PACKING_SUMM, FormsEnum._GRAIN_VSSL_PACK_SUM_AQIS, FormsEnum._GROWER_ADVICE_LASER, FormsEnum._GROWER_ADVICE_WITH_EMAIL, FormsEnum._L7157_33_UP_AVERY_LBLS, FormsEnum._L7168_LOCATION_BARCODE, FormsEnum._L7168_SMALL_LOCN_BARCODE, FormsEnum._LOAD_DETAIL, FormsEnum._LOAD_SUMMARY, FormsEnum._MARKERS_BALE_LIST, FormsEnum._MISSING_WOOL_IN_SIM_SEQ, FormsEnum._NOT_ENTERED, FormsEnum._RACK_COLUMN_LABELS, FormsEnum._RACK_LABEL_4_BC_ON_LEFT, FormsEnum._RACK_LABEL_4_BC_ON_RIGHT, FormsEnum._RACK_LABEL_5_BC_ON_LEFT, FormsEnum._RACK_LABEL_5_BC_ON_RIGHT, FormsEnum._RACK_LABELS, FormsEnum._RACKING_BARCODE_FULL, FormsEnum._RACKING_BC_FULL_A5, FormsEnum._SALE_APPRAISAL_ADVICE, FormsEnum._SALE_OFFER_PRICE_ADVICE, FormsEnum._SALE_PRICE_ADVICE, FormsEnum._STA_SAME, FormsEnum._SIMS_IN_LOCATIONS_FORM, FormsEnum._STA_STD, FormsEnum._STA_STDP, FormsEnum._STORAGE_CM_FREIGHT, FormsEnum._UBL, FormsEnum._VESSEL_PACKING_SUMMARY, FormsEnum._WOOL_PICKING, FormsEnum._WOOL_RACKING_STOCK_CHECK, FormsEnum._140_X_210, FormsEnum._1_UP_LABELS, FormsEnum._1X_ON_A4_SHEET_AWH_LOGO, FormsEnum._1XA5_ON_A4_SHEET, FormsEnum._280_X_210, FormsEnum._280_X_210_A4_LETTER_HEAD, FormsEnum._2_UP_LABELS, FormsEnum._3_AWN, FormsEnum._3_LANDMARK_BROOKLYN, FormsEnum._3_UP_LABELS, FormsEnum._4X145X90_FOR_WA_SFBOX, FormsEnum._4XA6_FOR_BROOKLYN, FormsEnum._4XA6_ON_A4_AWH_LOGO_WA, FormsEnum._4XA6_ON_A4_SHEET_NSW, FormsEnum._4XA6_WITH_LNDMRK_LOGO_WA, FormsEnum._57X99X10_AVERY_L7173);
    }

    public static List<FormsEnum> getBarcodeLabels() { /* 38025 Barcode Labels */
        return Arrays.asList(FormsEnum._BALE_TAG_BARCODE, FormsEnum._CR33_LOCATION_BARCODE, FormsEnum._L7157_33_UP_AVERY_LBLS, FormsEnum._L7168_LOCATION_BARCODE, FormsEnum._L7168_SMALL_LOCN_BARCODE, FormsEnum._RACK_COLUMN_LABELS, FormsEnum._RACK_LABELS, FormsEnum._RACKING_BARCODE_FULL);
    }

    public static List<FormsEnum> getGrainVpsum() { /* 56689 Grain VPSUM */
        return Arrays.asList(FormsEnum._GRAIN_VESSEL_PACKING_SUMM, FormsEnum._GRAIN_VSSL_PACK_SUM_AQIS);
    }

    public static List<FormsEnum> getLabels() { /* 25079 Labels */
        return Arrays.asList(FormsEnum._A4_LASER_1_UP_LABELS, FormsEnum._A4_LASER_2_UP_LABELS, FormsEnum._A4_LASER_3_UP_LABELS, FormsEnum._1_UP_LABELS, FormsEnum._2_UP_LABELS, FormsEnum._3_UP_LABELS);
    }

    public static List<FormsEnum> getOldReceivalLoad() { /* 45544 Old Receival Load */
        return Arrays.asList(FormsEnum._LOAD_DETAIL, FormsEnum._LOAD_SUMMARY);
    }

    public static List<FormsEnum> getRackingLabels() { /* 62067 Racking Labels */
        return Arrays.asList(FormsEnum._RACK_COLUMN_LABELS, FormsEnum._RACK_LABEL_4_BC_ON_LEFT, FormsEnum._RACK_LABEL_4_BC_ON_RIGHT, FormsEnum._RACK_LABEL_5_BC_ON_LEFT, FormsEnum._RACK_LABEL_5_BC_ON_RIGHT, FormsEnum._RACKING_BARCODE_FULL, FormsEnum._RACKING_BC_FULL_A5);
    }

    public static List<FormsEnum> getSfBoxLabels() { /* 37966 SF BOX Labels */
        return Arrays.asList(FormsEnum._CR33_LOCATION_BARCODE, FormsEnum._DALGETY_SF_A5_LOGO, FormsEnum._1X_ON_A4_SHEET_AWH_LOGO, FormsEnum._1XA5_ON_A4_SHEET, FormsEnum._3_AWN, FormsEnum._3_LANDMARK_BROOKLYN, FormsEnum._4X145X90_FOR_WA_SFBOX, FormsEnum._4XA6_FOR_BROOKLYN, FormsEnum._4XA6_ON_A4_AWH_LOGO_WA, FormsEnum._4XA6_ON_A4_SHEET_NSW, FormsEnum._4XA6_WITH_LNDMRK_LOGO_WA, FormsEnum._57X99X10_AVERY_L7173);
    }

    public static List<FormsEnum> getVesselPackingSummLst() { /* 51500 Vessel Packing Summ LST */
        return Arrays.asList(FormsEnum._COTTON_VESSEL_PACK_SUMM, FormsEnum._GRAIN_VESSEL_PACKING_SUMM, FormsEnum._GRAIN_VSSL_PACK_SUM_AQIS, FormsEnum._STA_STD, FormsEnum._STA_STDP, FormsEnum._VESSEL_PACKING_SUMMARY);
    }

    public static List<FormsEnum> getX33UpLocationBarcode() { /* 63908 33 up Location Barcode */
        return Arrays.asList(FormsEnum._CR33_LOCATION_BARCODE, FormsEnum._L7157_33_UP_AVERY_LBLS);
    }

    /**
     * Check
     */

    public static boolean isAllValues(FormsEnum forms) {  /* 19857 *ALL values */
        return FormsEnum.getAllValues().contains(forms);
    }

    public static boolean isBarcodeLabels(FormsEnum forms) {  /* 38025 Barcode Labels */
        return FormsEnum.getBarcodeLabels().contains(forms);
    }

    public static boolean isGrainVpsum(FormsEnum forms) {  /* 56689 Grain VPSUM */
        return FormsEnum.getGrainVpsum().contains(forms);
    }

    public static boolean isLabels(FormsEnum forms) {  /* 25079 Labels */
        return FormsEnum.getLabels().contains(forms);
    }

    public static boolean isOldReceivalLoad(FormsEnum forms) {  /* 45544 Old Receival Load */
        return FormsEnum.getOldReceivalLoad().contains(forms);
    }

    public static boolean isRackingLabels(FormsEnum forms) {  /* 62067 Racking Labels */
        return FormsEnum.getRackingLabels().contains(forms);
    }

    public static boolean isSfBoxLabels(FormsEnum forms) {  /* 37966 SF BOX Labels */
        return FormsEnum.getSfBoxLabels().contains(forms);
    }

    public static boolean isVesselPackingSummLst(FormsEnum forms) {  /* 51500 Vessel Packing Summ LST */
        return FormsEnum.getVesselPackingSummLst().contains(forms);
    }

    public static boolean isX33UpLocationBarcode(FormsEnum forms) {  /* 63908 33 up Location Barcode */
        return FormsEnum.getX33UpLocationBarcode().contains(forms);
    }
}