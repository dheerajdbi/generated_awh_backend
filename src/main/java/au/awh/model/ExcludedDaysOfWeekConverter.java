package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ExcludedDaysOfWeekEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ExcludedDaysOfWeekConverter implements AttributeConverter<ExcludedDaysOfWeekEnum, String> {

	@Override
	public String convertToDatabaseColumn(ExcludedDaysOfWeekEnum excludedDaysOfWeek) {
		if (excludedDaysOfWeek == null) {
			return "";
		}

		return excludedDaysOfWeek.getCode();
	}

	@Override
	public ExcludedDaysOfWeekEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ExcludedDaysOfWeekEnum.fromCode("");
		}

		return ExcludedDaysOfWeekEnum.fromCode(StringUtils.strip(dbData));
	}
}
