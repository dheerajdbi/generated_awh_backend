package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReturnCodeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReturnCodeConverter implements AttributeConverter<ReturnCodeEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReturnCodeEnum returnCode) {
		if (returnCode == null) {
			return "";
		}

		return returnCode.getCode();
	}

	@Override
	public ReturnCodeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReturnCodeEnum.fromCode("");
		}

		return ReturnCodeEnum.fromCode(StringUtils.strip(dbData));
	}
}
