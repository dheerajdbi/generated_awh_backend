package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for SelectedDaysDatesEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class SelectedDaysDatesConverter implements AttributeConverter<SelectedDaysDatesEnum, String> {

	@Override
	public String convertToDatabaseColumn(SelectedDaysDatesEnum selectedDaysDates) {
		if (selectedDaysDates == null) {
			return "";
		}

		return selectedDaysDates.getCode();
	}

	@Override
	public SelectedDaysDatesEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return SelectedDaysDatesEnum.fromCode("");
		}

		return SelectedDaysDatesEnum.fromCode(StringUtils.strip(dbData));
	}
}
