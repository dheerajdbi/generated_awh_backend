package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for LastUsedSurrogateTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class LastUsedSurrogateTypeConverter implements AttributeConverter<LastUsedSurrogateTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(LastUsedSurrogateTypeEnum lastUsedSurrogateType) {
		if (lastUsedSurrogateType == null) {
			return "";
		}

		return lastUsedSurrogateType.getCode();
	}

	@Override
	public LastUsedSurrogateTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return LastUsedSurrogateTypeEnum.fromCode("");
		}

		return LastUsedSurrogateTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
