package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for LastUsedSurrogateType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum LastUsedSurrogateTypeEnum {  // 7115 STS LSTSGTT Last Used Surrogate Type

	_LOCATION_SURROGATE_PART_1("AASG 1", "Location Surrogate Part 1") /* 36326 */ ,
	_LOCATION_SURROGATE_PART_2("AASG 2", "Location Surrogate Part 2") /* 36327 */ ,
	_LOCATION_ITEM_SGT_PART_1("ABSG 1", "Location Item Sgt Part 1") /* 36344 */ ,
	_LOCATION_ITEM_SGT_PART_2("ABSG 2", "Location Item Sgt Part 2") /* 36345 */ ,
	_ACCOUNT_TRANSACTION_SGT("ACCTRN", "Account Transaction Sgt") /* 42841 */ ,
	_ACTION_ITEM_SURROGATE("ACTITM", "Action Item Surrogate") /* 38387 */ ,
	_ACCOUNT_EVENT_SURROGATE("AEVTSGT", "Account Event Surrogate") /* 42625 */ ,
	_ACCOUNT_EXTRACT_SURROGATE("AEXTSGT", "Account Extract Surrogate") /* 44836 */ ,
	_AGENCY_LOT_NUMBER("AGENCYLOT", "Agency Lot Number") /* 63190 */ ,
	_ACCOUNT_INVOICE_SURROGATE("AINVSGT", "Account Invoice Surrogate") /* 44835 */ ,
	_BULK_CLASS_REFERENCE("BCREF", "Bulk Class Reference") /* 37285 */ ,
	_LOTTED_BALE_DELETE_AUDIT("BLDLTAUF", "Lotted Bale Delete Audit") /* 60208 */ ,
	_BULK_PRODUCT_BALE_NUMBER("BUBLNBRK", "Bulk Product Bale Number") /* 34925 */ ,
	_BUYER_STORAGE_CMK_SGT("BYSCMKSGT", "Buyer Storage CMK Sgt") /* 61025 */ ,
	_BUYER_STORAGE_FRE_SGT("BYSFRESGT", "Buyer Storage FRE Sgt") /* 61026 */ ,
	_BUYER_STORAGE_STG_SGT("BYSSTGSGT", "Buyer Storage STG Sgt") /* 61023 */ ,
	_DUMP_CONSIGN_ADVICE_SGT("CADVSGT", "Dump Consign Advice Sgt") /* 38778 */ ,
	_CONSIGNMENT_CONTAINER_SGT("CCTRSGT", "Consignment Container Sgt") /* 38843 */ ,
	_CTR_STG_ADVICE_SGT("CDADVH", "Ctr Stg Advice Sgt") /* 54786 */ ,
	_CTR_DEPOT_RELEASE_SGT("CDPCTRRLS", "Ctr Depot Release Sgt") /* 49550 */ ,
	_CTR_DEPOT_CONTAINER("CDPCTRSGT", "Ctr Depot Container") /* 49499 */ ,
	_CORE_LINE_PERFORMANCE("CLPERF", "Core Line Performance") /* 63344 */ ,
	_CORE_LINE_LOT_FSL_FOLIO_SGT("CLSGT", "Core Line Lot/Folio Sgt") /* 35520 */ ,
	_CLASSER_SPEC_SGT("CLSSPECSGT", "Classer Spec Sgt") /* 46764 */ ,
	_CLT_SALE_PROCEEDS("CLTSALPCDS", "Clt Sale Proceeds") /* 54037 */ ,
	_COUNTERMARK_WORD_NUMBER("CMWDNBR", "Countermark Word Number") /* 32743 */ ,
	_CO_DAS_LOCATED_WORK_SGT("COLOCSGT", "Co-located Work Sgt") /* 49011 */ ,
	_COTTON_BALE_SGT("COTTBALE", "Cotton Bale Sgt") /* 50675 */ ,
	_COTTON_BALE_REWEIGH_BTCH("COTTONRW", "Cotton Bale Reweigh Btch") /* 52106 */ ,
	_COTTON_WO_SGT("COTTONWO", "Cotton WO Sgt") /* 51742 */ ,
	_CTR_PARK_CONTAINER_SGT("CPCTRSGT", "Ctr Park Container Sgt") /* 44608 */ ,
	_CONTAINER_REQUEST_SLT_SGT("CRQSLT", "Container Request Slt Sgt") /* 43645 */ ,
	_CONSIGN_SUB_DAS_CONTAINER_SGT("CSCTRSGT", "Consign Sub-Container Sgt") /* 38862 */ ,
	_CTR_ACTIVUTY_SGT("CTRACT", "Ctr Activuty Sgt") /* 49606 */ ,
	_CONTAINER_ACTVTY_RATE_SGT("CTRACTRATE", "Container Actvty Rate Sgt") /* 54729 */ ,
	_COTTON_RCVL_LOAD_SGT("CTRCVLLDSG", "Cotton Rcvl Load sgt") /* 50509 */ ,
	_CONTAINER_REQUEST_SGT("CTRRQSSGT", "Container Request Sgt") /* 41878 */ ,
	_CONTAINER_SHIPPING_SGT("CTRSHIP", "Container Shipping Sgt") /* 41785 */ ,
	_COTTON_SUB_DAS_LOAD_SGT("CTSUBLOAD", "Cotton Sub-Load SGT") /* 52512 */ ,
	_CENTRE_TRANSFER_LOAD_NBR("D9NB", "Centre Transfer Load Nbr") /* 36927 */ ,
	_DALGETY_LOT_NUMBER("DALGETYLOT", "Dalgety Lot Number") /* 63470 */ ,
	_DUMP_BALE_EXCEPTION_SGT("DBESGT", "Dump Bale Exception Sgt") /* 39897 */ ,
	_DUMP_CONSIGNMENT_BALE_SGT("DCBLSGT", "Dump Consignment Bale Sgt") /* 38974 */ ,
	_DOCUMENT_CONNECTION_SGT("DCNNSGT", "Document Connection Sgt") /* 39219 */ ,
	_DUMP_CONSIGNMENT_LOT_SGT("DCONLOT", "Dump Consignment Lot Sgt") /* 38945 */ ,
	_DUMP_CONSIGNMENT_SGT("DCONSGT", "Dump Consignment Sgt") /* 38810 */ ,
	_DUMP_CONTAINER_TYPE_SGT("DCTYPSGT", "Dump Container Type Sgt") /* 38714 */ ,
	_WS_LIST_NUMBER("DDNB", "WS List Number") /* 35797 */ ,
	_DOCUMENT_DATA_SURROGATE("DDTASGT", "Document Data Surrogate") /* 39678 */ ,
	_DELIVERY_FOLIO_CTL_SGT("DFCSGT", "Delivery Folio Ctl Sgt") /* 43344 */ ,
	_DUMPING_MACHINE_SURROGATE("DMCHSGT", "Dumping Machine Surrogate") /* 38733 */ ,
	_DUMP_MASTER_CHARGE_SGT("DMCS", "Dump Master Charge Sgt") /* 42264 */ ,
	_DUMP_MARK_SURROGATE("DMRKSGT", "Dump Mark Surrogate") /* 38795 */ ,
	_DOCUMENT_MESSAGE_SGT("DMSSGT", "Document Message Sgt") /* 39629 */ ,
	_DUMMY_CLIENT_ACCOUNT_ID("DMYCLTACT", "Dummy Client Account Id") /* 50231 */ ,
	_DOCUMENT_SURROGATE("DOCSGT", "Document Surrogate") /* 39218 */ ,
	_DUMP_DEPOT_SURROGATE("DPTSGT", "Dump Depot Surrogate") /* 43459 */ ,
	_DUMP_PACK_UNIT_SURROGATE("DPUSGT", "Dump Pack Unit Surrogate") /* 38976 */ ,
	_DUMP_RECEIVAL_LOAD_SGT("DRLODSGT", "Dump Receival Load Sgt") /* 38752 */ ,
	_DELIVERY_REQUEST_NUMBER("DRQSNBR", "Delivery Request Number") /* 32705 */ ,
	_DUMP_CHARGE_RATE_SGT("DRSG", "Dump Charge Rate Sgt") /* 42511 */ ,
	_DUMP_RECV_SUB_DAS_LOAD_SGT("DRSLDSGT", "Dump Recv Sub-Load Sgt") /* 38878 */ ,
	_DOCUMENT_TYPE_CNN_SGT("DTCSGT", "Document Type Cnn Sgt") /* 39220 */ ,
	_DOC_TYPE_DISTR_LIST_SGT("DTDLSGT", "Doc Type Distr List Sgt") /* 43753 */ ,
	_DOCUMENT_TYPE_SURROGATE("DTYPSGT", "Document Type Surrogate") /* 39217 */ ,
	_DUMP_USER_ORG_SURROGATE("DUOSGT", "Dump User Org Surrogate") /* 39056 */ ,
	_DUMP_USER_SURROGATE("DUSRSGT", "Dump User Surrogate") /* 39046 */ ,
	_ACCOUNT_EVENT_CHARGE_SGT("ECHGSGT", "Account Event Charge Sgt") /* 42815 */ ,
	_CONSIGNMENT_ECN("ECN", "Consignment ECN") /* 48177 */ ,
	_ELECTRONIC_BALE_NUMBER("ELECBALE", "Electronic Bale Number") /* 34902 */ ,
	_EQUIPMENT_READING("EQPREAD", "Equipment reading") /* 58678 */ ,
	_EQUIPMENT_READING_CONTROL("ERCTRL", "Equipment Reading Control") /* 58698 */ ,
	_EXCEPTION_SURROGATE("EXCPTSGT", "Exception Surrogate") /* 38606 */ ,
	_EXTERNAL_LOAD_NUMBER("EXTLOAD", "External Load Number") /* 58940 */ ,
	_WOOL_LEVY_CLIENT_NO_ABN("FHNB", "Wool Levy Client No ABN") /* 37086 */ ,
	_FIELD_SURROGATE("FLDSGT", "Field Surrogate") /* 39320 */ ,
	_FOLIO_UNIQUE_ID("FOLID", "Folio Unique Id") /* 35361 */ ,
	_FOLIO_TRANSFER_SGT("FOLTRSGT", "Folio Transfer Sgt") /* 43760 */ ,
	_FREIGHT_REBATE_RULE("FRRBRUL", "Freight Rebate Rule") /* 32341 */ ,
	_GRAIN_DELIVERY_ORDER_SGT("GDOSGT", "Grain Delivery Order SGT") /* 56173 */ ,
	_MICHELL_COMMENT_SGT("GHMCOM", "Michell Comment Sgt") /* 40851 */ ,
	_MICHELL_LOT_BUILD_SGT("GHMLTB", "Michell Lot Build Sgt") /* 40850 */ ,
	_MICHELL_MOVEMENT_SGT("GHMMVT", "Michell Movement Sgt") /* 40803 */ ,
	_GRAIN_LOAD_SGT("GRAINLOAD", "Grain Load Sgt") /* 56227 */ ,
	_GRAIN_STOCK_TRANSACTION("GRSTKTRN", "Grain Stock transaction") /* 57330 */ ,
	_HELD_UP_SURROGATE("HELDUPSGT", "Held Up Surrogate") /* 65714 */ ,
	_ID_CODE_SURROGATE("IDCDSRG", "ID Code Surrogate") /* 33301 */ ,
	_ID_COMBINATION_SURROGATE("IDCOMSRG", "ID Combination Surrogate") /* 33426 */ ,
	_LABOUR_REPORT_GROUP_SGT("LBRRPTGRP", "Labour Report Group Sgt") /* 54604 */ ,
	_UN_FSL_LOCODE_SURROGATE("LCDSGT", "UN/LOCODE Surrogate") /* 38539 */ ,
	_LOAD_NUMBER("LODNBR", "Load Number") /* 32291 */ ,
	_LOT_UNIQUE_ID("LOTID", "Lot Unique Id") /* 35362 */ ,
	_LOT_NUMBER_KEY("LOTNBRK", "Lot Number            KEY") /* 34334 */ ,
	_LOT_PREVIOUS_OUR_SIM_SGT("LTPOSSGT", "Lot Previous Our Sim Sgt") /* 44902 */ ,
	_MARKED_SIM("MRKSIM", "Marked SIM") /* 45601 */ ,
	_DUMP_CHARGE_CODE_SGT("NMNB", "Dump Charge Code Sgt") /* 42393 */ ,
	_DUMP_CHARGE_RATE_SEQ_SGT("NNNB", "Dump Charge Rate Seq Sgt") /* 42495 */ ,
	_DUMP_CHG_RATE_TABLE_SGT("NPNB", "Dump Chg Rate Table Sgt") /* 42465 */ ,
	_DUMP_CHARGE_EXCEPTION_SGT("NRNB", "Dump Charge Exception Sgt") /* 42341 */ ,
	_OBJECT_ALIAS_SURROGATE("OBJASGT", "Object Alias Surrogate") /* 38562 */ ,
	_ORGANISATION_SURROGATE("ORGSGT", "Organisation Surrogate") /* 38687 */ ,
	_PICK_UP_ADVICE_SGT("PUADVICESG", "Pick Up Advice Sgt") /* 50476 */ ,
	_RECEIVAL_CARRIER_SGT("RCVLCARR", "Receival Carrier Sgt") /* 48123 */ ,
	_DOCUMENT_RCVM_LOG_SGT("RCVMSGT", "Document RCVM Log Sgt") /* 48723 */ ,
	_ORGANISATION_ACCOUNT_SGT("RTNB", "Organisation Account Sgt") /* 45804 */ ,
	_STRUCTURE_ENTRY_SURROGATE("STRENSGT", "Structure Entry Surrogate") /* 39323 */ ,
	_STRUCTURE_FIELD_SURROGATE("STRFLSGT", "Structure Field Surrogate") /* 39321 */ ,
	_STRUCTURE_SURROGATE("STRSGT", "Structure Surrogate") /* 39322 */ ,
	_TALMAN_CHG_OF_OWNER_SGT("TALCHOWN", "Talman Chg of Owner Sgt") /* 58122 */ ,
	_TALMAN_LOT_SGT("TALLOT", "Talman Lot Sgt") /* 58072 */ ,
	_TALMAN_DELIVERY_SGT("TDSGT", "Talman Delivery Sgt") /* 58346 */ ,
	_ACCOUNT_TRANS_EVENT_SGT("TEVTSGT", "Account Trans Event Sgt") /* 42884 */ ,
	_TALMAN_CLIENT_SGT("TMCLTSGT", "Talman Client Sgt") /* 58158 */ ,
	_TEMP_BC_BALE("TMPBCBLE", "Temp BC Bale") /* 44283 */ ,
	_TALMAN_RECEIVAL_SGT("TMRCVSGT", "Talman Receival Sgt") /* 58154 */ ,
	_TRANSMISSION_NUMBER_KEY("TMTNBRK", "Transmission Number   KEY") /* 34937 */ ,
	_TALMAN_XML_IMPORT_BATCH("TMX", "Talman XML Import Batch") /* 58069 */ ,
	_TALMAN_PAYMENT_CONF_SGT("TPCSRGT", "Talman Payment Conf Sgt") /* 58263 */ ,
	_ACCOUNT_TRANS_REVERSE_SGT("TRVSSGT", "Account Trans Reverse Sgt") /* 42966 */ ,
	_DUMP_TEST_RESULT("TSTRES", "Dump Test Result") /* 56926 */ ,
	_VESSEL_SURROGATE("VSLSGT", "Vessel Surrogate") /* 38503 */ ,
	_VOYAGE_SURROGATE("VYGSGT", "Voyage Surrogate") /* 38588 */ ,
	_WI_00_TRANSMISSION_NUMBER("WI00TRNN", "WI 00 Transmission Number") /* 31692 */ ,
	_WOOL_LEVY_SURROGATE("WOOLLEVY", "Wool Levy Surrogate") /* 36822 */ ,
	_XML_REHANDLE_DATA("XMLREHAND", "XML Rehandle Data") /* 63617 */ ;
    
	private final String code;
	private final String description;

	LastUsedSurrogateTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static LastUsedSurrogateTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<LastUsedSurrogateTypeEnum> getAllValues() { /* 31693 *ALL values */
        return Arrays.asList(LastUsedSurrogateTypeEnum._ACCOUNT_EVENT_CHARGE_SGT, LastUsedSurrogateTypeEnum._ACCOUNT_EVENT_SURROGATE, LastUsedSurrogateTypeEnum._ACCOUNT_EXTRACT_SURROGATE, LastUsedSurrogateTypeEnum._ACCOUNT_INVOICE_SURROGATE, LastUsedSurrogateTypeEnum._ACCOUNT_TRANS_EVENT_SGT, LastUsedSurrogateTypeEnum._ACCOUNT_TRANS_REVERSE_SGT, LastUsedSurrogateTypeEnum._ACCOUNT_TRANSACTION_SGT, LastUsedSurrogateTypeEnum._ACTION_ITEM_SURROGATE, LastUsedSurrogateTypeEnum._AGENCY_LOT_NUMBER, LastUsedSurrogateTypeEnum._BULK_CLASS_REFERENCE, LastUsedSurrogateTypeEnum._BULK_PRODUCT_BALE_NUMBER, LastUsedSurrogateTypeEnum._BUYER_STORAGE_CMK_SGT, LastUsedSurrogateTypeEnum._BUYER_STORAGE_FRE_SGT, LastUsedSurrogateTypeEnum._BUYER_STORAGE_STG_SGT, LastUsedSurrogateTypeEnum._CENTRE_TRANSFER_LOAD_NBR, LastUsedSurrogateTypeEnum._CLASSER_SPEC_SGT, LastUsedSurrogateTypeEnum._CLT_SALE_PROCEEDS, LastUsedSurrogateTypeEnum._CO_DAS_LOCATED_WORK_SGT, LastUsedSurrogateTypeEnum._CONSIGN_SUB_DAS_CONTAINER_SGT, LastUsedSurrogateTypeEnum._CONSIGNMENT_CONTAINER_SGT, LastUsedSurrogateTypeEnum._CONSIGNMENT_ECN, LastUsedSurrogateTypeEnum._CONTAINER_ACTVTY_RATE_SGT, LastUsedSurrogateTypeEnum._CONTAINER_REQUEST_SGT, LastUsedSurrogateTypeEnum._CONTAINER_REQUEST_SLT_SGT, LastUsedSurrogateTypeEnum._CONTAINER_SHIPPING_SGT, LastUsedSurrogateTypeEnum._CORE_LINE_LOT_FSL_FOLIO_SGT, LastUsedSurrogateTypeEnum._CORE_LINE_PERFORMANCE, LastUsedSurrogateTypeEnum._COTTON_BALE_REWEIGH_BTCH, LastUsedSurrogateTypeEnum._COTTON_BALE_SGT, LastUsedSurrogateTypeEnum._COTTON_RCVL_LOAD_SGT, LastUsedSurrogateTypeEnum._COTTON_SUB_DAS_LOAD_SGT, LastUsedSurrogateTypeEnum._COTTON_WO_SGT, LastUsedSurrogateTypeEnum._COUNTERMARK_WORD_NUMBER, LastUsedSurrogateTypeEnum._CTR_ACTIVUTY_SGT, LastUsedSurrogateTypeEnum._CTR_DEPOT_CONTAINER, LastUsedSurrogateTypeEnum._CTR_DEPOT_RELEASE_SGT, LastUsedSurrogateTypeEnum._CTR_PARK_CONTAINER_SGT, LastUsedSurrogateTypeEnum._CTR_STG_ADVICE_SGT, LastUsedSurrogateTypeEnum._DALGETY_LOT_NUMBER, LastUsedSurrogateTypeEnum._DELIVERY_FOLIO_CTL_SGT, LastUsedSurrogateTypeEnum._DELIVERY_REQUEST_NUMBER, LastUsedSurrogateTypeEnum._DOC_TYPE_DISTR_LIST_SGT, LastUsedSurrogateTypeEnum._DOCUMENT_CONNECTION_SGT, LastUsedSurrogateTypeEnum._DOCUMENT_DATA_SURROGATE, LastUsedSurrogateTypeEnum._DOCUMENT_MESSAGE_SGT, LastUsedSurrogateTypeEnum._DOCUMENT_RCVM_LOG_SGT, LastUsedSurrogateTypeEnum._DOCUMENT_SURROGATE, LastUsedSurrogateTypeEnum._DOCUMENT_TYPE_CNN_SGT, LastUsedSurrogateTypeEnum._DOCUMENT_TYPE_SURROGATE, LastUsedSurrogateTypeEnum._DUMMY_CLIENT_ACCOUNT_ID, LastUsedSurrogateTypeEnum._DUMP_BALE_EXCEPTION_SGT, LastUsedSurrogateTypeEnum._DUMP_CHARGE_CODE_SGT, LastUsedSurrogateTypeEnum._DUMP_CHARGE_EXCEPTION_SGT, LastUsedSurrogateTypeEnum._DUMP_CHARGE_RATE_SEQ_SGT, LastUsedSurrogateTypeEnum._DUMP_CHARGE_RATE_SGT, LastUsedSurrogateTypeEnum._DUMP_CHG_RATE_TABLE_SGT, LastUsedSurrogateTypeEnum._DUMP_CONSIGN_ADVICE_SGT, LastUsedSurrogateTypeEnum._DUMP_CONSIGNMENT_BALE_SGT, LastUsedSurrogateTypeEnum._DUMP_CONSIGNMENT_LOT_SGT, LastUsedSurrogateTypeEnum._DUMP_CONSIGNMENT_SGT, LastUsedSurrogateTypeEnum._DUMP_CONTAINER_TYPE_SGT, LastUsedSurrogateTypeEnum._DUMP_DEPOT_SURROGATE, LastUsedSurrogateTypeEnum._DUMP_MARK_SURROGATE, LastUsedSurrogateTypeEnum._DUMP_MASTER_CHARGE_SGT, LastUsedSurrogateTypeEnum._DUMP_PACK_UNIT_SURROGATE, LastUsedSurrogateTypeEnum._DUMP_RECEIVAL_LOAD_SGT, LastUsedSurrogateTypeEnum._DUMP_RECV_SUB_DAS_LOAD_SGT, LastUsedSurrogateTypeEnum._DUMP_TEST_RESULT, LastUsedSurrogateTypeEnum._DUMP_USER_ORG_SURROGATE, LastUsedSurrogateTypeEnum._DUMP_USER_SURROGATE, LastUsedSurrogateTypeEnum._DUMPING_MACHINE_SURROGATE, LastUsedSurrogateTypeEnum._ELECTRONIC_BALE_NUMBER, LastUsedSurrogateTypeEnum._EQUIPMENT_READING, LastUsedSurrogateTypeEnum._EQUIPMENT_READING_CONTROL, LastUsedSurrogateTypeEnum._EXCEPTION_SURROGATE, LastUsedSurrogateTypeEnum._EXTERNAL_LOAD_NUMBER, LastUsedSurrogateTypeEnum._FIELD_SURROGATE, LastUsedSurrogateTypeEnum._FOLIO_TRANSFER_SGT, LastUsedSurrogateTypeEnum._FOLIO_UNIQUE_ID, LastUsedSurrogateTypeEnum._FREIGHT_REBATE_RULE, LastUsedSurrogateTypeEnum._GRAIN_DELIVERY_ORDER_SGT, LastUsedSurrogateTypeEnum._GRAIN_LOAD_SGT, LastUsedSurrogateTypeEnum._GRAIN_STOCK_TRANSACTION, LastUsedSurrogateTypeEnum._HELD_UP_SURROGATE, LastUsedSurrogateTypeEnum._ID_CODE_SURROGATE, LastUsedSurrogateTypeEnum._ID_COMBINATION_SURROGATE, LastUsedSurrogateTypeEnum._LABOUR_REPORT_GROUP_SGT, LastUsedSurrogateTypeEnum._LOAD_NUMBER, LastUsedSurrogateTypeEnum._LOCATION_ITEM_SGT_PART_1, LastUsedSurrogateTypeEnum._LOCATION_ITEM_SGT_PART_2, LastUsedSurrogateTypeEnum._LOCATION_SURROGATE_PART_1, LastUsedSurrogateTypeEnum._LOCATION_SURROGATE_PART_2, LastUsedSurrogateTypeEnum._LOT_NUMBER_KEY, LastUsedSurrogateTypeEnum._LOT_PREVIOUS_OUR_SIM_SGT, LastUsedSurrogateTypeEnum._LOT_UNIQUE_ID, LastUsedSurrogateTypeEnum._LOTTED_BALE_DELETE_AUDIT, LastUsedSurrogateTypeEnum._MARKED_SIM, LastUsedSurrogateTypeEnum._MICHELL_COMMENT_SGT, LastUsedSurrogateTypeEnum._MICHELL_LOT_BUILD_SGT, LastUsedSurrogateTypeEnum._MICHELL_MOVEMENT_SGT, LastUsedSurrogateTypeEnum._OBJECT_ALIAS_SURROGATE, LastUsedSurrogateTypeEnum._ORGANISATION_ACCOUNT_SGT, LastUsedSurrogateTypeEnum._ORGANISATION_SURROGATE, LastUsedSurrogateTypeEnum._PICK_UP_ADVICE_SGT, LastUsedSurrogateTypeEnum._RECEIVAL_CARRIER_SGT, LastUsedSurrogateTypeEnum._STRUCTURE_ENTRY_SURROGATE, LastUsedSurrogateTypeEnum._STRUCTURE_FIELD_SURROGATE, LastUsedSurrogateTypeEnum._STRUCTURE_SURROGATE, LastUsedSurrogateTypeEnum._TALMAN_CHG_OF_OWNER_SGT, LastUsedSurrogateTypeEnum._TALMAN_CLIENT_SGT, LastUsedSurrogateTypeEnum._TALMAN_DELIVERY_SGT, LastUsedSurrogateTypeEnum._TALMAN_LOT_SGT, LastUsedSurrogateTypeEnum._TALMAN_PAYMENT_CONF_SGT, LastUsedSurrogateTypeEnum._TALMAN_RECEIVAL_SGT, LastUsedSurrogateTypeEnum._TALMAN_XML_IMPORT_BATCH, LastUsedSurrogateTypeEnum._TEMP_BC_BALE, LastUsedSurrogateTypeEnum._TRANSMISSION_NUMBER_KEY, LastUsedSurrogateTypeEnum._UN_FSL_LOCODE_SURROGATE, LastUsedSurrogateTypeEnum._VESSEL_SURROGATE, LastUsedSurrogateTypeEnum._VOYAGE_SURROGATE, LastUsedSurrogateTypeEnum._WI_00_TRANSMISSION_NUMBER, LastUsedSurrogateTypeEnum._WOOL_LEVY_CLIENT_NO_ABN, LastUsedSurrogateTypeEnum._WOOL_LEVY_SURROGATE, LastUsedSurrogateTypeEnum._WS_LIST_NUMBER, LastUsedSurrogateTypeEnum._XML_REHANDLE_DATA);
    }

    /**
     * Check
     */

    public static boolean isAllValues(LastUsedSurrogateTypeEnum lastUsedSurrogateType) {  /* 31693 *ALL values */
        return LastUsedSurrogateTypeEnum.getAllValues().contains(lastUsedSurrogateType);
    }
}