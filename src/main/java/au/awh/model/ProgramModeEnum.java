package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ProgramMode.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ProgramModeEnum {  // 12 STS PMD *Program mode

	_STA_ADD("ADD", "*ADD") /* 15518 */ ,
	_STA_AUTO("AUT", "*AUTO") /* 15547 */ ,
	_STA_CHANGE("CHG", "*CHANGE") /* 15520 */ ,
	_STA_COPY("CPY", "*COPY") /* 58439 */ ,
	_STA_DELETE("DEL", "*DELETE") /* 58440 */ ,
	_STA_DISPLAY("DSP", "*DISPLAY") /* 15522 */ ,
	_STA_ENTER("ENT", "*ENTER") /* 15546 */ ,
	_STA_NEW("NEW", "*NEW") /* 15637 */ ,
	_STA_OPEN("OPN", "*OPEN") /* 15638 */ ,
	_STA_SELECT("SEL", "*SELECT") /* 15521 */ ;
    
	private final String code;
	private final String description;

	ProgramModeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ProgramModeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ProgramModeEnum> getAllValues() { /* 15519 *ALL values */
        return Arrays.asList(ProgramModeEnum._STA_ADD, ProgramModeEnum._STA_AUTO, ProgramModeEnum._STA_CHANGE, ProgramModeEnum._STA_COPY, ProgramModeEnum._STA_DELETE, ProgramModeEnum._STA_DISPLAY, ProgramModeEnum._STA_ENTER, ProgramModeEnum._STA_NEW, ProgramModeEnum._STA_OPEN, ProgramModeEnum._STA_SELECT);
    }

    public static List<ProgramModeEnum> getNoAttributeChangeAllow() { /* 58448 No Attribute Change Allow */
        return Arrays.asList(ProgramModeEnum._STA_DELETE, ProgramModeEnum._STA_DISPLAY);
    }

    public static List<ProgramModeEnum> getNoKeyChangeAllowed() { /* 58447 No Key Change Allowed */
        return Arrays.asList(ProgramModeEnum._STA_CHANGE, ProgramModeEnum._STA_DELETE, ProgramModeEnum._STA_DISPLAY);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ProgramModeEnum programMode) {  /* 15519 *ALL values */
        return ProgramModeEnum.getAllValues().contains(programMode);
    }

    public static boolean isNoAttributeChangeAllow(ProgramModeEnum programMode) {  /* 58448 No Attribute Change Allow */
        return ProgramModeEnum.getNoAttributeChangeAllow().contains(programMode);
    }

    public static boolean isNoKeyChangeAllowed(ProgramModeEnum programMode) {  /* 58447 No Key Change Allowed */
        return ProgramModeEnum.getNoKeyChangeAllowed().contains(programMode);
    }
}