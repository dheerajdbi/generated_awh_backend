package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for HoldClFormatyesno.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum HoldClFormatyesnoEnum {  // 1706 STS HLDCLFMT Hold (CL Format *YES/*NO)

	_NOT_ENTERED("", "Not entered") /* 35903 */ ,
	_STA_NO("*NO", "*NO") /* 19916 */ ,
	_STA_YES("*YES", "*YES") /* 19914 */ ;
    
	private final String code;
	private final String description;

	HoldClFormatyesnoEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static HoldClFormatyesnoEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<HoldClFormatyesnoEnum> getAllValues() { /* 19915 *ALL values */
        return Arrays.asList(HoldClFormatyesnoEnum._STA_NO, HoldClFormatyesnoEnum._NOT_ENTERED, HoldClFormatyesnoEnum._STA_YES);
    }

    public static List<HoldClFormatyesnoEnum> getYesOrNo() { /* 41917 YES OR NO */
        return Arrays.asList(HoldClFormatyesnoEnum._STA_NO, HoldClFormatyesnoEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(HoldClFormatyesnoEnum holdClFormatyesno) {  /* 19915 *ALL values */
        return HoldClFormatyesnoEnum.getAllValues().contains(holdClFormatyesno);
    }

    public static boolean isYesOrNo(HoldClFormatyesnoEnum holdClFormatyesno) {  /* 41917 YES OR NO */
        return HoldClFormatyesnoEnum.getYesOrNo().contains(holdClFormatyesno);
    }
}