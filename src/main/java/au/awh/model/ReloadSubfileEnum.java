package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReloadSubfile.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReloadSubfileEnum {  // 43 STS RSF *Reload subfile

	_STA_NO("N", "*NO") /* 15511 */ ,
	_STA_YES("Y", "*YES") /* 15509 */ ;
    
	private final String code;
	private final String description;

	ReloadSubfileEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReloadSubfileEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReloadSubfileEnum> getAllValues() { /* 15510 *ALL values */
        return Arrays.asList(ReloadSubfileEnum._STA_NO, ReloadSubfileEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReloadSubfileEnum reloadSubfile) {  /* 15510 *ALL values */
        return ReloadSubfileEnum.getAllValues().contains(reloadSubfile);
    }
}