package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Rounded.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum RoundedEnum {  // 33 STS CRN *Rounded

	_STA_("", "*") /* 15479 */ ,
	_STA_ROUNDED("H", "*Rounded") /* 15477 */ ;
    
	private final String code;
	private final String description;

	RoundedEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static RoundedEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<RoundedEnum> getAllValues() { /* 15478 *ALL values */
        return Arrays.asList(RoundedEnum._STA_, RoundedEnum._STA_ROUNDED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(RoundedEnum rounded) {  /* 15478 *ALL values */
        return RoundedEnum.getAllValues().contains(rounded);
    }
}