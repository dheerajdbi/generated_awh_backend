package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for OrganisationStatusEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class OrganisationStatusConverter implements AttributeConverter<OrganisationStatusEnum, String> {

	@Override
	public String convertToDatabaseColumn(OrganisationStatusEnum organisationStatus) {
		if (organisationStatus == null) {
			return "";
		}

		return organisationStatus.getCode();
	}

	@Override
	public OrganisationStatusEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return OrganisationStatusEnum.fromCode("");
		}

		return OrganisationStatusEnum.fromCode(StringUtils.strip(dbData));
	}
}
