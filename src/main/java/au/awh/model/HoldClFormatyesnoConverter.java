package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for HoldClFormatyesnoEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class HoldClFormatyesnoConverter implements AttributeConverter<HoldClFormatyesnoEnum, String> {

	@Override
	public String convertToDatabaseColumn(HoldClFormatyesnoEnum holdClFormatyesno) {
		if (holdClFormatyesno == null) {
			return "";
		}

		return holdClFormatyesno.getCode();
	}

	@Override
	public HoldClFormatyesnoEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return HoldClFormatyesnoEnum.fromCode("");
		}

		return HoldClFormatyesnoEnum.fromCode(StringUtils.strip(dbData));
	}
}
