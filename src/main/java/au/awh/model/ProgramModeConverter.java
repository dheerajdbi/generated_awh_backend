package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ProgramModeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ProgramModeConverter implements AttributeConverter<ProgramModeEnum, String> {

	@Override
	public String convertToDatabaseColumn(ProgramModeEnum programMode) {
		if (programMode == null) {
			return "";
		}

		return programMode.getCode();
	}

	@Override
	public ProgramModeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ProgramModeEnum.fromCode("");
		}

		return ProgramModeEnum.fromCode(StringUtils.strip(dbData));
	}
}
