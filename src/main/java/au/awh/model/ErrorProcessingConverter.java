package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ErrorProcessingEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ErrorProcessingConverter implements AttributeConverter<ErrorProcessingEnum, String> {

	@Override
	public String convertToDatabaseColumn(ErrorProcessingEnum errorProcessing) {
		if (errorProcessing == null) {
			return "";
		}

		return errorProcessing.getCode();
	}

	@Override
	public ErrorProcessingEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ErrorProcessingEnum.fromCode("");
		}

		return ErrorProcessingEnum.fromCode(StringUtils.strip(dbData));
	}
}
