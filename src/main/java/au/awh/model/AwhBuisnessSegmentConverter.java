package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for AwhBuisnessSegmentEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class AwhBuisnessSegmentConverter implements AttributeConverter<AwhBuisnessSegmentEnum, String> {

	@Override
	public String convertToDatabaseColumn(AwhBuisnessSegmentEnum awhBuisnessSegment) {
		if (awhBuisnessSegment == null) {
			return "";
		}

		return awhBuisnessSegment.getCode();
	}

	@Override
	public AwhBuisnessSegmentEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return AwhBuisnessSegmentEnum.fromCode("");
		}

		return AwhBuisnessSegmentEnum.fromCode(StringUtils.strip(dbData));
	}
}
