package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ReadingFrequency.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ReadingFrequencyEnum {  // 15101 STS READFQ Reading Frequency

	_DAILY("", "Daily") /* 64185 */ ,
	_FORTNIGHTLY("F", "Fortnightly") /* 64189 */ ,
	_MONTHLY("M", "Monthly") /* 64188 */ ,
	_WEEKLY("W", "Weekly") /* 64187 */ ;
    
	private final String code;
	private final String description;

	ReadingFrequencyEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ReadingFrequencyEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ReadingFrequencyEnum> getAllValues() { /* 64186 *ALL values */
        return Arrays.asList(ReadingFrequencyEnum._DAILY, ReadingFrequencyEnum._FORTNIGHTLY, ReadingFrequencyEnum._MONTHLY, ReadingFrequencyEnum._WEEKLY);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ReadingFrequencyEnum readingFrequency) {  /* 64186 *ALL values */
        return ReadingFrequencyEnum.getAllValues().contains(readingFrequency);
    }
}