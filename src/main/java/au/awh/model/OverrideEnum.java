package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Override.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum OverrideEnum {  // 143 STS OVR *Override

	_STA_NO("0", "*NO") /* 15918 */ ,
	_STA_YES("1", "*YES") /* 15916 */ ;
    
	private final String code;
	private final String description;

	OverrideEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static OverrideEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<OverrideEnum> getAllValues() { /* 15917 *ALL values */
        return Arrays.asList(OverrideEnum._STA_NO, OverrideEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(OverrideEnum override) {  /* 15917 *ALL values */
        return OverrideEnum.getAllValues().contains(override);
    }
}