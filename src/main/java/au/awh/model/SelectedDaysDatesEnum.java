package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for SelectedDaysDates.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SelectedDaysDatesEnum {  // 196 STS DSE *Selected days/dates

	_STA_NO("", "*NO") /* 16069 */ ,
	_STA_EXCLUDED("0", "*EXCLUDED") /* 16071 */ ,
	_STA_INCLUDED("1", "*INCLUDED") /* 16072 */ ;
    
	private final String code;
	private final String description;

	SelectedDaysDatesEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SelectedDaysDatesEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SelectedDaysDatesEnum> getAllValues() { /* 16070 *ALL values */
        return Arrays.asList(SelectedDaysDatesEnum._STA_EXCLUDED, SelectedDaysDatesEnum._STA_INCLUDED, SelectedDaysDatesEnum._STA_NO);
    }

    /**
     * Check
     */

    public static boolean isAllValues(SelectedDaysDatesEnum selectedDaysDates) {  /* 16070 *ALL values */
        return SelectedDaysDatesEnum.getAllValues().contains(selectedDaysDates);
    }
}