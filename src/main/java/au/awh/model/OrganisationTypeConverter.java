package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for OrganisationTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class OrganisationTypeConverter implements AttributeConverter<OrganisationTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(OrganisationTypeEnum organisationType) {
		if (organisationType == null) {
			return "";
		}

		return organisationType.getCode();
	}

	@Override
	public OrganisationTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return OrganisationTypeEnum.fromCode("");
		}

		return OrganisationTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
