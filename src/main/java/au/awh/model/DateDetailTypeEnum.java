package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DateDetailType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DateDetailTypeEnum {  // 187 STS DET *Date detail type

	_STA_ABSOLUTE_DAY("AB", "*ABSOLUTE DAY") /* 16040 */ ,
	_STA_DAY_OF_MONTH("DM", "*DAY OF MONTH") /* 16042 */ ,
	_STA_DAY_OF_WEEK("DW", "*DAY OF WEEK") /* 16043 */ ,
	_STA_DAY_OF_YEAR("DY", "*DAY OF YEAR") /* 16044 */ ,
	_STA_LEAP_YEAR_QUE_("LY", "*LEAP YEAR ?") /* 16045 */ ,
	_STA_MONTH_LENGTH("ML", "*MONTH LENGTH") /* 16047 */ ,
	_STA_MONTH("MO", "*MONTH") /* 16046 */ ,
	_STA_SELECTED_QUE_("SE", "*SELECTED ?") /* 16048 */ ,
	_STA_YEAR_LENGTH("YL", "*YEAR LENGTH") /* 16050 */ ,
	_STA_YEAR("YR", "*YEAR") /* 16049 */ ;
    
	private final String code;
	private final String description;

	DateDetailTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DateDetailTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DateDetailTypeEnum> getAllValues() { /* 16041 *ALL values */
        return Arrays.asList(DateDetailTypeEnum._STA_ABSOLUTE_DAY, DateDetailTypeEnum._STA_DAY_OF_MONTH, DateDetailTypeEnum._STA_DAY_OF_WEEK, DateDetailTypeEnum._STA_DAY_OF_YEAR, DateDetailTypeEnum._STA_LEAP_YEAR_QUE_, DateDetailTypeEnum._STA_MONTH, DateDetailTypeEnum._STA_MONTH_LENGTH, DateDetailTypeEnum._STA_SELECTED_QUE_, DateDetailTypeEnum._STA_YEAR, DateDetailTypeEnum._STA_YEAR_LENGTH);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DateDetailTypeEnum dateDetailType) {  /* 16041 *ALL values */
        return DateDetailTypeEnum.getAllValues().contains(dateDetailType);
    }
}