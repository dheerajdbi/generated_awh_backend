package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for WsListFieldIdEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class WsListFieldIdConverter implements AttributeConverter<WsListFieldIdEnum, String> {

	@Override
	public String convertToDatabaseColumn(WsListFieldIdEnum wsListFieldId) {
		if (wsListFieldId == null) {
			return "";
		}

		return wsListFieldId.getCode();
	}

	@Override
	public WsListFieldIdEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return WsListFieldIdEnum.fromCode("");
		}

		return WsListFieldIdEnum.fromCode(StringUtils.strip(dbData));
	}
}
