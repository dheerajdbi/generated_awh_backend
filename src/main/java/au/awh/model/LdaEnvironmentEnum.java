package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for LdaEnvironment.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum LdaEnvironmentEnum {  // 749 STS LDAENV LDA Environment

	_DEVELOPMENT("D", "Development") /* 16806 */ ,
	_PRODUCTION("P", "Production") /* 16808 */ ,
	_TEST("T", "Test") /* 16809 */ ;
    
	private final String code;
	private final String description;

	LdaEnvironmentEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static LdaEnvironmentEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<LdaEnvironmentEnum> getAllValues() { /* 16807 *ALL values */
        return Arrays.asList(LdaEnvironmentEnum._DEVELOPMENT, LdaEnvironmentEnum._PRODUCTION, LdaEnvironmentEnum._TEST);
    }

    /**
     * Check
     */

    public static boolean isAllValues(LdaEnvironmentEnum ldaEnvironment) {  /* 16807 *ALL values */
        return LdaEnvironmentEnum.getAllValues().contains(ldaEnvironment);
    }
}