package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for GetRecordOptionEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class GetRecordOptionConverter implements AttributeConverter<GetRecordOptionEnum, String> {

	@Override
	public String convertToDatabaseColumn(GetRecordOptionEnum getRecordOption) {
		if (getRecordOption == null) {
			return "";
		}

		return getRecordOption.getCode();
	}

	@Override
	public GetRecordOptionEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return GetRecordOptionEnum.fromCode("");
		}

		return GetRecordOptionEnum.fromCode(StringUtils.strip(dbData));
	}
}
