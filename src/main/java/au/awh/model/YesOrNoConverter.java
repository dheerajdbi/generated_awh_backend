package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for YesOrNoEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class YesOrNoConverter implements AttributeConverter<YesOrNoEnum, String> {

	@Override
	public String convertToDatabaseColumn(YesOrNoEnum yesOrNo) {
		if (yesOrNo == null) {
			return "";
		}

		return yesOrNo.getCode();
	}

	@Override
	public YesOrNoEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return YesOrNoEnum.fromCode("");
		}

		return YesOrNoEnum.fromCode(StringUtils.strip(dbData));
	}
}
