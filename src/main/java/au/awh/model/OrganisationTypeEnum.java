package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for OrganisationType.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum OrganisationTypeEnum {  // 8800 STS ORGTYP Organisation Type

	_AGENT_FSL_BROKER("A", "Agent/Broker") /* 38609 */ ,
	_CARRIER("C", "Carrier") /* 38611 */ ,
	_DUMP("D", "Dump") /* 38613 */ ,
	_EXPORTER_FSL_BUYER("E", "Exporter/Buyer") /* 38612 */ ,
	_GIN("G", "Gin") /* 50482 */ ,
	_MISCELLANEOUS("M", "Miscellaneous") /* 38614 */ ,
	_PORT("P", "Port") /* 38615 */ ,
	_SHIPPING("S", "Shipping") /* 38616 */ ,
	_TEST_HOUSE("T", "Test House") /* 38617 */ ,
	_VESSEL("V", "Vessel") /* 38618 */ ,
	_WOOL_HANDLER("W", "Wool Handler") /* 58550 */ ,
	_COTTON_MERCHANT("X", "Cotton Merchant") /* 50480 */ ;
    
	private final String code;
	private final String description;

	OrganisationTypeEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static OrganisationTypeEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<OrganisationTypeEnum> getAllValues() { /* 38610 *ALL values */
        return Arrays.asList(OrganisationTypeEnum._AGENT_FSL_BROKER, OrganisationTypeEnum._CARRIER, OrganisationTypeEnum._COTTON_MERCHANT, OrganisationTypeEnum._DUMP, OrganisationTypeEnum._EXPORTER_FSL_BUYER, OrganisationTypeEnum._GIN, OrganisationTypeEnum._MISCELLANEOUS, OrganisationTypeEnum._PORT, OrganisationTypeEnum._SHIPPING, OrganisationTypeEnum._TEST_HOUSE, OrganisationTypeEnum._VESSEL, OrganisationTypeEnum._WOOL_HANDLER);
    }

    public static List<OrganisationTypeEnum> getCottonOrgs() { /* 50534 Cotton Orgs */
        return Arrays.asList(OrganisationTypeEnum._CARRIER, OrganisationTypeEnum._COTTON_MERCHANT, OrganisationTypeEnum._GIN);
    }

    public static List<OrganisationTypeEnum> getExporterOrMerchent() { /* 50905 Exporter or Merchent */
        return Arrays.asList(OrganisationTypeEnum._COTTON_MERCHANT, OrganisationTypeEnum._EXPORTER_FSL_BUYER, OrganisationTypeEnum._MISCELLANEOUS);
    }

    public static List<OrganisationTypeEnum> getShippingOrMisc() { /* 49469 Shipping or Misc */
        return Arrays.asList(OrganisationTypeEnum._MISCELLANEOUS, OrganisationTypeEnum._SHIPPING);
    }

    /**
     * Check
     */

    public static boolean isAllValues(OrganisationTypeEnum organisationType) {  /* 38610 *ALL values */
        return OrganisationTypeEnum.getAllValues().contains(organisationType);
    }

    public static boolean isCottonOrgs(OrganisationTypeEnum organisationType) {  /* 50534 Cotton Orgs */
        return OrganisationTypeEnum.getCottonOrgs().contains(organisationType);
    }

    public static boolean isExporterOrMerchent(OrganisationTypeEnum organisationType) {  /* 50905 Exporter or Merchent */
        return OrganisationTypeEnum.getExporterOrMerchent().contains(organisationType);
    }

    public static boolean isShippingOrMisc(OrganisationTypeEnum organisationType) {  /* 49469 Shipping or Misc */
        return OrganisationTypeEnum.getShippingOrMisc().contains(organisationType);
    }
}