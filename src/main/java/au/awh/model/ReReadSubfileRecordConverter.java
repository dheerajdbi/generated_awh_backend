package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for ReReadSubfileRecordEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class ReReadSubfileRecordConverter implements AttributeConverter<ReReadSubfileRecordEnum, String> {

	@Override
	public String convertToDatabaseColumn(ReReadSubfileRecordEnum reReadSubfileRecord) {
		if (reReadSubfileRecord == null) {
			return "";
		}

		return reReadSubfileRecord.getCode();
	}

	@Override
	public ReReadSubfileRecordEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return ReReadSubfileRecordEnum.fromCode("");
		}

		return ReReadSubfileRecordEnum.fromCode(StringUtils.strip(dbData));
	}
}
