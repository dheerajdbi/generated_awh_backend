package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for PrintEmailOrDisplayEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class PrintEmailOrDisplayConverter implements AttributeConverter<PrintEmailOrDisplayEnum, String> {

	@Override
	public String convertToDatabaseColumn(PrintEmailOrDisplayEnum printEmailOrDisplay) {
		if (printEmailOrDisplay == null) {
			return "";
		}

		return printEmailOrDisplay.getCode();
	}

	@Override
	public PrintEmailOrDisplayEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return PrintEmailOrDisplayEnum.fromCode("");
		}

		return PrintEmailOrDisplayEnum.fromCode(StringUtils.strip(dbData));
	}
}
