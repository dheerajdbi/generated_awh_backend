package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ExcludedDaysOfWeek.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ExcludedDaysOfWeekEnum {  // 193 STS DDO *Excluded days of week

	_STA_SATURDAY_COM_SUNDAY("1111100", "*SATURDAY,SUNDAY") /* 16067 */ ,
	_STA_SATURDAY("1111101", "*SATURDAY") /* 16066 */ ,
	_STA_SUNDAY("1111110", "*SUNDAY") /* 16068 */ ,
	_STA_NO("1111111", "*NO") /* 16064 */ ;
    
	private final String code;
	private final String description;

	ExcludedDaysOfWeekEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ExcludedDaysOfWeekEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ExcludedDaysOfWeekEnum> getAllValues() { /* 16065 *ALL values */
        return Arrays.asList(ExcludedDaysOfWeekEnum._STA_NO, ExcludedDaysOfWeekEnum._STA_SATURDAY, ExcludedDaysOfWeekEnum._STA_SATURDAY_COM_SUNDAY, ExcludedDaysOfWeekEnum._STA_SUNDAY);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ExcludedDaysOfWeekEnum excludedDaysOfWeek) {  /* 16065 *ALL values */
        return ExcludedDaysOfWeekEnum.getAllValues().contains(excludedDaysOfWeek);
    }
}