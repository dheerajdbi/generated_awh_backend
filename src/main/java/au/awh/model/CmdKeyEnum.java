package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for CmdKey.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum CmdKeyEnum {  // 27 STS CKY *CMD key

	_STA_NONE("00", "*NONE") /* 15836 */ ,
	_CF01("01", "CF01") /* 15446 */ ,
	_CF02("02", "CF02") /* 15448 */ ,
	_CF03("03", "CF03") /* 15449 */ ,
	_CF04("04", "CF04") /* 15450 */ ,
	_CF05("05", "CF05") /* 15451 */ ,
	_CF06("06", "CF06") /* 15452 */ ,
	_CF07("07", "CF07") /* 15453 */ ,
	_CF08("08", "CF08") /* 15454 */ ,
	_CF09("09", "CF09") /* 15455 */ ,
	_CF10("10", "CF10") /* 15456 */ ,
	_CF11("11", "CF11") /* 15457 */ ,
	_CF12("12", "CF12") /* 15458 */ ,
	_CF13("13", "CF13") /* 15459 */ ,
	_CF14("14", "CF14") /* 15460 */ ,
	_CF15("15", "CF15") /* 15461 */ ,
	_CF16("16", "CF16") /* 15462 */ ,
	_CF17("17", "CF17") /* 15463 */ ,
	_CF18("18", "CF18") /* 15464 */ ,
	_CF19("19", "CF19") /* 15465 */ ,
	_CF20("20", "CF20") /* 15466 */ ,
	_CF21("21", "CF21") /* 15467 */ ,
	_CF22("22", "CF22") /* 15468 */ ,
	_CF23("23", "CF23") /* 15469 */ ,
	_CF24("24", "CF24") /* 15470 */ ,
	_HELP("25", "HELP") /* 15568 */ ,
	_CLEAR("26", "CLEAR") /* 15844 */ ,
	_ROLLUP("27", "ROLLUP") /* 15565 */ ,
	_ROLLDOWN("28", "ROLLDOWN") /* 15566 */ ,
	_HOME("30", "HOME") /* 15567 */ ;
    
	private final String code;
	private final String description;

	CmdKeyEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static CmdKeyEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<CmdKeyEnum> getActions() { /* 15615 *Actions */
        return Arrays.asList(CmdKeyEnum._CF10);
    }

    public static List<CmdKeyEnum> getAdd() { /* 16787 Add */
        return Arrays.asList(CmdKeyEnum._CF06);
    }

    public static List<CmdKeyEnum> getAddOrganisation() { /* 42427 Add Organisation */
        return Arrays.asList(CmdKeyEnum._CF09);
    }

    public static List<CmdKeyEnum> getAllSelected() { /* 32702 All/Selected */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getAllValues() { /* 15447 *ALL values */
        return Arrays.asList(CmdKeyEnum._CF01, CmdKeyEnum._CF02, CmdKeyEnum._CF03, CmdKeyEnum._CF04, CmdKeyEnum._CF05, CmdKeyEnum._CF06, CmdKeyEnum._CF07, CmdKeyEnum._CF08, CmdKeyEnum._CF09, CmdKeyEnum._CF10, CmdKeyEnum._CF11, CmdKeyEnum._CF12, CmdKeyEnum._CF13, CmdKeyEnum._CF14, CmdKeyEnum._CF15, CmdKeyEnum._CF16, CmdKeyEnum._CF17, CmdKeyEnum._CF18, CmdKeyEnum._CF19, CmdKeyEnum._CF20, CmdKeyEnum._CF21, CmdKeyEnum._CF22, CmdKeyEnum._CF23, CmdKeyEnum._CF24, CmdKeyEnum._CLEAR, CmdKeyEnum._HELP, CmdKeyEnum._HOME, CmdKeyEnum._STA_NONE, CmdKeyEnum._ROLLDOWN, CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getAlternateView() { /* 33863 Alternate View */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getCancel() { /* 17540 Cancel */
        return Arrays.asList(CmdKeyEnum._CF12);
    }

    public static List<CmdKeyEnum> getCancelOrExit() { /* 36914 Cancel or Exit */
        return Arrays.asList(CmdKeyEnum._CF03, CmdKeyEnum._CF12);
    }

    public static List<CmdKeyEnum> getCancel_1() { /* 15622 *Cancel */
        return Arrays.asList(CmdKeyEnum._CF12);
    }

    public static List<CmdKeyEnum> getChangeBrokerCentre() { /* 31345 Change Broker/Centre */
        return Arrays.asList(CmdKeyEnum._CF22);
    }

    public static List<CmdKeyEnum> getChangeClipSale() { /* 35139 Change Clip Sale */
        return Arrays.asList(CmdKeyEnum._CF14);
    }

    public static List<CmdKeyEnum> getChangeMode() { /* 15561 *Change mode */
        return Arrays.asList(CmdKeyEnum._CF06);
    }

    public static List<CmdKeyEnum> getChangeRdb() { /* 15931 *Change RDB */
        return Arrays.asList(CmdKeyEnum._CF22);
    }

    public static List<CmdKeyEnum> getClearSelects() { /* 15840 *Clear selects */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getCommandEntryWindow() { /* 52866 Command entry window */
        return Arrays.asList(CmdKeyEnum._CF09, CmdKeyEnum._CF21);
    }

    public static List<CmdKeyEnum> getDelete() { /* 15562 *Delete */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getDisplayAudit() { /* 35491 Display Audit */
        return Arrays.asList(CmdKeyEnum._CF14);
    }

    public static List<CmdKeyEnum> getDisplayHideProcessed() { /* 32726 Display/Hide Processed */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getDisplayMessages() { /* 32103 Display Messages */
        return Arrays.asList(CmdKeyEnum._CF06);
    }

    public static List<CmdKeyEnum> getDisplayUserIndex() { /* 36543 Display User Index */
        return Arrays.asList(CmdKeyEnum._CF15);
    }

    public static List<CmdKeyEnum> getDuplicate() { /* 37452 Duplicate */
        return Arrays.asList(CmdKeyEnum._CF02);
    }

    public static List<CmdKeyEnum> getDuplicateField() { /* 27001 Duplicate Field */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getExit() { /* 15559 *Exit */
        return Arrays.asList(CmdKeyEnum._CF03);
    }

    public static List<CmdKeyEnum> getExpand() { /* 15619 *Expand */
        return Arrays.asList(CmdKeyEnum._CF20);
    }

    public static List<CmdKeyEnum> getExtract() { /* 24051 Extract */
        return Arrays.asList(CmdKeyEnum._CF08);
    }

    public static List<CmdKeyEnum> getF7f8() { /* 62874 F7F8 */
        return Arrays.asList(CmdKeyEnum._CF07, CmdKeyEnum._CF08);
    }

    public static List<CmdKeyEnum> getGoToaddMode() { /* 15571 *Go to 'Add' mode */
        return Arrays.asList(CmdKeyEnum._CF06);
    }

    public static List<CmdKeyEnum> getGoTochangeMode() { /* 15572 *Go to 'Change' mode */
        return Arrays.asList(CmdKeyEnum._CF06);
    }

    public static List<CmdKeyEnum> getHelp() { /* 15611 *Help */
        return Arrays.asList(CmdKeyEnum._CF01, CmdKeyEnum._HELP);
    }

    public static List<CmdKeyEnum> getHelpForHelp() { /* 15843 *Help for help */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getIdeographicConversion() { /* 15613 *Ideographic conversion */
        return Arrays.asList(CmdKeyEnum._CF18);
    }

    public static List<CmdKeyEnum> getKeyScreen() { /* 15560 *Key screen */
        return Arrays.asList(CmdKeyEnum._CF24);
    }

    public static List<CmdKeyEnum> getKeysUsedByObjectMove() { /* 54424 *Keys Used By Object Move */
        return Arrays.asList(CmdKeyEnum._CF03, CmdKeyEnum._CF09, CmdKeyEnum._CF10, CmdKeyEnum._CF11, CmdKeyEnum._CF12, CmdKeyEnum._CF17, CmdKeyEnum._CF21);
    }

    public static List<CmdKeyEnum> getLocationItems() { /* 37263 Location Items */
        return Arrays.asList(CmdKeyEnum._CF18);
    }

    public static List<CmdKeyEnum> getLocationPromptList() { /* 61253 Location Prompt list */
        return Arrays.asList(CmdKeyEnum._CF04, CmdKeyEnum._CF08, CmdKeyEnum._CF11, CmdKeyEnum._CF16, CmdKeyEnum._CF20);
    }

    public static List<CmdKeyEnum> getMessages() { /* 15841 *Messages */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getNext() { /* 15837 *Next */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getNextPage() { /* 15569 *Next page */
        return Arrays.asList(CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getOk() { /* 15842 *OK */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getPgupOrPageDown() { /* 36720 PgUp or Page Down */
        return Arrays.asList(CmdKeyEnum._ROLLDOWN, CmdKeyEnum._ROLLUP);
    }

    public static List<CmdKeyEnum> getPosition() { /* 15620 *Position */
        return Arrays.asList(CmdKeyEnum._CF17);
    }

    public static List<CmdKeyEnum> getPrevious() { /* 15838 *Previous */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getPreviousPage() { /* 15570 *Previous page */
        return Arrays.asList(CmdKeyEnum._ROLLDOWN);
    }

    public static List<CmdKeyEnum> getPrintReport() { /* 18115 Print Report */
        return Arrays.asList(CmdKeyEnum._CF10);
    }

    public static List<CmdKeyEnum> getPrintSheets() { /* 32703 Print Sheets */
        return Arrays.asList(CmdKeyEnum._CF16);
    }

    public static List<CmdKeyEnum> getPrompt() { /* 15614 *Prompt */
        return Arrays.asList(CmdKeyEnum._CF04);
    }

    public static List<CmdKeyEnum> getRefresh() { /* 16983 Refresh */
        return Arrays.asList(CmdKeyEnum._CF05);
    }

    public static List<CmdKeyEnum> getRepositionWindow() { /* 15845 *Reposition window */
        return Arrays.asList(CmdKeyEnum._CLEAR);
    }

    public static List<CmdKeyEnum> getReset() { /* 15610 *Reset */
        return Arrays.asList(CmdKeyEnum._CF05);
    }

    public static List<CmdKeyEnum> getResize() { /* 15623 *Resize */
        return Arrays.asList(CmdKeyEnum._CF19);
    }

    public static List<CmdKeyEnum> getSelect() { /* 17532 Select */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getSelectAll() { /* 15839 *Select all */
        return Arrays.asList(CmdKeyEnum._STA_NONE);
    }

    public static List<CmdKeyEnum> getSelectAll_1() { /* 25548 Select All */
        return Arrays.asList(CmdKeyEnum._CF11);
    }

    public static List<CmdKeyEnum> getSubmitToBatch() { /* 35778 Submit to Batch */
        return Arrays.asList(CmdKeyEnum._CF19);
    }

    public static List<CmdKeyEnum> getSubset() { /* 15621 *Subset */
        return Arrays.asList(CmdKeyEnum._CF17);
    }

    public static List<CmdKeyEnum> getUpdate() { /* 33920 Update */
        return Arrays.asList(CmdKeyEnum._CF10);
    }

    /**
     * Check
     */

    public static boolean isActions(CmdKeyEnum cmdKey) {  /* 15615 *Actions */
        return CmdKeyEnum.getActions().contains(cmdKey);
    }

    public static boolean isAdd(CmdKeyEnum cmdKey) {  /* 16787 Add */
        return CmdKeyEnum.getAdd().contains(cmdKey);
    }

    public static boolean isAddOrganisation(CmdKeyEnum cmdKey) {  /* 42427 Add Organisation */
        return CmdKeyEnum.getAddOrganisation().contains(cmdKey);
    }

    public static boolean isAllSelected(CmdKeyEnum cmdKey) {  /* 32702 All/Selected */
        return CmdKeyEnum.getAllSelected().contains(cmdKey);
    }

    public static boolean isAllValues(CmdKeyEnum cmdKey) {  /* 15447 *ALL values */
        return CmdKeyEnum.getAllValues().contains(cmdKey);
    }

    public static boolean isAlternateView(CmdKeyEnum cmdKey) {  /* 33863 Alternate View */
        return CmdKeyEnum.getAlternateView().contains(cmdKey);
    }

    public static boolean isCancel(CmdKeyEnum cmdKey) {  /* 17540 Cancel */
        return CmdKeyEnum.getCancel().contains(cmdKey);
    }

    public static boolean isCancelOrExit(CmdKeyEnum cmdKey) {  /* 36914 Cancel or Exit */
        return CmdKeyEnum.getCancelOrExit().contains(cmdKey);
    }

    public static boolean isCancel_1(CmdKeyEnum cmdKey) {  /* 15622 *Cancel */
        return CmdKeyEnum.getCancel_1().contains(cmdKey);
    }

    public static boolean isChangeBrokerCentre(CmdKeyEnum cmdKey) {  /* 31345 Change Broker/Centre */
        return CmdKeyEnum.getChangeBrokerCentre().contains(cmdKey);
    }

    public static boolean isChangeClipSale(CmdKeyEnum cmdKey) {  /* 35139 Change Clip Sale */
        return CmdKeyEnum.getChangeClipSale().contains(cmdKey);
    }

    public static boolean isChangeMode(CmdKeyEnum cmdKey) {  /* 15561 *Change mode */
        return CmdKeyEnum.getChangeMode().contains(cmdKey);
    }

    public static boolean isChangeRdb(CmdKeyEnum cmdKey) {  /* 15931 *Change RDB */
        return CmdKeyEnum.getChangeRdb().contains(cmdKey);
    }

    public static boolean isClearSelects(CmdKeyEnum cmdKey) {  /* 15840 *Clear selects */
        return CmdKeyEnum.getClearSelects().contains(cmdKey);
    }

    public static boolean isCommandEntryWindow(CmdKeyEnum cmdKey) {  /* 52866 Command entry window */
        return CmdKeyEnum.getCommandEntryWindow().contains(cmdKey);
    }

    public static boolean isDelete(CmdKeyEnum cmdKey) {  /* 15562 *Delete */
        return CmdKeyEnum.getDelete().contains(cmdKey);
    }

    public static boolean isDisplayAudit(CmdKeyEnum cmdKey) {  /* 35491 Display Audit */
        return CmdKeyEnum.getDisplayAudit().contains(cmdKey);
    }

    public static boolean isDisplayHideProcessed(CmdKeyEnum cmdKey) {  /* 32726 Display/Hide Processed */
        return CmdKeyEnum.getDisplayHideProcessed().contains(cmdKey);
    }

    public static boolean isDisplayMessages(CmdKeyEnum cmdKey) {  /* 32103 Display Messages */
        return CmdKeyEnum.getDisplayMessages().contains(cmdKey);
    }

    public static boolean isDisplayUserIndex(CmdKeyEnum cmdKey) {  /* 36543 Display User Index */
        return CmdKeyEnum.getDisplayUserIndex().contains(cmdKey);
    }

    public static boolean isDuplicate(CmdKeyEnum cmdKey) {  /* 37452 Duplicate */
        return CmdKeyEnum.getDuplicate().contains(cmdKey);
    }

    public static boolean isDuplicateField(CmdKeyEnum cmdKey) {  /* 27001 Duplicate Field */
        return CmdKeyEnum.getDuplicateField().contains(cmdKey);
    }

    public static boolean isExit(CmdKeyEnum cmdKey) {  /* 15559 *Exit */
        return CmdKeyEnum.getExit().contains(cmdKey);
    }

    public static boolean isExpand(CmdKeyEnum cmdKey) {  /* 15619 *Expand */
        return CmdKeyEnum.getExpand().contains(cmdKey);
    }

    public static boolean isExtract(CmdKeyEnum cmdKey) {  /* 24051 Extract */
        return CmdKeyEnum.getExtract().contains(cmdKey);
    }

    public static boolean isF7f8(CmdKeyEnum cmdKey) {  /* 62874 F7F8 */
        return CmdKeyEnum.getF7f8().contains(cmdKey);
    }

    public static boolean isGoToaddMode(CmdKeyEnum cmdKey) {  /* 15571 *Go to 'Add' mode */
        return CmdKeyEnum.getGoToaddMode().contains(cmdKey);
    }

    public static boolean isGoTochangeMode(CmdKeyEnum cmdKey) {  /* 15572 *Go to 'Change' mode */
        return CmdKeyEnum.getGoTochangeMode().contains(cmdKey);
    }

    public static boolean isHelp(CmdKeyEnum cmdKey) {  /* 15611 *Help */
        return CmdKeyEnum.getHelp().contains(cmdKey);
    }

    public static boolean isHelpForHelp(CmdKeyEnum cmdKey) {  /* 15843 *Help for help */
        return CmdKeyEnum.getHelpForHelp().contains(cmdKey);
    }

    public static boolean isIdeographicConversion(CmdKeyEnum cmdKey) {  /* 15613 *Ideographic conversion */
        return CmdKeyEnum.getIdeographicConversion().contains(cmdKey);
    }

    public static boolean isKeyScreen(CmdKeyEnum cmdKey) {  /* 15560 *Key screen */
        return CmdKeyEnum.getKeyScreen().contains(cmdKey);
    }

    public static boolean isKeysUsedByObjectMove(CmdKeyEnum cmdKey) {  /* 54424 *Keys Used By Object Move */
        return CmdKeyEnum.getKeysUsedByObjectMove().contains(cmdKey);
    }

    public static boolean isLocationItems(CmdKeyEnum cmdKey) {  /* 37263 Location Items */
        return CmdKeyEnum.getLocationItems().contains(cmdKey);
    }

    public static boolean isLocationPromptList(CmdKeyEnum cmdKey) {  /* 61253 Location Prompt list */
        return CmdKeyEnum.getLocationPromptList().contains(cmdKey);
    }

    public static boolean isMessages(CmdKeyEnum cmdKey) {  /* 15841 *Messages */
        return CmdKeyEnum.getMessages().contains(cmdKey);
    }

    public static boolean isNext(CmdKeyEnum cmdKey) {  /* 15837 *Next */
        return CmdKeyEnum.getNext().contains(cmdKey);
    }

    public static boolean isNextPage(CmdKeyEnum cmdKey) {  /* 15569 *Next page */
        return CmdKeyEnum.getNextPage().contains(cmdKey);
    }

    public static boolean isOk(CmdKeyEnum cmdKey) {  /* 15842 *OK */
        return CmdKeyEnum.getOk().contains(cmdKey);
    }

    public static boolean isPgupOrPageDown(CmdKeyEnum cmdKey) {  /* 36720 PgUp or Page Down */
        return CmdKeyEnum.getPgupOrPageDown().contains(cmdKey);
    }

    public static boolean isPosition(CmdKeyEnum cmdKey) {  /* 15620 *Position */
        return CmdKeyEnum.getPosition().contains(cmdKey);
    }

    public static boolean isPrevious(CmdKeyEnum cmdKey) {  /* 15838 *Previous */
        return CmdKeyEnum.getPrevious().contains(cmdKey);
    }

    public static boolean isPreviousPage(CmdKeyEnum cmdKey) {  /* 15570 *Previous page */
        return CmdKeyEnum.getPreviousPage().contains(cmdKey);
    }

    public static boolean isPrintReport(CmdKeyEnum cmdKey) {  /* 18115 Print Report */
        return CmdKeyEnum.getPrintReport().contains(cmdKey);
    }

    public static boolean isPrintSheets(CmdKeyEnum cmdKey) {  /* 32703 Print Sheets */
        return CmdKeyEnum.getPrintSheets().contains(cmdKey);
    }

    public static boolean isPrompt(CmdKeyEnum cmdKey) {  /* 15614 *Prompt */
        return CmdKeyEnum.getPrompt().contains(cmdKey);
    }

    public static boolean isRefresh(CmdKeyEnum cmdKey) {  /* 16983 Refresh */
        return CmdKeyEnum.getRefresh().contains(cmdKey);
    }

    public static boolean isRepositionWindow(CmdKeyEnum cmdKey) {  /* 15845 *Reposition window */
        return CmdKeyEnum.getRepositionWindow().contains(cmdKey);
    }

    public static boolean isReset(CmdKeyEnum cmdKey) {  /* 15610 *Reset */
        return CmdKeyEnum.getReset().contains(cmdKey);
    }

    public static boolean isResize(CmdKeyEnum cmdKey) {  /* 15623 *Resize */
        return CmdKeyEnum.getResize().contains(cmdKey);
    }

    public static boolean isSelect(CmdKeyEnum cmdKey) {  /* 17532 Select */
        return CmdKeyEnum.getSelect().contains(cmdKey);
    }

    public static boolean isSelectAll(CmdKeyEnum cmdKey) {  /* 15839 *Select all */
        return CmdKeyEnum.getSelectAll().contains(cmdKey);
    }

    public static boolean isSelectAll_1(CmdKeyEnum cmdKey) {  /* 25548 Select All */
        return CmdKeyEnum.getSelectAll_1().contains(cmdKey);
    }

    public static boolean isSubmitToBatch(CmdKeyEnum cmdKey) {  /* 35778 Submit to Batch */
        return CmdKeyEnum.getSubmitToBatch().contains(cmdKey);
    }

    public static boolean isSubset(CmdKeyEnum cmdKey) {  /* 15621 *Subset */
        return CmdKeyEnum.getSubset().contains(cmdKey);
    }

    public static boolean isUpdate(CmdKeyEnum cmdKey) {  /* 33920 Update */
        return CmdKeyEnum.getUpdate().contains(cmdKey);
    }
}