package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for ExitProgramOption.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum ExitProgramOptionEnum {  // 6901 STS EXPGOPT Exit Program Option

	_NORMAL("", "Normal") /* 31448 */ ,
	_CANCEL_REQUESTED("C", "Cancel requested") /* 31447 */ ,
	_EXIT_REQUESTED("E", "Exit requested") /* 31445 */ ,
	_NEXT_VIEW("N", "Next View") /* 43388 */ ,
	_PREVIOUS_VIEW("P", "Previous View") /* 43389 */ ;
    
	private final String code;
	private final String description;

	ExitProgramOptionEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static ExitProgramOptionEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<ExitProgramOptionEnum> getAllValues() { /* 31446 *ALL values */
        return Arrays.asList(ExitProgramOptionEnum._CANCEL_REQUESTED, ExitProgramOptionEnum._EXIT_REQUESTED, ExitProgramOptionEnum._NEXT_VIEW, ExitProgramOptionEnum._NORMAL, ExitProgramOptionEnum._PREVIOUS_VIEW);
    }

    public static List<ExitProgramOptionEnum> getCancelOrExit() { /* 36674 Cancel Or Exit */
        return Arrays.asList(ExitProgramOptionEnum._CANCEL_REQUESTED, ExitProgramOptionEnum._EXIT_REQUESTED);
    }

    public static List<ExitProgramOptionEnum> getNextOrPreviousView() { /* 43390 Next or Previous View */
        return Arrays.asList(ExitProgramOptionEnum._NEXT_VIEW, ExitProgramOptionEnum._PREVIOUS_VIEW);
    }

    /**
     * Check
     */

    public static boolean isAllValues(ExitProgramOptionEnum exitProgramOption) {  /* 31446 *ALL values */
        return ExitProgramOptionEnum.getAllValues().contains(exitProgramOption);
    }

    public static boolean isCancelOrExit(ExitProgramOptionEnum exitProgramOption) {  /* 36674 Cancel Or Exit */
        return ExitProgramOptionEnum.getCancelOrExit().contains(exitProgramOption);
    }

    public static boolean isNextOrPreviousView(ExitProgramOptionEnum exitProgramOption) {  /* 43390 Next or Previous View */
        return ExitProgramOptionEnum.getNextOrPreviousView().contains(exitProgramOption);
    }
}