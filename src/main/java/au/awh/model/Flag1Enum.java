package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for Flag1.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum Flag1Enum {  // 2938 STS FLG1U Flag 1

	_BLANK("", "Blank") /* 22504 */ ,
	_NO("N", "No") /* 23043 */ ,
	_EXIT("X", "Exit") /* 37905 */ ,
	_PRINTED("Y", "Printed") /* 22507 */ ;
    
	private final String code;
	private final String description;

	Flag1Enum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static Flag1Enum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<Flag1Enum> getAllValues() { /* 22503 *ALL values */
        return Arrays.asList(Flag1Enum._BLANK, Flag1Enum._EXIT, Flag1Enum._NO, Flag1Enum._PRINTED);
    }

    /**
     * Check
     */

    public static boolean isAllValues(Flag1Enum flag1) {  /* 22503 *ALL values */
        return Flag1Enum.getAllValues().contains(flag1);
    }
}