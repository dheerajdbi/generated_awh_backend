package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for DisplayFlagUsr.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum DisplayFlagUsrEnum {  // 1240 STS DSPFLGU Display flag          USR

	_STA_BLANK("", "*Blank") /* 21618 */ ,
	_NO("N", "NO") /* 18362 */ ,
	_YES("Y", "YES") /* 18360 */ ;
    
	private final String code;
	private final String description;

	DisplayFlagUsrEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static DisplayFlagUsrEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<DisplayFlagUsrEnum> getAllValues() { /* 18361 *ALL values */
        return Arrays.asList(DisplayFlagUsrEnum._STA_BLANK, DisplayFlagUsrEnum._NO, DisplayFlagUsrEnum._YES);
    }

    public static List<DisplayFlagUsrEnum> getNotYes() { /* 40039 Not Yes */
        return Arrays.asList(DisplayFlagUsrEnum._STA_BLANK, DisplayFlagUsrEnum._NO);
    }

    public static List<DisplayFlagUsrEnum> getYesOrNo() { /* 23096 Yes or No */
        return Arrays.asList(DisplayFlagUsrEnum._NO, DisplayFlagUsrEnum._YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(DisplayFlagUsrEnum displayFlagUsr) {  /* 18361 *ALL values */
        return DisplayFlagUsrEnum.getAllValues().contains(displayFlagUsr);
    }

    public static boolean isNotYes(DisplayFlagUsrEnum displayFlagUsr) {  /* 40039 Not Yes */
        return DisplayFlagUsrEnum.getNotYes().contains(displayFlagUsr);
    }

    public static boolean isYesOrNo(DisplayFlagUsrEnum displayFlagUsr) {  /* 23096 Yes or No */
        return DisplayFlagUsrEnum.getYesOrNo().contains(displayFlagUsr);
    }
}