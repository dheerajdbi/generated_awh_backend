package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for StateCodeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class StateCodeConverter implements AttributeConverter<StateCodeEnum, String> {

	@Override
	public String convertToDatabaseColumn(StateCodeEnum stateCode) {
		if (stateCode == null) {
			return "";
		}

		return stateCode.getCode();
	}

	@Override
	public StateCodeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return StateCodeEnum.fromCode("");
		}

		return StateCodeEnum.fromCode(StringUtils.strip(dbData));
	}
}
