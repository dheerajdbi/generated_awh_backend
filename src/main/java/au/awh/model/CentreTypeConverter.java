package au.awh.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;

/**
 * JPA Attribute Converter for CentreTypeEnum.
 *
 * @author X2EGenerator
 */
@Converter(autoApply=true)
public class CentreTypeConverter implements AttributeConverter<CentreTypeEnum, String> {

	@Override
	public String convertToDatabaseColumn(CentreTypeEnum centreType) {
		if (centreType == null) {
			return "";
		}

		return centreType.getCode();
	}

	@Override
	public CentreTypeEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return CentreTypeEnum.fromCode("");
		}

		return CentreTypeEnum.fromCode(StringUtils.strip(dbData));
	}
}
