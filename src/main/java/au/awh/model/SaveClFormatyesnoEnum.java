package au.awh.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * List of status for SaveClFormatyesno.
 *
 * @author X2EGenerator
 */
@JsonFormat(shape = Shape.SCALAR)
public enum SaveClFormatyesnoEnum {  // 1707 STS SAVCLFMT Save (CL Format *YES/*NO)

	_NOT_ENTERED("", "Not entered") /* 35904 */ ,
	_STA_NO("*NO", "*NO") /* 19919 */ ,
	_STA_YES("*YES", "*YES") /* 19917 */ ;
    
	private final String code;
	private final String description;

	SaveClFormatyesnoEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@JsonValue()
	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public static String getDescriptionByCode(String code) {
		try {
			return valueOf("_" + code).getDescription();
		} catch (IllegalArgumentException noSuchCode) {
			return null;
		}
	}

    @JsonSetter()
	public static SaveClFormatyesnoEnum fromCode(String code) {
		return Arrays.stream(values())
				.filter(x -> code.equals(x.code))
				.findFirst()
				.orElse(null);
	}

    /**
     * Mapping
     */

    public static List<SaveClFormatyesnoEnum> getAllValues() { /* 19918 *ALL values */
        return Arrays.asList(SaveClFormatyesnoEnum._STA_NO, SaveClFormatyesnoEnum._NOT_ENTERED, SaveClFormatyesnoEnum._STA_YES);
    }

    public static List<SaveClFormatyesnoEnum> getYesOrNo() { /* 41918 YES OR NO */
        return Arrays.asList(SaveClFormatyesnoEnum._STA_NO, SaveClFormatyesnoEnum._STA_YES);
    }

    /**
     * Check
     */

    public static boolean isAllValues(SaveClFormatyesnoEnum saveClFormatyesno) {  /* 19918 *ALL values */
        return SaveClFormatyesnoEnum.getAllValues().contains(saveClFormatyesno);
    }

    public static boolean isYesOrNo(SaveClFormatyesnoEnum saveClFormatyesno) {  /* 41918 YES OR NO */
        return SaveClFormatyesnoEnum.getYesOrNo().contains(saveClFormatyesno);
    }
}