package au.awh.support;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JobContext implements Serializable {
	private static final long serialVersionUID = -1L;

    private final LocalDate jobDate = LocalDate.now();
    private final LocalTime jobTime = LocalTime.now();

    public LocalDateTime getSystemTimestamp() {
        return LocalDateTime.now();
    }

    public LocalTime getJobTime() {
        return jobTime;
    }

    public LocalDate getJobDate() {
        return jobDate;
    }

	public String getUser() {
		return "USERNAME";
	}
}
