package au.awh.utils.security;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * Authentication provider that checks by validating credentials by attempting a
 * connection to the database.
 *
 * @author Robin Rizvi
 * @since (2015-10-06.18:57:12)
 */
@Component
public class RestAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private DataSource dataSource;

	@Override
	public Authentication authenticate(Authentication authentication)
    	throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		try {
			dataSource.getConnection(username, password);
			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			Authentication auth = new UsernamePasswordAuthenticationToken(
            		username, password, grantedAuths);

			return auth;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
