package au.awh.utils.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.awh.web.ExceptionDTO;

import org.springframework.http.HttpStatus;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * Handler for invalid authentication and successful logout operation.
 *
 * @author Robin Rizvi
 * @since (2015-10-06.17:57:12)
 */
public class RestAuthenticationHandler implements LogoutSuccessHandler, AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request,
		HttpServletResponse response, AuthenticationException authException)
		throws IOException, ServletException {
		response.setContentType("application/json");
		response.setStatus(HttpStatus.UNAUTHORIZED.value());

		ExceptionDTO exception = new ExceptionDTO();
		exception.setCode(HttpStatus.UNAUTHORIZED.toString());
		exception.setMessage("Authentication is required for access");

		PrintWriter out = response.getWriter();
		new ObjectMapper().writeValue(out, exception);
	}

	@Override
	public void onLogoutSuccess(HttpServletRequest request,
		HttpServletResponse response, Authentication authentication)
		throws IOException, ServletException {
		response.setContentType("application/json");
		response.setStatus(HttpStatus.ACCEPTED.value());
	}
}
