package com.freschesolutions.libs.callstack;

import com.freschesolutions.libs.callstack.runtime.StepMethod;
import com.freschesolutions.libs.callstack.runtime.StepRegistry;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Base implementation of a service. Concrete services should derive from this class.
 *
 * @param <U> The service class.
 * @param <T> The state class.
 */
public abstract class AbstractService<U extends Service<U, T>, T extends Serializable> implements Service<U, T> {

  private StepRegistry<U, T> registry;

  public AbstractService(Class<U> serviceClass, Class<T> stateClass) {
    this.registry = new StepRegistry<>(serviceClass, stateClass);
  }

  @Override
  public Class<U> getServiceClass() {
    return registry.getServiceClass();
  }

  @Override
  public Class<T> getStateClass() {
    return registry.getStateClass();
  }

  @Override
  public StepMethod lookup(Step step) {
    return registry.lookup(step);
  }

  protected Step define(String stepId, Function<T, StepResult> function) {
    return registry.define(stepId, function);
  }

  protected <Q> Step define(
      String stepId, Class<Q> paramClass, BiFunction<T, Q, StepResult> function) {
    return registry.define(stepId, paramClass, function);
  }

  protected Step define(String stepId, Consumer<T> function) {
    return registry.define(stepId, function);
  }

  protected <Q> Step define(String stepId, Class<Q> paramClass, BiConsumer<T, Q> function) {
    return registry.define(stepId, paramClass, function);
  }

  @Override
  public String toString() {
    return "Service {class:" + getClass().getSimpleName() + "}";
  }
}
