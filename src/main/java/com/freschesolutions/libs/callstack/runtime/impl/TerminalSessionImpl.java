package com.freschesolutions.libs.callstack.runtime.impl;

import com.freschesolutions.libs.callstack.Service;
import com.freschesolutions.libs.callstack.action.*;
import com.freschesolutions.libs.callstack.runtime.RunningService;
import com.freschesolutions.libs.callstack.runtime.ServiceProvider;
import com.freschesolutions.libs.callstack.runtime.TerminalSession;
import com.freschesolutions.libs.callstack.runtime.TerminalSessionInfo;
import com.freschesolutions.libs.callstack.runtime.errors.TerminalSessionIllegalStateException;

import java.io.Serializable;
import java.util.Deque;
import java.util.Map;

/**
 * Implementation of the TerminalSession interface.
 */
public class TerminalSessionImpl implements TerminalSession {

  public static final String ACTION = "action";
  public static final String LAST_REQ_ID = "lastRequestId";

  private final TerminalSessionInfo terminalSessionInfo;
  private final Map<String, Object> state;
  private final Deque<RunningService> serviceStack;
  private final ServiceProvider<?> serviceProvider;

  private boolean closed = false;
  private Long sequence;
  private Action action;
  private RunningService runningService;
  private Runnable onClose;

  public TerminalSessionImpl(
          TerminalSessionInfo terminalSessionInfo,
          Map<String, Object> state,
          Deque<RunningService> serviceStack,
          ServiceProvider<?> serviceProvider) {
    this.terminalSessionInfo = terminalSessionInfo;
    this.state = state;
    this.serviceStack = serviceStack;
    this.serviceProvider = serviceProvider;

    runningService = serviceStack.size() > 0 ? serviceStack.pop() : null;
    action = (Action) state.get(ACTION);
    sequence = (Long) state.getOrDefault(LAST_REQ_ID, 0L);
  }

  /**
   * Assert that the session is loaded.
   */
  private void assertOpen() {
    if (closed) {
      throw new TerminalSessionIllegalStateException("Cannot access an unloaded session.");
    }
  }

  private void pushRunningService(RunningService runningService) {
    if (this.runningService != null) {
      serviceStack.push(this.runningService);
    }
    this.runningService = runningService;
  }

  private void popRunningService() {
    if (serviceStack.size() > 0) {
      runningService = serviceStack.pop();
    } else {
      runningService = null;
    }
  }

  @Override
  public TerminalSessionInfo getInfo() {
    assertOpen();
    return terminalSessionInfo;
  }

  @Override
  public Class<?> getParameterClass() {
    assertOpen();
    if (runningService != null) {
      return runningService.getCurrentStepParameterClass();
    } else {
      return Void.class;
    }
  }

  @Override
  public Class<?> getParameterClass(Class serviceClass) {
    assertOpen();
    Service service = (Service) serviceProvider.get(serviceClass);
    return service.lookup(service.getInitialStep()).getParameterClass();
  }

  public void setOnClose(Runnable onClose) {
    assertOpen();
    this.onClose = onClose;
  }

  @Override
  public void close() {
    assertOpen();
    try {

      if (runningService != null) {
        serviceStack.push(runningService);
      }
      if (action != null) {
        state.put(ACTION, action);
      } else {
        state.remove(ACTION);
      }
      state.put(LAST_REQ_ID, sequence);

      runningService = null;
      action = null;
      sequence = null;

    } finally {
      closed = true;
      if (onClose != null) {
        onClose.run();
      }
    }
  }

  @Override
  public Action getAction() {
    assertOpen();
    return action;
  }

  @Override
  public Long getSequence() {
    assertOpen();
    return sequence;
  }

  @Override
  public Action start(Class serviceClass, Serializable params) {
    assertOpen();
    if (sequence != 0) {
      throw new IllegalStateException("Cannot start multiple services on the same session.");
    }
    pushRunningService(new RunningService((Service) serviceProvider.get(serviceClass)));
    return execute(params);
  }

  private void executeInternal(Serializable params) {
    if (runningService != null) {
      Service service = (Service) serviceProvider.get(runningService.getServiceClass());

      action = runningService.execute(service, params);
      action.accept(new VisitUntilCallScreenOrReturnVisitor());

    } else {
      throw new TerminalSessionIllegalStateException("Session is terminated.");
    }
  }

  @Override
  public Action execute(Serializable params) {
    assertOpen();
    executeInternal(params);
    sequence++;
    return action;
  }

  /**
   * Visitor that handles all call to sub-services using recursivity. Only CallScreen and
   * ReturnFromService of the last service will be returned by this visitor.
   */
  public class VisitUntilCallScreenOrReturnVisitor implements Visitor<Void> {

    @Override
    public Void visit(CallScreen action) {
      return null;
    }

    @Override
    public Void visit(CallService action) {
      pushRunningService(new RunningService((Service) serviceProvider.get(action.getService())));
      executeInternal(action.getParams());
      return null;
    }

    @Override
    public Void visit(ReturnFromService action) {
      popRunningService();
      if (runningService != null) {
        executeInternal(action.getResults());
      }
      return null;
    }
  }
}
