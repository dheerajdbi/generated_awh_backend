package com.freschesolutions.libs.callstack.runtime;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO containing the Terminal Session information.
 */
public class TerminalSessionInfo implements Serializable {

  private String tsId;
  private String principal;
  private Long creationTs;
  private Long activityTs;

  public String getTsId() {
    return tsId;
  }

  public void setTsId(String tsId) {
    this.tsId = tsId;
  }

  public String getPrincipal() {
    return principal;
  }

  public void setPrincipal(String principal) {
    this.principal = principal;
  }

  public Long getCreationTs() {
    return creationTs;
  }

  public void setCreationTs(Long creationTs) {
    this.creationTs = creationTs;
  }

  public Long getActivityTs() {
    return activityTs;
  }

  public void setActivityTs(Long activityTs) {
    this.activityTs = activityTs;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TerminalSessionInfo that = (TerminalSessionInfo) o;
    return Objects.equals(tsId, that.tsId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tsId);
  }
}
