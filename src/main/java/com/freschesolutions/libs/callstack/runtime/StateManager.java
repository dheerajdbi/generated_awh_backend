package com.freschesolutions.libs.callstack.runtime;

import com.freschesolutions.libs.callstack.runtime.errors.TerminalSessionNotFoundExeception;

public interface StateManager {

  /**
   * Create a new session for the provided principal.
   *
   * @param principal The principal owning this session.
   * @return The session information.
   */
  TerminalSessionInfo createSession(String principal);

  /**
   * Open an existing session for access. The caller is responsible for closing the access session.
   * This can be done with the try with resource pattern.
   * <p>
   * <p>Ex:
   * <p>
   * <pre>
   *   try (TerminalSession session = sm.openSessionForAccess(tsid, principal)) {
   *     ... code that access session here.
   *   }
   * </pre>
   *
   * @param sessionId The session id to fetch.
   * @param principal User accessing the information.
   * @return The closure's result or null if session is not active.
   */
  TerminalSession openSessionForAccess(String sessionId, String principal)
          throws TerminalSessionNotFoundExeception;

  /**
   * Delete an existing session.
   *
   * @param sessionId The id of the session to delete.
   * @return The deleted session, if any or null.
   */
  TerminalSessionInfo deleteSession(String sessionId);
}
