package com.freschesolutions.libs.callstack.runtime.impl;

import com.freschesolutions.libs.callstack.runtime.RunningService;
import com.freschesolutions.libs.callstack.runtime.ServiceProvider;
import com.freschesolutions.libs.callstack.runtime.StateManager;
import com.freschesolutions.libs.callstack.runtime.TerminalSessionInfo;
import com.freschesolutions.libs.callstack.runtime.errors.TerminalSessionNotFoundExeception;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.Codec;
import org.redisson.codec.SerializationCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.util.Deque;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

/**
 * Manage sessions with the client.
 */
@Service
public class StateManagerImpl implements StateManager {

  public static final String SESSION_MAP = "SESSIONS";
  public static final String SESSION_STATE_MAP_PREFIX = "SESSIONS_STATE_";
  public static final String SESSION_PROCESS_STACK_PREFIX = "SESSIONS_PROCESS_STACK_";

  private final RedissonClient redis;
  private final Clock clock;
  private final ServiceProvider serviceProvider;
  private final Codec codec = new SerializationCodec();
  private final RMap<String, TerminalSessionInfo> sessionMap;
  // FIXME Make this configurable.
  private final long timeoutMs = 10000000;

  @Autowired
  public StateManagerImpl(RedissonClient redis, Clock clock, ApplicationContext serviceProvider) {
    this(redis, clock, (clazz) -> (com.freschesolutions.libs.callstack.Service) serviceProvider.getBean(clazz));
  }

  public StateManagerImpl(RedissonClient redis, Clock clock, ServiceProvider serviceProvider) {
    this.redis = redis;
    this.clock = clock;
    this.serviceProvider = serviceProvider;
    sessionMap = this.redis.getMap(SESSION_MAP, codec);
  }

  /**
   * @return The internal session map
   */
  public RMap<String, TerminalSessionInfo> getSessionMap() {
    return sessionMap;
  }

  /**
   * @return map to store state related to session.
   */
  public RMap<String, Object> getStateMap(String sessionId) {
    return redis.getMap(SESSION_STATE_MAP_PREFIX + sessionId, codec);
  }

  /**
   * @return Stack of running service.
   */
  public Deque<RunningService> getServiceStack(String sessionId) {
    return redis.getDeque(SESSION_PROCESS_STACK_PREFIX + sessionId, codec);
  }

  @Override
  public TerminalSessionInfo createSession(String principal) {
    Objects.requireNonNull(principal);

    TerminalSessionInfo result = new TerminalSessionInfo();

    result.setTsId(UUID.randomUUID().toString());
    result.setPrincipal(principal);
    result.setCreationTs(clock.millis());
    result.setActivityTs(result.getCreationTs());

    Lock l = sessionMap.getLock(result.getTsId());
    l.lock();
    try {
      sessionMap.put(result.getTsId(), result);
    } finally {
      l.unlock();
    }
    return result;
  }

  @Override
  public TerminalSessionImpl openSessionForAccess(String sessionId, String principal) {
    Objects.requireNonNull(sessionId);
    Objects.requireNonNull(principal);

    Lock l = sessionMap.getLock(sessionId);
    l.lock();
    try {
      Long now = clock.millis();
      TerminalSessionInfo sessionInfo = sessionMap.get(sessionId);
      if (sessionInfo != null && sessionInfo.getPrincipal().equals(principal)) {
        if (now - sessionInfo.getActivityTs() < timeoutMs) {

          // Set activityTs to the start of the closure
          sessionInfo.setActivityTs(now);

          // Create the state map.
          Map<String, Object> stateMap = getStateMap(sessionId);
          Deque<RunningService> serviceStack = getServiceStack(sessionId);

          TerminalSessionImpl session =
                  new TerminalSessionImpl(sessionInfo, stateMap, serviceStack, serviceProvider);
          session.setOnClose(() -> unlockSession(sessionInfo, l));

          return session;
        } else {
          // Session is expired.
          deleteSession(sessionId);
          throw new TerminalSessionNotFoundExeception();
        }
      } else {
        // Session does not exists
        throw new TerminalSessionNotFoundExeception();
      }
    } catch (Exception e) {
      throw e;
    }
  }

  private void unlockSession(TerminalSessionInfo sessionInfo, Lock lock) {
    try {
      String sessionId = sessionInfo.getTsId();

      // Tag the activity ts again
      sessionInfo.setActivityTs(clock.millis());

      // Save session info to redis
      sessionMap.put(sessionId, sessionInfo);
    } finally {
      lock.unlock();
    }
  }

  @Override
  public TerminalSessionInfo deleteSession(String sessionId) {
    Lock l = sessionMap.getLock(sessionId);
    l.lock();
    try {
      getStateMap(sessionId).clear();
      getServiceStack(sessionId).clear();
      return sessionMap.remove(sessionId);
    } finally {
      l.unlock();
    }
  }
}
