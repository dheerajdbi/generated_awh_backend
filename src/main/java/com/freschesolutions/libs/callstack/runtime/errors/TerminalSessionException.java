package com.freschesolutions.libs.callstack.runtime.errors;

/**
 * Base class for TerminalSession-related exceptions.
 */
public class TerminalSessionException extends RuntimeException {
  public TerminalSessionException() {
  }

  public TerminalSessionException(String message) {
    super(message);
  }

  public TerminalSessionException(String message, Throwable cause) {
    super(message, cause);
  }

  public TerminalSessionException(Throwable cause) {
    super(cause);
  }

  public TerminalSessionException(
          String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
