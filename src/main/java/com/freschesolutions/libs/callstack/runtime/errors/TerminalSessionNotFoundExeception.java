package com.freschesolutions.libs.callstack.runtime.errors;

/**
 * Exception that denote that the session was not found.
 */
public class TerminalSessionNotFoundExeception extends TerminalSessionException {
}
