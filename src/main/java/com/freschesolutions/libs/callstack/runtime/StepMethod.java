package com.freschesolutions.libs.callstack.runtime;

import com.freschesolutions.libs.callstack.StepResult;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A callable instance of a step. It is obtained by trading a Step for a StepMethod using the
 * Service's lookup method.
 */
public abstract class StepMethod {

  private final Class<?> stateClass;
  private final Class<?> paramsClass;

  private StepMethod(Class<?> stateClass, Class<?> paramsClass) {
    Objects.requireNonNull(stateClass);
    Objects.requireNonNull(paramsClass);
    this.stateClass = stateClass;
    this.paramsClass = paramsClass;
  }

  /** @return The state class. */
  public Class<?> getStateClass() {
    return stateClass;
  }

  /** @return The parameter class, or Void.class if not parameters allowed. */
  public Class<?> getParameterClass() {
    return paramsClass;
  }

  /**
   * Call a method on a service instance.
   *
   * @param state The service state.
   * @return The result of the step execution.
   */
  public abstract StepResult call(Object state);

  /**
   * Call a method on a service instance.
   *
   * @param state The service state.
   * @param params The parameters passed to the step, if any.
   * @return The result of the step execution.
   */
  public abstract StepResult call(Object state, Object params);

  /**
   * Create a new Step Method from a Function. This step is not allowed to receive parameters.
   *
   * @param stateClass The state class.
   * @param function The function.
   * @param <S> State Type.
   * @return A callable StepMethod.
   */
  public static <S> StepMethod create(Class<S> stateClass, Function<S, StepResult> function) {
    return new FunctionStep(stateClass, function);
  }

  /**
   * Create a new StepMethod from a BiFunction. Those methods are allowed to receive parameters.
   *
   * @param stateClass The state class.
   * @param function The function.
   * @param <S> State Type.
   * @return A callable StepMethod.
   */
  public static <S, R> StepMethod create(
      Class<S> stateClass, Class<R> paramsClass, BiFunction<S, R, StepResult> function) {
    return new BiFunctionStep(stateClass, paramsClass, function);
  }

  /**
   * Create a new StepMethod from a Consumer. This step is not allowed to receive parameters and
   * always return StepResult#NO_ACTION.
   *
   * @param stateClass The state class.
   * @param function The function.
   * @param <S> State Type.
   * @return A callable StepMethod.
   */
  public static <S> StepMethod create(Class<S> stateClass, Consumer<S> function) {
    return new ConsumerStep(stateClass, function);
  }

  /**
   * Create a new Step Method from a BiConsumer. Those methods are allowed to receive parameters and
   * always return StepResult#NO_ACTION.
   *
   * @param stateClass The state class.
   * @param function The function.
   * @param <S> State Type.
   * @return A callable StepMethod.
   */
  public static <S, R> StepMethod create(
      Class<S> stateClass, Class<R> paramsClass, BiConsumer<S, R> function) {
    return new BiConsumerStep(stateClass, paramsClass, function);
  }

  /** Implementation of the Function-based StepMethod. */
  private static class FunctionStep extends StepMethod {

    private final Function function;

    private FunctionStep(Class<?> stateClass, Function function) {
      super(stateClass, Void.class);

      Objects.requireNonNull(function);
      this.function = function;
    }

    @Override
    public StepResult call(Object state) {
      return StepResult.class.cast(function.apply(getStateClass().cast(state)));
    }

    @Override
    public StepResult call(Object state, Object params) {
      //TODO: Commented below line to invoke other service classes
      //if (params != null) throw new IllegalStateException();

      return call(state);
    }
  }

  /** Implementation of the BiFunction-based StepMethod. */
  private static class BiFunctionStep extends StepMethod {

    private final Class<?> parameterClass;
    private final BiFunction function;

    private BiFunctionStep(Class<?> stateClass, Class<?> parameterClass, BiFunction function) {
      super(stateClass, Void.class);

      Objects.requireNonNull(parameterClass);
      Objects.requireNonNull(function);

      this.parameterClass = parameterClass;
      this.function = function;
    }

    public Class<?> getParameterClass() {
      return parameterClass;
    }

    @Override
    public StepResult call(Object state) {
      return StepResult.class.cast(function.apply(getStateClass().cast(state), null));
    }

    @Override
    public StepResult call(Object state, Object params) {
      return StepResult.class.cast(
          function.apply(getStateClass().cast(state), getParameterClass().cast(params)));
    }
  }

  /** Implementation of the Consumer-based StepMethod. */
  private static class ConsumerStep extends StepMethod {

    private final Consumer function;

    public ConsumerStep(Class<?> stateClass, Consumer function) {
      super(stateClass, Void.class);
      Objects.requireNonNull(function);

      this.function = function;
    }

    @Override
    public Class<?> getParameterClass() {
      return Void.class;
    }

    @Override
    public StepResult call(Object state) {
      function.accept(state);
      return StepResult.NO_ACTION;
    }

    @Override
    public StepResult call(Object state, Object params) {
      if (params != null) throw new IllegalStateException();
      return call(state);
    }
  }

  /** Implementation of the BiConsumer-based StepMethod. */
  private static class BiConsumerStep extends StepMethod {

    private final BiConsumer function;

    public BiConsumerStep(Class stateClass, Class paramsClass, BiConsumer function) {
      super(stateClass, paramsClass);
      this.function = function;
    }

    @Override
    public StepResult call(Object state) {
      function.accept(state, null);
      return StepResult.NO_ACTION;
    }

    @Override
    public StepResult call(Object state, Object params) {
      function.accept(state, params);
      return StepResult.NO_ACTION;
    }
  }
}
