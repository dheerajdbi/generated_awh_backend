package com.freschesolutions.libs.callstack.runtime;

import com.freschesolutions.libs.callstack.action.Action;

import java.io.Serializable;

/**
 * A terminal session containing a stack of running services.
 */
public interface TerminalSession extends AutoCloseable {

  /**
   * @return Retrieve the terminal session information.
   */
  TerminalSessionInfo getInfo();

  /**
   * The parameter class expected by the next step of this session.
   *
   * @return The parameter class.
   */
  Class<?> getParameterClass();

  /**
   * @param serviceClass The parameter class expected by the specific service class.
   * @return The parameter class.
   */
  Class<?> getParameterClass(Class serviceClass);

  /**
   * @return The interactive action expected to be performed.
   */
  Action getAction();

  /**
   * @return The sequence of the action.
   */
  Long getSequence();

  /**
   * Start the process on the session and execute to the next interactive action.
   *
   * @param params parmeters of the step, or null.
   * @return Action to perform interactively.
   */
  Action start(Class serviceClass, Serializable params);

  /**
   * Execute to the next interactive action.
   *
   * @param params Parmeters of the step, or null.
   * @return Action to perform interactively.
   */
  Action execute(Serializable params);
}
