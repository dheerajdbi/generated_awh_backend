package com.freschesolutions.libs.callstack.runtime;

import com.freschesolutions.libs.callstack.Service;

/**
 * Provides service instances to Terminal Sessions.
 *
 * @param <T> Service class.
 */
@FunctionalInterface
public interface ServiceProvider<T extends Service> {

  T get(Class<T> serviceClass);
}
