package com.freschesolutions.libs.callstack.action;

import java.io.Serializable;
import java.util.Objects;

/**
 * Describe the interactive action of calling a screen.
 */
public class CallScreen extends Action {

  private final String screenId;
  private final Serializable params;

  public CallScreen(String screenId, Serializable params) {
    this.screenId = screenId;
    this.params = params;
  }

  /**
   * @return A unique identifier for the screen. This identifier is sent to the client and is used for routing.
   */
  public String getScreenId() {
    return screenId;
  }

  /**
   * @return The parameters or model sent to the client to run the screen.
   */
  public Serializable getParams() {
    return params;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CallScreen that = (CallScreen) o;
    return Objects.equals(screenId, that.screenId) && Objects.equals(params, that.params);
  }

  @Override
  public int hashCode() {
    return Objects.hash(screenId, params);
  }

  @Override
  public String toString() {
    return "CallScreen{" + "screenId='" + screenId + '\'' + ", params=" + params + '}';
  }

  @Override
  public <T> T accept(Visitor<T> visitor) {
    return visitor.visit(this);
  }
}
