package com.freschesolutions.libs.callstack.action;

/**
 * Visitor interface for actions.
 *
 * @param <T> return types.
 */
public interface Visitor<T> {

  T visit(CallScreen action);

  T visit(CallService action);

  T visit(ReturnFromService action);
}
