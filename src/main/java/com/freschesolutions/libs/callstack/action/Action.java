package com.freschesolutions.libs.callstack.action;

import java.io.Serializable;

/** Describe actions to be performed outside of the normal program flow. */
public abstract class Action implements Serializable {

  /**
   * Accept a visitor.
   *
   * @param visitor The visitor.
   * @param <T> Type returned by visition.
   * @return The visitor's return value.
   */
  public abstract <T> T accept(Visitor<T> visitor);
}
