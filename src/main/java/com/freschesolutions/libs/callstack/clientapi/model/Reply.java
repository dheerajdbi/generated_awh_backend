package com.freschesolutions.libs.callstack.clientapi.model;

import com.fasterxml.jackson.databind.JsonNode;

/** A request from the client API. */
public class Reply {

  private String requestId;
  private JsonNode model;

  @SuppressWarnings("unused")
  public Reply() {
    // Used for serialization.
  }

  public Reply(String requestId, JsonNode model) {
    this.requestId = requestId;
    this.model = model;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public JsonNode getModel() {
    return model;
  }

  public void setModel(JsonNode model) {
    this.model = model;
  }
}
