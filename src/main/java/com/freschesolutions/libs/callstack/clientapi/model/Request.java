package com.freschesolutions.libs.callstack.clientapi.model;

/** A request from the client API. */
public class Request {

  private String requestId;
  private InteractiveAction action;
  private String screenId;
  private Object model;

  public Request() {}

  public Request(String requestId, InteractiveAction action, String screenId, Object model) {
    this.requestId = requestId;
    this.action = action;
    this.screenId = screenId;
    this.model = model;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public InteractiveAction getAction() {
    return action;
  }

  public void setAction(InteractiveAction action) {
    this.action = action;
  }

  public String getScreenId() {
    return screenId;
  }

  public void setScreenId(String screenId) {
    this.screenId = screenId;
  }

  public Object getModel() {
    return model;
  }

  public void setModel(Object model) {
    this.model = model;
  }
}
