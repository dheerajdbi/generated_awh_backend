package com.freschesolutions.libs.callstack;

import com.freschesolutions.libs.callstack.action.Action;
import com.freschesolutions.libs.callstack.action.CallScreen;
import com.freschesolutions.libs.callstack.action.CallService;
import com.freschesolutions.libs.callstack.action.ReturnFromService;

import java.io.Serializable;
import java.util.*;

/**
 * Describe the result of a service step. This is composed of 2 parts:
 * <ul>
 *   <li>Additional steps to schedule</li>
 *   <li>External action to perform</li>
 * </ul>
 *
 * External actions could be:
 * <ul>
 *   <li>Calling a service</li>
 *   <li>Calling a screen</li>
 *   <li>Returning from a service</li>
 * </ul>
 */
public class StepResult {

  private final List<Step> steps;
  private final Action action;

  public StepResult(List<Step> steps, Action action) {
    Objects.requireNonNull(steps);

    this.steps = Collections.unmodifiableList(steps);
    this.action = action;
  }

  public StepResult(Step step, Action action) {
    this(Arrays.asList(step), action);
  }

  public StepResult(Action action) {
    this(new ArrayList<>(), action);
  }

  public List<Step> getSteps() {
    return steps;
  }

  public Action getAction() {
    return action;
  }

  public StepResult thenCall(Step step) {
    List<Step> newSteps = new ArrayList<>(getSteps().size() + 1);
    newSteps.addAll(getSteps());
    newSteps.add(step);
    return new StepResult(newSteps, getAction());
  }

  public StepResult thenCall(Step step, Step... steps) {
    List<Step> newSteps = new ArrayList<>(getSteps().size() + steps.length + 1);
    newSteps.addAll(getSteps());
    newSteps.add(step);
    for (Step s : steps) {
      newSteps.add(s);
    }
    return new StepResult(newSteps, getAction());
  }

  public static StepResult call(Step step, Step... steps) {
    List<Step> newSteps = new ArrayList<>(steps.length + 1);
    newSteps.add(step);
    for (Step s : steps) {
      newSteps.add(s);
    }
    return new StepResult(newSteps, null);
  }

  public static StepResult NO_ACTION = new StepResult(new ArrayList<>(), null);

  public static StepResult callScreen(String screenId, Serializable params) {
    return new StepResult(new CallScreen(screenId, params));
  }

  public static <U extends Serializable, T extends Service<T, U>> StepResult callService(
      Class<T> serviceClass, Serializable params) {
    return new StepResult(new CallService(serviceClass, params));
  }

  public static StepResult returnFromService(Serializable results) {
    return new StepResult(new ReturnFromService(results));
  }
}
