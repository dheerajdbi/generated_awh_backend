package com.freschelegacy.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.WeekFields;

import java.util.Locale;

/**
 * Utility class for Date/Time operations.
 *
 * @author Amit Arya
 * @since (2016-07-18.18:01:02)
 */
public final class DateTimeUtils {

	/**
	 * Obtains the formatter to convert system timestamp values (LocalDateTime) to String
	 *
	 * @return formatter
	 */
	public static DateTimeFormatter getTimestampFormat() {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss.SSS");
	}

	/**
	 * Obtains the current date from the system clock in the default time-zone.
	 *
	 * @return local date
	 */
	public static LocalDate getDate() {
		return LocalDate.now();
	}

	/**
	 * Gets the current day of month.
	 *
	 * @return the day-of-month, from 1 to 31
	 */
	public static int getDay() {
		return LocalDate.now().getDayOfMonth();
	}

	/**
	 * Gets the current month of year from 1 to 12.
	 *
	 * @return the month-of-year, from 1 to 12
	 */
	public static int getMonth() {
		return LocalDate.now().getMonthValue();
	}

	/**
	 * Obtains the current date-time from the system clock in the default
	 * time-zone.
	 *
	 * @return local date-time
	 */
	public static LocalDateTime getTimestamp() {
		return LocalDateTime.now();
	}

	/**
	 * Returns the Week Number.
	 *
	 * @param date local date
	 * @return week of a week-based-year
	 */
	public static int getWeekNumber(LocalDate date) {
		WeekFields weekFields = WeekFields.of(Locale.getDefault());

		return date.get(weekFields.weekOfWeekBasedYear());
	}

	/**
	 * Gets the current year.
	 *
	 * @return the year, from MIN_YEAR to MAX_YEAR
	 */
	public static int getYear() {
		return LocalDate.now().getYear();
	}

	/**
	 * Returns a copy of this date with the specified constant subtracted.
	 *
	 * @param date local date
	 * @param constToSubtract the constant to subtract
	 * @return a LocalDate based on this date with the subtraction made
	 */
	public static LocalDate minusConst(LocalDate date, long constToSubtract) {
		if (constToSubtract == 10000) {
			return date.minusYears(1);
		} else if (constToSubtract == 100) {
			return date.minusMonths(1);
		}

		return date;
	}

	/**
	 * Returns a copy of this date with the specified constant added.
	 *
	 * @param date local date
	 * @param constToAdd the constant to add
	 * @return a LocalDate based on this date with the addition made
	 */
	public static LocalDate plusConst(LocalDate date, long constToAdd) {
		if (constToAdd == 10000) {
			return date.plusYears(1);
		} else if (constToAdd == 100) {
			return date.plusMonths(1);
		}

		return date;
	}

	/**
	 * Converts the date to Database format i.e. yyyy-MM-dd format.
	 *
	 * @param date local date
	 * @return converted date in Database format
	 */
	public static String toDbFormat(LocalDate date) {
		return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

	/**
	 * Converts the date to integer.
	 *
	 * @param date local date
	 * @return converted date in integer
	 */
	public static int toInt(LocalDate date) {
		return Integer.parseInt(date.getYear() +
			String.format("%02d", date.getMonthValue()) +
			String.format("%02d", date.getDayOfMonth()));
	}

	/**
	 * Obtains an instance of LocalDate in yyyyMMdd format from a text string.
	 *
	 * @param date the text to parse
	 * @return the parsed local date
	 */
	public static LocalDate toIntFormat(String date) {
		String longDate = date;

		if (date.length() == 6) {
			date = "0" + date;
		}

		if (date.length() == 7) {
			longDate = (Integer.parseInt(date.substring(0, 1)) + 19) +
				date.substring(1);
		}

		return LocalDate.parse(longDate, DateTimeFormatter.BASIC_ISO_DATE);
	}

	/**
	 * Converts from an Instant on the time-line to a LocalDate.
	 *
	 * @param epochMilli the number of milliseconds from 1970-01-01T00:00:00Z
	 * @return local date
	 */
	public static LocalDate toLocalDate(long epochMilli) {
		try {
			return toIntFormat(String.valueOf(epochMilli));
		} catch (DateTimeParseException dtpe) {
		}

		return Instant.ofEpochMilli(epochMilli).atZone(ZoneId.systemDefault())
					  .toLocalDate();
	}
}
