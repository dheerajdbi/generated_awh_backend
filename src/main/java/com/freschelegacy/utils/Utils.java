package com.freschelegacy.utils;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Utility class for different X-2E Modernize classes.
 * <p>
 * It is not to be instantiated, only use its public static members outside.
 *
 * @author Amit Arya
 * @since (2016-03-09.17:06:02)
 */
public final class Utils {

	/**
	 * Retrieves the name of the database product.
	 *
	 * @param ds factory for connections to the physical data source
	 * @return database product name
	 */
	public static String getDBName(DataSource ds) {
		String dbName = null;
		String url = ((DriverManagerDataSource)ds).getUrl();

		if (url.indexOf("mysql") != -1) {
			dbName = "MySQL";
		} else if (url.indexOf("oracle") != -1) {
			dbName = "Oracle";
		} else if (url.indexOf("db2") != -1) {
			dbName = "DB2/NT";
		} else if (url.indexOf("as400") != -1) {
			dbName = "DB2 UDB for AS/400";
		}

		return dbName;
	}

	/**
	 * Gets current logged in user name in Spring Security.
	 *
	 * @return current logged in user name
	 */
	public static String getUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

	/**
	 * Marks a transaction for Rollback within a transaction context when using
	 * programmatic transaction management.
	 *
	 * @param transactionManager the Spring transaction manager
	 */
	public static void markTransactionForRollback(
		PlatformTransactionManager transactionManager) {
		try {
			String name =
				TransactionSynchronizationManager.getCurrentTransactionName();
			int behavior = TransactionDefinition.PROPAGATION_REQUIRED;

			DefaultTransactionDefinition def =
				new DefaultTransactionDefinition();
			def.setName(name);
			def.setPropagationBehavior(behavior);

			TransactionStatus status = transactionManager.getTransaction(def);

			transactionManager.rollback(status);
		} catch (NoTransactionException nte) {
			nte.printStackTrace();
		}
	}
}
