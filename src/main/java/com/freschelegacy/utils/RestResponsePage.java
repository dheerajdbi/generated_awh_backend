package com.freschelegacy.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import au.awh.config.PageRequestDeserializer;

 /* Class uses instead of PagImpl to enable deseralization
 * src: https://www.codesd.com/item/spring-resttemplate-with-paged-api.html
 */
@JsonAutoDetect(fieldVisibility=Visibility.ANY,getterVisibility=Visibility.NONE,setterVisibility=Visibility.NONE)
@JsonIgnoreProperties(value={"last","first"})
public class RestResponsePage<T> extends PageImpl<T> implements Serializable {

    private static final long serialVersionUID = 3248189030448292002L;

    @JsonDeserialize(using=PageRequestDeserializer.class)
    @JsonProperty("pageable")
    private final PageRequest pageable = PageRequest.of(0,1);

    @JsonProperty("totalElements")
    private final long total = 0;

    public RestResponsePage(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public RestResponsePage(List<T> content) {
        super(content);
    }

    /* RestResponsePage does not have an empty constructor and this was causing an issue for RestTemplate to cast the Rest API response
     * back to Page.
     */
    public RestResponsePage() {
        super(new ArrayList<T>());
    }

}
