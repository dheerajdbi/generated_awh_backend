package com.freschelegacy.utils;

import java.io.File;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.BeanUtils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * Utility class for report operations.
 *
 * @author Amit Arya
 * @author Robin Rizvi
 * @since (2016-05-30.17:06:02)
 */
public final class ReportUtils {

	private static String webRootPath = "/";

	private static final Logger logger =
		LoggerFactory.getLogger(ReportUtils.class);

	public enum FileType {
		PDF("pdf", "application/pdf"),
		HTML("html", "text/html"),
		XLS("xls", "application/vnd.ms-excel"),
		XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		private final String extension;
		private final String mimeType;

		FileType(String extension, String mimeType) {
			this.extension = extension;
			this.mimeType = mimeType;
		}
	}

	/**
	 * Exports report as PDF/XLS file to client.
	 *
	 * @param fileName report file name
	 * @param fileType file type like pdf or xls
	 * @param body report body
	 * @return a new ResponseEntity with body, headers, and status code
	 */
	public static ResponseEntity<byte[]> exportReport(String fileName,
		FileType fileType, byte body[]) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType(fileType.mimeType));
		headers.setContentDispositionFormData(fileName, "reports/" + fileName);
		headers.setCacheControl("no-cache, no-store, must-revalidate");

		return new ResponseEntity<byte[]>(body, headers, HttpStatus.OK);
	}

	/**
	 * Constructs a valid file name for report file.
	 *
	 * @param title report file name
	 * @return report file name
	 */
	public static String getReportFileName(String title) {
		Date dt = new Date();
		String sdf =
			new SimpleDateFormat("yyyy-MM-dd" + "_" + "HH-mm-ss").format(dt);
		title = title.replaceAll("\\*", "");
		title = title.replaceAll("/", "-");

		int indx = title.lastIndexOf(":");

		if (indx > 2) {
			title = title.replace(": ", "-");
		}

		String fileName = title + "_" + sdf;

		return fileName;
	}

	/**
	 * Constructs a valid file name for report file with file extension.
	 *
	 * @param title report file name
	 * @param fileType file type like pdf or html or xls
	 * @return report file name
	 */
	public static String getReportFileName(String title, FileType fileType) {
		String fileName = getReportFileName(title) + "." + fileType.extension;

		return fileName;
	}

	/**
	 * Gets the full file path with name for report file.
	 *
	 * @param filePath report file path
	 * @param fileName report file name
	 * @return report full file path with name
	 */
	public static String getReportFilePath(String filePath, String fileName) {
		File reportDir = new File(filePath, "reports");

		if (!reportDir.exists()) {
			reportDir.mkdirs();
		}

		return reportDir.getPath() + File.separator + fileName;
	}

	/**
	 * Gets web root path.
	 *
	 * @return web root path
	 */
	public static String getWebRootPath() {
		return webRootPath;
	}

	/**
	 * Prepares Data Source for filling the report.
	 *
	 * @param <T> the type of the DTO
	 * @param reportDtos list of DTOs
	 * @param type type of DTO
	 * @param groupByFields the fields to group the list
	 * @return DataSource
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> prepareDataSource(final List<T> reportDtos,
		final Class<T> type, final String... groupByFields) {
		if ((groupByFields == null) || (groupByFields.length == 0)) {
			return reportDtos;
		}

		List<T> dataSource = new ArrayList<T>();

		try {
			while (dataSource.size() != reportDtos.size()) {
				final List<T> selectedList =
					(List<T>)CollectionUtils.select(reportDtos,
						new Predicate() {
					public boolean evaluate(Object obj) {
						try {
							Field fld = type.getDeclaredField(groupByFields[0]);
							fld.setAccessible(true);

							return fld.get(obj)
									  .equals(fld.get(reportDtos.get(0)));
						} catch (Exception e) {
							return false;
						}
					}
				});

				T reportDTO = type.newInstance();
				BeanUtils.copyProperties(reportDtos.get(0), reportDTO);

				Field fld = reportDTO.getClass().getDeclaredField("listDto");
				fld.setAccessible(true);
				fld.set(reportDTO, selectedList);
				reportDtos.removeAll(selectedList);
				reportDtos.add(reportDTO);
				dataSource.add(reportDTO);

				if (groupByFields.length > 1) {
					prepareDataSource((List<T>)fld.get(reportDTO), type,
						Arrays.copyOfRange(groupByFields, 1,
							groupByFields.length));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataSource;
	}

	/**
	 * Saves report under exportPath based on the specified file type.
	 *
	 * @param jasperPrint report
	 * @param exportPath path to save the report
	 * @param fileType format to export the report
	 * @throws JRException if a jasper report exception occurs
	 */
	public static void saveReport(JasperPrint jasperPrint, String exportPath,
		FileType fileType) throws JRException {
		switch (fileType) {
		case PDF:
			JasperExportManager.exportReportToPdfFile(jasperPrint, exportPath);

			break;

		case HTML:
			JasperExportManager.exportReportToHtmlFile(jasperPrint, exportPath);

			break;

		case XLS:

			JRXlsExporter xlsExporter = new JRXlsExporter();
			xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
					exportPath));
			xlsExporter.exportReport();

			break;

		case XLSX:

			JRXlsxExporter xlsxExporter = new JRXlsxExporter();
			xlsxExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			xlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
					exportPath));
			xlsxExporter.exportReport();

			break;
		}
	}

	/**
	 * Saves report in all formats so that the client may be able to choose the
	 * desired format to view the report.
	 *
	 * @param jasperPrint report
	 * @param prtFileName report file name
	 * @throws JRException if a jasper report exception occurs
	 */
	public static void saveReports(JasperPrint jasperPrint, String prtFileName)
		throws JRException {
		for (FileType fileType : FileType.values()) {
			String exportFileName = prtFileName + "." + fileType.extension;
			String exportPath = getReportFilePath(webRootPath, exportFileName);

			// Save report under exportPath
			saveReport(jasperPrint, exportPath, fileType);
			logger.info("Successfully exported report to " + exportPath);
		}
	}

	/**
	 * Sets web root path.
	 *
	 * @param webRootPath path of web root
	 */
	public static void setWebRootPath(String webRootPath) {
		ReportUtils.webRootPath = webRootPath;
	}
}
