@echo off

rem ======================================================================
rem
rem	   Author: Jun Yang
rem    Created: Monday, 07/10/2018 12:20:00
rem    Batch File for node modules installation and running grunt tasks for WSDEVM
rem
rem ======================================================================

@echo off
title Installation and Build
:home
cls
cd /d %~dp0/client
echo.
echo Select a Task:
echo ==============
echo.
echo 1) start application      (start the application (npm install and ng serve))
echo.
echo ==============            (Tasks below can be executed individually after installation)
echo.
echo 2) npm install            (install modules locally and execute build)
echo 3) npm install -g         (install modules globally and execute build)
echo 4) ng build               (detect errors and potential problems in js code)
echo 5) ng test                (execute tests using ng test)
echo 6) ng lint                (execute lint using ng lint)
echo 7) ng e2e                 (execute e2e using ng e2e)
echo.
echo 10) Exit
echo.
set /p opt=Type option:

if "%opt%"=="1" goto Option1
if "%opt%"=="2" goto Option2
if "%opt%"=="3" goto Option3
if "%opt%"=="4" goto Option4
if "%opt%"=="5" goto Option5
if "%opt%"=="10" goto End

goto End

:Option1
echo Proceeding with npm local installation
call npm cache verify
call npm cache clear -force
call npm install --unsafe-perm
call npm -g install karma@2.0.2
call ng serve
echo Installation completed and build task executed
echo Libraries in "webapp/libraries"
echo css compiled in "webapp/styles/App.css"
echo Build log in "target-grunt/output/log"
echo Code documentation in "target-grunt/output/code documentation/index.html"
echo Build log in "target-grunt/output/log"
echo jshint report in "target-grunt/output/report/js hint"
echo csslint report in "target-grunt/output/report/css lint"
echo Test report in "target-grunt/output/report/test/test-report.html"
echo Code analysis report in "target-grunt/output/report/code analysis/index.html"
call ng serve
pause
goto End

:Option2
echo Proceeding with npm local installation
call npm cache verify
call npm cache clear -force
call npm install --unsafe-perm
call npm -g install karma@2.0.2
echo Installation completed and build task executed
echo Libraries in "webapp/libraries"
echo css compiled in "webapp/styles/App.css"
echo Build log in "target-grunt/output/log"
echo Code documentation in "target-grunt/output/code documentation/index.html"
echo Build log in "target-grunt/output/log"
echo jshint report in "target-grunt/output/report/js hint"
echo csslint report in "target-grunt/output/report/css lint"
echo Test report in "target-grunt/output/report/test/test-report.html"
echo Code analysis report in "target-grunt/output/report/code analysis/index.html"
pause
goto End

:Option3
echo Proceeding with npm global installation
call npm cache verify
call npm cache clear -force
call npm install -g
call npm -g install karma@2.0.2
call npm install -g karma-cli
call npm uninstall -g WSDEVM
call npm unpublish WSDEVM --force
echo Installation completed and build task executed
echo Libraries in "webapp/libraries"
echo css compiled in "webapp/styles/App.css"
echo Build log in "target-grunt/output/log"
echo Code documentation in "target-grunt/output/code documentation/index.html"
echo Build log in "target-grunt/output/log"
echo jshint report in "target-grunt/output/report/js hint"
echo csslint report in "target-grunt/output/report/css lint"
echo Test report in "target-grunt/output/report/test/test-report.html"
echo Code analysis report in "target-grunt/output/report/code analysis/index.html"
pause
goto End

:Option4
echo Proceeding with jshint
call ng build
echo jshint report in "target-grunt/output/report/js hint"
pause
goto End

:Option5
echo Proceeding with test execution
call ng test
echo Test report in "target-grunt/output/report/test/test-report.html"
pause
goto End

:Option6
echo Proceeding lint
call ng lint
echo csslint report in "target-grunt/output/report/js hint"
pause
goto End

:Option7
echo Proceeding e2e"
call ng e2e
echo Test report in "target-grunt/output/report/js hint"
pause
goto End

:End
exit
