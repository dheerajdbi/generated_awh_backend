
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: geteReadingByDate of type EXCINTFUN
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 2 
	***********************************/
	// * WARNING: This function must never be implemented as a sub-routine
	// * as the use of *QUIT for the requested action will not work.
	// DEBUG genFunctionCall BEGIN 1000010 ACT Get Reading by Date - Equipment Readings  *
	getReadingByDateParams = new GetReadingByDateParams();
	getReadingByDateResult = new GetReadingByDateResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto, getReadingByDateParams);
	getReadingByDateParams.setAwhRegionCode(dto.getAwhRegionCode());
	getReadingByDateParams.setReadingType(dto.getReadingType());
	getReadingByDateParams.setPlantEquipmentCode(dto.getPlantEquipmentCode());
	getReadingByDateParams.setReadingRequiredDate(dto.getReadingRequiredDate());
	getReadingByDateParams.setCentreCodeKey(dto.getCentreCodeKey());
	stepResult = getReadingByDateService.execute(getReadingByDateParams);
	getReadingByDateResult = (GetReadingByDateResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(getReadingByDateResult.get_SysReturnCode());
	dto.setEquipmentReadingSgt(getReadingByDateResult.getEquipmentReadingSgt());
	dto.setReadingTimestamp(getReadingByDateResult.getReadingTimestamp());
	dto.setReadingValue(getReadingByDateResult.getReadingValue());
	dto.setReadingComment(getReadingByDateResult.getReadingComment());
	dto.setUserId(getReadingByDateResult.getUserId());
	dto.setEquipmentLeasor(getReadingByDateResult.getEquipmentLeasor());
	dto.setTransferToFromSgt(getReadingByDateResult.getTransferToFromSgt());
	dto.setAwhBuisnessSegment(getReadingByDateResult.getAwhBuisnessSegment());
	dto.setEquipmentTypeCode(getReadingByDateResult.getEquipmentTypeCode());
	// DEBUG genFunctionCall END
	if ((dto.getGetRecordOption() == GetRecordOptionEnum.fromCode("I")) && (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005"))) {
		// DEBUG genFunctionCall BEGIN 1000201 ACT PGM.*Return code = CND.*Normal
		dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
		// DEBUG genFunctionCall END
	}
	if (!(dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK"))) {
		// NOT PGM.*Return code is *Normal
		if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Do not send message
			// 
		} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
			// PGM.*Return code is *Record does not exist
			if (!(dto.getGetRecordOption() == GetRecordOptionEnum.fromCode("N"))) {
				// NOT PAR.Get Record Option is No message on not found
				// * NOTE: Insert <file> NF message here.
				// DEBUG genFunctionCall BEGIN 1000130 ACT Send error message - 'Equipment Readings     NF'
				// DEBUG genFunctionCall Message SNDERRMSG 1878067 ERR Equipment Readings not found.
				// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1878067) EXCINTFUN 1878815 EquipmentReadings.geteReadingByDate 
				// DEBUG genFunctionCall END
			}
			// 
		} else {
			// *OTHERWISE
			// * NOTE: Update parameters on generic error message.
			// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
			// DEBUG genFunctionCall Message SNDERRMSG 1367383 ERR Return Code invalid      Send error message
			// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1878815 EquipmentReadings.geteReadingByDate 
			// DEBUG genFunctionCall END
		}
		if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Ignore
			// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Quit
			// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
			// 
		} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Exit
			// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
			// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			// DEBUG genFunctionCall END
		}
	}
	
	/************ end USER ************/


