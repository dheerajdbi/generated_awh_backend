
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: getEquipmentReadings of type RTVOBJ
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 41 USER: Process Data Record
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000013 ACT PAR = DB1,CON By name
	// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
	// DEBUG genFunctionCall END
	// * Assume this is a FIFO partial key access.
	// DEBUG genFunctionCall BEGIN 1000020 ACT <-- *QUIT
	// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 48 USER: Initialize Routine
	***********************************/
	// Unprocessed SUB 48 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 52 USER: Processing if Data Record Not Found
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000002 ACT PAR = CON By name
	dto.setAwhRegionCode("");
	dto.setPlantEquipmentCode("");
	dto.setReadingTimestamp(LocalDateTime.of(1801, 1, 1, 0, 0));
	dto.setReadingType(ReadingTypeEnum.fromCode(""));
	dto.setReadingValue(0L);
	dto.setReadingComment("");
	dto.setUserId("");
	dto.setEquipmentLeasor("");
	dto.setTransferToFromSgt(0L);
	dto.setCentreCodeKey("");
	dto.setAwhBuisnessSegment(AwhBuisnessSegmentEnum.fromCode(""));
	dto.setEquipmentTypeCode("");
	dto.setReadingRequiredDate(LocalDate.of(1801, 1, 1));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000009 ACT PGM.*Return code = CND.*Record does not exist
	dto.set_SysReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 61 USER: Exit Processing
	***********************************/
	// Unprocessed SUB 61 - 
	
	/************ end USER ************/


