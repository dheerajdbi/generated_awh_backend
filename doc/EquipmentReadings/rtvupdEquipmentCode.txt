
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: rtvupdEquipmentCode of type RTVOBJ
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 41 USER: Process Data Record
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000006 ACT ChgE Equipment Code - Equipment Readings  * On error, quit
	chgeEquipmentCodeParams = new ChgeEquipmentCodeParams();
	chgeEquipmentCodeResult = new ChgeEquipmentCodeResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto, chgeEquipmentCodeParams);
	chgeEquipmentCodeParams.setErrorProcessing("5");
	chgeEquipmentCodeParams.setEquipmentReadingSgt(equipmentReadings.getEquipmentReadingSgt());
	chgeEquipmentCodeParams.setPlantEquipmentCode(dto.getNewPlantEquipmentCode());
	stepResult = chgeEquipmentCodeService.execute(chgeEquipmentCodeParams);
	chgeEquipmentCodeResult = (ChgeEquipmentCodeResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(chgeEquipmentCodeResult.get_SysReturnCode());
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 48 USER: Initialize Routine
	***********************************/
	// Unprocessed SUB 48 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 52 USER: Processing if Data Record Not Found
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000002 ACT PGM.*Return code = CND.*Record does not exist
	dto.set_SysReturnCode(ReturnCodeEnum.fromCode("Y2U0005"));
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 61 USER: Exit Processing
	***********************************/
	// Unprocessed SUB 61 - 
	
	/************ end USER ************/


