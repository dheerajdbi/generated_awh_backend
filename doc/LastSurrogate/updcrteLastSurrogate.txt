
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: updcrteLastSurrogate of type EXCINTFUN
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 2 
	***********************************/
	// * WARNING: This function must never be implemented as a sub-routine
	// * as the use of *QUIT for the requested action will not work.
	// DEBUG genFunctionCall BEGIN 1000010 ACT UpdCrt Last Surrogate - Last Surrogate  *
	updcrtLastSurrogateParams = new UpdcrtLastSurrogateParams();
	updcrtLastSurrogateResult = new UpdcrtLastSurrogateResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto, updcrtLastSurrogateParams);
	updcrtLastSurrogateParams.setLastSurrogateType(dto.getLastSurrogateType());
	stepResult = updcrtLastSurrogateService.execute(updcrtLastSurrogateParams);
	updcrtLastSurrogateResult = (UpdcrtLastSurrogateResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(updcrtLastSurrogateResult.get_SysReturnCode());
	dto.setLastSurrogate(updcrtLastSurrogateResult.getLastSurrogate());
	// DEBUG genFunctionCall END
	if (!(dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK"))) {
		// NOT PGM.*Return code is *Normal
		if (ErrorProcessingEnum.isDoNotSendMessage(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Do not send message
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000133 ACT Send error message - ' Return Code invalid'
			// DEBUG genFunctionCall Message SNDERRMSG 1367383 ERR Return Code invalid      Send error message
			// TODO: Unsupported Function Type 'Send Error Message' SNDERRMSG (message surrogate = 1367383) EXCINTFUN 1464895 LastSurrogate.updcrteLastSurrogate 
			// DEBUG genFunctionCall END
		}
		if (ErrorProcessingEnum.isIgnore(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Ignore
			// DEBUG genFunctionCall BEGIN 1000104 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else if (ErrorProcessingEnum.isQuit(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Quit
			// DEBUG genFunctionCall BEGIN 1000093 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
			// 
		} else if (ErrorProcessingEnum.isExit(dto.getErrorProcessing())) {
			// PAR.Error Processing is *Exit
			// DEBUG genFunctionCall BEGIN 1000096 ACT Exit program - return code PGM.*Return code
			// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
			// DEBUG genFunctionCall END
		}
	}
	
	/************ end USER ************/


