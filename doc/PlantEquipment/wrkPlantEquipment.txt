
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: wrkPlantEquipment of type DSPFIL
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 20 USER: Initialize Program
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000884 ACT GetE Centres Region - AWH Region/Centre  *
	geteCentresRegionParams = new GeteCentresRegionParams();
	geteCentresRegionResult = new GeteCentresRegionResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteCentresRegionParams);
	geteCentresRegionParams.setCentreCodeKey(dto.getCentreCodeKey());
	geteCentresRegionParams.setErrorProcessing("2");
	geteCentresRegionParams.setGetRecordOption("*BLANK");
	stepResult = geteCentresRegionService.execute(geteCentresRegionParams);
	geteCentresRegionResult = (GeteCentresRegionResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(geteCentresRegionResult.get_SysReturnCode());
	dto.setAwhRegionCode(geteCentresRegionResult.getAwhRegionCode());
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000910 ACT LCL.Reading Required Date = JOB.*System timestamp
	;
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1001115 ACT LCL.Equipment Status = CND.Active
	dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1001176 ACT LCL.New Program Date = JOB.*Job date + CON.-2 *DAYS
	dto.setLclNewProgramDate(BuildInFunctionUtils.getDateIncrement(
	LocalDate.now(), -2, "DY", "1111111", null, Boolean.FALSE, Boolean.FALSE));
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 41 USER: Initialize Subfile Record from DBF Record
	***********************************/
	if ((dto.getLclFirstPass() == FirstPassEnum.fromCode("Y")) && !(CmdKeyEnum.isNextPage(dto.get_SysCmdKey()))) {
		// DEBUG genFunctionCall BEGIN 1000723 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
		// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000727 ACT LCL.First Pass = CND.Not First Pass
		dto.setLclFirstPass(FirstPassEnum.fromCode("N"));
		// DEBUG genFunctionCall END
		// 
	} else if ((dto.getLclFirstPass() == FirstPassEnum.fromCode("N")) && (CmdKeyEnum.isNextPage(dto.get_SysCmdKey()))) {
		// DEBUG genFunctionCall BEGIN 1000738 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
		// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000742 ACT LCL.First Pass = CND.First Pass
		dto.setLclFirstPass(FirstPassEnum.fromCode("Y"));
		// DEBUG genFunctionCall END
	}
	// * Get previous reading.
	// DEBUG genFunctionCall BEGIN 1001008 ACT LCL.Reading Type = CND.Any
	dto.setLclReadingType(ReadingTypeEnum.fromCode("NE"));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000761 ACT GetE By reverse date - Equipment Readings  *
	geteByReverseDateParams = new GeteByReverseDateParams();
	geteByReverseDateResult = new GeteByReverseDateResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
	geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
	geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
	geteByReverseDateParams.setErrorProcessing("2");
	geteByReverseDateParams.setGetRecordOption("I");
	geteByReverseDateParams.setReadingType(dto.getLclReadingType());
	geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
	stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
	geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
	dto.setLclReadingValue(geteByReverseDateResult.getReadingValue());
	dto.setLclDateAdvised(geteByReverseDateResult.getReadingRequiredDate());
	dto.setLclReadingType(geteByReverseDateResult.getReadingType());
	// DEBUG genFunctionCall END
	if (!(dto.getReadingType() == ReadingTypeEnum.fromCode("*BLANK"))) {
		// NOT CTL.Reading Type is Not entered
		if (
		// DEBUG ComparatorJavaGenerator BEGIN
		dto.getReadingType() != dto.getLclReadingType()
		// DEBUG ComparatorJavaGenerator END
		) {
			// CTL.Reading Type NE LCL.Reading Type
			// DEBUG genFunctionCall BEGIN 1001247 ACT PGM.*Record selected = CND.*NO
			dto.set_SysRecordSelected(RecordSelectedEnum.fromCode("N"));
			// DEBUG genFunctionCall END
		}
	}
	if ((dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) && (gdo.getEquipmentStatus() == EquipmentStatusEnum.fromCode("RT"))) {
		// DEBUG genFunctionCall BEGIN 1001033 ACT PGM.*Record selected = CND.*NO
		dto.set_SysRecordSelected(RecordSelectedEnum.fromCode("N"));
		// DEBUG genFunctionCall END
	}
	if ((!gdo.getVisualEquipmentCode().equals("")) && (dto.getTextField19Chars().equals("** Visual Codes  **L"))) {
		// DEBUG genFunctionCall BEGIN 1001306 ACT RCD.Display Plant equip Code = RCD.Visual Equipment Code
		gdo.setDisplayPlantEquipCode(String.format("%-10s", gdo.getVisualEquipmentCode()));
		// DEBUG genFunctionCall END
	} else {
		// *OTHERWISE
		// DEBUG genFunctionCall BEGIN 1001314 ACT RCD.Display Plant equip Code = RCD.Plant Equipment Code
		gdo.setDisplayPlantEquipCode(gdo.getPlantEquipmentCode());
		// DEBUG genFunctionCall END
	}
	if (dto.get_SysRecordSelected() == RecordSelectedEnum.fromCode("Y")) {
		// PGM.*Record selected is *YES
		// DEBUG genFunctionCall BEGIN 1001012 ACT RCD.Reading Type = LCL.Reading Type
		gdo.setReadingType(dto.getLclReadingType());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000804 ACT Cvt Date to DD/MM format - Utilities Date/Time  *
		cvtDateToDdMmFormatParams = new CvtDateToDdMmFormatParams();
		cvtDateToDdMmFormatResult = new CvtDateToDdMmFormatResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), cvtDateToDdMmFormatParams);
		cvtDateToDdMmFormatParams.setDateFromIso(dto.getLclDateAdvised());
		stepResult = cvtDateToDdMmFormatService.execute(cvtDateToDdMmFormatParams);
		cvtDateToDdMmFormatResult = (CvtDateToDdMmFormatResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(cvtDateToDdMmFormatResult.get_SysReturnCode());
		gdo.setDateDdmm(cvtDateToDdMmFormatResult.getDateDdmm());
		// DEBUG genFunctionCall END
		if (
		// DEBUG ComparatorJavaGenerator BEGIN
		dto.getLclDateAdvised().isBefore(dto.getLclNewProgramDate()) || dto.getLclDateAdvised().equals(dto.getLclNewProgramDate())
		// DEBUG ComparatorJavaGenerator END
		) {
			// LCL.Date Advised LE LCL.New Program Date
			// DEBUG genFunctionCall BEGIN 1001182 ACT RCD.Flag 1 = CND.No
			gdo.setFlag1(Flag1Enum.fromCode("N"));
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1001192 ACT RCD.Flag 1 = CND.Blank
			gdo.setFlag1(Flag1Enum.fromCode(""));
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1001016 ACT LCL.Reading Type = CND.Service Reading
		dto.setLclReadingType(ReadingTypeEnum.fromCode("SV"));
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1000810 ACT GetE By reverse date - Equipment Readings  *
		geteByReverseDateParams = new GeteByReverseDateParams();
		geteByReverseDateResult = new GeteByReverseDateResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), geteByReverseDateParams);
		geteByReverseDateParams.setAwhRegionCode(gdo.getAwhRegionCode());
		geteByReverseDateParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
		geteByReverseDateParams.setErrorProcessing("2");
		geteByReverseDateParams.setGetRecordOption("I");
		geteByReverseDateParams.setReadingType(dto.getLclReadingType());
		geteByReverseDateParams.setReadingRequiredDate(dto.getLclReadingRequiredDate());
		stepResult = geteByReverseDateService.execute(geteByReverseDateParams);
		geteByReverseDateResult = (GeteByReverseDateResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(geteByReverseDateResult.get_SysReturnCode());
		dto.setLclServiceValue(geteByReverseDateResult.getReadingValue());
		dto.setLclReadingType(geteByReverseDateResult.getReadingType());
		// DEBUG genFunctionCall END
		// * Calculate value since Service
		if (dto.getLclServiceValue() != 0) {
			// LCL.Service Value is Entered
			// DEBUG genFunctionCall BEGIN 1000832 ACT RCD.Reading Value = LCL.Reading Value - LCL.Service Value
			gdo.setReadingValue(dto.getLclReadingValue() - dto.getLclServiceValue());
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1001004 ACT RCD.Last Reading = LCL.Reading Value
		gdo.setLastReading(dto.getLclReadingValue());
		// DEBUG genFunctionCall END
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 72 USER: Process Subfile Control (Pre-Confirm)
	***********************************/
	if (CmdKeyEnum.isCancel_1(dto.get_SysCmdKey())) {
		// CTL.*CMD key is *Cancel
		// DEBUG genFunctionCall BEGIN 1001124 ACT PAR.Exit Program Option = CND.Cancel requested
		dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("C"));
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1001128 ACT Exit program - return code CND.*Normal
		// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
		// DEBUG genFunctionCall END
	}
	if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("10")) {
		// CTL.*CMD key is CF10
		if (dto.getTextField19Chars().equals("** Visual Codes  **L")) {
			// CTL.Text Field 19 chars is Visual Code
			// DEBUG genFunctionCall BEGIN 1001340 ACT CTL.Text Field 19 chars = CND.Leasor Code
			dto.setTextField19Chars("** Leasor Codes **");
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1001358 ACT CTL.Text Field 19 chars = CND.Visual Code
			dto.setTextField19Chars("** Visual Codes  **");
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1001350 ACT PGM.*Reload subfile = CND.*YES
		dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
		// DEBUG genFunctionCall END
	}
	if (dto.get_SysCmdKey() == CmdKeyEnum.fromCode("11")) {
		// CTL.*CMD key is CF11
		if (dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) {
			// LCL.Equipment Status is Active
			// DEBUG genFunctionCall BEGIN 1001134 ACT LCL.Equipment Status = CND.Not Entered
			dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode(""));
			// DEBUG genFunctionCall END
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1001142 ACT LCL.Equipment Status = CND.Active
			dto.setLclEquipmentStatus(EquipmentStatusEnum.fromCode("AT"));
			// DEBUG genFunctionCall END
		}
		// DEBUG genFunctionCall BEGIN 1001148 ACT PGM.*Reload subfile = CND.*YES
		dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
		// DEBUG genFunctionCall END
	}
	if (CmdKeyEnum.isAdd(dto.get_SysCmdKey())) {
		// CTL.*CMD key is Add
		// DEBUG genFunctionCall BEGIN 1000450 ACT AddE Plant Equipment - Plant Equipment  *
		addePlantEquipmentParams = new AddePlantEquipmentParams();
		addePlantEquipmentResult = new AddePlantEquipmentResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addePlantEquipmentParams);
		addePlantEquipmentParams.setErrorProcessing("4");
		addePlantEquipmentParams.setAwhRegionCode(dto.getAwhRegionCode());
		// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (72) -> EXCINTFUN PlantEquipment.addePlantEquipment
		stepResult = addePlantEquipmentService.execute(addePlantEquipmentParams);
		addePlantEquipmentResult = (AddePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(addePlantEquipmentResult.get_SysReturnCode());
		dto.setLclExitProgramOption(addePlantEquipmentResult.getExitProgramOption());
		// DEBUG genFunctionCall END
		if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
			// LCL.Exit Program Option is Exit requested
			// * NOTE: This is only required where fast exit is to be enabled.
			// * Remove this condition check when fast exit is not required.
			// * All displays that are the first from menu must ignore fast exit.
			// DEBUG genFunctionCall BEGIN 1001373 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// * Ignore exit program option.
			// DEBUG genFunctionCall BEGIN 1000471 ACT PAR.Exit Program Option = CND.Normal
			dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
			// DEBUG genFunctionCall END
		}
		// * Subfile record processing will be bypassed.
		// DEBUG genFunctionCall BEGIN 1000475 ACT PGM.*Reload subfile = CND.*YES
		dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
		// DEBUG genFunctionCall END
	}
	if (
	// DEBUG ComparatorJavaGenerator BEGIN
	!dto.getAwhRegionCode().equals(dto.getLclAwhRegionCode())
	// DEBUG ComparatorJavaGenerator END
	) {
		// CTL.AWH Region Code NE LCL.AWH Region Code
		// DEBUG genFunctionCall BEGIN 1001210 ACT LCL.AWH Region Code = CTL.AWH Region Code
		dto.setLclAwhRegionCode(dto.getAwhRegionCode());
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1001214 ACT CTL.Centre Code           KEY = CND.Not entered
		dto.setCentreCodeKey("");
		// DEBUG genFunctionCall END
	}
	if (!dto.getAwhRegionCode().equals("")) {
		// CTL.AWH Region Code is Entered
		// DEBUG genFunctionCall BEGIN 1000906 ACT AWH region Name Drv      *FIELD                                             CTLA
		// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
		// DEBUG genFunctionCall END
	}
	// * Condition for normal processing of subfile records.
	// DEBUG genFunctionCall BEGIN 1000601 ACT LCL.Exit Program Option = CND.Normal
	dto.setLclExitProgramOption(ExitProgramOptionEnum.fromCode(""));
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 101 USER: Process Subfile Record (Pre-Confirm)
	***********************************/
	if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("C")) {
		// LCL.Exit Program Option is Cancel requested
		// * Reprocess this record next time user presses Enter.
		// DEBUG genFunctionCall BEGIN 1000578 ACT PGM.*Re-read Subfile Record = CND.*YES
		dto.set_SysReReadSubfileRecord(ReReadSubfileRecordEnum.fromCode("1"));
		// DEBUG genFunctionCall END
		// 
	} else {
		// *OTHERWISE
		if (!(gdo.get_SysSelected().equals("Not entered"))) {
			// NOT RCD.*SFLSEL is Not entered
			// DEBUG genFunctionCall BEGIN 1000320 ACT *Set Cursor: RCD.*SFLSEL  (*Override=*YES)
			// TODO: Unsupported Internal Function Type '*SET CURSOR' (message surrogate = 1002398)
			// DEBUG genFunctionCall END
		}
		if (gdo.get_SysSelected().equals("Change") || gdo.get_SysSelected().equals("2")) {
			// RCD.*SFLSEL is Edit
			// DEBUG genFunctionCall BEGIN 1000616 ACT EdtE Plant Equipment - Plant Equipment  *
			edtePlantEquipmentParams = new EdtePlantEquipmentParams();
			edtePlantEquipmentResult = new EdtePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), edtePlantEquipmentParams);
			edtePlantEquipmentParams.setProgramMode("CHG");
			edtePlantEquipmentParams.setErrorProcessing("4");
			edtePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
			edtePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.edtePlantEquipment
			stepResult = edtePlantEquipmentService.execute(edtePlantEquipmentParams);
			edtePlantEquipmentResult = (EdtePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(edtePlantEquipmentResult.get_SysReturnCode());
			dto.setLclExitProgramOption(edtePlantEquipmentResult.getExitProgramOption());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1000639 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000643 ACT PGM.*Reload subfile = CND.*YES
			dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			// DEBUG genFunctionCall END
		}
		if (gdo.get_SysSelected().equals("4") || gdo.get_SysSelected().equals("*Delete#2")) {
			// RCD.*SFLSEL is *Delete request
		}
		if (gdo.get_SysSelected().equals("5#1") || gdo.get_SysSelected().equals("5#2")) {
			// RCD.*SFLSEL is Display
			// DEBUG genFunctionCall BEGIN 1000650 ACT DspE Plant Equipment - Plant Equipment  *
			dspePlantEquipmentParams = new DspePlantEquipmentParams();
			dspePlantEquipmentResult = new DspePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), dspePlantEquipmentParams);
			dspePlantEquipmentParams.setErrorProcessing("4");
			dspePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
			dspePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.dspePlantEquipment
			stepResult = dspePlantEquipmentService.execute(dspePlantEquipmentParams);
			dspePlantEquipmentResult = (DspePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(dspePlantEquipmentResult.get_SysReturnCode());
			dto.setLclExitProgramOption(dspePlantEquipmentResult.getExitProgramOption());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1000674 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
		}
		if (gdo.get_SysSelected().equals("R")) {
			// RCD.*SFLSEL is R
			// DEBUG genFunctionCall BEGIN 1000967 ACT WrkE Equipment Readings - Equipment Readings  *
			wrkeEquipmentReadingsParams = new WrkeEquipmentReadingsParams();
			wrkeEquipmentReadingsResult = new WrkeEquipmentReadingsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), wrkeEquipmentReadingsParams);
			wrkeEquipmentReadingsParams.setErrorProcessing("4");
			wrkeEquipmentReadingsParams.setAwhRegionCode(gdo.getAwhRegionCode());
			wrkeEquipmentReadingsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN EquipmentReadings.wrkeEquipmentReadings
			stepResult = wrkeEquipmentReadingsService.execute(wrkeEquipmentReadingsParams);
			wrkeEquipmentReadingsResult = (WrkeEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(wrkeEquipmentReadingsResult.get_SysReturnCode());
			dto.setLclExitProgramOption(wrkeEquipmentReadingsResult.getExitProgramOption());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1000994 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
		}
		if (gdo.get_SysSelected().equals("A")) {
			// RCD.*SFLSEL is A
			// DEBUG genFunctionCall BEGIN 1000925 ACT AddE:M Equipment Readings - Equipment Readings  *
			addemEquipmentReadingsParams = new AddemEquipmentReadingsParams();
			addemEquipmentReadingsResult = new AddemEquipmentReadingsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), addemEquipmentReadingsParams);
			addemEquipmentReadingsParams.setErrorProcessing("4");
			addemEquipmentReadingsParams.setAwhRegionCode(gdo.getAwhRegionCode());
			addemEquipmentReadingsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			addemEquipmentReadingsParams.setReadingRequiredDate("0001-01-01");
			// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN EquipmentReadings.addemEquipmentReadings
			stepResult = addemEquipmentReadingsService.execute(addemEquipmentReadingsParams);
			addemEquipmentReadingsResult = (AddemEquipmentReadingsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(addemEquipmentReadingsResult.get_SysReturnCode());
			dto.setLclExitProgramOption(addemEquipmentReadingsResult.getExitProgramOption());
			gdo.setReadingValue(addemEquipmentReadingsResult.getReadingValue());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// DEBUG genFunctionCall BEGIN 1001377 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1000952 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1000956 ACT PGM.*Reload subfile = CND.*YES
			dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			// DEBUG genFunctionCall END
		}
		if (gdo.get_SysSelected().equals("B")) {
			// RCD.*SFLSEL is B
			// DEBUG genFunctionCall BEGIN 1001094 ACT UpdXE:E Reading Totals - Equipment Readings  *
			updxeeReadingTotalsParams = new UpdxeeReadingTotalsParams();
			updxeeReadingTotalsResult = new UpdxeeReadingTotalsResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), updxeeReadingTotalsParams);
			updxeeReadingTotalsParams.setErrorProcessing("2");
			updxeeReadingTotalsParams.setAwhRegionCode(gdo.getAwhRegionCode());
			updxeeReadingTotalsParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			stepResult = updxeeReadingTotalsService.execute(updxeeReadingTotalsParams);
			updxeeReadingTotalsResult = (UpdxeeReadingTotalsResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(updxeeReadingTotalsResult.get_SysReturnCode());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// DEBUG genFunctionCall BEGIN 1001075 ACT PAR.Exit Program Option = CND.Exit requested
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("E"));
				// DEBUG genFunctionCall END
				// DEBUG genFunctionCall BEGIN 1001079 ACT Exit program - return code CND.*Normal
				// TODO: Unsupported Internal Function Type '*EXIT PROGRAM' (message surrogate = 1001627)
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1001086 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1001090 ACT PGM.*Reload subfile = CND.*YES
			dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			// DEBUG genFunctionCall END
		}
		if (gdo.get_SysSelected().equals("N")) {
			// RCD.*SFLSEL is N
			// DEBUG genFunctionCall BEGIN 1001384 ACT ReName:ME Plant Equipment - Plant Equipment  *
			renamemePlantEquipmentParams = new RenamemePlantEquipmentParams();
			renamemePlantEquipmentResult = new RenamemePlantEquipmentResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto.getPageGdo().getContent().get(0), renamemePlantEquipmentParams);
			renamemePlantEquipmentParams.setErrorProcessing("4");
			renamemePlantEquipmentParams.setAwhRegionCode(gdo.getAwhRegionCode());
			renamemePlantEquipmentParams.setPlantEquipmentCode(gdo.getPlantEquipmentCode());
			// TODO SPLIT FUNCTION DSPFIL PlantEquipment.wrkPlantEquipment (101) -> EXCINTFUN PlantEquipment.renamemePlantEquipment
			stepResult = renamemePlantEquipmentService.execute(renamemePlantEquipmentParams);
			renamemePlantEquipmentResult = (RenamemePlantEquipmentResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(renamemePlantEquipmentResult.get_SysReturnCode());
			dto.setLclExitProgramOption(renamemePlantEquipmentResult.getExitProgramOption());
			// DEBUG genFunctionCall END
			if (dto.getLclExitProgramOption() == ExitProgramOptionEnum.fromCode("E")) {
				// LCL.Exit Program Option is Exit requested
				// * NOTE: This is only required where fast exit is to be enabled.
				// * Remove this condition check when fast exit is not required.
				// * All displays that are the first from menu must ignore fast exit.
				// 
			} else {
				// *OTHERWISE
				// * Ignore exit program option.
				// DEBUG genFunctionCall BEGIN 1001408 ACT PAR.Exit Program Option = CND.Normal
				dto.setExitProgramOption(ExitProgramOptionEnum.fromCode(""));
				// DEBUG genFunctionCall END
			}
			// DEBUG genFunctionCall BEGIN 1001412 ACT PGM.*Reload subfile = CND.*YES
			dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
			// DEBUG genFunctionCall END
		}
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 132 USER: Exit Program Processing
	***********************************/
	if (CmdKeyEnum.isExit(dto.get_SysCmdKey())) {
		// CTL.*CMD key is *Exit
		// DEBUG genFunctionCall BEGIN 1000251 ACT PAR.Exit Program Option = CND.Exit requested
		dto.setExitProgramOption(ExitProgramOptionEnum.fromCode("E"));
		// DEBUG genFunctionCall END
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 140 USER: Process Command Keys
	***********************************/
	// Unprocessed SUB 140 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 170 CALC: Subfile Record Function Fields
	***********************************/
	// Unprocessed SUB 170 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 175 CALC: Subfile Control Function Fields
	***********************************/
	if (
	// DEBUG ComparatorJavaGenerator BEGIN
	dto.getReadingType() != dto.getLclReadingType()
	// DEBUG ComparatorJavaGenerator END
	) {
		// CTL.Reading Type NE PAR.Reading Type
		// DEBUG genFunctionCall BEGIN 1001048 ACT PGM.*Reload subfile = CND.*YES
		dto.set_SysReloadSubfile(ReloadSubfileEnum.fromCode("Y"));
		// DEBUG genFunctionCall END
		// DEBUG genFunctionCall BEGIN 1001054 ACT PAR.Reading Type = CTL.Reading Type
		dto.setLclReadingType(dto.getReadingType());
		// DEBUG genFunctionCall END
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 182 USER: Initialize Subfile Control
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000689 ACT LCL.First Pass = CND.First Pass
	dto.setLclFirstPass(FirstPassEnum.fromCode("Y"));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1001221 ACT LCL.AWH Region Code = CTL.AWH Region Code
	dto.setLclAwhRegionCode(dto.getAwhRegionCode());
	// DEBUG genFunctionCall END
	if (dto.getTextField19Chars() != null && dto.getTextField19Chars().isEmpty()) {
		// CTL.Text Field 19 chars is Not entered
		// DEBUG genFunctionCall BEGIN 1001326 ACT CTL.Text Field 19 chars = CND.Visual Code
		dto.setTextField19Chars("** Visual Codes  **");
		// DEBUG genFunctionCall END
	}
	if (!dto.getAwhRegionCode().equals("")) {
		// CTL.AWH Region Code is Entered
		// DEBUG genFunctionCall BEGIN 1000896 ACT AWH region Name Drv      *FIELD                                             CTLA
		// TODO: Action Diagram Element of type "ACT" without an associated Message Surrogate (this is probably an error)
		// DEBUG genFunctionCall END
	}
	if (dto.getLclEquipmentStatus() == EquipmentStatusEnum.fromCode("AT")) {
		// LCL.Equipment Status is Active
		// DEBUG genFunctionCall BEGIN 1001103 ACT CTL.*Condition name = CON.*Active Only*
		dto.set_SysConditionName("*Active Only*");
		// DEBUG genFunctionCall END
	} else {
		// *OTHERWISE
		// DEBUG genFunctionCall BEGIN 1001109 ACT CTL.*Condition name = CON.*All Status*
		dto.set_SysConditionName("*All Status*");
		// DEBUG genFunctionCall END
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 209 USER: Process Subfile Record (Post-Confirm Pass)
	***********************************/
	// Unprocessed SUB 209 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 222 USER: Final processing (Pre-confirm)
	***********************************/
	// Unprocessed SUB 222 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 225 USER: Process subfile control (Post-confirm)
	***********************************/
	// Unprocessed SUB 225 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 228 USER: Final processing (Post-confirm)
	***********************************/
	// Unprocessed SUB 228 - 
	
	/************ end USER ************/


