
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: readLdaParamsPreloaded of type EXCINTFUN
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 2 
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000039 ACT Read LDA User Source - Utilities & User Source  *
	readLdaUserSourceParams = new ReadLdaUserSourceParams();
	readLdaUserSourceResult = new ReadLdaUserSourceResult();
	// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
	BeanUtils.copyProperties(dto, readLdaUserSourceParams);
	stepResult = readLdaUserSourceService.execute(readLdaUserSourceParams);
	readLdaUserSourceResult = (ReadLdaUserSourceResult)((ReturnFromService)stepResult.getAction()).getResults();
	dto.set_SysReturnCode(readLdaUserSourceResult.get_SysReturnCode());
	dto.setWfLdaTerminal(readLdaUserSourceResult.getLdaTerminal());
	dto.setWfLdaUser(readLdaUserSourceResult.getLdaUser());
	dto.setWfLdaCostCentre(readLdaUserSourceResult.getLdaCostCentre());
	dto.setWfLdaSecurityProfile(readLdaUserSourceResult.getLdaSecurityProfile());
	dto.setWfLdaNetworkSystemName(readLdaUserSourceResult.getLdaNetworkSystemName());
	dto.setWfLdaStateCostCentre(readLdaUserSourceResult.getLdaStateCostCentre());
	dto.setWfLdaPrinter(readLdaUserSourceResult.getLdaPrinter());
	dto.setWfLdaStateHighSpeedPrtr(readLdaUserSourceResult.getLdaStateHighSpeedPrtr());
	dto.setWfLdaEnvironment(readLdaUserSourceResult.getLdaEnvironment().getCode());
	dto.setWfLdaEldersStateCode(readLdaUserSourceResult.getLdaEldersStateCode());
	dto.setWfLdaEldersStateNumCode(readLdaUserSourceResult.getLdaEldersStateNumCode());
	dto.setWfLdaOutputCostCentre(readLdaUserSourceResult.getLdaOutputCostCentre());
	dto.setWfLdaHomeCostCentre(readLdaUserSourceResult.getLdaHomeCostCentre());
	dto.setWfLdaAttnMenuSwitch(readLdaUserSourceResult.getLdaAttnMenuSwitch());
	dto.setWfLdaCurrentBrokerId(readLdaUserSourceResult.getLdaCurrentBrokerId());
	dto.setWfLdaCurrentStateCode(readLdaUserSourceResult.getLdaCurrentStateCode().getCode());
	dto.setWfLdaCurrentCentreCode(readLdaUserSourceResult.getLdaCurrentCentreCode());
	dto.setWfLdaDefaultBrokerId(readLdaUserSourceResult.getLdaDefaultBrokerId());
	dto.setWfLdaDefaultStateCode(readLdaUserSourceResult.getLdaDefaultStateCode().getCode());
	dto.setWfLdaDefaultCentreCode(readLdaUserSourceResult.getLdaDefaultCentreCode());
	dto.setWfLdaDftSaleNbrSlgCtr(readLdaUserSourceResult.getLdaDftSaleNbrSlgCtr());
	dto.setWfLdaDftSaleNbrId(readLdaUserSourceResult.getLdaDftSaleNbrId());
	dto.setWfLdaDftSaleNbrStgCtr(readLdaUserSourceResult.getLdaDftSaleNbrStgCtr());
	dto.setWfLdaProcSaleNbrSlgCtr(readLdaUserSourceResult.getLdaProcSaleNbrSlgCtr());
	dto.setWfLdaProcSaleNbrId(readLdaUserSourceResult.getLdaProcSaleNbrId());
	dto.setWfLdaProcSaleNbrStgCtr(readLdaUserSourceResult.getLdaProcSaleNbrStgCtr());
	dto.setWfLdaClientAccountId(readLdaUserSourceResult.getLdaClientAccountId());
	dto.setWfLdaWoolNumber(readLdaUserSourceResult.getLdaWoolNumber());
	dto.setWfLdaClipCode(readLdaUserSourceResult.getLdaClipCode());
	dto.setWfLdaLotNumber(readLdaUserSourceResult.getLdaLotNumber());
	dto.setWfLdaFolioNumber(readLdaUserSourceResult.getLdaFolioNumber());
	dto.setWfLdaDftSaleSeasonCent(readLdaUserSourceResult.getLdaDftSaleSeasonCent());
	dto.setWfLdaDftSaleSeasonYear(readLdaUserSourceResult.getLdaDftSaleSeasonYear());
	dto.setWfLdaProcSaleSeasonCent(readLdaUserSourceResult.getLdaProcSaleSeasonCent());
	dto.setWfLdaProcSaleSeasonYear(readLdaUserSourceResult.getLdaProcSaleSeasonYear());
	// DEBUG genFunctionCall END
	
	/************ end USER ************/


