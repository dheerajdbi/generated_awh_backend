
-----------------------------------------------------------------------------------
ActionDiagram documentation for function: getDefault of type RTVOBJ
-----------------------------------------------------------------------------------
	/***********************************
	* USER: 41 USER: Process Data Record
	***********************************/
	// DEBUG genFunctionCall BEGIN 1000192 ACT PAR = DB1,CON By name
	// TODO: *MOVE ALL is not supported for parameters: PAR, DB1, CON,    ,    
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000002 ACT PAR.Current Level = CND.Yes
	dto.setCurrentLevel(YesOrNoEnum.fromCode("Y"));
	// DEBUG genFunctionCall END
	// * Assume this is a FIFO partial key access.
	// DEBUG genFunctionCall BEGIN 1000190 ACT <-- *QUIT
	// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
	// DEBUG genFunctionCall END
	
	/************ end USER ************/

	/***********************************
	* USER: 48 USER: Initialize Routine
	***********************************/
	// Unprocessed SUB 48 - 
	
	/************ end USER ************/

	/***********************************
	* USER: 52 USER: Processing if Data Record Not Found
	***********************************/
	// * Ignore return code.
	// DEBUG genFunctionCall BEGIN 1000174 ACT PGM.*Return code = CND.*Normal
	dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000199 ACT PAR = CON By name
	dto.setDefaultValue("");
	dto.setCurrentLevel(YesOrNoEnum.fromCode(""));
	// DEBUG genFunctionCall END
	// DEBUG genFunctionCall BEGIN 1000017 ACT PAR.Current Level = CND.No
	dto.setCurrentLevel(YesOrNoEnum.fromCode("N"));
	// DEBUG genFunctionCall END
	if (dto.getDefaultKey4().equals("*")) {
		// PAR.Default Key 4 is *None
	} else {
		// *OTHERWISE
		if (DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) {
			// PAR.Default Type is Allow Key 2 as Wild
			// DEBUG genFunctionCall BEGIN 1000251 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2("*");
			getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
			getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000263 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000268 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000275 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		if (DefaultTypeEnum.isAllowKey3AsWild(dto.getDefaultType())) {
			// PAR.Default Type is Allow Key 3 as Wild
			// DEBUG genFunctionCall BEGIN 1000280 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
			getDefaultStandardParams.setDefaultKey3("*");
			getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000292 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000297 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000304 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		if ((DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) && (DefaultTypeEnum.isAllowKey3AsWild(dto.getDefaultType()))) {
			// DEBUG genFunctionCall BEGIN 1000222 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2("*");
			getDefaultStandardParams.setDefaultKey3("*");
			getDefaultStandardParams.setDefaultKey4(dto.getDefaultKey4());
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000234 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000239 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000246 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		// DEBUG genFunctionCall BEGIN 1000087 ACT Get Default Standard - Default  *
		getDefaultStandardParams = new GetDefaultStandardParams();
		getDefaultStandardResult = new GetDefaultStandardResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getDefaultStandardParams);
		getDefaultStandardParams.setDefaultType(dto.getDefaultType());
		getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
		getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
		getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
		getDefaultStandardParams.setDefaultKey4("*");
		getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
		stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
		getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
		dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
		// DEBUG genFunctionCall END
		if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
			// PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000099 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
			// 
		} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
			// PGM.*Return code is *Record does not exist
			// DEBUG genFunctionCall BEGIN 1000207 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000105 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}
	}
	if (dto.getDefaultKey3().equals("*")) {
		// PAR.Default Key 3 is *None
	} else {
		// *OTHERWISE
		if (DefaultTypeEnum.isAllowKey2AsWild(dto.getDefaultType())) {
			// PAR.Default Type is Allow Key 2 as Wild
			// DEBUG genFunctionCall BEGIN 1000313 ACT Get Default Standard - Default  *
			getDefaultStandardParams = new GetDefaultStandardParams();
			getDefaultStandardResult = new GetDefaultStandardResult();
			// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
			BeanUtils.copyProperties(dto, getDefaultStandardParams);
			getDefaultStandardParams.setDefaultType(dto.getDefaultType());
			getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
			getDefaultStandardParams.setDefaultKey2("*");
			getDefaultStandardParams.setDefaultKey3(dto.getDefaultKey3());
			getDefaultStandardParams.setDefaultKey4("*");
			getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
			stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
			getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
			dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
			dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
			// DEBUG genFunctionCall END
			if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
				// PGM.*Return code is *Normal
				// DEBUG genFunctionCall BEGIN 1000325 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
				// 
			} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
				// PGM.*Return code is *Record does not exist
				// DEBUG genFunctionCall BEGIN 1000330 ACT PGM.*Return code = CND.*Normal
				dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
				// DEBUG genFunctionCall END
				// 
			} else {
				// *OTHERWISE
				// DEBUG genFunctionCall BEGIN 1000337 ACT <-- *QUIT
				// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
				// DEBUG genFunctionCall END
			}
		}
		// DEBUG genFunctionCall BEGIN 1000116 ACT Get Default Standard - Default  *
		getDefaultStandardParams = new GetDefaultStandardParams();
		getDefaultStandardResult = new GetDefaultStandardResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getDefaultStandardParams);
		getDefaultStandardParams.setDefaultType(dto.getDefaultType());
		getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
		getDefaultStandardParams.setDefaultKey2(dto.getDefaultKey2());
		getDefaultStandardParams.setDefaultKey3("*");
		getDefaultStandardParams.setDefaultKey4("*");
		getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
		stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
		getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
		dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
		// DEBUG genFunctionCall END
		if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
			// PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000128 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
			// 
		} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
			// PGM.*Return code is *Record does not exist
			// DEBUG genFunctionCall BEGIN 1000211 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000134 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}
	}
	if (dto.getDefaultKey2().equals("*")) {
		// PAR.Default Key 2 is *None
	} else {
		// *OTHERWISE
		// DEBUG genFunctionCall BEGIN 1000145 ACT Get Default Standard - Default  *
		getDefaultStandardParams = new GetDefaultStandardParams();
		getDefaultStandardResult = new GetDefaultStandardResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getDefaultStandardParams);
		getDefaultStandardParams.setDefaultType(dto.getDefaultType());
		getDefaultStandardParams.setDefaultKey1(dto.getDefaultKey1());
		getDefaultStandardParams.setDefaultKey2("*");
		getDefaultStandardParams.setDefaultKey3("*");
		getDefaultStandardParams.setDefaultKey4("*");
		getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
		stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
		getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
		dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
		// DEBUG genFunctionCall END
		if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("*BLANK")) {
			// PGM.*Return code is *Normal
			// DEBUG genFunctionCall BEGIN 1000157 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
			// 
		} else if (dto.get_SysReturnCode() == ReturnCodeEnum.fromCode("Y2U0005")) {
			// PGM.*Return code is *Record does not exist
			// DEBUG genFunctionCall BEGIN 1000215 ACT PGM.*Return code = CND.*Normal
			dto.set_SysReturnCode(ReturnCodeEnum.fromCode(""));
			// DEBUG genFunctionCall END
			// 
		} else {
			// *OTHERWISE
			// DEBUG genFunctionCall BEGIN 1000163 ACT <-- *QUIT
			// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
			// DEBUG genFunctionCall END
		}
	}
	if (dto.getDefaultKey1().equals("*")) {
		// PAR.Default Key 1 is *None
		// DEBUG genFunctionCall BEGIN 1000165 ACT PGM.*Return code = CND.Serious error
		dto.set_SysReturnCode(ReturnCodeEnum.fromCode("S-ERROR"));
		// DEBUG genFunctionCall END
		// 
	} else {
		// *OTHERWISE
		// DEBUG genFunctionCall BEGIN 1000056 ACT Get Default Standard - Default  *
		getDefaultStandardParams = new GetDefaultStandardParams();
		getDefaultStandardResult = new GetDefaultStandardResult();
		// TODO VERIFY WHY copyProperties before ActionDiagram generated parameters
		BeanUtils.copyProperties(dto, getDefaultStandardParams);
		getDefaultStandardParams.setDefaultType(dto.getDefaultType());
		getDefaultStandardParams.setDefaultKey1("*");
		getDefaultStandardParams.setDefaultKey2("*");
		getDefaultStandardParams.setDefaultKey3("*");
		getDefaultStandardParams.setDefaultKey4("*");
		getDefaultStandardParams.setDefaultFieldName(dto.getDefaultFieldName());
		stepResult = getDefaultStandardService.execute(getDefaultStandardParams);
		getDefaultStandardResult = (GetDefaultStandardResult)((ReturnFromService)stepResult.getAction()).getResults();
		dto.set_SysReturnCode(getDefaultStandardResult.get_SysReturnCode());
		dto.setDefaultValue(getDefaultStandardResult.getDefaultValue());
		// DEBUG genFunctionCall END
		// * Exit with return code for calling function to process.
		// DEBUG genFunctionCall BEGIN 1000169 ACT <-- *QUIT
		// TODO: Unsupported Internal Function 'QUIT' (message surrogate = 1001684)
		// DEBUG genFunctionCall END
	}
	
	/************ end USER ************/

	/***********************************
	* USER: 61 USER: Exit Processing
	***********************************/
	// Unprocessed SUB 61 - 
	
	/************ end USER ************/


