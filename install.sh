#!/bin/bash
#
#Author : Jun Yang
#
#install.sh - Shell script for node modules installation and running grunt tasks for WSDEVM.

title="Installation and Build"
echo -n -e "\033]0;$title\007"
cd ./client
clear
echo "Select a Task:"
echo "==============="
echo "1) start application      (start the application (npm install and ng serve))"
echo "==============            (Tasks below can be executed individually after installation)"
echo "2) npm install            (install modules locally and execute build)"
echo "3) npm install -g         (install modules globally and execute build)"
echo "4) ng build               (detect errors and potential problems in js code)"
echo "5) ng test                (execute tests using ng test)"
echo "6) ng lint                (execute lint using ng lint)"
echo "7) ng e2e                 (execute e2e using ng e2e)"
echo "10) Exit"

echo Type Option:
read option

case $option in
    1 )
    echo "Starting the client"
    npm cache verify
    npm cache clear -force
    npm install --unsafe-perm
    npm -g install karma@2.0.2
    echo "Installation completed and build task executed"
    echo "Libraries in webapp/libraries"
    echo "css compiled in webapp/styles/App.css"
    echo "Build log in target-grunt/output/log"
    echo "Code documentation in target-grunt/output/code documentation/index.html"
    echo "Build log in target-grunt/output/log"
    echo "jshint report in target-grunt/output/report/js hint"
    echo "csslint report in target-grunt/output/report/css lint"
    echo "Test report in target-grunt/output/report/test/test-report.html"
    echo "Code analysis report in target-grunt/output/report/code analysis/index.html"
    ng serve
    read line;;
    2 )
    echo "Proceeding with npm local installation"
    npm cache verify
    npm cache clear -force
    npm install --unsafe-perm
    npm -g install karma@2.0.2
    echo "Installation completed and build task executed"
    echo "Libraries in webapp/libraries"
    echo "css compiled in webapp/styles/App.css"
    echo "Build log in target-grunt/output/log"
    echo "Code documentation in target-grunt/output/code documentation/index.html"
    echo "Build log in target-grunt/output/log"
    echo "jshint report in target-grunt/output/report/js hint"
    echo "csslint report in target-grunt/output/report/css lint"
    echo "Test report in target-grunt/output/report/test/test-report.html"
    echo "Code analysis report in target-grunt/output/report/code analysis/index.html"
    echo "Done ! Press Enter to Exit"
    read line;;
    3 )
    echo "Proceeding with npm global installation"
    npm cache verify
    npm cache clear -force
    npm install -g
    npm -g install karma@2.0.2
    npm install -g karma-cli
    npm uninstall -g WSDEVM
    npm unpublish WSDEVM --force
    echo "Installation completed and build task executed"
    echo "Libraries in webapp/libraries"
    echo "css compiled in webapp/styles/App.css"
    echo "Build log in target-grunt/output/log"
    echo "Code documentation in target-grunt/output/code documentation/index.html"
    echo "Build log in target-grunt/output/log"
    echo "jshint report in target-grunt/output/report/js hint"
    echo "csslint report in target-grunt/output/report/css lint"
    echo "Test report in target-grunt/output/report/test/test-report.html"
    echo "Code analysis report in target-grunt/output/report/code analysis/index.html"
    echo "Done ! Press Enter to Exit"
    read line;;
    4 )
    echo "Proceeding built"
    ng build
    echo "Done ! Press Enter to Exit"
    read line;;
    5 )
    echo "Proceeding with test execution"
    ng test
    echo "Test report in target-grunt/output/report/test/test-report.html"
    echo "Done ! Press Enter to Exit"
    read line;;
    6 )
    echo "Proceeding lint"
    ng lint
    echo "csslint report in target-grunt/output/report/js hint"
    echo "Done ! Press Enter to Exit"
    read line;;
    7 )
    echo "Proceeding e2e"
    ng e2e
    echo "Test report in target-grunt/output/report/js hint"
    echo "Done ! Press Enter to Exit"
    read line;;
    10 )
    exit 0;;
esac

#end of Script
